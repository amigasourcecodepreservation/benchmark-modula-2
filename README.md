# Benchmark Modula-2

**Description**:  Benchmark Modula-2 is an environment for modula 2 programming on the amiga! This is a work-in-progress of preserving the works of Benchmark Modula-2 and it's different versions.

  - **Technology stack**: 
  - **Status**:  [CHANGELOG](https://gitlab.com/amigasourcecodepreservation/benchmark-modula-2/blob/master/CHANGELOG.md).

**Screenshot**:

![TO-DO](TO-DO)


## Trivia

*Like a lot of programmers, I first learned about Modula-2 from the August 1984 Byte cover story. I'd done a lot of programming in Pascal and Modula-2 was the logical successor. I started with another Modula-2, probably Stony Brook. That was so buggy I gave up on it after a few weeks. Later, I bought Avant-Garde Modula-2 and the add-on libraries. I found out that compiling the definition modules in the proper order for complicated programs was maddeningly difficult. After working out the relationships manually a few times, I wrote a program to handle it. When I offered the program, named "Benchmark Assistant," to Leon Frenkel, I was rather surprised when he offered to sell me the company. It turned out that almost everybody who was interested in Modula-2 had already bought it. I sold a lot of upgrades, but not nearly enough to make my investment back. Tom Breeden contacted me with some suggestions for improvements to Benchmark. He'd written his own version, called Aglet. He volunteered to upgrade the compiler and I was glad to take him up on it.*:wq
  
  - Jim Olinger

*I've also got lots of other sources and executables that might be of interest eg, for Leon's add-on modules and the Debugger. I'm trying to refresh my memory of the M68K days. Apparently when AmigaOS 2.04 came out I released my modules (as Aglet) for Benchmark use of the OS API calls before Jim purchased the Benchmark software system from Leon.*

 *Later, when AmigaOS 3.0 was released, did the same under the label of Armadillo Computing with Jim O.*

 *Armadillo also released an upgraded compiler/linker/editor as Benchmark v1.10, with a number of bug fixes and improvements Jim and I made to v1.04. This should be still compatible with any earlier software that used the earlier version..* 
*I continued working on a v2.0 of the compiler, to be much improved, but still compatible. And then a 32 bit version (the Benchmark compilers had been 16 bit INTEGER/CARDINAL) which would not be compatible.*

 - Tom Breeden

## Dependencies

* Amiga OS Classic
* TO-DO...

## Installation

See the [documentation](https://gitlab.com/amigasourcecodepreservation/benchmark-modula-2-documentation)

## Getting help

If you have questions, concerns, bug reports, etc, please file an issue in this repository's Issue Tracker.

Any ambitious developer who undertakes to improve the compiler should consult this paper available on arXiv.org:

[On the Maintenance of Classic Modula-2 Compilers](https://arxiv.org/abs/1809.07080)
Benjamin Kowarsch, Modula-2 Software Foundation  
September 2018 (arXiv.org preprint)  

----

## License

Benchmark Modula-2 is distributed under the terms of the MIT License. See the [LICENSE](https://gitlab.com/amigasourcecodepreservation/benchmark-modula-2/LICENSE.md) file for details.

----

## Credits and references

Many thanks to Jim Olinger, Tom Breeden for releasing the project under MIT, and again thanks to Tom for searching his archives. Mark Wickens for scanning the original manual and Leon Frenkel for the original work.

## Copyright

All material is copyright Jim Olinger/Armadillo Computing - (The items marked Leon Frenkel etc were sold to Armadillo in the early 90's), except were noted copyright Tom Breeden/Aglet Software.

The editor is based on an early version of [Mg](https://en.wikipedia.org/wiki/Mg_(editor)), at the time called MicroGnuEmacs, a Public Domain EMACS style editor.

## Creators

Jim Olinger, Tom Breeden, Leon Frenkel


