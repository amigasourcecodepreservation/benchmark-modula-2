(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: LongInOut.MOD                     Version: Amiga.00.00             *
 * Created: 07/22/87   Updated: 08/06/87   Author: Leon Frenkel             *
 * Description: Standard LongInOut module for LONGINT and LONGCARD types.   *
 ****************************************************************************)

IMPLEMENTATION MODULE LongInOut;

FROM InOut IMPORT ReadString, WriteString;

(*$L+*)

  (* ================================================================== *)
  (* Procedures For Improving Output Performance Of InOut Module        *)
  CONST NL = 79;
  VAR
    name: ARRAY [0..NL] OF CHAR; (* used as output buffer *)
    bufIdx: CARDINAL; (* buffer index *)

  (* Flush Output Buffer *)
  PROCEDURE FlushBuf();
  BEGIN
    name[bufIdx] := 0C; (* terminate string *)
    WriteString(name);
    bufIdx := 0; (* reset buffer index *)
  END FlushBuf;

  (* Send Char To Output Buffer *)
  PROCEDURE WriteBuf(c: CHAR);
  BEGIN
    name[bufIdx] := c; INC(bufIdx);
    IF (bufIdx = HIGH(name)) THEN (* flush out buffer *)
      FlushBuf();
    END;
  END WriteBuf;
  (* ================================================================== *)

  PROCEDURE ReadLongInt(VAR x: LONGINT);
    VAR i: LONGINT; n: CARDINAL;
      ch: CHAR; neg: BOOLEAN;
      buf: ARRAY [0..19] OF CHAR;

    PROCEDURE next;
    BEGIN ch := buf[n]; INC(n);
    END next;

  BEGIN ReadString(buf); n := 0; next;
    WHILE ch = " " DO next END ;
    IF ch = "-" THEN
      neg := TRUE; next
    ELSE neg := FALSE;
      IF ch = "+" THEN next END
    END ;
    IF ("0" <= ch) & (ch <= "9") THEN
      i := 0D; Done := TRUE;
      REPEAT i := 10D*i + (LONGINT(ch) - LONGINT(60B)); next
      UNTIL (ch < "0") OR ("9" < ch);
      IF neg THEN x := -i ELSE x := i END
    ELSE Done := FALSE
    END
  END ReadLongInt;

  PROCEDURE ReadLongCard(VAR x: LONGCARD);
    VAR i: LONGCARD;
      n: CARDINAL;
      ch: CHAR;
      buf: ARRAY [0..19] OF CHAR;

    PROCEDURE next;
    BEGIN ch := buf[n]; INC(n);
    END next;

  BEGIN ReadString(buf); n := 0; next;
    WHILE ch = " " DO next END ;
    IF ("0" <= ch) & (ch <= "9") THEN
      i := 0; Done := TRUE;
      REPEAT i := 10D*i + (LONGCARD(ch) - LONGCARD(60B)); next
      UNTIL (ch < "0") OR ("9" < ch);
      x := i
    ELSE Done := FALSE
    END
  END ReadLongCard;

  PROCEDURE WriteLongInt(x: LONGINT; n: CARDINAL);
    VAR i: CARDINAL;
      x0: LONGINT;
      a: ARRAY [0..19] OF CHAR;
  BEGIN i := 0; x0 := ABS(x);
    REPEAT a[i] := CHAR(x0 MOD 10D + LONGINT(60B));
      x0 := x0 DIV 10D; INC(i);
    UNTIL x0 = 0D;
    IF x < 0D THEN a[i] := "-"; INC(i); END ;
    WHILE n > i DO
      DEC(n); WriteBuf(" ")
    END ;
    REPEAT DEC(i); WriteBuf(a[i]) UNTIL i = 0;
    FlushBuf();
  END WriteLongInt;

  PROCEDURE WriteLongCard(x: LONGCARD; n: CARDINAL);
    VAR i: CARDINAL;
      a: ARRAY [0..19] OF CARDINAL;
  BEGIN i := 0;
    REPEAT a[i] := x MOD 10D; x := x DIV 10D; INC(i);
    UNTIL x = 0D;
    WHILE n > i DO
      DEC(n); WriteBuf(" ")
    END ;
    REPEAT DEC(i); WriteBuf(CHAR(a[i]+60B)) UNTIL i = 0;
    FlushBuf();
  END WriteLongCard;

  PROCEDURE WriteLongOct(x: LONGCARD; n: CARDINAL);
    VAR i: CARDINAL;
      a: ARRAY [0..19] OF CARDINAL;
  BEGIN i := 0;
    REPEAT a[i] := x MOD 8D; x := x DIV 8D; INC(i);
    UNTIL i = 10;
    IF x = 0D THEN a[i] := 0 ELSE a[i] := 1 END ;
    INC(i);
    WHILE n > i DO
      DEC(n); WriteBuf(" ")
    END ;
    REPEAT DEC(i); WriteBuf(CHAR(a[i]+60B)) UNTIL i = 0;
    FlushBuf();
  END WriteLongOct;

  PROCEDURE WriteLongHex(x: LONGCARD; n: CARDINAL);

    PROCEDURE HexDig(d: LONGCARD);
    BEGIN d := d MOD 16D;
      IF d < 10D THEN INC(d, LONGCARD(60B)) ELSE INC(d, LONGCARD(67B)) END ;
      WriteBuf(CHAR(d))
    END HexDig;

  BEGIN
    WHILE n > 8 DO
      DEC(n); WriteBuf(" ")
    END ;
    
    HexDig(x DIV LONGCARD(10000000H)); 
    HexDig(x DIV LONGCARD(1000000H));
    HexDig(x DIV LONGCARD(100000H));   
    HexDig(x DIV LONGCARD(10000H));
    HexDig(x DIV LONGCARD(1000H));     
    HexDig(x DIV LONGCARD(100H));
    HexDig(x DIV LONGCARD(10H));
    HexDig(x);
    FlushBuf();
  END WriteLongHex;

END LongInOut.
