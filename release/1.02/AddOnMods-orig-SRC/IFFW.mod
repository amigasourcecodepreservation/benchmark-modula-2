(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: IFFW.MOD                          Version: Amiga.00.00             *
 * Created: 04/20/87   Updated: 04/20/87   Author: Leon Frenkel             *
 * Description: IFF file write functions.                                   * 
 ****************************************************************************)

IMPLEMENTATION MODULE IFFW;

FROM SYSTEM   IMPORT ADR, ADDRESS, TSIZE;
FROM AmigaDOS IMPORT FileHandle, OffsetEnd, OffsetCurrent, OffsetBeginning,
		     Seek;
FROM IFF      IMPORT ChunkMoreBytes,
		     ClientFramePtr, szNotYetKnown,
		     EndMark, ShortChunk, ClientError,
		     IFFP, ID, GroupContext, GroupContextPtr,
		     NullChunk, ChunkHeader,
		     NoFile, DOSError, IFFOkay, NotIFF, BadIFF,
		     FORM, CAT, LIST, PROP;
FROM GIO      IMPORT GSeek, GWrite, GWriteFlush;

(*$L+*)


(* ======= PRIVATE ======= *)

(* A macro to test if a chunk size is definite, i.e. not szNotYetKnown.*)
PROCEDURE Known(size: LONGINT): BOOLEAN;
BEGIN
  RETURN (size # szNotYetKnown);
END Known;

(* ======= PUBLIC ======= *)

PROCEDURE OpenWIFF(File: FileHandle; VAR new: GroupContext; limit: LONGINT): IFFP;
VAR
  iffp : IFFP;
BEGIN
  iffp := IFFOkay;

  WITH new DO
    parent       := NIL;
    clientFrame  := NIL;
    file         := File;
    position     := 0D;
    bound        := limit;
    bytesSoFar   := 0D;
    ckHdr.ckID   := NullChunk;
    ckHdr.ckSize := 0D;
  END;

  IF 0D > Seek(File, 0D, OffsetBeginning) THEN (* Go to start of the file *)
    iffp := DOSError;
  ELSIF Known(limit) AND ODD(limit) THEN
    iffp := ClientError;
  END;

  RETURN (iffp);
END OpenWIFF;

PROCEDURE StartWGroup(VAR parent: GroupContext; groupType: ID; groupSize: LONGINT;
                      subtype: ID; VAR new: GroupContext): IFFP;
VAR
  iffp : IFFP;
BEGIN
  iffp := PutCkHdr(parent, groupType, groupSize);
  IF iffp = IFFOkay THEN
    iffp := IFFWriteBytes(parent, ADR(subtype), LONGINT(TSIZE(ID)));
  END;
  IF iffp = IFFOkay THEN
    iffp := OpenWGroup(parent, new);
  END;
  
  RETURN (iffp);
END StartWGroup;

PROCEDURE EndWGroup(VAR old: GroupContext): IFFP;
VAR
  parent : GroupContextPtr;
  iffp : IFFP;
BEGIN
  parent := old.parent;

  iffp := CloseWGroup(old);
  IF iffp = IFFOkay THEN
    iffp := PutCkEnd(parent^);
  END;
  RETURN (iffp);
END EndWGroup;

PROCEDURE OpenWGroup(VAR Parent, new: GroupContext): IFFP;
VAR
  ckEnd : LONGINT;
  iffp : IFFP;
BEGIN
  iffp := IFFOkay;

  WITH new DO
    parent       := ADR(Parent);
    clientFrame  := Parent.clientFrame;
    file         := Parent.file;
    position     := Parent.position;
    bound        := Parent.bound;
    bytesSoFar   := 0D;
    ckHdr.ckID   := NullChunk;
    ckHdr.ckSize := 0D;
  END;

  IF Known(Parent.ckHdr.ckSize) THEN
    ckEnd := new.position + ChunkMoreBytes(Parent);
    IF (new.bound = szNotYetKnown) OR (new.bound > ckEnd) THEN
      new.bound := ckEnd;
    END;
  END;

  IF (Parent.ckHdr.ckID = NullChunk) OR ODD(new.position) OR
     (Known(new.bound) AND ODD(new.bound)) THEN
    iffp := ClientError;
  END;
  RETURN (iffp);
END OpenWGroup;

PROCEDURE CloseWGroup(VAR old: GroupContext): IFFP;
VAR
  iffp : IFFP;
BEGIN
  iffp := IFFOkay;

  IF old.ckHdr.ckID # NullChunk THEN (* didn't close the last chunk *)
    iffp := ClientError;
  ELSIF old.parent = NIL THEN (* top level file context *)
    IF GWriteFlush(old.file) < 0D THEN
      iffp := DOSError;
    END;
  ELSE
    INC(old.parent^.bytesSoFar, old.position - old.parent^.position);
    old.parent^.position := old.position;
  END;
  RETURN (iffp);
END CloseWGroup;

PROCEDURE PutCk(VAR context: GroupContext; ckID: ID; ckSize: LONGINT; 
                data: ADDRESS): IFFP;
VAR
  iffp : IFFP;
BEGIN
  iffp := IFFOkay;

  IF ckSize = szNotYetKnown THEN
    iffp := ClientError;
  END;
  IF iffp = IFFOkay THEN
    iffp := PutCkHdr(context, ckID, ckSize);
  END;
  IF iffp = IFFOkay THEN
    iffp := IFFWriteBytes(context, data, ckSize);
  END;
  IF iffp = IFFOkay THEN
    iffp := PutCkEnd(context);
  END;
  RETURN (iffp);
END PutCk;

PROCEDURE PutCkHdr(VAR context: GroupContext; ckID: ID; ckSize: LONGINT): IFFP;
VAR
  minPSize : LONGINT;
BEGIN
  minPSize := LONGINT(TSIZE(ChunkHeader)); (*physical chunk >= minPSize bytes*)

  (* CLIENT_ERROR if we're already inside a chunk or asked to write
   * other than one FORM, LIST, or CAT at the top level of a file
   * Also, non-positive ID values are illegal and used for error codes.
   * (We could check for other illegal IDs...) *)
  IF (context.ckHdr.ckID # NullChunk) OR (ckID <= 0D) THEN
    RETURN (ClientError);
  ELSIF context.parent = NIL THEN
    IF NOT ((ckID = FORM) OR (ckID = LIST) OR (ckID = CAT)) THEN
      RETURN (ClientError);
    END;
    IF context.position # 0D THEN
      RETURN (ClientError);
    END;
  END;

  IF Known(ckSize) THEN
    IF ckSize < 0D THEN
      RETURN (ClientError);
    END;
    INC(minPSize, ckSize);
  END;

  IF (Known(context.bound)) AND (context.position + minPSize > context.bound) THEN
    RETURN (ClientError);
  END;

  WITH context DO
    ckHdr.ckID   := ckID;
    ckHdr.ckSize := ckSize;
    bytesSoFar   := 0D;
  END;

  IF 0D > GWrite(context.file, ADR(context.ckHdr), LONGINT(TSIZE(ChunkHeader))) THEN
    RETURN (DOSError);
  END;
  INC(context.position, LONGINT(TSIZE(ChunkHeader)) );
  RETURN (IFFOkay);
END PutCkHdr;

PROCEDURE IFFWriteBytes(VAR context: GroupContext; data: ADDRESS; 
                        nBytes: LONGINT): IFFP;
BEGIN
  IF (context.ckHdr.ckID = NullChunk) OR (* not in a chunk *)
     (nBytes < 0D) OR                    (* negative nBytes *)
     (Known(context.bound) AND           (* overflow context *)
      (context.position + nBytes > context.bound)) OR
     (Known(context.ckHdr.ckSize) AND    (* overflow chunk *)
      (context.bytesSoFar + nBytes > context.ckHdr.ckSize)) THEN
    RETURN (ClientError);
  END;
 
  IF 0D > GWrite(context.file, data, nBytes) THEN
    RETURN (DOSError);
  END;

  WITH context DO
    INC(bytesSoFar, nBytes);
    INC(position, nBytes);
  END;
  RETURN (IFFOkay);
END IFFWriteBytes;

PROCEDURE PutCkEnd(VAR context: GroupContext): IFFP;
VAR
  zero : INTEGER; (* padding source *)
BEGIN
  zero := 0;

  IF context.ckHdr.ckID = NullChunk THEN (* not in a chunk *)
    RETURN (ClientError);
  END;

  IF context.ckHdr.ckSize = szNotYetKnown THEN
    (* go back and set the chunk size to bytesSoFar *)
    IF (0D > GSeek(context.file,-(context.bytesSoFar+LONGINT(TSIZE(LONGINT))),OffsetCurrent))
      OR
       (0D > GWrite(context.file, ADR(context.bytesSoFar), LONGINT(TSIZE(LONGINT))))
      OR
       (0D > GSeek(context.file, context.bytesSoFar, OffsetCurrent)) THEN
      RETURN (DOSError);
    END;
  ELSE (* make sure the client wrote as many bytes as planned *)
    IF context.ckHdr.ckSize # context.bytesSoFar THEN
      RETURN (ClientError);
    END;
  END;

  (* Write a pad byte if needed to bring us up to an even boundary.
   * Since the context end must be even, and since we haven't
   * overwritten the context, if we're on an odd position there must
   * be room for a pad byte. *)
  IF ODD(context.bytesSoFar) THEN
    IF 0D > GWrite(context.file, ADR(zero), 1) THEN
      RETURN (DOSError);
    END;
    INC(context.position);
  END;

  WITH context DO
    bytesSoFar   := 0D;
    ckHdr.ckID   := NullChunk;
    ckHdr.ckSize := 0D;
  END;
  RETURN (IFFOkay);
END PutCkEnd;

END IFFW.
