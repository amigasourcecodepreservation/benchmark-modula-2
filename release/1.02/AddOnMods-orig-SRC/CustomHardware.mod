(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CustomHardware.MOD                Version: Amiga.00.00             *
 * Created: 11/29/86   Updated: 11/29/86   Author: Leon Frenkel             *
 * Description: Amiga Custom Hardware definitions.                          *
 ****************************************************************************)

IMPLEMENTATION MODULE CustomHardware;

(*$L+*)

BEGIN
  custom := CustomPtr(00DFF000H);
END CustomHardware.
