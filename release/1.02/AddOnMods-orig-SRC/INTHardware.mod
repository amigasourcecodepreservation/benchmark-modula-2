(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: INTHardware.MOD                   Version: Amiga.00.00             *
 * Created: 11/29/86   Updated: 11/30/86   Author: Leon Frenkel             *
 * Description: Amiga INT control definitions.                              *
 ****************************************************************************)

IMPLEMENTATION MODULE INTHardware;

FROM CustomHardware IMPORT custom, Custom;

(*$L+*)

PROCEDURE OnVBlank;
BEGIN
  custom^.intena := {INTSetClr, INTVertB};
END OnVBlank;

PROCEDURE OffVBlank;
BEGIN
  custom^.intena := {INTVertB};
END OffVBlank;

END INTHardware.
