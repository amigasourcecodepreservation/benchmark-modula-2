(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                      (c) 1986, 1987 by Leon Frenkel                      *
 ****************************************************************************
 * Name: SimpleConsoleCmds.DEF             Version: Amiga.00.00             *
 * Created: 04/06/87   Updated: 08/01/87   Author: Leon Frenkel             *
 * Description: Simplified commands for text i/o to Intuition windows.      *
 ****************************************************************************)

IMPLEMENTATION MODULE SimpleConsoleCmds;

FROM SYSTEM         IMPORT ADDRESS, BYTE, ADR;
FROM ConsoleDevUnit IMPORT ConUnitPtr, PMBASM, PMBAWM;
FROM Intuition      IMPORT Window, WindowPtr;
FROM SimpleConsole  IMPORT WindowExtPtr, PutBuf, PutCh;

(*$L+*)

CONST
  CSI = CHAR(09BH);

(* ========== PUBLIC  ========== *)

PROCEDURE ConvToStr(VAR bp: ADDRESS; val: CARDINAL);
VAR
  digit : CHAR;
BEGIN
  IF (val > 9) THEN
    ConvToStr(bp, val DIV 10);
  END;
  bp^ := CHR((val MOD 10) + CARDINAL('0'));
  INC(bp);
END ConvToStr;

PROCEDURE ConvFromStr(VAR bp: ADDRESS): CARDINAL;
VAR
  val : CARDINAL;
  ch  : CHAR;
BEGIN
  val := 0;
  LOOP
    ch := CHAR(bp^);
    IF (ch < "0") OR (ch > "9") THEN EXIT; END;
    val := val * 10;
    INC(val, CARDINAL(ch) - CARDINAL("0") );
    INC(bp);
  END;
  RETURN (val);
END ConvFromStr;

PROCEDURE wClrEndLine(VAR window: ARRAY OF BYTE (* Window *) );
VAR
  win : WindowPtr;
BEGIN
  win := ADR(window);
  PutBuf(win^, ADR("\x9B\x4B"), 2D);
END wClrEndLine;

PROCEDURE wClrEndScr(VAR window: ARRAY OF BYTE (* Window *) );
VAR
  win : WindowPtr;
BEGIN
  win := ADR(window);
  PutBuf(win^, ADR("\x9B\x4A"), 2D);
END wClrEndScr;

PROCEDURE wClrScr(VAR window: ARRAY OF BYTE (* Window *) );
VAR
  win : WindowPtr;
BEGIN
  win := ADR(window);
  PutCh(win^, "\x0C");
END wClrScr;

PROCEDURE wDoCmd(VAR window: ARRAY OF BYTE; (* Window *) 
                 cmd: CHAR; parm: CARDINAL);
VAR
  win : WindowPtr;
  bp  : ADDRESS;
  buf : ARRAY [0..31] OF CHAR;
BEGIN
  win := ADR(window);
  bp  := ADR(buf);
  bp^ := CSI; INC(bp);
  IF parm > 1 THEN
    ConvToStr(bp, parm);
  END;
  bp^ := cmd;
  PutBuf(win^, ADR(buf), bp - ADR(buf) + 1D);
END wDoCmd;

PROCEDURE wHome(VAR window: ARRAY OF BYTE (* Window *) );
VAR
  win : WindowPtr;
BEGIN
  win := ADR(window);
  wMove(win^, 1, 1);
END wHome;

PROCEDURE wMove(VAR window: ARRAY OF BYTE; (* Window *) x, y: CARDINAL);
VAR
  win : WindowPtr;
  bp  : ADDRESS;
  buf : ARRAY [0..31] OF CHAR;
BEGIN
  win := ADR(window);
  bp  := ADR(buf);
  bp^ := CSI; INC(bp);
  ConvToStr(bp, y);
  bp^ := ";"; INC(bp);
  ConvToStr(bp, x);
  bp^ := "\x48";
  PutBuf(win^, ADR(buf), bp - ADR(buf) + 1D);
END wMove;

PROCEDURE wPos(VAR window: ARRAY OF BYTE; (* Window *) VAR x, y: CARDINAL);
VAR
  win : WindowPtr;
  ext : WindowExtPtr;
  con : ConUnitPtr;
BEGIN
  win := ADR(window);
  ext := win^.UserData;
  con := ConUnitPtr(ext^.WriteMsg^.ioUnit);
  WITH con^ DO
    x := cuXCP + 1;
    y := cuYCP + 1;
  END;
END wPos;

PROCEDURE wReset(VAR window: ARRAY OF BYTE (* Window *) );
VAR
  win : WindowPtr;
BEGIN
  win := ADR(window);
  PutBuf(win^, ADR("\x1B\x63"), 2D);
END wReset;

PROCEDURE wSetColor(VAR window: ARRAY OF BYTE; (* Window *)
                    foreground, background: CARDINAL);
VAR
  win : WindowPtr;
  bp  : ADDRESS;
  buf : ARRAY [0..31] OF CHAR;
BEGIN
  win := ADR(window);
  bp  := ADR(buf);
  bp^ := CSI; INC(bp);
  bp^ := ";"; INC(bp);
  ConvToStr(bp, foreground + 30);
  bp^ := ";"; INC(bp);
  ConvToStr(bp, background + 40);
  bp^ := "\x6D";
  PutBuf(win^, ADR(buf), bp - ADR(buf) + 1D);
END wSetColor;

PROCEDURE wSetCursor(VAR window: ARRAY OF BYTE; (* Window *) visible: BOOLEAN);
VAR
  win : WindowPtr;
  bp  : ADDRESS;
  buf : ARRAY [0..31] OF CHAR;
BEGIN
  win := ADR(window);
  bp  := ADR(buf);
  bp^ := CSI; INC(bp);
  IF NOT visible THEN
    bp^ := "\x30"; INC(bp);
  END;
  bp^ := "\x20"; INC(bp);
  bp^ := "\x70";
  PutBuf(win^, ADR(buf), bp - ADR(buf) + 1D);
END wSetCursor;

PROCEDURE wSetModes(VAR window: ARRAY OF BYTE; (* Window *) 
                    wrap, scroll: BOOLEAN);
TYPE
  BYTESET = SET OF [0..7];
VAR
  win : WindowPtr;
  ext : WindowExtPtr;
  con : ConUnitPtr;
BEGIN
  win := ADR(window);
  ext := win^.UserData;
  con := ConUnitPtr(ext^.WriteMsg^.ioUnit);
  WITH con^ DO
    IF (wrap) THEN (* Auto Wrap On *)
      INCL(BYTESET(cuModes[PMBAWM DIV 8]), PMBAWM MOD 8);
    ELSE (* Auto Wrap Off *)
      EXCL(BYTESET(cuModes[PMBAWM DIV 8]), PMBAWM MOD 8);
    END;
    IF (scroll) THEN (* Auto Scroll On *)
      INCL(BYTESET(cuModes[PMBASM DIV 8]), PMBASM MOD 8);
    ELSE (* Auto Scroll Off *)
      EXCL(BYTESET(cuModes[PMBASM DIV 8]), PMBASM MOD 8);
    END;
  END;
END wSetModes;

PROCEDURE wSetStyle(VAR window: ARRAY OF BYTE; (* Window *) style: CARDINAL);
VAR
  win : WindowPtr;
  bp  : ADDRESS;
  buf : ARRAY [0..31] OF CHAR;
BEGIN
  win := ADR(window);
  bp  := ADR(buf);
  bp^ := CSI; INC(bp);
  ConvToStr(bp, style);
  bp^ := "\x6D";
  PutBuf(win^, ADR(buf), bp - ADR(buf) + 1D);
END wSetStyle;

PROCEDURE wStatus(VAR window: ARRAY OF BYTE; (* Window *)
                  VAR leftOfs, topOfs, lineLen, pageLen: CARDINAL);
VAR
  win : WindowPtr;
  ext : WindowExtPtr;
  con : ConUnitPtr;
BEGIN
  win := ADR(window);
  ext := win^.UserData;
  con := ConUnitPtr(ext^.WriteMsg^.ioUnit);
  WITH con^ DO
    leftOfs := cuXROrigin;
    topOfs  := cuYROrigin;
    lineLen := cuXMax + 1;
    pageLen := cuYMax + 1;
  END;
END wStatus;

END SimpleConsoleCmds.
