(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                      (c) 1986, 1987 by Leon Frenkel                      *
 ****************************************************************************
 * Name: SimpleScreens.MOD                 Version: Amiga.00.00             *
 * Created: 04/03/87   Updated: 07/22/87   Author: Leon Frenkel             *
 * Description: Simplified Intuition Screen functions.                      * 
 ****************************************************************************)

IMPLEMENTATION MODULE SimpleScreens;

FROM SYSTEM IMPORT
	ADDRESS, BYTE;
FROM GraphicsBase IMPORT
	GfxBasePtr;
FROM Intuition IMPORT
	Screen, ScreenPtr, NewScreen, CustomScreen, ScreenFlagsSet,
	OpenScreen, CloseScreen;
FROM Views IMPORT
	ViewModes, ViewModesSet;
FROM System IMPORT
	GfxBase;

PROCEDURE CreateScreen(width, height, depth: INTEGER; title: ADDRESS): ADDRESS;
VAR
  new : NewScreen;
  modes : ViewModesSet;
BEGIN
  modes := ViewModesSet{};

  IF width > (ScreenNormalColumns DIV 2) THEN
   INCL(modes, Hires);
  END;

  IF height > (ScreenNormalRows) THEN
   INCL(modes, Lace);
  END;

  IF (depth = 6) THEN
   INCL(modes, HAM);
  END;

  WITH new DO
    LeftEdge     := ScreenLeftEdge;
    TopEdge      := ScreenTopEdge;
    Width        := width;
    Height       := height;
    Depth        := depth;
    DetailPen    := ScreenDetailPen;
    BlockPen     := ScreenBlockPen;
    ViewModes    := modes;
    Type         := CustomScreen + ScreenFlagsSet(ScreenOptFlags);
    Font         := ScreenFont;
    DefaultTitle := title;
    Gadgets      := NIL;
    CustomBitMap := ScreenBitMap;
  END;
  RETURN OpenScreen(new);
END CreateScreen;

VAR
  gfx: GfxBasePtr;

BEGIN
  gfx := GfxBase;

  ScreenNormalColumns := gfx^.NormalDisplayColumns;
  ScreenNormalRows    := gfx^.NormalDisplayRows;

(*ScreenDetailPen := BYTE(0);*)
  ScreenBlockPen  := BYTE(1);

(*ScreenLeftEdge := 0; *)
(*ScreenTopEdge  := 0; *)

(*ScreenFont := NIL; *)

(*ScreenBitMap := NIL; *)

(*ScreenOptFlags := ScreenFlagsSet{}; *)

END SimpleScreens.
