(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                      (c) 1986, 1987 by Leon Frenkel                      *
 ****************************************************************************
 * Name: SimpleDBuf.MOD                 Version: Amiga.00.00                *
 * Created: 04/25/87   Updated: 12/25/87   Author: Leon Frenkel             *
 * Description: Functions for managing double buffered displays.            *
 *  The screen parameters are declared as ARRAY OF BYTE to avoid importing  *
 *  from Intuition. The declaration is compatible with VAR screen: Screen;  *
 ****************************************************************************)

IMPLEMENTATION MODULE SimpleDBuf;

FROM SYSTEM IMPORT
	ADDRESS, BYTE,
	ADR, TSIZE;
FROM Blit IMPORT
	BltClearFlagsSet,
	BltClear;
FROM Graphics IMPORT
	BitMap, BitMapPtr,
	InitBitMap;
FROM GraphicsBase IMPORT
	GfxBasePtr;
FROM Intuition IMPORT
	ScreenFlags, ScreenFlagsSet,
	CustomScreen,
	NewScreen,
	ScreenPtr,
	OpenScreen, CloseScreen,
	MakeScreen, RethinkDisplay;
FROM Memory IMPORT
	AllocMem, FreeMem, 
	MemReqSet, MemClear;
FROM Rasters IMPORT
	AllocRaster, FreeRaster;
FROM System IMPORT
	GfxBase;
FROM Views IMPORT
	ViewModes, ViewModesSet;

(*$L+*)


(* ======= PRIVATE ======= *)
PROCEDURE FreeScreenExt(ext: ScreenExtPtr);
VAR
  n : INTEGER;
  b : BOOLEAN;
BEGIN
  WITH ext^ DO
    FOR b := FALSE TO TRUE DO
      FOR n := 0 TO SDepth - 1 DO
        IF (Bufs[b].Planes[n] # NIL) THEN
          FreeRaster(Bufs[b].Planes[n], SWidth, SHeight);
        END; (* IF *)
      END; (* FOR *)
    END; (* FOR *)
  END; (* WITH *)
  FreeMem(ext, TSIZE(ScreenExt));
END FreeScreenExt;

(* ======= PUBLIC ====== *)

PROCEDURE CreateDBufScreen(width, height, depth: INTEGER): ADDRESS;
VAR
  SWidth  : INTEGER;
  SHeight : INTEGER;
  SDepth  : INTEGER;
  ns      : NewScreen;
  n       : INTEGER;
  MemOk   : BOOLEAN;
  modes   : ViewModesSet;
  csize   : LONGCARD;
  ext     : ScreenExtPtr;
  scr     : ScreenPtr;
BEGIN
  ext := AllocMem(TSIZE(ScreenExt), MemReqSet{MemClear});
  IF (ext = NIL) THEN
    RETURN (NIL); (* Failed! *)
  END;

  modes := ViewModesSet{};

  IF (width > ScreenNormalColumns DIV 2) THEN
   INCL(modes, Hires);
  END;

  IF (height > ScreenNormalRows) THEN
   INCL(modes, Lace);
  END;

  IF depth = 6 THEN
   INCL(modes, HAM);
  END;

  MemOk := TRUE;
  csize := (LONGCARD(width) * LONGCARD(height)) DIV 8D;

  WITH ext^ DO
    SWidth  := width;
    SHeight := height;
    SDepth  := depth;
    InitBitMap(Bufs[FALSE], depth, width, height);
    InitBitMap(Bufs[TRUE], depth, width, height);

    FOR n := 0 TO depth - 1 DO
      Bufs[FALSE].Planes[n] := AllocRaster(width, height);
      Bufs[TRUE].Planes[n]  := AllocRaster(width, height);
      IF (Bufs[FALSE].Planes[n] = NIL) OR (Bufs[TRUE].Planes[n] = NIL) THEN
        MemOk := FALSE;
      ELSE
        BltClear(Bufs[FALSE].Planes[n], csize, BltClearFlagsSet{});
        BltClear(Bufs[TRUE].Planes[n], csize, BltClearFlagsSet{});
      END;
    END;
  END; (* WITH *)

  IF (NOT MemOk) THEN
    FreeScreenExt(ext);
    RETURN (NIL); (* Failure! *)
  END;

  WITH ns DO
    LeftEdge     := ScreenLeftEdge;
    TopEdge      := ScreenTopEdge;
    Width        := width;
    Height       := height;
    Depth        := depth;
    DetailPen    := BYTE(0);
    BlockPen     := BYTE(1);
    ViewModes    := modes;
    Type         := ScreenFlagsSet{ScreenQuiet, CustomBitmap} + CustomScreen
                                  + ScreenFlagsSet(ScreenOptFlags);
    Font         := ScreenFont;
    DefaultTitle := NIL;
    Gadgets      := NIL;
    CustomBitMap := ADR(ext^.Bufs[TRUE]); (* Show BitMap 1 *)
  END;
  scr := OpenScreen(ns);
  IF (scr # NIL) THEN
    scr^.UserData := ext; (* Link extension to screen *)
    scr^.RastPort.BitMap := ADR(ext^.Bufs[FALSE]); (* Render BitMap 0 *)
  ELSE
    FreeScreenExt(ext);
  END;
  RETURN (scr);
END CreateDBufScreen;

PROCEDURE DeleteDBufScreen(VAR screen: ARRAY OF BYTE);
VAR
  scr : ScreenPtr;
BEGIN
  scr := ADR(screen);
  CloseScreen(scr^);
  FreeScreenExt(scr^.UserData);
END DeleteDBufScreen;

PROCEDURE FlipDBufScreen(VAR screen: ARRAY OF BYTE);
VAR
  scr : ScreenPtr;
  ext : ScreenExtPtr;
BEGIN
  scr := ADR(screen);
  ext := scr^.UserData;
  scr^.ViewPort.RasInfo^.BitMap := ADR(ext^.Bufs[ext^.Flip]);
  MakeScreen(scr^);
  RethinkDisplay();
  ext^.Flip := NOT ext^.Flip;
  scr^.RastPort.BitMap := ADR(ext^.Bufs[ext^.Flip]);
END FlipDBufScreen;

VAR
  gfx: GfxBasePtr;

BEGIN
  gfx := GfxBase;

  ScreenNormalColumns := gfx^.NormalDisplayColumns;
  ScreenNormalRows    := gfx^.NormalDisplayRows;

(*ScreenLeftEdge := 0; *)
(*ScreenTopEdge  := 0; *)

(*ScreenFont := NIL; *)

(*ScreenFlags := ScreenFlagsSet{}; *)
END SimpleDBuf.
