(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: ILBM.MOD                          Version: Amiga.00.00             *
 * Created: 04/20/87   Updated: 04/21/87   Author: Leon Frenkel             *
 * Description: Definitions for InterLeaved BitMap raster image.            *
 ****************************************************************************)

IMPLEMENTATION MODULE ILBM;

(*$L+*)

PROCEDURE RowBytes(w: CARDINAL): CARDINAL;
BEGIN
  RETURN ( (((w + 15) DIV 16) * 2) );
END RowBytes;

END ILBM.
