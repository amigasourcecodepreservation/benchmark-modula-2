(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: Conversions.MOD                   Version: Amiga.00.00             *
 * Created: 01/17/87   Updated: 08/07/87   Author: Leon Frenkel             *
 * Description: Functions for converting a number to a string and a string  *
 *  to a number.                                                            *
 ****************************************************************************)

IMPLEMENTATION MODULE Cxnversions;

FROM SYSTEM IMPORT LONGWORD;

(*$L+,D-*)

PROCEDURE ConvStringToNumber(string: ARRAY OF CHAR; VAR num: LONGWORD;
                             signed: BOOLEAN; base: CARDINAL): BOOLEAN;
VAR p     : INTEGER;
    digit : CARDINAL;
    ch    : CHAR;
    val   : LONGCARD;
    neg   : BOOLEAN;

  PROCEDURE NextToken(): BOOLEAN;
  (* Advance "p" to the next token.

     result - TRUE if valid token, FALSE if unexpected null-terminator found *)
  BEGIN
    WHILE string[p] # 0C DO
      IF string[p] # " " THEN RETURN TRUE; END; (* begining of token *)
      INC(p);
    END;
    RETURN FALSE; (* unexpected end of string *)
  END NextToken;

BEGIN
  p := 0;
  IF NOT NextToken() THEN RETURN FALSE; END;

  (* If a signed number expected then handle it *)
  IF signed THEN
     neg := FALSE;
     IF (string[p] = "-") OR (string[p] = "+") THEN
       neg := (string[p] = "-");
       INC(p);
       IF NOT NextToken() THEN RETURN FALSE; END;
     END;
  END;

  (* parse digits *)
  val := 0;
  WHILE (string[p] > " ") DO
    ch := CAP(string[p]);
    IF (ch >= "0") AND (ch <= "9") THEN digit := ORD(ch) - ORD("0");
    ELSIF (ch >= "A") AND (ch <= "Z") THEN digit := ORD(ch) - ORD("A") + 10;
    ELSE RETURN (FALSE); (* Invalid Char *)
    END;
    IF digit >= base THEN RETURN FALSE; END;
    val := (val * LONGCARD(base)) + LONGCARD(digit);
    INC(p)
  END;

  IF signed AND neg THEN val := -LONGINT(val); END;

  num := LONGWORD(val);
  RETURN TRUE;

END ConvStringToNumber;


PROCEDURE ConvNumberToString(VAR string: ARRAY OF CHAR; num: LONGWORD;
                             signed: BOOLEAN; base: CARDINAL;
                             digits: CARDINAL; padChar: CHAR);

VAR s     : ARRAY [0..79] OF CHAR;
    digit : CARDINAL;
    val   : LONGCARD;
    ps    : CARDINAL;
    pstr  : CARDINAL;
    i     : CARDINAL;
    dout  : CARDINAL;
BEGIN
  ps   := 0;
  pstr := 0;
  dout := 0;
  val  := LONGCARD(num);

  (* handle signed numbers *)
  IF signed THEN
    IF LONGINT(num) < 0D THEN 
      string[pstr] := "-";
      INC(pstr);
      val := -LONGINT(num)
    END;
  END;

  (* Convert number into char representation, if the number is 0 then just
   * one "0" digit will be output
   *)
  REPEAT
    digit := val MOD LONGCARD(base);
    IF digit < 10 THEN s[ps] := CHR(INTEGER(digit) + ORD("0"));
    ELSE s[ps] := CHR(INTEGER(digit) + ORD("A") - 10);
    END;
    INC(ps);
    val := val DIV LONGCARD(base);
  UNTIL (val = 0D);

  (* Output leading chars to get the specified field width *)
  IF digits > ps THEN
     i := digits - ps;
     WHILE (pstr < HIGH(string)) AND (i > 0) DO
       string[pstr] := padChar;
       INC(pstr); 
       DEC(i);
       INC(dout);
     END;
  END;

  (* copy significant digits to final output string *)
  DEC(ps);
  WHILE (INTEGER(ps) >= 0) AND (dout < digits) AND (pstr < HIGH(string)) DO
    string[pstr] := s[ps];
    INC(pstr);
    DEC(ps);
    INC(dout);
  END;
  string[pstr] := 0C; (* null terminate string *)

END ConvNumberToString;

END Cxnversions.
