(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CPrintTerminal.MOD                Version: Amiga.00.00             *
 * Created: 01/18/87   Updated: 02/23/87   Author: Leon Frenkel             *
 * Description: From the "C" language standard library, functions for       *
 *  doing formated output to the System.StdOutput.                          *
 ****************************************************************************)

IMPLEMENTATION MODULE CPrintTerminal;

FROM SYSTEM       IMPORT ADR, SETREG, INLINE, TSIZE;
FROM System       IMPORT DOSBase, StdOutput;
FROM FormatString IMPORT FormatArg, Format;

(*$L+,D-*)

TYPE 
  (* Output buffer type *)
  Buffer = ARRAY [0..127] OF CHAR;

VAR
  bufPtr : POINTER TO Buffer;
  bufIdx : INTEGER;

PROCEDURE WriteBuf;
CONST D1 = 1; D2 = 2; D3 = 3; A6 = 14; LVOWrite = 0FFD0H;
BEGIN
  SETREG(A6, DOSBase);
  SETREG(D3, LONGCARD(bufIdx));
  SETREG(D2, bufPtr);
  SETREG(D1, StdOutput);
  INLINE(4EAEH, LVOWrite); (* JSR LVOWrite(A6) *)
END WriteBuf;

PROCEDURE StoreCh(c: CHAR);
BEGIN
  bufPtr^[bufIdx] := c;
  INC(bufIdx);
  IF bufIdx = TSIZE(Buffer) THEN
    WriteBuf;
    bufIdx := 0;
  END;
END StoreCh;


PROCEDURE printf(formatStr: ARRAY OF CHAR; VAR args: ARRAY OF FormatArg);
VAR
  l      : INTEGER;
  OutBuf : Buffer;
BEGIN
  bufPtr := ADR(OutBuf);
  bufIdx := 0;
  l := Format(ADR(formatStr), args, StoreCh);
  WriteBuf;
END printf;

END CPrintTerminal.
