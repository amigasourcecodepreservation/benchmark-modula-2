(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: LargeSets.MOD                     Version: Amiga.00.00             *
 * Created: 06/29/87   Updated: 06/30/87   Author: Leon Frenkel             *
 * Description: Implements machine indipendant large sets (> machine word)  *
 *   The set range for LargeSets is [0..65535]                              *
 * WARNING: Parameters to set functions are not checked for validity.       *
 *   Set functions which take two sets as parameters require that both sets *
 *   have been created with the same range!                                 *
 ****************************************************************************)

IMPLEMENTATION MODULE LargeSets;

FROM SYSTEM IMPORT
	ADR, TSIZE,
	ADDRESS;
FROM Memory IMPORT
	MemReqSet, MemClear,
	AllocMem, FreeMem;

(*$L+*)

TYPE
  ByteSet = SET OF [0..7];

  LargeSet = POINTER TO LargeSetRecord;
  LargeSetRecord = RECORD
                     size         : CARDINAL; (* size of "array" *)
                     firstElement : CARDINAL; (* first element of set *)
                     lastElement  : CARDINAL; (* last element of set *)
                     array        : ARRAY [0..0] OF ByteSet; (* dynamic set *)
                   END;

CONST
  (* size of non static part of LastSetRecord *)
  LargeSetSIZE = CARDINAL(TSIZE(LargeSetRecord) - 2);


PROCEDURE CreateSet(VAR set: LargeSet; first, last: CARDINAL): BOOLEAN;
VAR
  setPtr : LargeSet;
  dynSize : CARDINAL;
BEGIN
  dynSize := ((last - first) DIV 8) + 1;
  setPtr := AllocMem(dynSize + LargeSetSIZE, MemReqSet{MemClear});
  IF (setPtr # NIL) THEN
    WITH setPtr^ DO
      size         := dynSize;
      firstElement := first;
      lastElement  := last;
    END;
    set := setPtr;
    RETURN (TRUE);
  ELSE (* Not Enough Memory! *)
    RETURN (FALSE);
  END;
END CreateSet;

PROCEDURE DeleteSet(set: LargeSet);
BEGIN
  FreeMem(set, set^.size + LargeSetSIZE);
END DeleteSet;

PROCEDURE SetInfo(set: LargeSet; VAR setAdr: ADDRESS; VAR setSize: CARDINAL);
BEGIN
  WITH set^ DO
    setAdr := ADR(array);
    setSize:= size;
  END;
END SetInfo;

PROCEDURE SetRange(set: LargeSet; VAR first, last: CARDINAL);
BEGIN
  WITH set^ DO
    first := firstElement;
    last  := lastElement;
  END;
END SetRange;

PROCEDURE In(set: LargeSet; element: CARDINAL): BOOLEAN;
BEGIN
  WITH set^ DO
    DEC(element, firstElement);
    RETURN ((element MOD 8) IN array[element DIV 8]);
  END;
END In;

PROCEDURE InclElement(set: LargeSet; element: CARDINAL);
BEGIN
  WITH set^ DO
    DEC(element, firstElement);
    INCL(array[element DIV 8], (element MOD 8));
  END;
END InclElement;

PROCEDURE ExclElement(set: LargeSet; element: CARDINAL);
BEGIN
  WITH set^ DO
    DEC(element, firstElement);
    EXCL(array[element DIV 8], (element MOD 8));
  END;
END ExclElement;

PROCEDURE InclRange(set: LargeSet; first, last: CARDINAL);
VAR
  element: CARDINAL;
BEGIN
  WITH set^ DO
    DEC(first, firstElement);
    DEC(last, firstElement);
    FOR element := first TO last DO
      INCL(array[element DIV 8], (element MOD 8));
    END;
  END;
END InclRange;

PROCEDURE ExclRange(set: LargeSet; first, last: CARDINAL);
VAR
  element: CARDINAL;
BEGIN
  WITH set^ DO
    DEC(first, firstElement);
    DEC(last, firstElement);
    FOR element := first TO last DO
      EXCL(array[element DIV 8], (element MOD 8));
    END;
  END;
END ExclRange;

PROCEDURE CopySet(dstSet, srcSet: LargeSet);
VAR
  i : CARDINAL;
BEGIN
  WITH dstSet^ DO
    i := size; (* Bytes to copy *)
    REPEAT
      DEC(i);
      array[i] := srcSet^.array[i];
    UNTIL (i = 0);
  END;
END CopySet;

PROCEDURE Union(dstSet, srcSet: LargeSet);
VAR
  i : CARDINAL;
BEGIN
  WITH dstSet^ DO
    i := size; (* Bytes to copy *)
    REPEAT
      DEC(i);
      array[i] := array[i] + srcSet^.array[i];
    UNTIL (i = 0);
  END;
END Union;

PROCEDURE Diff(dstSet, srcSet: LargeSet);
VAR
  i : CARDINAL;
BEGIN
  WITH dstSet^ DO
    i := size; (* Bytes to copy *)
    REPEAT
      DEC(i);
      array[i] := array[i] - srcSet^.array[i];
    UNTIL (i = 0);
  END;
END Diff;

PROCEDURE Intersection(dstSet, srcSet: LargeSet);
VAR
  i : CARDINAL;
BEGIN
  WITH dstSet^ DO
    i := size; (* Bytes to copy *)
    REPEAT
      DEC(i);
      array[i] := array[i] * srcSet^.array[i];
    UNTIL (i = 0);
  END;
END Intersection;

PROCEDURE SymmetricDiff(dstSet, srcSet: LargeSet);
VAR
  i : CARDINAL;
BEGIN
  WITH dstSet^ DO
    i := size; (* Bytes to copy *)
    REPEAT
      DEC(i);
      array[i] := array[i] / srcSet^.array[i];
    UNTIL (i = 0);
  END;
END SymmetricDiff;

END LargeSets.
