(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CFileIO.MOD                       Version: Amiga.00.00             *
 * Created: 02/12/87   Updated: 02/22/87   Author: Leon Frenkel             *
 * Description: From the "C" language standard library, functions for       *
 *  lo-level file io.                                                       *
 ****************************************************************************)

IMPLEMENTATION MODULE CFileIO;

FROM SYSTEM   IMPORT ADDRESS, ADR, TSIZE;
FROM AmigaDOS IMPORT Open, Close, DeleteFile, Lock, UnLock, Seek, IoErr, Rename,
                     Read, Write, ModeOldFile, ModeNewFile, FileHandle,
                     OffsetEnd, AccessWrite, AccessRead, ErrorObjectNotFound;
FROM Memory   IMPORT AllocMem, FreeMem, MemReqSet;

(*$L+*)


PROCEDURE close(file: FileDesc): BOOLEAN;
BEGIN
  IF (file^.mode * oNOCLOSE) = {} THEN
    Close(file^.handle);
  END;
  FreeMem(file, TSIZE(FileDescRecord));
  RETURN (TRUE); (* no error *)
END close;

PROCEDURE creat(name: ADDRESS; mode: INTEGER): FileDesc;
BEGIN
  RETURN open(name, oWRONLY + oTRUNC + oCREAT);
END creat;

PROCEDURE lseek(file: FileDesc; offset: LONGINT; mode: INTEGER): LONGINT;
BEGIN
  IF Seek(file^.handle, offset, LONGINT(mode)-1D) = -1D THEN
    errno := IoErr();
    RETURN(-1); (* error *)
  END;
  RETURN(Seek(file^.handle, 0D, 0D));
END lseek;

PROCEDURE open(name: ADDRESS; fmode: BITSET): FileDesc;
VAR
  fp   : FileDesc;
  file : FileHandle;
  r    : LONGINT;
BEGIN
  fp := AllocMem(TSIZE(FileDescRecord),MemReqSet{});
  IF fp = NIL THEN (* memory alloc failed *)
    errno := ENOMEM; 
    RETURN(NIL); (* open() failed *)
  END;
  LOOP
    IF (fmode * oTRUNC) # {} THEN
      file := Lock(name, AccessWrite);
      IF file # 0D THEN
        UnLock(file);
        IF NOT DeleteFile(name) THEN
          errno := IoErr();
          IF errno # ErrorObjectNotFound THEN
            EXIT; (* error *)
          END;
        END;
      END;
    END;
    file := Open(name, ModeOldFile);
    IF file = 0D THEN
      IF (fmode * oCREAT) = {} THEN
        errno := ENOENT;
        EXIT; (* error *)
      END;
      file := Open(name, ModeNewFile);
      IF file = 0D THEN
        errno := IoErr();
        EXIT; (* error *)
      END;
    ELSIF (fmode * (oCREAT + oEXCL)) = (oCREAT + oEXCL) THEN
      Close(file);
      errno := EEXIST;
      EXIT; (* error *)
    END;
    WITH fp^ DO
      handle := file;
      mode   := fmode;
    END;
    IF (fmode * oAPPEND) # {} THEN
      r := Seek(file, 0D, OffsetEnd);
    END;
    RETURN(fp);
  END; (* LOOP *)
  FreeMem(fp, TSIZE(FileDescRecord));
  RETURN(NIL); (* open() failed *)
END open;

PROCEDURE read(file: FileDesc; buffer: ADDRESS; length: LONGINT): LONGINT;
VAR
  res : LONGINT;
BEGIN
  IF (file^.mode * (oWRONLY + oRDWR)) = oWRONLY THEN
    errno := EINVAL;
    RETURN(-1); (* error *)
  END;
  res := Read(file^.handle, buffer, length);
  IF res = -1D THEN
    errno := IoErr();
    RETURN(-1); (* error *)
  END;
  RETURN (res); (* bytes read *)
END read;

PROCEDURE write(file: FileDesc; buffer: ADDRESS; length: LONGINT): LONGINT;
VAR
  res : LONGINT;
BEGIN
  IF (file^.mode * (oWRONLY + oRDWR)) = oRDONLY THEN
    errno := EINVAL;
    RETURN(-1); (* error *)
  END;
  res := Write(file^.handle, buffer, length);
  IF res = -1D THEN
    errno := IoErr();
    RETURN(-1); (* error *)
  END;
  RETURN (res); (* bytes written *)
END write;

END CFileIO.
