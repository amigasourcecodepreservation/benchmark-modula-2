(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: ReadPict.MOD                      Version: Amiga.00.00             *
 * Created: 04/22/87   Updated: 12/25/87   Author: Leon Frenkel             *
 * Description: Read an ILBM raster image file into RAM.                    *
 ****************************************************************************)

IMPLEMENTATION MODULE ReadPict;

FROM SYSTEM   IMPORT ADDRESS, BYTE, ADR;
FROM AmigaDOS IMPORT FileHandle;
FROM Graphics IMPORT BitMap, BitMapPtr, InitBitMap;
FROM IFF      IMPORT ClientFrame, IFFP, GroupContext,
		     IFFOkay, EndMark, BadFORM, IFFDone, ClientError;
FROM IFFR     IMPORT OpenRGroup, GetFChunkHdr, GetF1ChunkHdr, CloseRGroup,
		     GetPChunkHdr, ReadIList, ReadICat, ReadIFF;
FROM ILBM     IMPORT BitMapHeader, Color4, RowBytes,
		     IDILBM, IDBMHD, IDCMAP, IDBODY;
FROM ILBMR    IMPORT GetBMHD, GetCMAP, GetBODY;

(*$L+*)

(* This example's max number of planes in a bitmap. Could use MaxAmDepth. *)
CONST
  EXDepth = BYTE(5);
  maxColorReg = BYTE(32);

(*------------ ILBM reader -----------------------------------------------*)
(* ILBMFrame is our "client frame" for reading FORMs ILBM in an IFF file.
 * We allocate one of these on the stack for every LIST or FORM encountered
 * in the file and use it to hold BMHD & CMAP properties. We also allocate
 * an initial one for the whole file.
 * We allocate a new GroupContext (and initialize it by OpenRIFF or
 * OpenRGroup) for every group (FORM, CAT, LIST, or PROP) encountered. It's
 * just a context for reading (nested) chunks.
 *
 * If we were to scan the entire example file outlined below:
 *    reading          proc(s)                new               new
 *
 * --whole file--   ReadPicture+ReadIFF   GroupContext        ILBMFrame
 * CAT              ReadICat                GroupContext
 *   LIST           GetLiILBM+ReadIList       GroupContext        ILBMFrame
 *     PROP ILBM    GetPrILBM                   GroupContext
 *       CMAP       GetCMAP
 *       BMHD       GetBMHD
 *     FORM ILBM    GetFoILBM                   GroupContext        ILBMFrame
 *       BODY       GetBODY
 *     FORM ILBM    GetFoILBM                   GroupContext        ILBMFrame
 *       BODY       GetBODY
 *   FORM ILBM      GetFoILBM                 GroupContext        ILBMFrame
 *
 * NOTE: For a small version of this program, set Fancy to 0.
 * That'll compile a program that reads a single FORM ILBM in a file, which
 * is what DeluxePaint produces. It'll skip all LISTs and PROPs in the input
 * file. It will, however, look inside a CAT for a FORM ILBM.
 * That's suitable for 90% of the uses.
 *
 * For a fancier version that handles LISTs and PROPs, set Fancy to 1.
 * That'll compile a program that dives into a LIST, if present, to read
 * the first FORM ILBM. E.g. a DeluxePrint library of images is a LIST of
 * FORMs ILBM.
 *
 * For an even fancier version, set Fancy to 2. That'll compile a program
 * that dives into non-ILBM FORMs, if present, looking for a nested FORM ILBM.
 * E.g. a DeluxeVideo C.S. animated object file is a FORM ANBM containing a
 * FORM ILBM for each image frame. *)

CONST
  (* Define the size of a temporary buffer used in unscrambling the ILBM rows.*)
  bufSz = 512;

VAR
  (* Global access to client-provided pointers.*)
  gAllocator : Allocator;
  gBM        : BitMapPtr;		(* clien't bitmap *)
  giFrame    : ILBMFramePtr;		(* client frame *)

  bodyBuffer : ARRAY [0..bufSz-1] OF BYTE;


(* ======== PRIVATE ======= *)

(** GetFoILBM() *************************************************************
 *
 * Called via ReadPicture to handle every FORM encountered in an IFF file.
 * Reads FORMs ILBM and skips all others.
 * Inside a FORM ILBM, it stops once it reads a BODY. It complains if it
 * finds no BODY or if it has no BMHD to decode the BODY.
 *
 * Once we find a BODY chunk, we'll allocate the BitMap and read the image.
 *
 ****************************************************************************)
PROCEDURE GetFoILBM(VAR parent: GroupContext): IFFP;
VAR
  iffp        : IFFP;
  formContext : GroupContext;
  ilbmFrame   : ILBMFrame;	(* only used for non-clientFrame fields. *)
  i           : INTEGER;
  plsize      : LONGINT;	(* Plane size in bytes *)
  nPlanes     : BYTE;		(* number of planes in our display image *)
  r           : LONGINT;
  cf          : ILBMFramePtr;
BEGIN
  (* Handle a non-ILBM FORM. *)
  IF parent.subtype # IDILBM THEN
    (* Open a non-ILBM FORM and recursively scan it for ILBMs.*)
    iffp := OpenRGroup(parent, formContext);
    IF iffp # IFFOkay THEN (* CheckIFFP() *)
      RETURN (iffp);
    END;

    REPEAT
      iffp := GetF1ChunkHdr(formContext);
    UNTIL NOT (iffp >= IFFOkay);
    IF iffp = EndMark THEN
      iffp := IFFOkay;	(* then continue scanning the file *)
    END;
    r := CloseRGroup(formContext);
    RETURN (iffp);
  END;

  cf := ADDRESS(parent.clientFrame);
  ilbmFrame := cf^;
  iffp := OpenRGroup(parent, formContext);
  IF iffp # IFFOkay THEN (* CheckIFFP() *)
    RETURN (iffp);
  END;

  REPEAT
    iffp := GetFChunkHdr(formContext);
    IF iffp = IDBMHD THEN
      ilbmFrame.foundBMHD := TRUE;
      iffp := GetBMHD(formContext, ilbmFrame.bmHdr);
    ELSIF iffp = IDCMAP THEN
      ilbmFrame.nColorRegs := maxColorReg;
      iffp := GetCMAP(formContext,ADR(ilbmFrame.colorMap),ilbmFrame.nColorRegs);
    ELSIF iffp = IDBODY THEN
      IF NOT ilbmFrame.foundBMHD THEN
        RETURN (BadFORM);  (* No BMHD chunk! *)
      END;
      IF CARDINAL(ilbmFrame.bmHdr.nPlanes) < CARDINAL(EXDepth) THEN
        nPlanes := ilbmFrame.bmHdr.nPlanes;
      ELSE
        nPlanes := EXDepth;
      END;
      InitBitMap(gBM^, INTEGER(nPlanes), ilbmFrame.bmHdr.w, ilbmFrame.bmHdr.h);
      plsize := RowBytes(ilbmFrame.bmHdr.w) * ilbmFrame.bmHdr.h;
      (* Allocate all planes contiguously.  Not really necessary,
       * but it avoids writing code to back-out if only enough memory
       * for some of the planes.
       * WARNING: Don't change this without changing the code that
       * Frees these planes. *)
      gBM^.Planes[0] := gAllocator(LONGINT(nPlanes) * plsize);
      IF gBM^.Planes[0] # NIL THEN
        i := 1;
        WHILE i < INTEGER(nPlanes) DO
          WITH gBM^ DO
            Planes[i] := Planes[0] + LONGCARD(plsize) * LONGCARD(i);
          END;
          INC(i);
        END;

        iffp := GetBODY(formContext,gBM^,NIL,ilbmFrame.bmHdr,ADR(bodyBuffer),
                          bufSz);
          IF iffp = IFFOkay THEN
            iffp := IFFDone; (* Eureka *)
          END;
          giFrame^ := ilbmFrame; (* Copy fields to client's frame. *)

      ELSE
        iffp := ClientError;  (* not enought RAM for the bitmap *)
      END;
    ELSIF iffp = EndMark THEN
      iffp := BadFORM; (* No BODY chunk! *)
    END;
  UNTIL NOT (iffp >= IFFOkay);  (* loop if valid ID of ignored chunk or a
			         * subroutine returned IFF_OKAY (no errors).*)
  IF iffp # IFFDone THEN
    RETURN (iffp);
  END;

  (* If we get this far, there were no errors. *)
  r := CloseRGroup(formContext);
  RETURN (iffp);
END GetFoILBM;

(** Notes on extending GetFoILBM ********************************************
 *
 * To read more kinds of chunks, just add clauses to the switch statement.
 * To read more kinds of property chunks (GRAB, CAMG, etc.) add clauses to
 * the switch statement in GetPrILBM, too.
 *
 * To read a FORM type that contains a variable number of data chunks--e.g.
 * a FORM FTXT with any number of CHRS chunks--replace the ID_BODY case with
 * an ID_CHRS case that doesn't set iffp = IFF_DONE, and make the END_MARK
 * case do whatever cleanup you need.
 *
 ****************************************************************************)

(** GetPrILBM() *************************************************************
 *
 * Called via ReadPicture to handle every PROP encountered in an IFF file.
 * Reads PROPs ILBM and skips all others.
 *
 ****************************************************************************)
PROCEDURE GetPrILBM(VAR parent: GroupContext): IFFP;
VAR
  iffp : IFFP;
  propContext : GroupContext;
  ilbmFrame : ILBMFramePtr;
  r : LONGINT;
BEGIN
  ilbmFrame := ADDRESS(parent.clientFrame);

  IF parent.subtype # IDILBM THEN
    RETURN (IFFOkay);  (* just continue scaning the file *)
  END;

  iffp := OpenRGroup(parent, propContext);
  IF iffp # IFFOkay THEN (* CheckIFFP() *)
    RETURN (iffp);
  END;

  REPEAT
    iffp := GetPChunkHdr(propContext);
    IF iffp = IDBMHD THEN
      ilbmFrame^.foundBMHD := TRUE;
      iffp := GetBMHD(propContext, ilbmFrame^.bmHdr);
    ELSIF iffp = IDCMAP THEN
      ilbmFrame^.nColorRegs := maxColorReg; (* we have room for this many *)
      iffp := GetCMAP(propContext, ADR(ilbmFrame^.colorMap),
                      ilbmFrame^.nColorRegs);
    END;
  UNTIL NOT (iffp >= IFFOkay);  (* loop if valid ID of ignored chunk or a
			  * subroutine returned IFF_OKAY (no errors).*)
  r := CloseRGroup(propContext);
  IF iffp = EndMark THEN
    RETURN (IFFOkay);
  ELSE
    RETURN (iffp);
  END;
END GetPrILBM;

(** GetLiILBM() *************************************************************
 *
 * Called via ReadPicture to handle every LIST encountered in an IFF file.
 *
 ****************************************************************************)
PROCEDURE GetLiILBM(VAR parent: GroupContext): IFFP;
VAR
  newFrame : ILBMFrame; (* alloccate a new Frame *)
  fp : ILBMFramePtr;
BEGIN
  fp := ADDRESS(parent.clientFrame);
  newFrame := fp^; (* copy parent frame *)
  RETURN ReadIList(parent, ADR(newFrame));
END GetLiILBM;


(* ======== PUBLIC ======= *)

PROCEDURE ReadPicture(file: FileHandle;  VAR bm: BitMap; 
                      VAR iFrame: ILBMFrame;  allocator: Allocator): IFFP;
VAR
  iffp : IFFP;
BEGIN
  iffp := IFFOkay;

  (* Initialize the top-level client frame's property settings to the
   * program-wide defaults. This example just records that we haven't read
   * any BMHD property or CMAP color registers yet. For the color map, that
   * means the default is to leave the machine's color registers alone.
   * If you want to read a property like GRAB, init it here to (0, 0). *)
  WITH iFrame DO
    WITH clientFrame DO
      getList := GetLiILBM;
      getProp := GetPrILBM;
      getForm := GetFoILBM;
      getCat  := ReadICat ;
    END;
    foundBMHD := FALSE;
    nColorRegs:= BYTE(0);
  END;

  gAllocator := allocator;
  gBM := ADR(bm);
  giFrame := ADR(iFrame);
  (* Store a pointer to the client's frame in a global variable so that
   * GetFoILBM can update client's frame when done.  Why do we have so
   * many frames & frame pointers floating around causing confusion?
   * Because IFF supports PROPs which apply to all FORMs in a LIST,
   * unless a given FORM overrides some property.  
   * When you write code to read several FORMs,
   * it is ssential to maintain a frame at each level of the syntax
   * so that the properties for the LIST don't get overwritten by any
   * properties specified by individual FORMs.
   * We decided it was best to put that complexity into this one-FORM example,
   * so that those who need it later will have a useful starting place. *)

   iffp := ReadIFF(file, ADR(iFrame));
   RETURN (iffp);
END ReadPicture;

END ReadPict.
