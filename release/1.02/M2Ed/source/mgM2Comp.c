/****************************************************************************
 *                    MODULA-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: mgM2Comp.c                        Version: Amiga.00.09             *
 * Created: 12/11/86   Updated: 12/26/87   Author: Leon Frenkel             *
 * Description: MicroGnuEmacs functions, to interface to the Modula-2 Comp. *
 * This module is designed to be compiled under MANX-C with 16-bit integers!*
 ****************************************************************************/

/* Change history:
 * 00.00 Original version
 * 00.01 Speedup of err file reader
 * 00.02 Support for run commands
 * 00.03 New command to change and/or examine current directory
 * 00.04 Auto load compiler
 * 00.05 Change compiler options while compiler loaded
 *       The second hunk of the compiler must contain the compiler options
 * 00.06 Display sizes of code, data, etc.. after successful compilation
 * 00.07 Search for Modula-2 Compiler using AmigaDOS PATH
 * 00.08 Bug fix: changedir did not handle a volume no longer in drive correctly
 * 00.09 Turn cursor off while compiling
 */

/* Include Files */
#include <exec/types.h>
#include <libraries/dos.h>
#include <fcntl.h>
#undef TRUE
#undef FALSE
#include "def.h"

extern	int	exactfnamemode();	/* Exact FileName Mode Toggle	*/

#ifdef M2LINK
extern	int	m2linkmain();		/* Link main module		*/
extern	int	m2linkmodule();		/* Link any module		*/
extern	int	setm2linkoutdir();	/* Set M2 output dirs		*/
extern	int	setm2linkgensym();	/* Set M2 linker generate syms  */
extern	int	setm2mainmodule();	/* Set M2 main module		*/
extern	int	setm2objdir();		/* Set list of dir .OBM files 	*/

extern	int	setm2runstacksize();	/* Set prog stack size		*/
extern	int	setm2runwindow();	/* Set prog window spec		*/
extern	int	setm2runargs();		/* Set prog parameters		*/
extern	int	m2runmain();		/* Run main program		*/
extern	int	m2runmodule();		/* Run specified program	*/
#endif

#ifdef	FILEREQ
extern	int	filereqmode();		/* Toggle file requester mode 	*/
extern	int	Do_File_Requester();	/* Display file requester... */
extern	void	Clear_FileReq_Dir();	/* Clear directory name */
extern	char	File_Req_Flag;		/* TRUE = requester, FALSE = kbd */
#endif

extern	long	atol();
extern 	char 	*malloc();
extern	APTR	AllocMem();
extern	VOID	FreeMem();
extern	LONG	AvailMem();
extern	LONG	LoadSeg();
extern	VOID	UnLoadSeg();
extern	LONG	IoErr();
extern	LONG	Init_Compiler();
extern	LONG	Do_Compiler_Command();
extern	VOID	SetWindowTitles();
extern	long	Lock();
extern	long	CurrentDir();
extern	long	Examine();

extern	int	ttrow, ttcol;		/* tty.c module */


/* Memory mapping of compilers internal variables */
/* This structure must be the same as defined in the compiler file M2.MOD */
struct Compiler_Vars
{
	ULONG		CompOptHeapSize;
	ULONG		CompOptCodeBufSize;
	ULONG		CompOptConstBufSize;
	ULONG		CompOptIdentBufSize;
	ULONG		CompOptAdrRelocBufSize;
	ULONG		CompOptDiskBufSize;
	UBYTE		CompOptOverflowChk;
	UBYTE		CompOptRangeChk;
	UBYTE		CompOptClrModuleKey;
	UBYTE		CompOptGenRefFile;
	UBYTE		CompOptGlobalVarAbsAdr;
	UBYTE		CompOptInitialized;

	UWORD		CompVarOutFileType;		/* 0 = .SBM, 1 = .OBM */
	ULONG		CompVarCodeSize;
	ULONG		CompVarUDataSize;
	ULONG		CompVarIDataSize;
	ULONG		CompVarTotalSize;
};

#define COMinHeapSize        	10000L
#define COMaxHeapSize        	1000000L
#define COMinCodeBufSize     	1000L
#define COMaxCodeBufSize     	32766L
#define COMinConstBufSize    	1000L
#define COMaxConstBufSize    	32766L
#define COMinIdentBufSize    	1000L
#define COMaxIdentBufSize    	32766L
#define COMinDiskBufSize    	512L
#define COMaxDiskBufSize   	1000000L
#define COMinAdrRelocBufSize	100L
#define COMaxAdrRelocBufSize	32766L

/* Error structure */
struct ErrorEntry
{
  UWORD      code;  /* Error Code */
  char      *text;  /* Text associated with the Error Code */
};
extern	struct ErrorEntry	ErrorTable[]; 	/* Lookup table of err msgs */

extern	char			*version;	/* Default Window Title */
extern	APTR			EmW;		/* Emacs Window */

/* ERR.DAT file record tag */
#define ERR_FILE_TAG_FNM	149
#define ERR_FILE_TAG_ERR	249

/* m2compload( .. , n, .. ) n = MAGIC */
#define M2COMPLOAD_MAGIC	-23932

#define		COMP_ARG_STRING_LENGTH	255	/* Max length of arg string */
#define		DEFAULT_COMP_STACK_SIZE 10000	/* Default Compiler Stack Size*/

#define		COMP_STATE_UNINIT	0L	/* Compiler is not loaded */
#define		COMP_STATE_INIT		1L	/* Compiler is loaded	  */

#define		ERROR_STATE_UNINIT	0L	/* Error Processor inactive */
#define		ERROR_STATE_INIT	1L	/* Error Processor active   */

/* Result returned when compiler first initialized */
#define		ERR_COMP_INIT_OK	0L	/* Compiler initialized ok */
#define		ERR_COMP_INIT_BAD_ARGS	1L	/* Invalid Arg String	   */
#define		ERR_COMP_INIT_NO_MEMORY	2L	/* Ran out of memory	   */

/* Result returned when compiler asked to compile a program */
#define		ERR_COMP_PROG_NO_ERRORS		0L /* No Errors encountered */
#define		ERR_COMP_PROG_ERRORS		1L /* Errors encountered    */
#define		ERR_COMP_PROG_FILE_NOT_FOUND	2L /* Source File Not Found */
#define		ERR_COMP_NO_MEMORY		3L /* Ran out of memory     */
#define		ERR_COMP_ERR_FILE_NOT_OPENED	4L /* "err.DAT" not opened  */
#define		ERR_COMP_OUTPUT_FILE		5L /* Error outputing file  */

/* Command issued to compiler */
#define		EMACS_COMP_CMD_COMP_PROG	0L /* Compile a Program      */
#define		EMACS_COMP_CMD_PREP_UNLOAD	1L /* Prepare to be unloaded */

/* Error Node Structure, used to represent errors in a buffer */
struct ERROR
{
  struct ERROR 	*next;		/* Next Error Struc 	*/
  UWORD 	errcode; 	/* Error code     	*/
  union
  {
    struct
    {
      LINE	*uline;		/* ptr to line of error */
      UWORD	ucoffset;	/* char offset into line */
    } pt;
    ULONG 	upos; 		/* char from begining of file */
  } upt;
};
/* Use these names to access field in union */
#define		line	upt.pt.uline
#define		coffset	upt.pt.ucoffset
#define		pos 	upt.upos


/* Compiler variables */
static	UBYTE	Comp_State = COMP_STATE_UNINIT; /* Compiler State             */
static	ULONG	Comp_Stack_Size = DEFAULT_COMP_STACK_SIZE; /* Comp Stack Size */
static	APTR	Comp_Stack_Base;             /* Compiler Stack Base           */
static	APTR	Comp_Stack_Ptr;              /* Compiler Stack Top            */
static	ULONG	Comp_Segment_List;           /* Compiler Seg List             */
static	LINE	*Comp_Buffer_First_Line;     /* First line of buffer          */
static	LINE	*Comp_Buffer_Line;	     /* Current line in buffer        */
static	short	Comp_Buffer_Char;	     /* Current char in line          */
static	struct	Compiler_Vars *Comp_Var_Map;	/* Map of compiler variables  */

/* Error Processing variables */
static	UBYTE	Error_State = ERROR_STATE_UNINIT; /* Error Processor State    */
static	UBYTE	Error_Buffer_or_File_Flag; 	  /* TRUE = buf, FALSE = file */
static	UBYTE	Error_List_Type_Flag;		  /* T = line,ofs; F = cpos   */
static	UWORD	Error_Read_Limit = 20;		  /* Max # to read (0=all)    */
static	struct ERROR	*Error_List_Head; 	  /* Head of Error List       */
static	struct ERROR	*Error_List_Tail; 	  /* Tail of Error List       */
static	struct ERROR	*Current_Error;		  /* Ptr to the current error */
static	char	*Error_Buffer_or_File_Name = NULL;/* Errors in buffer or file */
static	char	Error_Msg[80];			  /* Error Msg in Title Bar   */

/* Display line number being compiled */
static	UWORD	Current_Line;			  /* Line #		   */
static	int	cl_row;				  /* Display row           */
static	int	cl_col;				  /* Display column        */
static	UWORD	Display_Mask;			  /* Mask to test display  */

/* FileNames refrenced in this file */
static	char	Comp_FileName[]   = "M2";	  /* FileName for Compiler   */
static	char	ErrDat_FileName[] = "T:M2.err";	  /* Name of err data file   */

/* Current Directory Command variables */
static	char	CD_Original_Flag;		/* TRUE = change to original */
static	long	CD_Original_Lock;		/* Original CurrentDir Lock  */

/* Variable Initialize To point to data segment, reg A4 */
#asm
	public	DataSegPtr
DataSegPtr	dc.L	0
#endasm

/****************************************************************************
 * m2compload -- load compiler and pass it an initialization string. The
 * initialization string always contains "-E" as the first argument to tell
 * the compiler its running as part of emacs.
 ****************************************************************************/
m2compload(f, n, k)
{ register int   s;
  register LONG  i, err;
  register APTR  comp_init_entry_pt;
  char  buf[COMP_ARG_STRING_LENGTH];
  char  comp_exec[NFILEN];	/* Compiler Executable FileName */
  char GetCharFromBuffer();	/* Defined later in this file */

  /* Abort if compiler already loaded */
  if (Comp_State == COMP_STATE_INIT)
  {
    ewprintf("ERROR: Modula-2 Compiler Already Loaded!");
    return(FALSE);
  }

  /* Initialize running under emacs compiler switch: "-E" */
  buf[0] = '-';
  buf[1] = 'E';
  buf[2] = ' ';
  buf[3] =   0;

  /* Skip args if auto loading */
  if (n != M2COMPLOAD_MAGIC)
  {
    /* Prompt user to enter additination arguments to pass to compiler */
    if ((s = ereply("Args: ", &(buf[3]), sizeof(buf)-3)) == ABORT)
    {
       return(s);
    }
  }
  
  /* Allocate memory for a stack to be used by the compiler */
  Comp_Stack_Base = AllocMem(Comp_Stack_Size, NULL);
  if (Comp_Stack_Base == NULL)
  {
    ewprintf("ERROR: Insufficient Memory to Allocate Stack for the Modula-2 Compiler!");
    return(FALSE);
  }
  Comp_Stack_Ptr = (APTR)((ULONG)Comp_Stack_Base + (ULONG)Comp_Stack_Size);

  /* Load Compiler from disk, searches the AmigaDOS PATH list for the compiler
   * program. 
   */
  ewprintf("Loading Modula-2 Compiler...");
  SearchPathList(&comp_exec[0], Comp_FileName);

  Comp_Segment_List = LoadSeg(comp_exec);

  if (Comp_Segment_List == NULL)
  {
    err = IoErr();
    if (err == ERROR_OBJECT_NOT_FOUND)
    {
       ewprintf("ERROR: Modula-2 Compiler Not Found!");
    }
    if (err == ERROR_NO_FREE_STORE)
    {
      ewprintf("ERROR: Insufficient Memory To Load the Modula-2 Compiler!");
    }
    else
    {
      ewprintf("ERROR: Modula-2 Compiler Loading Problem!");
    }
    FreeMem(Comp_Stack_Base, Comp_Stack_Size);
    return(FALSE);
  }

  /* convert BCPL segment ptr into adr of first instruction of first hunk */
  comp_init_entry_pt = (APTR)((Comp_Segment_List * 4) + 4);

  /* Initialize Compiler For Running Under Emacs */
  i = Init_Compiler(comp_init_entry_pt,
                    &GetCharFromBuffer,
                    Comp_Stack_Ptr,
                    &buf,
                    (LONG)strlen(buf) + 1L);
  if (i != ERR_COMP_INIT_OK)
  {
    if (i == ERR_COMP_INIT_BAD_ARGS)
    {
      ewprintf("ERROR: Invalid Arg String Passed To Modula-2 Compiler!");
    }
    if (i == ERR_COMP_INIT_NO_MEMORY)
    {
      ewprintf("ERROR: Insufficient Memory To Initialize the Modula-2 Compiler!");
    }
    Util_UnLoad_Compiler();
    return(FALSE);
  }

  /* Calculate ptr to second segment in segment list which is the first module
   * of the compiler and contains compiler configuration
   */
  Comp_Var_Map = (struct Compiler_Vars*)
                     (((*((long*)(Comp_Segment_List << 2))) << 2) + 4);

  ewprintf("Modula-2 Compiler Loaded.");
  Comp_State = COMP_STATE_INIT;
  return(TRUE);
}

/****************************************************************************
 * m2compunload - unload compiler from memory.
 ****************************************************************************/
m2compunload(f, n, k)
{
  if (Comp_State != COMP_STATE_INIT)
  {
    return(FALSE);
  }

  /* Tell compiler to prepare to be unloaded from memory */
  Do_Compiler_Command(EMACS_COMP_CMD_PREP_UNLOAD, NULL);

  Util_UnLoad_Compiler();

  ewprintf("Modula-2 Compiler UnLoaded.");
  Comp_State = COMP_STATE_UNINIT;
  return(TRUE);
}

/****************************************************************************
 * m2compcurrentbuffer - Compile file associated with current buffer.
 ****************************************************************************/
m2compcurrentbuffer(f, n, k)
{
  if (!Util_Test_Compiler_State())
  {
    return(FALSE);
  }

  return(Util_Compile_Buffer(curbp));
}

/***************************************************************************
 * m2compbuffer - Compile file associate with a specified buffer
 ***************************************************************************/
m2compbuffer(f, n, k)
{ register struct BUFFER  *bp;
  register int            s;
  char                    buf[NBUFN];

  if (!Util_Test_Compiler_State())
  {
    return(FALSE);
  }

  if ((s = eread("Compile Buffer: ", buf, sizeof(buf), EFNEW|EFBUF)) != TRUE)
  {
    return(s);
  }

  bp = (struct BUFFER*)bfind(buf, FALSE);
  if (bp == NULL)
  {
    ewprintf("ERROR: Buffer Not Found!");
    return(FALSE);
  }

  return(Util_Compile_Buffer(bp));
}

/***************************************************************************
 * m2compfile - compile a file of a specified name.
 ***************************************************************************/
m2compfile(f, n, k)
{ char            buf[NFILEN];
  register int    s;

  if (!Util_Test_Compiler_State())
  {
    return(FALSE);
  }

#ifdef FILEREQ
  if (File_Req_Flag)
  {
    if(!Do_File_Requester("Compile File", buf, sizeof(buf)))
      return(FALSE);
  }
  else
  {
#endif
    if ((s = ereply("Compile File: ", buf, sizeof(buf))) != TRUE)
    {
      return(s);
    }
#ifdef	FILEREQ
  }
#endif

  /* Convert Relative Path To -> Absolute Path */
  if (!GetAbsFilePath(buf))
  {
    return (FALSE);
  }
  return(Util_Compile_File(buf));
}

/***************************************************************************
 * setm2compstacksize - Set the amount of stack space to be allocated for the
 *  use of the compiler. Only works when the compiler is not loaded.
 ***************************************************************************/
setm2compstacksize(f, n, k)
{ register int  s;
  char          buf[16];

  if (Comp_State == COMP_STATE_INIT)
  {
    ewprintf("ERROR: Stack Size Can Not Be Changed Now!");
    return(FALSE);
  }

  if ((s=ereply("Compiler Stack Size: (default %ld) ",buf, sizeof(buf),
      (long)Comp_Stack_Size, (long)Comp_Stack_Size))==ABORT)
  { /* User Aborted Prompt */
    return(s);
  }
  if (s == FALSE)
  { /* User hit <RETURN> at prompt so keep the current stack size value */
    return(TRUE);
  }
  Comp_Stack_Size = atol(buf);
  return(TRUE);
}

/***************************************************************************
 * m2errcurrenterror - show current error position and error message
 ***************************************************************************/
m2errcurrenterror(f, n, k)
{
  Util_Show_Error(FALSE);
  return(TRUE);
}

/***************************************************************************
 * m2errnexterror - advance to next error and show its position and the
 *  error message
 ***************************************************************************/
m2errnexterror(f, n, k)
{
  Util_Show_Error(TRUE);
  return(TRUE);
}

/***************************************************************************
 * setm2errcount - specify the max number of errors to be read in after
 *  compilation.
 ***************************************************************************/
setm2errcount(f, n, k)
{ register int s;
  char         buf[16];

  if ((s=ereply("Max Error Count: (0 = ALL) (default %d) ",buf, sizeof(buf),
     Error_Read_Limit)) == ABORT)
  {
    return(s);
  }
  if (s == FALSE)
  {
    return(TRUE);
  }
  Error_Read_Limit = atoi(buf);
  return(TRUE);
}

/***************************************************************************
 * freememoryavailable - display the amount of memory available in system.
 *  usefull to check if enough memory to run compiler.
 ***************************************************************************/
freememoryavailable(f, n, k)
{
  ewprintf("Free Memory Available = %ld", AvailMem(NULL));
  return(TRUE);
}

/***************************************************************************
 * currentdir - show current directory and allow user to change to a new
 *   directory.
 ***************************************************************************/
currentdir(f, n, k)
{ register int  s;
  register long newlock;
  register long	fh;
  register struct FileInfoBlock  *fileinfo;
  char	dirType;
  char  buf[50];	/* input buffer */
  char	cpath[256];	/* current path */
  register char *show;	/* ptr to string to show */
  static  char err_CD_Msg[]  = "ERROR: Invalid Directory Path!";
  static  char err_Vol_Msg[] = "[VOLUME NOT MOUNTED]";

  if (GetAbsPath("", &cpath))
  { show = cpath; }
  else
  { show = err_Vol_Msg; }

  if ((s=ereply("Current Dir: (default %s) ",buf,sizeof(buf),show)) == ABORT)
  {
     return(s);
  }

  if (s == TRUE)
  {
    /* Put a Lock On the directory */
    newlock = Lock(buf, SHARED_LOCK);
    if (newlock == NULL)
    {
      ewprintf(err_CD_Msg);
      return (FALSE);
    }

    /* Allocate a file info block */
    fileinfo = (struct FileInfoBlock*)AllocMem((long)sizeof(struct FileInfoBlock), 0L);
    if (fileinfo == NULL)
    {
      UnLock(newlock);
      return (FALSE);
    }

    /* Load file info block */
    if (Examine(newlock, fileinfo) == 0L)
    {
      UnLock(newlock);
      FreeMem(fileinfo, (long)sizeof(struct FileInfoBlock));
      return (FALSE);
    }
    /* if fib_DirEntryType > 0 then a directory otherwise a file */
    dirType = fileinfo->fib_DirEntryType > 0L;

    /* free info block */
    FreeMem(fileinfo, (long)sizeof(struct FileInfoBlock));

    /* If not a directory then exit */
    if (!dirType)
    {
      UnLock(newlock);
      ewprintf(err_CD_Msg);
      return (FALSE);
    }

    if (CD_Original_Flag)
    {
       UnLock( CurrentDir(newlock) );
    }
    else
    {
       CD_Original_Lock = CurrentDir(newlock);
       CD_Original_Flag = TRUE;
    }

    if (GetAbsPath("", &cpath))
    { show = cpath; }
    else
    { show = err_Vol_Msg; }
    ewprintf("New Current Directory: %s", show);

#ifdef	FILEREQ
    /* clear the directory path for the file requester */
    Clear_FileReq_Dir();
#endif
  }
  return (TRUE);
}

/***************************************************************************
 * setm2compopt - change compiler options while compiler loaded
 ***************************************************************************/
setm2compopt(f, n, k)
{ register struct Compiler_Vars *map;

  if (!Util_Test_Compiler_State())
  {
    return(FALSE);
  }

  map = Comp_Var_Map;

/* The following code prompts for each of the compiler options, if the user
 * hits <Return> or enters a new value the next prompt will be shown.
 * If the user hits (CTRL-G) the command will be aborted
 */
if (
  Util_Prompt_Opt_Val(&map->CompOptHeapSize,COMinHeapSize,COMaxHeapSize,
                      "Heap Size")
&&
  Util_Prompt_Opt_Val(&map->CompOptCodeBufSize,COMinCodeBufSize,COMaxCodeBufSize,
                      "Code Buffer Size")
&&
  Util_Prompt_Opt_Val(&map->CompOptConstBufSize,COMinConstBufSize,COMaxConstBufSize,
                      "Const Buffer Size")
&&
  Util_Prompt_Opt_Val(&map->CompOptIdentBufSize,COMinIdentBufSize,COMaxIdentBufSize,
                      "Identifier Buffer Size")
&&
  Util_Prompt_Opt_Val(&map->CompOptDiskBufSize,COMinDiskBufSize,COMaxDiskBufSize,
                      "Disk Buffer Size")
&&
  Util_Prompt_Opt_Val(&map->CompOptAdrRelocBufSize,COMinAdrRelocBufSize,COMaxAdrRelocBufSize,
                      "Address Relocation Buffer Size")
&&
  Util_Prompt_Opt_Bool(&map->CompOptRangeChk, 
                       "Range Checking")
&&
  Util_Prompt_Opt_Bool(&map->CompOptOverflowChk, 
                       "Overflow Checking")
&&
  Util_Prompt_Opt_Bool(&map->CompOptGlobalVarAbsAdr,
                       "Global Variables Absolute Addressing")
&&
  Util_Prompt_Opt_Bool(&map->CompOptGenRefFile,
                       "Generate Reference File")
&&
  Util_Prompt_Opt_Bool(&map->CompOptClrModuleKey,
                       "Clear Module Key")
)
{ /* All prompts finished */
  return (TRUE);
}

/* User Aborted From One Of The Prompts */
  return (ABORT);
}

/**************************** UTILITY FUNCTIONS ****************************/

/***************************************************************************
 * Util_Test_Compiler_State() - returns TRUE if compiler loaded, FALSE if
 *  compiler not loaded. Attempts to load compiler if possible.
 ***************************************************************************/
static Util_Test_Compiler_State()
{
  if (Comp_State != COMP_STATE_INIT)
  {
    return ( m2compload(FALSE, M2COMPLOAD_MAGIC, KRANDOM) );
  }
  return(TRUE);
}

/***************************************************************************
 * Util_UnLoad_Compiler - Free memory occipied by compiler and compiler stack
 ***************************************************************************/
static Util_UnLoad_Compiler()
{
  FreeMem(Comp_Stack_Base, Comp_Stack_Size);
  UnLoadSeg(Comp_Segment_List);
}

/***************************************************************************
 * Util_Envoke_Compiler - Envoke the resident compiler. Return an error code.
 *  the fname specified the name of the file to compiler, if fname == ""
 *  then the compilation will take place from an emacs buffer.
 ***************************************************************************/
static Util_Envoke_Compiler(fname)
register char *fname;
{ register LONG i;
  register int  j;
  register struct Compiler_Vars *map;

  Util_Free_Error_List();

  /* Current Line Being Compiled */
  Current_Line = 0;
  Display_Mask = 0x0003;

  /* Current Line Display Position */
  cl_row = ttrow;
  cl_col = ttcol;

  ttcursortoggle();
  ttflush();
  i = Do_Compiler_Command(EMACS_COMP_CMD_COMP_PROG, fname);
  ttcursortoggle();
  ttflush();

  if (i == ERR_COMP_PROG_NO_ERRORS)
  {
    map = Comp_Var_Map;
    if (map->CompVarOutFileType) /* .OBM file */
    {
      ewprintf("Object Module Generated. Code:%ld UData:%ld IData:%ld Total:%ld",
               map->CompVarCodeSize,
               map->CompVarUDataSize,
               map->CompVarIDataSize,
               map->CompVarTotalSize);
    }
    else /* .SBM file */
    {
      ewprintf("Symbol Module Generated.");
    }
  }

  if (i == ERR_COMP_PROG_ERRORS)
  {
    j = Util_Read_Error_Data_File();
    ewprintf("Source Compiled, %d Errors Found.", j);
  }

  if (i == ERR_COMP_PROG_FILE_NOT_FOUND)
  {
    ewprintf("ERROR: Compilation Aborted. File '%s' Not Found!", fname);
  }

  if (i == ERR_COMP_NO_MEMORY)
  {
    ewprintf("ERROR: Compilation Aborted. Insufficient Memory To Compile Program!");
  }

  if (i == ERR_COMP_ERR_FILE_NOT_OPENED)
  {
    ewprintf("ERROR: Compilation Aborted. Compiler's Error Log File Not Opened!");
  }
  if (i == ERR_COMP_OUTPUT_FILE)
  {
    ewprintf("ERROR: Opening or Writing Output File Failed!");
  }
  return(i);
}


/***************************************************************************
 * Util_Read_Error_Data_File - Read the error data file and create a linked
 *  list of ERROR structures.
 ***************************************************************************/
static Util_Read_Error_Data_File()
{ register FILE          *fp;
  register struct ERROR  *p;
  register UBYTE         tag;
  register int           ErrCount;

  ErrCount = 0;
  Error_List_Head = NULL;
  fp = fopen(ErrDat_FileName, "r");
  if (fp == NULL)
  {
    return(ErrCount);
  }
  tag = getc(fp);
  if (tag != ERR_FILE_TAG_FNM)
  {
    goto close_error_file;
  }
  
  /* Skip null-term file name field */
  while (getc(fp) != 0) {}

  while (TRUE)
  {
    tag = getc(fp);
    if ((tag == EOF) || (tag != ERR_FILE_TAG_ERR))
    {
      goto done_reading_error_file;
    }
    p = (struct ERROR*)malloc(sizeof(struct ERROR));
    if (p == NULL) 
    {
      goto done_reading_error_file;
    }
    p->next    = NULL;
    p->pos     = ((ULONG)getc(fp)<<24)|((ULONG)getc(fp)<<16)|
                 ((ULONG)getc(fp)<<8)|((ULONG)getc(fp)<<0);
    p->errcode = (getc(fp)<<8)|(getc(fp)<<0);
    if (Error_List_Head == NULL)
    {
      Error_List_Head = p;
      Error_List_Tail = p;
    }
    else
    {
      Error_List_Tail->next = p;
      Error_List_Tail = p;
    }
    ErrCount++;
    if ((Error_Read_Limit != 0) && (Error_Read_Limit == ErrCount))
    {
        break;
    }
  }

  done_reading_error_file:
  if (ErrCount == 0)
  {
    goto close_error_file;
  }

  Current_Error = NULL;
  Error_List_Type_Flag = FALSE; /* Char pos list */
  Error_State = ERROR_STATE_INIT;
  goto close_error_file;

  close_error_file:
  fclose(fp);
  return(ErrCount);
}

/***************************************************************************
 * Util_Free_Error_List - deallocate memory occupied by error list nodes.
 ***************************************************************************/
static Util_Free_Error_List()
{ register struct ERROR  *p, *f;
  
  if (Error_State == ERROR_STATE_INIT)
  {
    p = Error_List_Head;
    while (p != NULL) 
    {
      f = p;
      p = p->next;
      free(f);
    }
    Error_State = ERROR_STATE_UNINIT;
    Util_Restore_Window_Title();
  }
}

/***************************************************************************
 * Util_Lookup_ErrMsg - lookup the error message for a given error code.
 ***************************************************************************/
static char *Util_Lookup_ErrMsg(errCode)
{ register int i;

  i = 0;
  while (ErrorTable[i].code != errCode)
  {
    if (ErrorTable[i].code == NULL)
    {
      break;
    }
    i++;
  }
  return(ErrorTable[i].text);
}


/* FmtNum - convert a number to ascii appending to string (util func) */
static char *fmtNumPtr;	/* ptr to end of string to store into */
static FmtNum(n)
UWORD n;
{
  if (n > 9)
    FmtNum((n / 10));
  *fmtNumPtr++ = (n % 10) + '0';
}

/***************************************************************************
 * Util_Show_Error - Utility functions used to display compiler errors.
 ***************************************************************************/
static Util_Show_Error(nextErrorFlag)
{ register struct BUFFER  *bp;
  register struct LINE    *lp;
  register struct LINE    *elp;
  register short          eco;
  register UWORD          i;
  register LONG           n;

  if (Error_State == ERROR_STATE_UNINIT)
  {
    ewprintf("No Errors Found During Last Compilation!");
    return;
  }

  if (Error_Buffer_or_File_Flag == TRUE) /* Compiled from buffer */
  {
    bp = (struct BUFFER*)bfind(Error_Buffer_or_File_Name, FALSE);
    if (bp == NULL) /* If buffer was killed then invalidate error list. */
    {      
      ewprintf("ERROR: Buffer '%s' Not Found! (Error List Discarded)", Error_Buffer_or_File_Name);
      Util_Restore_Window_Title();
      Util_Free_Error_List();
      Util_Free_Error_Name();
      return;
    }    
  }
  else /* Compiled from file */
  {
    bp = bheadp;
    while (bp != NULL)
    {
      if (strcmp(bp->b_fname, Error_Buffer_or_File_Name) == 0)
      {
        break;
      }
      bp = bp->b_bufp;
    }
    if (bp == NULL)
    {
      if (Error_List_Type_Flag == FALSE)
      {
        ewprintf("ERROR: File '%s' Not Loaded In Buffer!", Error_Buffer_or_File_Name);
      }
      else /* File Was Loaded Once But Was Then Killed! */
      {
        ewprintf("ERROR: File '%s' Not Loaded In Buffer! (Error List Discarded)", Error_Buffer_or_File_Name);
        Util_Free_Error_List();
        Util_Free_Error_Name();
      }

      Util_Restore_Window_Title();
      return;
    }    
    if (Error_List_Type_Flag == FALSE)
    {
      Util_Process_Error_List(bp);
    }
  }

  /* Advance to next error... */
  skip_to_next_error:

  if (Current_Error == NULL)
  { /* First Error In List */
    Current_Error = Error_List_Head;
  }
  else
  {
    if (nextErrorFlag == TRUE)
    {
      if (Current_Error->next == NULL)
      {
        ttbeep(); /* Blink display to get user's attention */
        ewprintf("End of Error List! (Returning to First Error)");
        Current_Error = NULL;
        Util_Restore_Window_Title();
        return;
      }
      Current_Error = Current_Error->next;
    }
  }

  nextErrorFlag = TRUE; /* Set in case we have to skip to next error */

  elp = Current_Error->line;

  if (elp == NULL)
  {
    goto skip_to_next_error;
  }

  lp  = lforw(bp->b_linep);

  while (elp != lp)
  {
    if (lp == bp->b_linep)
    {
      goto skip_to_next_error;
    }
    lp = lforw(lp);
  }

  eco = Current_Error->coffset;
  if (eco > llength(elp))
  {
      goto skip_to_next_error;
  }

  /* Set Cursor To Error Position */
  curbp = bp;
  showbuffer(bp, curwp, WFFORCE|WFHARD);
  curwp->w_dotp = elp;
  curwp->w_doto = eco;

  i = Current_Error->errcode;
  { char str[128];
    short pad;

    /* Construct String To Be Used For Title Bar */
    strcpy(str, "Error ");
    fmtNumPtr = &str[ strlen(str) ];
    FmtNum(i);
    *fmtNumPtr = 0; /* Terminate string */
    strcat(str, ": ");
    strcat(str, Util_Lookup_ErrMsg(i));
    pad = strlen(str);
    while (pad < sizeof(Error_Msg))
    {
      str[pad++] = ' ';
    }
    str[pad] = 0;

    strncpy(Error_Msg, str, sizeof(Error_Msg));
    SetWindowTitles(EmW, Error_Msg, -1L);
  }
  ewprintf("Error %d: %s", i, Util_Lookup_ErrMsg(i));
}

/***************************************************************************
 * Util_Process_Error_List - convert an error list containg char pos in
 *  buffer into a list of line ptrs and offset into line. This type of list
 *  is uneffected by manipulations of the buffer, and therefore maintains
 *  the original error position correctly.
 ***************************************************************************/
static Util_Process_Error_List(bp)
register struct BUFFER *bp;
{ register struct ERROR *ep;
  register short        llen;
  register short        cofs;
  register long         cpos;
  register struct LINE  *lp;
  register long         ppos;

  if (Error_State == ERROR_STATE_UNINIT)
  {
    return;
  }

  lp   = lforw(bp->b_linep);	/* first line of buffer */
  llen = llength(lp);		/* length of current line */
  cofs = 0;			/* initial offset into line */
  ep = Error_List_Head;		/* first error in lust */
  ppos = 0;			/* previous error position */
  while (ep != NULL)
  {
    cpos = ep->pos - ppos;	/* offset relative to previous error */
    ppos += cpos;

    while(lp != bp->b_linep)
    {
      while(cpos)
      {
        cpos--;
        if (cofs == llen)
        {
          break;
        }
        cofs++;
      }
      if (cpos == 0)
      {
        break; /* Valid Error position found */
      }

      lp = lforw(lp);		/* Advance to next line */
      llen = llength(lp);	/* Length of current line */
      cofs = 0;			/* Start of line */
    }

    if (lp != bp->b_linep) /* Valid Pos */
    {
      ep->line    = lp;
      ep->coffset = cofs;
    }
    else /* Invalid Pos */
    {
      ep->line = NULL;
    }

    ep = ep->next;
  }
  Error_List_Type_Flag = TRUE; /* List contains line,ofs pairs */
}

/***************************************************************************
 * Util_Compile_Buffer - Compile the contents of a buffer.
 *  This function expects a ptr to a buffer. returns TRUE if no errors 
 *  FALSE if errors or other problems.
 ***************************************************************************/
static Util_Compile_Buffer(bp)
register struct BUFFER *bp;
{   register int   s;

  Util_Free_Error_List();
  Util_Free_Error_Name();

  Error_Buffer_or_File_Flag = TRUE;
  Error_Buffer_or_File_Name = malloc(strlen(bp->b_bname)+1);
  if (Error_Buffer_or_File_Name == NULL)
  { /* No Memory For Name String */
    return(FALSE);
  }
  strcpy(Error_Buffer_or_File_Name, bp->b_bname);

  /* initialize for compilation from buffer */
  Comp_Buffer_First_Line = bp->b_linep;
  Comp_Buffer_Line       = lforw(bp->b_linep);
  Comp_Buffer_Char       = 0;

  ewprintf("Compiling Buffer: %s  ", bp->b_bname);

  /* empty string indicates compiling from buffer */
  s = Util_Envoke_Compiler("");

  if (s == ERR_COMP_PROG_ERRORS)
  { /* Convert error list of char pos -> line,offset */
    Util_Process_Error_List(bp);
  }

  return(s == ERR_COMP_PROG_NO_ERRORS);
}

/***************************************************************************
 * Util_Compile_File - Compile file of a specified name.
 *  Returns TRUE if no errors, FALSE if errors or other problems.
 ***************************************************************************/
static Util_Compile_File(fname)
char *fname;
{
  Error_Buffer_or_File_Flag = FALSE;

  Util_Free_Error_List();
  Util_Free_Error_Name();

  Error_Buffer_or_File_Name = malloc(strlen(fname)+1);
  if (Error_Buffer_or_File_Name == NULL)
  { /* No Memory For Name String */
    return(FALSE);
  }

  strcpy(Error_Buffer_or_File_Name, fname);

  ewprintf("Compiling File: %s  ", fname);

  return(Util_Envoke_Compiler(fname) == ERR_COMP_PROG_NO_ERRORS);
}

/***************************************************************************
 * Util_Free_Error_Name - free memory occupied by error name string
 ***************************************************************************/
static Util_Free_Error_Name()
{
  if (Error_Buffer_or_File_Name != NULL)
  {
    free(Error_Buffer_or_File_Name);
    Error_Buffer_or_File_Name = NULL;
  }
}

/***************************************************************************
 * Util_Restore_Window_Title - Restore window title to Emacs Version
 ***************************************************************************/
static Util_Restore_Window_Title()
{
    SetWindowTitles(EmW, version, -1L);
}

/***************************************************************************
 * Util_Init_Emacs_Comp - This function is used to initialize emacs to work
 *  with the Modula-2 Compiler/Linker.
 ***************************************************************************/
Util_Init_Emacs_Comp()
{
#asm
	lea	DataSegPtr(PC),A0
	move.L	A4,(A0)			; initialize C data segment ptr
#endasm

#ifdef	M2FKEYS
  keyadd((KEY)KF1,	m2errnexterror,		"m2-err-next-error");
  keyadd((KEY)KSF1,	m2errcurrenterror,	"m2-err-current-error");
  keyadd((KEY)KF2,	m2compcurrentbuffer,	"m2-comp-current-buffer");
  keyadd((KEY)KSF2,	m2compbuffer,		"m2-comp-buffer");
  keyadd((KEY)KF3,	m2linkmain,		"m2-link-main");
  keyadd((KEY)KSF3,	m2linkmodule,		"m2-link-module");
  keyadd((KEY)KF4,	m2runmain,		"m2-run-main");
  keyadd((KEY)KSF4,	m2runmodule,		"m2-run-module");
  keyadd((KEY)KF5,	m2compfile,		"m2-comp-file");
  keyadd((KEY)KSF5,	freememoryavailable,	"free-mem-avail");
  keyadd((KEY)KF6,	setm2mainmodule,	"set-m2-main-module");
  keyadd((KEY)KSF6,	setm2linkoutdir,	"set-m2-link-out-dir");
  keyadd((KEY)KF7,	setm2objdir,		"set-m2-obj-dir");
  keyadd((KEY)KSF7,	setm2linkgensym,	"set-m2-link-gen-sym");
  keyadd((KEY)KF8,	m2compload,		"m2-comp-load");
  keyadd((KEY)KSF8,	m2compunload,		"m2-comp-unload");
  keydup((KEY)KF9,	"find-file");
  keydup((KEY)KSF9,	"find-file-other-window");
  keydup((KEY)KF10,	"save-buffer");
  keydup((KEY)KSF10,	"write-file");
#endif	/* M2FKEYS */

  keyadd((KEY)-1,	setm2compopt,		"set-m2-comp-opt");
  keyadd((KEY)-1,	setm2compstacksize,	"set-m2-comp-stack-size");
  keyadd((KEY)-1,	setm2errcount,		"set-m2-err-count");
  keyadd((KEY)-1,	setm2runstacksize,	"set-m2-run-stack-size");
  keyadd((KEY)-1,	setm2runwindow,		"set-m2-run-window");
  keyadd((KEY)-1,	setm2runargs,		"set-m2-run-args");

  keyadd((KEY)-1,	currentdir,		"current-dir");
  keyadd((KEY)-1,	exactfnamemode,		"exact-fname-mode");

#ifdef	FILEREQ
  keyadd((KEY)-1,	filereqmode,		"file-req-mode");
#endif
}


/***************************************************************************
 * Util_Exit_Emacs_Comp_Cleanup - This function is called when the user
 *  issues an exit command, this code frees memory occupied by the compiler.
 ***************************************************************************/
Util_Exit_Emacs_Comp_Cleanup()
{
  if (Comp_State == COMP_STATE_INIT)
  {
    m2compunload(FALSE, NULL, NULL);
  }

  Util_Free_Error_Name();
  Util_Free_Error_List();

  /* Return to initial current directory if necessary */
  if (CD_Original_Flag)
  {
    UnLock( CurrentDir(CD_Original_Lock) );
  }
}


/***************************************************************************
 * GetCharFromBuffer - this function is called by the Modula-2 compiler to
 *  fetch the next char from the buffer.
 ***************************************************************************/

/* PrtNum - print a decimal number */
static VOID PrtNum(n)
register UWORD	n;
{
	if (n > 9)
		PrtNum((n / 10) );
	ttputc((n % 10) + '0');
}

static char GetCharFromBuffer()
{ register char c;

#asm
	move.L	A4,-(SP)		; preserve Modula-2 A4 reg
	move.L	DataSegPtr(PC),A4	; load ptr to C data segment
#endasm

  if ((Comp_Buffer_Char+1) > llength(Comp_Buffer_Line))
  {
    if (Comp_Buffer_Line == Comp_Buffer_First_Line)
    { /* returning 0 tells the compiler it has reached end of file */
      c = 0;
      goto done;
    }
    else
    { /* Advance to next line */
      Comp_Buffer_Line = lforw(Comp_Buffer_Line);
      Comp_Buffer_Char = 0;
      c = '\n';

      /* Display line # being compiled every (x) number of lines */
      if ((Current_Line & Display_Mask ) == 0)
      {
        ttcol = HUGE; /* Causes actual cursor position to move */
        ttmove(cl_row, cl_col);
        PrtNum(Current_Line + 1);
        ttflush();
        if (Current_Line == 64)
        {
          Display_Mask = 0x001f;
        }
      }
      Current_Line++;

      goto done;
    }
  }
  c = lgetc(Comp_Buffer_Line, Comp_Buffer_Char++);

done:
#asm
	move.L	(SP)+,A4	; restore Modula-2 A4 reg
#endasm
  return(c);
}

/***************************************************************************
 * Util_Prompt_Opt_Val - query input from user for value option
 * ret: TRUE = new value or default
 *      FALSE = user aborted (CTRL-G)
 ***************************************************************************/
Util_Prompt_Opt_Val(val, min, max, text)
ULONG	*val;
ULONG	min;
ULONG	max;
char	*text;
{ register int 	s;
  register ULONG res;
  char 		buf[16];

retry:
  if ((s=ereply("%s [%ld..%ld]: (default %ld) ",buf,sizeof(buf),
       text,min,max,*val)) != TRUE)
  {
    return (s != ABORT);
  }

  res = atol(buf);

  /* If value outside of min or max then retry */
  if ((res < min) || (res > max))
  {
    ttbeep(); /* flash display */
    goto retry;
  }

  /* Valid Value Entered */
  *val = res;
  return (TRUE);
}

/***************************************************************************
 * Util_Prompt_Opt_Bool - query input from user for boolean option
 * ret: TRUE = new value or default
 *      FALSE = user aborted (CTRL-G)
 ***************************************************************************/
Util_Prompt_Opt_Bool(flag, text)
UBYTE	*flag;
char	*text;
{ register int 	s;
  register UBYTE res;
  register char	*status;
  char 		buf[6];

  if (*flag) { status = "TRUE"; } else { status = "FALSE"; }
  
retry:
  if ((s=ereply("%s [T or F]: (default %s) ",buf,sizeof(buf),
       text,status)) != TRUE)
  {
    return (s != ABORT);
  }

  /* Convert to upper case */
  res = toupper(buf[0]);

  if ((res != 'T') && (res != 'F'))
  {
    ttbeep(); /* flash display */
    goto retry;
  }

  /* Valid Value Entered */
  *flag = (res == 'T');
  return (TRUE);
}
