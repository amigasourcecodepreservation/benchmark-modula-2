(* Name   : M2EM.DEF
 * Changed: 02/19/87 LF
 * Notes :
 * 1. Change the GlbVarStartAdr constant to 0 from -28, the old value used
 *  28 bytes to describe module at run-time, this is not really used, so
 *  the 28 bytes were freed for general purpose use.
 * 2. Changed constant "GlbParmStartAdr" to variable. This is necessary because
 *    depending on the glbReloc flag this variable will be 12 or 8.
 *)

(*$L+ relocatable *)
DEFINITION MODULE M2EM;

  FROM M2DM IMPORT ObjPtr, ParPtr, Standard;
  FROM M2HM IMPORT Item;
  FROM M2SM IMPORT Symbol;

  CONST   wtabmax        =   4;
          GlbVarStartAdr =   0;
          LocVarStartAdr =   0;
          LocParStartAdr =  12;

  VAR     wlev: CARDINAL;  (* WITH nesting level *)
          GlbParStartAdr : INTEGER;

  PROCEDURE AllocVar(obj: ObjPtr; VAR adr: INTEGER);
  PROCEDURE AllocPar(par: ParPtr; VAR adr: INTEGER);
  PROCEDURE AllocFld(obj: ObjPtr; VAR adr: INTEGER);

  PROCEDURE GenItem    (VAR x: Item; y, scope: ObjPtr);
  PROCEDURE GenIndex   (VAR x, y: Item);                (* x := x[y]     *)
  PROCEDURE GenField   (VAR x: Item; f: ObjPtr);        (* x := x.f      *)
  PROCEDURE GenDeRef   (VAR x: Item);                   (* x := x^       *)
  PROCEDURE GenNeg     (VAR x: Item);                   (* x := -x       *)
  PROCEDURE GenNot     (VAR x: Item);                   (* x := ~x       *)
  PROCEDURE GenAnd     (VAR x: Item);
  PROCEDURE GenOr      (VAR x: Item);
  PROCEDURE GenSingSet (VAR x, e: Item);                (* x := {e}      *)
  PROCEDURE GenSet     (VAR x, e1, e2: Item);           (* x := {e1..e2} *)
  PROCEDURE GenIn      (VAR x, y: Item);                (* x := x IN y   *)
  PROCEDURE GenOp      (op: Symbol; VAR x, y: Item);    (* x := x op y   *)
  PROCEDURE GenWith    (VAR x: Item; VAR adr: INTEGER); (* x := WITH x   *)
  PROCEDURE GenWith2;
  PROCEDURE GenStParam (VAR p, x: Item; fctno: Standard;
                        parno: CARDINAL; morepar: BOOLEAN);
  PROCEDURE GenStFct   (fctno: Standard; parno: CARDINAL);
  PROCEDURE InitM2EM;

END M2EM.
