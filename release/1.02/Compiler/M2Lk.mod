(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                      (c) 1986, 1987 by Leon Frenkel                      *
 ****************************************************************************
 * Name: M2Lk.MOD                          Version: Amiga.01.00             *
 * Created: 01/28/87   Updated: 12/25/87   Author: Leon Frenkel             *
 * Description: A Linker For Modula-2 Programs.                             *
 ****************************************************************************)

(* Change History:
 * 00.00 First Version
 * 00.01 Fixed improper module initialization order
 * 00.02 Removed "-v" option; Added "M2L:" as default search path
 * 00.03 Fixed no handing of module identifier in reference files.
 * 01.00 Added "-g" to support source level debugging
 * 01.01 Handle 32-bit source pos
 *)
MODULE M2Lk;

FROM SYSTEM IMPORT BYTE, WORD, LONGWORD, ADDRESS, TSIZE, ADR, SETREG, INLINE,
                   REG;
FROM AmigaDOS IMPORT Lock, UnLock, FileLock, FileHandle, SharedLock, Read, 
                     Write, Seek, OffsetBeginning, ModeNewFile, DeleteFile;
                     IMPORT AmigaDOS;
FROM CMemory IMPORT calloc, malloc, free;
FROM CPrintTerminal IMPORT printf;
FROM FormatString IMPORT FormatArg;
FROM FileSystem IMPORT Response, File, Lookup, Close, Delete, ReadWord, 
                       ReadChar, WriteWord, WriteChar, GetPos;
FROM System IMPORT argc, argv, CLIReturnCode, ExecBase;
FROM Strings IMPORT CopyString, ConcatString, StringLength, CompareString, 
                    Relation;

CONST
  Title = 
    "M2SCS: Linker  Amiga.01.01 Copyright \xA9 1987 by Leon Frenkel\n";

  CmdLineArgChars = "osdag"; (* Cmd Line Arg chars *)
  (* Out Files Dir                     : -o <str> *)
  (* Add Dir To Search List            : -s <str> *)
  (* Add debug symbols to all modules  : -d       *)
  (* Append module name to each symbol : -a       *)
  (* Gen Source Level Debugger Info    : -g       *)

TYPE
  ArgType = (argTypeOutFilesDir,
             argTypeSearchFilesDir,
             argTypeDebugSymbols,
             argTypeAppendModName,
             argTypeSLDInfo,
             argTypeLinkModule,
             argTypeInvalid,
             argTypeListEnd);

CONST
  FirstArgTableType = argTypeOutFilesDir;
  LastArgTableType  = argTypeListEnd;

  (* (L)ink (O)ption (D)efault ... *)
  LODDebugSymbols  = FALSE;
  LODAppendModName = FALSE;
  LODSLDInfo       = FALSE;

  (* Default Search Directory *)
  DefSearchPath = "M2L:";

  (* CLI Return codes *)
  ErrCLIInvalidArgs = 20;

  (* The linker excepts version 1 of OBM file *)
  OBMFileVersion = 1;

  (* OBM File Block Tags *)
  HDR = 1;
  IMP = 2;
  DAT = 3;
  COD = 4;

  (* AmigaDOS Hunk Tags *)
  HUNKHeader  = 3F3H;
  HUNKCode    = 3E9H;
  HUNKReloc32 = 3ECH;
  HUNKSymbol  = 3F0H;
  HUNKDebug   = 3F1H;
  HUNKEnd     = 3F2H;

  (* 68000 instruction opcodes *)
  InstJMPAbs      = 4EF9H;		(* JMP.L xxx     *)
  InstJSRAbs      = 4EB9H;		(* JSR.L xxx     *)

  (* Internal Error Codes *)
  ErrOBMFileNotFound  = 0;
  ErrOBMFileInvalid   = 1;
  ErrOBMFileVersion   = 2;
  ErrBadKey           = 3;
  ErrNoMemory         = 4;
  ErrImportedBadKey   = 5;
  ErrOutFileNotOpened = 6;
  ErrNoSystemModule   = 7;
  ErrSBMRFMFileInvalid= 8;
  ErrSBMRFMFileBadKey = 9;
  MaxErrCode = 9;

  MaxProcedure = 255;
  MaxModule    = 255;


TYPE
  FilePtr = POINTER TO File;

  NAME = ARRAY [0..255] OF CHAR;
  KEY  = ARRAY [0..2] OF CARDINAL;

  ModulePtr = POINTER TO Module;
  MTablePtr = POINTER TO MTable;
  MTable = ARRAY [0..MaxModule] OF ModulePtr;
  PTablePtr = POINTER TO PTable;
  PTable = ARRAY [0..MaxProcedure] OF LONGCARD;
  Module =
    RECORD
      Next             : ModulePtr;		(* Next Module In List       *)
      Name             : POINTER TO NAME;	(* Module Name               *)
      Key              : KEY;			(* Module Key                *)
      ModuleTable      : MTablePtr;		(* Table Of Imported Modules *)
      ProcTable        : PTablePtr;		(* Table Of Proc Offsets     *)
      Hunk             : CARDINAL;		(* Hunk # Assigned To Module *)
      HunkLWSize       : LONGCARD;		(* Size Of Hunk in LongWords *)
      HunkLWAlign      : BOOLEAN;		(* TRUE = needs to be aligned*)
      ModuleBaseOffset : LONGCARD;		(* Base Of Data in Hunk      *)
      CodeBaseOffset   : LONGCARD;		(* Base Of Code in Hunk      *)
      Procedures       : CARDINAL;		(* Procedures in Module      *)
      Modules          : CARDINAL;		(* Modules imported          *)
      CodeSize         : LONGCARD;		(* Size of code (word align) *)
      DataSize         : LONGCARD;		(* Size of data (word align) *)
      ConstSize        : LONGCARD;		(* Size of const(word align) *)
      FirstModProcReloc: CARDINAL;		(* Adr of first Proc Reloc   *)
      ModProcRelocCount: CARDINAL;		(* Proc Reloc Count          *)
      ModAdrRelocCount : CARDINAL;		(* Adr  Reloc Count          *)
      GenSymbols       : BOOLEAN;		(* TRUE = generate symbols   *)
      AppendModName    : BOOLEAN;		(* TRUE = prefix module name *)
      OBMFile          : FilePtr;		(* Ptr to File Structure     *)
      Visited          : BOOLEAN;
    END;

  (* a node structure in a name list *)
  NameNodePtr = POINTER TO NameNode;
  NameNode = RECORD
               Next : NameNodePtr;
               Name : POINTER TO NAME;
             END;

  (* a name list *)
  NameList = RECORD
               Head    : NameNodePtr;
               Tail    : NameNodePtr;
               Current : NameNodePtr;
             END;

VAR
  InFile         : FilePtr;			(* Get Input From This File *)
  OutFile        : FileHandle;			(* Put Output To This File  *)
  ModuleListHead : ModulePtr;			(* Head of Module List      *)
  ModuleListTail : ModulePtr;			(* Tail of Module List      *)
  CurrentModule  : ModulePtr;			(* Current Module           *)
  NextHunk       : CARDINAL;			(* Next Hunk ID             *)
  TotalCode      : LONGCARD;			(* Total Code Size          *)
  TotalData      : LONGCARD;			(* Total Data Size          *)
  TotalConst     : LONGCARD;			(* Total Const Size         *)
  TotalInit      : LONGCARD;			(* Total Init Size          *)
  ErrTable       : ARRAY [0..MaxErrCode] OF POINTER TO NAME; (* Err Msgs    *)
  ErrStatus      : BOOLEAN;			(* TRUE = error occured     *)
  argo           : ARRAY [0..9] OF FormatArg;	(* printf() args            *)
  PutBufPtr      : ADDRESS;			(* ptr to base of buffer    *)
  PutBufEnd      : ADDRESS;                     (* ptr to end of buffer     *)
  PutBufPos      : RECORD			(* current pos in buffer    *)
                     CASE :CARDINAL OF
                     |0: B: POINTER TO CHAR;    (* ptr used to store a char *)
                     |1: W: POINTER TO CARDINAL;(* ptr used to store a word *)
                     |2: L: POINTER TO LONGCARD;(* ptr used to store a long *)
                     END;
                   END;
  ArgTable         : POINTER TO ARRAY [FirstArgTableType..LastArgTableType] OF CHAR;
  ValidCmdLine     : BOOLEAN;
  CurrentArg       : CARDINAL;
  CurrentArgStrPtr : POINTER TO NAME;
  OutFilesDir      : POINTER TO NAME;		(* output to directory      *)
  LinkOptDebugSymbols: BOOLEAN;			(* T = add syms, F = no syms*)
  LinkOptAppendModName: BOOLEAN;		(* T = append mod name, F=no*)
  LinkOptSLDInfo   : BOOLEAN;                   (* T = SLD info, F = no SLD *)
  LinkModuleList   : NameList;			(* list of modules          *)
  OBMDirList       : NameList;			(* dirs to look for OBM     *)
  RFMSBMDirList    : NameList;			(* dirs to look for RFM,SBM *)
  VisitedVal       : BOOLEAN;			(* Node has been visited val*) 
  ModuleNodeFunc   : PROCEDURE(VAR Module);	(* Proc to call for each node*)
  mainmod          : ModulePtr;			(* Pointer to main module *)
  Offset           : LONGCARD;			(* Offset into hunk *)


(* GetChar - read a char from InFile *)
PROCEDURE GetChar(): CHAR;
VAR ch: CHAR;
BEGIN
  ReadChar(InFile^, ch);
  RETURN ch;
END GetChar;

(* GetWord - read a word from InFile *)
PROCEDURE GetWord(): CARDINAL;
VAR w: CARDINAL;
BEGIN
  ReadWord(InFile^, w);
  RETURN w;
END GetWord;

(* GetLong - read a long word from InFile *)
PROCEDURE GetLong(): LONGCARD;
VAR
  conv : RECORD
           CASE :CARDINAL OF
           |0: h, l: CARDINAL;
           |1: lw  : LONGCARD;
           END;
         END;
BEGIN
  ReadWord(InFile^, conv.h);
  ReadWord(InFile^, conv.l);
  RETURN conv.lw;
END GetLong;

(* InitPutBuf - Allocate an output buffer *)
PROCEDURE InitPutBuf(size: LONGCARD): BOOLEAN;
BEGIN
  PutBufPtr   := malloc(size);
  PutBufEnd   := PutBufPtr + ADDRESS(size);
  PutBufPos.L := PutBufPtr;
  RETURN(PutBufPtr # NIL);
END InitPutBuf;

(* FlushPutBuf - Write Buffer To Disk, and reset ptr to beginning *)
PROCEDURE FlushPutBuf;
VAR r : LONGINT;
BEGIN
  IF (ADDRESS(PutBufPos.L) - PutBufPtr) # 0D THEN
    r := Write(OutFile,PutBufPtr,LONGCARD(ADDRESS(PutBufPos.L) - PutBufPtr));
  END;
  PutBufPos.L := PutBufPtr; (* reset *)
END FlushPutBuf;

(* FreePutBuf - Free buffer *)
PROCEDURE FreePutBuf;
BEGIN
 free(PutBufPtr);
END FreePutBuf;

(* PutChar - store char into buffer *)
PROCEDURE PutChar(c: CHAR);
BEGIN
  IF (ADDRESS(PutBufPos.B) + ADDRESS(SIZE(c))) > PutBufEnd THEN
    FlushPutBuf;
  END;
  PutBufPos.B^ := c;
  INC(ADDRESS(PutBufPos.B), SIZE(c));
END PutChar;

(* PutWord - store word into buffer *)
PROCEDURE PutWord(w: CARDINAL);
BEGIN
  IF (ADDRESS(PutBufPos.W) + ADDRESS(SIZE(w))) > PutBufEnd THEN
    FlushPutBuf;
  END;
  PutBufPos.W^ := w;
  INC(ADDRESS(PutBufPos.W), SIZE(w));
END PutWord;

(* PutLong - store long word into buffer *)
PROCEDURE PutLong(lw: LONGCARD);
BEGIN
  IF (ADDRESS(PutBufPos.L) + ADDRESS(SIZE(lw))) > PutBufEnd THEN
    FlushPutBuf;
  END;
  PutBufPos.L^ := lw;
  INC(ADDRESS(PutBufPos.L), SIZE(lw));
END PutLong;

(* OpenFile - open a file for reading *)
PROCEDURE OpenFile(VAR fileName: ARRAY OF CHAR): FilePtr;
VAR fp: FilePtr;
BEGIN
  fp := malloc(TSIZE(File));
  IF fp # NIL THEN
    Lookup(fp^, fileName, FALSE);
    IF fp^.res # done THEN
      free(fp);
      fp := NIL;
    END;
  END;
  RETURN fp;
END OpenFile;

(* CloseFile - close a file *)
PROCEDURE CloseFile(fp: FilePtr);
BEGIN
  Close(fp^);
  free(fp);
END CloseFile;

(* LongWord - round number to a longword *)
PROCEDURE LongWord(l: LONGCARD): LONGCARD;
BEGIN
  RETURN (l + 3D) DIV 4D;
END LongWord;

(* PrintErr - print error msg *)
PROCEDURE PrintErr(errCode: CARDINAL);
BEGIN
  printf("ERROR: ",argo); printf(ErrTable[errCode]^, argo);
  ErrStatus := TRUE;
END PrintErr;

(* Test For "^C" Break By User. Returns TRUE - if ^C, FALSE - otherwise. *)
PROCEDURE BreakCheck(): BOOLEAN;
TYPE
  SignalSet = SET OF [0..31];
CONST
  D0 = 0; D1 = 1; A6 = 14;
  BreakC = 12; 
  BreakCMask = SignalSet{BreakC};
  LVOSetSignal = 0FECEH;
VAR SigMask : SignalSet;
BEGIN
  SETREG(A6, ExecBase);        (* MOVE.L ExecBase,A6    *)
  SETREG(D0, 0D);              (* MOVE.L #0,D0          *)
  SETREG(D1, BreakCMask);      (* MOVE.L #BreakCMask,D1 *)
  INLINE(4EAEH, LVOSetSignal); (* JSR LVOSetSignal(A6)  *)
  SigMask := SignalSet(REG(D0));
  IF BreakC IN SigMask THEN (* ^C Break Detected *)
    printf("^C\n",argo);
    RETURN TRUE;
  ELSE (* No Break *)
    RETURN FALSE;
  END;
END BreakCheck;

(* FreeNameList - free nodes of a name list *)
PROCEDURE FreeNameList(VAR nl: NameList);
VAR n, f: NameNodePtr;
BEGIN
  n := nl.Head;
  WHILE n # NIL DO
    f := n;
    n := n^.Next;
    free(f);
  END;
  nl.Head    := NIL;
  nl.Tail    := NIL;
  nl.Current := NIL;
END FreeNameList;

(* AddNameNode - add a name to a name list *)
PROCEDURE AddNameNode(VAR nl: NameList; VAR str: ARRAY OF CHAR): BOOLEAN;
VAR p : NameNodePtr;
BEGIN
  p := malloc(TSIZE(NameNode));
  IF p # NIL THEN
    p^.Next := NIL;
    p^.Name := ADR(str);
    IF nl.Head = NIL THEN
      nl.Head := p;
      nl.Tail := p;
    ELSE
      nl.Tail^.Next := p;
      nl.Tail := p;
    END;
  END;
  RETURN p # NIL;
END AddNameNode;

(* ScanDirList - modify the fname string if a file is in a directory *)
PROCEDURE ScanDirList(VAR nl: NameList; VAR fname: ARRAY OF CHAR);
VAR
  lock : FileLock;
  p    : NameNodePtr;
  path : NAME;
BEGIN
  IF nl.Head = NIL THEN RETURN; END;

  (* Look into current directory *)
  lock := Lock(ADR(fname), SharedLock);
  IF lock # NIL THEN
    UnLock(lock);
    RETURN;
  END;

  (* Begin searching through directories for a match *)
  p := nl.Head;
  WHILE p # NIL DO
    CopyString(path, p^.Name^);
    ConcatString(path, fname);
    lock := Lock(ADR(path), SharedLock);
    IF lock # NIL THEN
      UnLock(lock);
      CopyString(fname, path);
      RETURN;
    END;
    p := p^.Next;
  END;
END ScanDirList;


(* GenDebugSymbols - output a symbol block to output file *)
PROCEDURE GenDebugSymbols(VAR cm: Module);
VAR InFileName : NAME;
    FileType   : BOOLEAN;                         (* TRUE = RFM, FALSE = SBM *)
    ModNameLen : CARDINAL;                        (* Length of module name   *)
    block      : INTEGER;                         (* ref file block          *)
    i,n,l,a    : INTEGER;
    k0,k1,k2   : CARDINAL;
    c          : CHAR;
    symbolLen  : CARDINAL;                        (* length of symbol        *)
    symbol     : ARRAY [0..511] OF CHAR;          (* symbol string           *)
    src        : LONGCARD;

  (* Ref File block types *)
  CONST REFFILE = 335B;
    CTL = -5000B; anchor = 0; ModTag = 1; ProcTag = 2; RefTag = 3; linkage = 4;
    STR = -6000B; enum = 0; range = 1; pointer = 2; set = 3; procTyp = 4;
          funcTyp = 5; array = 6; dynarr = 7; record = 8; opaque = 9;
    CMP = -7000B; parref = 0; par = 1; field = 2;
    OBJ = -10000B; varref = 0; var = 1; const = 2; string = 3; type = 4;
          proc = 5; func = 6; module = 7; svc = 8; svcfunc = 9;

  (* GetName - read a name string *)
  PROCEDURE GetName();
  VAR c: CHAR;
      L: INTEGER;
      i: CARDINAL;
  BEGIN
    L := CARDINAL(GetChar()) - 1;
    symbolLen := L;
    i := 0;
    WHILE L > 0 DO
      symbol[i] := GetChar();
      INC(i);
      DEC(L);
    END;
  END GetName;

  (* PutSymbol - generate a symbol record *)
  PROCEDURE PutSymbol(value: LONGCARD; ModNameFlag: BOOLEAN);
  VAR len : CARDINAL;
      i,j : CARDINAL;

  BEGIN
    len := symbolLen;
    IF ModNameFlag AND cm.AppendModName THEN (* <ModuleName>_<Symbol> *)
      len := len + ModNameLen + 1;
    END;

    (* symbol data unit [0][NameLenInLongWords] *)
    PutLong(LongWord(len));

    (* prefix module name if necessary *)
    IF ModNameFlag AND cm.AppendModName THEN (* Output module name and _ *)
      i := 0;
      j := ModNameLen;
      WHILE j > 0 DO
        PutChar(cm.Name^[i]);
        INC(i);
        DEC(j);
      END;
      PutChar("_");
    END;

    (* output symbol *)
    i := 0;
    j := symbolLen;
    WHILE j > 0 DO
      PutChar(symbol[i]);
      INC(i);
      DEC(j);
    END;

    (* pad symbol to longword boundary *)
    j := (LongWord(len)*4D) - LONGCARD(len);
    WHILE j # 0 DO
      PutChar(0C);
      DEC(j);
    END;

    (* output symbol value *)
    PutLong(value);
  END PutSymbol;

BEGIN
  ModNameLen := StringLength(cm.Name^);

  CopyString(InFileName, cm.Name^);
  ConcatString(InFileName, ".RFM");
  ScanDirList(RFMSBMDirList, InFileName);
  InFile := OpenFile(InFileName);
  IF InFile = NIL THEN
    CopyString(InFileName, cm.Name^);
    ConcatString(InFileName, ".SBM");
    ScanDirList(RFMSBMDirList, InFileName);
    InFile := OpenFile(InFileName);
    IF InFile = NIL THEN
      RETURN; (* No Symbol File Found From Which To Read Symbols! *)
    END;
    FileType := FALSE; (* SBM File *)
  ELSE      
    FileType := TRUE; (* RFM File *)
  END;

  argo[0].L := ADR(InFileName);
  printf("Sym %s\n",argo);

  IF GetWord() # REFFILE THEN (* Invalid File *)
    CloseFile(InFile);
    argo[0].L := ADR(InFileName);
    PrintErr(ErrSBMRFMFileInvalid);
    RETURN;
  END;

  (* Check key of symbol file against key of object module! *)
  block := GetWord(); (* anchor *)
  k0    := GetWord(); (* key1 *)
  k1    := GetWord(); (* key2 *)
  k2    := GetWord(); (* key3 *)
  GetName();          (* name *)
  IF (k0 # cm.Key[0]) OR
     (k1 # cm.Key[1]) OR
     (k2 # cm.Key[2]) THEN
    CloseFile(InFile);
    argo[0].L := ADR(InFileName);
    PrintErr(ErrSBMRFMFileBadKey);
    RETURN;
  END;

  (* Start of Symbol Block *)
  PutLong(HUNKSymbol);

  (* Generate Module Base Symbol (for view from Hunk Display) *)
  CopyString(symbol, cm.Name^);
  symbolLen := StringLength(symbol);
  PutSymbol(0D, FALSE);

  (* Generate Module Body Symbol *)
  CopyString(symbol, cm.Name^);
  ConcatString(symbol, "_ModuleBody");
  symbolLen := StringLength(symbol);
  PutSymbol(cm.CodeBaseOffset + 4D, FALSE);

  LOOP
    block := GetWord();
    IF block < CMP THEN
      block := block - OBJ;
      CASE block OF
      | varref:      i := GetWord(); (* StrRef *)
                     i := GetWord(); (* level  *)
                     i := GetWord(); (* adr    *)
                     GetName();      (* name   *)
      | var:         i := GetWord(); (* StrRef *)
                     l := GetWord(); (* level  *)
                     a := GetWord(); (* adr    *)
                     GetName();      (* name   *)
                     IF l = 0 THEN (* Global Variable *)
                      PutSymbol(LONGINT(cm.ModuleBaseOffset) + LONGINT(a),TRUE);
                     END;
      | const:       i := GetWord(); (* StrRef *)
                     i := GetWord(); (* ModRef *)
                     c := GetChar(); (* value length *)
                     CASE ORD(c) OF (* value *)
                     | 2: c := GetChar();
                     | 3: i := GetWord();
                     | 5: i := GetWord(); i := GetWord();
                     | 9: i := GetWord(); i := GetWord();
                          i := GetWord(); i := GetWord();
                     END;
                     GetName();
      | string:      i := GetWord(); (* StrRef *)
                     GetName();      (* string *)
                     GetName();      (* name   *)
      | type:        i := GetWord(); (* StrRef *)
                     i := GetWord(); (* ModRef *)
                     GetName();      (* name   *)
      | proc,func:   IF block = func THEN (* function *)
                       i := GetWord(); (* ResRef *)
                     END;
                     n := GetWord(); (* ProcNo *)
                     l := GetWord(); (* level  *)
                     a := GetWord(); (* adr    *)
                     i := GetWord(); (* size   *)
                     GetName;        (* name   *)
                     IF FileType THEN (* RFM File *)
                       PutSymbol(LONGINT(cm.CodeBaseOffset) + LONGINT(a),TRUE);
                     ELSE (* SBM File *)
                       PutSymbol(cm.CodeBaseOffset + cm.ProcTable^[n],TRUE);
                     END;
      | module:      i := GetWord(); (* ModNo  *)
                     GetName;        (* name   *)
      | svc,svcfunc: IF block = svcfunc THEN
                       i := GetWord(); (* ResRef *)
                     END;
                     i := GetWord(); (* cnum *)
                     GetName();      (* name *)
      ELSE (* Unknown block type *)
        argo[0].L := ADR(InFileName);
        PrintErr(ErrSBMRFMFileInvalid);
        EXIT;
      END; (* CASE *)

    ELSIF block < STR THEN
      block := block - CMP;
      CASE block OF
      | parref, par: i := GetWord(); (* StrRef *)
      | field:       i := GetWord(); (* StrRef *)
                     i := GetWord(); (* offset *)
                     GetName();      (* name   *)
      ELSE (* Unknown block type *)
        argo[0].L := ADR(InFileName);
        PrintErr(ErrSBMRFMFileInvalid);
        EXIT;
      END; (* CASE *)

    ELSIF block < CTL THEN
      block := block - STR;
      i := GetWord(); (* size *)
      CASE block OF      
      | enum:             i := GetWord(); (* noConst *)
      | range:            i := GetWord(); (* BaseRef *)
                          i := GetWord(); (* min     *)
                          i := GetWord(); (* max     *)
      | pointer:          ;
      | set:              i := GetWord(); (* BaseRef *)
      | procTyp, funcTyp: IF block = funcTyp THEN
                            i := GetWord(); (* ResRef *)
                          END;
      | array:            i := GetWord(); (* ElemRef *)
                          i := GetWord(); (* IndxRef *)
      | dynarr:           i := GetWord(); (* ElemRef *)
      | record:           ;
      | opaque:           ;
      ELSE (* Unknown block type *)
        argo[0].L := ADR(InFileName);
        PrintErr(ErrSBMRFMFileInvalid);
        EXIT;
      END; (* CASE *)

    ELSIF block < 0 THEN 
      block := block - CTL;
      CASE block OF
      | anchor:  i := GetWord(); (* key1 *)
                 i := GetWord(); (* key2 *)
                 i := GetWord(); (* key3 *)
                 GetName();      (* name *)
      | ModTag:  i := GetWord(); (* ModNo *)
      | ProcTag: i := GetWord(); (* ProcNo *)
      | RefTag:  i := GetWord(); (* adr *)
                 i := GetWord(); (* pno *)
                 EXIT; (* End of file *)
      | linkage: i := GetWord(); (* StrRef *)
                 i := GetWord(); (* BaseRef *)
      ELSE (* Unknown block type *)
        argo[0].L := ADR(InFileName);
        PrintErr(ErrSBMRFMFileInvalid);
        EXIT;
      END; (* CASE *)

    ELSE (* PC block *)
      src := GetLong(); (* 32-bit src position *)
    END; (* IF *)
  END; (* LOOP *)

  PutLong(0); (* Symbol Block Terminator *)

  CloseFile(InFile);
END GenDebugSymbols;


(* NewModule - create a new Module struc or return ptr to existing struc *)
(* Return codes returned by this function *)
CONST
  NewModuleOk       = 0;
  NewModuleBadKey   = 1;
  NewModuleNoMemory = 2;

PROCEDURE NewModule(VAR mName: NAME; VAR mKey: KEY;
                    VAR modStruc: ModulePtr): CARDINAL;
VAR 
  mod : ModulePtr;
  i   : CARDINAL;
BEGIN
  mod := ModuleListHead;
  WHILE (mod # NIL) AND (CompareString(mod^.Name^,mName) # equal) DO
    mod := mod^.Next;
  END;
  IF mod # NIL THEN (* module found in list *)
    IF (mKey[0] # mod^.Key[0]) OR
       (mKey[1] # mod^.Key[1]) OR
       (mKey[2] # mod^.Key[2]) THEN (* Keys dont match *)
      RETURN(NewModuleBadKey);
    END;
  ELSE (* module not found in list *)
    (* Allocate module structure, clear all fields *)
    mod := calloc(1, TSIZE(Module));
    IF mod = NIL THEN
      RETURN(NewModuleNoMemory);
    END;

    (* Allocate space for module name *)
    mod^.Name := malloc(StringLength(mName)+1);
    IF mod^.Name = NIL THEN
      free(mod);
      RETURN(NewModuleNoMemory);
    END;

    (* Store string into buf *)
    CopyString(mod^.Name^, mName);

    (* Copy key into module struc *)
    mod^.Key := mKey;

    (* Assign module a hunk ID *)
    mod^.Hunk := NextHunk;
    INC(NextHunk);

    (* Link new structure into list *)
    IF ModuleListHead = NIL THEN
      ModuleListHead := mod;
      ModuleListTail := mod;
    ELSE
      ModuleListTail^.Next := mod;
      ModuleListTail := mod;
    END;
  END;

  modStruc := mod;

  RETURN(NewModuleOk);
END NewModule;

(* ReadProcTable - allocate memory and read from InFile the procedure table *)
PROCEDURE ReadProcTable(procs: CARDINAL): PTablePtr;
VAR p: PTablePtr;
    i: CARDINAL;
BEGIN
  p := malloc(CARDINAL(TSIZE(LONGCARD)) * procs);
  IF p # NIL THEN
    FOR i := 0 TO procs-1 DO
      p^[i] := GetWord();
    END;
  END;
  RETURN(p);
END ReadProcTable;

(* GetModuleName - read a module name(null-term string) and a key *)
PROCEDURE GetModuleName(VAR mName: NAME; VAR mKey: KEY): BOOLEAN;
VAR
  i : CARDINAL;
  c : CHAR;
BEGIN
  i := 0;
  REPEAT
    c := GetChar();
    mName[i] := c;
    INC(i);
  UNTIL (c = 0C);

  IF i = 1 THEN (* Dummy Import Entry *)
    RETURN(FALSE);
  END;

  (* Real Entry *)
  FOR i := 0 TO 2 DO
    mKey[i] := GetWord();
  END;

  RETURN(TRUE);
END GetModuleName;

(* GetModule - Get a module *)
PROCEDURE GetModule(VAR ModName: ARRAY OF CHAR);
VAR
  InFileName : NAME;
  mName      : NAME;
  mKey       : KEY;
  i, j       : CARDINAL;
  b          : BOOLEAN;
BEGIN
  (* Create FileName from Module Name *)
  CopyString(InFileName, ModName);
  ConcatString(InFileName, ".OBM");
  
  (* Scan dir list, stores result back into InFileName *)
  ScanDirList(OBMDirList, InFileName);
  
  (* Open '.OBM' file for input *)
  InFile := OpenFile(InFileName);
  IF InFile = NIL THEN
    argo[0].L := ADR(InFileName);
    PrintErr(ErrOBMFileNotFound);
    RETURN;
  END;
  
  (* read HDR block tag *)
  IF GetWord() # HDR THEN
    CloseFile(InFile);
    argo[0].L := ADR(InFileName);
    PrintErr(ErrOBMFileInvalid);
    RETURN;
  END;

  i := GetWord();  (* BlockSize *)

  (* read version of object module, must match version supported by linker *)
  IF GetWord() # OBMFileVersion THEN
    CloseFile(InFile);
    argo[0].L := ADR(InFileName);
    PrintErr(ErrOBMFileVersion);
    RETURN;
  END;

  (* Read Object Module Name *)
  b := GetModuleName(mName, mKey);
  
  i := NewModule(mName, mKey, CurrentModule);
  IF i = NewModuleBadKey THEN
    CloseFile(InFile);
    argo[0].L := ADR(ModName);
    PrintErr(ErrBadKey);
    RETURN;
  ELSIF i = NewModuleNoMemory THEN
    CloseFile(InFile);
    PrintErr(ErrNoMemory);
    RETURN;
  END;

  (* fill in module structure *)
  WITH CurrentModule^ DO
    OBMFile          := InFile;
    CodeSize         := GetWord();
    DataSize         := GetWord();
    ConstSize        := GetWord();
    Procedures       := GetWord();
    Modules          := GetWord();
    ModProcRelocCount:= GetWord();
    FirstModProcReloc:= GetWord();
    ModAdrRelocCount := GetWord();
    ModuleBaseOffset := DataSize;
    CodeBaseOffset   := ModuleBaseOffset + ConstSize;
    HunkLWSize       := LongWord(CodeBaseOffset + CodeSize);
    HunkLWAlign      := ((CodeBaseOffset + CodeSize) MOD 4D) # 0D;

    (* Debug Symbol Init Stuff *)
    GenSymbols    := LinkOptDebugSymbols;
    AppendModName := LinkOptAppendModName;

    ModuleTable      := malloc(CARDINAL(TSIZE(ModulePtr)) * Modules);
    IF ModuleTable = NIL THEN
      PrintErr(ErrNoMemory);
      RETURN;
    END;
    ModuleTable^[0] := CurrentModule;
    
    (* Process Import Block *)
    i := GetWord(); (* IMP Tag *)
    i := GetWord(); (* BlockSize *)
    FOR i := 1 TO Modules-1 DO
      IF NOT GetModuleName(mName, mKey) THEN (* Dummy Import Entry *)
        ModuleTable^[i] := NIL;
      ELSE (* Real Import Entry *)
        j := NewModule(mName, mKey, ModuleTable^[i]);
        IF j = NewModuleBadKey THEN
          argo[0].L := ADR(mName);
          argo[1].L := ADR(ModName);
          PrintErr(ErrImportedBadKey);
          RETURN;
        ELSIF j = NewModuleNoMemory THEN
          PrintErr(ErrNoMemory);
          RETURN;
        END; (* IF *)
      END; (* IF *)
    END; (* FOR *)

    (* DAT Block *)
    i := GetWord(); (* Tag *)
    i := GetWord(); (* BlockSize *)
    ProcTable := ReadProcTable(Procedures);
    IF ProcTable = NIL THEN
      PrintErr(ErrNoMemory);
      RETURN;
    END;
  END; (* WITH *)

  argo[0].L := ADR(InFileName);
  printf(" <- %s\n",argo);

END GetModule;

(* LinkModule - generate a hunk for specified module *)
PROCEDURE LinkModule(VAR cm: Module);
TYPE
  RelocRec = RECORD
               Next   : CARDINAL;	(* Next RelocRec (array index) *)
               Offset : LONGCARD;	(* 32-bit Hunk offset          *)
             END;
  RelocHdr = RECORD
               First : CARDINAL;	(* Index of first RelocRec *)
               Count : CARDINAL;	(* Number of RelocRec in list *)
             END;

VAR
  (* Read Adr Relocs into this buffer *)
  ModAdrBufPtr  : POINTER TO ARRAY [0..16382] OF INTEGER;
  (* Read Code into this buffer *)
  CodeBufPtr    : POINTER TO ARRAY [0..16382] OF CARDINAL;
  (* Generate Relocs into this buffer *)
  RelocBufPtr   : POINTER TO ARRAY [0..5460] OF RelocRec;
  (* Root of reloc records, array indexed by hunk id *)
  RelocHdrPtr   : POINTER TO ARRAY [0..8190] OF RelocHdr;
  i,j,k,p,m     : CARDINAL;
  a             : INTEGER; (* Signed 16-bit Adr Offset *)
  r             : LONGINT;
  pos           : LONGCARD;
  RelocIdx      : CARDINAL; (* next relocation record index *)
  conv : RECORD
           CASE :CARDINAL OF
           |0: h, l: CARDINAL;
           |1: lw  : LONGCARD;
           END;
         END;

  (* Reloc - Generate a reloc record and link into hunk list *)
  PROCEDURE Reloc(hunk: CARDINAL; ofs: LONGCARD);
  BEGIN
    RelocBufPtr^[RelocIdx].Next   := RelocHdrPtr^[hunk].First;
    RelocBufPtr^[RelocIdx].Offset := ofs;
    RelocHdrPtr^[hunk].First := RelocIdx;
    INC(RelocHdrPtr^[hunk].Count);
    INC(RelocIdx);
  END Reloc;

BEGIN
  RelocIdx := 0;
  InFile := cm.OBMFile; (* Connect input stream to current object module *)

  (* Update totals *)
  INC(TotalCode,  LONGCARD(cm.CodeSize));
  INC(TotalData,  LONGCARD(cm.DataSize));
  INC(TotalConst, LONGCARD(cm.ConstSize));

  (* Maximum possible size of buffer! *)
  IF NOT InitPutBuf((cm.HunkLWSize*4D)+
                   ((LONGCARD(cm.ModAdrRelocCount)+
                     LONGCARD(cm.ModProcRelocCount)+1D)*12D) + 100D) THEN
    PrintErr(ErrNoMemory);
    RETURN;
  END;

  (* Perform all memory allocations for this function! *)
  ModAdrBufPtr := malloc(cm.ModAdrRelocCount * 2);
  CodeBufPtr   := malloc(cm.CodeSize);
  RelocBufPtr  := malloc((cm.ModAdrRelocCount + cm.ModProcRelocCount + 1)
                         * CARDINAL(TSIZE(RelocRec)));
  RelocHdrPtr  := calloc(NextHunk, TSIZE(RelocHdr)); (* Init to 0 *)
  IF (ModAdrBufPtr = NIL) OR (CodeBufPtr = NIL) OR (RelocBufPtr = NIL) OR
     (RelocHdrPtr = NIL) THEN
    IF ModAdrBufPtr # NIL THEN free(ModAdrBufPtr); END;
    IF CodeBufPtr   # NIL THEN free(CodeBufPtr);   END;
    IF RelocBufPtr  # NIL THEN free(RelocBufPtr);  END;
    IF RelocHdrPtr  # NIL THEN free(RelocHdrPtr);  END;
    FreePutBuf;
    PrintErr(ErrNoMemory);
    RETURN;
  END;

  (* Read Adr Reloc Offset List *)
  j := 0;
  i := cm.ModAdrRelocCount;
  WHILE i # 0 DO
    ModAdrBufPtr^[j] := GetWord();
    INC(j);
    DEC(i);
  END;

  (* Output Code Hunk Representing Module *)
  PutLong(HUNKCode);
  PutLong(cm.HunkLWSize);

  (* Output Data Section *)
  i := cm.DataSize;
  WHILE i # 0 DO
    PutWord(0);
    DEC(i, 2);
  END;

  (* Output Const Section *)
  i := cm.ConstSize;
  WHILE i # 0 DO
    PutWord(GetWord());
    DEC(i, 2);
  END;

  i := GetWord(); (* Tag COD *)
  i := GetWord(); (* BlockSize *)
  (********************************************************************)
  (* WARNING: The following operation makes a direct call to AmigaDOS *)
  (*          and therefore no further FileSystem module calls can be *)
  (*          performed. This is because the AmigaDOS file ptr would  *)
  (*          get mixed up.                                           *)
  (********************************************************************)
  GetPos(InFile^, pos);
  r := Seek(InFile^.handle, pos, OffsetBeginning);
  r := Read(InFile^.handle, ADR(CodeBufPtr^), cm.CodeSize);

  (* Store ptr to ModuleBase into adr 0 of code section *)
  conv.lw := cm.ModuleBaseOffset;
  CodeBufPtr^[0] := conv.h;
  CodeBufPtr^[1] := conv.l;

  (* Reloc entry for ptr to modulebase at pc address 0 *)
  Reloc(cm.Hunk, cm.CodeBaseOffset);

  (* Fixup Proc Reloc *)
  i := cm.ModProcRelocCount;
  j := cm.FirstModProcReloc DIV 2;
  WHILE i # 0 DO
    k := j;
    j := CodeBufPtr^[k+1] DIV 2;
    m := CARDINAL(BYTE(CodeBufPtr^[k]));
    p := CARDINAL(BYTE(CodeBufPtr^[k] DIV 256));
    conv.lw := cm.ModuleTable^[m]^.ProcTable^[p] +
               cm.ModuleTable^[m]^.CodeBaseOffset;
    CodeBufPtr^[k]   := conv.h;
    CodeBufPtr^[k+1] := conv.l;

    Reloc(cm.ModuleTable^[m]^.Hunk, cm.CodeBaseOffset + LONGCARD(k * 2));
    DEC(i);
  END;

  (* Fixup Adr Reloc *)
  i := cm.ModAdrRelocCount;
  j := 0;
  WHILE i # 0 DO
    k := ModAdrBufPtr^[j] DIV 2;
    INC(j);
    m := CodeBufPtr^[k];
    a := CodeBufPtr^[k+1];
    (* The variable "a" is an INTEGER and the conversion to LONGINT is done
     * using sign extension *)
    conv.lw := LONGINT(cm.ModuleTable^[m]^.ModuleBaseOffset) + LONGINT(a);
    CodeBufPtr^[k]   := conv.h;
    CodeBufPtr^[k+1] := conv.l;

    Reloc(cm.ModuleTable^[m]^.Hunk, cm.CodeBaseOffset + LONGCARD(k * 2));
    DEC(i);
  END;

  (* Output CodeBuf *)
  i := cm.CodeSize;
  j := 0;
  WHILE i # 0 DO
    PutWord(CodeBufPtr^[j]);
    INC(j);
    DEC(i, 2);
  END;

  (* Align on Long Word Boundary If Not Already *)
  IF cm.HunkLWAlign THEN
    PutWord(0);
  END;

  (* Output Reloc 32 For The Current Hunk *)
  (* To conserve on space all refrences to a given hunk should be *)
  (* generated at one time *)
  PutLong(HUNKReloc32);
  FOR i := 1 TO NextHunk-1 DO
    IF RelocHdrPtr^[i].Count # 0 THEN
      j := RelocHdrPtr^[i].First;
      PutLong(RelocHdrPtr^[i].Count); (* # of offsets *)
      PutLong(i); (* Hunk # *)
      FOR k := 1 TO RelocHdrPtr^[i].Count DO
        PutLong(RelocBufPtr^[j].Offset);
        j := RelocBufPtr^[j].Next;
      END;
    END;
  END;
  PutLong(0); (* Reloc 32 Terminator *)

  IF cm.GenSymbols THEN (* Generate symbols *)
    FlushPutBuf;
    FreePutBuf;

    IF InitPutBuf(20000D) THEN (* symbol buffer allocated *)
      
      GenDebugSymbols(cm);
      (* InFile is invalid after this func *)

      (* End of Hunk *)
      PutLong(HUNKEnd);

      (* write put buffer to disk *)
      FlushPutBuf;
      FreePutBuf;
    ELSE (* no memory for symbol buffer *)
      PrintErr(ErrNoMemory);
      (* continue... (deallocation) *)
    END;

  ELSE (* Dont generate symbols *)
    (* End of Hunk *)
    PutLong(HUNKEnd);

    (* write put buffer to disk *)
    FlushPutBuf;
    FreePutBuf;
  END;

  (* deallocate all buffers *)
  free(ModAdrBufPtr);
  free(CodeBufPtr);
  free(RelocBufPtr);
  free(RelocHdrPtr);
END LinkModule;

(* FindModule - return ptr to the module of the specified name or NIL *)
PROCEDURE FindModule(ModName: ARRAY OF CHAR): ModulePtr;
VAR
  mod : ModulePtr;
BEGIN
  mod := ModuleListHead;
  WHILE (mod # NIL) AND (CompareString(mod^.Name^, ModName) # equal) DO
    mod := mod^.Next;
  END;
  RETURN(mod);
END FindModule;

(* TraverseModule - Used to recursively traverse the module tree *)
PROCEDURE TraverseModule(VAR mod: Module);
VAR
  n : CARDINAL;
BEGIN
  IF (mod.Visited # VisitedVal) THEN (* Visit node *)
    mod.Visited := VisitedVal; (* Node already visited *)
    FOR n := 1 TO mod.Modules-1 DO (* Traverse each node of subtree *)
      IF mod.ModuleTable^[n] # NIL THEN (* not a dummy entry *)
        TraverseModule(mod.ModuleTable^[n]^);
      END;
    END;
    ModuleNodeFunc(mod); (* Perform operation for current node *)
  END;
END TraverseModule;

(* OutputInitJump - Output JMP or JSR for module node *)
PROCEDURE OutputInitJump(VAR mod: Module);
BEGIN
  IF ADR(mod) = mainmod THEN
    PutWord(InstJMPAbs);
  ELSE
    PutWord(InstJSRAbs);
  END;
  PutLong(mod.CodeBaseOffset + 4D);
END OutputInitJump;

(* OutputInitReloc - Output a relocation entry for a module node *)
PROCEDURE OutputInitReloc(VAR mod: Module);
BEGIN
  PutLong(1);
  PutLong(mod.Hunk);
  PutLong(Offset);
  INC(Offset, 6D);
END OutputInitReloc;

(* LinkHeader - generate the initial hunk for a Modula-2 program. The purpose
 *              of this hunk is to initialize a program. This proc generates
 *              the following code:
 *              JSR xxx.L  ( System.Module_Body )
 *              JSR xxx.L  ( Library1.Module_Body )
 *              JSR xxx.L  ( Library2.Module_Body )
 *                 ...
 *              JSR xxx.L  ( LibraryN.Module_Body )
 *              JMP xxx.L  ( Main.Module_Body )
 *)
PROCEDURE LinkHeader;
VAR
  InitCodeSize   : LONGCARD;
  InitCodeLWSize : LONGCARD;
  InitCodeAlign  : BOOLEAN;
  mp,mod,sysmod  : ModulePtr;
  tmpPos         : POINTER TO LONGCARD;
  i              : INTEGER;
BEGIN
  mainmod := ModuleListHead;
  sysmod := FindModule("System");
  IF sysmod = NIL THEN (* Nobody imported the module "System" *)
    PrintErr(ErrNoSystemModule);
    RETURN;
  END;

  InitCodeLWSize := LongWord((LONGCARD(NextHunk) - 1D) * 6D);
  InitCodeSize   := InitCodeLWSize * 4D;
  InitCodeAlign  := (((LONGCARD(NextHunk) - 1D) * 6D) MOD 4D) # 0D;

  (* Update total *)
  TotalInit := InitCodeSize;

  IF NOT InitPutBuf(5000D) THEN
    PrintErr(ErrNoMemory);
    RETURN;
  END;
  PutLong(HUNKHeader);			(* hunk tag *)
  PutLong(0);				(* end of resident lib list *)
  PutLong(NextHunk);			(* hunk table size *)
  PutLong(0);				(* first hunk in table *)
  PutLong(NextHunk-1);			(* last hunk in table *)
  PutLong(InitCodeLWSize);		(* size of first hunk *)
  mod := ModuleListHead;
  WHILE mod # NIL DO			(* generate rest of hunk table *)
    PutLong(mod^.HunkLWSize);
    mod := mod^.Next;
  END;
  PutLong(HUNKCode);			(* generate code hunk *)
  PutLong(InitCodeLWSize);

  PutWord(InstJSRAbs);			(* JSR System.Module_Body *)
  PutLong(sysmod^.CodeBaseOffset + 4D);

  (* Generate a JSR to each library module and a JMP to the main module.
   * The order of the jumps in the table is determined by traversing the
   * module tree. *)
  sysmod^.Visited := TRUE; (* Skip the System module *)
  VisitedVal := TRUE;
  ModuleNodeFunc := OutputInitJump; (* Function to call for each node *)
  TraverseModule(ModuleListHead^);

  (* Align code hunk on long word boundary *)
  IF InitCodeAlign THEN
    PutWord(0);
  END;

  (* Generate SourceLevelDebugger Info in a HUNKDebug *)
  IF (LinkOptSLDInfo) THEN
    (* Output debuging hunk *)
    PutLong(HUNKDebug);

    (* remember current position for backpatching *)
    tmpPos := ADDRESS(PutBufPos.L);
    PutLong(0);

    (* # of modules *)
    PutWord(NextHunk - 1);

    (* Output Module Records *)
    mod := ModuleListHead;
    WHILE mod # NIL DO
      PutLong(mod^.ModuleBaseOffset);
      PutLong(mod^.CodeBaseOffset);
      PutWord(mod^.Key[0]);
      PutWord(mod^.Key[1]);
      PutWord(mod^.Key[2]);
      i := -1;
      REPEAT
        INC(i);
        PutChar(mod^.Name^[i]);
      UNTIL (mod^.Name^[i] = 0C);
      IF (NOT ODD(i)) THEN PutChar(0C); END;

      mod := mod^.Next;
    END;

    (* Pad block to longword boundary *)
    i := (ADDRESS(PutBufPos.L) MOD 4D);
    IF (i # 0) THEN
      i := 4 - i;
      WHILE (i # 0) DO
        PutChar(0C);
        DEC(i);
      END;
    END;

    (* Backpatch size of block *)
    tmpPos^ := (ADDRESS(PutBufPos.L) - ADDRESS(tmpPos) - 4D) DIV 4D;
  END; (* LinkOptSLDInfo *)


  PutLong(HUNKReloc32);			(* hunk reloc tag *)
  Offset := 2D;				(* relocation offset = 2 *)

  (* Generate reloc for JSR to System *)
  PutLong(1);
  PutLong(sysmod^.Hunk);
  PutLong(Offset);
  INC(Offset, 6D);

  (* Generate a relocation entry for each JSR or JMP in the module initalizaiton
   * table. This operation is performed by traversing the module tree. *)
  sysmod^.Visited := FALSE; (* Skip the System module *)
  VisitedVal := FALSE;
  ModuleNodeFunc := OutputInitReloc; (* Function to output a reloc entry *)
  TraverseModule(ModuleListHead^);

  (* Terminate Reloc32 block *)
  PutLong(0);

  (* Terminate Hunk *)
  PutLong(HUNKEnd);

  (* Output Buffer To Disk *)
  FlushPutBuf;
  FreePutBuf;
END LinkHeader;

(* CleanupModuleList - free resource allocated by module list *)
PROCEDURE CleanupModuleList;
VAR
  p, f : ModulePtr;
BEGIN
  p := ModuleListHead;
  WHILE p # NIL DO
    IF p^.Name        # NIL THEN free(p^.Name); END;
    IF p^.ModuleTable # NIL THEN free(p^.ModuleTable); END;
    IF p^.ProcTable   # NIL THEN free(p^.ProcTable); END;
    IF p^.OBMFile     # NIL THEN CloseFile(p^.OBMFile); END;
    f := p;
    p := p^.Next;
    free(f);
  END;
END CleanupModuleList;

(* LinkProgram - This proc is called with the name of a program to be linked *)
PROCEDURE LinkProgram(VAR ProgName: ARRAY OF CHAR);
VAR
  OutFileName : NAME;
  b : BOOLEAN;
BEGIN
  (* Initialization *)
  NextHunk := 1;		(* Hunk 0 is the init table *)
  ModuleListHead := NIL;
  ModuleListTail := NIL;
  TotalCode  := 0;
  TotalData  := 0;
  TotalConst := 0;
  TotalInit  := 0;
  ErrStatus  := FALSE;

  (* Linker Pass 1 *)
  GetModule(ProgName);
  LOOP
    CurrentModule := CurrentModule^.Next;
    IF (CurrentModule = NIL) OR (ErrStatus) THEN EXIT; END;
    GetModule(CurrentModule^.Name^);
  END;
  IF NOT ErrStatus THEN (* Pass 1 Finished Ok *)

    (* Generate output file name *)
    CopyString(OutFileName, OutFilesDir^);
    ConcatString(OutFileName, ProgName);

    (* Open Output File *)
    OutFile := AmigaDOS.Open(ADR(OutFileName),ModeNewFile);
    IF OutFile = NIL THEN
      argo[0].L := ADR(OutFileName);
      PrintErr(ErrOutFileNotOpened);
    ELSE (* Do Linker Pass 2 *)
      LinkHeader;
      CurrentModule := ModuleListHead;
      LOOP
        IF (CurrentModule = NIL) OR (ErrStatus) THEN EXIT; END;
        LinkModule(CurrentModule^);
        CurrentModule := CurrentModule^.Next;
      END;
        (* Close Output File *)
        AmigaDOS.Close(OutFile);
      IF ErrStatus THEN (* Delete Output File, Since link failed *)
        b := DeleteFile(ADR(OutFileName));
      ELSE (* No Errors *)
        argo[0].L := ADR(OutFileName);
        printf(" -> %s\n",argo);

        argo[0].L := TotalCode;
        argo[1].L := TotalData;
        argo[2].L := TotalConst;
        argo[3].L := TotalInit;
        argo[4].L := TotalCode + TotalData + TotalConst + TotalInit;
        printf("    Code:%lu UData:%lu IData:%lu Init:%lu Total:%lu\n",argo);
      END;
    END;
  END;

  (* Deallocate local resources *)
  CleanupModuleList;
END LinkProgram;

(* GetNextArg - fetch next argument from cmd line *)
PROCEDURE GetNextArg(): BOOLEAN;
BEGIN
  INC(CurrentArg);
  IF CurrentArg < argc THEN
    CurrentArgStrPtr := ADR(argv^[CurrentArg]^);
    RETURN TRUE;
  ELSE
    RETURN FALSE; 
  END;
END GetNextArg;

(* IdentifyNextArg - get next argument and return its type *)
PROCEDURE IdentifyNextArg(): ArgType;
VAR i: ArgType; 
    c: CHAR;
BEGIN
  IF GetNextArg() THEN

    (* A valid argument is ["-"] [<ch>] [ 0 ] *)
    IF (CurrentArgStrPtr^[0] = "-") AND (CurrentArgStrPtr^[2] = 0C) THEN
      c := CurrentArgStrPtr^[1];
      i := FirstArgTableType;
      WHILE (i <= LastArgTableType) DO
        IF ArgTable^[i] = c THEN 
          RETURN (i);
        END;
        INC(i);
      END;
    END;

    (* If first char of arg is "-" then unknown compiler argument *)
    IF CurrentArgStrPtr^[0] = "-" THEN 
      RETURN argTypeInvalid;
    ELSE (* Otherwise a filename strings *)
      RETURN argTypeLinkModule;
    END;
  ELSE (* no more cmd line args *)
    RETURN argTypeListEnd;
  END;
END IdentifyNextArg;

(* DoStrArg - handle a string arg. Eliminates '"' around name *)
PROCEDURE DoStrArg;
VAR len: CARDINAL;
BEGIN
  (* If arg has " around it remove the quotes *)
  len := StringLength(CurrentArgStrPtr^);
  IF (len > 2) AND
    (CurrentArgStrPtr^[len-1] = '"') AND
    (CurrentArgStrPtr^[0] = '"') THEN
    CurrentArgStrPtr^[len-1] := 0C;          (* strip off right " *)
    INC(ADDRESS(CurrentArgStrPtr));          (* strip off left  " *)
  END;
END DoStrArg;

(* ReadCmdLineArgs - process cmd line args *)
PROCEDURE ReadCmdLineArgs;
VAR
  str : POINTER TO ARRAY [0..255] OF CHAR;
BEGIN
  (* Set defaults for Linker Options *)
  LinkOptDebugSymbols  := LODDebugSymbols;
  LinkOptAppendModName := LODAppendModName;
  LinkOptSLDInfo       := LODSLDInfo;

  (* Current Dir is the default output files dir *)
  OutFilesDir := ADR("");

  ArgTable := ADR(CmdLineArgChars);
  ValidCmdLine := TRUE;

  (* Setup a default search path *)
  str := ADR(DefSearchPath);
  ValidCmdLine := AddNameNode(OBMDirList, str^);
  ValidCmdLine := AddNameNode(RFMSBMDirList, str^);

  LOOP
    IF NOT ValidCmdLine THEN EXIT; END;
    CASE IdentifyNextArg() OF
    | argTypeOutFilesDir: (* Output Files To Directory *)
      IF GetNextArg() THEN
        DoStrArg;
        OutFilesDir := ADDRESS(CurrentArgStrPtr);
      ELSE
        ValidCmdLine := FALSE;
      END;

    | argTypeSearchFilesDir: (* Add a name to list of dirs to search *)
      IF GetNextArg() THEN
        DoStrArg;
        ValidCmdLine := AddNameNode(OBMDirList, CurrentArgStrPtr^);
        ValidCmdLine := AddNameNode(RFMSBMDirList, CurrentArgStrPtr^);
      ELSE
        ValidCmdLine := FALSE;
      END;

    | argTypeDebugSymbols: (* Add debuging symbols to load file *)
      LinkOptDebugSymbols := NOT LinkOptDebugSymbols;

    | argTypeAppendModName: (* Debuging symbols appended with module name *)
      LinkOptAppendModName := NOT LinkOptAppendModName;

    | argTypeSLDInfo: (* Add SLD info to load file *)
      LinkOptSLDInfo := NOT LinkOptSLDInfo;

    | argTypeLinkModule: (* Add a module name to list to be linked *)
      DoStrArg;
      ValidCmdLine := AddNameNode(LinkModuleList, CurrentArgStrPtr^);

    | argTypeInvalid: (* Invalid Argument *)
      ValidCmdLine := FALSE;

    | argTypeListEnd: (* End Of Arg List Reached *)
      EXIT;

    END (* CASE *)
  END; (* LOOP *)
END ReadCmdLineArgs;

PROCEDURE AbortToDOS;
BEGIN
  FreeNameList(LinkModuleList);
  FreeNameList(OBMDirList);
  FreeNameList(RFMSBMDirList);
  HALT;
END AbortToDOS;

BEGIN
  (* Initialize table of error messages *)
  ErrTable[ErrOBMFileNotFound]  := ADR("Can't Open Obj File '%s'!\n");
  ErrTable[ErrOBMFileInvalid]   := ADR("Obj File '%s' is Invalid!\n");
  ErrTable[ErrOBMFileVersion]   := ADR("Obj File '%s' Wrong Version!\n");
  ErrTable[ErrBadKey]           := ADR("Module '%s' Has Bad Key!\n");
  ErrTable[ErrNoMemory]         := ADR("Not Enough Memory To Continue!\n");
  ErrTable[ErrImportedBadKey]   := ADR("Module '%s' Imported By '%s' Has Bad Key!\n");
  ErrTable[ErrOutFileNotOpened] := ADR("Can't Open Output File '%s'!\n");
  ErrTable[ErrNoSystemModule]   := ADR("Module 'System' Not Imported!\n");
  ErrTable[ErrSBMRFMFileInvalid]:= ADR("Sym File '%s' is Invalid!\n");
  ErrTable[ErrSBMRFMFileBadKey] := ADR("Sym File '%s' Has Bad key!\n");

  printf(Title, argo);
  ReadCmdLineArgs;
  IF NOT ValidCmdLine THEN
    argo[0].L := CurrentArgStrPtr;
    printf("Invalid Cmd Line Argument: %s\n",argo);
    CLIReturnCode := ErrCLIInvalidArgs;
    AbortToDOS;
  END;

  (* Link all modules specified in list *)
  LinkModuleList.Current := LinkModuleList.Head;
  WHILE LinkModuleList.Current # NIL DO
    argo[0].L := LinkModuleList.Current^.Name;
    printf("    %s\n",argo);
    LinkProgram(LinkModuleList.Current^.Name^);
    LinkModuleList.Current := LinkModuleList.Current^.Next;
    printf("\n",argo);

    (* Check for "^C" Abort From Linking *)
    IF BreakCheck() THEN
      LinkModuleList.Current := NIL; (* End Of List *)
    END;
  END;
  AbortToDOS;
END M2Lk.
