(* Name   : M2SM.MOD
 * Changed: 10/31/87 LF
 * Notes :
 * 1. Changed procedure "Times16" to a multiply by 16 operation.
 * 2. Changed procedure "ErrorBlock to write data to ERR.DAT in high-low order.
 * 3. Eliminated generating an an "err.LOG" file.
 * 4. Changed procedure "Diff" to use INC rather than var := var + 1, for
 *    purposes of efficency.
 * 5. Changed IdBuf to be dynamicly allocated. IdBuf now becomes a ptr to
 *    an array of chars.
 * 3. Removed procedure "CloseScanner".
 * 4. Removed procedure "Times16".
 * 5. Changed maxExp to maxPosExp and added maxNegExp also fixed a bug in the
 *    floating point parser which did not check if negative exponent was to
 *    large!
 * 6. Fixed bug: procedure "Number" array "dig" is not checked for going
 *    beyound the bounds of the array. I am adding code to do this check
 *    and exit the digit colection loop if array bounds is reached.
 * 7. Changed procedure "Enter","EnterKW" to assume that a literal string 
 *    passed will include the null-terminator as part of its HIGH size.
 * 8. Changed "InitScanner" to be more efficent.
 * 9. Changed "String","Identifier","Enter" to AbortCompilation if identifier
 *    buffer overflows.
 * 10.Changed "Comment" to recognize compiler switches.
 * 11.Changed procedure "Mark" to not write out errors that occure next to other
 *    errors to avoid cascading errors.
 * 12.Changed procedure "GetCh" and "Mark" to handle compiling from a buffer
 *    correctly. Added new variable "SourcePos" to keep track of current 
 *    pos in source.
 * 13.Changed procedure "GetSym" and "Identifier" to support "_" underscores
 *    in identifier.
 * 14.Changed procedure "String" to support 'C' style strings.
 * 15.Changed procedure "String". If the last char before a new-line char is
 *    "\" then the next line is considered to be part of the current line.
 * 16.Changed procedure "Number" "H" number to multiply using LONGCARD rathern
 *    than LONGINT to avoid overflow errors which are not correct.
 * 17.Changed constants FNM and ERR.
 * 18.Changed minimum distance between errors to 8 characters.
 * 19.Turned off compiler switch "D".
 * 20.Added huristic testing of identifiers to avoid unnecessary keyword
 *    searches. If an identifiers consits of any characters other than
 *    uppercase letters then no keyword search. An array is used to look if
 *    for the given length the possible first letter of a keyword matches
 *    that of the toke.
 * 21.The error position is now marked more accurately at the actual token
 *    which caused the error.
 * 22.Moved procedure "Ten" to outside of procedure "Number".
 * 23.Moved procedure "Comment" to outside of procedure "GenSym".
 * 24.Changed procedure "Comment" to ignore compiler switches of nested comments
 * 25.Changed error position to current token position.
 * 26.Changed error position to current token position before the comment.
 * 27.Added overflow checking for LONGINT/LONGCARD constants.
 * 28.Moved TokenPos from .MOD -> .DEF
 * Changed: 10/25/93 JKO
 *  1. Changed "IdBuf" references from array to pointer arithmatic.
 *  2. Changed "id0" and "id1" from integer to longcard for compatability
 *     with "IdBuf".
 *)
  

(*$L-,D-  SB-relative *)
IMPLEMENTATION MODULE M2SM;


FROM SYSTEM IMPORT ADDRESS, LONG, SETREG, INLINE; (*VAL*)
FROM FileSystem IMPORT ReadChar, WriteChar;
FROM M2   IMPORT DEBUG, AbortCompilation;
FROM M2HM IMPORT rngchk, ovflchk, glbReloc;
FROM M2PM IMPORT CopyDynValueParms;
FROM M2EM IMPORT GlbParStartAdr;

FROM InOut IMPORT Write, WriteCard, WriteString;


CONST 
   KW = 43; (*number of keywords*)
   maxDig = 7;
   maxCard = 177777B;
   (*      maxPosExp = 38; for IEEE*)
   (*      maxNegExp = 38; for IEEE*)
   maxPosExp = 18; (* for FFP *)
   maxNegExp = 20; (* for FFP *)
   FNM = VAL(CHAR,149);
   ERR = VAL(CHAR,249);
   ErrorDistance = 8D; (* minimum distance between multiple errors *)

   (* Special char codes *)
   BS = 010C;
   HT = 011C;
   LF = 012C;
   VT = 013C;
   FF = 014C;
   CR = 015C;

VAR 
  idBufPtr  : ADDRESS; (* id buffer pointer arithmatic variable *)
  
  ch:     CHAR;      (*current character*)
  (** id0, id1: INTEGER;  (*indices of identifier buffer*) **)
  id0, id1: LONGCARD;  (*indices of identifier buffer*)
  keyTab:   ARRAY [0..KW-1] OF
                RECORD sym: Symbol; ind: INTEGER END;
  K:        CARDINAL;
  pow:      ARRAY [0..5] OF REAL;

  PrevErrPos: LONGINT; (* location of previous error *)

  (* 0..13 - max length of KeyWord = 14 characters *)
  (* This array contains for each possible KeyWord length a set representing
   * the first letter of the keywords of that length. This allows a quick
   * check to avoid unnecessary keyword searches. *)
  KWTestSet: ARRAY [0..13] OF SET OF [0..31];

  (* Depth of comment nesting (if < 1 then ignore compiler switches) *)
  CommentNesting : CARDINAL;


PROCEDURE WriteCh(char: CHAR);

BEGIN
  IF (char < " ") OR (char > "~") THEN
    Write('"');
    WriteCard(ORD(char), 3);
    Write('"');
  ELSE
    Write(char);
  END (*IF*);
END WriteCh;


PROCEDURE ErrorBlock(errPos: LONGINT; errCod: CARDINAL);

VAR i: CARDINAL;
        conv: RECORD
                 CASE :CARDINAL OF
                  0: long: LONGINT
                | 1: sys:  ARRAY [0..3] OF CHAR;
                END;
              END;
BEGIN
    IF errPos >= 1D THEN DEC(errPos); END;
    WriteChar(errDat, ERR);
    WITH conv DO
      long := errPos; FOR i := 0 TO 3 DO WriteChar(errDat, sys[i]) END;
    END;
    WriteChar(errDat, CHR(errCod DIV 256)); 
    WriteChar(errDat, CHR(errCod MOD 256));
END ErrorBlock;


PROCEDURE Mark(n: INTEGER);

VAR
  conv : RECORD
           CASE :CARDINAL OF
           |0: H, L: CARDINAL;
           |1: HL  : LONGCARD;
           END;
         END;
BEGIN
  IF DEBUG THEN
     WriteString("Mark: ");
     WriteCard(n, 3);
     WriteString("\n");
  END (*IF*);
  scanerr := TRUE;
  IF (PrevErrPos < 0D) OR ((PrevErrPos + ErrorDistance) < TokenPos) THEN
    ErrorBlock(TokenPos, n);
    PrevErrPos := TokenPos; (* update prev error position *)
  END;
END Mark;


PROCEDURE GetCh();

BEGIN 
  IF NOT SourceInputFlag THEN
    ReadChar(source, ch)
  ELSE
    ch := SourceInputGetCharFunc();
  END;
  IF ch # 0C THEN (* if not end of file *)
    INC(SourcePos);
  END;
END GetCh;

  
PROCEDURE Diff(i, j: INTEGER): INTEGER;

BEGIN
  SETREG(0, LONGCARD(i));	(* D0 *)
  SETREG(1, LONGCARD(j));	(* D1 *)
  SETREG(8, IdBuf);		(* A0 *)
  INLINE(043F0H, 01000H);	(* lea    0(A0,D1.W),A1 *)
  INLINE(041F0H, 00000H);	(* lea    0(A0,D0.W),A0 *)
  INLINE(07000H);		(* moveq  #0,D0 *)
  INLINE(07E00H);		(* moveq  #0,D7 *)
  INLINE(01E11H);		(* move.B (A1),D7 *)
  INLINE(05347H);		(* subq.W #1,D7 *)
  INLINE(0B308H);		(* loop: cmp.B (A0)+,(A1)+ *)
  INLINE(056CFH, 0FFFCH);	(* dbne D7,loop *)
  INLINE(06706H);		(* beq.S exit *)
  INLINE(01020H);		(* move.B -(A0),D0 *)
  INLINE(09021H);		(* sub.B  -(A1),D0 *)
  INLINE(04880H);		(* ext.W D0 *)
 				(* exit: *)
END Diff;


(* NOTE: Modula-2 Version of Diff() function slower but more portable!
PROCEDURE Diff(i, j: INTEGER): INTEGER;

VAR k: CARDINAL;

BEGIN  k := ORD(IdBuf^[i]);
  LOOP
    IF k = 0 THEN RETURN 0
    ELSIF IdBuf^[i] # IdBuf^[j] THEN
      RETURN VAL(INTEGER,ORD(IdBuf^[i])) - VAL(INTEGER,ORD(IdBuf^[j]))
    ELSE
      INC(i); INC(j); DEC(k);
    END
  END
END Diff;
*)


PROCEDURE KeepId;

BEGIN 
  id := id1
END KeepId;

   
PROCEDURE String(termCh: CHAR);

VAR
  val       : CARDINAL; (* value of a char *)
  dgtcnt    : CARDINAL; (* number of digits *)
  dgt       : CHAR;     (* digit char *)
  storeChar : BOOLEAN;  (* store char or go through loop again *)

BEGIN
  IF DEBUG THEN
     WriteString("PROCEDURE M2SM.String(): ");
  END (*IF*);
  id1 := id + 1D;
  GetCh();
  LOOP
    storeChar := TRUE;
    IF ch = termCh THEN EXIT END ;
    IF ch < " " THEN Mark(45); EXIT END ;
    IF ch = 134C (* "\" *) THEN (* special char specification *)
      GetCh();
      (* If line ends with "\",LF then the next line is considered part of *)
      (* current line. *)
      IF ch = LF THEN GetCh(); storeChar := FALSE; (* check current char! *)
      ELSIF ch = "n" THEN ch := LF;
      ELSIF ch = "t" THEN ch := HT;
      ELSIF ch = "b" THEN ch := BS;
      ELSIF ch = "r" THEN ch := CR;
      ELSIF ch = "f" THEN ch := FF;
      ELSIF ch = "v" THEN ch := VT;
      ELSIF ch = "x" THEN (* Hexidecimal specification *)
        val := 0;
        dgtcnt := 2;
        GetCh(); dgt := CAP(ch);
        WHILE (((dgt >= "0") & (dgt <= "9")) OR ((dgt >= "A") & (dgt <= "F")))
              AND (dgtcnt > 0) DO
          IF (dgt <= "9") THEN (* "0".."9" *)
            val := (val * 16) + (CARDINAL(dgt) - CARDINAL("0"));
          ELSE (* "A".."F" *)
            val := (val * 16) + (CARDINAL(dgt) - 55);
          END;
          GetCh(); dgt := CAP(ch);
          DEC(dgtcnt);
        END;
        (**
	IdBuf^[id1] := CHR(val); 
	**)
	idBufPtr := IdBuf + id1;
	idBufPtr^ := CHR(val);
	IF DEBUG THEN 
	   (* WriteCh(IdBuf^[id1]); *)
	   WriteCh(CHAR(idBufPtr^)); 
	END (*IF*);
	INC(id1);
        storeChar := FALSE; (* check current char! *)

      ELSIF (ch >= "0") AND (ch <= "7") THEN (* Octal specification *)
        dgtcnt := 3; (* maximum number of octal digits *)
        val := 0;
        WHILE (dgtcnt > 0) AND ((ch >= "0") AND (ch <= "7")) DO
          val := (val * 8) + (CARDINAL(ch) - CARDINAL("0"));
          GetCh();
          DEC(dgtcnt);
        END;        
	(** IdBuf^[id1] := CHR(val); **)
        idBufPtr := IdBuf + id1;
	idBufPtr^ := CHR(val); 
	IF DEBUG THEN 
	   (* WriteCh(IdBuf^[id1]); *)
	   WriteCh(CHAR(idBufPtr^)); 
	END (*IF*);
	INC(id1);
        storeChar := FALSE; (* check current char! *)
      END;
      (* if not a special char then simply fell through *)
    END; (* IF "\" *)
    
    IF storeChar THEN
      (** IdBuf^[id1] := ch; **)
      idBufPtr := IdBuf + id1;
      idBufPtr^ := ch; 
      IF DEBUG THEN 
         (** WriteCh(IdBuf^[id1]); **)
         WriteCh(CHAR(idBufPtr^)); 
      END (*IF*);
      INC(id1);
      GetCh();
      IF id1 > IdBufLim THEN (* Identifier Buffer Overflow *)
        Mark(227);
        AbortCompilation;
      END;
    END;
  END; (* LOOP *)

  GetCh(); 
  (** IdBuf^[id] := CHR(id1-id); (*length*) **)
  idBufPtr := IdBuf + id;
  idBufPtr^ := CHR(id1-id); (*length*)
  (** IF IdBuf^[id] = 2C THEN **)
  idBufPtr := IdBuf + id;
  IF CHAR(idBufPtr^) = 2C THEN
    sym := number; 
    numtyp := 3; 
    (** intval := ORD(IdBuf^[id+1]); **)
    idBufPtr := IdBuf + id + 1D;
    intval := ORD(CHAR(idBufPtr^));
  ELSE 
    sym := string;
    (** IF IdBuf^[id] = 1C THEN  **)
    idBufPtr := IdBuf + id;
    IF CHAR(idBufPtr^) = 1C THEN  
      (*empty string*)      
      (** IdBuf^[id1] := 0C; **)
      idBufPtr := IdBuf + id1;
      idBufPtr^ := 0C; 
      INC(id1); 
      (** IdBuf^[id] := 2C; **)
      idBufPtr := IdBuf + id;
      idBufPtr^ := 2C;
    END;
  END;
  IF DEBUG THEN 
     WriteString("\n"); 
  END (*IF*);
END String;


PROCEDURE Identifier();

VAR 
  k, l, m: CARDINAL;
  notKW: BOOLEAN;
  uc: CHAR;
  idBufPtr1: ADDRESS;

BEGIN 
  IF DEBUG THEN
    WriteString("PROCEDURE M2SM.Identifier(): ");
  END (*IF*);
  id1 := id + 1D;
  IF id1 > IdBufLim THEN (* Identifier Buffer Overflow *)
    Mark(227);
    AbortCompilation;
    (* id1 := 1; *)
  END;

  IF DEBUG THEN
    WriteString(ch);
  END (*IF*);
  
  notKW := FALSE;
  REPEAT
    (* If Not UpperCase Then Can't Be A KeyWord! *)
    notKW := notKW OR (ch > "Z") OR (ch < "A");
    (** IdBuf^[id1] := ch; **)
    idBufPtr := IdBuf + id1;
    idBufPtr^ := ch; 
    INC(id1);
    GetCh();
    IF DEBUG THEN
       WriteCh(ch);
    END (*IF*);
    uc := CAP(ch);
  UNTIL (uc < "0") OR ("9" < uc) & (uc < "A") OR ("Z" < uc) & (uc # "_");
  IF DEBUG THEN
     WriteString("\n");
  END (*IF*);
  (** IdBuf^[id] := CHR(id1-id); (*Length*) **)
  idBufPtr := IdBuf + id;
  idBufPtr^ := CHR(id1-id); (*Length*)
    
  (* Heuristic test to avoid unnecessary KeyWord Searches! *)
  idBufPtr := IdBuf + id;
  idBufPtr1 := IdBuf + id + 1D;
  IF (notKW) OR
     (**
     (IdBuf^[id] > CHR(15)) OR 
     NOT (CARDINAL(IdBuf^[id+1])-CARDINAL("A") IN 
          KWTestSet[CARDINAL(IdBuf^[id])-2])
     **)
     (CHAR(idBufPtr^) > CHR(15)) OR 
     NOT (CARDINAL(idBufPtr1^)-CARDINAL("A") IN 
          KWTestSet[CARDINAL(idBufPtr^)-2])
  THEN
    sym := ident;
    RETURN; 
  END;

  k := 0; 
  l := KW;
  REPEAT 
    m := (k + l) DIV 2;
    IF Diff(id, keyTab[m].ind) <= 0 THEN 
       l := m ELSE k := m + 1 
    END
  UNTIL k >= l;
  IF (k < KW) & (Diff(id, keyTab[k].ind) = 0) THEN 
    sym := keyTab[k].sym
  ELSE 
    sym := ident
  END
END Identifier;


  PROCEDURE Ten(e: CARDINAL): REAL;
    VAR k: CARDINAL; u: REAL;
  BEGIN k := 0; u := FLOAT(1) (* 1.0 *);
    WHILE e > 0 DO
      IF ODD(e) THEN u := pow[k] * u END ;
      e := e DIV 2; INC(k);
    END ;
    RETURN u
  END Ten;

  PROCEDURE Number;
    VAR i, j, l, d, e, n: CARDINAL;
    x, f:   REAL;
    d0, d1, d2: LONGCARD;
    neg:    BOOLEAN;
    lastCh: CHAR;
    dig:    ARRAY [0..31] OF CHAR;

  BEGIN sym := number; i := 0;
    REPEAT dig[i] := ch; INC(i); GetCh()
    UNTIL ((ch < "0") OR ("9" < ch) & (CAP(ch) < "A") OR ("Z" < CAP(ch))) OR
          (i > HIGH(dig)); (* Check for overflow of "dig" array *)
    lastCh := ch; j := 0;
    WHILE (j < i) & (dig[j] = "0") DO INC(j) END ;
    IF ch = "." THEN GetCh();
      IF ch = "." THEN
        lastCh := 0C; ch := 177C (*ellipsis*)
      END
    END ;
    IF lastCh = "." THEN (*decimal point*)
      x := FLOAT(0) (* 0.0 *); l := 0;
      WHILE j < i DO (*read int part*)
        IF l < maxDig THEN
          IF dig[j] > "9" THEN Mark(40) END ;
          x := x * FLOAT(10) (* 10.0 *) + FLOAT(ORD(dig[j])-60B);
          INC(l);
        ELSE Mark(41)
        END;
        INC(j);
      END ;
      l := 0; f := FLOAT(0) (* 0.0 *);
      WHILE ("0" <= ch) & (ch <= "9") DO (*read fraction*)
        IF l < maxDig THEN
          f := f * FLOAT(10) (* 10.0 *) + FLOAT(ORD(ch)-60B);
          INC(l);
        END ;
        GetCh()
      END ;
      x := f / Ten(l) + x; e := 0; neg := FALSE;
      IF ch = "E" THEN GetCh();
        IF ch = "-" THEN
          neg := TRUE; GetCh()
        ELSIF ch = "+" THEN GetCh()
        END ;
        WHILE ("0" <= ch) & (ch <= "9") DO (*read exponent*)
          e := e * 10 + VAL(CARDINAL,ORD(ch))-60B;
          GetCh()
        END
      END ;
      IF neg THEN
        IF e <= maxNegExp THEN x := x / Ten(e) ELSE Mark(41) END
      ELSE
        IF e <= maxPosExp THEN f := Ten(e);
          IF MAX(REAL) / f >= x THEN x := f*x ELSE Mark(41) END
        ELSE Mark(41)
        END
      END ;
      numtyp := 4; realval := x
    ELSE (*integer*)
      lastCh := dig[i-1];
      IF lastCh = "B" THEN
        DEC(i); intval := 0; numtyp := 1;
        WHILE j < i DO
          d := ORD(dig[j]) - 60B;
          IF (d < 10B) & ((maxCard - d) DIV 10B >= intval) THEN
            intval := 10B * intval + d
          ELSE Mark(29); intval := 0
          END ;
          INC(j);
        END
      ELSIF lastCh = "H" THEN DEC(i);
        IF i <= j+4 THEN
          numtyp := 1; intval := 0;
          WHILE j < i DO
            d := ORD(dig[j]) - 60B;
            IF d > 26B THEN Mark(29); d := 0
               ELSIF d > 9 THEN DEC(d, 7);
            END ;
            intval := 10H * intval + d; INC(j);
          END
        ELSIF i <= j+8 THEN
          numtyp := 2; dblval := 0D;
          REPEAT d := ORD(dig[j]) - 60B;
            IF d > 26B THEN Mark(29); d := 0
               ELSIF d > 9 THEN DEC(d, 7);
            END ; (* no overflow check *)
            dblval := (LONGCARD(dblval) * 16D) + LONGCARD(d); INC(j);
          UNTIL j = i
        ELSE Mark(29); numtyp := 2
        END
      ELSIF lastCh = "D" THEN
        DEC(i); d1 := 0D; numtyp := 2;
        WHILE j < i DO
          d := ORD(dig[j]) - 60B;
          IF d < 10 THEN
            d2 := d1; (* current value *)
            INC(d1, d1); d0 := d1 + d1; d1 := d0 + d0 + d1 + LONGCARD(d);
            IF (d2 > d1) THEN Mark(29); END; (* Overflow Check *)
          ELSE Mark(29); d1 := 0D
          END ;
          INC(j);
        END ;
        dblval := d1
      ELSIF lastCh = "C" THEN
        DEC(i); intval := 0; numtyp := 3;
        WHILE j < i DO
          d := ORD(dig[j]) - 60B; intval := 10B * intval + d;
          IF (d >= 10B) OR (intval >= 400B) THEN
            Mark(29); intval := 0
          END ;
          INC(j);
        END
      ELSE (*decimal?*)
        numtyp := 1; intval := 0;
        WHILE j < i DO
          d := ORD(dig[j]) - 60B;
          IF (d < 10) & ((maxCard-d) DIV 10 >= intval) THEN
            intval := 10*intval + d
          ELSE Mark(29); intval := 0
          END ;
          INC(j);
        END
      END
    END
  END Number;

    PROCEDURE Comment;
    VAR cs: CHAR;
    BEGIN
      INC(CommentNesting);
      GetCh();
      IF (CommentNesting = 1) AND (ch = "$") THEN (* Compiler Switch! *)
       LOOP (* compiler switch loop *)
        GetCh(); (* compiler switch *)
        cs := CAP(ch); (* get compiler switch and force to upper case *)
        GetCh(); (* + or - char *)
        CASE (cs) OF
        | "R": (* Enable/Disable Range Checking *)
               rngchk := ch = "+";
        | "V": (* Enable/Disable Overflow Checking *)
               ovflchk := ch = "+";
        | "D": (* Enable/Disable Generating Code To Copy Dynamic Value Parms *)
               CopyDynValueParms := ch = "+";
        | "L": (* Enable/Disable Generating relocatable or SB relative code *)
               glbReloc := ch = "+";
               IF glbReloc THEN (* relocatable mode *)
                 GlbParStartAdr := 8;
               ELSE (* SB relative mode *)
                 GlbParStartAdr := 12;
               END;
        ELSE (* Unknown Compiler Switch *)
          Mark(43);
        END;
        IF (ch # "+") AND (ch # "-") THEN (* + or - expected *)
          Mark(46);
        END;
        GetCh();
        IF ch # "," THEN (* no more compiler switches *)
          EXIT;
        END; 
       END; (* LOOP *)
      END;
      REPEAT
        WHILE (ch # "*") & (ch > 0C) DO
          IF ch = "(" THEN GetCh();
            IF ch = "*" THEN Comment END
          ELSE GetCh()
          END
        END ;
        GetCh()
      UNTIL (ch = ")") OR (ch = 0C);
      IF ch > 0C THEN GetCh() ELSE Mark(42) END;
      DEC(CommentNesting);
    END Comment;

  PROCEDURE GetSym;
  BEGIN
    (* Skip Over Any White Spaces (Space, LF, CR, TAB, ...) *)
    WHILE (ch <= " ") AND (ch # 0C) DO
      GetCh();
    END;

    TokenPos := SourcePos; (* error position *)

    CASE ch OF   (* " " < ch <= 177C *)
        "!"  : sym := null; GetCh() |
        '"'  : String('"') |
        "#"  : sym := neq; GetCh()  |
        "$"  : sym := null; GetCh() |
        "%"  : sym := null; GetCh() |
        "&"  : sym := and; GetCh()  |
        "'"  : String("'") |
        "("  : GetCh();
               IF ch = "*" THEN Comment; GetSym
                 ELSE sym := lparen
               END |
        ")"  : sym := rparen; GetCh()|
        "*"  : sym := times; GetCh() |
        "+"  : sym := plus; GetCh()  |
        ","  : sym := comma; GetCh() |
        "-"  : sym := minus; GetCh() |
        "."  : GetCh();
               IF ch = "." THEN GetCh(); sym := ellipsis
                 ELSE sym := period
               END |
        "/"  : sym := slash; GetCh() |
        "0".."9": Number |
        ":"  : GetCh();
               IF ch = "=" THEN GetCh(); sym := becomes
                 ELSE sym := colon
               END |
        ";"  : sym := semicolon; GetCh() |
        "<"  : GetCh();
               IF ch = "=" THEN GetCh(); sym := leq
                 ELSIF ch = ">" THEN GetCh(); sym := neq
                 ELSE sym := lss
               END |
        "="  : sym := eql; GetCh()   |
        ">"  : GetCh();
               IF ch = "=" THEN GetCh(); sym := geq
                 ELSE sym := gtr
               END |
        "?"  : sym := null; GetCh()  |
        "@"  : sym := null; GetCh()  |
        "A".."Z": Identifier       |
        "["  : sym := lbrak; GetCh() |
        134C : sym := null; GetCh()  | (* "\" *)
        "]"  : sym := rbrak; GetCh() |
        "^"  : sym := arrow; GetCh() |
        "_"  : Identifier          | (* Allows "_" in identifier *)
        "`"  : sym := null; GetCh()  |
        "a".."z": Identifier       |
        "{"  : sym := lbrace; GetCh()|
        "|"  : sym := bar; GetCh()   |
        "}"  : sym := rbrace; GetCh()|
        "~"  : sym := not; GetCh()   |

        (* This code is used by the procedure "Number" when it finds ".." *)
        177C : sym := ellipsis; GetCh() 
    ELSE (* ch = 0 OR ch > 127 = Invalid Character In File! *)
      sym := eof; GetCh();
    END
  END GetSym;


PROCEDURE Enter(name: ARRAY OF CHAR): INTEGER;

VAR j, l: INTEGER;

BEGIN
  IF DEBUG THEN
    WriteString("PROCEDURE M2SM.Enter(): ");
  END (*IF*);
  l := HIGH(name)+1;
  id1 := id;
  IF id1+LONGCARD(l) >= IdBufLeng THEN (* Identifier Buffer Overflow *)
    Mark(227);
    AbortCompilation;
  END;
  (** IdBuf^[id] := CHR(l); **)
  idBufPtr := IdBuf + id;
  idBufPtr^ := CHR(l); 
  IF DEBUG THEN
    (** WriteCh(IdBuf^[id]); **)
    WriteCh(CHAR(idBufPtr^));
  END (*IF*);
  INC(id);
  FOR j := 0 TO l-1 DO 
     (** IdBuf^[id] := name[j]; **)
     idBufPtr := IdBuf + id;
     idBufPtr^ := name[j]; 
     IF DEBUG THEN
       (** WriteCh(IdBuf^[id]); **)
       WriteCh(CHAR(idBufPtr^));
     END (*IF*);
     INC(id); 
  END;
  IF DEBUG THEN
    WriteString("\n");
  END (*IF*);
  RETURN id1;
END Enter;


  (* This procedure is called every time a new file is to be compiled *)
  PROCEDURE InitScanner(VAR filename: ARRAY OF CHAR);
    VAR i: CARDINAL; c: CHAR;
  BEGIN 
    ch := " ";
    PrevErrPos := -1D;
    SourcePos := 0D;
    TokenPos := 0D;
    CommentNesting := 0;
    scanerr := FALSE;
    id := id0;
    WriteChar(errDat, FNM);
    i := 0;
    REPEAT
      c := filename[i];
      INC(i);
      WriteChar(errDat, c);
    UNTIL (c = 0C);
  END InitScanner;

  
PROCEDURE EnterKW(sym: Symbol; name: ARRAY OF CHAR);

VAR l, L: CARDINAL;

BEGIN (* No Check For Identifier Buffer Overflow *)
  IF DEBUG THEN
    WriteString("PROCEDURE M2SM.EnterKW(): ");
  END (*IF*);
  keyTab[K].sym := sym;
  keyTab[K].ind := id;
  l := 0; 
  L := HIGH(name);
  (** IdBuf^[id] := CHR(L+1); **)
  idBufPtr := IdBuf + id;
  idBufPtr^ := CHR(L+1); 
  IF DEBUG THEN
    (** WriteCh(IdBuf^[id]); **)
    WriteCh(CHAR(idBufPtr^));
  END (*IF*);
  INC(id);
  WHILE l < L DO
    (** IdBuf^[id] := name[l]; **)
    idBufPtr := IdBuf + id;
    idBufPtr^ := name[l];
    IF DEBUG THEN
      (** WriteCh(IdBuf^[id]); **)
      WriteCh(CHAR(idBufPtr^));
    END (*IF*);
    INC(id); 
    INC(l);
  END;
  INC(K);

  (* Initialize KWTestSet Array *)
  INCL(KWTestSet[L-1], CARDINAL(name[0])-CARDINAL("A"));
  IF DEBUG THEN
    WriteString("\n");
  END (*IF*);
END EnterKW;

  
PROCEDURE M2SMModuleBody;
  BEGIN
  K := 0; 
  (** IdBuf^[0] := 1C; **)
  IdBuf^ := 1C; 
  id := 1;
  pow[0] := FLOAT(10)       (* 1.0E1 *);
  pow[1] := pow[0] * pow[0] (* 1.0E2 *);
  pow[2] := pow[1] * pow[1] (* 1.0E4 *);
  pow[3] := pow[2] * pow[2] (* 1.0E8 *);
  pow[4] := pow[3] * pow[3] (* 1.0E16 *);
  pow[5] := pow[4] * pow[4] (* 1.0E32 *);
  EnterKW(by,"BY");
  EnterKW(do,"DO");
  EnterKW(if,"IF");
  EnterKW(in,"IN");
  EnterKW(of,"OF");
  EnterKW(or,"OR");
  EnterKW(to,"TO");
  EnterKW(and,"AND");
  EnterKW(div,"DIV");
  EnterKW(end,"END");
  EnterKW(for,"FOR");
  EnterKW(mod,"MOD");
  EnterKW(not,"NOT");
  EnterKW(rem,"REM");
  EnterKW(set,"SET");
  EnterKW(var,"VAR");
  EnterKW(case,"CASE");
  EnterKW(code,"CODE");
  EnterKW(else,"ELSE");
  EnterKW(exit,"EXIT");
  EnterKW(from,"FROM");
  EnterKW(loop,"LOOP");
  EnterKW(then,"THEN");
  EnterKW(type,"TYPE");
  EnterKW(with,"WITH");
  EnterKW(array,"ARRAY");
  EnterKW(begin,"BEGIN");
  EnterKW(const,"CONST");
  EnterKW(elsif,"ELSIF");
  EnterKW(until,"UNTIL");
  EnterKW(while,"WHILE");
  EnterKW(export,"EXPORT");
  EnterKW(import,"IMPORT");
  EnterKW(module,"MODULE");
  EnterKW(record,"RECORD");
  EnterKW(repeat,"REPEAT");
  EnterKW(return,"RETURN");
  EnterKW(forward,"FORWARD");
  EnterKW(pointer,"POINTER");
  EnterKW(procedure,"PROCEDURE");
  EnterKW(qualified,"QUALIFIED");
  EnterKW(definition,"DEFINITION");
  EnterKW(implementation,"IMPLEMENTATION");
  id0 := id;
  END M2SMModuleBody;


END M2SM.
============================================================
    IF DEBUG THEN
    END (*IF*);
============================================================
