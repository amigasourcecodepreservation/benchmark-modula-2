(* Name   : M2DM.DEF
 * Changed: 02/19/87 LF
 * Notes :
 * 1. Added new variable "longwordtyp" which is a new type defined in SYSTEM
 *    as "LONGWORD" this type is undefined and occupied 4 bytes.
 * 2. The type Set now comes in 3 sizes Byte,Word,LongWord depending on the
 *    number of elements. The ConstValue: Set: s0, S : BITSET represent a 32-bit
 *    set constant. 
 * 2. The heap is now allocated in another module, to initialize the heap
 *    management variables the procedure "InitHeap" is called.
 * 3. Added extra field to "Key" record. This new field is a flag indicating
 *    if the module the key is associated with was explicitly imported or
 *    imported as a result of importing another module!
 * 4. Remove procedure "Available" from module, useless.
 * 5. Changed MinInt to -32768.
 *)

(*$L+  relocatable *)
DEFINITION MODULE M2DM;

  FROM SYSTEM IMPORT ADDRESS, WORD;

  CONST   WordSize   =     16;
          MaxChar    =   377C;
          MaxCard    =  65535;
          MinInt     = VAL(INTEGER,8000H); (* -32768 *)
          MaxInt     =  32767;
          NilVal     =     0D;

  TYPE    ObjPtr     = POINTER TO Object;
          StrPtr     = POINTER TO Structure;
          ParPtr     = POINTER TO Parameter;
          PDPtr      = POINTER TO PDesc;
          KeyPtr     = POINTER TO Key;

          ObjClass   = (Header, Const, Typ, Var, Field, Proc, Code, Module, Temp);

          (* Note: for scalar types:     form <= Double        *)
          (*       for subrange types:   RBaseTyp^.form < form *)
          (*       for structured types: form > Opaque         *)
          StrForm    = (Undef, Bool, Char, Card, Int, Enum, LCard, Double,
                        Range, Real, LongReal, Pointer, Set, ProcTyp, Opaque,
                        String, Array, Record);

          Standard   = (Abs, Adr, Cap, Chr, Dec, Excl, Float, FloatD, Halt,
                        High, Inc, Incl, Inline, Long, Max, Min, Odd, Ord,
                        Reg, Setreg, Shift, Short, Size, Tsize, Trunc, TruncD, 
                        Val, NonStand);

          ConstValue = RECORD
                         CASE :StrForm OF
                           Undef:               U:  LONGINT;
                         | Bool:    b0, b1, b2, B:  BOOLEAN;
                         | Char:    h0, h1, h2, Ch: CHAR;
                         | Card:            c0, C:  CARDINAL;
                         | Int, Enum:       i0, I:  INTEGER;
                                             prev:  ObjPtr;
                         | LCard, Double:       D:  LONGINT;
                         | Real:                R:  REAL;
                         (* s0:S - 32 bit set *)
                         | Set:             s0, S:  BITSET;
                         (* D0 = Adr, D1 = Length, D2 = id array index
                          * IF D0 = -1 THEN it has not been output to the
                          * constant area!
                          *)
                         | String:  D0, D1, D2, D3: INTEGER;
                         END;
                       END;

          PDesc      = RECORD
                         forward, exp: BOOLEAN;
                         num, lev, adr, size, endAdr: INTEGER;
                       END;

          Object     = RECORD
                         name:     INTEGER;  (* index to name buffer *)
                         typ:      StrPtr;
                         left,
                         right,
                         next:     ObjPtr;
                         exported: BOOLEAN;
                         CASE class: ObjClass OF
                           Header: kind:       ObjClass;  (* Typ, Proc, Module *)
                                   last, heap: ObjPtr;
                                   withadr:    INTEGER;
                         | Const:  conval:     ConstValue;
                         | Typ:    mod:        ObjPtr;
                         | Var:    varpar:     BOOLEAN;
                                   vmod, vlev,
                                   vadr:       INTEGER;
                         | Field:  offset:     INTEGER;
                         | Proc:   pd:         PDPtr;
                                   firstParam: ParPtr;
                                   firstLocal: ObjPtr;
                                   pmod:       INTEGER;
                         | Code:   cd:         PDPtr;
                                   firstArg:   ParPtr;
                                   std:        Standard;
                                   cnum:       CARDINAL;
                         | Module: key:        KeyPtr;
                                   firstObj,
                                   root:       ObjPtr;
                                   modno:      INTEGER;
                         | Temp:   baseref:    INTEGER;
                         END;
                       END;

          Structure  = RECORD
                         strobj: ObjPtr;  (* object (type) naming structure *)
                         size, ref: INTEGER;
                         CASE form: StrForm OF
                           Undef, Bool, Char, Card, Int, LCard, Double,
                           Real, LongReal, String:  (* no further field *)
                         | Enum:    ConstLink: ObjPtr;
                                    NofConst:  INTEGER;
                         | Range:   RBaseTyp:  StrPtr;
                                    min, max,
                                    BndAdr:    INTEGER;
                         | Pointer: PBaseTyp:  StrPtr;
                                    BaseId:    INTEGER;  (* forward refs *)
                         | Set:     SBaseTyp:  StrPtr;
                         | Array:   ElemTyp,
                                    IndexTyp:  StrPtr;
                                    dyn:       BOOLEAN;
                         | Record:  firstFld:  ObjPtr;
                         | ProcTyp: firstPar:  ParPtr;
                                    resTyp:    StrPtr;
                         | Opaque:  (* no field *)
                         END;
                       END;

          Parameter  = RECORD
                         name: INTEGER; varpar: BOOLEAN; typ: StrPtr; next: ParPtr;
                       END;
          Key        = RECORD
                         k0, k1, k2: INTEGER;
                         (* Module Directly(TRUE) or Indirectly(FALSE) Import *)
                         DirectImport: BOOLEAN;
                       END;

  VAR     mainmod, sysmod: ObjPtr;
          notyp, undftyp, booltyp, chartyp, cardtyp, inttyp, bitstyp,
          lcardtyp, dbltyp, realtyp, lrltyp, proctyp, stringtyp, addrtyp,
          wordtyp, bytetyp, longwordtyp: StrPtr;

          HeapBasePtr : ADDRESS;  (* Ptr to base of heap *)
          HeapSize    : LONGCARD; (* Size of heap in bytes *)

  PROCEDURE ALLOCATE(VAR a: ADDRESS; n: CARDINAL);
  PROCEDURE ResetHeap(a: ADDRESS);
  PROCEDURE InitHeap;

END M2DM.
