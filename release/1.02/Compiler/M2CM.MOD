(* Name   : M2CM.MOD
 * Changed: 01/06/88 LF
 * Notes :
 * 1. Added handling for "longwordtyp" in the following procedures.
 *    GenAssign - In the handling of Undef type added longwordtyp.
 *    GenParam - In the handling of bytetyp, wordtyp added longwordtyp.
 * 2. In the procedure "GenEnterMod" the imported module initialization
 *    loop is commented out, because the initialization of library modules
 *    is now handled by the linker!!
 * 3. Added handling for "ARRAY OF WORD" and "ARRAY OF LONGWORD" to the
 *    current handling of "ARRAY OF BYTE". This allows the actual parameter
 *    to be any type when one of the above 3 formal parms are specified!
 *    GenAssign - CASE variant "Array" added to word,longword to byte handling!
 *    GenParam - CASE variant "Array" added to word,longword to byte handling!
 * 4. Fixed bug: in procedures "GenCFJ","GenCBJ" procMd was not taken into
 *    acount in the logic. This allowed statements like flag := Test; to pass
 *    through when flag is a boolean and Test is a functions which returns
 *    a boolean.
 * 5. Added handling for LCard and Double as control variables in a FOR loop.
 *    This required changing "GenFor1","GenFor2","GenFor3","GenFor4". Also
 *    added code to handle type checking of BY <ConstExpr>.
 * 6. Change: The return parameter from a function call now returns the
 *    result in register D0, rather than on top of stack. The procedure
 *    "SpaceForFunciton" has been eliminated since it is of no use now.
 *    The procedure "RestoreResultAndRegs" is changed to handle return parm
 *    in register D0. Procedure "PrepCall" changed to perform no handling of
 *    the return parameter. The procedure "GenResult" changed to load result
 *    into D0. Or in case of double-longword result into D0/D1.
 * 7. Updated compiler version.
 * 8. Changed procedure "GenAssign" Array type, check length of string against
 *    dimmensions of array string is being assigned to.
 * 9. Bug fix: procedure "GenAssign" Array type, the last ELSIF should contain
 *    a check for dyn = FALSE because dynamic array parameters should not
 *    be assgined string literals etc... 
 * 10.Bug fix: procedure "GenCall" did not properly handle calls to a non
 *    procedure object, this occures when source program has a call of a
 *    non procedure type.
 * 11.Bug fix: procedure "GenResult" did not set the corect result type
 *    when doing an assignment to register D0.
 * 12.Turned off compiler switch "D".
 * 13.Changed procedure "RestoreResultAndRegs" to restore registers first
 *    then get the result. Also the position of the instruction "MOVE D0,Dn"
 *    is remembered in the variable "pcLastMoveD0toDn" so that it may possibly
 *    be elliminated later.
 * 14.Changed procedure "GenAssign" to check if (pcLastMoveD0toDn) was the
 *    previous instruction and may safely be elliminated.
 * 15.Bug fix: procedure "GenFor1" to perform the assignment of the FOR loop
 *    initialization expression. The assigment used to be performed in procedure
 *    "GenFor2" which caused problems.
 * 16.Bug fix: negative step loops were improperly handled
 * 17.Bug fix: initializer and limit expressions must be compatible with
 *             the control variable.
 * 18.Bug fix: GenAssign() fix procedure type problem!
 * 19.Change: GenAssign() allow assignment of constant strings to
 *    fixed size arrays ARRAY [0..2] OF CHAR; := "ABC". No room for null char
 *    but the compiler will not complain about this!
 * 20.Change: GenAssign() allow assignment of a CHAR variable to an
 *    ARRAY OF CHAR parmater.  This is consistent with Wirth's 3d edition!
 * 21.Change: GenParam() to allow assign of a CHAR variable to a VAR
 *    ARRAY OF CHAR parameter. This is consistent with Wirth's 3d edition!
 *)

(*$L+,D- relocatable *)
IMPLEMENTATION MODULE M2CM;

  FROM SYSTEM IMPORT
    WORD;
  FROM M2DM IMPORT
    WordSize, MaxInt, ObjPtr, StrPtr, ParPtr, PDPtr,
    ObjClass, StrForm, PDesc, Object,
    notyp, undftyp, booltyp, chartyp, cardtyp, inttyp, bitstyp, 
    lcardtyp, dbltyp, realtyp, lrltyp, proctyp, stringtyp, addrtyp, 
    wordtyp, bytetyp, longwordtyp;
  FROM M2SM IMPORT
    Mark;
  FROM M2HM IMPORT
    D0, D1, SB, MP, SP,
    byte, word, long,
    Condition, RegType, Register, WidType, ItemMode, Item,
    WordVal, LongVal, SignedT, SimpleT, RealT, GetReg, GetRegPair, Release,
    ReleaseReg, SaveRegs, RestoreRegs, SetlocMd, SetregMd, SetstkMd, SetconMd,
    StackTop, SetupSL, GenHalt,
    LoadAdr, LoadD, LoadX, Move, MoveAdr, MoveBlock, 
    Neg1, Tst1, Add2, Cmp2,
    CheckClimit, CheckRange, CheckDbltoSingle, DynArray, 
    Jf, Jb, EnterCase, ExitCase, Link, Unlink, CallInt, CallExt, CallInd,
    EnterModule, ExitModule, InitModule, ovflchk, pcLastProcResult;
  FROM M2FM IMPORT
    FMove, LoadF, FLong, FShort;
  FROM M2LM IMPORT
    codeB, pc, maxP, maxM, PutWord, AllocChar, FixLink, FixLinkWith, fixup;

  VAR sp0, sp : CARDINAL;


  PROCEDURE err(n: CARDINAL);
    (* local synonym for M2SM.Mark to save space! *)
  BEGIN 
    Mark(n);
  END err;

  PROCEDURE Put16(w : WORD);
    (* local synonym for M2LM.PutWord to save space! *)
  BEGIN
    PutWord(w);
  END Put16;
 
  PROCEDURE SRTest(VAR x : Item);
  BEGIN
    WITH x DO
      IF typ^.form = Range THEN typ := typ^.RBaseTyp END;
    END (*WITH*);
  END SRTest;
  
  PROCEDURE setCC(VAR x : Item; fcc : Condition);
  BEGIN
    Release(x);
    WITH x DO
      typ := booltyp; mode := cocMd; CC := fcc;
      Tjmp := 0; Fjmp := 0;
    END;
  END setCC;
  
  PROCEDURE GenAssign(VAR x, y : Item);
    (*       x    :=    y     *)
    (*       y  ---->>  x     *)
    (* or    g  ---->>  f     *)
    VAR f, g, h : StrForm;
        xp, yp : ParPtr;
        x0, y0 : Item;
        Dn : Register;
        s, sadr,i : INTEGER;
        L : CARDINAL;
        sz : WidType;
        xt : StrPtr;
  BEGIN
    (* if procedure identifier then left-side must be a procedure variable. *)
    IF (y.mode = procMd) AND (x.typ^.form # ProcTyp) THEN err(133); END;

    IF (x.mode = conMd) OR (x.mode > DregMd) THEN err(134) END; (* added *)
    SRTest(y);

    (* Peephole optimizer! *)
    (* Elliminate redundant MOVE.L Result,Dn which is generated after each func
     * call(included system related functions calls). If the last instruction
     * was MOVE.L Result(D0 or D1),Dn then backup the "pc" by 2 bytes and 
     * store directly from Result to destination. *)
    IF (pcLastProcResult = pc-2) AND (y.mode = DregMd) THEN
      Release(y);
      DEC(pc, 2);
      (* Extract Source Register Field From MOVE instruction (D0 or D1) *)
      y.R := codeB^[pc DIV 2] MOD 8;
    END;



    f := x.typ^.form;
    h := f; (* holds original form of x during GenAssign *)
    g := y.typ^.form;
    IF f = Range THEN
      (* perform range check. *)
      WITH x DO
        IF y.mode = conMd THEN
          IF (VAL(INTEGER,WordVal(y)) < typ^.min) OR
             (VAL(INTEGER,WordVal(y)) > typ^.max) THEN
            err(138)
          END
        ELSE
          CheckRange(y, typ^.min, typ^.max);
        END;
        typ := typ^.RBaseTyp;
        f := typ^.form;
      END (*WITH*);
    END (*f = Range*);
    xt := x.typ; (* hold original type of x *)
  
    CASE f (* destination form *) OF
      (* Added support for longwordtyp *)
      Undef :    IF ((x.typ = longwordtyp) & (y.typ^.size = 4)) OR
                    ((x.typ = wordtyp) & (y.typ^.size = 2)) OR
                    ((x.typ = bytetyp) & (y.typ^.size = 1)) THEN
                   Move(y,x)
                 ELSE err(133)
                 END;
  
    | Bool :     IF g = Bool THEN Move(y,x)
                 ELSE err(133)
                 END;
  
    | Char :     IF g = Char THEN Move(y,x)
                 ELSE err(133)
                 END;
  
    | Card :     IF g = Card THEN Move(y,x)
                 ELSIF g = Int THEN
                   IF y.mode = conMd THEN
                     IF y.val.I < 0 THEN err(132) END;
                   ELSE
                     (* emit check : *) 
                     IF h <> Range THEN CheckClimit(y, MaxInt) END;
                   END;
                   Move(y,x)
                 ELSIF (g = Double) OR (g = LCard) THEN
                   LoadD(y); (* must be loaded long! *)
                   CheckDbltoSingle(y,x);
                   y.typ := x.typ;
                   Move(y,x)
                 ELSE err(133)
                 END;
  
    | Int :      IF g = Int THEN Move(y,x)
                 ELSIF g = Card THEN
                   IF y.mode = conMd THEN
                     IF y.val.C > MaxInt THEN err(208) END;
                   ELSE
                     (* emit check : *) 
                     IF h <> Range THEN CheckClimit(y, MaxInt) END;
                   END;
                   Move(y,x)
                 ELSIF (g = Double) OR (g = LCard) THEN
                   LoadD(y); (* must be loaded long! *)
                   CheckDbltoSingle(y,x);
                   y.typ := x.typ;
                   Move(y,x)
                 ELSE err(133)
                 END;
  
    | Double,
      LCard :    IF ((g = Double) OR (g = LCard)) OR
                    ((x.typ = addrtyp) & (g = Pointer)) THEN Move(y,x)
                 ELSIF (g = Int) OR (g = Card) THEN
                   LoadX(y,long);
                   y.typ := x.typ;
                   Move(y,x)
                 ELSE err(133)
                 END;
  
    | Real :     IF g = Real THEN FMove(y,x)
                 ELSIF g = LongReal THEN
                   FShort(y);
                   y.typ := x.typ;
                   FMove(y,x)
                 ELSE err(133)
                 END;
  
    | LongReal : IF g = LongReal THEN FMove(y,x)
                 ELSIF g = Real THEN
                   FLong(y);
                   y.typ := x.typ;
                   FMove(y,x)
                 ELSE err(133)
                 END;
  
    | Range :    (* must not occur here! *)
  
    | Enum :     IF x.typ = y.typ THEN Move(y,x)
                 ELSE err(133)
                 END;
  
    | Set :      IF x.typ = y.typ THEN Move(y,x)
                 ELSE err(133)
                 END;
  
    | Pointer,
      Opaque :   IF (x.typ = y.typ) OR (y.typ = addrtyp) THEN Move(y,x)
                 ELSE err(133)
                 END;
  
    | Record :   IF x.typ = y.typ THEN
                   s := x.typ^.size;
                   MoveBlock(y,x,s,FALSE)
                 ELSE err(133)
                 END;
  
    | ProcTyp :  IF y.mode = procMd THEN
                   (* procedure-constant to procedure-variable : *)
                   IF y.proc^.pd^.lev <> 0 THEN err(127)
                   ELSIF x.typ^.resTyp <> y.proc^.typ THEN err(128)
                   ELSE xp := x.typ^.firstPar; yp := y.proc^.firstParam;
                     WHILE xp <> NIL DO
                       IF yp <> NIL THEN
                         IF (xp^.varpar <> yp^.varpar) OR
                            ((xp^.typ <> yp^.typ) AND
                            ((xp^.typ^.form <> Array) OR
                             NOT xp^.typ^.dyn OR
                             (yp^.typ^.form <> Array) OR
                             NOT yp^.typ^.dyn OR
                             (xp^.typ^.ElemTyp <> yp^.typ^.ElemTyp))) THEN
                           err(129)
                         END;
                         yp := yp^.next
                       ELSE err(130)
                       END;
                       xp := xp^.next
                     END (*WHILE*);
                     IF yp <> NIL THEN err(131) END;
                     MoveAdr(y,x);
                   END;
                 ELSIF x.typ = y.typ THEN Move(y,x)
                 ELSE err(133)
                 END;
  
    | Array :    s := x.typ^.size;
                 IF (x.typ = y.typ) & NOT(x.typ^.dyn) THEN
                   MoveBlock(y,x,s,FALSE)
                 ELSIF (x.mode = stkMd) & x.typ^.dyn THEN
                   (* formal parameter is dynamic array : *)
                   IF (g = Array) & (x.typ^.ElemTyp = y.typ^.ElemTyp) THEN
                     DynArray(x,y)
                   ELSE
                     IF (x.typ^.ElemTyp = chartyp) OR
                        (x.typ^.ElemTyp = bytetyp) OR  (* Added *)
                        (x.typ^.ElemTyp = wordtyp) OR  (* Added *)
                        (x.typ^.ElemTyp = longwordtyp) (* Added *) THEN
                       IF g = String THEN
                         DynArray(x,y)
                       ELSIF (g = Char) & (y.mode = conMd) THEN
                         (* character-constant to dynamic array : *)
                         AllocChar(y.val.Ch, sadr);
                         WITH y DO
                           typ := stringtyp; val.D0 := sadr; val.D1 := 1;
                         END (*WITH*);
                         DynArray(x,y)
                       ELSIF (g = Char) THEN (* Char var to dyn array *)
                         DynArray(x,y)
                       ELSIF (x.typ^.ElemTyp = bytetyp) OR 
                             (x.typ^.ElemTyp = wordtyp) OR  (* Added *)
                             (x.typ^.ElemTyp = longwordtyp) (* Added *)
                             THEN DynArray(x,y)
                       ELSE err(133)
                       END
                     ELSE err(133)
                     END
                   END
                 ELSIF (x.typ^.ElemTyp = chartyp) & NOT(x.typ^.dyn) THEN
                   (* calculate dimmensions of array *)
                   i := x.typ^.IndexTyp^.max - x.typ^.IndexTyp^.min + 1; 
                   IF g = String THEN
                     (* string to fixed-size array : *)
                     (* check length of string against array length *)
                     (* y.val.D1 = 1 for null-string *)
                     IF (y.val.D1 # 1) AND (y.val.D1 > i) THEN err(146); END;
                     MoveBlock(y,x,i,TRUE);
                   ELSIF (g = Char) & (y.mode = conMd) THEN
                     (* character-constant to fixed-size array : *)
                     AllocChar(y.val.Ch, sadr);
                     WITH y DO
                       typ := stringtyp; val.D0 := sadr; val.D1 := 1;
                     END (*WITH*);
                     MoveBlock(y,x,i,TRUE);
                   ELSE err(133)
                   END;
                 ELSE err(133)
                 END;
  
    | String :   err(133); (* must not occur on the left side *)
  
    END (*CASE f*);
    x.typ := xt; (* restore original type of x *)
    Release(y);
    Release(x);
  END GenAssign;

  PROCEDURE GenFJ(VAR loc: CARDINAL);
  BEGIN
    Jf(T, loc);
  END GenFJ;

  PROCEDURE GenCFJ(VAR x: Item; VAR loc: CARDINAL);
  BEGIN
    IF x.typ = booltyp THEN
      IF x.mode < cocMd THEN Tst1(x); setCC(x, EQ) END;
      (* Added the following test to catch procMd and codMd! *)
      IF x.mode > cocMd THEN setCC(x, EQ); err(135); END;
    ELSE
      setCC(x, EQ); err(135);  (* type of expression must be boolean *)
    END;
    loc := x.Fjmp; Jf(x.CC, loc); FixLink(x.Tjmp);
  END GenCFJ;

  PROCEDURE GenBJ(loc: CARDINAL);
  BEGIN
    Jb(T, loc);
  END GenBJ;

  PROCEDURE GenCBJ(VAR x: Item; loc: CARDINAL);
  BEGIN
    IF x.typ = booltyp THEN
      IF x.mode < cocMd THEN Tst1(x); setCC(x, EQ) END;
      (* Added the following test to catch procMd and codMd! *)
      IF x.mode > cocMd THEN setCC(x, EQ); err(135); END;
    ELSE
      setCC(x, EQ); err(135);  (* type of expression must be boolean *)
    END;
    Jb(x.CC, loc); FixLinkWith(x.Fjmp, loc); FixLink(x.Tjmp);
  END GenCBJ;

  PROCEDURE PrepCall(VAR x: Item; VAR fp: ParPtr; VAR regs: BITSET);
  BEGIN
    WITH x DO
      IF (mode = procMd) OR (mode = codMd) THEN
        fp := proc^.firstParam;
      ELSIF typ^.form = ProcTyp THEN
        fp := typ^.firstPar;
      ELSE
        fp := NIL;
        err(136);  (* call of an object which is not a procedure *)
      END;
      SaveRegs(regs);
    END (*WITH*); 
  END PrepCall;

  PROCEDURE GenParam(VAR ap: Item; f: ParPtr);
    VAR fp: Item;
  BEGIN
    SetstkMd(fp, f^.typ);
    IF f^.varpar THEN
      IF (fp.typ^.form = Array) & fp.typ^.dyn & 
         ((fp.typ^.ElemTyp = bytetyp) OR
          (fp.typ^.ElemTyp = wordtyp) OR   (* Added *)
          (fp.typ^.ElemTyp = longwordtyp)) (* Added *) THEN DynArray(fp, ap);
      ELSIF (fp.typ^.form = Array) & fp.typ^.dyn &
            (ap.typ^.form = Array) & (ap.typ^.ElemTyp = fp.typ^.ElemTyp) THEN
        DynArray(fp, ap);
      ELSIF (ap.typ = fp.typ) OR
        (* Added support for longword typ *)
        (fp.typ = longwordtyp) & (ap.typ^.size = 4) OR
        (fp.typ = wordtyp) & (ap.typ^.size = 2) OR
        (fp.typ = bytetyp) & (ap.typ^.size = 1) OR
        (fp.typ = addrtyp) & (ap.typ^.form = Pointer) THEN
        MoveAdr(ap, fp);
      ELSIF (fp.typ^.form = Array) & (fp.typ^.dyn) &
            (fp.typ^.ElemTyp = chartyp) &
            (ap.typ^.form = Char) & (ap.mode # conMd) THEN
          (* Allow a CHAR variable to be assigned to a VAR s: ARRAY OF CHAR *)
        DynArray(fp, ap);
      ELSE
        err(137);  (* type of VAR par is not identical to that of actual par *)
      END;
    ELSE
      GenAssign(fp, ap);  (* type check in GenAssign *)
    END;
    Release(ap);
  END GenParam;

  PROCEDURE RestoreResultAndRegs(VAR x : Item; regs : BITSET);
    VAR y : Item; sz : CARDINAL; Dn: Register;
  BEGIN
    IF regs # {} THEN RestoreRegs(regs); END;

    (* Caution: saved registers remain busy, so the GetReg() *)
    (* -------  below gets a pool-register which is NOT in   *)
    (*          the set of the registers to be restored.     *)         
    WITH x DO
      (* result in reg D0, must be copied to a pool register *)
      sz := VAL(CARDINAL,typ^.size);
      IF NOT (sz IN {1,2,4,8}) THEN err(200); END;
      IF sz IN {1,2,4} THEN (* byte/word/long result D0 *)
        GetReg(Dn,Dreg);
        SetregMd(x, Dn, typ);
        SetregMd(y, D0, typ);
        pcLastProcResult := pc; (* Remember last usage of MOVE.L D0,Dn *)
        Move(y,x);
      ELSE (* sz = 8, double-longword result D0/D1 *)
        GetRegPair(Dn);
        mode := fltMd;
        FR := Dn;
        y.typ  := typ;
        y.mode := fltMd;
        y.FR   := D0; (* D0/D1 *)
        FMove(y,x);
      END;
    END;
  END RestoreResultAndRegs;

  PROCEDURE GenCall(VAR x: Item; regs: BITSET);
    VAR pd: PDPtr; y, z: Item;
  BEGIN
    WITH x DO
      IF mode = procMd THEN
        pd := proc^.pd;
        IF pd^.adr <> 0 THEN  (* module internal call *)
          IF pd^.lev > 0 THEN SetupSL(pd^.lev) END;
          CallInt(proc);
        ELSE  (* external call *)
          CallExt(proc);
        END;
        typ := proc^.typ;
      ELSIF (mode < conMd) & (typ^.form = ProcTyp) THEN
        CallInd(x);
        typ := typ^.resTyp;
      ELSIF mode = codMd THEN
        Put16(proc^.cnum); typ := proc^.typ;
      ELSE (* only gets here if invalid item "x" *)
        typ := notyp;
      END;
      IF typ <> notyp THEN  (* function call *)
        RestoreResultAndRegs(x,regs)
      ELSE (* procedure call *)
        RestoreRegs(regs)
      END;
    END (*WITH*);
  END GenCall;

  PROCEDURE GenEnter(VAR l: CARDINAL; lev: CARDINAL);
  BEGIN
    Link(l, lev); sp := 0; sp0 := 0;
  END GenEnter;

  PROCEDURE GenResult(VAR x: Item; proc: ObjPtr; VAR l: CARDINAL);
    VAR res: Item; sz: CARDINAL;
  BEGIN
    IF x.typ <> notyp THEN (* function *)
      sz := VAL(CARDINAL, proc^.typ^.size);
      IF NOT (sz IN {1,2,4,8}) THEN err(200); END;
      IF sz IN {1,2,4} THEN (* byte/word/long load result into D0 *)
        SetregMd(res, D0, proc^.typ)
      ELSE (* sz = 8, double-longword load result into D0/D1 *)
        res.typ  := proc^.typ;
        res.mode := fltMd;
        res.FR   := D0; (* D0/D1 *)
      END;
      GenAssign(res, x);
    END;
    GenFJ(l);
  END GenResult;

  PROCEDURE GenReturn(proc: ObjPtr; l: CARDINAL);
  BEGIN
    IF proc^.typ # notyp THEN GenHalt(2) END;  (* function *)
    IF l # 0 THEN FixLink(l) END;
    Unlink(proc^.pd^.size, proc^.pd^.lev);
  END GenReturn;

  PROCEDURE GenCase1(VAR x: Item; VAR l0: CARDINAL);
  BEGIN
    SRTest(x);
    IF (x.typ^.form = Undef) OR (x.typ^.form > Enum) THEN
      err(83);  (* illegal type of case expression *)
    END;
    LoadX(x, word); ReleaseReg(x.R); l0 := 0; GenFJ(l0); INC(sp, 2);
  END GenCase1;

  PROCEDURE GenCase2;
  BEGIN
    ExitCase;
  END GenCase2;

  PROCEDURE GenCase3(VAR x: Item; l0, l1, n: CARDINAL;
                     VAR tab: ARRAY OF LabelRange);
    VAR i: CARDINAL; base, j: INTEGER;
  BEGIN
    base := pc + 2; Put16(VAL(INTEGER,l1) - base);  (* ELSE entry *)
    IF n > 0 THEN  (* not empty CASE statement *)
      i := 0; j := tab[0].low;
      WHILE i < n DO
        WHILE j < tab[i].low DO
          Put16(VAL(INTEGER,l1) - base); INC(j);    (* ELSE entry *)
        END;
        WHILE j <= tab[i].high DO
          Put16(VAL(INTEGER,tab[i].label) - base); INC(j);
        END;
        INC(i);
      END;
      fixup(l0); EnterCase(x, base, tab[0].low, tab[n-1].high);
    ELSE
      fixup(l0); EnterCase(x, base, 1, 0);  (* empty CASE statement *)
    END;
    DEC(sp, 2);
  END GenCase3;


  (* ForCheck - check if FOR loop control variable compatible with limit or
                initializer value *)
  PROCEDURE ForCheck(VAR x, y: Item);
  VAR xForm, yForm: StrForm;
  BEGIN
    SRTest(x);
    SRTest(y);
    xForm := x.typ^.form;
    yForm := y.typ^.form;
    IF NOT (
       (x.typ = y.typ) 
       OR
       (((xForm = Card) OR (xForm = Int)) AND 
        ((yForm = Card) OR (yForm = Int)) AND
        (y.mode = conMd))
       OR
       (((xForm = LCard) OR (xForm = Double)) AND 
        ((yForm = LCard) OR (yForm = Double)) AND
        (y.mode = conMd))
       ) THEN
      err(117);  (* incompatible operands *)
    END;
  END ForCheck;

  PROCEDURE GenFor1(v: Item; VAR e1: Item);
  BEGIN
    SRTest(v);
    IF (v.typ^.form = Undef) OR (v.typ^.form > Double) THEN
      err(142);  (* illegal type of control variable *)
    END;
    ForCheck(v, e1);
    GenAssign(v, e1);
  END GenFor1;

  PROCEDURE GenFor2(v: Item; VAR e1, e2: Item);
    VAR w: Item;
        EvenSize: CARDINAL;
  BEGIN
    ForCheck(v, e2);
    IF e2.mode # conMd THEN
      SetstkMd(w, e2.typ); GenAssign(w, e2); e2 := w; e2.mode := RindMd;
      (* Calculate amount of stack space used by test variable(e2) *)
      EvenSize := e2.typ^.size;
      (* When pushed on stack size must be even *)
      IF ODD(EvenSize) THEN
        INC(EvenSize);
      END;
      INC(sp, EvenSize);
    END;
  END GenFor2;

  PROCEDURE GenFor3(v: Item; VAR e2, e3: Item; VAR l0, l1: CARDINAL);
    VAR lv: LONGINT; 
        IncForm: StrForm;
        c: Condition;
  BEGIN
    SRTest(v);
    (* Perform type checking on FOR loop Increment *)
    IncForm := e3.typ^.form;
    IF NOT ((IncForm = Int) OR (IncForm = Card) OR
            (IncForm = Double) OR (IncForm = LCard)) THEN
      err(143); (* Illegal type of FOR loop increment expression *)
      SetconMd(e3, 1D, v.typ); (* Recovery action! *)
    END;
    IF LongVal(e3) = 0D THEN
      err(141);  (* step in FOR clause cannot be 0 *)
    END;

    (* If control variable is 1 or 2 bytes then increment must be CARDINAL or
     * INTEGER.
     *)
    IF (v.typ^.size < e3.typ^.size) THEN (* Trunc increment *)
      IF (e3.typ = lcardtyp) THEN (* unsigned *)
        e3.typ := cardtyp;
      ELSIF (e3.typ = dbltyp) THEN (* signed *)
        e3.typ := inttyp;
      END;
    END;

    (* Branch Condition To Exit To Out Loop *)
    IF (VAL(LONGINT,LongVal(e3)) > 0D) THEN
      IF SignedT(v) THEN c := GT ELSE c := HI END;
    ELSE
      IF SignedT(v) THEN c := LT ELSE c := CS END;
    END;

    l0 := pc; (* Top of FOR LOOP *)

    Cmp2(v, e2);(* Test For End of FOR loop *)
    Release(v);

    l1 := 0; Jf(c, l1);
  END GenFor3;

  PROCEDURE GenFor4(v: Item; VAR e2, e3: Item; l0, l1: CARDINAL);
    VAR c: Condition;
        EvenSize: CARDINAL;
        chk: BOOLEAN;
  BEGIN
    IF SignedT(v) THEN (* Signed Types *)
      c := VC;
    ELSE (* UnSigned Types *)
      c := CC;
    END;

    chk := ovflchk;
    ovflchk := FALSE;
    Add2(v, e3);
    Release(v);
    ovflchk := chk;

    Jb(c, l0);	(* Branch To Top Of FOR loop *)
    fixup(l1);  (* Destination of Branch Out of FOR loop *)

    IF e2.mode # conMd THEN
      (* Calculate amount of stack space used by limit variable(e2) *)
      EvenSize := e2.typ^.size;
      (* When pushed on stack size must be even *)
      IF ODD(EvenSize) THEN
        INC(EvenSize);
      END;
      StackTop(EvenSize); IF sp >= EvenSize THEN DEC(sp, EvenSize); END;
    END;
  END GenFor4;

(* More efficient FOR loop code:
  (*********************
   * FOR Loop Structure:
   *
   * FOR I := J TO K BY 10 DO X := 1; END;
   * 	move.W	J,I			; Initialize Control Variable
   * 	move.W	K,-(SP)			; Push Limit Expression
   * 	bra	1$
   *
   * 2$	move.W	1,X			; Statement Sequence
   *
   *    addq.W	#1,I			; Increment Control Variable
   *
   * 1$ move.W	I,D2			; Check For End Of Loop
   *    cmp.W	(SP),D2
   *   	bls.S	2$
   *
   *    addq	#2,SP 			; Drop Limit From Stack
   *
   * NOTE: One ploblem with this technique is that a loop which ends on a
   *       boundary of the control variable type will not function properly.
   *       example: VAR i: CARDINAL;
   *       FOR i := 0 TO 65535 DO END;
   *       FOR i := 10 TO 0 BY -1 DO END;
   *)

  PROCEDURE GenFor3(v: Item; VAR e2, e3: Item; VAR l0, l1: CARDINAL);
    VAR lv: LONGINT; 
        IncForm: StrForm;
  BEGIN
    SRTest(v);
    (* Perform type checking on FOR loop Increment *)
    IncForm := e3.typ^.form;
    IF NOT ((IncForm = Int) OR (IncForm = Card) OR
            (IncForm = Double) OR (IncForm = LCard)) THEN
      err(143); (* Illegal type of FOR loop increment expression *)
      SetconMd(e3, 1D, v.typ); (* Recovery action! *)
    END;
    IF LongVal(e3) = 0D THEN
      err(141);  (* step in FOR clause cannot be 0 *)
    END;

    (* If control variable is 1 or 2 bytes then increment must be CARDINAL or
     * INTEGER.
     *)
    IF (v.typ^.size < e3.typ^.size) THEN (* Trunc increment *)
      IF (e3.typ = lcardtyp) THEN (* unsigned *)
        e3.typ := cardtyp;
      ELSIF (e3.typ = dbltyp) THEN (* signed *)
        e3.typ := inttyp;
      END;
    END;

    l1 := 0; Jf(T, l1);
    l0 := pc;
  END GenFor3;

  PROCEDURE GenFor4(v: Item; VAR e2, e3: Item; l0, l1: CARDINAL);
    VAR c: Condition;
        f: StrForm; 
        EvenSize: CARDINAL;
        chk: BOOLEAN;
  BEGIN
    (* This should precede Add2() because the function modifies the operand! *)
    IF (VAL(LONGINT,LongVal(e3) > 0D) THEN
      IF SignedT(v) THEN c := LE ELSE c := LS END;
    ELSE
      IF SignedT(v) THEN c := GE ELSE c := CC END;
    END;

    chk := ovflchk;
    ovflchk := FALSE;
    Add2(v, e3);
    ovflchk := chk;

    fixup(l1);  (* Destination of Branch To FOR loop test *)

    Cmp2(v, e2);(* Test For End of FOR loop *)
    Release(v);

    Jb(c, l0);	(* Branch To Top Of FOR loop *)

    IF e2.mode # conMd THEN
      (* Calculate amount of stack space used by limit variable(e2) *)
      EvenSize := e2.typ^.size;
      (* When pushed on stack size must be even *)
      IF ODD(EvenSize) THEN
        INC(EvenSize);
      END;
      StackTop(EvenSize); IF sp >= EvenSize THEN DEC(sp, EvenSize); END;
    END;
  END GenFor4;

*)


  PROCEDURE GenLoop1(VAR s, m: CARDINAL; n: CARDINAL);
  BEGIN
    s := sp0; sp0 := sp; m := n;
  END GenLoop1;

  PROCEDURE GenLoop2(s, m: CARDINAL; VAR n: CARDINAL; VAR tab: ExitTable);
  BEGIN
    WHILE n > m DO fixup(tab[n-1]); DEC(n); END;
    sp0 := s;
  END GenLoop2;

  PROCEDURE GenExit(VAR n: CARDINAL; VAR tab: ExitTable);
  BEGIN
    StackTop(sp - sp0);
    IF n <= MaxExit THEN
      tab[n] := 0; GenFJ(tab[n]); INC(n);
    ELSE
      err(93);  (* too many exit statements *)
    END;
  END GenExit;

  PROCEDURE GenEnterMod(modList: ObjPtr; mno, pno: CARDINAL);
    VAR obj: ObjPtr; i: CARDINAL;
  BEGIN
    EnterModule;
    maxP := pno + 1;  (* 1 for initialization *)
    maxM := mno + 1;  (* 1 for System *)
    sp := 0; sp0 := 0;
  END GenEnterMod;

  PROCEDURE GenExitMod;
  BEGIN
    ExitModule;
  END GenExitMod;

END M2CM.
