(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Jim Olinger                          *
 ****************************************************************************
 * Name: SystemIEEE.MOD                       Version: Amiga.01.10          *
 * Created: 02/21/94   Updated:               Author: Tom Breeden           *
 * Description: AmigaDOS Modula-2 program startup code for IEEE sp REALs    *
 ****************************************************************************)

(*$2-*)
(*###############################*)
 IMPLEMENTATION MODULE SystemIEEE;         (* vx.x-031494 *)
(*###############################*)

FROM SYSTEM IMPORT ADDRESS, ADR, INLINE, REG, SETREG;

FROM System    IMPORT CLIReturnCode, DOSBase, MathIeeeSingBasBase, REALflavor;
IMPORT System;

(*IMPORT ExecBase;*)
(*FROM ExecBase IMPORT AF68020, AF68030, AF68040;*)
(*FROM Libraries IMPORT OpenLibrary;*)

(* NOTE: 1. Compiler automatically imports this if the IEEE REALs
            code generation switch is used.

         2. Linker arranges it to be called just after the System module
            initialization.

         3. The cannot import anything (except System), since currently the
            way the Linker is written will create a bad init jump hunk
            (if nothing else imports ExecBase).

         4. Assigns 1 to REALflavor at top.
*)

CONST libVersionOffset = 20D;

CONST ErrorNotImplemented  = 236D;
      A6                   = 14;
      A1                   = 9;
      D0                   = 0;
      D1                   = 1;
      D2                   = 2;

      ErrMsg = "Requires IEEElib";

VAR   eb             :ADDRESS(*ExecBase.ExecBasePtr*);
      nam            :ADDRESS;
      execlibVersion :POINTER TO CARDINAL;

BEGIN

REALflavor := 1;

eb := System.ExecBase;

execlibVersion := eb + libVersionOffset;

(*MathIeeeSingBasBase := OpenLibrary(ADR("mathieeesingbas.library"), 0D);*)
nam := ADR("mathieeesingbas.library");

SETREG(A6, eb);                    (* MOVE.L ExecBase,A6     *)
SETREG(A1, nam);                   (* MOVE.L nam,A1          *)
SETREG(D0, 0D);                    (* MOVE.L #0,D0           *)
INLINE(4EAEH, -228H);              (* JSR LVOOpenLibrary(A6) *)
MathIeeeSingBasBase := REG(D0);

IF MathIeeeSingBasBase = NIL THEN

   CLIReturnCode := 10D;

   (*>>>IF DOSBase # NIL THEN*)

       IF execlibVersion^ >= 36 THEN

          (*PrintFault(code:LONGINT; header:ADDRESS):BOOLEAN;*)
          (* also does a SetIoErr() *)

          SETREG(A6, DOSBase);               (* MOVE.L DOSBase,A6    *)
          SETREG(D1, ErrorNotImplemented);   (* MOVE.L #236,D1       *)
          SETREG(D2, ADR(ErrMsg));           (* MOVE.L ADR(),D2      *)
          INLINE(4EAEH, -1DAH);              (* JSR LVOSetIoErr(A6)  *)

       END;

   (*>>>END;*)

   HALT;

END;

END SystemIEEE.
