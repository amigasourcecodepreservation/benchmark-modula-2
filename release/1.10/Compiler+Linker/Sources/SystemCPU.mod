(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Jim Olinger                          *
 ****************************************************************************
 * Name: SystemCPU.MOD                        Version: Amiga.01.10          *
 * Created:  2/21/94   Updated:            Author: Tom Breeden              *
 * Description: AmigaDOS Modula-2 program startup code for M68020+          *
 ****************************************************************************)

(*$2-*)
(*##############################*)
 IMPLEMENTATION MODULE SystemCPU;        (* vx.x-031494 *)
(*##############################*)

FROM SYSTEM IMPORT ADDRESS, ADR, INLINE, SETREG;

IMPORT System;
FROM System IMPORT StdOutput;

(*IMPORT ExecBase;*)
(*FROM ExecBase IMPORT AF68020, AF68030, AF68040;*)

(* NOTE: 1. Compiler automatically imports this if the 68020 code generation
            switch is used.

         2. Linker arranges it to be called just after the System module
            initialization.

         3. The cannot import anything (except System), since currently the
            way the Linker is written will create a bad init jump hunk
            (if nothing else imports ExecBase).
*)

(* Cut from ExecBase.def: *)
CONST AF68010   = 0;     (* also set for 68020 *)
      AF68020   = 1;     (* also set for 68030 *)
      AF68030   = 2;     (* also set for 68040 *)
      AF68040   = 3;

      libVersionOffset = 20D;
      AttnFlgsOffset   = 296D;

CONST ErrorNotImplemented  = 236D;
      A6                   = 14;
      D1                   = 1;
      D2                   = 2;

      ErrMsg = "Requires M68020";

VAR   eb         :ADDRESS(*ExecBase.ExecBasePtr*);

      libVersion :POINTER TO CARDINAL;
      AttnFlgs   :POINTER TO BITSET;

BEGIN

eb := System.ExecBase;

libVersion := eb + libVersionOffset;
AttnFlgs := eb + AttnFlgsOffset;

IF {AF68020, AF68030, AF68040} * AttnFlgs^ = {} THEN

   System.CLIReturnCode := 10D;

   (*>>IF System.DOSBase # NIL THEN*)

       IF libVersion^ >= 36 THEN

          (*PrintFault(code:LONGINT; header:ADDRESS):BOOLEAN;*)
          (* also does a SetIoErr() *)

          SETREG(A6, System.DOSBase);        (* MOVE.L DOSBase,A6    *)
          SETREG(D1, ErrorNotImplemented);   (* MOVE.L #236,D1       *)
          SETREG(D2, ADR(ErrMsg));           (* MOVE.L ADR(),D2      *)
          INLINE(4EAEH, -1DAH);              (* JSR LVOSetIoErr(A6)  *)

       END;

   (*>>END;*)

   HALT;
END;

END SystemCPU.
