
(* Name   : M2FM.MOD        for v1.10

 * Changed: 06/06/87 LF
 * Notes :
 * 1. Changed literal constants "0.0" to FLOAT(0) for easier portability
 *    between different floating point types!
 * 2. Functions have been changed to return result in D0 rather than on top
 *    of stack. Procedures "FOp1" and "FOp2" to handle floating point results
 *    return in reg D0.
 * 3. Turned off compiler switch "D".
 * 4. Changed procedure "FOp1" to set variable "pcLastProcResult" to the "pc"
 *    where the "MOVE.L D0,Dn" is generated.  Changed parameter pushing
 *    sequence to be more efficient by avoiding loading the parameter into D0
 *    before pushing on stack.
 * 5. Changed procedure "FOp2" to set variable "pcLastProcResult" to the "pc"
 *    where the "MOVE.L D0,Dn" is generated. Changed parameter pushing
 *    sequence to be more efficient by avoiding loading the parameters into D0
 *    before pushing on stack.  When the operation returns results in condcodes
 *    a special function is called "RestoreRegsSaveCC" to avoid loosing cond
 *    codes.
 * 6. In procedures "LoadF" and "FMove" commented out code to handle LONGREAL
 *    since they are not currently supported!
 *)

(***************************************************************************
 * Changes for new release 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.1/2 -----
 *
 * Call Amiga mathffp.library directly for FFP REAL
 *
 *   1. Added 68000 Addressing Mode consts and Left Shift constants to top
 *      of file. Changed left shift octals in Put16() calls to use the
 *      Left Shift consts instead.
 *   2. Added CONST MOVEAL   = 020100B; (* MOVEA.L ea,An *)
 *      also JSR = 047200B
 *   3. Added proc variable "AlibOffset:CARDINAL" to FOp1() and FOp2().
 *   4. Changed "CASE op OF" statement in FOpt1() and FOpt2() to assign
 *      the appropriate AlibOffset for the library call for this op.
 *   5. Calling SaveAmigaRegs() before Amiga Lib call instead of SaveRegs()
 *      in FOp1() and FOp2().
 *   6. Setting y item regMd and D0 reg before Amiga Lib call instead of
 *      stkMd in FOp1().
 *      For FOpt2() this is item z and z2.
 *   7. Doing call to Amiga library instead of call to System.xxx func in FOp1/2():
 *        Put16(MOVEAL +A6*LS9 +XXXL);         (* MOVE.L System.MathBase.L,A6     *)
 *        PutADRReloc(maxM - 1, MathBaseVAR);  (*           System is mod #maxM-1 *)
 *        Put16(JSR + AOFF+A6);                (* JSR _LVOx(A6)                   *)
 *        Put16(AlibOffset);
 *   8. Added item "z2" for FOpt2().
 *
 * Code Generation for Either IEEE or FFP REALs
 *
 *    1. Added procs for compile time expressions for either flavor, based
 *       on "codeIEEE" boolean:
 *
 *          FltFloat(i:INTEGER):REAL
 *          FltAdd/Sub/Mul/Div(a, b:REAL):REAL;
 *          FltNeg(a:REAL):REAL;
 *          FltCmp(a, b:REAL):INTEGER;
 *          FltMax/Min():REAL;
 *
 *       These simply do the operation if codeIEEE is FALSE, otherwise they
 *       call the corresponding MathIEEESPBas library routine.
 *
 *    2. Compiler is assumed to be generated for FFP REALs. IMPORTing from
 *       Amiga standard library ieeesingbas.library (in ROM) for doing the IEEE
 *       REAL calculations. AmigaDOS v1.3 does not include this library,
 *       so IEEE REALs may not be available under v1.3 (unless a disk based
 *       version of that library is in LIBS:).
 *    3. Replaced FLOAT() calls with FltFloat().
 *    4. In FOp1() and FOp2(), loading A6 with libr base address for either
 *       the FFP library of the IEEEsing library, depending on codeIEEE boolean.
 *       The function offsets are the same for these libraries!
 *    5. Had to treat TRUNC() specially, since it appears that the CBM IEEEsing
 *       library uses the FINT 6888x instruction for the SPFix call. That
 *       instruction converts to an integer based on the current "rounding mode"
 *       set up for the 6888x. It appears that CBM sets up for
 *       "round-to-nearest", which causes FINT to do a ROUND() rather than a
 *       TRUNC().
 *
 *       The library running in emulation mode (ie, if no 6888x is present)
 *       seems to always do a TRUNC().
 *
 *       For now, rather than depend on possibly changing behavior, I've
 *       generated code that will work in either case, though it is slower
 *       than I would like:
 *          an IEEESPFloor() or IEEESPCeil call is done first, depending on
 *          whether the number is pos or neg. Then the IEEESPFix call is done.
 *
 *      (* Special case for TRUNC since IEEESPFix appears to work as a round *)
 *      (* operation if the 68881 is present - see notes above               *)
 *      IF codeIEEE AND (op = TRUNCs) THEN
 *        Put16(TST+LS7+D0);
 *        Put16(BMI+6);
 *        Put16(JSR + AOFF+A6);                (* JSR _LVOx(A6)                 *)
 *        Put16(SPFloor);
 *        Put16(BRA+4);
 *        Put16(JSR + AOFF+A6);                (* JSR _LVOx(A6)                 *)
 *        Put16(SPCeil);
 *      END;
 *      <then SPFix is called here>
 *
 *       THIS IS NOT SATISFACTORY, since Floor (and Ceil?) seem to be very
 *       much slower than Fix - causing TRUNC() to be about 10X slower than
 *       it would be with Fix() alone.
 *    6. Added another "Peephole optimizer operation", similar to the one
 *       in M2CM.GenAssign(). For this one, if the last generated code was
 *       MOVE.L D0,Dn, and we are about to generate MOVE.L Dn,D0, then don't
 *       generate either one. Since the x item has Release() called on it
 *       immediately afterwards, this should be ok. Check that its register
 *       is not locked, however.
 *       This is worth it since probably every three factor/term real expression
 *       will result in a hit here.
 *
 *               ----- Beta.3 -----
 *
 * Support for LONGREAL
 *
 *    0. IMPORTing from Amiga standard library ieeedoubbas.library (on disk)
 *       for doing the LONGREAL calculations. AmigaDOS v1.3 does not include
 *       this library, LONGREALs may not be available under v1.3 (unless a disk
 *       based version of that library is in LIBS:).
 *    1. Restored the long real code previous commented out in LoadF() and
 *       FMove(). Except that the "PreLoad" code in FMove() does not seem to
 *       make any sense - left that commented out.
 *    2. In FMove() added code to call M2HM.MoveBlock() if none of the other
 *       cases met (the "PreLoad" section had handled them before).
 *    3. In FOp1() and FOp2(), loading A6 with libr base address the IEEEdoub
 *       library, except for FLONG and FSHORT, which require IEEEdoubTrans
 *       library.
 *    4. Using SetfltMd() here in top of FOp1/2() where SetregMd() had been used
 *       at the point D0 (and now D0/D1) is to be loaded for the libr call.
 *       For SP REAL, this does a SetregMd(), but for DP LONGREAL, this actually
 *       sets fltMd.
 *    5. In FOp1() and FOp2(), where the save-only-some-regs kludge takes place,
 *       added test for fltMd in the code that resets item regs to busy afterward.
 *    6. For FOp2(), using SaveAmigeDPRegs() instead of SaveAmigaRegs() if a
 *       double precision op is being done.
 *    7. For second param in FOp2(), added code to SetfldMd() into D2/D3.
 *    8. Had to treat TRUNC() specially, as done for sp IEEE above.
 *
 *       THIS IS NOT SATISFACTORY, since Floor (and Ceil?) seem to be very
 *       much slower than Fix - causing TRUNC() to be about 10X slower than
 *       it would be with Fix() alone.
 *    9. FLong and FShort require the mathieeedoubtrans.library to use
 *       the to/fromIEEE calls.
 *
 * Improved Code Generation, Q&D
 *
 *    1. Added a PreLoadF() to be called from FOp2():
 *
 *          a. For symmetric REAL or LONGREAL ops, swaps Items x and y if y
 *             was an Item just returned from a function, so that the
 *             pcLastResult/8 will be taken advantage of (ie, not MOVE D0, D2;
 *             MOVE D2, D0).
 *
 *    2. Using pcLastResultProc8 in FOp1() and FOp2() at to before the FMove()
 *       of x into D0/D1.
 *
 *       Setting pcLastResultPro8 in FOp1() and FOp2() at end before the
 *       FMove of the D0/D1 result into another Dreg pair.
 *
 ***************************************************************************
*)

(*$L+,D- relocatable *)
IMPLEMENTATION MODULE M2FM;
  
  FROM SYSTEM IMPORT
          ADR, LONG, WORD (*, VAL*);
  FROM M2DM IMPORT
          ObjPtr, StrPtr, ConstValue, Object, Structure,
          notyp, undftyp, booltyp, chartyp, cardtyp,
          inttyp, bitstyp, lcardtyp, dbltyp, realtyp, lrltyp,
          proctyp, stringtyp, addrtyp, wordtyp, bytetyp,
          MaxCard, MinInt, MaxInt;
  FROM M2SM IMPORT
           Mark;
  FROM M2HM IMPORT
          D0, D1, D2, SB, MP, SP, A6,
          byte, word, long,
          Condition, RegType, Register,
          WidType, ItemMode, ItSet, Item,
          SimpleT, RealT, SetregMd, SetstkMd,
          GetReg, GetRegPair, Release, ReleaseReg, Islocked,
          SaveRegs, SaveAmigaRegs, SaveAmigaDPRegs, RestoreRegs, Gea, Ext,
          StackTop, CallSystem, LoadD, LoadX, Move, MoveBlock, SetbusyReg,
          pcLastProcResult, pcLastProcResult8, codeIEEE;
  FROM M2LM IMPORT
          codeB, maxM, pc, PutADRReloc, PutWord;

  FROM System IMPORT
          MathIeeeSingBasBase;
  FROM Libraries IMPORT
          OpenLibrary;
  FROM MathIEEESPBas IMPORT
          IEEESPAbs, IEEESPAdd, IEEESPCmp, IEEESPDiv, IEEESPFlt, IEEESPMul,
          IEEESPNeg, IEEESPSub; IMPORT MathIEEESPBas (*MathIeeeSingBasBase*);
  
  CONST IEEELibName = "mathieeesingbas.library";

  CONST
  
     (* System procedure numbers used by the compiler :          *)
     (* These numbers must correspond with the procedure numbers *)
     (* associated with a specific procedure in the definition   *)
     (* module 'System'.                                         *)

     (* NOTE: With Amiga library usage, these do not cause a call to
        module System routines, but their values, and their ordering (!)
        is still used
     *)

     BodyOfSystem        = 0; (* 0 is reserved for module body       *)
     HALTX               = 1; (* System.HALTX  = HALT-statement      *)
     MULU32              = 2; (* System.MULU32 = unsigned long MUL   *)
     DIVU32              = 3; (* System.DIVU32 = unsig. long DIV/MOD *)
     MULS32              = 4; (* System.MULS32 = signed long MUL     *)
     DIVS32              = 5; (* System.DIVS32 = signed long DIV/MOD *)
     FADDs               = 6; (* System.FADDs  = Floating ADD single *)
     FSUBs               = 7; (* System.FSUBs  = Floating SUB single *)
     FMULs               = 8; (* System.FMULs  = Floating MUL single *)
     FDIVs               = 9; (* System.FDIVs  = Floating DIV single *)
     FREMs               = 10;(* System.FREMs  = Floating REM single *)
     FCMPs               = 11;(* System.FDIVs  = Floating CMP single *)
     FNEGs               = 12;(* System.FNEGs  = Floating NEG single *)
     FABSs               = 13;(* System.FABSs  = Floating ABS single *)
     FLOATs              = 14;(* System.FLOATs = FLOAT single        *)
     TRUNCs              = 15;(* System.TRUNCs = TRUNC single        *)
     FADDd               = 16;(* System.FADDd  = Floating ADD double *)
     FSUBd               = 17;(* System.FSUBd  = Floating SUB double *)
     FMULd               = 18;(* System.FMULd  = Floating MUL double *)
     FDIVd               = 19;(* System.FDIVd  = Floating DIV double *)
     FREMd               = 20;(* System.FREMd  = Floating REM double *)
     FCMPd               = 21;(* System.FDIVd  = Floating CMP double *)
     FNEGd               = 22;(* System.FNEGd  = Floating NEG double *)
     FABSd               = 23;(* System.FABSd  = Floating ABS double *)
     FLOATd              = 24;(* System.FLOATd = FLOAT double        *)
     TRUNCd              = 25;(* System.TRUNCd = TRUNC double        *)
     FLONG               = 26;(* System.FLONG  = Floating single to double *)
     FSHORT              = 27;(* System.FSHORT = Floating double to single *)

     (* System Global Variable offsets             *)
     (* must correspond with the System.def module *)

     MathBaseVAR              = -12;     (* FFP  *)
     MathIeeeSingBasBaseVAR   = -66;     (* IEEE *)
     MathIeeeDoubBasBaseVAR   = -70;     (* LONGREAL *)
     MathIeeeDoubTransBaseVAR = -76;     (* LONGREAL for SHORT()/LONG() *)

  CONST

     (* mathffp.library, mathieeesingbas.library, and mathieeedoubbas.library
        offsets *)

     SPFix   = 0FFE2H;
     SPFlt   = 0FFDCH;
     SPCmp   = 0FFD6H;
   (*SPTst   = 0FFD0H;*)  (* CAREFUL: FFP single param is in D1, for IEEE in D0 *)
     SPAbs   = 0FFCAH;
     SPNeg   = 0FFC4H;
     SPAdd   = 0FFBEH;
     SPSub   = 0FFB8H;
     SPMul   = 0FFB2H;
     SPDiv   = 0FFACH;
     SPFloor = 0FFA6H;
     SPCeil  = 0FFA0H;

     (* mathieeedoubtrans.library offsets *)

     IEEEDPTieee = 0FF9AH;
     IEEEDPFieee = 0FF94H;

  CONST

     MOVEMDEC = 044347B; (* MOVEM.L regs,-(SP) *)
     MOVEMSTD = 044300B; (* MOVEM.L regs,ea    *)
     MOVEMINC = 046337B; (* MOVEM.L (SP)+,regs *)
     MOVEMLDD = 046300B; (* MOVEM.L ea,regs    *)   
     MVEMSP   = 027400B; (* MOVE.L  ea,-(SP)   *)
     MVESPP   = 020037B; (* MOVE.L  (SP)+,ea   *)
     MOVEL    = 020000B; (* MOVE.L  ea,ea      *)
     MOVELIMM = 020074B; (* MOVE.L  #imm,ea    *)
     MOVEAL   = 020100B; (* MOVEA.L ea,An      *)

     JSR      = 047200B;
     TST      = 045000B;
     BRA      = 060000B; BMI      = 065400B;

     (* Addressing Mode Categories. *)
  
     DDIR     = 0;       (* D-Reg. direct  *)
     ADIR     = 10B;     (* A-Reg. direct  *)
     AIDR     = 20B;     (*     (An)       *)
     AINC     = 30B;     (*     (An)+      *)
     ADEC     = 40B;     (*    -(An)       *)
     AOFF     = 50B;     (*  d16(An)       *)
     AIDX     = 60B;     (*   d8(An,Rx)    *)
     XXXW     = 70B;     (* absolute short *)
     XXXL     = 71B;     (* absolute long  *)
     PREL     = 72B;     (*  d16(PC)       *)
     IMM      = 74B;     (* immediate or SR*)
  
     (* Left shift constants. *)
     LS3  =  10B;  LS4  =  20B;  LS5  =  40B;   LS6  =  100B;
     LS7  = 200B;  LS8  = 400B;  LS9  =  1000B; LS10 =  2000B;
     LS11 = 4000B; LS12 = 10000B;

  PROCEDURE err(n : CARDINAL);
    (* local synonym for M2SM.Mark to save space! *)
  BEGIN
     Mark(n);
  END err;

  PROCEDURE Put16(w : WORD);
    (* local synonym for M2LM.PutWord to save space! *)
  BEGIN
    PutWord(w);
  END Put16;

  PROCEDURE XSHIFT(x, n : CARDINAL) : CARDINAL;
  BEGIN
    WHILE n > 0 DO INC(x, x); DEC(n) END;
    RETURN x
  END XSHIFT;

  PROCEDURE SetfltMd(VAR x : Item; fR : Register; ftyp : StrPtr);
  BEGIN
    WITH x DO
      IF ftyp = realtyp THEN     (* for single real          *)
        SetregMd(x, fR, ftyp);   (* resulting mode = DregMd! *)
      ELSE 
        typ := ftyp;             (* for double real          *)
        mode := fltMd; FR := fR; (* resulting mode = fltMd ! *)
      END;
    END (*WITH*);
  END SetfltMd;

  PROCEDURE LoadF(VAR x : Item);
    (* Load x into a Floating-Point-Register.                        *)
    (* The current implementation simulates Floating-Point-Registers *)
    (* by means of one (single) D-Register or a (double) D-Register- *)
    (* Pair.                                                         *)
    VAR Dn : Register; ea : CARDINAL;
  BEGIN
    WITH x DO
      IF typ = realtyp THEN (* single real *) LoadD(x)
      ELSE (* LongReals *)
        (* Assert( typ = lrltyp ); *)
        IF mode <= stkMd THEN
          GetRegPair(Dn);
          Gea(x,ea);
          IF mode = conMd THEN
            (* Note : NO immeditate's for MOVEM! *)
            Put16(MOVELIMM + Dn*LS9);
            Put16(val.D0); Put16(val.D1);
            Put16(MOVELIMM + (Dn+1)*LS9);
            Put16(val.D2); Put16(val.D3);
          ELSE 
            Put16(MOVEMLDD + ea);
            Put16(XSHIFT(3, Dn));
            Ext(x);
          END;
          Release(x); (* NOW release old registers! *)
          mode := fltMd; FR := Dn;
        ELSIF mode <> fltMd THEN
          err(239); Release(x);
          mode := fltMd; FR := D0;
        END;
      END;
    END (*WITH*);
  END LoadF;

  PROCEDURE FMove(VAR x, y : Item);
    (* move REAL type x  --->>  REAL type y *)
    (* perform floating type moves :        *)
    (*        memory    to   memory         *)
    (*        register  to   memory         *)
    (*        memory    to   register       *)
    (* The current implementation simulates Floating-Point-Registers *)
    (* by means of one (single) D-Register or a (double) D-Register- *)
    (* Pair.                                                         *)

    VAR Dn : Register; ea : CARDINAL;
  BEGIN
    (* Assert( y.mode <> conMd ); *)
    (* Assert( x.typ  =  y.typ ); *)
    IF x.typ = realtyp THEN (* single real *)

      Move(x,y);

    ELSIF (x.mode <> stkMd) OR (y.mode <> stkMd) THEN (* LongReal *)

      (* Assert( x.typ = lrltyp ); *)
      (* >>>>>>>>>>> ????????????????
      >>> IF (x.mode <= stkMd) THEN
      >>>   (* Preload floating value to scratch D0/D1 : *)
      >>>   (* Don't waste D-pool-Registers !            *)
      >>>   WITH x DO
      >>>     IF mode = conMd THEN
      >>>       (* Note : NO immeditate's for MOVEM! *)
      >>>       Put16(MOVELIMM + D0*LS9);
      >>>       Put16(val.D0); Put16(val.D1);
      >>>       Put16(MOVELIMM + D1*LS9);
      >>>       Put16(val.D2); Put16(val.D3);
      >>>     ELSE
      >>>       Gea(x,ea); 
      >>>       Put16(MOVEMLDD + ea);
      >>>       Put16(3); (* register list for D0/D1 *)
      >>>       Ext(x);
      >>>       Release(x); (* NOW release old registers! *)
      >>>     END;
      >>>     mode := fltMd; FR := D0;
      >>>   END (*WITH x*);
      >>> END (*Preload*);
      <<<<<<<<<<<<<<<<<<< ?????????????*)
      IF (x.mode <= stkMd) & (y.mode = fltMd) THEN
        (* memory to register : *)
        Dn := y.FR;
        Gea(x,ea);
        Put16(MOVEMLDD + ea);
        Put16(XSHIFT(3, Dn));
        Ext(x);
      ELSIF (x.mode = fltMd) & (y.mode <= stkMd) THEN
        (* register to memory : *)
        Dn := x.FR;
        IF y.mode = stkMd THEN
          Put16(MOVEMDEC);
          Put16(XSHIFT(3, 14 - Dn));
        ELSE
          Gea(y,ea);
          Put16(MOVEMSTD + ea);
          Put16(XSHIFT(3, Dn));
          Ext(y);
        END;
      ELSIF (x.mode = fltMd) & (y.mode = fltMd) THEN
        (* register to register : *)
        Dn := y.FR;
        IF x.FR <> Dn THEN
          Put16(MOVEL + Dn*LS9 + x.FR);
          Put16(MOVEL + (Dn+1)*LS9 + (x.FR+1));
        END;
      ELSIF (x.mode <= stkMd) & (y.mode <= stkMd) THEN
        (* memory to memory : *)
         MoveBlock (x, y, 8(*sz*), FALSE(*isstring*));
      ELSE (* illegal modes *) err(241)
      END;

    END (*long reals*);
  END FMove;

 (*------------------------------------------*)
  PROCEDURE FOp1(op : CARDINAL; VAR x : Item);
 (*------------------------------------------*)
    VAR regs : BITSET; y : Item; rtyp : StrPtr; sz : CARDINAL; Dn : Register;
        AlibOffset : CARDINAL;
  BEGIN
    (* Assert( RealT(x) ); *)       (* even for FLOATs/FLOATd *)
      CASE op OF
        (* define resulting type *)
        FNEGs          : rtyp := realtyp; AlibOffset := SPNeg;
      | FABSs          : rtyp := realtyp; AlibOffset := SPAbs;
      | FNEGd          : rtyp := lrltyp;  AlibOffset := SPNeg;
      | FABSd          : rtyp := lrltyp;  AlibOffset := SPAbs;
      | TRUNCs         : rtyp := dbltyp;  AlibOffset := SPFix;
      | TRUNCd         : rtyp := dbltyp;  AlibOffset := SPFix;
      | FLOATs         : rtyp := realtyp; AlibOffset := SPFlt;
      | FSHORT         : rtyp := realtyp; AlibOffset := IEEEDPTieee; (* entry is in trans library *)
      | FLOATd         : rtyp := lrltyp;  AlibOffset := SPFlt;
      | FLONG          : rtyp := lrltyp;  AlibOffset := IEEEDPFieee; (* entry is in trans library *)
      END;

      (* Release Operand Temporarily to avoid unnecessarily saving registers. *)
      Release(x);
      SaveAmigaRegs(regs);       (* save busy registers that AmigaLibs might zap *)

      (* Reset registers used by operand to busy! *)
      IF x.mode IN ItSet{RindMd,RidxMd,AregMd,DregMd} THEN
        SetbusyReg(x.R);
      ELSIF x.mode = fltMd THEN
        SetbusyReg(x.FR);
        SetbusyReg(x.FR+1);
      END;
      IF x.mode = RidxMd THEN
        SetbusyReg(x.RX)
      END;

      (*SetregMd(y, D0, x.typ);*)
      SetfltMd(y, D0, x.typ);

      (* Another Peephole optimizer operation! (see also M2CM.GenAssign() *)
      (* Elliminate MOVE.L Result,Dn which is generated after each func
       * call (included system related functions calls) if redundant.
       * If the last instruction was MOVE.L Result(ie, D0),Dn and this one
       * will be MOVE.L Dn,D0 then don't generate this one *)
      (* This is worth it since probably every three factor/term real expression
         will result in a hit here *)

      IF x.typ = realtyp THEN
         IF (pcLastProcResult = pc-2)
                  AND (x.mode = DregMd)
                  AND (x.R = (codeB^[(pc-2) DIV 2] DIV LS9) MOD 8)
                  (*AND ((codeB^[(pc-2) DIV 2] MOD 8) = 0)*)
                  AND NOT(Islocked(x.R)) THEN
            DEC(pc, 2);  (* zap the already generated MOVE.L instru *)
         ELSE
            FMove(x,y);           (* move parameter to D0 *)
                         (* CAREFUL, if FMove() changes this will BREAK *)
         END;
      ELSE
         IF (pcLastProcResult8 = pc-4)
                  AND (x.mode = fltMd)
                  AND (x.FR = (codeB^[(pc-4) DIV 2] DIV LS9) MOD 8)
                  (*AND ((codeB^[(pc-4) DIV 2] MOD 8) = 0)*)
                  AND NOT(Islocked(x.FR) AND NOT(Islocked(x.FR+1))) THEN
            DEC(pc, 4);  (* zap the two already generated MOVE.L instrus *)
         ELSE
            FMove(x,y);           (* move parameter to D0/D1 *)
                         (* CAREFUL, if FMove() changes this will BREAK *)
         END;
      END;

      Release(x);              (* now release the parameter *)

      (* call to Amiga FPP or IEEE or IEEEdoub library *)
      Put16(MOVEAL +A6*LS9 +XXXL);         (* MOVE.L System.MathxBase.L,A6    *)

      (* System is mod #maxM-1 *)
      IF op >= FADDd THEN
        IF (op = FLONG) OR (op = FSHORT) THEN
           PutADRReloc(maxM - 1, MathIeeeDoubTransBaseVAR);
        ELSE
           PutADRReloc(maxM - 1, MathIeeeDoubBasBaseVAR);
        END;
      ELSIF codeIEEE THEN
        PutADRReloc(maxM - 1, MathIeeeSingBasBaseVAR);
      ELSE
        PutADRReloc(maxM - 1, MathBaseVAR);
      END;

      (* Special case for TRUNC since IEEESP/DP/Fix appears to work as a round *)
      (* operation if the 68881 is present - see notes above               *)
      IF (codeIEEE AND (op = TRUNCs)) OR (op = TRUNCd) THEN
        Put16(TST+LS7+D0);
        Put16(BMI+6);
        Put16(JSR + AOFF+A6);                (* JSR _LVOx(A6)                 *)
        Put16(SPFloor);
        Put16(BRA+4);
        Put16(JSR + AOFF+A6);                (* JSR _LVOx(A6)                 *)
        Put16(SPCeil);
      END;

      Put16(JSR + AOFF+A6);                (* JSR _LVOx(A6)                   *)
      Put16(AlibOffset);

      IF regs # {} THEN RestoreRegs(regs); END; (* restore busy registers *)

      (* result in reg D0 or D0/D1, must be copied to a pool register *)
      sz := VAL(CARDINAL,rtyp^.size);
      IF NOT (sz IN {4,8}) THEN err(200); END;
      IF sz IN {4} THEN (* long result D0 *)

        GetReg(Dn,Dreg);
        SetregMd(x, Dn, rtyp);
        SetregMd(y, D0, rtyp);

        (* Remember position of MOVE.L D0, Dn *)
        pcLastProcResult := pc;

        Move(y,x);

      ELSE (* sz = 8, double-longword result D0/D1 *)

        GetRegPair(Dn);
        x.mode := fltMd;
        x.FR   := Dn;
        y.typ  := rtyp;
        y.mode := fltMd;
        y.FR   := D0; (* D0/D1 *)

        (* Remember position of MOVE.L D0, Dn; MOVE.L D1,Dn+1 *)
        pcLastProcResult8 := pc;

        FMove(y,x);

      END;
  END FOp1;

 (*--------------------------------------------------*)
  PROCEDURE PreLoadF(op : CARDINAL; VAR x , y : Item);
 (*--------------------------------------------------*)
    (* preload x and y for FOp2, if possible. *)
    (* for optimisation purposes.              *)
    VAR z : Item;
  BEGIN

    (*Assert( NOT ((x.mode = conMd) & (y.mode = conMd)) );*)

    (*>IF (op >= FADDd) THEN
    >>>  RETURN;
    >>>END;
    <<<*)

    (* symmetric operators : set up to take advantage of pcLastProcResult *)
    IF (op = FADDs) OR (op = FMULs) THEN
      IF (pcLastProcResult = pc-2) AND (y.mode = DregMd)
               AND (y.R = (codeB^[(pc-2) DIV 2] DIV LS9) MOD 8) THEN
          z := x; x := y; y := z;
      END;
    ELSIF (op = FADDd) OR (op = FMULd) THEN
       IF (pcLastProcResult8 = pc-4) AND (y.mode = fltMd)
                AND (y.FR = (codeB^[(pc-4) DIV 2] DIV LS9) MOD 8) THEN
          z := x; x := y; y := z;
       END;
   (*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    >>>ELSIF (op >= eql) & (op <= geq) THEN
    >>>  (* relational operators : *)
    >>>  IF x.mode = conMd THEN
    >>>    (* y.mode <> conMd ! *)
    >>>    z := x; x := y; y := z;
    >>>    IF    op = lss THEN op := gtr
    >>>    ELSIF op = leq THEN op := geq
    >>>    ELSIF op = gtr THEN op := lss
    >>>    ELSIF op = geq THEN op := leq
    >>>    ELSE (* op := op *)
    >>>    END;
    >>>  END;
    <<<<<<<<<<<<<<<<<<<<<<<<*)
    ELSE (* all other ops *)
    END;
  END PreLoadF;

  (* Restore register but don't destroy Cond Codes! *)
  (* This is almost exactly like M2HM.RestoreRegs *)
  PROCEDURE RestoreRegsSaveCC(saveSet : BITSET);
    (* restore the registers given by saveSet. *)
    VAR r, lr : Register; x, reglist, n : CARDINAL;
  BEGIN
    r := D0; n := 0; x := 1; reglist := 0;
    REPEAT (* from D0 up to SP-1 *)
      IF r IN saveSet THEN
        INC(n); lr := r;
        INC(reglist, x);
      END;
      INC(x, x); INC(r);
    UNTIL r = SP + 8;
    IF saveSet <> {} THEN (* Always use MOVEM so that Cond Codes Not Destroyed*)
      Put16(MOVEMINC); Put16(reglist);
    END;
  END RestoreRegsSaveCC;

 (*---------------------------------------------*)
  PROCEDURE FOp2(op : CARDINAL; VAR x, y : Item);
 (*---------------------------------------------*)
    CONST D0 = 0; D7 = 7;
    VAR regs : BITSET; z, z2 : Item; rtyp : StrPtr; sz : CARDINAL; Dn : Register;
        AlibOffset : CARDINAL;
  BEGIN
    CASE op OF
      (* define resulting type *)
      FADDs           : rtyp := realtyp; AlibOffset := SPAdd;
    | FSUBs           : rtyp := realtyp; AlibOffset := SPSub;
    | FMULs           : rtyp := realtyp; AlibOffset := SPMul;
    | FDIVs           : rtyp := realtyp; AlibOffset := SPDiv;
    | FREMs           : rtyp := realtyp; (* NOT IMPLEMENTED *)
    | FADDd           : rtyp := lrltyp;  AlibOffset := SPAdd;
    | FSUBd           : rtyp := lrltyp;  AlibOffset := SPSub;
    | FMULd           : rtyp := lrltyp;  AlibOffset := SPMul;
    | FDIVd           : rtyp := lrltyp;  AlibOffset := SPDiv;
    | FREMd           : rtyp := lrltyp;  (* NOT IMPLEMENTED *)
    | FCMPs           : rtyp := notyp;   AlibOffset := SPCmp;
    | FCMPd           : rtyp := notyp;   AlibOffset := SPCmp;
    END;

    (* Release Operands Temporarily to avoid unnecessarily saving registers. *)
    Release(x);
    Release(y);
    IF op >= FADDd THEN
      SaveAmigaDPRegs(regs);    (* save busy registers that AmigaLibs might zap *)
                                (* and also D2/D3 if busy *)
    ELSE
      SaveAmigaRegs(regs);       (* save busy registers that AmigaLibs might zap *)
    END;

    (* Reset registers used by operands to busy! *)
    IF x.mode IN ItSet{RindMd,RidxMd,AregMd,DregMd} THEN
      SetbusyReg(x.R);
    ELSIF x.mode = fltMd THEN
        SetbusyReg(x.FR);
        SetbusyReg(x.FR+1);
    END;
    IF x.mode = RidxMd THEN SetbusyReg(x.RX) END;
    IF y.mode IN ItSet{RindMd,RidxMd,AregMd,DregMd} THEN
      SetbusyReg(y.R);
    ELSIF y.mode = fltMd THEN
        SetbusyReg(y.FR);
        SetbusyReg(y.FR+1);
    END;
    IF y.mode = RidxMd THEN SetbusyReg(y.RX) END;

    (*SetregMd(z, D0, x.typ);*)
    SetfltMd(z, D0, x.typ); (* CAREFUL, if SPTst for FFP is added, its single param is in D1, not D0 *)

    PreLoadF(op, x, y);

    (* Another Peephole optimizer operation! (see also M2CM.GenAssign() *)
    (* Elliminate MOVE.L Result,Dn which is generated after each func
     * call (included system related functions calls) if redundant.
     * If the last instruction was MOVE.L Result(ie, D0),Dn and this one
     * will be MOVE.L Dn,D0 then don't generate this one *)
    (* This is worth it since probably every in three factor/term real expression
       there will be a hit here *)

    IF x.typ = realtyp THEN
       IF (pcLastProcResult = pc-2)
                AND (x.mode = DregMd)
                AND (x.R = (codeB^[(pc-2) DIV 2] DIV LS9) MOD 8)
                (*AND ((codeB^[(pc-2) DIV 2] MOD 8) = 0)*)
                AND NOT(Islocked(x.R)) THEN
          DEC(pc, 2);  (* zap the already generated MOVE.L instru *)
       ELSE
          FMove(x,z);           (* move parameter to D0 *)
                       (* CAREFUL, if FMove() changes this will BREAK *)
       END;
    ELSE
       IF (pcLastProcResult8 = pc-4)
                AND (x.mode = fltMd)
                AND (x.FR = (codeB^[(pc-4) DIV 2] DIV LS9) MOD 8)
                (*AND ((codeB^[(pc-4) DIV 2] MOD 8) = 0)*)
                AND NOT(Islocked(x.FR) AND NOT(Islocked(x.FR+1))) THEN
          DEC(pc, 4);  (* zap the two already generated MOVE.L instrus *)
       ELSE
          FMove(x,z);           (* move parameter to D0/D1 *)
                       (* CAREFUL, if FMove() changes this will BREAK *)
       END;
    END;

    Release(x);                   (* now release the x-parameter *)

    IF op < FADDd THEN                        (* NOTE: only for FOp2() ops *)
       SetfltMd(z2, D1, y.typ);
    ELSE
       SetfltMd(z2, D2, y.typ);
    END;

    FMove(y,z2);                  (* y-parameter into D1 or D2/D3 *)
    Release(y);                   (* now release the y-parameter  *)

    (* call to Amiga FPP or IEEE library *)
    Put16(MOVEAL +A6*LS9 +XXXL);         (* MOVE.L System.MathxBase.L,A6     *)

    (* System is mod #maxM-1 *)
    IF op >= FADDd THEN                        (* NOTE: only for FOp2() ops *)
      PutADRReloc(maxM - 1, MathIeeeDoubBasBaseVAR);
    ELSIF codeIEEE THEN
      PutADRReloc(maxM - 1, MathIeeeSingBasBaseVAR);
    ELSE
      PutADRReloc(maxM - 1, MathBaseVAR);
    END;

    Put16(JSR + AOFF+A6);                (* JSR _LVOx(A6)                   *)
    Put16(AlibOffset);

    (* This allows the Release(y) in GenOp() to do nothing! *)
    SetregMd(y, D0, rtyp);

    (* Caution : for FCMPs/FCMPd result is in the CCR :     *)
    (* -------   avoid the restoring of a single D-Register *)
    (*           (eventually done by M2HM.RestoreRegs)      *)
    (*           because this would destroy the CCR !       *)
    IF (regs # {}) THEN (* restore busy registers *)
      IF (regs * {D0..D7} # {}) AND (rtyp = notyp) THEN
        RestoreRegsSaveCC(regs); (* Does not destroy condition codes! *)
      ELSE
        RestoreRegs(regs);  (* normally restore busy registers *)
      END;
    END;
    
    (* result in reg D0 or D0/D1, must be copied to a pool register *)
    IF rtyp # notyp THEN
      sz := VAL(CARDINAL,rtyp^.size);
      IF NOT (sz IN {4,8}) THEN err(200); END;
      IF sz IN {4} THEN (* long result D0 *)
        GetReg(Dn,Dreg);
        SetregMd(x, Dn, rtyp);
        SetregMd(z, D0, rtyp);

        (* Remember position of MOVE.L D0, Dn *)
        pcLastProcResult := pc;

        Move(z,x);

      ELSE (* sz = 8, double-longword result D0/D1 *)
        GetRegPair(Dn);
        x.typ  := rtyp;
        x.mode := fltMd;
        x.FR   := Dn;
        z.typ  := rtyp;
        z.mode := fltMd;
        z.FR   := D0; (* D0/D1 *)

        (* Remember position of MOVE.L D0, Dn; MOVE.L D1,Dn+1 *)
        pcLastProcResult8 := pc;

        FMove(z,x);

      END;
    END;
  END FOp2;

  PROCEDURE FNeg1(VAR x : Item);
  BEGIN
    IF x.typ = realtyp THEN FOp1(FNEGs,x) ELSE FOp1(FNEGd,x) END;
  END FNeg1;
  
  PROCEDURE FAbs1(VAR x : Item);
  BEGIN
    IF x.typ = realtyp THEN FOp1(FABSs,x) ELSE FOp1(FABSd,x) END;
  END FAbs1;
  
  PROCEDURE Float1(VAR x : Item);
    (* Int/Double to Single-Real conversion : *)
  BEGIN
    LoadX(x,long);
    x.typ := realtyp; (* essential for FOp1 ! *)
    FOp1(FLOATs,x);
  END Float1;
  
  PROCEDURE Trunc1(VAR x : Item);
    (* Single-Real to Int/Double conversion : *)
  BEGIN
    FOp1(TRUNCs,x);
    (* M2TM defines resulting type : Int or LongInt *)
    LoadD(x); (* pop long value from stack *)
  END Trunc1;

  PROCEDURE FloatD1(VAR x : Item);
    (* Int/Double to Long-Real conversion : *)
  BEGIN
    LoadX(x,long);
    x.typ := realtyp; (* essential for FOp1 ! *)
    FOp1(FLOATd,x);
  END FloatD1;
  
  PROCEDURE TruncD1(VAR x : Item);
    (* Long-Real to Double conversion : *)
  BEGIN
    FOp1(TRUNCd,x);
  END TruncD1;

  PROCEDURE FLong(VAR x : Item);
    (* Single-Real to Long-Real conversion : *)
  BEGIN
    FOp1(FLONG,x);
  END FLong;

  PROCEDURE FShort(VAR x : Item);
    (* Long-Real to Single-Real conversion : *)
  BEGIN
    FOp1(FSHORT,x);
  END FShort;

  PROCEDURE FAdd2(VAR x, y : Item);
    VAR i : CARDINAL;
  BEGIN
    IF x.typ = realtyp THEN i := FADDs ELSE i := FADDd END;
    FOp2(i,x,y);
  END FAdd2;
  
  PROCEDURE FSub2(VAR x, y : Item);
    VAR i : CARDINAL;
  BEGIN
    IF x.typ = realtyp THEN i := FSUBs ELSE i := FSUBd END;
    FOp2(i,x,y);
  END FSub2;
  
  PROCEDURE FMul2(VAR x, y : Item);
    VAR i : CARDINAL;
  BEGIN
    IF x.typ = realtyp THEN i := FMULs ELSE i := FMULd END;
    FOp2(i,x,y);
  END FMul2;
  
  PROCEDURE FDiv2(VAR x, y : Item);
    VAR i : CARDINAL;
  BEGIN
    IF x.typ = realtyp THEN
      IF (y.mode = conMd) & (y.val.R = FltFloat(0)) THEN err(205) END;
      i := FDIVs;
    ELSE
      i := FDIVd;
    END;
    FOp2(i,x,y);
  END FDiv2;
  
  PROCEDURE FRem2(VAR x, y : Item);
    VAR i : CARDINAL;
  BEGIN
  (* NOTE: THIS IS USED ONLY BY STD FUNC REM(), WHICH IS NOT CURRENTLY
           IMPLEMENTED IN THIS COMPILER
  *)
    IF x.typ = realtyp THEN
      IF (y.mode = conMd) & (y.val.R = FltFloat(0)) THEN err(205) END;
      i := FREMs;
    ELSE
      i := FREMd;
    END;
    FOp2(i,x,y);
  END FRem2;

  PROCEDURE FCmp2(VAR x, y : Item);
    VAR i : CARDINAL;
  BEGIN
    IF x.typ = realtyp THEN i := FCMPs ELSE i := FCMPd END;
    FOp2(i,x,y);
  END FCmp2;

(*=================================*)
 PROCEDURE FltFloat(i:INTEGER):REAL;
(*=================================*)

BEGIN

IF codeIEEE THEN
   RETURN REAL(IEEESPFlt(i));
ELSE
   RETURN FLOAT(i);
END;

END FltFloat;

(*===============================*)
 PROCEDURE FltAdd(a, b:REAL):REAL;
(*===============================*)

BEGIN

IF codeIEEE THEN
   RETURN REAL(IEEESPAdd(a, b));
ELSE
   RETURN a+b;
END;

END FltAdd;

(*===============================*)
 PROCEDURE FltSub(a, b:REAL):REAL;
(*===============================*)

BEGIN

IF codeIEEE THEN
   RETURN REAL(IEEESPSub(a, b));
ELSE
   RETURN a-b;
END;

END FltSub;

(*===============================*)
 PROCEDURE FltMul(a, b:REAL):REAL;
(*===============================*)

BEGIN

IF codeIEEE THEN
   RETURN REAL(IEEESPMul(a, b));
ELSE
   RETURN a*b;
END;

END FltMul;

(*===============================*)
 PROCEDURE FltDiv(a, b:REAL):REAL;
(*===============================*)

BEGIN

IF codeIEEE THEN
   RETURN REAL(IEEESPDiv(a, b));
ELSE
   RETURN a/b;
END;

END FltDiv;

(*============================*)
 PROCEDURE FltNeg(a:REAL):REAL;
(*============================*)

BEGIN

IF codeIEEE THEN
   RETURN REAL(IEEESPNeg(a));
ELSE
   RETURN -a;
END;

END FltNeg;

(*============================*)
 PROCEDURE FltAbs(a:REAL):REAL;
(*============================*)

BEGIN

IF codeIEEE THEN
   RETURN REAL(IEEESPAbs(a));
ELSE
   RETURN ABS(a);
END;

END FltAbs;

(*==================================*)
 PROCEDURE FltCmp(a, b:REAL):INTEGER;
(*==================================*)
 (* 1 if a > b, 0 if a = b, -1 if a < b *)

BEGIN

IF codeIEEE THEN
   RETURN IEEESPCmp(a, b);
ELSE
   IF a > b THEN
      RETURN 1;
   ELSIF a < b THEN
      RETURN -1;
   ELSE
      RETURN 0;
   END;
END;

END FltCmp;

(*======================*)
 PROCEDURE FltMax():REAL;
(*======================*)

BEGIN

IF codeIEEE THEN
   RETURN REAL(LONG(07EFFH,0FFFFH));
ELSE
   RETURN MAX(REAL);
END;

END FltMax;

(*======================*)
 PROCEDURE FltMin():REAL;
(*======================*)

BEGIN

IF codeIEEE THEN
   RETURN REAL(LONG(0FEFFH,0FFFFH));
ELSE
   RETURN MIN(REAL);
END;

END FltMin;

(*-------------------------*)
 BEGIN (* module init code *)
(*-------------------------*)

IF MathIeeeSingBasBase = NIL THEN
   MathIeeeSingBasBase := OpenLibrary(ADR(IEEELibName), 0D);

   (* KLUDGE: TO BE CLEANED UP *)
   MathIEEESPBas.MathIeeeSingBasBase := (*System.*)MathIeeeSingBasBase;

END;
(* System module will close it *)

END M2FM.
