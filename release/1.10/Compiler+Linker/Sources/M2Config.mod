
(************************************************************************
 *                Modula-2 Software Construction Set                    *
 *                     (c) 1993 by Jim Olinger                          *
 ************************************************************************
 * Name: M2Config.MOD                  Version: Amiga.01.10             *
 * Created: 05/16/87   Updated: 03/21/94   Author: Leon Frenkel         *
 * Description: Change the default compiler options to new values.      *
 * Usage: M2Config [-s|-d] <M2_FileName>                                *
 *     = Input Configuration                                            *
 *  -s = Only Show Configuration                                        *
 *  -d = Set Default Configuration                                      *
 ************************************************************************)
 
 (* Version 1.01: 10/26/93 JKO
  *  1. Changed title.
  *  2. Changed "COMaxIdentBufSize" from 32766D to 65532D.
  *)

(***************************************************************************
 * Changes for new release 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.1/2 -----
 *
 * Add standard Amiga version strings
 *
 *   1. Added CONST AmigaStdVersion ="$VER: M2Config 1.01x (03.12.93)"
 *
 * Big IdBuffer
 *
 *   1. Change "CODIdentBufSize" to 40000D.
 *   2. Change "COMaxIdentBufSize" to 1000000D.
 *
 * "-2", 68020+ code gen
 *
 *   1. Add const "CODCode68020 = FALSE".
 *   2. Add "CompOptCode68020 := CODCode68020" to DefConfig().
 *   3. Add global variable "CompOptCode68020" just after CompOptOverflowChk.
 *   4. "CompOptCode68020 := BOOLEAN(getc(M2File))" in ReadConfig() just before
 *      reading CompOptOverflowChk.
 *   5. "i := putc(CHAR(CompOptCode68020), M2File)" in WriteConfig() just before
 *      writing CompOptOverflowChk.
 *   6. "argo[0].L := TorF(CompOptCode68020);"
 *      "printf("Generate M68020+ code..................%s\n");"
 *      in ShowConfig().
 *   7. "QueryBool(CompOptCode68020, "Overflow Checking")" in QueryConfig().
 *   8. Add getc(dummy) in ReadConfig() and WriteConfig() for ODD number of
 *      BOOLEAN config vars to align as compiler allocates them.
 *
 * "-e" for IEEE Single Precision REAL option
 *
 *   1. Add const "CODCodeIEEE = FALSE".
 *   2. Add "CompOptCodeIEEE := CODCodeIEEE" to DefConfig().
 *   3. Add global variable "CompOptCodeIEEE" just after CompOptCode68020.
 *   4. "CompOptCodeIEEE := BOOLEAN(getc(M2File))" in ReadConfig() just before
 *      reading CompOptCode68020.
 *      Removed getc(dummy) in ReadConfig() (before CompOpt68020 read) and
 *      WriteConfig() (before CompOpt68020 write) since now
 *      number of BOOLEAN config vars is even and no need to align as compiler
 *      allocates them.
 *   5. "i := putc(CHAR(CompOptCodeIEEE), M2File)" in WriteConfig() just before
 *      writing CompOptCode68020.
 *   6. "argo[0].L := TorF(CompOptCodeIEEE);"
 *      "printf("Generate IEEE REALs code.............%s\n");"
 *      in ShowConfig().
 *   7. "QueryBool(CompOptCodeIEEE, "Generate IEEE REALs code" in QueryConfig().
 *
 ***************************************************************************
*)

(* NOTE: this traps if checking is "on" due to putw() stupidity *)

(*$L-,D- *)
MODULE M2Config;

FROM SYSTEM IMPORT
	ADDRESS, BYTE,
	ADR, TSIZE;
FROM Conversions IMPORT
	ConvStringToNumber;
IMPORT CPrintStdOut;
FROM CStdIO IMPORT
	FILE,
	openstdio, closestdio,
	fopen, fclose,
	fseek, ftell,
	getc, getw, fgets, getchar, ungetc,
	putc, putw;
FROM CStrings IMPORT
	strcmp;
FROM FormatString IMPORT
	FormatArg;
FROM System IMPORT
	argc, argv;

CONST

  Title =
    "M2SCS: Compiler Config v1.10 Copyright \xA9 1993 by Jim Olinger\n";
  AmigaStdVersion ="$VER: M2Config 1.10 (21.3.94)";

  MsgUsage = "Usage: M2Config [-d|-s] <M2_FileName>";

  M2Hunks = 16D;	(* Hunks in a M2 compiler executable *)

  HUNKHeader  = LONGCARD(03F3H);
  HUNKCode    = LONGCARD(03E9H);
  HUNKReloc32 = LONGCARD(03ECH);
  HUNKSymbol  = LONGCARD(03F0H);
  HUNKDebug   = LONGCARD(03F1H);
  HUNKEnd     = LONGCARD(03F2H);

  (* (C)ompiler(O)ption(D)efault... *)
  CODHeapSize          = 150000D;
  CODCodeBufSize       = 32766D;
  CODConstBufSize      = 32766D;
  CODIdentBufSize      = 40000D;
  CODAdrRelocBufSize   = 1000D;
  CODGenRefFile        = FALSE;
  CODRangeChk          = FALSE;
  CODOverflowChk       = FALSE;
  CODDiskBufSize       = 1024D;
  CODCopyDynValueParms = TRUE;
  CODClrModuleKey      = FALSE;
  CODGlobalVarAbsAdr   = FALSE;
  CODCode68020         = FALSE;
  CODCodeIEEE          = FALSE;

  COMinHeapSize        = 10000D;
  COMaxHeapSize        = 0FFFFFFFFH;
  COMinCodeBufSize     = 1000D;
  COMaxCodeBufSize     = 32766D;
  COMinConstBufSize    = 1000D;
  COMaxConstBufSize    = 32766D;
  COMinIdentBufSize    = 1000D;
  (** COMaxIdentBufSize    = 32766D; **)
  COMaxIdentBufSize    = 1000000D;
  COMinDiskBufSize     = 512D;
  COMaxDiskBufSize     = 0FFFFFFFFH;
  COMinAdrRelocBufSize = 100D;
  COMaxAdrRelocBufSize = 32766D;

TYPE
  CmdMode = (Config, Show, Defaults);

VAR
  M2FileName  : ADDRESS;
  NameArg     : CARDINAL;
  Mode        : CmdMode;
  M2File      : FILE;
  argo        : ARRAY [0..9] OF FormatArg;
  ch          : CHAR;
  CompOptInitialized     : BOOLEAN; (* TRUE = initialize, FALSE = not init *)
  CompOptGlobalVarAbsAdr : BOOLEAN;
  CompOptGenRefFile      : BOOLEAN;
  CompOptClrModuleKey    : BOOLEAN;
  CompOptRangeChk        : BOOLEAN;
  CompOptOverflowChk     : BOOLEAN;
  CompOptCode68020       : BOOLEAN;
  CompOptCodeIEEE        : BOOLEAN;
  CompOptDiskBufSize     : LONGCARD;
  CompOptAdrRelocBufSize : LONGCARD;
  CompOptIdentBufSize    : LONGCARD;
  CompOptConstBufSize    : LONGCARD;
  CompOptCodeBufSize     : LONGCARD;
  CompOptHeapSize        : LONGCARD;

(* printf - a short form for printf *)
PROCEDURE printf(s: ARRAY OF CHAR);
BEGIN
  CPrintStdOut.printf(ADR(s), argo);
END printf;

(* Init - initialize program *)
PROCEDURE Init();
BEGIN
  openstdio();
END Init;

(* Cleanup - cleanup before exiting *)
PROCEDURE Cleanup();
VAR b : BOOLEAN;
BEGIN
  IF (M2File # NIL) THEN
    b := fclose(M2File);
  END;
  closestdio();
END Cleanup;

PROCEDURE fatal(s: ARRAY OF CHAR);
BEGIN
  argo[0].L := ADR(s);
  printf("ERROR: %s\n");
  Cleanup();
  HALT;
END fatal;

(* getl - get a long from a file *)
PROCEDURE getl(stream: FILE): LONGCARD;
VAR
  conv : RECORD
           CASE : CARDINAL OF 
           | 0: h, l : CARDINAL;
           | 1: hl   : LONGCARD;
           END;
         END;
BEGIN
  conv.h := getw(stream);
  conv.l := getw(stream);
  RETURN (conv.hl);
END getl;

(* putl - put a long to a file *)
PROCEDURE putl(long: LONGCARD; stream: FILE);
VAR
  r : INTEGER;
  conv : RECORD
           CASE : CARDINAL OF 
           | 0: h, l : CARDINAL;
           | 1: hl   : LONGCARD;
           END;
         END;
BEGIN
  conv.hl := long;
  r := putw(conv.h, stream);
  r := putw(conv.l, stream);
END putl;

PROCEDURE ReadString(VAR s: ARRAY OF CHAR);
VAR i : CARDINAL; ch : CHAR;
BEGIN
  i := 0;
  LOOP
    ch := CHR(getchar());
    IF (ch = "\n") THEN EXIT; END;
    s[i] := CAP(ch);
    INC(i);
  END;
  s[i] := 0C;
END ReadString;

PROCEDURE TorF(bool: BOOLEAN): ADDRESS;
BEGIN
  IF bool THEN
    RETURN ADR("TRUE");
  ELSE
    RETURN ADR("FALSE");
  END;
END TorF;

PROCEDURE QueryInt(
             VAR val: LONGCARD; min, max: LONGCARD; text: ARRAY OF CHAR
	  );
VAR
  in : ARRAY [0..127] OF CHAR;
  num: LONGCARD;
BEGIN
  argo[0].L := ADR(text);
  argo[1].L := val;
  LOOP
    printf("%s [%lu]: ");
    ReadString(in);
    IF (in[0] = 0C) THEN EXIT; END; (* No Change *)
    IF (ConvStringToNumber(in,num,FALSE,10)) THEN
      IF (num < min) THEN
        printf("Input Too Small!\n");
      ELSIF (num > max) THEN
        printf("Input Too Large!\n");
      ELSE
        val := num; EXIT; 
      END;
    ELSE
      printf("Invalid Input!\n");
    END;
  END;
END QueryInt;

PROCEDURE QueryBool(VAR flag: BOOLEAN; text: ARRAY OF CHAR);
VAR
  in : ARRAY [0..127] OF CHAR;
  t, f : BOOLEAN;
BEGIN
  argo[0].L := ADR(text);
  argo[1].L := TorF(flag);
  LOOP
    printf("%s [%s]: ");
    ReadString(in);
    IF (in[0] = 0C) THEN EXIT; END; (* No Change *)
    t := strcmp(ADR(in), ADR("TRUE")) = 0;
    f := strcmp(ADR(in), ADR("FALSE")) = 0;
    IF (t = TRUE) THEN
      flag := TRUE; EXIT;
    ELSIF (f = TRUE) THEN
      flag := FALSE; EXIT;
    ELSE
      printf("Invalid Input!\n");
    END;
  END;
END QueryBool;

PROCEDURE ReadCodeBlock();
VAR
  b : BOOLEAN;
  l,l1 : LONGCARD;
BEGIN
  IF (getl(M2File) # HUNKCode) THEN
    fatal("M2 File Invalid!");
  END;

  (* Skip over body of code hunk *)
  b := fseek(M2File, getl(M2File) * 4D, 1);

  LOOP
    l := getl(M2File);
    IF (l = HUNKEnd) THEN 
      EXIT; (* Done. *)
    ELSIF (l = HUNKReloc32) THEN
      LOOP
        l := getl(M2File); 		(* # Offsets *)
        IF (l = 0D) THEN EXIT; END;	(* Offsets = 0 then done. *)
        l1 := getl(M2File);		(* Hunk ID *)
        WHILE (l > 0D) DO		(* Read Offsets *)
          l1 := getl(M2File);
          DEC(l);
        END;
      END;
    ELSIF (l = HUNKDebug) THEN
      b := fseek(M2File, getl(M2File) * 4D, 1); (* skip over debug data *)
    ELSIF (l = HUNKSymbol) THEN
      LOOP
        l := getl(M2File);		(* Name Length *)
        IF (l = 0D) THEN EXIT; END; 	(* Done *)
        WHILE (l > 0D) DO
          l1 := getl(M2File);
          DEC(l);
        END;
        l := getl(M2File);		(* Value *)
      END;
    ELSE
      fatal("M2 File Invalid!");
    END;
  END;
END ReadCodeBlock;

PROCEDURE FindConfigHunk();
VAR
  l : LONGCARD;
  TableSize : LONGCARD;
BEGIN
  IF (getl(M2File) # HUNKHeader) THEN
    fatal("M2 File Not An Executable File");
  END;

  l := getl(M2File);	(* Resident Libs (should be 0) *)
  TableSize := getl(M2File);
  IF (TableSize # M2Hunks) THEN
    printf("WARNING: M2 File Has More Hunks Than M2Hunks!\n");
  END;

  l := getl(M2File);	(* First Hunk *)
  l := getl(M2File);	(* Last Hunk *)

  (* Skip over Hunk Table *)
  WHILE (TableSize > 0D) DO
    l := getl(M2File);
    DEC(TableSize);
  END;

  ReadCodeBlock();
  IF (getl(M2File) # HUNKCode) THEN
    fatal("M2 File Invalid!");
  END;
  l := getl(M2File); (* Code Hunk Size *)

END FindConfigHunk;

PROCEDURE ReadConfig();
VAR
  pos : LONGINT;
  b : BOOLEAN;
BEGIN
  pos := ftell(M2File);			(* Remember position *)
  CompOptHeapSize        := getl(M2File);
  CompOptCodeBufSize     := getl(M2File);
  CompOptConstBufSize    := getl(M2File);
  CompOptIdentBufSize    := getl(M2File);
  CompOptAdrRelocBufSize := getl(M2File);
  CompOptDiskBufSize     := getl(M2File);
  CompOptCodeIEEE        := BOOLEAN(getc(M2File));
  CompOptCode68020       := BOOLEAN(getc(M2File));
  CompOptOverflowChk     := BOOLEAN(getc(M2File));
  CompOptRangeChk        := BOOLEAN(getc(M2File));
  CompOptClrModuleKey    := BOOLEAN(getc(M2File));
  CompOptGenRefFile      := BOOLEAN(getc(M2File));
  CompOptGlobalVarAbsAdr := BOOLEAN(getc(M2File));
  CompOptInitialized     := BOOLEAN(getc(M2File));

  b := fseek(M2File, pos, 0);		(* Restore to beginning of config *)
END ReadConfig;

PROCEDURE WriteConfig();
VAR i : INTEGER;
BEGIN
  CompOptInitialized := TRUE;
  putl(CompOptHeapSize, M2File);
  putl(CompOptCodeBufSize, M2File);
  putl(CompOptConstBufSize, M2File);
  putl(CompOptIdentBufSize, M2File);
  putl(CompOptAdrRelocBufSize, M2File);
  putl(CompOptDiskBufSize, M2File);
  i := putc(CHAR(CompOptCodeIEEE), M2File);
  i := putc(CHAR(CompOptCode68020), M2File);
  i := putc(CHAR(CompOptOverflowChk), M2File);
  i := putc(CHAR(CompOptRangeChk), M2File);
  i := putc(CHAR(CompOptClrModuleKey), M2File);
  i := putc(CHAR(CompOptGenRefFile), M2File);
  i := putc(CHAR(CompOptGlobalVarAbsAdr), M2File);
  i := putc(CHAR(CompOptInitialized), M2File);
END WriteConfig;

PROCEDURE DefConfig();
BEGIN
  (* Set defaults for Compiler Options *)
  CompOptHeapSize        := CODHeapSize;
  CompOptCodeBufSize     := CODCodeBufSize;
  CompOptConstBufSize    := CODConstBufSize;
  CompOptIdentBufSize    := CODIdentBufSize;
  CompOptDiskBufSize     := CODDiskBufSize;
  CompOptAdrRelocBufSize := CODAdrRelocBufSize;
  CompOptRangeChk        := CODRangeChk;
  CompOptOverflowChk     := CODOverflowChk;
  CompOptGlobalVarAbsAdr := CODGlobalVarAbsAdr;
  CompOptGenRefFile      := CODGenRefFile;
  CompOptClrModuleKey    := CODClrModuleKey;
  CompOptCode68020       := CODCode68020;
  CompOptCodeIEEE        := CODCodeIEEE;
END DefConfig;

PROCEDURE ShowConfig();
BEGIN
  argo[0].L := CompOptHeapSize;
  printf("Heap Size..............................%lu\n");

  argo[0].L := CompOptCodeBufSize;
  printf("Code Buffer Size.......................%lu\n");

  argo[0].L := CompOptConstBufSize;
  printf("Constant Buffer Size...................%lu\n");

  argo[0].L := CompOptIdentBufSize;
  printf("Identifier Buffer Size.................%lu\n");

  argo[0].L := CompOptDiskBufSize;
  printf("Disk Buffer Size.......................%lu\n");

  argo[0].L := CompOptAdrRelocBufSize;
  printf("Address Relocation Buffer Size.........%lu\n");

  argo[0].L := TorF(CompOptRangeChk);
  printf("Range Checking.........................%s\n");

  argo[0].L := TorF(CompOptOverflowChk);
  printf("Overflow Checking......................%s\n");

  argo[0].L := TorF(CompOptCode68020);
  printf("Generate M68020 code...................%s\n");

  argo[0].L := TorF(CompOptCodeIEEE);
  printf("Generate IEEE REALs code...............%s\n");

  argo[0].L := TorF(CompOptGlobalVarAbsAdr);
  printf("Global Variables Absolute Addressing...%s\n");

  argo[0].L := TorF(CompOptGenRefFile);
  printf("Generate Reference File................%s\n");

  argo[0].L := TorF(CompOptClrModuleKey);
  printf("Clear Module Key.......................%s\n");
END ShowConfig;

PROCEDURE QueryConfig();
BEGIN
  QueryInt(CompOptHeapSize,COMinHeapSize,COMaxHeapSize,
           "Heap Size");
  QueryInt(CompOptCodeBufSize,COMinCodeBufSize,COMaxCodeBufSize,
           "Code Buffer Size");
  QueryInt(CompOptConstBufSize,COMinConstBufSize,COMaxConstBufSize,
           "Constant Buffer Size");
  QueryInt(CompOptIdentBufSize,COMinIdentBufSize,COMaxIdentBufSize,
           "Identifier Buffer Size");
  QueryInt(CompOptDiskBufSize,COMinDiskBufSize,COMaxDiskBufSize,
           "Disk Buffer Size");
  QueryInt(CompOptAdrRelocBufSize,COMinAdrRelocBufSize,COMaxAdrRelocBufSize,
           "Address Relocation Buffer Size");
  QueryBool(CompOptRangeChk, "Range Checking");
  QueryBool(CompOptOverflowChk, "Overflow Checking");
  QueryBool(CompOptCode68020, "Generate M68020 code");
  QueryBool(CompOptCodeIEEE, "Generate IEEE REALs code");
  QueryBool(CompOptGlobalVarAbsAdr, "Global Variables Absolute Addressing");
  QueryBool(CompOptGenRefFile, "Generate Reference File");
  QueryBool(CompOptClrModuleKey, "Clear Module Key");
END QueryConfig;

PROCEDURE ConfigCompiler();
BEGIN
  M2File := fopen(M2FileName, ADR("r+")); (* Open for read/write *)
  IF (M2File = NIL) THEN
    fatal("M2 File Not Opened!");
  END;

  FindConfigHunk();
  ReadConfig();
  IF (Mode = Defaults) OR ((Mode = Config) AND (NOT CompOptInitialized)) THEN
    DefConfig();
  END;
  argo[0].L := M2FileName;
  printf("Compiler FileName: %s\n");
  ShowConfig();
  IF (Mode = Defaults) OR ((Mode = Config) AND (NOT CompOptInitialized)) THEN
    printf("=== Default Configuration Set ===\n");
  END;
  IF (Mode # Show) THEN
    IF (Mode = Config) THEN
      QueryConfig();
    END;
    WriteConfig();
  END;

  IF NOT fclose(M2File) THEN
    M2File := NIL;
    fatal("Error Writing To M2 File!");
  END;
  M2File := NIL;
END ConfigCompiler;

BEGIN
  Init();

  printf(Title);
  IF (argc < 2) OR (argc > 3) THEN
    fatal(MsgUsage);
  END;

  Mode := Config;
  NameArg := 1;
  IF (argc = 3) THEN
    ch := CAP(argv^[1]^[1]);
    IF ((argv^[1]^[0] # "-") OR (argv^[1]^[2] # 0C) OR
        ( (ch # "S") AND (ch # "D") ) ) THEN
      fatal(MsgUsage);
    END;

    IF (ch = "S") THEN (* -s, Show Option *)
      Mode := Show;
    ELSE (* -d, Set Defaults Option *)
      Mode := Defaults;
    END;
    NameArg := 2;
  END;

  M2FileName := argv^[NameArg];
  ConfigCompiler();

  Cleanup();
END M2Config.
