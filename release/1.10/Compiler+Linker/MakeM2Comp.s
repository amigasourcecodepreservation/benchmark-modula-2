copy b:M2 t:
copy b:m2lk t:
Assign bb: QU1:Armadillo/Benchmark.wrk/v1.10

copy bb:system.sbm rad:
copy bb:system.obm rad:
copy bb:systemffp.obm rad:
copy work:3.0/syms/mathieeespbas.sbm rad:
copy work:3.0/objs/mathieeespbas.obm rad:
copy work:3.0/syms/mathieeedpbas.sbm rad:
copy work:3.0/objs/mathieeedpbas.obm rad:

assign m2l: bb: b:m2l
t:M2 -e- -2- -r- -v- -g+ bb:M2DM.DEF 
t:M2 -e- -2- -r- -v- -g+ bb:M2SM.DEF 
t:M2 -e- -2- -r- -v- -g+ bb:M2TM.DEF 
t:M2 -e- -2- -r- -v- -g+ bb:M2RM.DEF 
t:M2 -e- -2- -r- -v- -g+ bb:M2LM.DEF 
t:M2 -e- -2- -r- -v- -g+ bb:M2HM.DEF 
t:M2 -e- -2- -r- -v- -g+ bb:M2EM.DEF 
t:M2 -e- -2- -r- -v- -g+ bb:M2CM.DEF 
t:M2 -e- -2- -r- -v- -g+ bb:M2FM.DEF 
t:M2 -e- -2- -r- -v- -g+ bb:M2PM.DEF 
t:M2 -e- -2- -r- -v- -g+ bb:M2.DEF 
t:M2 -e- -2- -r- -v- -g+ bb:M2.MOD 
t:M2 -e- -2- -r- -v- -g+ bb:M2DM.MOD 
t:M2 -e- -2- -r- -v- -g+ bb:M2SM.MOD 
t:M2 -e- -2- -r- -v- -g+ bb:M2TM.MOD 
t:M2 -e- -2- -r- -v- -g+ bb:M2RM.MOD 
t:M2 -e- -2- -r- -v- -g+ bb:M2LM.MOD 
t:M2 -e- -2- -r- -v- -g+ bb:M2HM.MOD 
t:M2 -e- -2- -r- -v- -g+ bb:M2EM.MOD 
t:M2 -e- -2- -r- -v- -g+ bb:M2CM.MOD 
t:M2 -e- -2- -r- -v- -g+ bb:M2FM.MOD 
t:M2 -e- -2- -r- -v- -g+ bb:M2PM.MOD
t:M2LK -g M2

Assign bb: aa:benchmark.wrk
