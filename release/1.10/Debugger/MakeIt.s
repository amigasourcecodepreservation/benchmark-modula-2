;
; MakeIt.s
;  04/03/94
;
; Script file for creating M2Debug v01.10
;

cd RAD:
ASSIGN m2l: b:m2l b:CLIB b:m2l/Simple 

copy bb:System.sbm RAD:
copy bb:System.obm RAD:

;M2 dd:XText.def
;net:fh2/assembler/assem -i aa:AsmInterface.wrk,sc:include -o RAD:XText.o dd:XText.a
;sc:c/slink from xtext.o to xtext.cod LIB sc:lib/Amiga.lib
;M2GENOBM -m XText
copy dd:XText.sbm RAD:
copy dd:Xtext.obm RAD:

copy dd:MISC/DBSaveRegs.SBM RAD:
copy dd:MISC/DBSaveRegs.OBM RAD:
copy dd:MISC/M2Debug.config RAD:

copy work:3.0/syms/Exec.sbm RAD:
copy work:3.0/objs/Exec.obm RAD:
;copy work:3.0/syms/ExecBase.sbm RAD:
;copy work:3.0/objs/ExecBase.obm RAD:
;copy work:3.0/syms/IFFParse.sbm rad:
;copy work:3.0/objs/IFFParse.obm rad:
copy work:3.0/syms/MathIEEESPBas.sbm rad:
copy work:3.0/objs/MathIEEESPBas.obm rad:
copy work:3.0/syms/MathFFP.sbm rad:
copy work:3.0/objs/MathFFP.obm rad:

M2 -k work:3.0/defs/Utility.def
copy work:3.0/objs/Utility.obm rad:
work:3.0/utilpgms/m2key -obm -jam 0 0 0 Utility.obm
M2 -k work:3.0/defs/IFFParse.def
copy work:3.0/objs/IffParse.obm rad:
work:3.0/utilpgms/m2key -obm -jam 0 0 0 IFFParse.obm
M2 -k work:3.0/defs/AmigaDOSTags.def
copy work:3.0/objs/AmigaDOSTags.obm rad:
work:3.0/utilpgms/m2key -obm -jam 0 0 0 AmigaDOSTags.obm

M2 dd:XTextUtil.def
M2 dd:XTextUtil.mod

;M2 aa:def.wrk/RealOps.def
;M2 aa:mod.wrk/RealOps.mod
copy bb:RealOps.sbm rad:
copy bb:RealOps.obm rad:
;M2 dd:RealConversionsSwitch.MOD
copy dd:RealConversionsSwitch.OBM rad:RealConversions.OBM
copy bb:LongRealConversions.sbm rad:
copy dd:LongRealConversionsLate.obm rad:LongRealConversions.obm

M2 <dd:MISC/make -s dd:

copy bb:SystemFFP.obm RAD:
copy dd:SystemIEEENull.obm RAD:SystemIEEE.obm
copy dd:SystemIEEEdpNull.obm RAD:SystemIEEEdp.obm
;copy bb:SystemIEEE.obm RAD:SystemIEEE.obm
;copy bb:SystemIEEEdp.obm RAD:SystemIEEEdp.obm

; NOTE: CASE IS IMPORTANT HERE SINCE m2lk ALLOWS ONLY "M2Debug" TO DO BOTH REALS
bb:m2lk M2Debug

delete rad:RealConversions.OBM
delete RAD:SystemIEEE.obm
delete RAD:SystemIEEEdp.obm
