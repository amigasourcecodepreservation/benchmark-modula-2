(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBDirectoryWindows.MOD            Version: Amiga.01.10             *
 * Created: 02/22/89   Updated: 03/26/89   Author: Leon Frenkel             *
 * Description: Debugger directory windows handling module.                 *
 ****************************************************************************)

IMPLEMENTATION MODULE DBDirectoryWindows;

FROM SYSTEM IMPORT
  ADDRESS, BYTE,
  ADR;
FROM AmigaDOS IMPORT
  FileLock, FileInfoBlock, FileInfoBlockPtr, SharedLock,
  Lock, UnLock, Examine, ExNext;
FROM AmigaDOSExt IMPORT
  DosLibraryPtr, RootNodePtr, DosInfoPtr, DeviceListPtr, 
  DLTVolume, DLTDirectory;
FROM CPrintBuffer IMPORT
  sprintf;
FROM CStrings IMPORT
  strlen, strcpy, strcat;
FROM DBConfiguration IMPORT
  ConfigWindow;
FROM DBMenusKbd IMPORT
  DoGlobalMenuCommand, DoGlobalKbdCommand, LinkGlobalMenuStrip,
  AddMenuItemNoHighlight;
FROM DBMisc IMPORT
  AllocMemory, DeallocMemory, AllocString, DeallocString, BPTRtoAPTR;
FROM DBRequesters IMPORT
  RequestTextFgBgColors;
FROM DBSelection IMPORT
  SelectionData,
  ClearSelection, GetSelection, SetSelection, GetSelectionLineInfo,
  IsPosSelected, IfCurrentWindowClearSelection;
FROM DBStatusWindow IMPORT
  ShowErrMsgNoMemory, ShowErrMsgBadDest,
  ShowError;
FROM DBTextWindows IMPORT
  DefMinWindowWidth, DefMinWindowHeight, DefMaxWindowWidth, DefMaxWindowHeight,
  VTWindow, NewVTWindow, VTWindowNIL, VTWindowParms,
  OpenVTWindow, CloseVTWindow, InitParmsVTWindow, SetCursorPosVTWindow,
  GetUserDataVTWindow, ClearVTWindow, RefreshVTWindow, WriteStringVTWindow,
  SetTitleVTWindow, WriteColorVTWindow, GetInfoVTWindow, SetMenuStripVTWindow,
  ClearMenuStripVTWindow, GetDetailPenBlockPenVTWindow, 
  SetDetailPenBlockPenVTWindow, SetDefaultColorVTWindow, 
  SetSizeVerifyModeAllVTWindows;
FROM DBWindows IMPORT
  WindowClass,
  GetVTWindowsMsgPort;
FROM FormatString IMPORT
  FormatArg;
FROM Interrupts IMPORT
  Disable, Enable;
FROM Intuition IMPORT
  Menu, MenuPtr;
FROM Memory IMPORT
  MemReqSet, MemClear;
FROM SimpleMenus IMPORT
  BeginMenuStrip, EndMenuStrip, FreeMenuStrip, AddMenu, AddMenuItem;
FROM System IMPORT
  DOSBase;


CONST
  DirectoryListTitle = "Directory List";       (* Win title for showing dirs *)
  VolumeListTitle    = "Volumes/Assigns List"; (* Win title for showing vols *)

  KeyGotoVolumes = CHAR(27); (* ESC *)

  LocalMenus = 1; (* Number of window specific menus *)

  (* First item in directory list which is used to return to parent dir *)
  ParentDirString = "<RETURN TO PARENT DIRECTORY>";

  MinVolumeWidth = 40; (* Minimum number of chars for volume list *)
  FileInfoWidth  = 11; (* Number of chars to reserve for file size info *)

  MaxPathLength = 512; (* Maximum filepath string length *)

  DirErrMsgReadDir = "Reading directory.";

TYPE
  (* Directory window line type code *)
  LineType = (LTParent, LTFile, LTDirectory, LTVolume);

  (* One text display line descriptor *)
  TextLinePtr = POINTER TO TextLine;
  TextLine = RECORD
               tlNext     : TextLinePtr; (* ptr to next line or NIL *)
               tlType     : LineType;    (* type of line for user selection *)
               tlString   : ADDRESS;     (* ptr to a line text string *)
               tlSize     : LONGCARD;    (* file size value *)
              END;

  (* Directory window display mode *)
  DisplayMode = (DMVolumes, DMDirectory);

  (* Directory window descriptor *)
  DirectoryWindowPtr = POINTER TO DirectoryWindow;
  DirectoryWindow = RECORD
                      dwNext          : DirectoryWindowPtr; (* Next window *)
                      dwVTWindow      : VTWindow;      (* text window *)
                      dwVTWindowParms : VTWindowParms; (* window parms *)
                      dwLineList      : TextLinePtr;   (* ptr to list of lines*)
                      dwFileInfoPos   : CARDINAL;      (* x pos for file size *)
                      dwDisplayMode   : DisplayMode;   (* display type *)
                      dwDirectoryPath : ADDRESS;       (* current filepath *)
                    END;

VAR
  DirectoryWindowList : DirectoryWindowPtr; (* list of directory windows *)
  DirectoryMenuStrip  : MenuPtr;            (* directory window menu strip *)
  SelectedFilePath    : ADDRESS;            (* ptr to selected path string *)

(* ===== PRIVATE ===== *)

(* SetNewSelectedFilePath - delete old string and allocate new string *)
PROCEDURE SetNewSelectedFilePath(path: ADDRESS): ADDRESS;
BEGIN
  IF (SelectedFilePath # NIL) THEN
    DeallocString(SelectedFilePath);
  END;
  SelectedFilePath := AllocString(path);
  RETURN (SelectedFilePath);
END SetNewSelectedFilePath;

(* DeleteLineList - dealloc list of text line records *)
PROCEDURE DeleteLineList(list: TextLinePtr);
VAR
  f: TextLinePtr;
BEGIN
  WHILE (list # NIL) DO
    IF (list^.tlString # NIL) THEN
      DeallocString(list^.tlString);
    END;
    f    := list;
    list := list^.tlNext;
    DeallocMemory(f);
  END;
END DeleteLineList;

(* AddTextLine - allocate a new text line record and link into list *)
PROCEDURE AddTextLine(VAR listHead, listTail: TextLinePtr; 
                      type: LineType; str: ADDRESS; size: LONGCARD): BOOLEAN;
VAR
  tl: TextLinePtr;
  newStr: ADDRESS;
BEGIN
  newStr := AllocString(str);
  IF (newStr # NIL) THEN
    tl := AllocMemory(SIZE(TextLine), MemReqSet{});
    IF (tl # NIL) THEN
      IF (listHead = NIL) THEN
        listHead := tl;
      ELSE
        listTail^.tlNext := tl;
      END;
      listTail := tl;

      WITH tl^ DO
        tlNext   := NIL;
        tlType   := type;
        tlString := newStr;
        tlSize   := size;
      END;
    ELSE
      DeallocString(newStr);
    END;
  END;
  RETURN (tl # NIL);
END AddTextLine;

(* CopyVolName - copy a volume name BSTR to an output string *)
PROCEDURE CopyVolName(src: ADDRESS; dst: ADDRESS);
VAR
  length: CARDINAL;
BEGIN
  length := CARDINAL(src^); INC(src);
  WHILE (length # 0) DO
    dst^ := src^;
    INC(src); INC(dst);
    DEC(length);
  END;
  dst^ := ":"; INC(dst);
  dst^ := 0C;
END CopyVolName;

(* CreateVolumeLineList - create a list of currently mounted volumes *)
PROCEDURE CreateVolumeLineList(VAR newList: TextLinePtr);
VAR
  volListHead: TextLinePtr;
  volListTail: TextLinePtr;
  ok: BOOLEAN;
  DosBaseAdr: DosLibraryPtr;
  DosRoot: RootNodePtr;
  DosInfo: DosInfoPtr;
  DevList: DeviceListPtr;
  type: LONGINT;
  volName: ARRAY [0..255] OF CHAR;
BEGIN
  ok          := FALSE;
  volListHead := NIL;

  Disable();

  DosBaseAdr := DOSBase;
  DosRoot    := DosBaseAdr^.dlRoot;
  DosInfo    := DosInfoPtr(BPTRtoAPTR(DosRoot^.rnInfo));
  DevList    := DeviceListPtr(BPTRtoAPTR(DosInfo^.diDevInfo));

  LOOP
    IF (DevList = NIL) THEN
      ok := TRUE;
      EXIT; (* Success! *)
    END;

    type := DevList^.dlType;
    IF (type = DLTVolume) OR (type = DLTDirectory) THEN
      CopyVolName(BPTRtoAPTR(DevList^.dlName), ADR(volName[0]));
      IF (NOT AddTextLine(volListHead,volListTail,LTVolume,ADR(volName[0]),0D)) THEN
        EXIT; (* Error! *)
      END;
    END;

    DevList := DeviceListPtr(BPTRtoAPTR(DevList^.dlNext));
  END; (* LOOP *)

  Enable();

  IF (NOT ok) THEN
    DeleteLineList(volListHead);
    volListHead := NIL;
    ShowError(ShowErrMsgNoMemory);
  END;
  newList := volListHead;
END CreateVolumeLineList;

(* CreateDirectoryLineList - create a list of files/dirs in specified dir *)
PROCEDURE CreateDirectoryLineList(dirPath: ADDRESS; VAR newList: TextLinePtr);
VAR
  dirListHead: TextLinePtr;
  dirListTail: TextLinePtr;
  fib: FileInfoBlockPtr;
  lock: FileLock;
  type: LineType;
  ok: BOOLEAN;
  noMemErr: BOOLEAN;
  diskErr: BOOLEAN;
BEGIN
  SetSizeVerifyModeAllVTWindows(FALSE); (* Disable size verify *)

  ok          := FALSE;
  dirListHead := NIL;
  fib         := NIL;
  lock        := NIL;
  noMemErr    := FALSE;
  diskErr     := FALSE;

  LOOP
    fib := AllocMemory(SIZE(FileInfoBlock), MemReqSet{});
    IF (fib = NIL) THEN
      noMemErr := TRUE;
      EXIT; (* Error! *)
    END;

    lock := Lock(dirPath, SharedLock);
    IF (lock = NIL) THEN
      diskErr := TRUE;
      EXIT; (* Error! *)
    END;

    IF (NOT Examine(lock, fib^)) OR (NOT (fib^.fibDirEntryType > 0D)) THEN
      diskErr := TRUE;
      EXIT; (* Error! *)
    END;

    IF (NOT AddTextLine(dirListHead,dirListTail,LTParent,ADR(ParentDirString),0D)) THEN
      noMemErr := TRUE;
      EXIT; (* Error! *)
    END;

    WHILE (ExNext(lock, fib^)) DO
      WITH fib^ DO
        IF (fibDirEntryType < 0D) THEN (* FileType *)
          type := LTFile;
        ELSE (* DirectoryType *)
          type := LTDirectory;
        END;
        IF (NOT AddTextLine(dirListHead,dirListTail,type,ADR(fibFileName[0]),fibSize)) THEN
          noMemErr := TRUE;
          EXIT; (* Error! *)
        END;
      END; (* WITH *)
    END; (* WHILE *)

    ok := TRUE;
    EXIT; (* Success! *)
  END; (* LOOP *)

  IF (fib # NIL) THEN
    DeallocMemory(fib);
  END;

  IF (lock # NIL) THEN
    UnLock(lock);
  END;

  IF (NOT ok) THEN
    DeleteLineList(dirListHead);
    dirListHead := NIL;
  END;

  IF (noMemErr) THEN
    ShowError(ShowErrMsgNoMemory);
  END;

  IF (diskErr) THEN
    ShowError(ADR(DirErrMsgReadDir));
  END;

  SetSizeVerifyModeAllVTWindows(TRUE); (* Enable size verify *)

  newList := dirListHead;
END CreateDirectoryLineList;

(* CalcLineListWidthAndLength - count # of lines and longest line width *)
PROCEDURE CalcLineListWidthAndLength(list: TextLinePtr; VAR width, length: CARDINAL);
VAR
  maxWidth: CARDINAL;
  curWidth: CARDINAL;
  listLength: CARDINAL;
BEGIN
  maxWidth   := 0;
  listLength := 0;
  WHILE (list # NIL) DO
    curWidth := strlen(list^.tlString);
    IF (curWidth > maxWidth) THEN
      maxWidth := curWidth;
    END;
    INC(listLength);
    list := list^.tlNext;
  END;
  width  := maxWidth;
  length := listLength;
END CalcLineListWidthAndLength;

(* GetTextLineNo - get ptr to text line record from its list position *)
PROCEDURE GetTextLineNo(list: TextLinePtr; no: CARDINAL): TextLinePtr;
BEGIN
  WHILE (no # 0) DO
    list := list^.tlNext;
    DEC(no);
  END;
  RETURN (list);
END GetTextLineNo;

(* SetNewDirectoryPath - delete old dir path and alloc new dir path string *)
PROCEDURE SetNewDirectoryPath(VAR dw: DirectoryWindow; path: ADDRESS): BOOLEAN;
BEGIN
  WITH dw DO
    IF (dwDirectoryPath # NIL) THEN
      DeallocString(dwDirectoryPath);
    END;
    dwDirectoryPath := AllocString(path);
    IF (dwDirectoryPath = NIL) THEN
      ShowError(ShowErrMsgNoMemory);
    END;
    RETURN (dwDirectoryPath # NIL);
  END;
END SetNewDirectoryPath;

(* AddWindowNode - create a window node and link into window list *)
PROCEDURE AddWindowNode(): DirectoryWindowPtr;
VAR
  dw: DirectoryWindowPtr;
BEGIN
  dw := AllocMemory(SIZE(DirectoryWindow), MemReqSet{MemClear});
  IF (dw # NIL) THEN
    IF (DirectoryWindowList # NIL) THEN
      dw^.dwNext := DirectoryWindowList;
    END;
    DirectoryWindowList := dw;
  END;
  RETURN (dw);
END AddWindowNode;

(* DeleteWindowNode - delete window node and unlink from window list *)
PROCEDURE DeleteWindowNode(dw: DirectoryWindowPtr);
VAR
  prev: DirectoryWindowPtr;
  cur: DirectoryWindowPtr;
BEGIN
  prev := NIL;
  cur  := DirectoryWindowList;
  WHILE (cur # dw) DO
    prev := cur;
    cur  := cur^.dwNext;
  END;

  IF (prev = NIL) THEN (* First in list *)
    DirectoryWindowList := dw^.dwNext;
  ELSE (* Not first in list *)
    prev^.dwNext := dw^.dwNext;
  END;

  DeallocMemory(dw);
END DeleteWindowNode;

(* GetWindowIndex - get position of window node in list *)
PROCEDURE GetWindowIndex(dw: DirectoryWindowPtr): CARDINAL;
VAR
  index: CARDINAL;
  p: DirectoryWindowPtr;
BEGIN
  index := 0;
  p     := DirectoryWindowList;
  WHILE (dw # p) DO
    INC(index);
    p := p^.dwNext;
  END;
  RETURN (index);
END GetWindowIndex;

(* CloseDirectoryWindow - close directory window *)
PROCEDURE CloseDirectoryWindow(VAR dw: DirectoryWindow);
BEGIN
  WITH dw DO
    IfCurrentWindowClearSelection(dwVTWindow);

    IF (dwDirectoryPath # NIL) THEN
      DeallocString(dwDirectoryPath);
    END;

    DeleteLineList(dwLineList);

    CloseVTWindow(dwVTWindow);
  END;
  DeleteWindowNode(ADR(dw));
END CloseDirectoryWindow;

(* CloseDirectoryWindowNoMemory - close window and show no memory err msg *)
PROCEDURE CloseDirectoryWindowNoMemory(VAR dw: DirectoryWindow);
BEGIN
  CloseDirectoryWindow(dw);
  ShowError(ShowErrMsgNoMemory);
END CloseDirectoryWindowNoMemory;

(* UpdateDirectoryWindow - update contents of directory window *)
PROCEDURE UpdateDirectoryWindow(VAR dw: DirectoryWindow);
VAR
  newList: TextLinePtr;
  newWidth: CARDINAL;
  newLength: CARDINAL;
  ok: BOOLEAN;
BEGIN
  WITH dw DO
    IfCurrentWindowClearSelection(dwVTWindow);

    IF (dwDisplayMode = DMDirectory) THEN
      CreateDirectoryLineList(dwDirectoryPath, newList);
      IF (newList = NIL) THEN (* Error reading directory *)
        dwDisplayMode := DMVolumes; (* Return to volume list *)
      END;
    END;

    IF (dwDisplayMode = DMVolumes) THEN
      CreateVolumeLineList(newList);
    END;

    ok := newList # NIL;
    IF (ok) THEN
      (* Delete old list and set new list *)
      DeleteLineList(dwLineList);
      dwLineList := newList;

      CalcLineListWidthAndLength(newList, newWidth, newLength);
      dwVTWindowParms.vtpTotalLines := newLength;

      IF (dwDisplayMode = DMVolumes) THEN
        SetTitleVTWindow(dwVTWindow, ADR(VolumeListTitle));
        IF (newWidth < MinVolumeWidth) THEN
          newWidth := MinVolumeWidth;
        END;
        dwVTWindowParms.vtpMaxColumns := newWidth;
      ELSE (* dwDisplayMode = DMDirectory *)
        SetTitleVTWindow(dwVTWindow, ADR(DirectoryListTitle));
        dwFileInfoPos := newWidth + 1;
        dwVTWindowParms.vtpMaxColumns := newWidth + FileInfoWidth;
      END;
      ok := InitParmsVTWindow(dwVTWindow, dwVTWindowParms);
    END;
    IF (NOT ok) THEN
      CloseDirectoryWindowNoMemory(dw);
    END;
  END;
END UpdateDirectoryWindow;

(* SelectDirectoryObject - perform directory window action based on line *)
PROCEDURE SelectDirectoryObject(VAR dw: DirectoryWindow; y: CARDINAL);
VAR
  line: TextLinePtr;
  path: ARRAY [0..MaxPathLength] OF CHAR;
  i: CARDINAL;
BEGIN
  WITH dw DO
    line := GetTextLineNo(dwLineList, y);

    WITH line^ DO
      CASE (tlType) OF
      | LTParent:    strcpy(ADR(path[0]), dwDirectoryPath);
                     i := strlen(ADR(path[0])) - 1;
                     IF (path[i] = "/") THEN (* Last Char = "/" *)
                       (* Backup to volume name or previous directory *)
                       REPEAT
                         DEC(i);
                       UNTIL (path[i] = "/") OR (path[i] = ":");
                       path[i+1] := 0C;
                       IF (SetNewDirectoryPath(dw, ADR(path[0]))) THEN
                         UpdateDirectoryWindow(dw);
                       END;
                     ELSIF (path[i] = ":") THEN (* Already at volume name *)
                       dwDisplayMode := DMVolumes;
                       UpdateDirectoryWindow(dw);
                     END;
      | LTFile:      (* Do Nothing! *)
      | LTDirectory: strcpy(ADR(path[0]), dwDirectoryPath);
                     strcat(ADR(path[0]), tlString);
                     strcat(ADR(path[0]), ADR("/"));
                     IF (SetNewDirectoryPath(dw, ADR(path[0]))) THEN
                       UpdateDirectoryWindow(dw);
                     END;
      | LTVolume:    IF (SetNewDirectoryPath(dw, tlString)) THEN
                       dwDisplayMode := DMDirectory;
                       UpdateDirectoryWindow(dw);
                     END;
      END;
    END;
  END;
END SelectDirectoryObject;

(* SetNewDirectorySelection - setup new user selection based on y position *)
PROCEDURE SetNewDirectorySelection(VAR dw: DirectoryWindow; y: CARDINAL);
VAR
  data: SelectionData;
  path: ARRAY [0..MaxPathLength] OF CHAR;
  line: TextLinePtr;
BEGIN
  line := GetTextLineNo(dw.dwLineList, y);

  WITH data DO
    sdWindowClass := WCDirectory;
    sdVTWindow    := dw.dwVTWindow;
    sdLineNo      := y;
    sdStartPos    := 0;
    sdEndPos      := dw.dwVTWindowParms.vtpMaxColumns - 1;
    sdDefFgColor  := dw.dwVTWindowParms.vtpDefFgColor;
    sdDefBgColor  := dw.dwVTWindowParms.vtpDefBgColor;
    WITH line^ DO
      IF (tlType = LTFile) THEN
        strcpy(ADR(path[0]), dw.dwDirectoryPath);
        strcat(ADR(path[0]), tlString);
        sdFilePath := SetNewSelectedFilePath(ADR(path[0]));
      ELSE (* tlType: LTParent, LTDirectory, LTVolume *)
        sdFilePath := NIL;
      END;
    END;
  END;
  SetSelection(data);
END SetNewDirectorySelection;

(* VTUpdateProc - called to output new text to window *)
PROCEDURE VTUpdateProc(Window: VTWindow; OutputRow,FirstLine,LastLine: CARDINAL);
VAR
  dw: DirectoryWindowPtr;
  line: CARDINAL;
  text: TextLinePtr;
  r: CARDINAL;
  startPos: CARDINAL;
  endPos: CARDINAL;
  argo: ARRAY [0..9] OF FormatArg;
  outBuf: ARRAY [0..19] OF CHAR;
BEGIN
  dw := GetUserDataVTWindow(Window);

  text := GetTextLineNo(dw^.dwLineList, FirstLine);

  FOR line := FirstLine TO LastLine DO
    SetCursorPosVTWindow(Window, 0, OutputRow);
    WITH text^ DO
      CASE (tlType) OF
      | LTParent:    WriteStringVTWindow(Window, tlString);
      | LTFile:      WriteStringVTWindow(Window, tlString);
                     SetCursorPosVTWindow(Window, dw^.dwFileInfoPos, OutputRow);
                     argo[0].L := tlSize;
                     r := sprintf(ADR(outBuf[0]), ADR("%10lu"), argo);
                     WriteStringVTWindow(Window, ADR(outBuf[0]));
      | LTDirectory: WriteStringVTWindow(Window, tlString);
                     SetCursorPosVTWindow(Window, dw^.dwFileInfoPos, OutputRow);
                     WriteStringVTWindow(Window, ADR("     <DIR>"));
      | LTVolume:    WriteStringVTWindow(Window, tlString);
      END;
    END;

    IF (GetSelectionLineInfo(Window, line, startPos, endPos)) THEN
      WITH dw^.dwVTWindowParms DO
        WriteColorVTWindow(Window, OutputRow, startPos, endPos,
                           vtpDefBgColor, vtpDefFgColor);
      END;
    END;

    text := text^.tlNext;

    INC(OutputRow);
  END; (* FOR *)
END VTUpdateProc;

(* VTSelectProc - called when left mouse button click in window *)
PROCEDURE VTSelectProc(Window: VTWindow; x, y: CARDINAL; pressed: BOOLEAN);
VAR
  dw: DirectoryWindowPtr;
BEGIN
  dw := GetUserDataVTWindow(Window);

  WITH dw^ DO
    IF (pressed) THEN
      IF (IsPosSelected(Window, x, y)) THEN
        ClearSelection();
        SelectDirectoryObject(dw^, y);
      ELSE
        SetNewDirectorySelection(dw^, y);
      END;
    ELSE
      IF (NOT IsPosSelected(Window, x, y)) THEN
        SetNewDirectorySelection(dw^, y);
      END;
    END;
  END;
END VTSelectProc;

(* VTMenuProc - called when menu selection made *)
PROCEDURE VTMenuProc(Window: VTWindow; MenuNum, ItemNum, SubNum: CARDINAL);
VAR
  dw: DirectoryWindowPtr;
  fg: BYTE;
  bg: BYTE;
BEGIN
  dw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalMenuCommand(MenuNum, ItemNum, SubNum, LocalMenus,
                              WCDirectory, GetWindowIndex(dw),
                              dw^.dwVTWindow)) THEN
    WITH dw^ DO
      IfCurrentWindowClearSelection(dwVTWindow);

      CASE (ItemNum) OF
      | 0: (* Update List *)
           UpdateDirectoryWindow(dw^);
      (*1: -------------------- *)
      | 2: (* Set Window Color *)
           GetDetailPenBlockPenVTWindow(dwVTWindow, fg, bg);
           IF (RequestTextFgBgColors(fg, bg)) THEN
             SetDetailPenBlockPenVTWindow(dwVTWindow, fg, bg);
           END;
      | 3: (* Set Text Color *)
           WITH dwVTWindowParms DO
             IF (RequestTextFgBgColors(vtpDefFgColor, vtpDefBgColor)) THEN
               SetDefaultColorVTWindow(dwVTWindow, vtpDefFgColor,vtpDefBgColor);
             END;
           END;
      END;
    END;
  END;
END VTMenuProc;

(* VTDestProc - called when user clicks on destination-data gadget *)
PROCEDURE VTDestProc(Window: VTWindow);
BEGIN
  ShowError(ShowErrMsgBadDest);
END VTDestProc;

(* VTKeyProc - called when user types key into window *)
PROCEDURE VTKeyProc(Window: VTWindow; keyBuf: ADDRESS; keyLength: CARDINAL);
VAR
  dw: DirectoryWindowPtr;
BEGIN
  dw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalKbdCommand(keyBuf, keyLength, WCDirectory,
                             GetWindowIndex(dw))) THEN
    IF (keyLength = 1) AND (CHAR(keyBuf^) = KeyGotoVolumes) THEN
      dw^.dwDisplayMode := DMVolumes;
      UpdateDirectoryWindow(dw^);
    END;
  END;
END VTKeyProc;

(* VTCloseProc - called when close-box hit *)
PROCEDURE VTCloseProc(Window: VTWindow);
VAR
  dw: DirectoryWindowPtr;
BEGIN
  dw := GetUserDataVTWindow(Window);
  CloseDirectoryWindow(dw^);
END VTCloseProc;

(* VTErrorProc - called when failed to alloc text info for resized window *)
PROCEDURE VTErrorProc(Window: VTWindow);
VAR
  dw: DirectoryWindowPtr;
BEGIN
  dw := GetUserDataVTWindow(Window);
  CloseDirectoryWindowNoMemory(dw^);
END VTErrorProc;

(* ===== PUBLIC ===== *)

(* OpenDirectoryWindow - open a new directory window *)
PROCEDURE OpenDirectoryWindow(VAR config: ConfigWindow): BOOLEAN;
VAR
  dw: DirectoryWindowPtr;
  ok: BOOLEAN;
  nvt: NewVTWindow;
BEGIN
  ok := FALSE;
  dw := AddWindowNode();
  IF (dw # NIL) THEN
    WITH nvt DO
      WITH config DO
        nvtLeftEdge  := cwX;
        nvtTopEdge   := cwY;
        nvtWidth     := cwWidth;
        nvtHeight    := cwHeight;
        nvtDetailPen := cwDetailPen;
        nvtBlockPen  := cwBlockPen;
        nvtTitle     := ADR(VolumeListTitle);
        nvtMinWidth  := DefMinWindowWidth;
        nvtMinHeight := DefMinWindowHeight;
        nvtMaxWidth  := DefMaxWindowWidth;
        nvtMaxHeight := DefMaxWindowHeight;
        nvtMsgPort   := GetVTWindowsMsgPort();
        nvtUserData  := dw;
      END; (* WITH *)
    END; (* WITH *)
    dw^.dwVTWindow := OpenVTWindow(nvt);
    ok := dw^.dwVTWindow # VTWindowNIL;

    IF (ok) THEN
      WITH dw^.dwVTWindowParms DO
        WITH config DO
          vtpUpdateProc    := VTUpdateProc;
          vtpSelectProc    := VTSelectProc;
          vtpMenuProc      := VTMenuProc;
          vtpDestProc      := VTDestProc;
          vtpKeyProc       := VTKeyProc;
          vtpCloseProc     := VTCloseProc;
          vtpErrorProc     := VTErrorProc;
        (*vtpInitialLine   := 0;*)
        (*vtpTotalLines    := 1;*)
        (*vtpInitialColumn := 0;*)
        (*vtpMaxColumns    := 1;*)
          vtpDefFgColor    := cwFgColor;
          vtpDefBgColor    := cwBgColor;
        END;
      END;

      WITH dw^ DO
        dwLineList      := NIL;
        dwDisplayMode   := DMVolumes;
        dwDirectoryPath := NIL;
      END;
      SetMenuStripVTWindow(dw^.dwVTWindow, DirectoryMenuStrip^);

      UpdateDirectoryWindow(dw^);
    ELSE (* NOT ok *)
      DeleteWindowNode(dw);
    END;
  END;
  RETURN (ok);
END OpenDirectoryWindow;

(* UpdateDirectoryWindows - update contents of all directory windows *)
PROCEDURE UpdateDirectoryWindows();
VAR
  dw: DirectoryWindowPtr;
  p: DirectoryWindowPtr;
BEGIN
  dw := DirectoryWindowList;
  WHILE (dw # NIL) DO
    p  := dw;
    dw := dw^.dwNext;
    IF (p^.dwDisplayMode = DMVolumes) THEN
      UpdateDirectoryWindow(p^);
    END;
  END;  
END UpdateDirectoryWindows;

(* CloseDirectoryWindows - close all directory windows *)
PROCEDURE CloseDirectoryWindows();
BEGIN
  WHILE (DirectoryWindowList # NIL) DO
    CloseDirectoryWindow(DirectoryWindowList^);
  END;
END CloseDirectoryWindows;

(* GetDirectoryWindowInfo - get config parameters of a specified window *)
PROCEDURE GetDirectoryWindowInfo(VAR index: CARDINAL;
                                 VAR config: ConfigWindow): BOOLEAN;
VAR
  dw: DirectoryWindowPtr;
  count: CARDINAL;
BEGIN
  count := index;
  dw    := DirectoryWindowList;
  WHILE (dw # NIL) DO
    IF (count = 0) THEN
      WITH config DO
        WITH dw^ DO
          GetInfoVTWindow(dwVTWindow, cwX, cwY, cwWidth, cwHeight,
                          cwDetailPen, cwBlockPen);
          cwMaxCharWidth := 0;
          cwFgColor      := dwVTWindowParms.vtpDefFgColor;
          cwBgColor      := dwVTWindowParms.vtpDefBgColor;
        END;
      END;
      INC(index);
      RETURN (TRUE); (* Window Found! *)
    END;
    DEC(count);
    dw := dw^.dwNext;
  END;
  RETURN (FALSE); (* End of list *)
END GetDirectoryWindowInfo;

(* InitDBDirectoryWindows - allocate module resources *)
PROCEDURE InitDBDirectoryWindows(): BOOLEAN;
BEGIN
  BeginMenuStrip();
    AddMenu(                           ADR("Directory "));
      AddMenuItem(                       ADR(" Update List"));
      AddMenuItemNoHighlight(            ADR(" ---------------- "));
      AddMenuItem(                       ADR(" Set Window Color"));
      AddMenuItem(                       ADR(" Set Text Color"));
  DirectoryMenuStrip := EndMenuStrip();
  IF (DirectoryMenuStrip # NIL) THEN
    LinkGlobalMenuStrip(DirectoryMenuStrip^);
  END;
  RETURN (DirectoryMenuStrip # NIL);
END InitDBDirectoryWindows;

(* CleanupDBDirectoryWindows - cleanup module resources *)
PROCEDURE CleanupDBDirectoryWindows();
BEGIN
  CloseDirectoryWindows();

  IF (DirectoryMenuStrip # NIL) THEN
    FreeMenuStrip(DirectoryMenuStrip^);
  END;

  IF (SelectedFilePath # NIL) THEN
    DeallocString(SelectedFilePath);
  END;
END CleanupDBDirectoryWindows;

END DBDirectoryWindows.
