
(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBBreakPoints.MOD                 Version: Amiga.01.10             *
 * Created: 01/07/89   Updated: 03/21/94   Author: Leon Frenkel             *
 * Description: Debugger break-point handling module.                       *
 ****************************************************************************)

IMPLEMENTATION MODULE DBBreakPoints;

(***************************************************************************
 * Changes for new release 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.2 -----
 * 
 * BUGFIX: Crash if CPU Cache Enabled on M68040
 * 
 * 
 *    1. IMPORTing variable OSVersion2 boolean from DBEnvironmentInquiry.def
 * 
 *    2. CacheClearU() called:
 * 
 *           In InstallBreakPoints() at end if any installed.
 * 
 *           In RemoveBreakPoints() at end if any removed.
 * 
 ***************************************************************************
*)

FROM SYSTEM IMPORT
  ADDRESS,
  ADR;
FROM DBDisAsmWindows IMPORT
  UpdateDisAsmWindowsShowingAdr;
FROM DBEnvironmentInquiry IMPORT
  OSVersion2;
FROM DBFiles IMPORT
  Item, ItemInfo, StructureForm,
  InvalidModNo, InvalidStatNo, InvalidLineNo,
  GetModNoModOfs, GetModuleItem, GetProcedureItem, GetItemInfo, EmptyItem,
  GetStatementNoFromModOffset, GetSourceLineInfoFromStatementNo,
  GetModuleName;
FROM DBMisc IMPORT
  AllocMemory, DeallocMemory;
FROM DBSourceWindows IMPORT
  UpdateSourceWindowsShowingLine;
FROM DBStatusWindow IMPORT
  ShowErrMsgNoMemory,
  ShowError;
FROM DBWindows IMPORT
  WindowClass, WindowClassSet,
  UpdateWindows;
FROM Exec IMPORT
   CacheClearU;
FROM Memory IMPORT
  MemReqSet, MemClear;

CONST
  (* ILLEGAL Instruction used to cause a debugger break point *)
  InstILLEGAL = 04AFCH;

TYPE
  (* Pointer to a 16-bit value *)
  WordPtr = POINTER TO CARDINAL;

  (* Break point descriptor record *)
  BreakPointPtr = POINTER TO BreakPoint;
  BreakPoint = RECORD
                 bpNext     : BreakPointPtr; (* Ptr to the next break point *)
                 bpAdr      : WordPtr;       (* Address of break point *)
                 bpEnabled  : BOOLEAN;       (* TRUE = enabled, FALSE = disabled *)
                 bpModNo    : CARDINAL;      (* module # or InvalidModNo *)
                 bpLineNo   : CARDINAL;      (* line # or InvalidLineNo *)
                 bpStartPos : CARDINAL;      (* start char pos if valid line # *)
                 bpEndPos   : CARDINAL;      (* end char pos if valid line # *)
                 bpModName  : ADDRESS;       (* module name or NIL *)
                 bpProcName : ADDRESS;       (* procedure name or NIL *)
                 bpCount    : LONGCARD;      (* current break counter *)
                 bpLimit    : LONGCARD;      (* break limit value *)
                 bpSaveData : CARDINAL;      (* contents of memory where bp placed *)
               END;

  (* Temporary break point descriptor *)
  TempBreakPointPtr = POINTER TO TempBreakPoint;
  TempBreakPoint = RECORD
                     tbpEnabled  : BOOLEAN;  (* TRUE = enabled, FLASE = disabled *)
                     tbpAdr      : WordPtr;  (* address of break point *)
                     tbpSaveData : CARDINAL; (* contents of memory where br is placed *)
                   END;
VAR
  BPListHead  : BreakPointPtr;  (* Head of break point list *)
  BPListTail  : BreakPointPtr;  (* Tail of break point list *)
  BPCount     : CARDINAL;       (* Current # of break points in list *)
  BPTemp      : TempBreakPoint; (* Temporary break point info *)
  BPInstalled : BOOLEAN;        (* TRUE = bps set, FALSE = bps not set *)

(* ===== PRIVATE ===== *)

(* UpdateDisplay - update source, disasm, bplist windows *)
PROCEDURE UpdateDisplay(modNo, lineNo: CARDINAL; adr: ADDRESS;
                        updateBPList: BOOLEAN);
BEGIN
  IF (modNo # InvalidModNo) AND (lineNo # InvalidLineNo) THEN
    UpdateSourceWindowsShowingLine(modNo, lineNo);
  END;
  UpdateDisAsmWindowsShowingAdr(adr);
  IF (updateBPList) THEN
    UpdateWindows(WindowClassSet{WCBreakPointList});
  END;
END UpdateDisplay;

(* AllocBreakPoint - allocate a new break point record and link into list *)
PROCEDURE AllocBreakPoint(): BreakPointPtr;
VAR
  bp: BreakPointPtr;
BEGIN
  bp := AllocMemory(SIZE(BreakPoint), MemReqSet{MemClear});
  IF (bp # NIL) THEN
    bp^.bpNext := NIL;
    
    IF (BPListHead = NIL) THEN
      BPListHead := bp;
    ELSE
      BPListTail^.bpNext := bp;
    END;
    BPListTail         := bp;

    INC(BPCount);
  END;
  RETURN (bp);
END AllocBreakPoint;

(* FreeBreakPoint - free a break point structure and unlink from list *)
PROCEDURE FreeBreakPoint(bp: BreakPointPtr; updateBPList: BOOLEAN);
VAR
  prev: BreakPointPtr;
  cur: BreakPointPtr;
  modNo: CARDINAL;
  lineNo: CARDINAL;
  adr: ADDRESS;
BEGIN
  prev := NIL;
  cur := BPListHead;
  WHILE (cur # bp) DO
    prev := cur;
    cur  := cur^.bpNext;
  END;

  WITH cur^ DO
    modNo  := bpModNo;
    lineNo := bpLineNo;
    adr    := bpAdr;
  END;

  IF (prev = NIL) THEN (* First break pt in list *)
    BPListHead := bp^.bpNext;
  ELSE (* Not first in list *)
    prev^.bpNext := bp^.bpNext;
  END;

  IF (bp = BPListTail) THEN (* Last in list *)
    BPListTail := prev;
  END;

  DEC(BPCount);

  DeallocMemory(bp);

  UpdateDisplay(modNo, lineNo, adr, updateBPList);
END FreeBreakPoint;

(* GetBreakPointFromBpNo - get break point record from index *)
PROCEDURE GetBreakPointFromBpNo(bpNo: CARDINAL): BreakPointPtr;
VAR
  bp: BreakPointPtr;
BEGIN
  bp := BPListHead;
  WHILE (bpNo # 0) DO
    bp := bp^.bpNext;
    DEC(bpNo);
  END;
  RETURN (bp);
END GetBreakPointFromBpNo;

(* GetBreakPointFromAdr - find a break point record at a specified adr *)
PROCEDURE GetBreakPointFromAdr(adr: ADDRESS): BreakPointPtr;
VAR
  bp: BreakPointPtr;
BEGIN
  bp := BPListHead;
  WHILE (bp # NIL) DO
    IF (bp^.bpAdr = adr) THEN
      RETURN (bp); (* Found! *)
    END;
    bp := bp^.bpNext;
  END; 
  RETURN (NIL); (* Not Found! *)
END GetBreakPointFromAdr;

(* HitBreakPoint - Update break count and test if limit reached *)
PROCEDURE HitBreakPoint(VAR bp: BreakPoint): BOOLEAN;
BEGIN
  WITH bp DO
    INC(bpCount);
    IF (bpCount = bpLimit) THEN
      bpCount := 0D; (* Reset *)
    END;
    IF (bpLimit # 1D) THEN
      UpdateWindows(WindowClassSet{WCBreakPointList});
    END;
    RETURN (bpCount = 0D);
  END;
END HitBreakPoint;

(* ===== PUBLIC ===== *)

(* GetBreakPointCount - get current # of break points *)
PROCEDURE GetBreakPointCount(): CARDINAL;
BEGIN
  RETURN (BPCount);
END GetBreakPointCount;

(* CreateBreakPoint - create a new break point at the specified address *)
PROCEDURE CreateBreakPoint(adr: ADDRESS);
VAR
  bp: BreakPointPtr;
  modNo: CARDINAL;
  modOfs: LONGCARD;
  statNo: CARDINAL;
  firstInst: BOOLEAN;
  lineNo: CARDINAL;
  startX: CARDINAL;
  stopX: CARDINAL;
  modItem: Item;
  procItem: Item;
  procInfo: ItemInfo;
BEGIN
  bp := AllocBreakPoint();
  IF (bp # NIL) THEN
    WITH bp^ DO
      bpAdr       := adr;
      bpEnabled   := TRUE;
      bpCount     := 0D;
      bpLimit     := 1D;
      bpModNo     := InvalidModNo;
      bpLineNo    := InvalidLineNo;
      bpModName   := NIL;
      bpProcName  := NIL;

      GetModNoModOfs(adr, modNo, modOfs);
      IF (modNo # InvalidModNo) THEN
        bpModNo   := modNo;
        bpModName := GetModuleName(modNo);

        GetModuleItem(modNo, modItem, FALSE);
        IF (NOT EmptyItem(modItem)) THEN

          GetProcedureItem(modItem, modOfs, procItem);
          IF (NOT EmptyItem(procItem)) THEN
            GetItemInfo(procItem, procInfo);

            IF (procInfo.iiForm = Prc) THEN
              bpProcName := procInfo.iiNameString;
            ELSIF (procInfo.iiForm = Mod) THEN
              bpProcName := ADR("MODULE_BODY");
            END;
  
            GetStatementNoFromModOffset(modNo, modOfs, statNo, firstInst);
            IF (statNo # InvalidStatNo) THEN
              GetSourceLineInfoFromStatementNo(modNo, statNo, lineNo,
                                                 startX, stopX);
              IF (lineNo # InvalidLineNo) THEN
                bpLineNo   := lineNo;
                bpStartPos := startX;
                bpEndPos   := stopX;
              END; (* IF *)
            END; (* IF *)
          END; (* IF *)
        END; (* IF *)
      END; (* IF *)
      UpdateDisplay(bpModNo, bpLineNo, bpAdr, TRUE);
    END; (* WITH *)
  ELSE
    ShowError(ShowErrMsgNoMemory);
  END; (* IF *)
END CreateBreakPoint;

(* DeleteBreakPoint - delete break point at specified address *)
PROCEDURE DeleteBreakPoint(adr: ADDRESS);
VAR
  bp: BreakPointPtr;
BEGIN
  bp := GetBreakPointFromAdr(adr);
  IF (bp # NIL) THEN
    FreeBreakPoint(bp, TRUE);
  END;
END DeleteBreakPoint;

(* DeleteBreakPoints - delete all break points *)
PROCEDURE DeleteBreakPoints();
BEGIN
  WHILE (BPListHead # NIL) DO
    FreeBreakPoint(BPListHead, FALSE);
  END;
  UpdateWindows(WindowClassSet{WCBreakPointList});
END DeleteBreakPoints;

(* GetBreakPointLimit - get current break point limit value *)
PROCEDURE GetBreakPointLimit(adr: ADDRESS): LONGCARD;
VAR
  bp: BreakPointPtr;
BEGIN
  bp := GetBreakPointFromAdr(adr);
  IF (bp # NIL) THEN
    RETURN (bp^.bpLimit);
  END;
  RETURN (0D);
END GetBreakPointLimit;

(* SetBreakPointLimit - set a new break point limit and clear break count *)
PROCEDURE SetBreakPointLimit(adr: ADDRESS; limit: LONGCARD);
VAR
  bp: BreakPointPtr;
BEGIN
  bp := GetBreakPointFromAdr(adr);
  IF (bp # NIL) THEN
    WITH bp^ DO
      bpLimit := limit;
      bpCount := 0D;
    END;
    UpdateWindows(WindowClassSet{WCBreakPointList});
  END;
END SetBreakPointLimit;

(* CreateTempBreakPoint - enable a temporary break point *)
PROCEDURE CreateTempBreakPoint(adr: ADDRESS);
BEGIN
  WITH BPTemp DO
    tbpEnabled := TRUE;
    tbpAdr     := adr;
  END;
END CreateTempBreakPoint;

(* DeleteTempBreakPoint - disable temporary break point *)
PROCEDURE DeleteTempBreakPoint();
BEGIN
  BPTemp.tbpEnabled := FALSE;
END DeleteTempBreakPoint;

(* EnableBreakPoint - enable a break point at specified adr *)
PROCEDURE EnableBreakPoint(adr: ADDRESS);
VAR
  bp: BreakPointPtr;
BEGIN
  bp := GetBreakPointFromAdr(adr);
  IF (bp # NIL) THEN
    WITH bp^ DO
      bpEnabled := TRUE;
      UpdateDisplay(bpModNo, bpLineNo, bpAdr, TRUE);
    END;
  END;
END EnableBreakPoint;

(* DisableBreakPoint - disable a break point at specified adr *)
PROCEDURE DisableBreakPoint(adr: ADDRESS);
VAR
  bp: BreakPointPtr;
BEGIN
  bp := GetBreakPointFromAdr(adr);
  IF (bp # NIL) THEN
    WITH bp^ DO
      bpEnabled := FALSE;
      UpdateDisplay(bpModNo, bpLineNo, bpAdr, TRUE);
    END;
  END;
END DisableBreakPoint;

(* EnableBreakPoints - enable all break points *)
PROCEDURE EnableBreakPoints();
VAR
  bp: BreakPointPtr;
BEGIN
  bp := BPListHead;
  WHILE (bp # NIL) DO
    WITH bp^ DO
      bpEnabled := TRUE;
      UpdateDisplay(bpModNo, bpLineNo, bpAdr, FALSE);
    END;
    bp := bp^.bpNext;
  END;
  UpdateWindows(WindowClassSet{WCBreakPointList});
END EnableBreakPoints;

(* DisableBreakPoints - Disable all break points *)
PROCEDURE DisableBreakPoints();
VAR
  bp: BreakPointPtr;
BEGIN
  bp := BPListHead;
  WHILE (bp # NIL) DO
    WITH bp^ DO
      bpEnabled := FALSE;
      UpdateDisplay(bpModNo, bpLineNo, bpAdr, FALSE);
    END;
    bp := bp^.bpNext;
  END;
  UpdateWindows(WindowClassSet{WCBreakPointList});
END DisableBreakPoints;

(* InstallBreakPoints - install in memory all break points *)
PROCEDURE InstallBreakPoints();
VAR
  bp:  BreakPointPtr;
  Any: BOOLEAN;
BEGIN
  BPInstalled := TRUE;
  Any := FALSE;

  (* Temporary break point has priority over user break points *)
  WITH BPTemp DO
    IF (tbpEnabled) THEN
      tbpSaveData := tbpAdr^;
      tbpAdr^     := InstILLEGAL;
      Any := TRUE;
    END;
  END;

  bp := BPListHead;
  WHILE (bp # NIL) DO
    WITH bp^ DO
      IF (bpEnabled) THEN
        bpSaveData := bpAdr^;
        bpAdr^     := InstILLEGAL;
        Any := TRUE;
      END;
    END;

    bp := bp^.bpNext;
  END;

  IF Any AND OSVersion2 THEN
     CacheClearU();
  END;
END InstallBreakPoints;

(* RemoveBreakPoints - remove all break points by restoring original code *)
PROCEDURE RemoveBreakPoints();
VAR
  bp:  BreakPointPtr;
  Any: BOOLEAN;
BEGIN
  IF (NOT BPInstalled) THEN
    RETURN; (* Abort if not currently installed *)
  END;
  BPInstalled := FALSE;

  (* Restore user break points first (opposite of installation process) *)
  bp := BPListHead;
  WHILE (bp # NIL) DO
    WITH bp^ DO
      IF (bpEnabled) THEN
        bpAdr^ := bpSaveData;
        Any := TRUE;
      END;
    END;
    bp := bp^.bpNext;
  END;

  WITH BPTemp DO
    IF (tbpEnabled) THEN
      tbpAdr^    := tbpSaveData;
      Any := TRUE;
    END;
  END;

  IF Any AND OSVersion2 THEN
     CacheClearU();
  END;
END RemoveBreakPoints;

(* StopOnBreakPoint - check if user program has hit a break point *)
PROCEDURE StopOnBreakPoint(adr: ADDRESS; VAR stopBP, bpTemp: BOOLEAN): BOOLEAN;
VAR
  bp: BreakPointPtr;
  tempHit: BOOLEAN;
BEGIN
  (* Determine if temporary break point hit *)
  WITH BPTemp DO
    tempHit := (tbpEnabled) AND (tbpAdr = adr);
  END;

  (* User break point has priority over temporary break point except *)
  (* in cases where the user break point has not reached break limit *)
  bp := GetBreakPointFromAdr(adr);
  IF (bp # NIL) AND (bp^.bpEnabled) THEN
    stopBP := HitBreakPoint(bp^);
    IF (stopBP) OR (NOT tempHit) THEN
      bpTemp := FALSE;
      RETURN (TRUE); (* Break Point *)
    END;
  END;

  IF (tempHit) THEN (* Stop on temporary break point *)
    stopBP := TRUE;
    bpTemp := TRUE;
    RETURN (TRUE); (* Break Point *)
  END;

  RETURN (FALSE); (* Not A Break Point Adr *)
END StopOnBreakPoint;
  
(* IsAdrBreakPoint - check if specified adr contains a break point *)
PROCEDURE IsAdrBreakPoint(adr: ADDRESS): BOOLEAN;
BEGIN
  RETURN (GetBreakPointFromAdr(adr) # NIL);
END IsAdrBreakPoint;

(* IsAdrEnabledBreakPoint - check if specified adr contains an enabled bp *)
PROCEDURE IsAdrEnabledBreakPoint(adr: ADDRESS): BOOLEAN;
VAR
  bp: BreakPointPtr;
BEGIN
  WITH BPTemp DO
    IF (tbpEnabled) AND (tbpAdr = adr) THEN
      RETURN (TRUE); (* Found! *)
    END;
  END;

  bp := GetBreakPointFromAdr(adr);
  RETURN ((bp # NIL) AND (bp^.bpEnabled));
END IsAdrEnabledBreakPoint;

(* GetBreakPointOnLineNo - get info on a break point on specified source line *)
PROCEDURE GetBreakPointOnLineNo(modNo, lineNo: CARDINAL;
                                VAR bpLineIndex: CARDINAL;
                                VAR startPos, endPos: CARDINAL): BOOLEAN;
VAR
  bp: BreakPointPtr;
  count: CARDINAL;
BEGIN
  count := bpLineIndex;
  bp := BPListHead;
  WHILE (bp # NIL) DO
    WITH bp^ DO
      IF (bpEnabled) AND (bpModNo = modNo) AND (bpLineNo = lineNo) THEN
        IF (count = 0) THEN
          startPos := bpStartPos;
          endPos   := bpEndPos;
          INC(bpLineIndex);
          RETURN (TRUE); (* break point found *)
        ELSE
          DEC(count);
        END;
      END;
    END;
    bp := bp^.bpNext;
  END;
  RETURN (FALSE); (* No more break points on line *)
END GetBreakPointOnLineNo;

(* GetBreakPointInAdrRange - check if adr range contains a break point *)
PROCEDURE GetBreakPointInAdrRange(startAdr, endAdr: ADDRESS): BOOLEAN;
VAR
  bp: BreakPointPtr;
BEGIN
  bp := BPListHead;
  WHILE (bp # NIL) DO
    WITH bp^ DO
      IF (bpEnabled) AND
         (startAdr <= ADDRESS(bpAdr)) AND (endAdr > ADDRESS(bpAdr)) THEN
        RETURN (TRUE); (* break point found *)
      END;
    END;
    bp := bp^.bpNext;
  END;
  RETURN (FALSE); (* No break points in range *)
END GetBreakPointInAdrRange;

(* GetBreakPointInfo - get info on a specified break point *)
PROCEDURE GetBreakPointInfo(bpNo: CARDINAL; VAR adr: ADDRESS;
                            VAR enabled: BOOLEAN; VAR modNo, lineNo: CARDINAL;
                            VAR modName, procName: ADDRESS;
                            VAR count, limit: LONGCARD);
VAR
  bp: BreakPointPtr;
BEGIN
  bp := GetBreakPointFromBpNo(bpNo);
  WITH bp^ DO
    adr      := bpAdr;
    enabled  := bpEnabled;
    modNo    := bpModNo;
    lineNo   := bpLineNo;
    modName  := bpModName;
    procName := bpProcName;
    count    := bpCount;
    limit    := bpLimit;
  END;
END GetBreakPointInfo;

(* GetBreakPointAdr - get address of a specified break point no *)
PROCEDURE GetBreakPointAdr(bpNo: CARDINAL): ADDRESS;
VAR
  bp: BreakPointPtr;
BEGIN
  bp := GetBreakPointFromBpNo(bpNo);
  RETURN (bp^.bpAdr);
END GetBreakPointAdr;

(* InitDBBreakPoints - initialize module *)
PROCEDURE InitDBBreakPoints(): BOOLEAN;
BEGIN
  RETURN (TRUE);
END InitDBBreakPoints;

(* CleanupDBBreakPoints - cleanup module *)
PROCEDURE CleanupDBBreakPoints();
END CleanupDBBreakPoints;

END DBBreakPoints.
