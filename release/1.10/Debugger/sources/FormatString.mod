(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Jim Olinger                          *
 ****************************************************************************
 * Name: FormatString.MOD                  Version: Amiga.00.00             *
 * Created: 01/17/87   Updated: 02/20/89   Author: Leon Frenkel             *
 * Description: Low level utility function for producing formating output,  *
 *  used as the basis for the high level functions printf, fprintf, sprintf.*
 * WARNING: The REAL format conversions: "%f, %g, %e", are not avaialable   *
 *  unless the module "FormatStringReals" is imported.                      *
 *                                                                          *
 * This version of the module has been modified slightly for use with the   *
 * Source-Level Debugger.  The hex format has been changed to display       *
 * chars in upper case instead of lower case.  An additional format type    *
 * been added "%b" this is used for binary format.                          *
 ****************************************************************************)

IMPLEMENTATION MODULE FormatString;

FROM SYSTEM IMPORT ADR, ADDRESS, TSIZE;

(*$L+,D-*)

TYPE 
  (* This is the same enumeration as defined in RealConversions module *)
  RealToStringFormat = (Scientific,    (* Scientific - [-]m.nnnnnnE[+|-]xx *)
                        Decimal,       (* Decimal    - [-]mmm.nnnnn        *)
                        ShortestForm); (* Use which ever form is shorter   *)

TYPE
  CPTR  = POINTER TO CHAR;
  CIPTR = POINTER TO ARRAY [0..32765] OF CHAR;

VAR
  digits : POINTER TO ARRAY [0..15] OF CHAR;

PROCEDURE strlen(p: CIPTR): INTEGER;
VAR i: INTEGER;
BEGIN
  i := 0;
  WHILE p^[i] # 0C DO
    INC(i);
  END;
  RETURN i;
END strlen;


PROCEDURE cvt(v: FormatArg; base: INTEGER; cp: CPTR; len: INTEGER): CPTR;
VAR val: LONGCARD;
BEGIN
  IF len = TSIZE(LONGINT) THEN
    val := LONGINT(v.L);
  ELSIF base > 0 THEN
    val := CARDINAL(v.W);
  ELSE
    val := INTEGER(v.W);
  END;

  len := 0;
  IF base < 0 THEN
    base := -base;
    IF LONGINT(val) < 0D THEN
      val := - LONGINT(val);
      len := 1;
    END;
  END;
  
  REPEAT
    DEC(ADDRESS(cp));
    cp^ := digits^[INTEGER(val MOD LONGCARD(base))];
    val := val DIV LONGCARD(base);
  UNTIL val = 0D;
  IF len # 0 THEN
    DEC(ADDRESS(cp));
    cp^ := "-";
  END;
  RETURN cp;
END cvt;


PROCEDURE Format(fmt: ADDRESS; VAR args: ARRAY OF FormatArg;
                 putFunc: PutCharFuncType): CARDINAL;
VAR argIdx    : INTEGER;
    rj        : INTEGER;
    charcount : INTEGER;
    width     : INTEGER;
    maxwidth  : INTEGER;
    i, j, k   : INTEGER;
    val       : CARDINAL;
    cp        : CPTR;
    skip      : BOOLEAN;
    rf        : RealToStringFormat;
    fillc     : CHAR;
    c         : CHAR;
    s         : ARRAY [0..199] OF CHAR;
BEGIN
  charcount := 0;
  argIdx := 0;
  c := CHAR(fmt^); INC(fmt);
  WHILE c # 0C DO
    IF c = "%" THEN
      s[40] := 0C;
      rj := 1;
      fillc := " ";
      maxwidth := 10000;
      c := CHAR(fmt^); INC(fmt);
      IF c = "-" THEN
        rj := 0;
        c := CHAR(fmt^); INC(fmt);
      END;
      IF c = "0" THEN
        fillc := "0";
        c := CHAR(fmt^); INC(fmt);
      END;
      IF c = "*" THEN
        width := INTEGER(args[argIdx].W); INC(argIdx);
        c := CHAR(fmt^); INC(fmt);
      ELSE
        width := 0;
        WHILE (c >= "0") AND (c <= "9") DO
          width := (width * 10) + INTEGER(c) - ORD("0");
          c := CHAR(fmt^); INC(fmt);
        END;
      END;
      IF c = "." THEN
        c := CHAR(fmt^); INC(fmt);
        IF c = "*" THEN
          maxwidth := INTEGER(args[argIdx].W); INC(argIdx);
          c := CHAR(fmt^); INC(fmt);
        ELSE
          maxwidth := 0;
          WHILE (c >= "0") AND (c <= "9") DO
            maxwidth := (maxwidth * 10) + INTEGER(c) - ORD("0");
            c := CHAR(fmt^); INC(fmt);
          END;
        END;
      END;
      i := TSIZE(INTEGER);
      IF c = "l" THEN
        c := CHAR(fmt^); INC(fmt);
        i := TSIZE(LONGINT);
      ELSIF c = "h" THEN
        c := CHAR(fmt^); INC(fmt);
      END;

      skip := FALSE;
      CASE c OF
        | "b": k := 2;
               cp := cvt(args[argIdx],k,ADR(s)+40D,i); INC(argIdx);
        | "o": k := 8;   
               cp := cvt(args[argIdx],k,ADR(s)+40D,i); INC(argIdx);
        | "u": k := 10;
               cp := cvt(args[argIdx],k,ADR(s)+40D,i); INC(argIdx);
        | "x": k := 16;
               cp := cvt(args[argIdx],k,ADR(s)+40D,i); INC(argIdx);
        | "d": k := -10;
               cp := cvt(args[argIdx],k,ADR(s)+40D,i); INC(argIdx);
        | "s": cp := CPTR(args[argIdx]); INC(argIdx);
               i  := strlen(CIPTR(cp)); 
               skip := TRUE;
        | "e","f","g": IF ADDRESS(RealConvFunc) = NIL THEN (* no REAL support *)
                         cp := ADR(s) + 39D;
                         cp^ := c;
                       ELSE (* REAL support available *)
                         IF maxwidth = 10000 THEN 
                           j := 6;
                         ELSE
                           j := maxwidth;
                         END;
                         IF    c = "e" THEN rf := Scientific;
                         ELSIF c = "f" THEN rf := Decimal;
                         ELSE(*c = "g" *)   rf := ShortestForm;
                         END;
                         RealConvFunc(REAL(args[argIdx].L), s, j, rf);
                         INC(argIdx);
                         cp := ADR(s);
                         i := strlen(CIPTR(cp));
                         maxwidth := 200;
                         skip := TRUE;
                       END;
        | "c":         cp  := ADR(s) + 39D;
                       cp^ := CHAR(args[argIdx].B); INC(argIdx);
      ELSE
        cp := ADR(s) + 39D;
        cp^ := c;
      END;
      IF NOT skip THEN
        i := (ADR(s) + 40D) - ADDRESS(cp);
      END;

      IF i > maxwidth THEN
        i := maxwidth;
      END;
      IF rj # 0 THEN
        IF ((cp^ = "-") OR (cp^ = "+")) AND (fillc = "0") THEN
          DEC(width);
          putFunc(cp^);
          INC(ADDRESS(cp));
        END;
        WHILE width > i DO
          DEC(width);
          putFunc(fillc);
          INC(charcount);
        END;
      END;
      k := 0;
      WHILE (cp^ # 0C) AND (k < maxwidth) DO
        putFunc(cp^);
        INC(ADDRESS(cp));
        INC(k);
      END;

      INC(charcount, k);

      IF rj = 0 THEN
        WHILE width > i DO
          DEC(width);
          putFunc(" ");
          INC(charcount);
        END;
      END
    ELSE
      putFunc(c);
      INC(charcount);
    END;
    c := CHAR(fmt^); INC(fmt);
  END;
  RETURN charcount;
END Format;

BEGIN
  digits := ADR("0123456789ABCDEF");
END FormatString.
