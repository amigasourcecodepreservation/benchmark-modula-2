
(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBConfiguration.MOD               Version: Amiga.01.10             *
 * Created: 01/02/89   Updated: 03/21/94   Author: Leon Frenkel             *
 * Description: M2 debugger configuration module.                           *
 ****************************************************************************)

IMPLEMENTATION MODULE DBConfiguration;

(***************************************************************************
 * Changes for new release 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.2 -----
 *
 * BUGFIX: Default Memory window configuration was not well chosen
 * 
 *    1. Default Memory window display format is now "Number" rather than
 *       "Real". Size is word and number format is hexadecimal.
 * 
 * BUGFIX: Src and Sym Dir List consider leading/trailing blanks significant
 * 
 *    1. Added code in AddDirNode() to remove leading and trailing blanks in
 *       the directory name before using it. Too bad for anyone who actually
 *       wants to name dirs with leading or trail blanks, but this makes entry
 *       of the dir lists more robust.
 * 
 ***************************************************************************
*)

FROM SYSTEM IMPORT
  ADDRESS, BYTE, WORD, LONGWORD,
  ADR;
FROM CPrintFile IMPORT
  fprintf;
FROM CScanBuffer IMPORT
  sscanf;
FROM CScanFile IMPORT
  fscanf;
FROM CStdIO IMPORT
  FILE,
  fopen, fclose;
FROM CStrings IMPORT
  strcmp, strlen, strcat;
FROM DBBreakPointWindows IMPORT
  GetBreakPointWindowInfo;
FROM DBDirectoryWindows IMPORT
  GetDirectoryWindowInfo;
FROM DBDisAsmWindows IMPORT
  GetDisAsmWindowInfo;
FROM DBInfoWindows IMPORT
  GetInfoWindowInfo;
FROM DBMemoryWindows IMPORT
  GetMemoryWindowInfo;
FROM DBMisc IMPORT
  AllocMemory, DeallocMemory, AllocString, DeallocString;
FROM DBModuleListWindows IMPORT
  GetModuleListWindowInfo;
FROM DBProcChainWindows IMPORT
  GetProcChainWindowInfo;
FROM DBRegistersWindows IMPORT
  GetRegistersWindowInfo;
FROM DBSourceWindows IMPORT
  GetSourceWindowInfo;
FROM DBStatusWindow IMPORT
  GetStatusWindowInfo;
FROM DBSymDataWindows IMPORT
  GetSymDataWindowInfo;
FROM DBTextWindows IMPORT
  SetSizeVerifyModeAllVTWindows;
FROM DBUserProgram IMPORT
  StartupMode, LanguageMode;
FROM DBWindows IMPORT
  WindowClass;
FROM FormatString IMPORT
  FormatArg;
FROM Intuition IMPORT
  Screen, WBenchScreen,
  GetScreenData;
FROM Memory IMPORT
  MemReqSet;
FROM ScanString IMPORT
  ScanArg;
FROM SimpleScreens IMPORT
  ScreenNormalColumns, ScreenNormalRows;
FROM System IMPORT
  argc, argv, CmdLinePtr, CmdLineLength, GfxBase;

  
CONST
  (* Default Configuration parameter values *)
  DefConfigPrivateScreen         = FALSE;
  DefConfigInterlaceScreen       = FALSE;
  DefConfigScreenColor0          = 000FH;
  DefConfigScreenColor1          = 0FFFH;
  DefConfigScreenColor2          = 0000H;
  DefConfigScreenColor3          = 0F00H;
  DefConfigCheckModFileDate      = FALSE;
  DefConfigMaxModFilesLoaded     = 0;
  DefConfigStringSearchCase      = FALSE;
  DefConfigProgPriority          = 0;
  DefConfigProgStackSize         = 20000D;
  DefConfigProgStartupMode       = SMMainModule;
  DefConfigProgLanguageMode      = LMM2;
  DefConfigStatusWindowX         = 0;
  DefConfigStatusWindowY         = 0;
  DefConfigStatusWindowDetailPen = BYTE(0);
  DefConfigStatusWindowBlockPen  = BYTE(1);
  DefConfigStatusWindowFgColor   = BYTE(1);
  DefConfigStatusWindowBgColor   = BYTE(0);

  DirCurrent   = "";                 (* Current directory string *)
  CFileName    = "M2Debug.config";   (* Default startup configuration file *)
  AltCFileName = "s:M2Debug.config"; (* Alternate startup configuration file *)

  ReadFileMode  = "r"; (* fopen() read mode string *)
  WriteFileMode = "w"; (* fopen() write mode string *)

  CFileHdr = "M2Debug_1.0"; (* Configuration file header *)

  (* Configuration file commands *)
  CFCmdPrivateScreen           = "PrivateScreen";
  CFCmdInterlaceScreen         = "InterlaceScreen";
  CFCmdScreenColors            = "ScreenColors";
  CFCmdCheckModFileDate        = "CheckModFileDate";
  CFCmdMaxModFilesLoaded       = "MaxModFilesLoaded";
  CFCmdStringSearchCase        = "StringSearchCase";
  CFCmdProgPriority            = "ProgPriority";
  CFCmdProgStackSize           = "ProgStackSize";
  CFCmdProgCurrentDir          = "ProgCurrentDir";
  CFCmdProgStartupMode         = "ProgStartupMode";
  CFCmdProgLanguageMode        = "ProgLanguageMode";
  CFCmdSymbolDir               = "SymbolDir";
  CFCmdSourceDir               = "SourceDir";
  CFCmdSourceWindow            = "OpenSourceWindow";
  CFCmdDisassemblyWindow       = "OpenDisassemblyWindow";
  CFCmdSymbolicDataWindow      = "OpenSymbolicDataWindow";
  CFCmdMemoryWindow            = "OpenMemoryWindow";
  CFCmdModuleListWindow        = "OpenModuleListWindow";
  CFCmdProcedureChainWindow    = "OpenProcedureChainWindow";
  CFCmdBreakPointListWindow    = "OpenBreakPointListWindow";
  CFCmdRegistersWindow         = "OpenRegistersWindow";
  CFCmdInfoWindow              = "OpenInfoWindow";
  CFCmdDirectoryWindow         = "OpenDirectoryWindow";
  CFCmdStatusWindow            = "OpenStatusWindow";
  CFCmdNewSourceWindow         = "NewSourceWindow";
  CFCmdNewDisassemblyWindow    = "NewDisassemblyWindow";
  CFCmdNewSymbolicDataWindow   = "NewSymbolicDataWindow";
  CFCmdNewMemoryWindow         = "NewMemoryWindow";
  CFCmdNewModuleListWindow     = "NewModuleListWindow";
  CFCmdNewProcedureChainWindow = "NewProcedureChainWindow";
  CFCmdNewBreakPointListWindow = "NewBreakPointListWindow";
  CFCmdNewRegistersWindow      = "NewRegistersWindow";
  CFCmdNewInfoWindow           = "NewInfoWindow";
  CFCmdNewDirectoryWindow      = "NewDirectoryWindow";
  CFCmdEndOfFile               = "END";

  (* This value is used to mark a new window template as initialized *)
  NewWindowInfo = ConfigWindowPtr(0FFFFFFFFH);

VAR
  (* Table of OpenWindow configuration file command strings *)
  OpenWindowParmTable : ARRAY WindowClass OF ADDRESS;

  (* Table of NewWindow configuration file command strings *)
  NewWindowParmTable  : ARRAY WindowClass OF ADDRESS;

(* ===== PRIVATE ===== *)

(* DeleteCurrentDir - dealloc current dir config parm *)
PROCEDURE DeleteCurrentDir();
BEGIN
  IF (ConfigProgCurrentDir # NIL) THEN
    DeallocString(ConfigProgCurrentDir);
    ConfigProgCurrentDir := NIL;
  END;
END DeleteCurrentDir;

(* AddDirNode - create a new DirNode and link to end of list *)
PROCEDURE AddDirNode(VAR list: DirNodePtr; name: ADDRESS): BOOLEAN;
VAR
  str: ADDRESS;
  dn: DirNodePtr;
  cur, prev: DirNodePtr;
  i, len: INTEGER;
  ptr, ptr2 :ADDRESS;
BEGIN
  dn := NIL;

  (* Remove any trailing blanks in dir *)
  len := strlen(name);
  ptr := name + VAL(LONGCARD,len)-1D;
  WHILE (len > 0) AND (CHAR(ptr^) = ' ') DO
     ptr^ := 0C;
     ptr := ptr - 1D;
     DEC(len);
  END;

  (* Remove any leading blanks in dir *)
  i := 0;
  ptr := name;
  ptr2 := name;
  WHILE (i < len) AND (CHAR(ptr^) = ' ') DO
     ptr := ptr + 1D;
     INC(i);
  END;
  IF i > 0 THEN
     WHILE (i < len) DO
        ptr2^ := ptr^;
        ptr := ptr + 1D;
        ptr2 := ptr2 + 1D;
        INC(i);
     END;
     ptr2^ := 0C;
  END;

  (* Make copy of string *)
  str := AllocString(name);
  IF (str # NIL) THEN
    (* Allocate DirNode *)
    dn := AllocMemory(SIZE(DirNode), MemReqSet{});
    IF (dn # NIL) THEN
      (* Insert Node At Back of List *)
      IF (list = NIL) THEN (* Empty list *)
        list := dn;
      ELSE (* Non-empty list, find end and insert node *)
        cur := list;
        WHILE (cur # NIL) DO
          prev := cur;
          cur  := cur^.dnNext;
        END;
        prev^.dnNext := dn;
      END;

      (* Init node *)
      dn^.dnNext := NIL;
      dn^.dnName := str;
    ELSE (* failed to alloc node struct *)
      DeallocString(str);
    END;
  END;
  RETURN (dn # NIL);
END AddDirNode;

(* FreeDirNode - deallocate a DirNode *)
PROCEDURE FreeDirNode(VAR dn: DirNode);
BEGIN
  DeallocString(dn.dnName);
  DeallocMemory(ADR(dn));
END FreeDirNode;

(* DeleteDirList - delete both symbol and source directory lists *)
PROCEDURE DeleteDirLists();
VAR
  dn, fdn: DirNodePtr;
BEGIN
  dn := ConfigSymbolFileDirList;
  WHILE (dn # NIL) DO
    fdn := dn;
    dn  := dn^.dnNext;
    FreeDirNode(fdn^);
  END;
  ConfigSymbolFileDirList := NIL;

  dn := ConfigSourceFileDirList;
  WHILE (dn # NIL) DO
    fdn := dn;
    dn  := dn^.dnNext;
    FreeDirNode(fdn^);
  END;
  ConfigSourceFileDirList := NIL;
END DeleteDirLists;

(* AddWindowNode - create a ConfigWindow node and link to end of list *)
PROCEDURE AddWindowNode(): ConfigWindowPtr;
VAR
  cw: ConfigWindowPtr;
  cur: ConfigWindowPtr;
  prev: ConfigWindowPtr;
BEGIN
  cw := AllocMemory(SIZE(ConfigWindow), MemReqSet{});
  IF (cw # NIL) THEN
    (* Insert Node At Back of List *)
    IF (ConfigWindowList = NIL) THEN (* Empty List *)
      ConfigWindowList := cw;
    ELSE (* Non-empty list, find end and insert node *)
      cur := ConfigWindowList;
      WHILE (cur # NIL) DO
        prev := cur;
        cur  := cur^.cwNext;
      END;
      prev^.cwNext := cw;
    END;
    cw^.cwNext  := NIL;
  END;
  RETURN (cw);
END AddWindowNode;

(* DeleteConfigWindowList - delete all nodes in config window list *)
PROCEDURE DeleteConfigWindowList();
VAR
  cw, fcw: ConfigWindowPtr;
BEGIN
  cw := ConfigWindowList;
  WHILE (cw # NIL) DO
    fcw := cw;
    cw  := cw^.cwNext;
    DeallocMemory(fcw);
  END;
  ConfigWindowList := NIL;
END DeleteConfigWindowList;

(* ReadConfigFile - read configuration file *)
PROCEDURE ReadConfigFile(fileName: ADDRESS): BOOLEAN;
VAR
  f: FILE;
  b: BOOLEAN;
  argi: ARRAY [0..9] OF ScanArg;
  str: ARRAY [0..127] OF CHAR;
  firstParm: BOOLEAN;
  done: BOOLEAN;
  err: BOOLEAN;

  (* EqualStrings - compare default string and a specified string for equality*)
  PROCEDURE EqualStrings(str1: ADDRESS): BOOLEAN;
  BEGIN
    RETURN (strcmp(ADR(str[0]), str1) = 0);
  END EqualStrings;

  (* GetCmdString - read a configuration command string *)
  PROCEDURE GetCmdString();
  BEGIN
    (* Get Command String (read until left paren found) *)
    argi[0] := ADR(str[0]);
    IF (fscanf(f, ADR("%100[~( \n]"), argi) # 1) THEN
      err := TRUE; (* Invalid Config File! *)
    END;
    firstParm := TRUE;
  END GetCmdString;

  (* CheckForFirstParm - check for open paren if no parms have been parsed *)
  PROCEDURE CheckForFirstParm();
  BEGIN
    IF (firstParm) THEN
      firstParm := FALSE;

      (* Next non-white space char must be "(" *)
      argi[0] := ADR(str[0]);
      IF (fscanf(f, ADR("%[(]"), argi) # 1) THEN
        err := TRUE; (* Invalid Config File! *)
      END;
    ELSE
      argi[0] := ADR(str[0]);
      IF (fscanf(f, ADR("%[,]"), argi) # 1) THEN
        err := TRUE; (* Invalid Config File! *)
      END;
    END;
  END CheckForFirstParm;

  (* GetParmEnd - check for close parenthesis *)
  PROCEDURE GetParmEnd();
  BEGIN
    (* Next non-white space char must be ")" *)
    argi[0] := ADR(str[0]);
    IF (fscanf(f, ADR("%[)]"), argi) # 1) THEN
      err := TRUE; (* Invalid Config File! *)
    END;
  END GetParmEnd;

  (* GetStringParm - read a string parameter enclosed in single quotes *)
  PROCEDURE GetStringParm(buf: ADDRESS);
  VAR
    ch: CHAR;
  BEGIN
    CheckForFirstParm();

    argi[0] := ADR(ch);
    IF (fscanf(f, ADR(" %c"), argi) = 1) THEN
      IF (ch = "'") THEN
        argi[0] := buf;
        IF (fscanf(f, ADR("%100[~']"), argi) # 1) THEN
          buf^ := 0C; (* Empty string *)
        END;
        argi[0] := ADR(ch);
        IF (fscanf(f, ADR("%c"), argi) = 1) THEN
          IF (ch = "'") THEN
            RETURN; (* Success! *)
          END;
        END;
      END;
    END;
    err := TRUE; (* Invalid Config File! *)
  END GetStringParm;

  (* GetValue32Parm - read 32-bit value parameter *)
  PROCEDURE GetValue32Parm(VAR val: LONGWORD);
  VAR
    v: LONGCARD;
  BEGIN
    CheckForFirstParm();
    argi[0] := ADR(v);
    IF (fscanf(f, ADR("%ld"), argi) # 1) THEN
      err := TRUE; (* Invalid Config File! *)
    END;
    val := v;
  END GetValue32Parm;

  (* GetValue16Parm - read a 16-bit value parameter *)
  PROCEDURE GetValue16Parm(VAR val: WORD);
  VAR
    v: CARDINAL;
  BEGIN
    CheckForFirstParm();
    argi[0] := ADR(v);
    IF (fscanf(f, ADR("%d"), argi) # 1) THEN
      err := TRUE; (* Invalid Config File! *)
    END;
    val := v;
  END GetValue16Parm;

  (* GetValue8Parm - read 8-bit value parameter *)
  PROCEDURE GetValue8Parm(VAR val: BYTE);
  VAR
    v: INTEGER;
  BEGIN
    CheckForFirstParm();
    argi[0] := ADR(v);
    IF (fscanf(f, ADR("%d"), argi) # 1) OR (v < 0) OR (v > 255) THEN
      err := TRUE; (* Invalid Config File! *)
    END;
    val := BYTE(v);
  END GetValue8Parm;

  (* GetWindowParms - read all window related parameters for any window type *)
  PROCEDURE GetWindowParms(VAR cw: ConfigWindow);
  BEGIN
    WITH cw DO
      GetValue16Parm(cwX);
      GetValue16Parm(cwY);
      GetValue16Parm(cwWidth);
      GetValue16Parm(cwHeight);
      GetValue16Parm(cwMaxCharWidth);
      GetValue8Parm(cwDetailPen);
      GetValue8Parm(cwBlockPen);
      GetValue8Parm(cwFgColor);
      GetValue8Parm(cwBgColor);
      CASE (cwClass) OF
      | WCSource:      GetValue8Parm(cwSourceUpdateMode);
                       GetValue16Parm(cwSourceContextLines);
                       GetValue8Parm(cwSourceHighlightFgColor);
                       GetValue8Parm(cwSourceHighlightBgColor);
                       GetValue8Parm(cwSourceBreakPtFgColor);
                       GetValue8Parm(cwSourceBreakPtBgColor);
      | WCDisassembly: GetValue8Parm(cwDisAsmUpdateMode);
                       GetValue8Parm(cwDisAsmShowAdr);
                       GetValue8Parm(cwDisAsmShowOffset);
                       GetValue8Parm(cwDisAsmShowObjCode);
                       GetValue8Parm(cwDisAsmHighlightFgColor);
                       GetValue8Parm(cwDisAsmHighlightBgColor);
                       GetValue8Parm(cwDisAsmBreakPtFgColor);
                       GetValue8Parm(cwDisAsmBreakPtBgColor);
      | WCSymData:     GetValue8Parm(cwSymDataUpdateMode);
                       GetValue16Parm(cwSymDataValueWidth);
                       GetValue8Parm(cwSymDataAltFgColor);
      | WCMemory:      GetValue8Parm(cwMemoryDispFormat);
                       GetValue8Parm(cwMemoryNumberSize);
                       GetValue8Parm(cwMemoryNumberFormat);
      | WCInfo:        GetValue8Parm(cwInfoDispMode);
      END;
      GetParmEnd();
    END; (* WITH *)
  END GetWindowParms;

  (* CreateConfigWindow - read parms for a window to be opened *)
  PROCEDURE CreateConfigWindow(type: WindowClass);
  VAR
    cw: ConfigWindowPtr;
  BEGIN
    cw := AddWindowNode();
    IF (cw # NIL) THEN
      cw^.cwClass := type;
      GetWindowParms(cw^);
    ELSE
      err := TRUE; (* No memory error! *)
    END;
  END CreateConfigWindow;

  (* SetNewWindowInfo - read parms for a new window template *)
  PROCEDURE SetNewWindowInfo(type: WindowClass);
  BEGIN
    WITH ConfigNewWindowTable[type] DO
      cwNext  := NewWindowInfo;
      cwClass := type;
    END;
    GetWindowParms(ConfigNewWindowTable[type]);
  END SetNewWindowInfo;

BEGIN (* ReadConfigFile *)
  IF (fileName = NIL) THEN (* Startup Config File *)
    f := fopen(ADR(CFileName), ADR(ReadFileMode));
    IF (f = NIL) THEN
      f := fopen(ADR(AltCFileName), ADR(ReadFileMode));
      IF (f = NIL) THEN
        RETURN (TRUE); (* No Startup File (No Err!) *)
      END;
    END;
  ELSE (* Load Config File *)
    f := fopen(fileName, ADR(ReadFileMode));
    IF (f = NIL) THEN
      RETURN (FALSE); (* No Config File (Err!) *)
    END;
  END;

  SetSizeVerifyModeAllVTWindows(FALSE); (* Disable size verify *)

  (* Check if valid configuration file, abort if not valid *)
  argi[0] := ADR(str[0]);
  err := (fscanf(f, ADR("%s"), argi) # 1) OR (NOT EqualStrings(ADR(CFileHdr)));

  done := FALSE;
  LOOP
    IF (done) OR (err) THEN EXIT; END;

    GetCmdString();

    IF (EqualStrings(ADR(CFCmdPrivateScreen))) THEN
      GetValue8Parm(ConfigPrivateScreen);
      GetParmEnd();
    ELSIF (EqualStrings(ADR(CFCmdInterlaceScreen))) THEN
      GetValue8Parm(ConfigInterlaceScreen);
      GetParmEnd();
    ELSIF (EqualStrings(ADR(CFCmdScreenColors))) THEN
      GetValue16Parm(ConfigScreenColors[0]);
      GetValue16Parm(ConfigScreenColors[1]);
      GetValue16Parm(ConfigScreenColors[2]);
      GetValue16Parm(ConfigScreenColors[3]);
      GetParmEnd();
    ELSIF (EqualStrings(ADR(CFCmdCheckModFileDate))) THEN
      GetValue8Parm(ConfigCheckModFileDate);
      GetParmEnd();
    ELSIF (EqualStrings(ADR(CFCmdMaxModFilesLoaded))) THEN
      GetValue16Parm(ConfigMaxModFilesLoaded);
      GetParmEnd();
    ELSIF (EqualStrings(ADR(CFCmdStringSearchCase))) THEN
      GetValue8Parm(ConfigStringSearchCase);
      GetParmEnd();
    ELSIF (EqualStrings(ADR(CFCmdProgPriority))) THEN
      GetValue16Parm(ConfigProgPriority);
      GetParmEnd();
    ELSIF (EqualStrings(ADR(CFCmdProgStackSize))) THEN
      GetValue32Parm(ConfigProgStackSize);
      GetParmEnd();
    ELSIF (EqualStrings(ADR(CFCmdProgCurrentDir))) THEN
      GetStringParm(ADR(str[0]));
      DeleteCurrentDir();
      ConfigProgCurrentDir := AllocString(ADR(str[0]));
      IF (ConfigProgCurrentDir = NIL) THEN
        err := TRUE; (* No Memory Error! *)
      END;
      GetParmEnd();
    ELSIF (EqualStrings(ADR(CFCmdProgStartupMode))) THEN
      GetValue8Parm(ConfigProgStartupMode);
      GetParmEnd();
    ELSIF (EqualStrings(ADR(CFCmdProgLanguageMode))) THEN
      GetValue8Parm(ConfigProgLanguageMode);
      GetParmEnd();
    ELSIF (EqualStrings(ADR(CFCmdSymbolDir))) THEN
      GetStringParm(ADR(str[0]));
      IF (NOT AddDirNode(ConfigSymbolFileDirList, ADR(str[0]))) THEN
        err := TRUE; (* No Memory Error! *)
      END;
      GetParmEnd();
    ELSIF (EqualStrings(ADR(CFCmdSourceDir))) THEN
      GetStringParm(ADR(str[0]));
      IF (NOT AddDirNode(ConfigSourceFileDirList, ADR(str[0]))) THEN
        err := TRUE; (* No Memory Error! *)
      END;
      GetParmEnd();
    ELSIF (EqualStrings(ADR(CFCmdSourceWindow))) THEN
      CreateConfigWindow(WCSource);
    ELSIF (EqualStrings(ADR(CFCmdDisassemblyWindow))) THEN
      CreateConfigWindow(WCDisassembly);
    ELSIF (EqualStrings(ADR(CFCmdSymbolicDataWindow))) THEN
      CreateConfigWindow(WCSymData);
    ELSIF (EqualStrings(ADR(CFCmdMemoryWindow))) THEN
      CreateConfigWindow(WCMemory);
    ELSIF (EqualStrings(ADR(CFCmdModuleListWindow))) THEN
      CreateConfigWindow(WCModuleList);
    ELSIF (EqualStrings(ADR(CFCmdProcedureChainWindow))) THEN
      CreateConfigWindow(WCProcChain);
    ELSIF (EqualStrings(ADR(CFCmdBreakPointListWindow))) THEN
      CreateConfigWindow(WCBreakPointList);
    ELSIF (EqualStrings(ADR(CFCmdRegistersWindow))) THEN
      CreateConfigWindow(WCRegisters);
    ELSIF (EqualStrings(ADR(CFCmdInfoWindow))) THEN
      CreateConfigWindow(WCInfo);
    ELSIF (EqualStrings(ADR(CFCmdDirectoryWindow))) THEN
      CreateConfigWindow(WCDirectory);
    ELSIF (EqualStrings(ADR(CFCmdStatusWindow))) THEN
      GetValue16Parm(ConfigStatusWindowX);
      GetValue16Parm(ConfigStatusWindowY);
      GetValue8Parm(ConfigStatusWindowDetailPen);
      GetValue8Parm(ConfigStatusWindowBlockPen);
      GetValue8Parm(ConfigStatusWindowFgColor);
      GetValue8Parm(ConfigStatusWindowBgColor);
      GetParmEnd();
    ELSIF (EqualStrings(ADR(CFCmdNewSourceWindow))) THEN
      SetNewWindowInfo(WCSource);
    ELSIF (EqualStrings(ADR(CFCmdNewDisassemblyWindow))) THEN
      SetNewWindowInfo(WCDisassembly);
    ELSIF (EqualStrings(ADR(CFCmdNewSymbolicDataWindow))) THEN
      SetNewWindowInfo(WCSymData);
    ELSIF (EqualStrings(ADR(CFCmdNewMemoryWindow))) THEN
      SetNewWindowInfo(WCMemory);
    ELSIF (EqualStrings(ADR(CFCmdNewModuleListWindow))) THEN
      SetNewWindowInfo(WCModuleList);
    ELSIF (EqualStrings(ADR(CFCmdNewProcedureChainWindow))) THEN
      SetNewWindowInfo(WCProcChain);
    ELSIF (EqualStrings(ADR(CFCmdNewBreakPointListWindow))) THEN
      SetNewWindowInfo(WCBreakPointList);
    ELSIF (EqualStrings(ADR(CFCmdNewRegistersWindow))) THEN
      SetNewWindowInfo(WCRegisters);
    ELSIF (EqualStrings(ADR(CFCmdNewInfoWindow))) THEN
      SetNewWindowInfo(WCInfo);
    ELSIF (EqualStrings(ADR(CFCmdNewDirectoryWindow))) THEN
      SetNewWindowInfo(WCDirectory);
    ELSIF (EqualStrings(ADR(CFCmdEndOfFile))) THEN
      done := TRUE;
    ELSE (* Invalid Command *)
      err := TRUE;
    END;
  END; (* LOOP *)

  b := fclose(f);
  IF (NOT b) THEN
    err := TRUE;
  END;

  IF (err) THEN
    DeleteCurrentDir();
    DeleteDirLists();
    DeleteConfigWindowList();
  END;

  SetSizeVerifyModeAllVTWindows(TRUE); (* Enable size verify *)

  RETURN (NOT err);
END ReadConfigFile;

(* WriteConfigFile - write configuration file *)
PROCEDURE WriteConfigFile(fileName: ADDRESS): BOOLEAN;
VAR
  err: BOOLEAN;
  f: FILE;
  argo: ARRAY [0..1] OF FormatArg;

  (* PutValueParm - write config file command with one value parameter *)
  PROCEDURE PutValueParm(parmName: ADDRESS; value: LONGINT);
  VAR
    argo: ARRAY [0..1] OF FormatArg;
  BEGIN
    argo[0].L := parmName;
    argo[1].L := value;
    fprintf(f, ADR("%s(%ld)\n"), argo);
  END PutValueParm;

  (* PutValue8Parm - write config file command with one 8-bit value parameter *)
  PROCEDURE PutValue8Parm(parmName: ADDRESS; val: BYTE);
  BEGIN
    PutValueParm(parmName, LONGCARD(val));
  END PutValue8Parm;

  (* PutScreenColors - write config screen colors command *)
  PROCEDURE PutScreenColors();
  VAR
    argo: ARRAY [0..4] OF FormatArg;
  BEGIN
    argo[0].L := ADR(CFCmdScreenColors);
    argo[1].W := ConfigScreenColors[0];
    argo[2].W := ConfigScreenColors[1];
    argo[3].W := ConfigScreenColors[2];
    argo[4].W := ConfigScreenColors[3];
    fprintf(f, ADR("%s(%u,%u,%u,%u)\n"), argo);
  END PutScreenColors;

  (* PutStringParm - write a config file command with a string parameter *)
  PROCEDURE PutStringParm(parmName: ADDRESS; str: ADDRESS);
  VAR
    argo: ARRAY [0..1] OF FormatArg;
  BEGIN
    IF (str # NIL) THEN
      argo[0].L := parmName;
      argo[1].L := str;
      fprintf(f, ADR("%s('%s')\n"), argo);
    END;
  END PutStringParm;

  (* PutDirList - write a config file directory list command (entire list) *)
  PROCEDURE PutDirList(parmName: ADDRESS; dn: DirNodePtr);
  VAR
    argo: ARRAY [0..1] OF FormatArg;
  BEGIN
    WHILE (dn # NIL) DO
      IF (strlen(dn^.dnName) # 0) THEN
        argo[0].L := parmName;
        argo[1].L := dn^.dnName;
        fprintf(f, ADR("%s('%s')\n"), argo);
      END;
      dn := dn^.dnNext;
    END;
  END PutDirList;

  (* PutWindow - write a config file window command (open or new template) *)
  PROCEDURE PutWindow(VAR cw: ConfigWindow; openParm: BOOLEAN);
  VAR
    argo: ARRAY [0..10] OF FormatArg;
  BEGIN
    WITH cw DO
      IF (openParm) THEN
        argo[0].L := OpenWindowParmTable[cwClass];
      ELSE
        argo[0].L := NewWindowParmTable[cwClass];
      END;
      argo[1].W := cwX;
      argo[2].W := cwY;
      argo[3].W := cwWidth;
      argo[4].W := cwHeight;
      argo[5].W := cwMaxCharWidth;
      argo[6].W := CARDINAL(cwDetailPen);
      argo[7].W := CARDINAL(cwBlockPen);
      argo[8].W := CARDINAL(cwFgColor);
      argo[9].W := CARDINAL(cwBgColor);
      fprintf(f, ADR("%s(%u,%u,%u,%u,%u,%u,%u,%u,%u"), argo);

      CASE (cwClass) OF
      | WCSource:      argo[0].W := CARDINAL(cwSourceUpdateMode);
                       argo[1].W := cwSourceContextLines;
                       argo[2].W := CARDINAL(cwSourceHighlightFgColor);
                       argo[3].W := CARDINAL(cwSourceHighlightBgColor);
                       argo[4].W := CARDINAL(cwSourceBreakPtFgColor);
                       argo[5].W := CARDINAL(cwSourceBreakPtBgColor);
                       fprintf(f, ADR(",%u,%u,%u,%u,%u,%u"), argo);
      | WCDisassembly: argo[0].W := CARDINAL(cwDisAsmUpdateMode);
                       argo[1].W := CARDINAL(cwDisAsmShowAdr);
                       argo[2].W := CARDINAL(cwDisAsmShowOffset);
                       argo[3].W := CARDINAL(cwDisAsmShowObjCode);
                       argo[4].W := CARDINAL(cwDisAsmHighlightFgColor);
                       argo[5].W := CARDINAL(cwDisAsmHighlightBgColor);
                       argo[6].W := CARDINAL(cwDisAsmBreakPtFgColor);
                       argo[7].W := CARDINAL(cwDisAsmBreakPtBgColor);
                       fprintf(f, ADR(",%u,%u,%u,%u,%u,%u,%u,%u"), argo);
      | WCSymData:     argo[0].W := CARDINAL(cwSymDataUpdateMode);
                       argo[1].W := cwSymDataValueWidth;
                       argo[2].W := CARDINAL(cwSymDataAltFgColor);
                       fprintf(f, ADR(",%u,%u,%u"), argo);
      | WCMemory:      argo[0].W := CARDINAL(cwMemoryDispFormat);
                       argo[1].W := CARDINAL(cwMemoryNumberSize);
                       argo[2].W := CARDINAL(cwMemoryNumberFormat);
                       fprintf(f, ADR(",%u,%u,%u"), argo);
      | WCInfo:        argo[0].W := CARDINAL(cwInfoDispMode);
                       fprintf(f, ADR(",%u"), argo);
      END;

      fprintf(f, ADR(")\n"), argo);
    END;
  END PutWindow;

  (* PutOpenWindow - write a config file cmd for each currently open window *)
  PROCEDURE PutOpenWindows();
  VAR
    w: WindowClass;
    index: CARDINAL;
    config: ConfigWindow;
  BEGIN
    FOR w := WCSource TO WCDirectory DO
      index := 0;
      WHILE (WindowInfoProcTable[w](index, config)) DO
        config.cwClass := w;
        PutWindow(config, TRUE);
      END;
    END;
  END PutOpenWindows;

  (* PutNewWindowTable - write a config file cmd for each new window template *)
  PROCEDURE PutNewWindowTable();
  VAR
    w: WindowClass;
  BEGIN
    FOR w := WCSource TO WCDirectory DO
      PutWindow(ConfigNewWindowTable[w], FALSE);
    END;
  END PutNewWindowTable;

  (* PutStatusWindow - write config file status window command *)
  PROCEDURE PutStatusWindow();
  VAR
    argo: ARRAY [0..6] OF FormatArg;
  BEGIN
    GetStatusWindowInfo();
    argo[0].L := ADR(CFCmdStatusWindow);
    argo[1].W := ConfigStatusWindowX;
    argo[2].W := ConfigStatusWindowY;
    argo[3].W := CARDINAL(ConfigStatusWindowDetailPen);
    argo[4].W := CARDINAL(ConfigStatusWindowBlockPen);
    argo[5].W := CARDINAL(ConfigStatusWindowFgColor);
    argo[6].W := CARDINAL(ConfigStatusWindowBgColor);
    fprintf(f, ADR("%s(%u,%u,%u,%u,%u,%u)\n"), argo);
  END PutStatusWindow;

BEGIN (* WriteConfigFile *)
  SetSizeVerifyModeAllVTWindows(FALSE); (* Disable size verify *)

  err := TRUE;
  f := fopen(fileName, ADR(WriteFileMode));
  IF (f # NIL) THEN
    fprintf(f, ADR(CFileHdr), argo);
    fprintf(f, ADR("\n"), argo);

    PutValue8Parm(ADR(CFCmdPrivateScreen), ConfigPrivateScreen);
    PutValue8Parm(ADR(CFCmdInterlaceScreen), ConfigInterlaceScreen);

    PutScreenColors();

    PutValue8Parm(ADR(CFCmdCheckModFileDate), ConfigCheckModFileDate);
    PutValueParm(ADR(CFCmdMaxModFilesLoaded), ConfigMaxModFilesLoaded);
    PutValue8Parm(ADR(CFCmdStringSearchCase), ConfigStringSearchCase);

    PutValueParm(ADR(CFCmdProgPriority), ConfigProgPriority); (* Sign-ext *)
    PutValueParm(ADR(CFCmdProgStackSize), ConfigProgStackSize);
    PutStringParm(ADR(CFCmdProgCurrentDir), ConfigProgCurrentDir);
    PutValue8Parm(ADR(CFCmdProgStartupMode), ConfigProgStartupMode);
    PutValue8Parm(ADR(CFCmdProgLanguageMode), ConfigProgLanguageMode);

    PutDirList(ADR(CFCmdSymbolDir), ConfigSymbolFileDirList);
    PutDirList(ADR(CFCmdSourceDir), ConfigSourceFileDirList);

    PutStatusWindow();
    PutOpenWindows();
    PutNewWindowTable();

    fprintf(f, ADR(CFCmdEndOfFile), argo);

    err := NOT fclose(f);
  END;

  SetSizeVerifyModeAllVTWindows(TRUE); (* Enable size verify *)

  RETURN (NOT err);
END WriteConfigFile;

(* InitDefaultConfig - initialize default configuration parameters *)
PROCEDURE InitDefaultConfig(): BOOLEAN;
VAR
  ok: BOOLEAN;
BEGIN
  DeleteCurrentDir();
  DeleteDirLists();
  DeleteConfigWindowList();

  ConfigPrivateScreen       := DefConfigPrivateScreen;
  ConfigInterlaceScreen     := DefConfigInterlaceScreen;
  ConfigScreenColors[0]     := DefConfigScreenColor0;
  ConfigScreenColors[1]     := DefConfigScreenColor1;
  ConfigScreenColors[2]     := DefConfigScreenColor2;
  ConfigScreenColors[3]     := DefConfigScreenColor3;
  ConfigCheckModFileDate    := DefConfigCheckModFileDate;
  ConfigMaxModFilesLoaded   := DefConfigMaxModFilesLoaded;
  ConfigStringSearchCase    := DefConfigStringSearchCase;
  ConfigProgPriority        := DefConfigProgPriority;
  ConfigProgStackSize       := DefConfigProgStackSize;
  ConfigProgStartupMode     := DefConfigProgStartupMode;
  ConfigProgLanguageMode    := DefConfigProgLanguageMode;
  ConfigWindowList          := NIL;

  ok := TRUE;
  ok := ok AND AddDirNode(ConfigSymbolFileDirList, ADR(DirCurrent));
  ok := ok AND AddDirNode(ConfigSourceFileDirList, ADR(DirCurrent));
  IF (NOT ok) THEN
    DeleteDirLists();
  END;

  ConfigStatusWindowX         := DefConfigStatusWindowX;
  ConfigStatusWindowY         := DefConfigStatusWindowY;
  ConfigStatusWindowDetailPen := DefConfigStatusWindowDetailPen;
  ConfigStatusWindowBlockPen  := DefConfigStatusWindowBlockPen;
  ConfigStatusWindowFgColor   := DefConfigStatusWindowFgColor;
  ConfigStatusWindowBgColor   := DefConfigStatusWindowBgColor;

  RETURN (ok);
END InitDefaultConfig;

(* CalcScreenConfig - calculate screen configuration parameters *)
PROCEDURE CalcScreenConfig();
VAR
  WBscr: Screen;
BEGIN
  IF (ConfigPrivateScreen) THEN (* Custom screen *)
    ConfigScreenWidth  := ScreenNormalColumns;
    ConfigScreenHeight := ScreenNormalRows;

    IF (ConfigInterlaceScreen) THEN (* Interlace mode is twice as high *)
      INC(ConfigScreenHeight, ConfigScreenHeight);
    END;
  ELSE (* WorkBench screen *)
    IF GetScreenData(ADR(WBscr), SIZE(WBscr), WBenchScreen, WBscr) THEN (* ok *)
      ConfigScreenWidth  := WBscr.Width;
      ConfigScreenHeight := WBscr.Height;
    ELSE (* Unable to find and/or open a WorkBench Screen *)
      ConfigScreenWidth  := ScreenNormalColumns;
      ConfigScreenHeight := ScreenNormalRows;
    END;
  END;
END CalcScreenConfig;

(* AdjustWindowParms - adjust window parms so that it fits in the screen *)
PROCEDURE AdjustWindowParms(VAR cw: ConfigWindow);
BEGIN
  WITH cw DO
    IF (cwX + cwWidth > ConfigScreenWidth) THEN (* Reset to left edge *)
      cwX := 0;
    END;
    IF (cwWidth > ConfigScreenWidth) THEN (* Reset to screen width *)
      cwWidth := ConfigScreenWidth;
    END;

    IF (cwY + cwHeight > ConfigScreenHeight) THEN (* Reset to top edge *)
      cwY := 0;
    END;

    IF (cwHeight > ConfigScreenHeight) THEN (* Reset to screen height *)
      cwHeight := ConfigScreenHeight;
    END;
  END;
END AdjustWindowParms;

(* AdjustConfigWindows - adjust all config windows to be opened to fit screen *)
PROCEDURE AdjustConfigWindows();
VAR
  cw: ConfigWindowPtr;
BEGIN
  cw := ConfigWindowList;
  WHILE (cw # NIL) DO
    AdjustWindowParms(cw^);
    cw := cw^.cwNext;
  END; (* WHILE *)
END AdjustConfigWindows;

(* DoNewWindowTable - init any uninitialized window templates *)
PROCEDURE DoNewWindowTable();
VAR
  w: WindowClass;
BEGIN
  FOR w := WCSource TO WCDirectory DO
    WITH ConfigNewWindowTable[w] DO
      IF (cwNext = NIL) THEN
        cwNext         := NewWindowInfo;
        cwX            := 0;
        cwY            := 0;
        cwWidth        := 320;
        cwHeight       := 50;
        cwMaxCharWidth := 60;
        cwDetailPen    := BYTE(0);
        cwBlockPen     := BYTE(1);
        cwFgColor      := BYTE(1);
        cwBgColor      := BYTE(0);
        cwClass        := w;
        CASE (w) OF
        | WCSource:      cwSourceUpdateMode       := BYTE(0);
                         cwSourceContextLines     := 2;
                         cwSourceHighlightFgColor := BYTE(2);
                         cwSourceHighlightBgColor := BYTE(3);
                         cwSourceBreakPtFgColor   := BYTE(3);
                         cwSourceBreakPtBgColor   := BYTE(2);
        | WCDisassembly: cwDisAsmUpdateMode       := BYTE(0);
                         cwDisAsmShowAdr          := TRUE;
                         cwDisAsmShowOffset       := FALSE;
                         cwDisAsmShowObjCode      := FALSE;
                         cwDisAsmHighlightFgColor := BYTE(2);
                         cwDisAsmHighlightBgColor := BYTE(3);
                         cwDisAsmBreakPtFgColor   := BYTE(3);
                         cwDisAsmBreakPtBgColor   := BYTE(2);
        | WCSymData:     cwSymDataUpdateMode := BYTE(0);
                         cwSymDataValueWidth := 14;
                         cwSymDataAltFgColor := BYTE(3);
        | WCMemory:      cwMemoryDispFormat   := BYTE(3);  (* Number *)
                         cwMemoryNumberSize   := BYTE(1);  (* Word   *)
                         cwMemoryNumberFormat := BYTE(2);  (* Hex    *)
        | WCInfo:        cwInfoDispMode := BYTE(0);
        END; (* CASE *)
      END; (* IF *)
    END; (* WITH *)
    AdjustWindowParms(ConfigNewWindowTable[w]);
  END; (* FOR *)
END DoNewWindowTable;

(* ReadCLIParms - read CLI parameters *)
PROCEDURE ReadCLIParms(): BOOLEAN;
VAR
  ok: BOOLEAN;
  argi: ARRAY [0..0] OF ScanArg;
BEGIN
  ok := TRUE;
  IF (argc > 1) THEN
    IF (argv^[1]^[0] = "$") THEN (* PMDInfo adr *)
      CLIPostMortemDebug := TRUE;
      argi[0] := ADR(CLIProgPMDInfo);
      ok := (sscanf(ADR(argv^[1]^[1]), ADR("%lx"), argi) = 1);
    ELSE
      CLIProgFileName    := argv^[1];
      CLIProgParmsPtr    := CmdLinePtr + LONGCARD(strlen(argv^[1]));
      CLIProgParmsLength := CmdLineLength - LONGCARD(strlen(argv^[1]));
    END;
  END;
  RETURN (ok);
END ReadCLIParms;

(* ===== PUBLIC ===== *)

(* GetCLIParms - get CLI parameters and setup global vars *)
PROCEDURE GetCLIParms(): BOOLEAN;
BEGIN
  CLIConfigFileName := NIL;
  RETURN (ReadCLIParms());
END GetCLIParms;

(* LoadConfigFile - read a configuration file or startup config file *)
PROCEDURE LoadConfigFile(fileName: ADDRESS): BOOLEAN;
VAR
  ok: BOOLEAN;
BEGIN
  ok := InitDefaultConfig();
  IF (ok) THEN
    ok := ReadConfigFile(fileName);
    IF (ok) THEN
      IF (fileName = NIL) THEN (* Screen Only Setup At Startup *)
        CalcScreenConfig();
      END;

      AdjustConfigWindows();
      DoNewWindowTable();
    END;
  END;
  RETURN (ok);
END LoadConfigFile;

(* SaveConfigFile - write a configuration file *)
PROCEDURE SaveConfigFile(fileName: ADDRESS): BOOLEAN;
BEGIN
  RETURN (WriteConfigFile(fileName));
END SaveConfigFile;

(* SetNewWindowParms - setup a new window template *)
PROCEDURE SetNewWindowParms(winClass: WindowClass; winIndex: CARDINAL);
BEGIN
  IF (WindowInfoProcTable[winClass](winIndex, ConfigNewWindowTable[winClass])) THEN
  END;
END SetNewWindowParms;

(* SetNewProgCurrentDir - set a new current directory path *)
PROCEDURE SetNewProgCurrentDir(newDirStr: ADDRESS);
BEGIN
  DeleteCurrentDir();
  ConfigProgCurrentDir := AllocString(newDirStr);
END SetNewProgCurrentDir;

(* SetNewDirList - set a new directory list and free the old list *)
PROCEDURE SetNewDirList(VAR oldList: DirNodePtr; newList: DirNodePtr);
VAR
  p: DirNodePtr;
  fp: DirNodePtr;
BEGIN
  p := oldList;
  WHILE (p # NIL) DO
    fp := p;
    p  := p^.dnNext;
    FreeDirNode(fp^);
  END;

  oldList := newList;
END SetNewDirList;

(* ConvDirListToString - convert a directory list to a string *)
PROCEDURE ConvDirListToString(dn: DirNodePtr; str: ADDRESS);
VAR
  p: DirNodePtr;
  len: CARDINAL;
  end: ADDRESS;
BEGIN
  str^ := 0C;
  p    := dn;
  WHILE (p # NIL) DO
    IF (strlen(p^.dnName) # 0) THEN
      strcat(str, p^.dnName);
      strcat(str, ADR(","));
    END;
    p := p^.dnNext;
  END;
  len := strlen(str);
  IF (len # 0) THEN (* Eliminate "," at the end if not an empty string *)
    end  := str + LONGCARD(len) - 1D;
    end^ := 0C;
  END;
END ConvDirListToString;

(* ConvStringToDirList - convert a string of directory names to a dir list *)
PROCEDURE ConvStringToDirList(dirStr: ADDRESS): DirNodePtr;
VAR
  list: DirNodePtr;
  count: CARDINAL;
  str: ADDRESS;
  len: CARDINAL;
  ok: BOOLEAN;
BEGIN
  count := 0;
  str   := dirStr;
  IF (str # NIL) THEN
    WHILE (CHAR(str^) # 0C) DO
      IF (CHAR(str^) = ",") THEN
        str^ := 0C;
        INC(count);
      END;
      INC(str);
    END;
    INC(count); (* last dir-name has no "," *)
  END;

  list := NIL;
  ok   := AddDirNode(list, ADR(DirCurrent));

  str := dirStr;
  WHILE (count # 0) DO
    len := strlen(str);
    IF (len # 0) THEN
      ok := AddDirNode(list, str);
    END;
    INC(str, len + 1);
    DEC(count);
  END;
  RETURN (list);
END ConvStringToDirList;

(* InitDBConfiguration - initialize DBConfiguration module *)
PROCEDURE InitDBConfiguration(): BOOLEAN;
BEGIN
  OpenWindowParmTable[WCSource]         := ADR(CFCmdSourceWindow);
  OpenWindowParmTable[WCDisassembly]    := ADR(CFCmdDisassemblyWindow);
  OpenWindowParmTable[WCSymData]        := ADR(CFCmdSymbolicDataWindow);
  OpenWindowParmTable[WCMemory]         := ADR(CFCmdMemoryWindow);
  OpenWindowParmTable[WCModuleList]     := ADR(CFCmdModuleListWindow);
  OpenWindowParmTable[WCProcChain]      := ADR(CFCmdProcedureChainWindow);
  OpenWindowParmTable[WCBreakPointList] := ADR(CFCmdBreakPointListWindow);
  OpenWindowParmTable[WCRegisters]      := ADR(CFCmdRegistersWindow);
  OpenWindowParmTable[WCInfo]           := ADR(CFCmdInfoWindow);
  OpenWindowParmTable[WCDirectory]      := ADR(CFCmdDirectoryWindow);

  NewWindowParmTable[WCSource]         := ADR(CFCmdNewSourceWindow);
  NewWindowParmTable[WCDisassembly]    := ADR(CFCmdNewDisassemblyWindow);
  NewWindowParmTable[WCSymData]        := ADR(CFCmdNewSymbolicDataWindow);
  NewWindowParmTable[WCMemory]         := ADR(CFCmdNewMemoryWindow);
  NewWindowParmTable[WCModuleList]     := ADR(CFCmdNewModuleListWindow);
  NewWindowParmTable[WCProcChain]      := ADR(CFCmdNewProcedureChainWindow);
  NewWindowParmTable[WCBreakPointList] := ADR(CFCmdNewBreakPointListWindow);
  NewWindowParmTable[WCRegisters]      := ADR(CFCmdNewRegistersWindow);
  NewWindowParmTable[WCInfo]           := ADR(CFCmdNewInfoWindow);
  NewWindowParmTable[WCDirectory]      := ADR(CFCmdNewDirectoryWindow);

  WindowInfoProcTable[WCSource]         := GetSourceWindowInfo;
  WindowInfoProcTable[WCDisassembly]    := GetDisAsmWindowInfo;
  WindowInfoProcTable[WCSymData]        := GetSymDataWindowInfo;
  WindowInfoProcTable[WCMemory]         := GetMemoryWindowInfo;
  WindowInfoProcTable[WCModuleList]     := GetModuleListWindowInfo;
  WindowInfoProcTable[WCProcChain]      := GetProcChainWindowInfo;
  WindowInfoProcTable[WCBreakPointList] := GetBreakPointWindowInfo;
  WindowInfoProcTable[WCRegisters]      := GetRegistersWindowInfo;
  WindowInfoProcTable[WCInfo]           := GetInfoWindowInfo;
  WindowInfoProcTable[WCDirectory]      := GetDirectoryWindowInfo;

  RETURN (TRUE);
END InitDBConfiguration;

(* CleanupDBConfiguration - cleanup DBConfiguration module *)
PROCEDURE CleanupDBConfiguration();
BEGIN
  DeleteCurrentDir();
  DeleteDirLists();
  DeleteConfigWindowList();
END CleanupDBConfiguration;

END DBConfiguration.
