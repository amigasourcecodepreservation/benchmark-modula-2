(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBSelection.MOD                   Version: Amiga.01.10             *
 * Created: 01/19/89   Updated: 03/19/89   Author: Leon Frenkel             *
 * Description: Debugger user-selection module.                             *
 ****************************************************************************)

IMPLEMENTATION MODULE DBSelection;

FROM SYSTEM IMPORT
  ADDRESS,
  ADR;
FROM DBDisAsmWindows IMPORT
  UpdateDisAsmSelection;
FROM DBTextWindows IMPORT
  VTWindow,
  GetDisplayRangeVTWindow, RefreshRangeVTWindow, WriteColorVTWindow;
FROM DBWindows IMPORT
  WindowClass, WindowClassSet;

VAR
  SelSet  : BOOLEAN;       (* TRUE = selection set, FALSE = no selection *)
  SelData : SelectionData; (* Current selection data *)

(* ===== PRIVATE ===== *)

(* UpdateSelection - update window with new selection or restore window *)
PROCEDURE UpdateSelection();
VAR
  first, last, line: CARDINAL;
BEGIN
  WITH SelData DO
    IF (sdWindowClass = WCDisassembly) THEN
      UpdateDisAsmSelection(sdVTWindow, sdAdr);
    ELSE
      GetDisplayRangeVTWindow(sdVTWindow, first, last);
      IF (first <= sdLineNo) AND (last >= sdLineNo) THEN
        line := sdLineNo - first;
        IF (NOT SelSet) THEN
          WriteColorVTWindow(sdVTWindow, line, sdStartPos, sdEndPos, 
                             sdDefFgColor, sdDefBgColor);
        END;
        RefreshRangeVTWindow(sdVTWindow, line, line);
      END;
    END;
  END;
END UpdateSelection;

(* ===== PUBLIC ===== *)

(* ClearSelection - if selection set then clear *)
PROCEDURE ClearSelection();
BEGIN
  IF (SelSet) THEN
    SelSet := FALSE;
    UpdateSelection();
  END;
END ClearSelection;

(* GetSelection - if selection set then return selection info *)
PROCEDURE GetSelection(VAR data: SelectionData): BOOLEAN;
VAR
  ok: BOOLEAN;
BEGIN
  ok := SelSet;
  IF (ok) THEN
    data := SelData;
  END;
  RETURN (ok);
END GetSelection;

(* SetSelection - clear old selection.  set new selection info. update window *)
PROCEDURE SetSelection(VAR data: SelectionData);
BEGIN
  IF (SelSet) THEN
    ClearSelection();
  END;
  SelSet       := TRUE;
  SelData      := data;
  UpdateSelection();
END SetSelection;

(* IsPosSelected - check if window coordinates are in current selection *)
PROCEDURE IsPosSelected(window: VTWindow; x, y: CARDINAL): BOOLEAN;
BEGIN
  WITH SelData DO
    RETURN ((SelSet) AND (sdVTWindow = window) AND (sdLineNo = y) AND
           (sdStartPos <= x) AND (sdEndPos >= x));
  END;
END IsPosSelected;

(* IsDisAsmPosSelected - check if current selection is in address range *)
PROCEDURE IsDisAsmPosSelected(window: VTWindow; startAdr, endAdr: ADDRESS): BOOLEAN;
BEGIN
  WITH SelData DO
    RETURN ((SelSet) AND (sdVTWindow = window) AND 
           (sdWindowClass = WCDisassembly) AND
           (startAdr <= sdAdr) AND (endAdr > sdAdr));
  END;
END IsDisAsmPosSelected;

(* IfCurrentWindowClearSelection - if selection in current window then clear *)
PROCEDURE IfCurrentWindowClearSelection(window: VTWindow);
BEGIN
  IF (SelSet) AND (SelData.sdVTWindow = window) THEN
    ClearSelection();
  END;
END IfCurrentWindowClearSelection;

(* GetSelectionLineInfo - if selection on specified line then return info *)
PROCEDURE GetSelectionLineInfo(window: VTWindow; lineNo: CARDINAL;
                               VAR startPos, endPos: CARDINAL): BOOLEAN;
VAR
  ok: BOOLEAN;
BEGIN
  WITH SelData DO
    ok := (SelSet) AND (sdVTWindow = window) AND (sdLineNo = lineNo);
    IF (ok) THEN
      startPos := sdStartPos;
      endPos   := sdEndPos;
    END;
  END;
  RETURN (ok);
END GetSelectionLineInfo;

(* GetDisAsmSelectionLineInfo - if selection in adr range then return info *)
PROCEDURE GetDisAsmSelectionLineInfo(window: VTWindow; startAdr, endAdr: ADDRESS;
                                     VAR startPos, endPos: CARDINAL): BOOLEAN;
VAR
  ok: BOOLEAN;
BEGIN
  WITH SelData DO
    ok := (SelSet) AND (sdVTWindow = window) AND
          (sdWindowClass = WCDisassembly) AND
          (startAdr <= sdAdr) AND (endAdr > sdAdr);
    IF (ok) THEN
      startPos := sdStartPos;
      endPos   := sdEndPos;
    END;
  END;
  RETURN (ok);
END GetDisAsmSelectionLineInfo;

(* InitDBSelection - initialize DBSelection module *)
PROCEDURE InitDBSelection(): BOOLEAN;
BEGIN
  RETURN (TRUE);
END InitDBSelection;

(* CleanupDBSelection - cleanup DBSelection module *)
PROCEDURE CleanupDBSelection();
BEGIN
END CleanupDBSelection;

END DBSelection.
