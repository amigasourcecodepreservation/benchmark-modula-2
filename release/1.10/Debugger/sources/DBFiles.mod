
(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBFiles.MOD                       Version: Amiga.01.10             *
 * Created: 01/03/89   Updated: 03/21/94   Author: Leon Frenkel             *
 * Description: Debugger executable, symbol, source file handler module.    *
 ****************************************************************************)

IMPLEMENTATION MODULE DBFiles;

(***************************************************************************
 * Changes for new release 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.2 -----
 * 
 * BUGFIX: Source and Symbol file Dir List requires user enter a trailing '/'
 * 
 *    1. Internal proc MakeFileName() now adds a '/' between "dir" and "name"
 *       if "dir" is not null and does not end with either a '/' or a ':'.
 * 
 * Support for IEEE and FPP REALs
 * 
 *   1. As of now, simple check for an import of SystemIEEE
 *      in CreateModuleInfoArray(). If found, poke "1" into System.REALflavor
 *      (M2Debug's System.REALflavor), otherwise poke "0".
 *
 *               ----- Beta.3 -----
 * 
 * Improve User Error Messages
 * 
 *    1. Added two return parameters to FindAvailableFiles() - MainModFileStatus
 *       and MainRfmFileStatus. These take on values from a :
 * 
 *    2. New type for FindAvailableFiles() return - FAFMainFileErrors, so that
 *       the RunProgramAsSLD() caller can display a more precise message as to
 *       why it won't startup.
 * 
 *    3. In LoadProgram(), DOS returns ErrorObjectWrongType if file is not
 *       executable. Check for this to output the appropriate message.
 * 
 *    4. In FindAvailableFiles(), added booleans RfmFound, RfmBadKey,
 *       ModFound, ModBadDate, so that the new VAR parameters MainModFileStatus
 *       and MainRfmFileStatus can be correctly set.
 *
 *               ----- Beta.4 -----
 * 
 * SymWindow Display of LONGREAL Values
 * 
 *    1. In CreateModuleInfoArray(), checking each module for "SystemIEEEdp",
 *       and opening both IEEEdp libraries into System.MathIeeeDoubBasBase
 *       and System.MathIeeeDoubTransBase if not already opened.
 * 
 *    2. Added two new CreateModuleInfoErrors, MIErrNoIEEELib and
 *       MIErrNoIEEEdpLib. If the corresponging library base(s) are still
 *       NIL after they should be opened.
 * 
 *       NOTE: 
 *         M2Debug now IMPORTs both SystemIEEE and SystemFFP.
 * 
 *         The "null" SystemIEEE and SystemIEEEdp are used so that these librs
 *         never get opened if only FFP (or no REALs). They are actually opened
 *         in DBFiles if needed.
 * 
 *         Currently, RealOps attempts to open the mathieeesingbas library (and
 *         store it in SystemIEEE), but does not complain if the open fails.
 * 
 ***************************************************************************
*)

FROM SYSTEM IMPORT
  ADDRESS,
  ADR, LONG;
FROM System IMPORT
  MathIeeeDoubBasBase, MathIeeeDoubTransBase, MathIeeeSingBasBase, REALflavor;
FROM AmigaDOS IMPORT
  ErrorNoFreeStore, ErrorFileNotObject, ErrorObjectNotFound,
  ErrorObjectWrongType, SharedLock,
  DateStampRecord, FileLock, FileInfoBlock, FileInfoBlockPtr,
  FileHandle, ModeOldFile,
  LoadSeg, UnLoadSeg, IoErr, Examine, Lock, UnLock, Read, Open, Close;
IMPORT CStdIO;
FROM CStdIO IMPORT
  FILE,
  fopen, fclose, fseek, ferror;
FROM CStrings IMPORT
  strcpy, strcat, strcmp, strlen, rindex;
FROM DBConfiguration IMPORT
  DirNodePtr,
  ConfigSymbolFileDirList, ConfigSourceFileDirList, ConfigCheckModFileDate,
  ConfigMaxModFilesLoaded, ConfigStringSearchCase;
FROM DBMisc IMPORT
  ConvToLong,
  AllocMemory, DeallocMemory, ReduceMemory, AllocString, DeallocString,
  GetMemorySize, BPTRtoAPTR;
FROM DBSourceWindows IMPORT
  ClearSourceWindowsShowingFile;
FROM DBStatusWindow IMPORT
  ShowErrMsgNoMemory,
  ShowError;
FROM DBTextWindows IMPORT
  SetSizeVerifyModeAllVTWindows;
FROM DBWindows IMPORT
  WindowClass, WindowClassSet,
  UpdateWindows;
FROM Libraries IMPORT
  OpenLibrary;
FROM Memory IMPORT
  MemReqSet, MemClear;
  
CONST
  ModuleBodyCodeOffset   = 4D; (* Code offset to first inst of module body *)


  MaxSourceFileLineWidth = 256;   (* Maximum length of source line *)
  DefHeapBlockSize       = 4096D; (* Symbol table heap allocation size *)
  MaxPosInfoCount        = 5461;  (* Maximum number of pos info records *)
  MaxLineInfoCount       = 8191;  (* Maximum number of lines in source file *)

  NewLineChar = CHAR(0AH); (* Source text line termination character *)

TYPE
  StructurePtr = POINTER TO Structure;
  ObjectPtr = POINTER TO Object;

  (* Object class variant record tags *)
  ObjectClass = (Header, Const, Typ, Var, Field, Proc, Code, Module);

  (* WARNING: When changing this structure update the AllocObject() proc *)
  Object = RECORD
             oName : ADDRESS;       (* object identifier string *)
             oType : StructurePtr;  (* object type structure *)
             oNext : ObjectPtr;     (* next object in list *)
             CASE oClass : ObjectClass OF  (* Class of object, variant tag *)
             | Header:     oLast        : ObjectPtr;  (* Last object in list *)
             | Const:      oValue       : CARDINAL;   (* Enumeration constant *)
             | Typ:
             | Var:        oVarAdr      : INTEGER;    (* Relative address *)
                           oVarPar      : BOOLEAN;    (* T: VAR par, F: value *)
                           oVarIndirect : BOOLEAN;    (* T: adr ptr, F: real adr*)
             | Field:      oOffset      : INTEGER;    (* field offsets *)
             | Proc:       oProcNo      : CARDINAL;   (* proc number *)
                           oProcAdr     : CARDINAL;   (* proc code adr *)
                           oProcSize    : CARDINAL;   (* proc size *)
                           oLevel       : CARDINAL;   (* proc scope level *)
                           oFirstLocal  : ObjectPtr;  (* first local object *)
             | Code:
             | Module:     oFirstObj    : ObjectPtr;  (* first local object *)
                           oModType     : ModuleType; (* module type *)
             END;
           END;

  (* WARNING: When changing this structure update the AllocStructure() proc *)
  Structure = RECORD
                sStrObj : ObjectPtr;   (* named object associated with type *)
                sSize   : CARDINAL;    (* structure size *)
                CASE sForm : StructureForm OF (* structure form variant tag *)
                | Undef, Bool, Char, Card, Int, Double,
                  LCard, Real, LongReal, String:
                | Enum:      sNofConst   : CARDINAL;     (* # of enumerations *)
                             sFirstConst : ObjectPtr;    (* first enum const *)
                | Range:     sRBaseTyp   : StructurePtr; (* range base type *)
                             sMin        : INTEGER;      (* min value of range*)
                             sMax        : INTEGER       (* max value of range*)
                | Pointer:   sPBaseTyp   : StructurePtr; (* ptr base type *)
                | Set:       sSBaseTyp   : StructurePtr; (* set base type *)
                | Array:     sElemTyp    : StructurePtr; (* element base type *)
                             sIndexTyp   : StructurePtr; (* index base type *)
                             sDyn        : BOOLEAN;      (* T: dynamic, F: static *)
                | Record:    sFirstFld   : ObjectPtr;    (* first field object *)
                | ProcTyp:
                | Opaque:
                | Prc:
                | Mod:
                END;
              END;

  (* Record relating the source position to a code offset *)
  PosInfo = RECORD
              piSourcePos  : LONGCARD;
              piCodeOffset : CARDINAL;
            END;

  (* A sequential array of source position information *)
  PosInfoArrayPtr = POINTER TO PosInfoArray;
  PosInfoArray = ARRAY [0..MaxPosInfoCount-1] OF PosInfo;

  (* An array of pointers into the text buffer for each source line *)
  SourceLineInfoArrayPtr = POINTER TO SourceLineInfoArray;
  SourceLineInfoArray = ARRAY [0..MaxLineInfoCount-1] OF ADDRESS;

  (* Module version control key *)
  ModuleKey = ARRAY [0..2] OF CARDINAL;

  (* Symbol Heap Node *)
  HeapNodePtr = POINTER TO HeapNode;
  HeapNode = RECORD
               hnNext : HeapNodePtr;
             END;

  (* Program module descriptor record *)
  ModuleInfoPtr = POINTER TO ModuleInfo;
  ModuleInfo = RECORD
                 miName         : ADDRESS;   (* ptr to module name string *)
                 miKey          : ModuleKey; (* module version control key *)
                 miDataStart    : LONGCARD;  (* adr of start of module data *)
                 miDataBase     : LONGCARD;  (* adr of middle of module data *)
                 miCodeBase     : LONGCARD;  (* adr of start of module code *)
                 miCodeEnd      : LONGCARD;  (* adr of end of module code *)
                 miDefFileName  : ADDRESS;   (* .DEF source file name or NIL *)
                 miModFileName  : ADDRESS;   (* .MOD source file name or NIL *)
                 miSbmFileName  : ADDRESS;   (* .SBM symbol file name or NIL *)
                 miRfmFileName  : ADDRESS;   (* .RFM symbol file name or NIL *)
                 miDefFileSize  : LONGINT;   (* .DEF source file size *)
                 miModFileSize  : LONGINT;   (* .MOD source file size *)
                 miSymbolTable  : ObjectPtr; (* module symbol table or NIL *)
                 miPosInfoCount : CARDINAL;  (* # of pos info records *)
                 miPosInfo      : PosInfoArrayPtr; (* ptr to array of records *)
                 miHeapList     : HeapNodePtr; (* heap list or NIL *)
                 miDefMaxWidth  : CARDINAL;    (* .DEF longest line width *)
                 miDefLineCount : CARDINAL;    (* .DEF line count *)
                 miDefLineInfo  : SourceLineInfoArrayPtr; (* ptr array of lines*)
                 miDefText      : ADDRESS;     (* .DEF text file buffer *)
                 miModMaxWidth  : CARDINAL;    (* .MOD longest line width *)
                 miModLineCount : CARDINAL;    (* .MOD line count *)
                 miModLineInfo  : SourceLineInfoArrayPtr; (* ptr array of lines*)
                 miModText      : ADDRESS;     (* .MOD text file buffer *)
                 miModLastUse   : CARDINAL;    (* .MOD last use value *)
               END;

  (* An array of module info descriptors *)
  ModuleInfoArrayPtr = POINTER TO ModuleInfoArray;
  ModuleInfoArray = ARRAY [0..255] OF ModuleInfo;

  (* ReadSymbolFile() return codes *)
  ReadSymbolFileErrors = (RSErrOk,
                          RSErrNoMemory,
                          RSErrNotOpenFile,
                          RSErrInvalidFile,
                          RSErrTooComplex,
                          RSErrBadKey);

  (* CreateModuleInfo() return codes *)
  CreateModuleInfoErrors = (MIErrOk,
                            MIErrNoMemory,
                            MIErrNotOpenFile,
                            MIErrNotExec,
                            MIErrNoHunkDebug,
                            MIErrInvalidFile,
                            MIErrNoIEEELib,
                            MIErrNoIEEEdpLib);

  (* LoadProgram() return codes *)
  LoadProgramErrors = (LPErrOk,
                       LPErrNoMemory,
                       LPErrNotExec,
                       LPErrNotFound,
                       LPErrUnknown);

  (* FindAvailableFiles() return codes *)
  FindAvailableFilesErrors = (FFErrOk,
                              FFErrNoMemory);

  (* ReadSourceFile() return codes *)
  GetSourceFileErrors = (GSErrOk,
                         GSErrNoMemory,
                         GSErrNotOpenFile,
                         GSErrReadingFile,
                         GSErrLineTooLong);

CONST
  (* Debugger data files expected extensions *)
  DEFFileExt = ".DEF";
  MODFileExt = ".MOD";
  SBMFileExt = ".SBM";
  RFMFileExt = ".RFM";

  REFFILE = 335B; (* Magic word at beginning of symbol file *)

  (* Symbol file block definitions *)
  CTL = -5000B; anchor = 0; ModTag = 1; ProcTag = 2; RefTag = 3; linkage = 4;
  STR = -6000B; enum = 0; range = 1; pointer = 2; set = 3; procTyp = 4;
        funcTyp = 5; array = 6; dynarr = 7; record = 8; opaque = 9;
  CMP = -7000B; parref = 0; par = 1; field = 2;
  OBJ = -10000B; varref = 0; var = 1; const = 2; string = 3; type = 4;
        proc = 5; func = 6; module = 7; svc = 8; svcfunc = 9;

  maxM =  256;  (* maximum number of modules / symbol file *)
  maxP =  256;  (* maximum number of procedures / symbol file *)
  minS =   32;  (* first non-standard structure *)
  maxS = 1024;  (* maximum number of structures / symbol file *)


VAR
  ProgFileName : ADDRESS; (* Filename of program executable file *)
  ProgSegList  : ADDRESS; (* Segment list for current program *)
  ProgUnLoad   : BOOLEAN; (* TRUE = UnLoad program, FALSE = do nothing *)

  ProgModuleCount : CARDINAL;           (* # of modules contained in program *)
  ProgModuleArray : ModuleInfoArrayPtr; (* ptr to array of module descriptors *)

  (* Standard types definitions *)
  undftyp     : StructurePtr;  (* undefined *)
  booltyp     : StructurePtr;  (* BOOLEAN *)
  chartyp     : StructurePtr;  (* CHAR *)
  inttyp      : StructurePtr;  (* INTEGER *)
  cardtyp     : StructurePtr;  (* CARDINAL *)
  dbltyp      : StructurePtr;  (* LONGINT *)
  realtyp     : StructurePtr;  (* REAL *)
  lrltyp      : StructurePtr;  (* LONGREAL *)
  bitstyp     : StructurePtr;  (* BITSET *)
  proctyp     : StructurePtr;  (* PROC *)
  stringtyp   : StructurePtr;  (* string *)
  addrtyp     : StructurePtr;  (* ADDRESS *)
  bytetyp     : StructurePtr;  (* BYTE *)
  wordtyp     : StructurePtr;  (* WORD *)
  lcardtyp    : StructurePtr;  (* LONGCARD *)
  longwordtyp : StructurePtr;  (* LONGWORD *)

  (* Objects used for user types with no identifiers *)
  enuObj : ObjectPtr; (* ENUMERATION *)
  ptrObj : ObjectPtr; (* POINTER *)
  setObj : ObjectPtr; (* SET *)
  arrObj : ObjectPtr; (* ARRAY *)
  recObj : ObjectPtr; (* RECORD *)
  ptyObj : ObjectPtr; (* PROCEDURE *)
  opaObj : ObjectPtr; (* OPAQUE *)

  ModFilesLoaded  : CARDINAL; (* # of .MOD files currently loaded *)
  ModLastUseValue : CARDINAL; (* .MOD Last use value *)

  StdHeapList : HeapNodePtr;  (* Standard definitions heap *)

  InitCodeBase : LONGCARD;    (* Init-code start adr *)
  InitCodeEnd  : LONGCARD;    (* Init-code end adr *)

  SymFile : FILE; (* Temporary variable setup when reading symbol file *)

  HeapList    : HeapNodePtr; (* Heap list for symbol file being read *)
  HeapBase    : HeapNodePtr; (* Ptr to current heap *)
  HeapPtr     : ADDRESS;     (* Current heap allocation ptr *)
  HeapLastPtr : ADDRESS;     (* Previous heap allocation ptr *)
  HeapEnd     : ADDRESS;     (* End of heap ptr *)
  HeapErr     : BOOLEAN;     (* Set to TRUE if a heap allocation fails *)

  TempPosInfoPtr   : PosInfoArrayPtr; (* ptr to array of pos-info records *)
  TempPosInfoCount : CARDINAL;        (* # of position info records read *)

(* ===== PRIVATE ====== *)

(* CreatePosInfo - Allocate space for reading pos-info records *)
PROCEDURE CreatePosInfo(): BOOLEAN;
BEGIN
  TempPosInfoCount := 0;
  TempPosInfoPtr   := AllocMemory(MaxPosInfoCount * SIZE(PosInfo), MemReqSet{});
  RETURN (TempPosInfoPtr # NIL);
END CreatePosInfo;

(* DeletePosInfo - free space allocated for pos-info records *)
PROCEDURE DeletePosInfo();
BEGIN
  IF (TempPosInfoPtr # NIL) THEN
    DeallocMemory(TempPosInfoPtr);
    TempPosInfoPtr := NIL;
  END;
END DeletePosInfo;

(* GetPosInfo - called after all records have been read to cleanup *)
PROCEDURE GetPosInfo(VAR posInfo: PosInfoArrayPtr; VAR posCount: CARDINAL);
BEGIN
  IF (TempPosInfoCount # 0) THEN
    ReduceMemory(TempPosInfoPtr, TempPosInfoCount * SIZE(PosInfo));
    posInfo  := TempPosInfoPtr;
    posCount := TempPosInfoCount;
  ELSE (* No pos-info records *)
    DeallocMemory(TempPosInfoPtr);
    posInfo  := NIL;
    posCount := 0;
  END;

  TempPosInfoPtr := NIL;
END GetPosInfo;

(* AddPosInfo - add a new pos-info record to list *)
PROCEDURE AddPosInfo(srcPos: LONGCARD; pc: CARDINAL);
BEGIN
  (* Discard PosInfo record located at same address as previous record *)
  IF (TempPosInfoCount # 0) THEN
    IF (TempPosInfoPtr^[TempPosInfoCount-1].piCodeOffset = pc) THEN
      DEC(TempPosInfoCount);
    END;
  END;

  (* If maximum number of records exceeded then simply discard *)
  IF (TempPosInfoCount < MaxPosInfoCount) THEN
    WITH TempPosInfoPtr^[TempPosInfoCount] DO
      piSourcePos  := srcPos;
      piCodeOffset := pc;
    END;
    INC(TempPosInfoCount);
  END;
END AddPosInfo;

(* AllocHeap - allocate memory for a new heap *)
PROCEDURE AllocHeap(): BOOLEAN;
BEGIN
  HeapBase := AllocMemory(DefHeapBlockSize, MemReqSet{});
  IF (HeapBase # NIL) THEN
    HeapBase^.hnNext := NIL;
    HeapPtr          := ADDRESS(HeapBase) + LONGCARD(SIZE(HeapNode));
    HeapLastPtr      := HeapPtr;
    HeapEnd          := ADDRESS(HeapBase) + DefHeapBlockSize;
  END;
  RETURN (HeapBase # NIL);
END AllocHeap;

(* DeallocHeapList - deallocate a list of heap nodes *)
PROCEDURE DeallocHeapList(heapList: HeapNodePtr);
VAR
  hp, hq: HeapNodePtr;
BEGIN
  hp := heapList;
  WHILE (hp # NIL) DO
    hq := hp;
    hp := hp^.hnNext;
    DeallocMemory(hq)
  END;
END DeallocHeapList;

(* LinkHeap - reduce current heap size to actual use size and link in *)
PROCEDURE LinkHeap();
BEGIN
  IF (HeapBase # NIL) THEN
    ReduceMemory(HeapBase, HeapPtr - ADDRESS(HeapBase));

    IF (HeapList # NIL) THEN
      HeapBase^.hnNext := HeapList;
    END;
    HeapList := HeapBase;
  END;
END LinkHeap;

(* CreateHeap - create a new default heap *)
PROCEDURE CreateHeap(): BOOLEAN;
VAR
  ok: BOOLEAN;
BEGIN
  HeapErr  := FALSE;
  HeapList := NIL;
  RETURN (AllocHeap());
END CreateHeap;

(* DeleteHeap - delete the current default heap after an error *)
PROCEDURE DeleteHeap();
BEGIN
  LinkHeap();

  IF (HeapList # NIL) THEN
    DeallocHeapList(HeapList);
    HeapList := NIL;
  END;
END DeleteHeap;

(* GetHeap - get current heap list and size, only use if HeapErr = FALSE *)
PROCEDURE GetHeapInfo(VAR list: HeapNodePtr; VAR size: LONGCARD);
VAR
  sizeCount: LONGCARD;
  hp: HeapNodePtr;
BEGIN
  LinkHeap();

  sizeCount := 0D;
  hp := HeapList;
  WHILE (hp # NIL) DO
    INC(sizeCount, GetMemorySize(hp));
    hp := hp^.hnNext;
  END;

  list := HeapList;
  size := sizeCount;
END GetHeapInfo;

(* ResetHeap - reset heap ptr to position before the last alloc *)
PROCEDURE ResetHeap();
BEGIN
  HeapPtr := HeapLastPtr;
END ResetHeap;

(* AllocFromHeap - allocate memory from heap *)
PROCEDURE AllocFromHeap(VAR ptr: ADDRESS; size: LONGCARD);
BEGIN
  IF (HeapErr) THEN (* previously encountered error, do nothing *)
    ptr := NIL;
  ELSE (* attempt allocation *)
    size := ((size + 1D) DIV 2D) * 2D; (* Make size an even number *)
    IF (HeapPtr + size >= HeapEnd) THEN (* Check if current heap full *)
      LinkHeap(); (* Link in current heap *)
      IF (NOT AllocHeap()) THEN (* Attempt to allocate new heap *)
        HeapErr := TRUE;
        ptr     := NIL;
        RETURN; (* Error *)
      END;
    END;
    HeapLastPtr := HeapPtr; (* Save current heap position *)
    ptr         := HeapPtr; (* Return ptr to allocated space *)
    INC(HeapPtr, size);     (* Adjust heap ptr past current allocation *)
  END;
END AllocFromHeap;

(* AllocObject - allocate from heap enough memory for a given class of object *)
PROCEDURE AllocObject(VAR obj: ObjectPtr; class: ObjectClass);
CONST
  FixedSIZE = 14; (* Size of static portion of record *)
VAR
  size: CARDINAL;
BEGIN
  CASE (class) OF
  | Header: size := FixedSIZE + 4;
  | Const:  size := FixedSIZE + 2;
  | Typ:    size := FixedSIZE + 0;
  | Var:    size := FixedSIZE + 4;
  | Field:  size := FixedSIZE + 2;
  | Proc:   size := FixedSIZE + 12;
  | Code:   size := FixedSIZE + 0;
  | Module: size := FixedSIZE + 6;
  END;
  AllocFromHeap(obj, size);
END AllocObject;

(* AllocStructure - allocate from heap memory for a given structure type*)
PROCEDURE AllocStructure(VAR str: StructurePtr; form: StructureForm);
CONST
  FixedSIZE = 8; (* Size of static portion of record *)
VAR
  size: CARDINAL;
BEGIN
  CASE (form) OF
  | Undef, Bool, Char, Card, Int, Double,
    LCard, Real, LongReal, String:
             size := FixedSIZE + 0;
  | Enum:    size := FixedSIZE + 6;
  | Range:   size := FixedSIZE + 8;
  | Pointer: size := FixedSIZE + 4;
  | Set:     size := FixedSIZE + 4;
  | Array:   size := FixedSIZE + 10;
  | Record:  size := FixedSIZE + 4;
  | ProcTyp: size := FixedSIZE + 0;
  | Opaque:  size := FixedSIZE + 0;
  | Prc:     size := FixedSIZE + 0;
  | Mod:     size := FixedSIZE + 0;
  END;
  AllocFromHeap(str, size);
END AllocStructure;

(* ConvBlockToObjectClass - convert a symbol file block -> object class *)
PROCEDURE ConvBlockToObjectClass(block: CARDINAL): ObjectClass;
VAR
  objClass: ObjectClass;
BEGIN
  CASE (block) OF
  | varref, var:  objClass := Var;
  | const:        objClass := Const;
  | string:       objClass := Const;
  | type:         objClass := Typ;
  | proc, func:   objClass := Proc;
  | module:       objClass := Module;
  | svc, svcfunc: objClass := Code;
  END;
  RETURN (objClass);
END ConvBlockToObjectClass;

(* ConvBlockToStructureForm - convert symbol file block -> structure form *)
PROCEDURE ConvBlockToStructureForm(block: CARDINAL): StructureForm;
VAR
  strForm: StructureForm;
BEGIN
  CASE (block) OF
  | enum:             strForm := Enum;
  | range:            strForm := Range;
  | pointer:          strForm := Pointer;
  | set:              strForm := Set;
  | procTyp, funcTyp: strForm := ProcTyp;
  | array:            strForm := Array;
  | dynarr:           strForm := Array;
  | record:           strForm := Record;
  | opaque:           strForm := Opaque;
  END;
  RETURN (strForm);
END ConvBlockToStructureForm;

(* getb - read one byte from a file *)
PROCEDURE getb(f: FILE): CHAR;
BEGIN
  RETURN CHAR(CStdIO.getc(f));
END getb;

(* getw - read one word from a file *)
PROCEDURE getw(f: FILE): CARDINAL;
BEGIN
  RETURN CStdIO.getw(f);
END getw;

(* getl - read one longword from a file *)
PROCEDURE getl(f: FILE): LONGCARD;
VAR
  conv : ConvToLong;
BEGIN
  conv.h := CStdIO.getw(f);
  conv.l := CStdIO.getw(f);
  RETURN (conv.hl);
END getl;

(* Sgetb - read one byte from current symbol file *)
PROCEDURE Sgetb(): CHAR;
BEGIN
  RETURN CHAR(CStdIO.getc(SymFile));
END Sgetb;

(* Sgetw - read one word from current symbol file *)
PROCEDURE Sgetw(): CARDINAL;
BEGIN
  RETURN CStdIO.getw(SymFile);
END Sgetw;

(* Sgetl - read one longword from current symbol file *)
PROCEDURE Sgetl(): LONGCARD;
VAR
  conv : ConvToLong;
BEGIN
  conv.h := CStdIO.getw(SymFile);
  conv.l := CStdIO.getw(SymFile);
  RETURN (conv.hl);
END Sgetl;

(* SskipB - advance file pointer by one byte on current symbol file *)
PROCEDURE Sskipb();
VAR
  skip: INTEGER;  
BEGIN
  skip := CStdIO.getc(SymFile);
END Sskipb;

(* Sskipw - advance file pointer by one word on current symbol file *)
PROCEDURE Sskipw();
VAR
  skip: CARDINAL;
BEGIN
  skip := CStdIO.getw(SymFile);
END Sskipw;

(* SskipId - advance file pointer over string on current symbol file *)
PROCEDURE SskipId();
VAR
  length: CARDINAL;
  ch: CHAR;
BEGIN
  length := CARDINAL(Sgetb()) - 1;
  WHILE (length > 0) DO
    ch := Sgetb();
    DEC(length);
  END;
END SskipId;

(* SgetId - read string from current symbol file and store on heap *)
PROCEDURE SgetId(VAR retId: ADDRESS);
VAR
  length: CARDINAL;
  id: ADDRESS;
BEGIN
  length := CARDINAL(Sgetb()) - 1;
  AllocFromHeap(id, length + 1);

  (* Store ptr to first char of identifier *)
  retId := id;

  IF (id # NIL) THEN
    WHILE (length > 0) DO
      id^ := Sgetb();
      INC(id);
      DEC(length);
    END;
    id^ := 0C;
  END;
END SgetId;

(* CalcSegEnd - given a ptr to a segment calculate its ending address *)
PROCEDURE CalcSegEnd(seg: ADDRESS): ADDRESS;
VAR
  sizePtr: POINTER TO LONGCARD;
BEGIN
  sizePtr := seg - 4D;                 (* calc ptr to segSize field *)
  RETURN ADDRESS(sizePtr) + sizePtr^;
END CalcSegEnd;

(*---------------------------------------------------------------*)
 PROCEDURE MakeFileName(outStr: ADDRESS; dir, file, ext: ADDRESS);
(*---------------------------------------------------------------*)
(* MakeFileName - copy dirname, filename and extension to output string *)

VAR  len  :INTEGER;
     ptr  :ADDRESS;

BEGIN

strcpy(outStr, dir);

(* Make sure path ends with a good separator *)
len := strlen(outStr);
IF (len > 0) THEN
   ptr := outStr + VAL(LONGCARD,len)-1D;
   IF (rindex(outStr, ':') # ptr) AND (rindex(outStr, '/') # ptr) THEN
      strcat(outStr, ADR("/"));
   END;
END;

strcat(outStr, file);
strcat(outStr, ext);

END MakeFileName;

(* CheckSymbolFileKey - read symbol file key and compare to module descriptor *)
PROCEDURE CheckSymbolFileKey(VAR mod: ModuleInfo; fname: ADDRESS): BOOLEAN;
VAR
  ok: BOOLEAN;
  b: BOOLEAN;
  f: FILE;
BEGIN
  ok := FALSE;
  f := fopen(fname, ADR("r"));
  IF (f # NIL) THEN
    (* Check for valid file *)
    IF (getw(f) = REFFILE) AND (getw(f) = CARDINAL(CTL+anchor)) THEN
      (* Check for valid key *)
      IF (getw(f) = mod.miKey[0]) AND 
         (getw(f) = mod.miKey[1]) AND
         (getw(f) = mod.miKey[2]) THEN
        ok := TRUE;
      END;
    END;
    b := fclose(f);
  END;
  RETURN (ok);
END CheckSymbolFileKey;

(* GetFileInfo - examine file info block to obtain date and fileSize *)
PROCEDURE GetFileInfo(lock: FileLock; VAR date: DateStampRecord; 
                      VAR fileSize: LONGINT): BOOLEAN;
VAR
  fib: FileInfoBlockPtr;
  ok: BOOLEAN;
BEGIN
  ok := FALSE;
  fib := AllocMemory(SIZE(FileInfoBlock), MemReqSet{});
  IF (fib # NIL) THEN
    IF Examine(lock, fib^) THEN
      date := fib^.fibDate;
      fileSize := fib^.fibSize;
      ok := TRUE;
    END;
    DeallocMemory(fib);
  END;
  RETURN (ok);
END GetFileInfo;

(* CompareDates - return TRUE if "old" date is older then "new" date *)
PROCEDURE CompareDates(VAR old, new: DateStampRecord): BOOLEAN;
BEGIN
  IF (old.dsDays > new.dsDays) THEN
    RETURN (FALSE);
  ELSIF (old.dsDays < new.dsDays) THEN
    RETURN (TRUE);
  END;

  IF (old.dsMinute > new.dsMinute) THEN
    RETURN (FALSE);
  ELSIF (old.dsMinute < new.dsMinute) THEN
    RETURN (TRUE);
  END;

  RETURN (old.dsTick < new.dsTick);
END CompareDates;

(* EnterType - enter type definition into symbol table *)
PROCEDURE EnterType(VAR str: StructurePtr; form: StructureForm; size: CARDINAL;
                    id: ADDRESS);
BEGIN
  AllocStructure(str, form);
  IF (str # NIL) THEN
    WITH str^ DO
      sForm          := form;
      sSize          := size;
      sPBaseTyp      := NIL;

      AllocObject(sStrObj, Typ);
      IF (sStrObj # NIL) THEN
        sStrObj^.oName := id;
      END;
    END;
  END;
END EnterType;

(* EnterObject - enter object definition into symbol table *)
PROCEDURE EnterObject(VAR obj: ObjectPtr; id: ADDRESS);
BEGIN
  AllocObject(obj, Typ);
  IF (obj # NIL) THEN
    obj^.oName := id;
  END;
END EnterObject;

(* ReadSymbolFile - read a symbol file for a specified module *)
PROCEDURE ReadSymbolFile(modNo: CARDINAL): ReadSymbolFileErrors;
VAR
  Struct: ARRAY [0..maxS] OF StructurePtr;
  LocMod: ARRAY [0..maxM] OF ObjectPtr;
  LocProc: ARRAY [0..maxP] OF ObjectPtr;
  block: INTEGER;
  CurStr: CARDINAL;
  CurMod: CARDINAL;
  newobj: ObjectPtr;
  obj: ObjectPtr;
  newstr: StructurePtr;
  objList: ObjectPtr;
  fldList: ObjectPtr;
  i: CARDINAL;
  s: CARDINAL;
  ch: CHAR;
  SbmFileName: ADDRESS;
  RfmFileName: ADDRESS;
  b: BOOLEAN;
  key0, key1, key2: CARDINAL;
  RootModuleName: ADDRESS;
  resetHeapFlag: BOOLEAN;
  err: ReadSymbolFileErrors;
  isRfmFile: BOOLEAN;
  heapSize: LONGCARD;
BEGIN
  SetSizeVerifyModeAllVTWindows(FALSE); (* Disable size verify *)

  WITH ProgModuleArray^[modNo] DO
    SbmFileName := miSbmFileName;
    RfmFileName := miRfmFileName;
  END;

  isRfmFile := RfmFileName # NIL;
  IF (isRfmFile) THEN
    SymFile := fopen(RfmFileName, ADR("r"));
  ELSE
    SymFile := fopen(SbmFileName, ADR("r"));
  END;
  err := RSErrNotOpenFile;
  IF (SymFile # NIL) THEN
    err := RSErrInvalidFile;
    IF (Sgetw() = REFFILE) THEN
      LOOP
        Struct[ 1] := undftyp; 
        Struct[ 2] := booltyp; 
        Struct[ 3] := chartyp;
        Struct[ 4] := inttyp;  
        Struct[ 5] := cardtyp; 
        Struct[ 6] := dbltyp;
        Struct[ 7] := realtyp; 
        Struct[ 8] := lrltyp;  
        Struct[ 9] := bitstyp;
        Struct[10] := proctyp; 
        Struct[11] := stringtyp;
        Struct[12] := addrtyp; 
        Struct[13] := bytetyp; 
        Struct[14] := wordtyp;
        Struct[15] := lcardtyp; 
        Struct[16] := longwordtyp;
        CurStr := minS;
        CurMod := 0;

        IF (isRfmFile) THEN
          IF (NOT CreatePosInfo()) THEN
            err := RSErrNoMemory;
            EXIT;
          END;
        END;
        IF (NOT CreateHeap()) THEN
          err := RSErrNoMemory;
          EXIT;
        END;

        AllocObject(objList, Header);
        AllocObject(fldList, Header);
        IF (objList = NIL) OR (fldList = NIL) THEN
          err := RSErrNoMemory;
          EXIT; (* error *)
        END;
        WITH objList^ DO
          oClass := Header;
          oNext  := NIL;
          oLast  := objList;
        END;
        WITH fldList^ DO
          oClass := Header;
          oNext  := NIL;
          oLast  := fldList;
        END;

        err := RSErrOk;

        LOOP (* Read File *)
          block := Sgetw();

          IF (ferror(SymFile)) THEN
            err := RSErrInvalidFile;
            (* error, exit at top of loop *)
          END;

          (* Exit if an Invalid File Error *)
          IF (err = RSErrInvalidFile) THEN EXIT; END;

          IF (block < CMP) THEN
            DEC(block, OBJ);
            IF (block <= svcfunc) THEN (* Valid *)
              AllocObject(newobj, ConvBlockToObjectClass(block));
              IF (newobj = NIL) THEN
                err := RSErrNoMemory;
                EXIT; (* error *)
              END;

              resetHeapFlag := FALSE;

              WITH newobj^ DO
                CASE (block) OF
                | varref, var:  oClass       := Var;
                                oType        := Struct[Sgetw()];
                                oVarPar      := (block = varref);
                                oVarIndirect := oVarPar;
                                IF (oType^.sForm = Array) AND (oType^.sDyn) THEN
                                  oVarIndirect := TRUE;
                                END;
                                Sskipw(); (* level *)
                                oVarAdr := Sgetw();
                | const:        oClass := Const;
                                oType := Struct[Sgetw()];
                                Sskipw(); (* ModRef *)
                                i := CARDINAL(Sgetb()); (* length *)
                                oValue := CARDINAL(Sgetb()); (* enum value *)
                                WHILE (i > 2) DO (* skip over rest of const *)
                                  Sskipb();
                                  DEC(i);
                                END;
                                IF (oType^.sForm = Enum) THEN
                                   IF (oType^.sFirstConst = NIL) THEN
                                     oType^.sFirstConst := newobj;
                                   END;
                                ELSE
                                  ResetHeap(); (* Discard this object *)
                                  resetHeapFlag := TRUE;
                                END;
                | string:       oClass := Const;
                                oType := Struct[Sgetw()];
                                SskipId(); (* string *)
                                ResetHeap(); (* Discard this object *)
                                resetHeapFlag := TRUE;
                | type:         oClass := Typ;                               
                                s := Sgetw();
                                oType := Struct[s];
                                (* Check if user-defined type! *)
                                IF (s >= minS) THEN
                                  oType^.sStrObj := newobj;
                                END;
                                Sskipw(); (* ModRef *)
                | proc, func:   oClass := Proc;
                                IF (block = func) THEN
                                  oType := Struct[Sgetw()];
                                ELSE (* block = proc *)
                                  oType := NIL;
                                END;
                                oProcNo     := Sgetw();
                                oLevel      := Sgetw();
                                oProcAdr    := Sgetw();
                                oProcSize   := Sgetw();
                                oFirstLocal := LocProc[oProcNo];
                | module:       oClass    := Module;
                                oFirstObj := LocMod[Sgetw()];
                                oModType  := MTLocal;
                | svc, svcfunc: oClass := Code;
                                IF (block = svcfunc) THEN
                                  Sskipw(); (* return type *)
                                END;
                                Sskipw(); (* cnum *)
                                ResetHeap(); (* Discard this object *)
                                resetHeapFlag := TRUE;
                END; (* CASE *)
                IF (resetHeapFlag) THEN (* Current object discarded *)
                  SskipId();
                ELSE (* Current object allocated *)
                  SgetId(oName);
                  IF (oName = NIL) THEN
                    err := RSErrNoMemory;
                    EXIT; (* error *)
                  END;
                  oNext := NIL;
                  WITH objList^ DO
                    oLast^.oNext := newobj;
                    oLast        := newobj;
                  END;
                END;
              END; (* WITH *)
            ELSE (* Invalid *)
              err := RSErrInvalidFile;
              (* error, exit at top of loop *)
            END; (* block <= svcfunc *)

          ELSIF (block < STR) THEN
            DEC(block, CMP);
            IF (block <= field) THEN (* Valid *)
              IF (block = field) THEN
                AllocObject(newobj, Field);
                IF (newobj = NIL) THEN
                  err := RSErrNoMemory;
                  EXIT; (* error *)
                END;
                WITH newobj^ DO
                  oClass  := Field;
                  oNext   := NIL;
                  oType   := Struct[Sgetw()];
                  oOffset := Sgetw();
                  SgetId(oName);
                  IF (oName = NIL) THEN
                    err := RSErrNoMemory;
                    EXIT; (* error *)
                  END;
                  fldList^.oLast^.oNext := newobj;
                  fldList^.oLast        := newobj;
                END; (* WITH *)
              ELSE (* block = parref OR par *)
                Sskipw(); (* struct *)
              END;
            ELSE (* Invalid *)
              err := RSErrInvalidFile;
              (* error, exit at top of loop *)
            END; (* block <= field *)

          ELSIF (block < CTL) THEN
            DEC(block, STR);
            IF (block <= opaque) THEN (* Valid *)
              AllocStructure(newstr, ConvBlockToStructureForm(block));

              IF (newstr = NIL) THEN
                err := RSErrNoMemory;
                EXIT; (* error *)
              END;
              WITH newstr^ DO
                sStrObj := NIL;
                sSize   := Sgetw();
                CASE (block) OF
                | enum:             sForm       := Enum;
                                    sNofConst   := Sgetw();
                                    sFirstConst := NIL;
                                    sStrObj     := enuObj;
                | range:            sForm     := Range;
                                    sRBaseTyp := Struct[Sgetw()];
                                    sMin      := Sgetw();
                                    sMax      := Sgetw();
                                    sStrObj   := sRBaseTyp^.sStrObj;
                | pointer:          sForm     := Pointer;
                                    sPBaseTyp := NIL;
                                    sStrObj   := ptrObj;
                | set:              sForm     := Set;
                                    sSBaseTyp := Struct[Sgetw()];
                                    sStrObj   := setObj;
                | procTyp, funcTyp: sForm := ProcTyp;
                                    IF (block = funcTyp) THEN
                                      Sskipw();
                                    END;
                                    sStrObj   := ptyObj;
                | array:            sForm     := Array;
                                    sDyn      := FALSE;
                                    sElemTyp  := Struct[Sgetw()];
                                    sIndexTyp := Struct[Sgetw()];
                                    sStrObj   := arrObj;
                | dynarr:           sForm     := Array;
                                    sDyn      := TRUE;
                                    sElemTyp  := Struct[Sgetw()];
                                    sIndexTyp := NIL;
                                    sStrObj        := arrObj;
                | record:           sForm          := Record;
                                    sFirstFld      := fldList^.oNext;
                                    fldList^.oNext := NIL;
                                    fldList^.oLast := fldList;
                                    sStrObj        := recObj;
                | opaque:           sForm   := Opaque;
                                    sStrObj := opaObj;
                END; (* CASE *)
              END; (* WITH *)
              IF (CurStr > maxS) THEN
                err := RSErrTooComplex;
                EXIT; (* error *)
              END;
              Struct[CurStr] := newstr;
              INC(CurStr);
            ELSE (* Invalid *)
              err := RSErrInvalidFile;
              (* error, exit at top of loop *)
            END; (* block <= opaque *)

          ELSIF (block < 0) THEN
            DEC(block, CTL);
            IF (block <= linkage) THEN (* Valid *)
              CASE (block) OF
              | anchor: key0 := Sgetw(); (* key1 *)
                        key1 := Sgetw(); (* key2 *)
                        key2 := Sgetw(); (* key3 *)
                        IF (CurMod = 0) THEN (* Root Module *)
                          WITH ProgModuleArray^[modNo] DO
                            IF (miKey[0] # key0) OR
                               (miKey[1] # key1) OR
                               (miKey[2] # key2) THEN
                               err := RSErrBadKey;
                               EXIT; (* error *)
                             END; (* IF *)
                           END; (* WITH *)
                           SgetId(RootModuleName);
                           IF (RootModuleName = NIL) THEN
                             err := RSErrNoMemory;
                             EXIT; (* error *)
                           END;
                         ELSE (* Imported Module *)
                           SskipId(); (* name *)
                         END; (* IF *)
                         IF (CurMod > maxM) THEN
                           err := RSErrTooComplex;
                           EXIT; (* error *)
                         END;
                         INC(CurMod);
              | ModTag:  LocMod[Sgetw()] := objList^.oNext;
                         objList^.oNext  := NIL;
                         objList^.oLast  := objList;
              | ProcTag: LocProc[Sgetw()] := objList^.oNext;
                         objList^.oNext   := NIL;
                         objList^.oLast   := objList;
              | RefTag:  Sskipw(); (* adr *)
                         Sskipw(); (* pno *)

                         AllocObject(newobj, Module);
                         IF (newobj = NIL) THEN
                           err := RSErrNoMemory;
                           EXIT; (* error *)
                         END;

                         WITH newobj^ DO
                           oName     := RootModuleName;
                           oType     := NIL;
                           oNext     := NIL;
                           oClass    := Module;
                           oFirstObj := LocMod[0];
                           IF (isRfmFile) THEN
                             oModType := MTMod;
                           ELSE
                             oModType := MTDef;
                           END;
                         END;

                         ProgModuleArray^[modNo].miSymbolTable := newobj;

                         err := RSErrOk;
                         EXIT; (* Success! *)
              | linkage: s := Sgetw();
                         Struct[Sgetw()]^.sPBaseTyp := Struct[s];
              END;
            ELSE (* Invalid *)
              err := RSErrInvalidFile;
              (* error, exit at top of loop *)
            END; (* block <= linkage *)
          ELSE (* line block *)
            IF (isRfmFile) THEN (* Only .RFM files can have line-blocks *)
              AddPosInfo(Sgetl(), block);
            ELSE
              err := RSErrInvalidFile;
              (* error, exit at top of loop *)
            END;
          END; (* IF *)
        END; (* LOOP Read File *)

        EXIT; (* Done *)
      END; (* LOOP (for exiting on error) *)

      IF (err = RSErrOk) THEN
        WITH ProgModuleArray^[modNo] DO
          GetHeapInfo(miHeapList, heapSize);
          IF (isRfmFile) THEN
            GetPosInfo(miPosInfo, miPosInfoCount);
          END;
        END;
      ELSE
        DeleteHeap(); (* maybe called even if CreateHeap() failed *)
        IF (isRfmFile) THEN
          DeletePosInfo(); (* maybe called even if CreatePosInfo() failed *)
        END;
      END;

    END; (* getw() = REFFILE *)
    b := fclose(SymFile);
  END; (* SymFile # NIL *)

  SetSizeVerifyModeAllVTWindows(TRUE); (* Enable size verify *)

  RETURN (err);
END ReadSymbolFile;

(* PurgeModFile - Free .MOD source file if maximum # of allowed exceeded *)
PROCEDURE PurgeModFile();
VAR
  mod: CARDINAL;
  smallUse: CARDINAL;
  smallMod: CARDINAL;
BEGIN
  IF (ConfigMaxModFilesLoaded # 0) AND
     (ModFilesLoaded >= ConfigMaxModFilesLoaded) THEN

    (* Find the mod file with the smallest use value *)
    mod := 0;
    smallMod := 0;
    smallUse := 65535;
    FOR mod := 0 TO ProgModuleCount - 1 DO
      WITH ProgModuleArray^[mod] DO
        IF (miModText # NIL) AND (miModLastUse < smallUse) THEN
          smallMod := mod;
          smallUse := miModLastUse;
        END;
      END; (* WITH *)
    END; (* FOR *)

    (* Purge Mod File *)
    FreeSourceFileMod(smallMod);
  END;
END PurgeModFile;

(* ReadSourceFile - read a source file and create ptrs to each source line *)
PROCEDURE ReadSourceFile(VAR text: ADDRESS; VAR line: SourceLineInfoArrayPtr;
                         VAR count: CARDINAL;
                         VAR width: CARDINAL;
                         fileName: ADDRESS; 
                         fileSize: LONGINT): GetSourceFileErrors;
VAR
  err: GetSourceFileErrors;
  textBuf: ADDRESS;
  lineArray: SourceLineInfoArrayPtr;
  lineCount: CARDINAL;
  srcFile: FileHandle;
  curWidth: CARDINAL;
  maxWidth: CARDINAL;
  
  n: LONGINT;
  cp: ADDRESS;
BEGIN
  SetSizeVerifyModeAllVTWindows(FALSE); (* Disable size verify *)

  textBuf   := NIL;
  lineArray := NIL;
  srcFile   := NIL;
  LOOP
    textBuf := AllocMemory(fileSize + 1D, MemReqSet{});
    IF (textBuf = NIL) THEN
      err := GSErrNoMemory;
      EXIT; (* error *)
    END;

    lineArray := AllocMemory(MaxLineInfoCount * SIZE(LONGCARD), MemReqSet{});
    IF (lineArray = NIL) THEN
      err := GSErrNoMemory;
      EXIT; (* error *)
    END;

    srcFile := Open(fileName, ModeOldFile);
    IF (srcFile = NIL) THEN
      err := GSErrNotOpenFile;
      EXIT; (* error *)
    END;

    IF (Read(srcFile, textBuf, fileSize) # fileSize) THEN
      err := GSErrReadingFile;
      EXIT; (* error *)
    END;

    curWidth  := 0;
    maxWidth  := 0;
    lineCount := 0;
    n         := fileSize;
    cp        := textBuf;

    (* Init ptr to first line *)
    lineArray^[lineCount] := cp;
    INC(lineCount);

    WHILE (n # 0D) DO
      IF (CHAR(cp^) = NewLineChar) THEN (* New line char found *)

        (* Change new line char to null-terminator *)
        cp^ := 0C;

        (* Check for overflow of line array *)
        IF (lineCount < MaxLineInfoCount) THEN
          (* Init ptr to next line *)
          lineArray^[lineCount] := cp + 1D;
          INC(lineCount);
        END;
        (* Check if current width exceeds previous max width *)
        IF (curWidth > maxWidth) THEN
          maxWidth := curWidth;
          IF (maxWidth > MaxSourceFileLineWidth) THEN
            err := GSErrLineTooLong;
            EXIT; (* error *)
          END;
        END;
        curWidth := 0;
      ELSE
        INC(curWidth);
      END;

      INC(cp);
      DEC(n);
    END;

    (* Put null-terminator after last line since it may not have one *)
    cp^ := 0C;

    (* Avoid adding a blank line on files which end on a new-line char *)
    IF (lineArray^[lineCount-1] = cp) THEN
      DEC(lineCount);
    END;

    (* Reduce size of line array to reflect actual use *)
    ReduceMemory(lineArray, lineCount * SIZE(LONGCARD));

    text  := textBuf;
    line  := lineArray;
    count := lineCount;
    width := maxWidth;

    err := GSErrOk;
    EXIT; (* Success! *)
  END; (* LOOP *)

  IF (srcFile # NIL) THEN
    Close(srcFile);
  END;

  IF (err # GSErrOk) THEN (* Error cleanup *)
    IF (textBuf # NIL) THEN
      DeallocMemory(textBuf);
    END;
    IF (lineArray # NIL) THEN
      DeallocMemory(lineArray);
    END;
  END;

  SetSizeVerifyModeAllVTWindows(TRUE); (* Enable size verify *)

  RETURN (err);
END ReadSourceFile;

(* FindSourceString - search source file for a search string *)
PROCEDURE FindSourceString(lineInfo: SourceLineInfoArrayPtr; lineCount: CARDINAL;
                           findStr: ADDRESS; VAR lineNo,startX,stopX: CARDINAL);
TYPE
  CharPtr = POINTER TO CHAR;
VAR
  curLineNo: CARDINAL;
  text: CharPtr;
  textStart: ADDRESS;
  firstFound: ADDRESS;
  curFindStr: CharPtr;
  chText: CHAR;
  chFind: CHAR;
BEGIN
  curLineNo := lineNo;

  IF (findStr = NIL) OR (CHAR(findStr^) = 0C) THEN
    curLineNo := InvalidLineNo;
  END;

  IF (curLineNo # InvalidLineNo) THEN
    textStart  := lineInfo^[curLineNo];
    text       := ADDRESS(textStart + LONGCARD(startX));
    curFindStr := findStr;
    LOOP
      chText := text^;
      chFind := curFindStr^;

      IF (NOT ConfigStringSearchCase) THEN (* Not Case Sensitive *)
        chText := CAP(chText);
        chFind := CAP(chFind);
      END;

      IF (curFindStr = findStr) THEN (* Check for first char *)
        IF (chFind = chText) THEN (* Match *)
          firstFound := text;
          INC(ADDRESS(curFindStr));
        END;
      ELSE (* Check for subsequent char *)
        IF (chFind = chText) THEN (* Match *)
          INC(ADDRESS(curFindStr));
        ELSE (* no-Match *)
          curFindStr := findStr; (* Reset to start of search string *)
          IF (text # textStart) THEN
            DEC(ADDRESS(text)); (* Resume search with current char *)
          END;
        END;
      END;

      IF (curFindStr^ = 0C) THEN
        startX := CARDINAL(firstFound - textStart);
        stopX  := CARDINAL(ADDRESS(text) - textStart);
        EXIT; (* Success! *)
      END;

      IF (text^ = 0C) THEN (* End of line *)
        INC(curLineNo);
        IF (curLineNo = lineCount) THEN
          curLineNo := InvalidLineNo;
          EXIT; (* Failed! *)
        END;
        textStart  := lineInfo^[curLineNo];
        text       := textStart;
        curFindStr := findStr;
      ELSE (* Advance to next char *)
        INC(ADDRESS(text));
      END;
    END; (* LOOP *)
  END;

  lineNo := curLineNo;
END FindSourceString;

(* ShowLoadProgramError - display LoadProgram() error message *)
PROCEDURE ShowLoadProgramError(err: LoadProgramErrors);
VAR
  str: ADDRESS;
BEGIN
  CASE (err) OF
  | LPErrOk:       str := NIL;
  | LPErrNoMemory: str := ShowErrMsgNoMemory;
  | LPErrNotExec:  str := ADR("File is not an executable program.");
  | LPErrNotFound: str := ADR("Executable file not found.");
  | LPErrUnknown:  str := ADR("Unknown problem loading executable file.");
  END;

  IF (str # NIL) THEN
    ShowError(str);
  END;
END ShowLoadProgramError;

(* ShowCreateModuleInfoError - display CreateModuleInfo() error message *)
PROCEDURE ShowCreateModuleInfoError(err: CreateModuleInfoErrors);
VAR
  str: ADDRESS;
BEGIN
  CASE (err) OF
  | MIErrOk:          str := NIL;
  | MIErrNoMemory:    str := ShowErrMsgNoMemory;
  | MIErrNotOpenFile: str := ADR("Failed to open executable file.");
  | MIErrNotExec:     str := ADR("File is not an executable program.");
  | MIErrNoHunkDebug: str := ADR("Debugger information not found in executable file.");
  | MIErrInvalidFile: str := ADR("Invalid executable file.");
  | MIErrNoIEEELib:   str := ADR("IEEE single lib not opened.");
  | MIErrNoIEEEdpLib: str := ADR("IEEE double lib(s) not opened.");
  END;

  IF (str # NIL) THEN
    ShowError(str);
  END;
END ShowCreateModuleInfoError;

(* ShowFindAvailableFilesError - display FindAvailableFiles() error message *)
PROCEDURE ShowFindAvailableFilesError(err: FindAvailableFilesErrors);
VAR
  str: ADDRESS;
BEGIN
  CASE (err) OF
  | FFErrOk:          str := NIL;
  | FFErrNoMemory:    str := ShowErrMsgNoMemory;
  END;

  IF (str # NIL) THEN
    ShowError(str);
  END;
END ShowFindAvailableFilesError;

(* ShowGetSourceFileError - display ReadSourceFile() error message *)
PROCEDURE ShowGetSourceFileError(err: GetSourceFileErrors);
VAR
  str: ADDRESS;
BEGIN
  CASE (err) OF
  | GSErrOk:           str := NIL;
  | GSErrNoMemory:     str := ShowErrMsgNoMemory;
  | GSErrNotOpenFile:  str := ADR("Opening source file.");
  | GSErrReadingFile:  str := ADR("Reading source file.");
  | GSErrLineTooLong:  str := ADR("Source file line too long.");
  END;

  IF (str # NIL) THEN
    ShowError(str);
  END;
END ShowGetSourceFileError;

(* ShowReadSymbolFileError - display ReadSymbolFile() error message *)
PROCEDURE ShowReadSymbolFileError(err: ReadSymbolFileErrors);
VAR
  str: ADDRESS;
BEGIN
  CASE (err) OF
  | RSErrOk:           str := NIL;
  | RSErrNoMemory:     str := ShowErrMsgNoMemory;
  | RSErrNotOpenFile:  str := ADR("Opening symbol file.");
  | RSErrInvalidFile:  str := ADR("Invalid symbol file.");
  | RSErrTooComplex:   str := ADR("Symbol file is too complex.");
  | RSErrBadKey:       str := ADR("Symbol file has a bad key.");
  END;

  IF (str # NIL) THEN
    ShowError(str);
  END;
END ShowReadSymbolFileError;

(* FindModNoModOfs - find module number and offset from an address *)
PROCEDURE FindModNoModOfs(adr: ADDRESS; VAR modNo: CARDINAL; 
                          VAR modOfs: LONGCARD);
VAR
  mod: CARDINAL;
BEGIN
  FOR mod := 0 TO ProgModuleCount - 1 DO
    WITH ProgModuleArray^[mod] DO
      IF (miCodeBase + ModuleBodyCodeOffset <= adr) AND (miCodeEnd > adr) THEN
        modNo  := mod;
        modOfs := adr - miCodeBase;
        RETURN; (* Success! *)
      END;
    END;
  END;
  modNo := InvalidModNo;
END FindModNoModOfs;

(* GetAnySonItem - get object linked to specified object or empty. *)
PROCEDURE GetAnySonItem(VAR item: Item; altItem: BOOLEAN);
VAR
  skipStr: BOOLEAN;
BEGIN
  skipStr := FALSE;
  WITH item DO
    IF (iObj # NIL) THEN
      CASE (iObj^.oClass) OF
      | Var, Field: iStr := iObj^.oType;
                    iObj := NIL;
      | Proc:       IF (altItem) THEN
                      skipStr := TRUE;
                      iStr := iObj^.oType;
                      iObj := NIL;
                    ELSE
                      iObj := iObj^.oFirstLocal;
                      iStr := NIL;
                    END;
      | Module:     iObj := iObj^.oFirstObj;
                    iStr := NIL;
      ELSE
        iObj := NIL;
        iStr := NIL;
      END; (* CASE *)
      WHILE (iObj # NIL) AND (iObj^.oClass # Var) AND
            (iObj^.oClass # Field) AND (iObj^.oClass # Module) DO
        iObj := iObj^.oNext;
      END;
    END; (* IF *)
    IF (NOT skipStr) AND (iStr # NIL) THEN
      CASE (iStr^.sForm) OF
      | Enum:    iObj := iStr^.sFirstConst;
                 iStr := NIL;
      | Range:   iStr := iStr^.sRBaseTyp;
                 iObj := NIL;
      | Pointer: iStr := iStr^.sPBaseTyp;
                 iObj := NIL;
      | Set:     iStr := iStr^.sSBaseTyp;
                 iObj := NIL;
      | Array:   IF (altItem) THEN
                   iStr := iStr^.sIndexTyp;
                 ELSE
                   iStr := iStr^.sElemTyp;
                 END;
                 iObj := NIL;
      | Record:  iObj := iStr^.sFirstFld;
                 iStr := NIL;
      ELSE
        iObj := NIL;
        iStr := NIL;
      END; (* CASE *)
    END; (* IF *)
  END; (* WITH *)
END GetAnySonItem;

(* GetSourcePosFromStatementNo - lookup src-pos by indexing with statement no *)
PROCEDURE GetSourcePosFromStatementNo(modNo: CARDINAL; statNo: CARDINAL): LONGCARD;
VAR
  res: LONGCARD;
BEGIN
  res := InvalidSrcPos;
  WITH ProgModuleArray^[modNo] DO
    IF (miPosInfo # NIL) THEN
      IF (statNo < miPosInfoCount) THEN
        res := miPosInfo^[statNo].piSourcePos;
      END;
    END;
  END;
  RETURN (res);
END GetSourcePosFromStatementNo;

(* GetLineNoFromSourcePos - Find line # from source position *)
PROCEDURE GetLineNoFromSourcePos(modNo: CARDINAL; srcPos: LONGCARD;
                                 VAR lineNo: CARDINAL);
VAR
  line: CARDINAL;
  low: CARDINAL;
  mid: CARDINAL;
  high: CARDINAL;
  curPos: LONGCARD;
BEGIN
  lineNo := InvalidLineNo;
  WITH ProgModuleArray^[modNo] DO
    INC(srcPos, miModText);

    IF (miModLineInfo # NIL) THEN
      low  := 0;
      high := miModLineCount;
      REPEAT
        mid    := (low + high) DIV 2;
        curPos := miModLineInfo^[mid];
        IF (curPos = srcPos) OR
           ((curPos < srcPos) AND
            ((mid = miModLineCount-1) OR 
             (miModLineInfo^[mid+1] > srcPos))) THEN
          lineNo := mid;
          RETURN; (* Found! *)
        END;

        IF (srcPos < curPos) THEN
          high := mid;
        ELSE
          low := mid + 1;
        END;
      UNTIL (low >= high);
    END;
  END;

(******************* Linear Search Version ****************************
      FOR line := 0 TO miModLineCount - 1 DO
        IF (miModLineInfo^[line] <= srcPos) THEN
          lineNo := line;
        END;
      END;
***********************************************************************)
END GetLineNoFromSourcePos;

(* ===== PUBLIC ===== *)

(* SetProgramInfo - Setup externally loaded program info. (PMD Mode) *)
PROCEDURE SetProgramInfo(progName: ADDRESS; progSegList: ADDRESS);
BEGIN
  ProgFileName := progName;
  ProgSegList  := progSegList;
  ProgUnLoad   := FALSE;
END SetProgramInfo;

(* LoadProgram - load an executable file from disk *)
PROCEDURE LoadProgram(progName: ADDRESS): BOOLEAN;
VAR
  err: LoadProgramErrors;
  dosErr: LONGINT;
BEGIN
  SetSizeVerifyModeAllVTWindows(FALSE); (* Disable size verify *)

  ProgFileName := progName;

  ProgSegList := LoadSeg(ProgFileName);
  IF (ProgSegList # NIL) THEN
    ProgUnLoad := TRUE;
    err := LPErrOk;
  ELSE (* Program not loaded *)
    dosErr := IoErr();
    IF (dosErr = ErrorNoFreeStore) THEN
      err := LPErrNoMemory;
    ELSIF (dosErr = ErrorFileNotObject) OR (dosErr = ErrorObjectWrongType) THEN
      err := LPErrNotExec;
    ELSIF (dosErr = ErrorObjectNotFound) THEN
      err := LPErrNotFound;
    ELSE
      err := LPErrUnknown;
    END;
  END;

  ShowLoadProgramError(err);

  SetSizeVerifyModeAllVTWindows(TRUE); (* Enable size verify *)

  RETURN (err = LPErrOk);
END LoadProgram;

(* UnLoadProgram - unload executable program if it was loaded by the debuger *)
PROCEDURE UnLoadProgram();
BEGIN
  IF (ProgUnLoad) THEN
    IF (ProgSegList # NIL) THEN
      UnLoadSeg(ProgSegList);
    END;
    ProgUnLoad  := FALSE;
    ProgSegList := NIL;
  END;
END UnLoadProgram;

(* GetProgramSegList - get program segment list *)
PROCEDURE GetProgramSegList(): ADDRESS;
BEGIN
  RETURN (ProgSegList);
END GetProgramSegList;

(* GetModuleCount - get number of modules in program *)
PROCEDURE GetModuleCount(): CARDINAL;
BEGIN
  RETURN (ProgModuleCount);
END GetModuleCount;

(* GetModuleName - get module name string from module number *)
PROCEDURE GetModuleName(modNo: CARDINAL): ADDRESS;
BEGIN
  RETURN (ProgModuleArray^[modNo].miName);
END GetModuleName;

(* CreateModuleInfoArray - read debug info from executable file *)
PROCEDURE CreateModuleInfoArray(): BOOLEAN;
CONST
  HUNKHeader  = LONGCARD(03F3H);
  HUNKCode    = LONGCARD(03E9H);
  HUNKDebug   = LONGCARD(03F1H);

TYPE
  SegList = POINTER TO ADDRESS;

VAR
  f: FILE;
  err: CreateModuleInfoErrors;
  seg: SegList;
  mod: CARDINAL;
  i: INTEGER;
  l: LONGCARD;
  c: CHAR;
  b: BOOLEAN;
  hunkTableSize: CARDINAL;
  str: ARRAY [0..127] OF CHAR;
BEGIN
  SetSizeVerifyModeAllVTWindows(FALSE); (* Disable size verify *)

  seg := BPTRtoAPTR(ProgSegList);

  InitCodeBase := LONGCARD(seg) + 4D;
  InitCodeEnd  := CalcSegEnd(seg);

  seg := BPTRtoAPTR(seg^);

  err := MIErrNotOpenFile;
  f := fopen(ProgFileName, ADR("r"));  
  IF (f # NIL) THEN
    (* Calc ptr to second segment (skipping over module jump table) *)

    LOOP
      err := MIErrNotExec;
      IF (getl(f) # HUNKHeader) THEN
        EXIT; (* error *)
      END;

      l := getl(f);                        (* end of resident libs list *)
      hunkTableSize := getl(f);            (* hunk table size *)
      l := getl(f);                        (* first hunk *)
      l := getl(f);                        (* last hunk *)
      b := fseek(f, hunkTableSize * 4, 1); (* skip over hunk table *)
  
      err := MIErrInvalidFile;
      IF (getl(f) # HUNKCode) THEN
        EXIT; (* error *)
      END;

      (* Get HUNKCode size and skip over body *)
      b := fseek(f, getl(f) * 4D, 1);

      err := MIErrNoHunkDebug;
      IF (getl(f) # HUNKDebug) THEN
        EXIT; (* error *)
      END;

      l := getl(f); (* Debug Block Size *)

      (* Read number of modules in program *)
      ProgModuleCount := getw(f);
      ProgModuleArray := AllocMemory(ProgModuleCount * SIZE(ModuleInfo),
                                     MemReqSet{MemClear});
      err := MIErrNoMemory;
      IF (ProgModuleArray = NIL) THEN
        EXIT; (* error *)
      END;

      (*System.*)REALflavor := 0;  (* FFP *)

      FOR mod := 0 TO ProgModuleCount - 1 DO
        WITH ProgModuleArray^[mod] DO
          miDataStart := LONGCARD(seg) + 4D;
          miDataBase  := getl(f) + LONGCARD(seg) + 4D;
          miCodeBase  := getl(f) + LONGCARD(seg) + 4D;
          miCodeEnd   := CalcSegEnd(seg);
          miKey[0]    := getw(f);
          miKey[1]    := getw(f);
          miKey[2]    := getw(f);
          
          (* Read Module Name *)
          i := -1;
          REPEAT
            INC(i);
            str[i] := getb(f);
          UNTIL (str[i] = 0C);

          IF (NOT ODD(i)) THEN (* Skip over pad byte *)
            c := getb(f);
          END;

          (* REALflavor kludge for RealConversions "switching" module *)
          IF ((*System.*)REALflavor = 0)
                          AND (strcmp(ADR(str), ADR("SystemIEEE")) = 0) THEN
            IF (*System.*)MathIeeeSingBasBase = NIL THEN
               (*System.*)MathIeeeSingBasBase := OpenLibrary(ADR("mathieeesingbas.library"), 0D);
            END;
            IF (*System.*)MathIeeeSingBasBase = NIL THEN
              err := MIErrNoIEEELib;
              DeleteModuleInfoArray();
              EXIT;
            END;
            (*System.*)REALflavor := 1;  (* IEEE *)
          ELSIF (((*System.*)MathIeeeDoubBasBase = NIL)
                          OR ((*System.*)MathIeeeDoubTransBase = NIL))
                          AND (strcmp(ADR(str), ADR("SystemIEEEdp")) = 0) THEN
            (*System.*)MathIeeeDoubBasBase := OpenLibrary(ADR("mathieeedoubbas.library"), 0D);
            IF (*System.*)MathIeeeDoubTransBase = NIL THEN
              (*System.*)MathIeeeDoubTransBase := OpenLibrary(ADR("mathieeedoubtrans.library"), 0D);
            END;
            (* maybe should not be assuming these two always go together *)
            IF ((*System.*)MathIeeeDoubBasBase = NIL)
                             OR ((*System.*)MathIeeeDoubTransBase = NIL) THEN
              err := MIErrNoIEEEdpLib;
              DeleteModuleInfoArray();
              EXIT;
            END;
          END;

          miName := AllocString(ADR(str[0]));
          IF (miName = NIL) THEN
            err := MIErrNoMemory;
            DeleteModuleInfoArray();
            EXIT;
          END;
        END; (* WITH *)
        seg := BPTRtoAPTR(seg^); (* Advance to next hunk *)
      END; (* FOR *)

      err := MIErrOk; (* Success! *)
      EXIT; (* done *)
    END; (* LOOP *)
    b := fclose(f);
  END;

  ShowCreateModuleInfoError(err);

  SetSizeVerifyModeAllVTWindows(TRUE); (* Enable size verify *)

  RETURN (err = MIErrOk);
END CreateModuleInfoArray;

(* DeleteModuleInfoArray - free all symbol, source info and module info *)
PROCEDURE DeleteModuleInfoArray();
VAR
  mod: CARDINAL;
BEGIN
  IF (ProgModuleArray # NIL) THEN
    FOR mod := 0 TO ProgModuleCount - 1 DO
      WITH ProgModuleArray^[mod] DO
        IF (miName # NIL) THEN DeallocString(miName); END;
        IF (miDefFileName # NIL) THEN DeallocString(miDefFileName); END;
        IF (miModFileName # NIL) THEN DeallocString(miModFileName); END;
        IF (miSbmFileName # NIL) THEN DeallocString(miSbmFileName); END;
        IF (miRfmFileName # NIL) THEN DeallocString(miRfmFileName); END;
        IF (miPosInfo # NIL) THEN DeallocMemory(miPosInfo); END;
        IF (miDefText # NIL) THEN 
          ClearSourceWindowsShowingFile(mod, FALSE);
          DeallocMemory(miDefText); 
        END;
        IF (miDefLineInfo # NIL) THEN DeallocMemory(miDefLineInfo); END;
        IF (miModText # NIL) THEN
          ClearSourceWindowsShowingFile(mod, TRUE);
          DeallocMemory(miModText);
        END;
        IF (miModLineInfo # NIL) THEN DeallocMemory(miModLineInfo); END;
        IF (miHeapList # NIL) THEN DeallocHeapList(miHeapList); END;
      END;
    END;
    DeallocMemory(ProgModuleArray);
    ProgModuleArray := NIL;
    ProgModuleCount := 0;
  END;
END DeleteModuleInfoArray;

(*-------------------------------------------------------------------------*)
 PROCEDURE FindAvailableFiles(VAR MainModFileStatus,
                              MainRfmFileStatus:FAFMainFileErrors): BOOLEAN;
(*-------------------------------------------------------------------------*)
(* FindAvailableFiles - scan directories looking for program files *)

VAR
  FFerr: FindAvailableFilesErrors;
  dn: DirNodePtr;
  fname: ARRAY [0..MaxFileNameLength-1] OF CHAR;
  RFMFile: BOOLEAN;
  RFMDate: DateStampRecord;
  MODDate: DateStampRecord;
  DEFDate: DateStampRecord;
  MODOk: BOOLEAN;
  b: BOOLEAN;
  mod: CARDINAL;
  lock: FileLock;
  l: LONGINT;
  RfmFound, RfmBadKey,
  ModFound, ModBadDate  :BOOLEAN;

BEGIN
  SetSizeVerifyModeAllVTWindows(FALSE); (* Disable size verify *)

  LOOP
    FOR mod := 0 TO ProgModuleCount - 1 DO

      RfmFound := FALSE; RfmBadKey := FALSE;
      ModFound := FALSE; ModBadDate := FALSE;

      WITH ProgModuleArray^[mod] DO

        (* Find All SBM and RFM files *)
        dn := ConfigSymbolFileDirList;
        WHILE (dn # NIL) DO
          RFMFile := TRUE;
          MakeFileName(ADR(fname[0]), dn^.dnName, miName, ADR(RFMFileExt));
          lock := Lock(ADR(fname[0]), SharedLock);
          IF (lock = NIL) THEN
            RFMFile := FALSE;
            MakeFileName(ADR(fname[0]), dn^.dnName, miName, ADR(SBMFileExt));
            lock := Lock(ADR(fname[0]), SharedLock);
          ELSE
            RfmFound := TRUE;
          END;

          IF (lock # NIL) THEN
            dn := NIL; (* end of search *)

            b := GetFileInfo(lock, RFMDate, l);
            UnLock(lock);
            IF (NOT b) THEN
              FFerr := FFErrNoMemory;
              EXIT; (* error *)
            END;
            
            (* Check For A Valid Key *)
            IF CheckSymbolFileKey(ProgModuleArray^[mod], ADR(fname[0])) THEN
              IF (RFMFile) THEN (* RFM File *)
                miRfmFileName := AllocString(ADR(fname[0]));
                IF (miRfmFileName = NIL) THEN
                  FFerr := FFErrNoMemory;
                  EXIT; (* err *)
                END;
              ELSE (* SBM File *)
                miSbmFileName := AllocString(ADR(fname[0]));
                IF (miSbmFileName = NIL) THEN
                  FFerr := FFErrNoMemory;
                  EXIT; (* err *)
                END;
              END;
            ELSE (* Invalid Symbol File *)
              RfmBadKey := RfmFound;
              RFMFile := FALSE; (* Ignore Date *)
            END;
          ELSE (* file not found, try next dir *)
            dn := dn^.dnNext;
          END;
        END; (* WHILE *)

        IF mod = 0 THEN
           IF NOT RfmFound THEN
              MainRfmFileStatus := FAFErrMainNotFound;
           ELSIF RfmBadKey THEN
              MainRfmFileStatus := FAFErrMainBadKey;
           ELSE
              MainRfmFileStatus := FAFErrMainOk;
           END;
        END;

        (* Find All MOD Files *)
        dn := ConfigSourceFileDirList;
        WHILE (dn # NIL) DO
          MakeFileName(ADR(fname[0]), dn^.dnName, miName, ADR(MODFileExt));
          lock := Lock(ADR(fname[0]), SharedLock);
          IF (lock # NIL) THEN
            ModFound := TRUE;
            dn := NIL; (* end of search *)
          
            b := GetFileInfo(lock, MODDate, miModFileSize);
            UnLock(lock);
            IF (NOT b) THEN
              FFerr := FFErrNoMemory;
              EXIT; (* error *)
            END;

            (* A .MOD file of length 0 is not valid *)
            MODOk := miModFileSize > 0D;

            IF (MODOk) AND (RFMFile) AND (ConfigCheckModFileDate) THEN
              ModBadDate := NOT CompareDates(MODDate, RFMDate);
              MODOk := NOT ModBadDate;
            END;

            IF (MODOk) THEN
              miModFileName := AllocString(ADR(fname[0]));
              IF (miModFileName = NIL) THEN
                FFerr := FFErrNoMemory;
                EXIT; (* error *)
              END;
            END;
          ELSE (* file not found, try next dir *)
            dn := dn^.dnNext;
          END;          
        END;

        IF mod = 0 THEN
           IF NOT ModFound THEN
              MainModFileStatus := FAFErrMainNotFound;
           ELSIF ModBadDate THEN
              MainModFileStatus := FAFErrMainBadDate;
           ELSE
              MainModFileStatus := FAFErrMainOk;
           END;
        END;

        (* Find All DEF Files *)
        dn := ConfigSourceFileDirList;
        WHILE (dn # NIL) DO
          MakeFileName(ADR(fname[0]), dn^.dnName, miName, ADR(DEFFileExt));
          lock := Lock(ADR(fname[0]), SharedLock);
          IF (lock # NIL) THEN
            dn := NIL; (* end of search *)

            b := GetFileInfo(lock, DEFDate, miDefFileSize);
            UnLock(lock);
            IF (NOT b) THEN
              FFerr := FFErrNoMemory;
              EXIT; (* error *)
            END;

            (* A .DEF file of length 0 is not valid *)
            IF (miDefFileSize > 0D) THEN
              miDefFileName := AllocString(ADR(fname[0]));
              IF (miDefFileName = NIL) THEN
                FFerr := FFErrNoMemory;
                EXIT; (* error *)
              END;
            END;
          ELSE (* file not found, try next dir *)
            dn := dn^.dnNext;
          END;          
        END;
      END; (* WITH *)
    END; (* FOR *)

    FFerr := FFErrOk;
    EXIT; (* Success! *)
  END; (* LOOP *)

  ShowFindAvailableFilesError(FFerr);

  SetSizeVerifyModeAllVTWindows(TRUE); (* Enable size verify *)

  RETURN (FFerr = FFErrOk);
END FindAvailableFiles;

(* GetSourceFileDef - Read .DEF source file if not already loaded. *)
PROCEDURE GetSourceFileDef(modNo: CARDINAL; 
                           VAR lineCount, maxWidth: CARDINAL): BOOLEAN;
VAR
  err: GetSourceFileErrors;
BEGIN
  WITH ProgModuleArray^[modNo] DO
    IF (miDefFileName = NIL) THEN (* .DEF file not available *)
      RETURN (FALSE);
    ELSE (* .DEF File available *)
      IF (miDefText = NIL) THEN (* Currently not loaded *)
        err := ReadSourceFile(miDefText, miDefLineInfo, miDefLineCount,
                              miDefMaxWidth, miDefFileName, miDefFileSize);
        IF (err = GSErrOk) THEN
          UpdateWindows(WindowClassSet{WCModuleList});
        END;
      ELSE (* Currently loaded *)
        err := GSErrOk;
      END;
      lineCount := miDefLineCount;
      maxWidth  := miDefMaxWidth;
    END;

    ShowGetSourceFileError(err);
  END; (* WITH *)

  RETURN (err = GSErrOk);
END GetSourceFileDef;

(* GetSourceFileMod - Read .MOD source file if not already loaded *)
PROCEDURE GetSourceFileMod(modNo: CARDINAL;
                           VAR lineCount, maxWidth: CARDINAL): BOOLEAN;
VAR
  err: GetSourceFileErrors;
BEGIN
  WITH ProgModuleArray^[modNo] DO
    IF (miModFileName = NIL) THEN (* .MOD file not available *)
      RETURN (FALSE);
    ELSE (* .MOD File available *)
      IF (miModText = NIL) THEN (* Currently not loaded *)
        PurgeModFile();
        err := ReadSourceFile(miModText, miModLineInfo, miModLineCount,
                              miModMaxWidth, miModFileName, miModFileSize);
        IF (err = GSErrOk) THEN 
          INC(ModFilesLoaded);
          UpdateWindows(WindowClassSet{WCModuleList});
        END;
      ELSE (* Currently loaded *)
        err := GSErrOk;
      END;
      IF (err = GSErrOk) THEN
        miModLastUse := ModLastUseValue;
        INC(ModLastUseValue);
      END;
      lineCount := miModLineCount;
      maxWidth  := miModMaxWidth;
    END;

    ShowGetSourceFileError(err);
  END; (* WITH *)

  RETURN (err = GSErrOk);
END GetSourceFileMod;

(* FreeSourceFileDef - free memory used by .DEF source file *)
PROCEDURE FreeSourceFileDef(modNo: CARDINAL);
BEGIN
  WITH ProgModuleArray^[modNo] DO
    IF (miDefText # NIL) THEN
      ClearSourceWindowsShowingFile(modNo, FALSE);
      DeallocMemory(miDefText);
      DeallocMemory(miDefLineInfo);
      miDefText     := NIL;
      miDefLineInfo := NIL;
      UpdateWindows(WindowClassSet{WCModuleList});
    END;
  END;
END FreeSourceFileDef;

(* FreeSourceFileMod - free memory used by .MOD source file *)
PROCEDURE FreeSourceFileMod(modNo: CARDINAL);
BEGIN
  WITH ProgModuleArray^[modNo] DO
    IF (miModText # NIL) THEN
      ClearSourceWindowsShowingFile(modNo, TRUE);
      DeallocMemory(miModText);
      DeallocMemory(miModLineInfo);
      miModText     := NIL;
      miModLineInfo := NIL;
      DEC(ModFilesLoaded);
      UpdateWindows(WindowClassSet{WCModuleList});
    END;
  END;
END FreeSourceFileMod;

(* GetSourceLineDef - get ptr to specified source .DEF line # *)
PROCEDURE GetSourceLineDef(modNo: CARDINAL; lineNo: CARDINAL): ADDRESS;
BEGIN
  WITH ProgModuleArray^[modNo] DO
    RETURN (miDefLineInfo^[lineNo]);
  END;
END GetSourceLineDef;

(* GetSourceLineMod - get ptr to specified source .MOD line # *)
PROCEDURE GetSourceLineMod(modNo: CARDINAL; lineNo: CARDINAL): ADDRESS;
BEGIN
  WITH ProgModuleArray^[modNo] DO
    RETURN (miModLineInfo^[lineNo]);
  END;
END GetSourceLineMod;

(* FindSourceStringDef - search source file .DEF for a string *)
PROCEDURE FindSourceStringDef(modNo: CARDINAL; findStr: ADDRESS;
                              VAR lineNo, startX, stopX: CARDINAL);
BEGIN
  WITH ProgModuleArray^[modNo] DO
    FindSourceString(miDefLineInfo, miDefLineCount, findStr,
                     lineNo, startX, stopX);
  END;
END FindSourceStringDef;

(* FindSourceStringMod - search source file .MOD for a string *)
PROCEDURE FindSourceStringMod(modNo: CARDINAL; findStr: ADDRESS;
                              VAR lineNo, startX, stopX: CARDINAL);
BEGIN
  WITH ProgModuleArray^[modNo] DO
    FindSourceString(miModLineInfo, miModLineCount, findStr,
                     lineNo, startX, stopX);
  END;
END FindSourceStringMod;

(* IsAdrInitCode - check if a specified adr is inside the program init code *)
PROCEDURE IsAdrInitCode(adr: ADDRESS): BOOLEAN;
BEGIN
  RETURN (InitCodeBase <= adr) AND (InitCodeEnd > adr);
END IsAdrInitCode;

(* GetInitCodeBase - get adr of start of init code *)
PROCEDURE GetInitCodeBase(): LONGCARD;
BEGIN
  RETURN (InitCodeBase);
END GetInitCodeBase;

(* GetInitCodeEnd - get adr of end of init code *)
PROCEDURE GetInitCodeEnd(): LONGCARD;
BEGIN
  RETURN (InitCodeEnd);
END GetInitCodeEnd;

(* GetStatementNoFromModOffset - determine statement # from code position *)
PROCEDURE GetStatementNoFromModOffset(modNo: CARDINAL; modOfs: LONGCARD; 
                                      VAR statNo: CARDINAL; 
                                      VAR firstInst: BOOLEAN);
VAR
  modItem: Item;
  modOfs16: CARDINAL;
  codeOffset: CARDINAL;
  low: CARDINAL;
  mid: CARDINAL;
  high: CARDINAL;
BEGIN
  (* Force .RFM file into memory if it is not loaded already. *)
  GetModuleItem(modNo, modItem, FALSE);

  modOfs16 := CARDINAL(modOfs);

  statNo := InvalidStatNo;
  WITH ProgModuleArray^[modNo] DO
    IF (miPosInfo # NIL) THEN
      low  := 0;
      high := miPosInfoCount;
      REPEAT
        mid        := (low + high) DIV 2;
        codeOffset := miPosInfo^[mid].piCodeOffset;
        IF (codeOffset = modOfs16) OR
           ((codeOffset < modOfs16) AND
            ((mid = miPosInfoCount-1) OR 
             (miPosInfo^[mid+1].piCodeOffset > modOfs16))) THEN
          statNo    := mid;
          firstInst := codeOffset = modOfs16;
          RETURN; (* Found! *)
        END;

        IF (modOfs16 < codeOffset) THEN
          high := mid;
        ELSE
          low := mid + 1;
        END;
      UNTIL (low >= high);
    END;
  END;

(******************* Linear Search Version ****************************
      FOR stat := 0 TO miPosInfoCount - 1 DO
        IF (miPosInfo^[stat].piCodeOffset <= modOfs16) THEN
          statNo    := stat;
          firstInst := miPosInfo^[stat].piCodeOffset = modOfs16;
        END;
      END;
***********************************************************************)
END GetStatementNoFromModOffset;

(* GetModOffsetFromStatementNo - determine code offset from statement # *)
PROCEDURE GetModOffsetFromStatementNo(modNo: CARDINAL; statNo: CARDINAL;
                                      VAR modOfs: LONGCARD);
VAR
  offset: LONGCARD;
BEGIN
  offset := InvalidModOfs;
  WITH ProgModuleArray^[modNo] DO
    IF (miPosInfo # NIL) THEN
      IF (statNo < miPosInfoCount) THEN
        offset := LONGCARD(miPosInfo^[statNo].piCodeOffset);
      END;
    END;
  END;
  modOfs := offset;
END GetModOffsetFromStatementNo;

(* GetSourceLineInfoFromStatementNo - get source pos from statement # *)
PROCEDURE GetSourceLineInfoFromStatementNo(modNo: CARDINAL; statNo: CARDINAL;
                                           VAR lineNo, startX, stopX: CARDINAL);
VAR
  curStatSrc: ADDRESS;
  nextStatSrc: ADDRESS;
  curStatLine: CARDINAL;
  nextStatLine: CARDINAL;
  cp: ADDRESS;
BEGIN
  lineNo := InvalidLineNo;
  WITH ProgModuleArray^[modNo] DO
    IF (miModLineInfo # NIL) THEN
      curStatSrc  := GetSourcePosFromStatementNo(modNo, statNo);
      nextStatSrc := GetSourcePosFromStatementNo(modNo, statNo + 1);

      (* Get lineNo of current statement *)
      GetLineNoFromSourcePos(modNo, curStatSrc, curStatLine);

      IF (nextStatSrc = InvalidSrcPos) THEN (* no next statement *)
        nextStatLine := InvalidLineNo;
      ELSE (* Get lineNo of next statement *)
        GetLineNoFromSourcePos(modNo, nextStatSrc, nextStatLine);
      END;

      IF (curStatLine # InvalidLineNo) THEN (* Valid lineNo *)
        lineNo := curStatLine;
        startX := (curStatSrc + miModText) - miModLineInfo^[curStatLine];
        IF (curStatLine = nextStatLine) THEN (* Next statement on same line *)
          (* Current stat ends at first non ' ' char backwards from next stat *)
          cp := nextStatSrc + miModText - 1D;
          WHILE (CHAR(cp^) = ' ') DO
            DEC(cp);
          END;
          stopX := cp - miModLineInfo^[curStatLine];
        ELSE (* curStatLine # nextStatLine (next statement on another line) *)
          (* Current statement ends at end of line *)
          cp := curStatSrc + miModText;
          WHILE (CHAR(cp^) # 0C) DO
            INC(cp);
          END;
          stopX := (cp - 1D) - miModLineInfo^[curStatLine];
        END;
      END;
    END;
  END; (* WITH *)
END GetSourceLineInfoFromStatementNo;

(* GetAdrFromLinePos - given a line # and char pos determine abs address  *)
PROCEDURE GetAdrFromLinePos(modNo: CARDINAL; lineX, lineNo: CARDINAL;
                            VAR absAdr: ADDRESS): BOOLEAN;
VAR
  absSrcPos: ADDRESS;
  relSrcPos: LONGCARD;
  line: CARDINAL;
  startX: CARDINAL;
  stopX: CARDINAL;
  modItem: Item;
  curPos: LONGCARD;
  low: CARDINAL;
  mid: CARDINAL;
  high: CARDINAL;
BEGIN
  (* Force .RFM file into memory if it is not loaded already. *)
  GetModuleItem(modNo, modItem, FALSE);

  WITH ProgModuleArray^[modNo] DO
    IF (miModLineInfo # NIL) AND (miPosInfo # NIL) THEN
      absSrcPos := miModLineInfo^[lineNo] + LONGCARD(lineX);
      relSrcPos := absSrcPos - miModText;

      IF (relSrcPos < miPosInfo^[0].piSourcePos) THEN
        RETURN (FALSE); (* Failed! *)
      END;

      low  := 0;
      high := miPosInfoCount;
      REPEAT
        mid    := (low + high) DIV 2;
        curPos := miPosInfo^[mid].piSourcePos;
        IF (curPos <= relSrcPos) AND
           ((mid = miPosInfoCount-1) OR
            (miPosInfo^[mid+1].piSourcePos > relSrcPos)) THEN
          GetSourceLineInfoFromStatementNo(modNo, mid, line, startX, stopX);
          absAdr := miCodeBase + LONGCARD(miPosInfo^[mid].piCodeOffset);
          RETURN ((lineNo = line) AND (startX <= lineX) AND (stopX >= lineX));
        END;

        IF (relSrcPos < curPos) THEN
          high := mid;
        ELSE
          low := mid + 1;
        END;
      UNTIL (low >= high);
    END;
  END;
  RETURN (FALSE); (* Failed! *)

(******************* Linear Search Version ****************************
      lastStat := miPosInfoCount - 1;
      FOR statNo := 0 TO lastStat DO
        IF (relSrcPos < miPosInfo^[statNo].piSourcePos) OR 
           (statNo = lastStat) THEN
          IF (statNo = 0) THEN (* Before first statement *)
            RETURN (FALSE); (* Failed! *)
          ELSIF (relSrcPos < miPosInfo^[statNo].piSourcePos) THEN
            DEC(statNo);
          END;
          GetSourceLineInfoFromStatementNo(modNo, statNo, line, startX, stopX);
          absAdr := miCodeBase + LONGCARD(miPosInfo^[statNo].piCodeOffset);
          RETURN ((lineNo = line) AND (startX <= lineX) AND (stopX >= lineX));
        END;
      END;
***********************************************************************)
END GetAdrFromLinePos;

(* GetModNoFromName - given a module name determine its module number *)
PROCEDURE GetModNoFromName(name: ADDRESS; VAR modNo: CARDINAL);
VAR
  mod: CARDINAL;
BEGIN
  FOR mod := 0 TO ProgModuleCount - 1 DO
    WITH ProgModuleArray^[mod] DO
      IF (strcmp(miName, name) = 0) THEN
        modNo := mod;
        RETURN; (* Success! *)
      END;
    END;
  END;
  modNo := InvalidModNo; (* Failure! *)
END GetModNoFromName;

(* GetFilePath - return filepath string for a given module and file type *)
PROCEDURE GetFilePath(modNo: CARDINAL; type: FileType): ADDRESS;
BEGIN
  WITH ProgModuleArray^[modNo] DO
    CASE (type) OF
    | FTDef: RETURN (miDefFileName);
    | FTMod: RETURN (miModFileName);
    | FTSbm: RETURN (miSbmFileName);
    | FTRfm: RETURN (miRfmFileName);
    END;
  END;
END GetFilePath;

(* GetFilesLoadStatus - return info on which files are currently loaded *)
PROCEDURE GetFilesLoadStatus(modNo: CARDINAL; VAR mod, def, sbm, rfm: BOOLEAN);
BEGIN
  WITH ProgModuleArray^[modNo] DO
    mod := miModText # NIL;
    def := miDefText # NIL;
    sbm := miSymbolTable # NIL;
    rfm := miSymbolTable # NIL;
  END;
END GetFilesLoadStatus;

(* GetModuleItem - get module object, forcing a symbol file load if necessary *)
PROCEDURE GetModuleItem(modNo: CARDINAL; VAR mod: Item; sbmOk: BOOLEAN);
VAR
  err: ReadSymbolFileErrors;
  obj: ObjectPtr;
BEGIN
  WITH ProgModuleArray^[modNo] DO
    IF (miSymbolTable = NIL) THEN
      (* If neither .SBM nor .RFM available then do nothing. *)
      IF (miRfmFileName # NIL) OR ((sbmOk) AND (miSbmFileName # NIL)) THEN
        err := ReadSymbolFile(modNo);
        ShowReadSymbolFileError(err);
        IF (err = RSErrOk) THEN
          UpdateWindows(WindowClassSet{WCModuleList});
        END;
      END;
    END;

    obj := miSymbolTable;
    IF (miSbmFileName # NIL) AND (NOT sbmOk) THEN
      obj := NIL;
    END;

    (* Initialize (mod) Item *)
    WITH mod DO
      iObj := obj;
      iStr := NIL;
    END;
  END; (* WITH *)
END GetModuleItem;

(* GetProcedureItem - get procedure object given its code offset *)
PROCEDURE GetProcedureItem(VAR mod: Item; modOfs: LONGCARD; VAR proc: Item);
VAR
  procObj: ObjectPtr;
  procOfs: CARDINAL;

  (* FindProcedure - recursively look for a proc inside others procs and mods *)
  PROCEDURE FindProcedure(obj: ObjectPtr);
  BEGIN
    WHILE (obj # NIL) DO
      WITH obj^ DO
        IF (oClass = Proc) THEN
          IF (procOfs >= oProcAdr) AND (procOfs < oProcAdr + oProcSize) AND
             (((procObj^.oClass = Proc) AND (oProcSize < procObj^.oProcSize)) OR
              (procObj^.oClass = Module)) THEN
            procObj := obj;
          END;
          FindProcedure(oFirstLocal);
        ELSIF (oClass = Module) THEN
          FindProcedure(oFirstObj)
        END;
      END; (* WITH *)
      obj := obj^.oNext;
    END; (* WHILE *)
  END FindProcedure;

BEGIN
  proc.iObj := NIL;
  proc.iStr := NIL;
  IF (mod.iObj # NIL) THEN
    procOfs := modOfs;
    procObj := mod.iObj;
    FindProcedure(procObj);
    proc.iObj := procObj;
  END;
END GetProcedureItem;

(* GetItemInfo - return private info on any object *)
PROCEDURE GetItemInfo(VAR item: Item; VAR info: ItemInfo);
BEGIN
  WITH item DO
    WITH info DO
      iiNameString := NIL;
      iiTypeString := NIL;
      iiAdr        := 0D;
      iiSize       := 0;
      iiIndirect   := FALSE;
      iiForm       := Undef;
      IF (iObj # NIL) THEN
        iiNameString := iObj^.oName;

        CASE (iObj^.oClass) OF
        | Const:  (* No other info available *)
        | Var:    iStr := iObj^.oType;
                  IF (iObj^.oVarAdr >= 0) THEN (* 0..32767 *)
                    iiAdr := LONG(00000H, iObj^.oVarAdr);
                  ELSE (* -32768..-1 (sign extend to 32 *)
                    iiAdr := LONG(0FFFFH, iObj^.oVarAdr);
                  END;
                  iiIndirect := iObj^.oVarIndirect;
                  iiVarPar   := iObj^.oVarPar;
        | Field:  iStr  := iObj^.oType;
                  iiAdr := LONG(0000H, iObj^.oOffset);
        | Proc:   iiForm       := Prc;
                  iiAdr        := LONG(0000H, iObj^.oProcAdr);
                  iiLevel      := iObj^.oLevel;
        | Module: iiForm       := Mod;
                  iiAdr        := 0D; (* Global data for local mods *)
                  iiModType    := iObj^.oModType;
                  IF (iiModType = MTLocal) THEN
                    iiTypeString := ADR("MODULE");
                  END;
        END; (* CASE *)
      END; (* IF *)
      IF (iStr # NIL) THEN
        iiTypeString := iStr^.sStrObj^.oName;
        iiForm       := iStr^.sForm;
        iiSize       := iStr^.sSize;

        CASE (iiForm) OF
        | Enum:  iiNofConst := iStr^.sNofConst;
        | Range: iiMin := iStr^.sMin;
                 iiMax := iStr^.sMax;
        | Array: iiDyn := iStr^.sDyn;
        END; (* CASE *)

        IF (iObj # NIL) THEN
          iStr := NIL;
        END;
      END; (* IF *)
    END; (* WITH *)
  END; (* WITH *)
END GetItemInfo;

(* GetBrotherItem - get next object in current scope *)
PROCEDURE GetBrotherItem(VAR item: Item; count: CARDINAL);
VAR
  obj: ObjectPtr;
  typ: StructurePtr;
BEGIN
  obj := item.iObj;
  (* Enumerations are handled by searching for constant with specified value *)
  IF (obj # NIL) AND (obj^.oClass = Const) AND (obj^.oType^.sForm = Enum) THEN
    typ := obj^.oType;
    WHILE (obj # NIL) AND ((obj^.oClass # Const) OR (obj^.oType # typ) OR
          (obj^.oValue # count)) DO
      obj := obj^.oNext;
    END;
  ELSE (* Var, Field or Module, skip over any other objects *)
    LOOP
      WHILE (obj # NIL) AND (obj^.oClass # Var) AND
            (obj^.oClass # Field) AND (obj^.oClass # Module) DO
        obj := obj^.oNext;
      END;
      IF (obj = NIL) OR (count = 0) THEN (* End of list or object found *)
        EXIT;
      END;
      obj := obj^.oNext;
      DEC(count);
    END;
  END;
  item.iObj := obj;
END GetBrotherItem;

(* GetSonItem - get object linked to current object *)
PROCEDURE GetSonItem(VAR item: Item);
BEGIN
  GetAnySonItem(item, FALSE);
END GetSonItem;

(* GetArrayIndexItem - get array index object *)
PROCEDURE GetArrayIndexItem(VAR item: Item);
BEGIN
  GetAnySonItem(item, TRUE);
END GetArrayIndexItem;

(* GetProcReturnItem - get procedure return type object *)
PROCEDURE GetProcReturnItem(VAR item: Item);
BEGIN
  GetAnySonItem(item, TRUE);
END GetProcReturnItem;

(* EmptyItem - determine if current object is empty *)
PROCEDURE EmptyItem(VAR item: Item): BOOLEAN;
BEGIN
  RETURN ((item.iObj = NIL) AND (item.iStr = NIL));
END EmptyItem;

(* NumberOfSons - count number of objects inside of specified object *)
PROCEDURE NumberOfSons(VAR item: Item): CARDINAL;
VAR
  count: CARDINAL;
  myItem: Item;
BEGIN
  myItem := item;
  count  := 0;
  GetSonItem(myItem);
  WHILE (NOT EmptyItem(myItem)) DO
    GetBrotherItem(myItem, 1);
    INC(count);
  END;
  RETURN (count);
END NumberOfSons;

(* EqualItems - compare two objects for equality *)
PROCEDURE EqualItems(VAR item1, item2: Item): BOOLEAN;
BEGIN
  RETURN (item1.iObj = item2.iObj) AND (item1.iStr = item2.iStr);
END EqualItems;

(* GetModuleCodeBase - get start of module code adr *)
PROCEDURE GetModuleCodeBase(modNo: CARDINAL): LONGCARD;
BEGIN
  RETURN (ProgModuleArray^[modNo].miCodeBase);
END GetModuleCodeBase;

(* GetModuleCodeEnd - get end of mode code adr *)
PROCEDURE GetModuleCodeEnd(modNo: CARDINAL): LONGCARD;
BEGIN
  RETURN (ProgModuleArray^[modNo].miCodeEnd);
END GetModuleCodeEnd;

(* GetModuleDataStart - get module data start adr *)
PROCEDURE GetModuleDataStart(modNo: CARDINAL): LONGCARD;
BEGIN
  RETURN (ProgModuleArray^[modNo].miDataStart);
END GetModuleDataStart;

(* GetModuleDataBase - get module middle of data adr *)
PROCEDURE GetModuleDataBase(modNo: CARDINAL): LONGCARD;
BEGIN
  RETURN (ProgModuleArray^[modNo].miDataBase);
END GetModuleDataBase;

(* GetModNo - determine if specified adr is in a specific module of program *)
PROCEDURE GetModNo(adr: ADDRESS; VAR modNo: CARDINAL);
VAR
  dummyOfs: LONGCARD;
BEGIN
  FindModNoModOfs(adr, modNo, dummyOfs);
END GetModNo;

(* GetModNoModOfs - determine module no and code offset based on abs adr *)
PROCEDURE GetModNoModOfs(adr: ADDRESS; VAR modNo: CARDINAL;
                         VAR modOfs: LONGCARD);
BEGIN
  FindModNoModOfs(adr, modNo, modOfs);
END GetModNoModOfs;

(* InitDBFiles - initialize DBFiles module *)
PROCEDURE InitDBFiles(): BOOLEAN;
VAR
  heapSize: LONGCARD;
BEGIN
  StdHeapList := NIL;
  IF (CreateHeap()) THEN
    EnterType(undftyp,     Undef,    1, ADR("UNDEFINED"));
    EnterType(booltyp,     Bool,     1, ADR("BOOLEAN"));
    EnterType(chartyp,     Char,     1, ADR("CHAR"));
    EnterType(cardtyp,     Card,     2, ADR("CARDINAL"));
    EnterType(inttyp,      Int,      2, ADR("INTEGER"));
    EnterType(dbltyp,      Double,   4, ADR("LONGINT"));
    EnterType(realtyp,     Real,     4, ADR("REAL"));
    EnterType(lrltyp,      LongReal, 8, ADR("LONGREAL"));
    EnterType(bitstyp,     Set,      2, ADR("BITSET"));
    EnterType(proctyp,     ProcTyp,  4, ADR("PROC"));
    EnterType(stringtyp,   String,   0, ADR("STRING"));
    EnterType(addrtyp,     Pointer,  4, ADR("ADDRESS"));
    EnterType(bytetyp,     Undef,    1, ADR("BYTE"));
    EnterType(wordtyp,     Undef,    2, ADR("WORD"));
    EnterType(lcardtyp,    LCard,    4, ADR("LONGCARD"));
    EnterType(longwordtyp, Undef,    4, ADR("LONGWORD"));

    EnterObject(enuObj, ADR("ENUMERATION")); 
    EnterObject(ptrObj, ADR("POINTER"));
    EnterObject(setObj, ADR("SET")); 
    EnterObject(arrObj, ADR("ARRAY"));
    EnterObject(recObj, ADR("RECORD")); 
    EnterObject(ptyObj, ADR("PROCEDURE"));
    EnterObject(opaObj, ADR("OPAQUE"));

    (* Define BITSET = SET OF CARDINAL[0..15] *)
    IF (bitstyp # NIL) THEN
      AllocStructure(bitstyp^.sSBaseTyp, Range);
      IF (bitstyp^.sSBaseTyp # NIL) THEN
        WITH bitstyp^.sSBaseTyp^ DO
          sStrObj   := cardtyp^.sStrObj;
          sSize     := 2;
          sForm     := Range;
          sRBaseTyp := cardtyp;
          sMin      := 0;
          sMax      := 15;
        END; (* WITH *)
      END; (* IF *)
    END; (* IF *)

    (* Define ADDRESS = POINTER TO ARRAY [0..32765] OF CHAR *)
    IF (addrtyp # NIL) THEN
      AllocStructure(addrtyp^.sPBaseTyp, Array);
      IF (addrtyp^.sPBaseTyp # NIL) THEN
        WITH addrtyp^.sPBaseTyp^ DO
          sStrObj   := arrObj;
          sSize     := 32766;
          sForm     := Array;
          sElemTyp  := chartyp;
          AllocStructure(sIndexTyp, Range);
          sDyn      := FALSE;
        END; (* WITH *)
        IF (addrtyp^.sPBaseTyp^.sIndexTyp # NIL) THEN
          WITH addrtyp^.sPBaseTyp^.sIndexTyp^ DO
            sStrObj   := cardtyp^.sStrObj;
            sSize     := 2;
            sForm     := Range;
            sRBaseTyp := cardtyp;
            sMin      := 0;
            sMax      := 32765;
          END; (* WITH *)
        END; (* IF *)
      END; (* IF *)
    END; (* IF *)

    IF (HeapErr) THEN (* error *)
      DeleteHeap();
    ELSE (* No error *)
      GetHeapInfo(StdHeapList, heapSize);
    END;
  END; (* IF (ok) *)
  RETURN (StdHeapList # NIL);
END InitDBFiles;

(* CleanupDBFiles - cleanup local module resources *)
PROCEDURE CleanupDBFiles();
BEGIN
  IF (StdHeapList # NIL) THEN
    DeallocHeapList(StdHeapList);
  END;
END CleanupDBFiles;

END DBFiles.
