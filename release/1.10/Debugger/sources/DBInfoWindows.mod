(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBInfoWindows.MOD                 Version: Amiga.01.10             *
 * Created: 02/22/89   Updated: 03/28/89   Author: Leon Frenkel             *
 * Description: Debugger Amiga information windows handling module.         *
 ****************************************************************************)

IMPLEMENTATION MODULE DBInfoWindows;

FROM SYSTEM IMPORT
  ADDRESS, BYTE,
  ADR;
FROM AmigaDOSProcess IMPORT
  ProcessPtr;
FROM CPrintBuffer IMPORT
  sprintf;
FROM CStrings IMPORT
  strcmp, strlen;
FROM DBConfiguration IMPORT
  ConfigWindow;
FROM DBMenusKbd IMPORT
  LinkGlobalMenuStrip, DoGlobalMenuCommand, DoGlobalKbdCommand,
  AddMenuItemWithCheck, AddMenuItemWithCheckList, AddMenuItemNoHighlight;
FROM DBMisc IMPORT
  AllocMemory, DeallocMemory, AllocString, DeallocString;
FROM DBRequesters IMPORT
  RequestTextFgBgColors;
FROM DBSelection IMPORT
  SelectionData,
  ClearSelection, GetSelection, SetSelection, GetSelectionLineInfo,
  IsPosSelected, IfCurrentWindowClearSelection;
FROM DBStatusWindow IMPORT
  ShowErrMsgNoMemory, ShowErrMsgBadDest,
  ShowError;
FROM DBTextWindows IMPORT
  DefMinWindowWidth, DefMinWindowHeight, DefMaxWindowWidth, DefMaxWindowHeight,
  VTWindow, NewVTWindow, VTWindowNIL, VTWindowParms,
  OpenVTWindow, CloseVTWindow, InitParmsVTWindow, SetCursorPosVTWindow,
  GetUserDataVTWindow, ClearVTWindow, RefreshVTWindow, WriteStringVTWindow,
  SetTitleVTWindow, GetInfoVTWindow, SetMenuStripVTWindow,
  ClearMenuStripVTWindow, SetDetailPenBlockPenVTWindow, SetDefaultColorVTWindow,
  GetDetailPenBlockPenVTWindow;
FROM DBWindows IMPORT
  WindowClass,
  GetVTWindowsMsgPort;
FROM ExecBase IMPORT
  ExecBasePtr;
FROM FormatString IMPORT
  FormatArg;
FROM IODevices IMPORT
  DevicePtr;
FROM Interrupts IMPORT
  InterruptPtr,
  Disable, Enable, Forbid, Permit;
FROM Intuition IMPORT
  Menu, MenuPtr, MenuItemMutualExcludeSet;
FROM Libraries IMPORT
  Library, LibraryPtr;
FROM Memory IMPORT
  MemReqSet, MemClear, MemChip, MemFast, MemLargest,
  AvailMem;
FROM Nodes IMPORT
  NTTask, NTProcess;
FROM Ports IMPORT
  MsgPortPtr, PASignal, PASoftInt, PAIgnore;
FROM Resources IMPORT
  ResourcePtr;
FROM SimpleMenus IMPORT
  BeginMenuStrip, EndMenuStrip, FreeMenuStrip, AddMenu, AddMenuItem;
FROM System IMPORT
  ExecBase;
FROM Tasks IMPORT
  TaskState, CurrentTask,
  FindTask;

CONST
  (* Info window titles for different display modes *)
  InfoModeResourcesTitle  = "Info: Resources";
  InfoModeDevicesTitle    = "Info: Devices";
  InfoModeLibrariesTitle  = "Info: Libraries";
  InfoModePortsTitle      = "Info: Ports";
  InfoModeTasksTitle      = "Info: Tasks";
  InfoModeMemoryTitle     = "Info: Memory";

  LocalMenus = 1; (* Number of window specific menus *)

  MaxInfoStringLength = 255; (* Maximum length of any info string *)

TYPE
  (* Information window display modes *)
  InfoMode = (IMResources, IMDevices, IMLibraries, IMPorts, IMTasks, IMMemory);

  (* Info window text line descriptor *)
  InfoLinePtr = POINTER TO InfoLine;
  InfoLine = RECORD
               ilNext   : InfoLinePtr;  (* ptr to next line or NIL *)
               ilString : ADDRESS;      (* ptr to text for line *)
             END;

  (* Information window descriptor *)
  InfoWindowPtr = POINTER TO InfoWindow;
  InfoWindow = RECORD
                 iwNext          : InfoWindowPtr; (* next window *)
                 iwVTWindow      : VTWindow;      (* text window handle *)
                 iwVTWindowParms : VTWindowParms; (* text window parms *)
                 iwMenuStrip     : MenuPtr;       (* local menu strip *)
                 iwLineList      : InfoLinePtr;   (* list of text lines *)
                 iwInfoMode      : InfoMode;      (* display mode *)
                 iwInfoLength    : CARDINAL;      (* char width of info *)
                 iwInfoWidth     : CARDINAL;      (* lines of info *)
               END;

VAR
  InfoWindowList : InfoWindowPtr; (* list of information windows *)
  ExecBaseAdr    : ExecBasePtr;   (* ptr to exec.library base *)

(* ===== PRIVATE ===== *)

(* DeleteInfoList - delete a linked list of text lines *)
PROCEDURE DeleteInfoList(list: InfoLinePtr);
VAR
  f: InfoLinePtr;
BEGIN
  WHILE (list # NIL) DO
    IF (list^.ilString # NIL) THEN
      DeallocString(list^.ilString);
    END;
    f     := list;
    list  := list^.ilNext;
    DeallocMemory(f);
  END;
END DeleteInfoList;

(* AddInfoLine - create a text line node and link into list *)
PROCEDURE AddInfoLine(VAR list: InfoLinePtr; str: ADDRESS): BOOLEAN;
VAR
  il: InfoLinePtr;
  newStr: ADDRESS;
  cur: InfoLinePtr;
  prev: InfoLinePtr;
BEGIN
  il := NIL;

  newStr := AllocString(str);
  IF (newStr # NIL) THEN
    il := AllocMemory(SIZE(InfoLine), MemReqSet{});
    IF (il # NIL) THEN
      (* Insert Node At Back of List *)
      IF (list = NIL) THEN (* Empty list *)
        list := il;
      ELSE (* Non-empty list, find end and insert node *)
        cur := list;
        WHILE (cur # NIL) DO
          prev := cur;
          cur  := cur^.ilNext;
        END;
        prev^.ilNext := il;
      END;

      (* Init node *)
      il^.ilNext   := NIL;
      il^.ilString := newStr;
    ELSE (* failed to alloc node struct *)
      DeallocString(str);
    END;
  END;
  RETURN (il # NIL);
END AddInfoLine;

(* NilString - if input parameter is NIL then return string with "NIL" text *)
PROCEDURE NilString(str: ADDRESS): ADDRESS;
BEGIN
  IF (str = NIL) THEN
    RETURN (ADR("[NIL]"));
  ELSE
    RETURN (str);
  END;
END NilString;

(* CreateResourceInfoList - create a list containg system resource info *)
PROCEDURE CreateResourcesInfoList(): InfoLinePtr;
CONST
           (*00000000001111111111222222222233333333334444444444555555555566666666667777777777*)
           (*01234567890123456789012345678901234567890123456789012345678901234567890123456789*)
  Heading = "Name                 Adr";
  Fmt     = "%-20s $%08lx";
VAR
  ok: BOOLEAN;
  list: InfoLinePtr;
  resource: ResourcePtr;
  r: CARDINAL;
  argo: ARRAY [0..9] OF FormatArg;
  outBuf: ARRAY [0..MaxInfoStringLength] OF CHAR;
BEGIN
  ok   := FALSE;
  list := NIL;
  IF (AddInfoLine(list, ADR(Heading))) THEN
    resource := ResourcePtr(ExecBaseAdr^.ResourceList.lhHead);
    LOOP
      IF (resource^.rLibrary.libNode.lnSucc = NIL) THEN
        ok := TRUE;
        EXIT; (* Success! *)
      END;

      WITH resource^.rLibrary.libNode DO
        argo[0].L := NilString(lnName);
        argo[1].L := resource;
        r := sprintf(ADR(outBuf[0]), ADR(Fmt), argo);
      END;

      IF (NOT AddInfoLine(list, ADR(outBuf[0]))) THEN
        EXIT; (* Faliure! *)
      END;
      
      resource := ResourcePtr(resource^.rLibrary.libNode.lnSucc);
    END; (* LOOP *)
  END;

  IF (NOT ok) THEN
    DeleteInfoList(list);
    list := NIL;
  END;
  RETURN (list);
END CreateResourcesInfoList;

(* CreateDevicesInfoList - create a list containg system devices info *)
PROCEDURE CreateDevicesInfoList(): InfoLinePtr;
CONST
           (*00000000001111111111222222222233333333334444444444555555555566666666667777777777*)
           (*01234567890123456789012345678901234567890123456789012345678901234567890123456789*)
  Heading = "Name                 Adr       OpenCount Version Revision IdString";
  Fmt     = "%-20s $%08lx %-9u %-7u %-8u %-20s";
VAR
  ok: BOOLEAN;
  list: InfoLinePtr;
  device: DevicePtr;
  r: CARDINAL;
  argo: ARRAY [0..9] OF FormatArg;
  outBuf: ARRAY [0..MaxInfoStringLength] OF CHAR;
BEGIN
  ok   := FALSE;
  list := NIL;
  IF (AddInfoLine(list, ADR(Heading))) THEN
    device := DevicePtr(ExecBaseAdr^.DeviceList.lhHead);
    LOOP
      IF (device^.ddLibrary.libNode.lnSucc = NIL) THEN
        ok := TRUE;
        EXIT; (* Success! *)
      END;

      WITH device^.ddLibrary DO
        argo[0].L := NilString(libNode.lnName);
        argo[1].L := device;
        argo[2].W := libOpenCnt;
        argo[3].W := libVersion;
        argo[4].W := libRevision;
        argo[5].L := NilString(libIdString);
        r := sprintf(ADR(outBuf[0]), ADR(Fmt), argo);
      END;

      IF (NOT AddInfoLine(list, ADR(outBuf[0]))) THEN
        EXIT; (* Faliure! *)
      END;
      
      device := DevicePtr(device^.ddLibrary.libNode.lnSucc);
    END; (* LOOP *)
  END;

  IF (NOT ok) THEN
    DeleteInfoList(list);
    list := NIL;
  END;
  RETURN (list);
END CreateDevicesInfoList;

(* CreateLibrariesInfoList - create a list containg system libraries info *)
PROCEDURE CreateLibrariesInfoList(): InfoLinePtr;
CONST
           (*00000000001111111111222222222233333333334444444444555555555566666666667777777777*)
           (*01234567890123456789012345678901234567890123456789012345678901234567890123456789*)
  Heading = "Name                 Adr       OpenCount Version Revision IdString";
  Fmt     = "%-20s $%08lx %-9u %-7u %-8u %-20s";
VAR
  ok: BOOLEAN;
  list: InfoLinePtr;
  library: LibraryPtr;
  r: CARDINAL;
  argo: ARRAY [0..9] OF FormatArg;
  outBuf: ARRAY [0..MaxInfoStringLength] OF CHAR;
BEGIN
  ok   := FALSE;
  list := NIL;
  IF (AddInfoLine(list, ADR(Heading))) THEN
    library := LibraryPtr(ExecBaseAdr^.LibList.lhHead);
    LOOP
      IF (library^.libNode.lnSucc = NIL) THEN
        ok := TRUE;
        EXIT; (* Success! *)
      END;

      WITH library^ DO
        argo[0].L := NilString(libNode.lnName);
        argo[1].L := library;
        argo[2].W := libOpenCnt;
        argo[3].W := libVersion;
        argo[4].W := libRevision;
        argo[5].L := NilString(libIdString);
        r := sprintf(ADR(outBuf[0]), ADR(Fmt), argo);
      END;

      IF (NOT AddInfoLine(list, ADR(outBuf[0]))) THEN
        EXIT; (* Faliure! *)
      END;
      
      library := LibraryPtr(library^.libNode.lnSucc);
    END; (* LOOP *)
  END;

  IF (NOT ok) THEN
    DeleteInfoList(list);
    list := NIL;
  END;
  RETURN (list);
END CreateLibrariesInfoList;

(* CreatePortsInfoList - create a list containg system ports info *)
PROCEDURE CreatePortsInfoList(): InfoLinePtr;
CONST
           (*00000000001111111111222222222233333333334444444444555555555566666666667777777777*)
           (*01234567890123456789012345678901234567890123456789012345678901234567890123456789*)
  Heading = "Name                 Adr       Mode      SigBit SigTask";
  Fmt     = "%-20s $%08lx %-9s %-6u $%08lx";
VAR
  ok: BOOLEAN;
  list: InfoLinePtr;
  port: MsgPortPtr;
  flagsStr: ADDRESS;
  r: CARDINAL;
  argo: ARRAY [0..9] OF FormatArg;
  outBuf: ARRAY [0..MaxInfoStringLength] OF CHAR;
BEGIN
  ok   := FALSE;
  list := NIL;
  IF (AddInfoLine(list, ADR(Heading))) THEN
    port := MsgPortPtr(ExecBaseAdr^.PortList.lhHead);
    LOOP
      IF (port^.mpNode.lnSucc = NIL) THEN
        ok := TRUE;
        EXIT; (* Success! *)
      END;

      WITH port^ DO
        argo[0].L := NilString(mpNode.lnName);
        argo[1].L := port;
        IF (CARDINAL(mpFlags) = CARDINAL(PASignal)) THEN
          flagsStr := ADR("Signal");
        ELSIF (CARDINAL(mpFlags) = CARDINAL(PASoftInt)) THEN
          flagsStr := ADR("SoftInt");
        ELSIF (CARDINAL(mpFlags) = CARDINAL(PAIgnore)) THEN
          flagsStr := ADR("Ignore");
        ELSE
          flagsStr := ADR("UNDEFINED");
        END;
        argo[2].L := flagsStr;
        argo[3].W := CARDINAL(mpSigBit);
        argo[4].L := mpSigTask;
        r := sprintf(ADR(outBuf[0]), ADR(Fmt), argo);
      END;

      IF (NOT AddInfoLine(list, ADR(outBuf[0]))) THEN
        EXIT; (* Faliure! *)
      END;
      
      port := MsgPortPtr(port^.mpNode.lnSucc);
    END; (* LOOP *)
  END;

  IF (NOT ok) THEN
    DeleteInfoList(list);
    list := NIL;
  END;
  RETURN (list);
END CreatePortsInfoList;

(* CreateTasksInfoList - create a list containg system tasks/processes info *)
PROCEDURE CreateTasksInfoList(): InfoLinePtr;
CONST
           (*00000000001111111111222222222233333333334444444444555555555566666666667777777777*)
           (*01234567890123456789012345678901234567890123456789012345678901234567890123456789*)
  Heading = "Name                 Adr       Type    Priority State     \
IDNestCnt TDNestCnt SigAlloc  SigWait   SigRecvd  SigExcept TrapAlloc \
TrapAble ExceptData ExceptCode TrapData  TrapCode";

  Fmt     = "%-20s $%08lx %-7s %-8d %-9s %-9u %-9u $%08lx $%08lx $%08lx $%08lx \
$%04x     $%04x    $%08lx  $%08lx  $%08lx $%08lx";

VAR
  ok: BOOLEAN;
  list: InfoLinePtr;
  process: ProcessPtr;
  readyList: BOOLEAN;
  debugProcess: ProcessPtr;
  str: ADDRESS;
  taskState: TaskState;
  priority: INTEGER;
  r: CARDINAL;
  argo: ARRAY [0..19] OF FormatArg;
  outBuf: ARRAY [0..MaxInfoStringLength] OF CHAR;
BEGIN
  ok   := FALSE;
  list := NIL;
  IF (AddInfoLine(list, ADR(Heading))) THEN
    process      := ProcessPtr(ExecBaseAdr^.TaskReady.lhHead);
    readyList    := TRUE;
    debugProcess := ProcessPtr(FindTask(CurrentTask));;
    LOOP
      IF (process^.prTask.tcNode.lnSucc = NIL) THEN
        IF (readyList) THEN
          readyList := FALSE;
          process   := ProcessPtr(ExecBaseAdr^.TaskWait.lhHead);
        ELSE (* WaitList done *)
          process := debugProcess; (* Do currently running process *)
        END;
      END;

      WITH process^.prTask DO
        argo[0].L := NilString(tcNode.lnName);
        argo[1].L := process;

        IF (CARDINAL(tcNode.lnType) = CARDINAL(NTProcess)) THEN
          str := ADR("Process");
        ELSE
          str := ADR("Task");
        END;
        argo[2].L := str;

        priority := INTEGER(tcNode.lnPri);
        IF (priority > 127) THEN (* 0..255 -> -128..127) *)
          DEC(priority, 256);
        END;
        argo[3].W := priority;

        taskState := TaskState(tcState);
        IF (taskState = TSAdded) THEN
          str := ADR("Added");
        ELSIF (taskState = TSRun) THEN
          str := ADR("Run");
        ELSIF (taskState = TSReady) THEN
          str := ADR("Ready");
        ELSIF (taskState = TSWait) THEN
          str := ADR("Wait");
        ELSIF (taskState = TSExcept) THEN
          str := ADR("Except");
        ELSIF (taskState = TSRemoved) THEN
          str := ADR("Removed");
        ELSE
          str := ADR("UNDEFINED");
        END;
        argo[4].L := str;

        argo[5].W  := CARDINAL(tcIDNestCnt);
        argo[6].W  := CARDINAL(tcTDNestCnt);
        argo[7].L  := tcSigAlloc;
        argo[8].L  := tcSigWait;
        argo[9].L  := tcSigRecvd;
        argo[10].L := tcSigExcept;
        argo[11].W := tcTrapAlloc;
        argo[12].W := tcTrapAble;
        argo[13].L := tcExceptData;
        argo[14].L := tcExceptCode;
        argo[15].L := tcTrapData;
        argo[16].L := tcTrapCode;
        r := sprintf(ADR(outBuf[0]), ADR(Fmt), argo);
      END;

      IF (NOT AddInfoLine(list, ADR(outBuf[0]))) THEN
        EXIT; (* Faliure! *)
      END;

      IF (process = debugProcess) THEN
        ok := TRUE;
        EXIT; (* Success! *)
      END;

      process := ProcessPtr(process^.prTask.tcNode.lnSucc);
    END; (* LOOP *)
  END;

  IF (NOT ok) THEN
    DeleteInfoList(list);
    list := NIL;
  END;
  RETURN (list);
END CreateTasksInfoList;

(* CreateMemoryInfoList - create a list containg memory usage info *)
PROCEDURE CreateMemoryInfoList(): InfoLinePtr;
CONST
           (*00000000001111111111222222222233333333334444444444555555555566666666667777777777*)
           (*01234567890123456789012345678901234567890123456789012345678901234567890123456789*)
  Heading = "Total     Chip      Fast      ChipLargest FastLargest";
  Fmt     = "%-9lu %-9lu %-9lu %-11lu %-11lu";
VAR
  ok: BOOLEAN;
  list: InfoLinePtr;
  r: CARDINAL;
  argo: ARRAY [0..9] OF FormatArg;
  outBuf: ARRAY [0..MaxInfoStringLength] OF CHAR;
BEGIN
  ok   := FALSE;
  list := NIL;
  IF (AddInfoLine(list, ADR(Heading))) THEN
    argo[0].L := AvailMem(MemReqSet{});
    argo[1].L := AvailMem(MemReqSet{MemChip});
    argo[2].L := AvailMem(MemReqSet{MemFast});
    argo[3].L := AvailMem(MemReqSet{MemChip, MemLargest});
    argo[4].L := AvailMem(MemReqSet{MemFast, MemLargest});
    r := sprintf(ADR(outBuf[0]), ADR(Fmt), argo);

    ok := AddInfoLine(list, ADR(outBuf[0]));
  END;

  IF (NOT ok) THEN
    DeleteInfoList(list);
    list := NIL;
  END;
  RETURN (list);
END CreateMemoryInfoList;

(* CreateInfoList - create a list of a specified type of info *)
PROCEDURE CreateInfoList(mode: InfoMode; VAR newList: InfoLinePtr; 
                         VAR newTitle: ADDRESS);
VAR
  il: InfoLinePtr;
  title: ADDRESS;
BEGIN
  Forbid();

  CASE (mode) OF
  | IMResources:  il    := CreateResourcesInfoList();
                  title := ADR(InfoModeResourcesTitle);
  | IMDevices:    il    := CreateDevicesInfoList();
                  title := ADR(InfoModeDevicesTitle);
  | IMLibraries:  il    := CreateLibrariesInfoList();
                  title := ADR(InfoModeLibrariesTitle);
  | IMPorts:      il    := CreatePortsInfoList();
                  title := ADR(InfoModePortsTitle);
  | IMTasks:      Disable(); (* Required because Interrupts change task list *)
                  il    := CreateTasksInfoList();
                  title := ADR(InfoModeTasksTitle);
                  Enable();
  | IMMemory:     il    := CreateMemoryInfoList();
                  title := ADR(InfoModeMemoryTitle);
  END;

  Permit();

  newList  := il;
  newTitle := title;
END CreateInfoList;

(* CompareInfoLists - compare two lists of text lines for equality *)
PROCEDURE CompareInfoLists(list1, list2: InfoLinePtr): BOOLEAN;
BEGIN
  LOOP
    IF (list1 = NIL) AND (list2 = NIL) THEN
      RETURN (TRUE); (* Equal! *)
    END;
    
    IF (list1 = NIL) OR (list2 = NIL) OR
       (strcmp(list1^.ilString, list2^.ilString) # 0) THEN
      RETURN (FALSE); (* Not Equal! *)
    END;

    list1 := list1^.ilNext;
    list2 := list2^.ilNext;
  END;
END CompareInfoLists;

(* CalcInfoListWidthAndLength - count # of lines and length of longest line *)
PROCEDURE CalcInfoListWidthAndLength(list: InfoLinePtr; VAR width, length: CARDINAL);
VAR
  maxWidth: CARDINAL;
  curWidth: CARDINAL;
  listLength: CARDINAL;
BEGIN
  maxWidth   := 0;
  listLength := 0;
  WHILE (list # NIL) DO
    curWidth := strlen(list^.ilString);
    IF (curWidth > maxWidth) THEN
      maxWidth := curWidth;
    END;
    INC(listLength);
    list := list^.ilNext;
  END;
  width  := maxWidth;
  length := listLength;
END CalcInfoListWidthAndLength;

(* AddWindowNode - create a window node and link into window list *)
PROCEDURE AddWindowNode(): InfoWindowPtr;
VAR
  iw: InfoWindowPtr;
BEGIN
  iw := AllocMemory(SIZE(InfoWindow), MemReqSet{MemClear});
  IF (iw # NIL) THEN
    IF (InfoWindowList # NIL) THEN
      iw^.iwNext := InfoWindowList;
    END;
    InfoWindowList := iw;
  END;
  RETURN (iw);
END AddWindowNode;

(* DeleteWindowNode - delete window node and unlink from window list *)
PROCEDURE DeleteWindowNode(iw: InfoWindowPtr);
VAR
  prev: InfoWindowPtr;
  cur: InfoWindowPtr;
BEGIN
  prev := NIL;
  cur  := InfoWindowList;
  WHILE (cur # iw) DO
    prev := cur;
    cur  := cur^.iwNext;
  END;

  IF (prev = NIL) THEN (* First in list *)
    InfoWindowList := iw^.iwNext;
  ELSE (* Not first in list *)
    prev^.iwNext := iw^.iwNext;
  END;

  DeallocMemory(iw);
END DeleteWindowNode;

(* GetWindowIndex - get position of window node in list *)
PROCEDURE GetWindowIndex(iw: InfoWindowPtr): CARDINAL;
VAR
  index: CARDINAL;
  p: InfoWindowPtr;
BEGIN
  index := 0;
  p     := InfoWindowList;
  WHILE (iw # p) DO
    INC(index);
    p := p^.iwNext;
  END;
  RETURN (index);
END GetWindowIndex;

(* CloseInfoWindows - close information window *)
PROCEDURE CloseInfoWindow(VAR iw: InfoWindow);
BEGIN
  WITH iw DO
    IfCurrentWindowClearSelection(iwVTWindow);

    IF (iwMenuStrip # NIL) THEN
      ClearMenuStripVTWindow(iwVTWindow);
      FreeMenuStrip(iwMenuStrip^);
    END;

    DeleteInfoList(iwLineList);

    CloseVTWindow(iwVTWindow);
  END;
  DeleteWindowNode(ADR(iw));
END CloseInfoWindow;

(* CloseInfoWindowNoMemory - close window and show no memory err msg *)
PROCEDURE CloseInfoWindowNoMemory(VAR iw: InfoWindow);
BEGIN
  CloseInfoWindow(iw);
  ShowError(ShowErrMsgNoMemory);
END CloseInfoWindowNoMemory;

(* CreateMenuStrip - create a local menu strip for a window *)
PROCEDURE CreateMenuStrip(VAR iw: InfoWindow): BOOLEAN;
TYPE
  M = MenuItemMutualExcludeSet;

VAR
  ms: MenuPtr;
  ok: BOOLEAN;
BEGIN
  BeginMenuStrip();
    AddMenu(                           ADR("Info "));
      AddMenuItemWithCheckList(          ADR(" Show Resources   "), (iw.iwInfoMode = IMResources), M{1,2,3,4,5});
      AddMenuItemWithCheckList(          ADR(" Show Devices"), (iw.iwInfoMode = IMDevices), M{0,2,3,4,5});
      AddMenuItemWithCheckList(          ADR(" Show Libraries"), (iw.iwInfoMode = IMLibraries), M{0,1,3,4,5});
      AddMenuItemWithCheckList(          ADR(" Show Ports"), (iw.iwInfoMode = IMPorts), M{0,1,2,4,5});
      AddMenuItemWithCheckList(          ADR(" Show Tasks"), (iw.iwInfoMode = IMTasks), M{0,1,2,3,5});
      AddMenuItemWithCheckList(          ADR(" Show Memory"), (iw.iwInfoMode = IMMemory), M{0,1,2,3,4});
      AddMenuItemNoHighlight(            ADR(" ----------------"));
      AddMenuItem(                       ADR(" Set Window Color"));
      AddMenuItem(                       ADR(" Set Text Color"));
  ms := EndMenuStrip();
  ok := (ms # NIL);

  IF (ok) THEN
    LinkGlobalMenuStrip(ms^);
    WITH iw DO
      iwMenuStrip := ms;
      SetMenuStripVTWindow(iwVTWindow, iwMenuStrip^);
    END;
  ELSE
    CloseInfoWindowNoMemory(iw);
  END;
  RETURN (ok);
END CreateMenuStrip;

(* UpdateInfoWindow - update contents of information window *)
PROCEDURE UpdateInfoWindow(VAR iw: InfoWindow);
VAR
  newList: InfoLinePtr;
  newTitle: ADDRESS;
  newWidth: CARDINAL;
  newLength: CARDINAL;
  refresh: BOOLEAN;
BEGIN
  WITH iw DO
    refresh := FALSE;
    IF (iwLineList = NIL) THEN (* No current list *)
      CreateInfoList(iwInfoMode, newList, newTitle);
      IF (newList = NIL) THEN (* Insufficient memory to create a list *)
        CloseInfoWindowNoMemory(iw);
      ELSE
        iwLineList := newList;
        SetTitleVTWindow(iwVTWindow, newTitle);
        refresh := TRUE;
      END;
    ELSE (* window has an existing list *)
      CreateInfoList(iwInfoMode, newList, newTitle);
      IF (newList = NIL) THEN (* Insufficient memory to create a list *)
        CloseInfoWindowNoMemory(iw);
      ELSE
        IF (NOT CompareInfoLists(newList, iwLineList)) THEN (* New # old *)
          DeleteInfoList(iwLineList);
          iwLineList := newList;
          refresh := TRUE;
        ELSE (* New list and old list are identical *)
          DeleteInfoList(newList);
        END;
      END;
    END;

    IF (refresh) THEN
      CalcInfoListWidthAndLength(iwLineList, newWidth, newLength);
      IF (iwInfoWidth = newWidth) AND (iwInfoLength = newLength) THEN
        ClearVTWindow(iwVTWindow);
        RefreshVTWindow(iwVTWindow);
      ELSE
        iwInfoWidth  := newWidth;
        iwInfoLength := newLength;
        iwVTWindowParms.vtpTotalLines := iwInfoLength;
        iwVTWindowParms.vtpMaxColumns := iwInfoWidth;
        IF (NOT InitParmsVTWindow(iwVTWindow, iwVTWindowParms)) THEN
          CloseInfoWindowNoMemory(iw);
        END;
      END;
    END;
  END;
END UpdateInfoWindow;

(* VTUpdateProc - called to output new text to window *)
PROCEDURE VTUpdateProc(Window: VTWindow; OutputRow,FirstLine,LastLine: CARDINAL);
VAR
  iw: InfoWindowPtr;
  line: CARDINAL;
  text: InfoLinePtr;
  i: CARDINAL;
BEGIN
  iw := GetUserDataVTWindow(Window);

  WITH iw^ DO
    text := iwLineList;
    i    := FirstLine;
    WHILE (i # 0) DO
      text := text^.ilNext;
      DEC(i);
    END;

    FOR line := FirstLine TO LastLine DO
      SetCursorPosVTWindow(Window, 0, OutputRow);
      WriteStringVTWindow(Window, text^.ilString);

      text := text^.ilNext;

      INC(OutputRow);
    END; (* FOR *)
  END;
END VTUpdateProc;

(* VTSelectProc - called when left mouse button click in window *)
PROCEDURE VTSelectProc(Window: VTWindow; x, y: CARDINAL; pressed: BOOLEAN);
BEGIN
  ClearSelection();
END VTSelectProc;

(* VTMenuProc - called when menu selection made *)
PROCEDURE VTMenuProc(Window: VTWindow; MenuNum, ItemNum, SubNum: CARDINAL);
VAR
  iw: InfoWindowPtr;
  fg: BYTE;
  bg: BYTE;
  update: BOOLEAN;
BEGIN
  iw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalMenuCommand(MenuNum, ItemNum, SubNum, LocalMenus,
                              WCInfo, GetWindowIndex(iw),
                              iw^.iwVTWindow)) THEN
    WITH iw^ DO
      IfCurrentWindowClearSelection(iwVTWindow);

      update := FALSE;

      CASE (ItemNum) OF
      | 0: (* Show Resources *)
           iwInfoMode := IMResources;
           update := TRUE;
      | 1: (* Show Devices *)
           iwInfoMode := IMDevices;
           update := TRUE;
      | 2: (* Show Libraries *)
           iwInfoMode := IMLibraries;
           update := TRUE;
      | 3: (* Show Ports *)
           iwInfoMode := IMPorts;
           update := TRUE;
      | 4: (* Show Tasks *)
           iwInfoMode := IMTasks;
           update := TRUE;
      | 5: (* Show Memory *)
           iwInfoMode := IMMemory;
           update := TRUE;
      (*6: -------------------- *)
      | 7: (* Set Window Color *)
           GetDetailPenBlockPenVTWindow(iwVTWindow, fg, bg);
           IF (RequestTextFgBgColors(fg, bg)) THEN
             SetDetailPenBlockPenVTWindow(iwVTWindow, fg, bg);
           END;
      | 8: (* Set Text Color *)
           WITH iwVTWindowParms DO
             IF (RequestTextFgBgColors(vtpDefFgColor, vtpDefBgColor)) THEN
               SetDefaultColorVTWindow(iwVTWindow, vtpDefFgColor,vtpDefBgColor);
             END;
           END;
      END;
      IF (update) THEN
        DeleteInfoList(iwLineList);
        iwLineList := NIL;
        UpdateInfoWindow(iw^);
      END;
    END;
  END;
END VTMenuProc;

(* VTDestProc - called when user clicks on destination-data gadget *)
PROCEDURE VTDestProc(Window: VTWindow);
BEGIN
  ShowError(ShowErrMsgBadDest);
END VTDestProc;

(* VTKeyProc - called when user types key into window *)
PROCEDURE VTKeyProc(Window: VTWindow; keyBuf: ADDRESS; keyLength: CARDINAL);
VAR
  iw: InfoWindowPtr;
BEGIN
  iw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalKbdCommand(keyBuf, keyLength, WCInfo,
                             GetWindowIndex(iw))) THEN
  END;
END VTKeyProc;

(* VTCloseProc - called when close-box hit *)
PROCEDURE VTCloseProc(Window: VTWindow);
VAR
  iw: InfoWindowPtr;
BEGIN
  iw := GetUserDataVTWindow(Window);
  CloseInfoWindow(iw^);
END VTCloseProc;

(* VTErrorProc - called when failed to alloc text info for resized window *)
PROCEDURE VTErrorProc(Window: VTWindow);
VAR
  iw: InfoWindowPtr;
BEGIN
  iw := GetUserDataVTWindow(Window);
  CloseInfoWindowNoMemory(iw^);
END VTErrorProc;

(* ===== PUBLIC ===== *)

(* OpenInfoWindow - open a new information window *)
PROCEDURE OpenInfoWindow(VAR config: ConfigWindow): BOOLEAN;
VAR
  iw: InfoWindowPtr;
  ok: BOOLEAN;
  nvt: NewVTWindow;
BEGIN
  ok := FALSE;
  iw := AddWindowNode();
  IF (iw # NIL) THEN
    WITH nvt DO
      WITH config DO
        nvtLeftEdge  := cwX;
        nvtTopEdge   := cwY;
        nvtWidth     := cwWidth;
        nvtHeight    := cwHeight;
        nvtDetailPen := cwDetailPen;
        nvtBlockPen  := cwBlockPen;
        nvtTitle     := ADR("");
        nvtMinWidth  := DefMinWindowWidth;
        nvtMinHeight := DefMinWindowHeight;
        nvtMaxWidth  := DefMaxWindowWidth;
        nvtMaxHeight := DefMaxWindowHeight;
        nvtMsgPort   := GetVTWindowsMsgPort();
        nvtUserData  := iw;
      END; (* WITH *)
    END; (* WITH *)
    iw^.iwVTWindow := OpenVTWindow(nvt);
    ok := iw^.iwVTWindow # VTWindowNIL;

    IF (ok) THEN
      WITH iw^.iwVTWindowParms DO
        WITH config DO
          vtpUpdateProc    := VTUpdateProc;
          vtpSelectProc    := VTSelectProc;
          vtpMenuProc      := VTMenuProc;
          vtpDestProc      := VTDestProc;
          vtpKeyProc       := VTKeyProc;
          vtpCloseProc     := VTCloseProc;
          vtpErrorProc     := VTErrorProc;
        (*vtpInitialLine   := 0;*)
        (*vtpTotalLines    := 1;*)
        (*vtpInitialColumn := 0;*)
        (*vtpMaxColumns    := 1;*)
          vtpDefFgColor    := cwFgColor;
          vtpDefBgColor    := cwBgColor;
        END;
      END;

      WITH iw^ DO
        iwLineList := NIL;
        iwInfoMode := InfoMode(config.cwInfoDispMode);
      END;

      UpdateInfoWindow(iw^);

      ok := CreateMenuStrip(iw^);
    ELSE (* NOT ok *)
      DeleteWindowNode(iw);
    END;
  END;
  RETURN (ok);
END OpenInfoWindow;

(* UpdateInfoWindows - update contents of all information windows *)
PROCEDURE UpdateInfoWindows();
VAR
  iw: InfoWindowPtr;
  p: InfoWindowPtr;
BEGIN
  iw := InfoWindowList;
  WHILE (iw # NIL) DO
    p  := iw;
    iw := iw^.iwNext;
    UpdateInfoWindow(p^);
  END;
END UpdateInfoWindows;

(* CloseInfoWindows - close all information windows *)
PROCEDURE CloseInfoWindows();
BEGIN
  WHILE (InfoWindowList # NIL) DO
    CloseInfoWindow(InfoWindowList^);
  END;
END CloseInfoWindows;

(* GetInfoWindowInfo - get config parameters of a specified window *)
PROCEDURE GetInfoWindowInfo(VAR index: CARDINAL;
                            VAR config: ConfigWindow): BOOLEAN;
VAR
  iw: InfoWindowPtr;
  count: CARDINAL;
BEGIN
  count := index;
  iw    := InfoWindowList;
  WHILE (iw # NIL) DO
    IF (count = 0) THEN
      WITH config DO
        WITH iw^ DO
          GetInfoVTWindow(iwVTWindow, cwX, cwY, cwWidth, cwHeight,
                          cwDetailPen, cwBlockPen);
          cwMaxCharWidth := 0;
          cwFgColor      := iwVTWindowParms.vtpDefFgColor;
          cwBgColor      := iwVTWindowParms.vtpDefBgColor;
          cwInfoDispMode := iwInfoMode;
        END;
      END;
      INC(index);
      RETURN (TRUE); (* Window Found! *)
    END;
    DEC(count);
    iw := iw^.iwNext;
  END;
  RETURN (FALSE); (* End of list *)
END GetInfoWindowInfo;

(* InitDBInfoWindows - initialize module *)
PROCEDURE InitDBInfoWindows(): BOOLEAN;
BEGIN
  ExecBaseAdr := ExecBase;

  RETURN (TRUE);
END InitDBInfoWindows;

(* CleanupDBInfoWindows - cleanup module *)
PROCEDURE CleanupDBInfoWindows();
BEGIN
  CloseInfoWindows();
END CleanupDBInfoWindows;

END DBInfoWindows.
