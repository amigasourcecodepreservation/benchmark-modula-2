(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBMisc.DEF                        Version: Amiga.01.10             *
 * Created: 01/03/89   Updated: 03/19/89   Author: Leon Frenkel             *
 * Description: Debugger misc. utility functions.                           *
 ****************************************************************************)

DEFINITION MODULE DBMisc;

FROM SYSTEM IMPORT
  ADDRESS, BYTE, WORD, LONGWORD;
FROM Memory IMPORT
  MemReqSet;
FROM Utility IMPORT
  Tag, TagItem;

TYPE
  (* Template for converting two 16-bit values to a 32-bit value *)
  ConvToLong = RECORD
                 CASE : CARDINAL OF
                 | 0: h, l : CARDINAL;
                 | 1: hl   : LONGCARD;
                 END;
               END;

  (* Template for converting a 32-bit value into two 16-bit values *)
  ConvToWord = RECORD
                 CASE : CARDINAL OF
                 | 0: h, l : BYTE;
                 | 1: hl   : CARDINAL;
                 END;
               END;


PROCEDURE AllocMemory(size: LONGCARD; flags: MemReqSet): ADDRESS;
(* Dynamically allocate a block of memory of a specified type and size.

   size - size of memory block to be allocated
   flags - type of memory to be allocated

   result - allocated memory block or NIL if insufficient memory *)


PROCEDURE DeallocMemory(ptr: ADDRESS);
(* Dealloc a block of memory previously allocated.

   ptr - ptr to the allocated block of memory *)


PROCEDURE ReduceMemory(ptr: ADDRESS; newSize: LONGCARD);
(* Reduce a previously allocated memory block to a new smaller size.

   ptr - ptr to a previously allocated memory block
   newSize - new smaller size of memory block *)


PROCEDURE GetMemorySize(ptr: ADDRESS): LONGCARD;
(* Get the size of a dynamically allocated block of memory.

   ptr - ptr to a previously allocated memory block

   result - size of memory block *)


PROCEDURE AllocString(str: ADDRESS): ADDRESS;
(* Allocate memory for a specified string and copy the string.

  str - string to be dynamically allocated and copied

  result - ptr to a dynamically allocated string or NIL if insufficient mem *)


PROCEDURE DeallocString(str: ADDRESS);
(* Dealloc previously allocated string.

   str - ptr to a previously allocated string *)


PROCEDURE GetMemoryByte(adr: ADDRESS): BYTE;
(* Get a byte from an address in memory.

   adr - memory address

   result - byte value found at address *)


PROCEDURE GetMemoryWord(adr: ADDRESS): CARDINAL;
(* Get a word from an address. Odd address is valid.

   adr - memory address

   result - word value found at address *)


PROCEDURE GetMemoryLong(adr: ADDRESS): LONGCARD;
(* Get a longword from an address.  Odd address is valid.

   adr - memory address

   result - longword value found at address *)


PROCEDURE GetMemoryValue(adr: ADDRESS; indirect: BOOLEAN; 
                         size: CARDINAL; Size8Ptr:ADDRESS): LONGCARD;
(* Get a value of a specified size at a specified adr. Odd address is valid.

   adr      - memory address 
   indirect - TRUE = get value through pointer, FALSE = get value directly
   size     - size of value to get (1, 2 or 4, or 8 is valid)
   Size8Ptr - if non-NIL, then if size is 8 the value is copied into this addr.

   result - value found at address *)


PROCEDURE GetMemoryString(adr: ADDRESS; buf: ADDRESS; maxSize: CARDINAL);
(* Get string starting at specific adr until buffer full or null-term found.

   adr - address of string
   buf - buffer to put string into
   maxSize - max length of string to get (incl. null-term) *)


PROCEDURE GetMemoryHIGH(adr: ADDRESS): CARDINAL;
(* Get dynamic array HIGH() parameter from memory.  Odd address is valid.

   adr - memory address

   result - HIGH value stored 4 bytes above the specified adr *)


PROCEDURE PutMemoryByte(adr: ADDRESS; value: BYTE);
(* Put a byte at a specified address.

   adr - memory address
   value - byte value to store *)


PROCEDURE PutMemoryWord(adr: ADDRESS; value: WORD);
(* Put a word at a specified address. Odd address is valid.

   adr - memory address
   value - word value to store *)


PROCEDURE PutMemoryLong(adr: ADDRESS; value: LONGWORD);
(* Put longword at a specified address. Odd address is valid.

   adr - memory address
   value - longword value to store *)


PROCEDURE PutMemoryValue(adr: ADDRESS; value: LONGWORD; size: CARDINAL);
(* Put a value of a specified size at a specified address.

   adr - memory address
   value - value to be stored
   size - size of value to put (1, 2 or 4 is valid) *)


PROCEDURE PutMemoryString(adr: ADDRESS; str: ADDRESS);
(* Copy a string to a specified memory location.

   adr - memory address
   str - string to be copied to memory *)


PROCEDURE CompareMemory(buf1, buf2: ADDRESS; length: LONGCARD): BOOLEAN;
(* Compare two block of memory for equality.

   buf1 - ptr to the first block of memory
   buf2 - ptr to the second block of memory
   length - number of bytes to compare

   result - TRUE = equal blocks of memory, FALSE = unequal memory *)


PROCEDURE SearchMemoryValue(start, end: ADDRESS; valueSize: CARDINAL;
                            value: LONGCARD): ADDRESS;
(* Search a range of memory for a given value of a specified size.

   start - start of memory range to search
   end - end of memory range to search
   valueSize - size of value to search for (1, 2 or 4 is valid)
   value - memory search value

   result - address where value was found or InvalidAbsAdr if not found *)


PROCEDURE BPTRtoAPTR(bptr: ADDRESS): ADDRESS;
(* Convert a BCPL ptr to a normal ptr, by shifting it left two bits.

   bptr - BCPL pointer value

   result - normal APTR value *)


PROCEDURE DeadKeyConvert(msg: ADDRESS; kbuf: ADDRESS; ksize: LONGCARD): INTEGER;
(* Translate a RawKey IDCMP message into a kbd indipendant ascii string.

   msg - ptr to an Intuition message
   kbuf - ptr to a keyboard output buffer
   ksize - size of keyboard output buffer

   result - number of chars stored into output buffer *)

 PROCEDURE CatTag(VAR Tags:ARRAY OF TagItem; t0:Tag; d0:LONGCARD);
(* Concatenate a new tag onto an existing, well formed, Tags array.

   Tags  - TagItem array, last used elem must contain TagDone.
   t0    - Tag to add
   d0    - Value for new tag *)

PROCEDURE InitDBMisc(): BOOLEAN;
(* Initialize DBMisc module.

   result - TRUE = sucessful, FALSE = failed *)


PROCEDURE CleanupDBMisc();
(* Cleanup DBMisc module. *)

END DBMisc.
