(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1988 by Jim Olinger                      *
 ****************************************************************************
 * Name: Viewer.MOD                        Version: Amiga.01.00             *
 * Created: 12/30/88   Updated: 12/30/88   Author: Leon Frenkel             *
 * Description: A multiple file text viewer utility.                        *
 ****************************************************************************)

MODULE Viewer;

FROM SYSTEM IMPORT
  ADDRESS, BYTE,
  ADR;
FROM AmigaDOS IMPORT
  FileInfoBlockPtr, FileLock, FileHandle,
  ModeOldFile, OffsetEnd, OffsetBeginning, SharedLock,
  Open, Close, Read, Seek, Lock, Examine, ExNext, UnLock;
FROM CMemory IMPORT
  calloc, malloc, free;
IMPORT CPrintTerminal;
FROM CPrintBuffer IMPORT
  sprintf;
FROM CStrings IMPORT
  strcpy, strcat, strlen;
FROM DBMisc IMPORT
  InitDBMisc, CleanupDBMisc;
FROM DBTextWindows IMPORT
  NewVTWindow, VTWindow, VTWindowParms, VTWindowNIL,
  DefMinWindowWidth, DefMinWindowHeight, DefMaxWindowWidth, DefMaxWindowHeight,
  InitVirtualTextWindows, CleanupVirtualTextWindows,
  OpenVTWindow, CloseVTWindow, RenderVTWindow, WriteCharVTWindow,
  InitParmsVTWindow, SetCursorPosVTWindow, SetColorVTWindow,
  HandleMsgVTWindow, ClearVTWindow, RefreshVTWindow, ScrollVTWindow,
  DefaultUpdateProc, DefaultSelectProc, DefaultMenuProc, DefaultCloseProc,
  GetIntuiWindowVTWindow, GetUserDataVTWindow, GetDisplayRangeVTWindow,
  RefreshRangeVTWindow;
FROM Drawing IMPORT
  Move;
FROM FormatString IMPORT
  FormatArg;
FROM Intuition IMPORT
  ScreenPtr, MenuPtr, WindowPtr, GadgetPtr, IntuiMessagePtr, MenuItemPtr,
  MenuItemFlags, MenuItemFlagsSet, MenuItemMutualExcludeSet, MenuNull,
  StringInfoPtr,
  IDCMPFlags, IDCMPFlagsSet, WindowFlags, WindowFlagsSet, SmartRefresh,
  CloseScreen, CloseWindow, SetMenuStrip, ClearMenuStrip, RemoveGList,
  ItemAddress, MENUNUM, ITEMNUM, SUBNUM;
FROM Memory IMPORT
  MemReqSet,
  AvailMem;
FROM Ports IMPORT
  MsgPortPtr,
  GetMsg, ReplyMsg;
FROM PortsUtil IMPORT
  CreatePort, DeletePort;
FROM Rasters IMPORT
  RastPortPtr;
FROM SimpleGadgets IMPORT
  LastGadget,
  BeginGadgetList, EndGadgetList, FreeGadgetList, AddGadgetString;
FROM SimpleMenus IMPORT
  DefaultItemFlags,
  BeginMenuStrip, EndMenuStrip, AddMenu, AddMenuItem, FreeMenuStrip,
  MenuItemOpt;
FROM SimpleScreens IMPORT
  CreateScreen;
FROM SimpleWindows IMPORT
  CreateWindow;
FROM Tasks IMPORT
  SignalSet,
  Wait;
FROM Text IMPORT
  Text;

CONST
  MsgTitle = "Viewer: Multi-Window Text File Viewer  Ver.01.00\n";

  MaxTextLines   = 8000;
  MaxFileNameStr = 128;
  MaxFindStr     = 128;
  MaxOutputWIDTH = 132;

  ScrWIDTH  = 640;
  ScrHEIGHT = 200;
  ScrDEPTH  = 2;
  ScrTITLE  = "Multi-Window Text File Viewer  Ver.01.00";

  SWinX      = 160;
  SWinY      = 65;
  SWinWIDTH  = 320;
  SWinHEIGHT = 70;
  SWinIDCMP  = IDCMPFlagsSet{MenuPick};
  SWinFLAGS  = WindowFlagsSet{WindowDrag, WindowDepth, Activate} + SmartRefresh;
  SWinTITLE  = "Viewer Input/Output Window";

  VWinX      = 0;
  VWinY      = 0;
  VWinWIDTH  = 320;
  VWinHEIGHT = 100;

  DirWinCharWIDTH = 60;
  DirWinNamePOS   = 0;
  DirWinSizePOS   = 40;

  DirWinDefFgColor  = BYTE(1);
  DirWinDefBgColor  = BYTE(2);
  DirWinSelFgColor  = BYTE(2);
  DirWinSelBgColor  = BYTE(1);

  TextWinDefFgColor  = BYTE(3);
  TextWinDefBgColor  = BYTE(2);
  TextWinFindFgColor = BYTE(2);
  TextWinFindBgColor = BYTE(3);

TYPE
  ViewerWindowType = (DirView, TextView);

  FileNameStr = ARRAY [0..MaxFileNameStr-1] OF CHAR;
  LineBufPtr = POINTER TO ARRAY [0..MaxTextLines-1] OF ADDRESS;

  DirObjPtr = POINTER TO DirObj;
  DirObj = RECORD
             doNext      : DirObjPtr;
             doFileType  : BOOLEAN;
             doName      : FileNameStr;
             doSize      : LONGCARD;
           END;

  ViewerWindowPtr = POINTER TO ViewerWindow;
  ViewerWindow = RECORD
                   vwNext        : ViewerWindowPtr;
                   vwTitle       : ADDRESS;
                   vwVTWindow    : VTWindow;
                   vwIntuiWindow : WindowPtr;
                   vwDispOn      : BOOLEAN;
                   CASE vwType : ViewerWindowType OF
                   | DirView:   vwDirPath    : FileNameStr;
                                vwDirObjs    : DirObjPtr;
                   | TextView:  vwFileBuf    : ADDRESS;
                                vwLineBuf    : LineBufPtr;
                                vwTotalLines : CARDINAL;
                                vwFindShow   : BOOLEAN;
                                vwFindLine   : CARDINAL;
                                vwFindFChar  : CARDINAL;
                                vwFindLChar  : CARDINAL;
                   END;
                 END;

VAR
  vVTWPort      : MsgPortPtr;
  vScreen       : ScreenPtr;
  vStatusMenu   : MenuPtr;
  vDirMenu      : MenuPtr;
  vTextMenu     : MenuPtr;
  vStatusGads   : GadgetPtr;
  vStatusWindow : WindowPtr;
  vVTWInit      : BOOLEAN;
  vWindows      : ViewerWindowPtr;

  vTextVTWParms : VTWindowParms;
  vDirVTWParms  : VTWindowParms;

  vItemSelFlag   : BOOLEAN;
  vItemSelWindow : VTWindow;
  vItemSelLine   : CARDINAL;

  vSearchStrBuf  : ADDRESS;
  vDirPathStrBuf : ADDRESS;

  vTextWindowCount : CARDINAL;
  vDirWindowCount : CARDINAL;

  vStatusWindowRP : RastPortPtr;

  vVTWPortSig : CARDINAL;
  vSWinPortSig : CARDINAL;
  vWaitMask : SignalSet;

  vProgDone : BOOLEAN;

  argo: ARRAY [0..9] OF FormatArg;

PROCEDURE printf(s: ARRAY OF CHAR);
BEGIN
  CPrintTerminal.printf(s, argo);
END printf;

PROCEDURE WriteStrVTWindow(VT: VTWindow; str: ADDRESS);
BEGIN
  WHILE (CHAR(str^) # 0C) DO
    WriteCharVTWindow(VT, CHAR(str^));
    INC(str);
  END;
END WriteStrVTWindow;

PROCEDURE ClrWindowParms(VAR vw: ViewerWindow; VAR parm: VTWindowParms);
BEGIN
  WITH parm DO
    vtpInitialLine := 0;
    vtpTotalLines  := 1;
    vtpMaxColumns  := 1;
  END;
  IF NOT InitParmsVTWindow(vw.vwVTWindow, parm) THEN 
    (* Can't Abort() because this routine can be called from Cleanup() *)
    printf("FATAL ERROR\n");
    HALT;
  END;
END ClrWindowParms;

PROCEDURE FindDirObj(VAR vw: ViewerWindow; count: CARDINAL): DirObjPtr;
VAR
  do: DirObjPtr;
BEGIN
  WITH vw DO
    do := vwDirObjs;
    WHILE (count # 0) DO
      do := do^.doNext;
      DEC(count);
    END;
  END;
  RETURN (do);
END FindDirObj;

PROCEDURE ClearItemSelected();
VAR
  first, last: CARDINAL;
BEGIN
  IF (vItemSelFlag) THEN
    vItemSelFlag := FALSE;
    GetDisplayRangeVTWindow(vItemSelWindow, first, last);
    IF (vItemSelLine >= first) AND (vItemSelLine <= last) THEN
      RefreshRangeVTWindow(vItemSelWindow, vItemSelLine - first,
                                           vItemSelLine - first);
    END;
  END;
END ClearItemSelected;

PROCEDURE NewItemSelected(VT: VTWindow; line: CARDINAL);
VAR
  first, last: CARDINAL;
BEGIN
  ClearItemSelected();
  vItemSelFlag   := TRUE;
  vItemSelWindow := VT;
  vItemSelLine   := line;
  GetDisplayRangeVTWindow(vItemSelWindow, first, last);
  RefreshRangeVTWindow(vItemSelWindow, vItemSelLine - first,
                                       vItemSelLine - first);
END NewItemSelected;

PROCEDURE IsItemSelected(VT: VTWindow; line: CARDINAL): BOOLEAN;
BEGIN
  RETURN (vItemSelFlag) AND (vItemSelWindow = VT) AND (vItemSelLine = line);
END IsItemSelected;

PROCEDURE IsWindowSelected(VT: VTWindow): BOOLEAN;
BEGIN
  RETURN (vItemSelFlag) AND (vItemSelWindow = VT);
END IsWindowSelected;

PROCEDURE GetItemSelected(filePath: ADDRESS; VAR fileSize: LONGCARD): BOOLEAN;
VAR
  vw: ViewerWindowPtr;
  l: CARDINAL;
  sp: POINTER TO ARRAY [0..32765] OF CHAR;
  do: DirObjPtr;
BEGIN
  IF (vItemSelFlag) THEN
    sp := filePath;
    vw := GetUserDataVTWindow(vItemSelWindow);
    do := FindDirObj(vw^, vItemSelLine);
    IF (do^.doFileType) THEN
      strcpy(ADR(sp^[0]), ADR(vw^.vwDirPath[0]));
      l := strlen(ADR(sp^[0]));
      IF (l = 0) OR ((sp^[l-1] # ":") AND (sp^[l-1] # "/")) THEN
        strcat(ADR(sp^[0]), ADR("/"));
      END;
      strcat(ADR(sp^[0]), ADR(do^.doName[0]));

      fileSize := do^.doSize;;

      ClearItemSelected();
      RETURN (TRUE);
    END;
  END;
  RETURN (FALSE);
END GetItemSelected;

PROCEDURE CloseTextData(VAR vw: ViewerWindow);
BEGIN
  WITH vw DO
    IF (vwDispOn) THEN
      vwDispOn   := FALSE;
      vwFindShow := FALSE;
      free(vwFileBuf);
      free(vwLineBuf);
      ClrWindowParms(vw, vTextVTWParms);
    END;
  END;
END CloseTextData;

PROCEDURE CloseDirData(VAR vw: ViewerWindow);
VAR
  p, q: DirObjPtr;
BEGIN
  WITH vw DO
    IF (vwDispOn) THEN
      IF (IsWindowSelected(vw.vwVTWindow)) THEN
        ClearItemSelected();
      END;
      vwDispOn := FALSE;
      p := vwDirObjs;
      WHILE (p # NIL) DO
        q := p^.doNext;
        free(p);
        p := q;
      END;
      vwDirObjs := NIL;
      ClrWindowParms(vw, vDirVTWParms);
    END;
  END;
END CloseDirData;

PROCEDURE CloseViewerWindow(VAR vw: ViewerWindow);
VAR
  pvw: ViewerWindowPtr;
  cvw: ViewerWindowPtr;
BEGIN
  cvw := vWindows;
  pvw := NIL;
  WHILE (cvw # ADR(vw)) DO
    pvw := cvw;
    cvw := cvw^.vwNext;
  END;

  IF (pvw = NIL) THEN (* first window in list *)
    vWindows :=vw.vwNext;
  ELSE (* not-first window in list *)
    pvw^.vwNext := vw.vwNext;
  END;

  WITH vw DO
    IF (vwType = DirView) THEN
      CloseDirData(vw);
    ELSE (* type = TextView *)
      CloseTextData(vw);
    END;

    IF (vwVTWindow # VTWindowNIL) THEN
      ClearMenuStrip(vwIntuiWindow^);
      CloseVTWindow(vwVTWindow);
    END;
    IF (vwTitle # NIL) THEN
      free(vwTitle);
    END;
  END;
  free(ADR(vw));
END CloseViewerWindow;

PROCEDURE Cleanup();
VAR
  r: INTEGER;
BEGIN
  WHILE (vWindows # NIL) DO
    CloseViewerWindow(vWindows^);
  END;

  IF (vVTWInit) THEN
    CleanupVirtualTextWindows();
  END;

  IF (vStatusWindow # NIL) THEN
    ClearMenuStrip(vStatusWindow^);
    r := RemoveGList(vStatusWindow^, vStatusGads^, -1);
    CloseWindow(vStatusWindow^);
  END;

  IF (vStatusGads # NIL) THEN
    FreeGadgetList(vStatusGads^);
  END;

  IF (vTextMenu # NIL) THEN
    FreeMenuStrip(vTextMenu^);
  END;

  IF (vDirMenu # NIL) THEN
    FreeMenuStrip(vDirMenu^);
  END;

  IF (vStatusMenu # NIL) THEN
    FreeMenuStrip(vStatusMenu^);
  END;

  IF (vScreen # NIL) THEN
    CloseScreen(vScreen^);
  END;

  IF (vVTWPort # NIL) THEN
    DeletePort(vVTWPort^);
  END;  

  CleanupDBMisc();
END Cleanup;

PROCEDURE Abort();
BEGIN
  Cleanup();
  HALT;
END Abort;

PROCEDURE OpenViewerWindow(type: ViewerWindowType): ViewerWindowPtr;
VAR
  vw: ViewerWindowPtr;
  outStr: ARRAY [0..79] OF CHAR;
  outLen: CARDINAL;
  nw: NewVTWindow;
BEGIN
  vw := calloc(1, SIZE(vw^));
  IF (vw = NIL) THEN Abort(); END;

  (* Link into list *)
  vw^.vwNext := vWindows;
  vWindows   := vw;

  vw^.vwType := type;

  IF (type = DirView) THEN
    INC(vDirWindowCount);
    argo[0].L := ADR("Directory Window");
    argo[1].W := vDirWindowCount;
  ELSE (* type = TextView *)
    INC(vTextWindowCount);
    argo[0].L := ADR("Text Window");
    argo[1].W := vTextWindowCount;
  END;
  outLen := sprintf(ADR(outStr[0]), ADR("%s [%u]"), argo);

  vw^.vwTitle := malloc(outLen + 1);
  IF (vw^.vwTitle = NIL) THEN Abort(); END;

  strcpy(vw^.vwTitle, ADR(outStr[0]));

  WITH nw DO
    nvtLeftEdge  := VWinX;
    nvtTopEdge   := VWinY;
    nvtWidth     := VWinWIDTH;
    nvtHeight    := VWinHEIGHT;
    nvtDetailPen := BYTE(0);
    nvtBlockPen  := BYTE(1);
    nvtTitle     := vw^.vwTitle;
    nvtMinWidth  := DefMinWindowWidth;
    nvtMinHeight := DefMinWindowHeight;
    nvtMaxWidth  := DefMaxWindowWidth;
    nvtMaxHeight := DefMaxWindowHeight;
    nvtMsgPort   := vVTWPort;
    nvtUserData  := vw;                 (* Back-link *)
  END;
  vw^.vwVTWindow := OpenVTWindow(nw);
  IF (vw^.vwVTWindow = VTWindowNIL) THEN Abort(); END;

  vw^.vwIntuiWindow := GetIntuiWindowVTWindow(vw^.vwVTWindow);

  IF (type = DirView) THEN
    SetMenuStrip(vw^.vwIntuiWindow^, vDirMenu^);
    ClrWindowParms(vw^, vDirVTWParms);
  ELSE (* type = TextView *)
    SetMenuStrip(vw^.vwIntuiWindow^, vTextMenu^);
    ClrWindowParms(vw^, vTextVTWParms);
  END;
END OpenViewerWindow;

PROCEDURE OpenTextWindow();
VAR
  vw: ViewerWindowPtr;
BEGIN
  vw := OpenViewerWindow(TextView);
END OpenTextWindow;

PROCEDURE OpenDirWindow();
VAR
  vw: ViewerWindowPtr;
BEGIN
  vw := OpenViewerWindow(DirView);
END OpenDirWindow;

PROCEDURE ShowMemoryUsage();
VAR
  outStr: ARRAY [0..79] OF CHAR;
  numChars: CARDINAL;
BEGIN
  argo[0].L := AvailMem(MemReqSet{});
  numChars := sprintf(ADR(outStr[0]), ADR("Available Memory = %lu       "), argo);

  Move(vStatusWindowRP^, 8, 66);
  Text(vStatusWindowRP^, ADR(outStr[0]), numChars);
END ShowMemoryUsage;

PROCEDURE GlobalMenuHandler(menuNum, itemNum, subNum: CARDINAL): BOOLEAN;
BEGIN
  IF (menuNum > 1) THEN RETURN (FALSE); END;

  CASE (menuNum) OF
  | 0: CASE (itemNum) OF
       | 0: ShowMemoryUsage();
       | 1: vProgDone := TRUE;
       END;
  | 1: CASE(itemNum) OF
       | 0: OpenDirWindow();
       | 1: OpenTextWindow();
    END;
  END;

  RETURN (TRUE);
END GlobalMenuHandler;

PROCEDURE HandleMsgStatusWindow(imsg: IntuiMessagePtr);
VAR
  menuCode: CARDINAL;
  menuItemPtr: MenuItemPtr;
  b: BOOLEAN;
BEGIN
  (* Only MenuPick msg possible *)
  menuCode := imsg^.Code;
  ReplyMsg(imsg);

  WHILE (menuCode # MenuNull) DO
    b :=GlobalMenuHandler(MENUNUM(menuCode),ITEMNUM(menuCode),SUBNUM(menuCode));

    menuItemPtr := ItemAddress(vStatusMenu^, menuCode);
    menuCode    := menuItemPtr^.NextSelect;
  END;
END HandleMsgStatusWindow;

PROCEDURE MainLoop();
VAR
  retMask: SignalSet;
  msg: ADDRESS;
BEGIN
  WHILE (NOT vProgDone) DO
    retMask := Wait(vWaitMask);
    LOOP
      IF (vSWinPortSig IN retMask) THEN
        LOOP
          msg := GetMsg(vStatusWindow^.UserPort^);
          IF (msg = NIL) THEN EXIT; END;
          HandleMsgStatusWindow(msg);
        END;
      END;
      IF (vVTWPortSig IN retMask) THEN
        LOOP
          msg := GetMsg(vVTWPort^);
          IF (msg = NIL) THEN EXIT; END;
          HandleMsgVTWindow(msg);
        END;
      END;
      EXIT; (* Wait() again *)
    END; (* LOOP *)
  END; (* WHILE *)
END MainLoop;

PROCEDURE IsPathDir(dirPath: ADDRESS; VAR lock: FileLock;
                    VAR fib: FileInfoBlockPtr): BOOLEAN;
BEGIN
  fib := malloc(SIZE(fib^));
  IF (fib = NIL) THEN Abort(); END;

  lock := Lock(dirPath, SharedLock);
  IF (lock # NIL) THEN
    IF (NOT Examine(lock, fib^)) OR (NOT (fib^.fibDirEntryType > 0D)) THEN
      UnLock(lock);
      free(fib);
      lock := NIL;
    END;
  END;
  RETURN (lock # NIL);
END IsPathDir;

PROCEDURE OpenDirData(VAR vw: ViewerWindow; dirPath: ADDRESS);
VAR
  fib: FileInfoBlockPtr;
  lock: FileLock;
  do: DirObjPtr;
  objCount: CARDINAL;
BEGIN
  CloseDirData(vw);
  WITH vw DO
    IF NOT IsPathDir(dirPath, lock, fib) THEN RETURN; END;

    strcpy(ADR(vwDirPath[0]), dirPath);
    objCount := 0;
    WHILE ExNext(lock, fib^) DO
      do := malloc(SIZE(do^));
      IF (do = NIL) THEN
        free(fib);
        UnLock(lock);
        Abort();
      END;

      vwDispOn := TRUE;

      WITH do^ DO
        doNext    := vwDirObjs;
        vwDirObjs := do;
        doFileType := fib^.fibDirEntryType < 0D;
        doSize     := fib^.fibSize;
        strcpy(ADR(doName[0]), ADR(fib^.fibFileName[0]));
      END;
      INC(objCount);
    END;
    free(fib);
    UnLock(lock);

    WITH vDirVTWParms DO
      vtpInitialLine := 0;
      vtpTotalLines  := objCount;
      vtpMaxColumns  := DirWinCharWIDTH;
    END;
    IF NOT InitParmsVTWindow(vwVTWindow, vDirVTWParms) THEN Abort(); END;
  END; (* WITH *)
END OpenDirData;

PROCEDURE OpenTextData(VAR vw: ViewerWindow; filePath: ADDRESS;
                       fileSize: LONGCARD);
VAR
  fh: FileHandle;
  fbuf: ADDRESS;
  lbuf: LineBufPtr;
  lineCount: CARDINAL;
  maxWidth: CARDINAL;
  curWidth: CARDINAL;
  cp: ADDRESS;
  n: LONGCARD;
  readErr: BOOLEAN;
  i: CARDINAL;
BEGIN
  CloseTextData(vw);
  WITH vw DO
    fh := Open(filePath, ModeOldFile);
    IF (fh = NIL) THEN RETURN; END;

    fbuf := malloc(fileSize);
    IF (fh = NIL) THEN
      Close(fh);
      Abort();
    END;

    readErr := Read(fh, fbuf, fileSize) # LONGINT(fileSize);
    Close(fh);
    IF (readErr) THEN
      free(fbuf);
      Abort();
    END;

    maxWidth := 0;
    curWidth := 0;
    lineCount := 0;
    cp := fbuf;
    n := fileSize;
    WHILE (n # 0D) DO
      IF (CHAR(cp^) = CHAR(0AH)) THEN
        IF (lineCount < MaxTextLines) THEN
          INC(lineCount);
        END;
        IF (curWidth > maxWidth) THEN
          maxWidth := curWidth;
        END;
        curWidth := 0;
      ELSE
        INC(curWidth);
      END;

      INC(cp);
      DEC(n);
    END;

    lbuf := malloc(SIZE(LONGCARD) * lineCount);
    IF (lbuf = NIL) THEN
      free(fbuf);
      Abort();
    END;

    cp := fbuf;
    FOR i := 0 TO lineCount-1 DO
      lbuf^[i] := cp;
      WHILE (CHAR(cp^) # CHAR(0AH)) DO
        INC(cp);
      END;
      cp^ := 0C; (* Set LF = NUL *)
      INC(cp);
    END;

    vwDispOn     := TRUE;
    vwFileBuf    := fbuf;
    vwLineBuf    := lbuf;
    vwTotalLines := lineCount;

    WITH vTextVTWParms DO
      vtpInitialLine := 0;
      vtpTotalLines  := lineCount;
      vtpMaxColumns  := maxWidth;
    END;
    IF NOT InitParmsVTWindow(vwVTWindow, vTextVTWParms) THEN Abort(); END;
  END; (* WITH *)
END OpenTextData;

PROCEDURE FindStrInLine(buf: ADDRESS; searchStr: ADDRESS;
                        VAR firstChar: CARDINAL): BOOLEAN;
TYPE
  CPTR = POINTER TO CHAR;
VAR
  scanStrStart: CPTR;
  scanStrCurrent: CPTR;
  codeCurrent: CPTR;
  codeSize: CARDINAL;
  found: BOOLEAN;
  firstPtr: ADDRESS;
BEGIN
  found          := FALSE;
  scanStrStart   := searchStr;
  scanStrCurrent := scanStrStart;
  codeCurrent    := buf;
  codeSize       := strlen(buf);

  LOOP
    IF (codeSize = 0) THEN EXIT; END;

    IF (scanStrCurrent = scanStrStart) THEN (* Check for first char *)
      firstPtr := codeCurrent;
      IF (scanStrCurrent^ = codeCurrent^) THEN (* Match *)
        INC(ADDRESS(scanStrCurrent));
      END;
    ELSE (* Check for subsequent char *)
      IF (scanStrCurrent^ = codeCurrent^) THEN (* Match *)
        INC(ADDRESS(scanStrCurrent));
        IF (scanStrCurrent^ = 0C) THEN
          found := TRUE;
          EXIT;
        END; (* Match-Done *)
      ELSE (* no-Match *)
        scanStrCurrent := scanStrStart; (* reset to start of source *)
        INC(codeSize); DEC(ADDRESS(codeCurrent)); (* do this char again *)
      END;
    END;
    DEC(codeSize);
    INC(ADDRESS(codeCurrent));
  END; (* LOOP *)

  IF (found) THEN
    firstChar := firstPtr - buf;
  END;
  RETURN (found);
END FindStrInLine;

PROCEDURE FindTextStr(VAR vw: ViewerWindow; searchStr: ADDRESS);
VAR
  line: CARDINAL;
  firstChar: CARDINAL;
  first, last: CARDINAL;
BEGIN
  WITH vw DO
    IF (vwFindShow) THEN
      vwFindShow := FALSE;
      GetDisplayRangeVTWindow(vwVTWindow, first, last);
      IF (vwFindLine >= first) AND (vwFindLine <= last) THEN
        RefreshRangeVTWindow(vwVTWindow, vwFindLine - first, vwFindLine - first);
      END;
    END;

    IF (CHAR(searchStr^) = 0C) THEN RETURN; END;

    line := 0;
    WHILE (line < vwTotalLines) AND
          (NOT FindStrInLine(vwLineBuf^[line], searchStr, firstChar)) DO
      INC(line);
    END;

    IF (line < vwTotalLines) THEN (* Found! *)
      vwFindShow  := TRUE;
      vwFindLine  := line;
      vwFindFChar := firstChar;
      vwFindLChar := firstChar + CARDINAL(strlen(searchStr) - 1);

      ScrollVTWindow(vwVTWindow, firstChar, line);
      RefreshRangeVTWindow(vwVTWindow, 0, 0);
    END;
  END;
END FindTextStr;

PROCEDURE TextWinUpdateProc(VT: VTWindow; OutputRow: CARDINAL;
                            FirstLine, LastLine: CARDINAL);
VAR
  vw: ViewerWindowPtr;
  line: CARDINAL;
  curChar: CARDINAL;
  cp: POINTER TO CHAR;
BEGIN
  vw := GetUserDataVTWindow(VT);
  WITH vw^ DO
    IF (vwDispOn) THEN
      FOR line := FirstLine TO LastLine DO
        SetCursorPosVTWindow(VT, 0, OutputRow);

        IF (vwFindShow) AND (vwFindLine = line) THEN
          cp := vwLineBuf^[line];
          curChar := 0;
          WHILE (curChar # vwFindFChar) DO
            WriteCharVTWindow(VT, cp^);
            INC(ADDRESS(cp));
            INC(curChar);
          END;

          SetColorVTWindow(VT, TextWinFindFgColor, TextWinFindBgColor);
          WHILE (curChar <= vwFindLChar) DO
            WriteCharVTWindow(VT, cp^);
            INC(ADDRESS(cp));
            INC(curChar);
          END;
          SetColorVTWindow(VT, TextWinDefFgColor, TextWinDefBgColor);

          WHILE (cp^ # 0C) DO
            WriteCharVTWindow(VT, cp^);
            INC(ADDRESS(cp));
          END;
        ELSE
          WriteStrVTWindow(VT, vwLineBuf^[line]);
        END;
        INC(OutputRow);
      END;
    END;
  END;
END TextWinUpdateProc;

PROCEDURE TextWinSelectProc(VT: VTWindow; x, y: CARDINAL; pressed: BOOLEAN);
END TextWinSelectProc;

PROCEDURE TextWinMenuProc(VT: VTWindow; MenuNum, ItemNum, SubNum: CARDINAL);
VAR
  vw: ViewerWindowPtr;
BEGIN
  vw := GetUserDataVTWindow(VT);
  IF NOT GlobalMenuHandler(MenuNum, ItemNum, SubNum) THEN
    CASE (ItemNum) OF
    | 0: FindTextStr(vw^, vSearchStrBuf);
    | 1: CloseTextData(vw^);
    END; (* CASE *)
  END; (* IF *)
END TextWinMenuProc;

PROCEDURE TextWinDestProc(VT: VTWindow);
VAR
  fileSize: LONGCARD;
  filePath: FileNameStr;
  vw: ViewerWindowPtr;
BEGIN
  vw := GetUserDataVTWindow(VT);
  IF GetItemSelected(ADR(filePath[0]), fileSize) THEN
    OpenTextData(vw^, ADR(filePath[0]), fileSize);
  END;
END TextWinDestProc;

PROCEDURE TextWinKeyProc(VT: VTWindow; bufPtr: ADDRESS; bufLength: CARDINAL);
BEGIN
  printf("TextWinKeyProc:");
  WHILE (bufLength # 0) DO
    argo[0].W := CARDINAL(bufPtr^);
    printf("[%u]");
    INC(bufPtr);
    DEC(bufLength);
  END;
  printf("\n");
END TextWinKeyProc;

PROCEDURE TextWinCloseProc(VT: VTWindow);
VAR
  vw: ViewerWindowPtr;
BEGIN
  vw := GetUserDataVTWindow(VT);
  CloseViewerWindow(vw^);
END TextWinCloseProc;

PROCEDURE TextWinErrorProc(VT: VTWindow);
VAR
  vw: ViewerWindowPtr;
BEGIN
  vw := GetUserDataVTWindow(VT);
  CloseViewerWindow(vw^);
END TextWinErrorProc;

PROCEDURE DirWinUpdateProc(VT: VTWindow; OutputRow: CARDINAL;
                            FirstLine, LastLine: CARDINAL);
VAR
  vw: ViewerWindowPtr;
  line: CARDINAL;
  r: CARDINAL;
  do: DirObjPtr;
  outStr: ARRAY [0..79] OF CHAR;
  i: CARDINAL;
BEGIN
  vw := GetUserDataVTWindow(VT);
  WITH vw^ DO
    IF (vwDispOn) THEN
      FOR line := FirstLine TO LastLine DO
        do := FindDirObj(vw^, line);
        WITH do^ DO
          IF IsItemSelected(VT, line) THEN
            SetColorVTWindow(VT, DirWinSelFgColor, DirWinSelBgColor);
          END;
          SetCursorPosVTWindow(VT, 0, OutputRow);
          FOR i := 1 TO DirWinCharWIDTH DO
            WriteCharVTWindow(VT, " ");
          END;
          SetCursorPosVTWindow(VT, DirWinNamePOS, OutputRow);
          WriteStrVTWindow(VT, ADR(doName[0]));
          SetCursorPosVTWindow(VT, DirWinSizePOS, OutputRow);
          IF (doFileType) THEN
            argo[0].L := doSize;
            r := sprintf(ADR(outStr[0]), ADR("%lu"), argo);
            WriteStrVTWindow(VT, ADR(outStr[0]));
          ELSE
            WriteStrVTWindow(VT, ADR("Dir"));
          END;
          SetColorVTWindow(VT, DirWinDefFgColor, DirWinDefBgColor);
        END;
        INC(OutputRow);
      END;
    END;
  END;
END DirWinUpdateProc;

PROCEDURE DirWinSelectProc(VT: VTWindow; x, y: CARDINAL; pressed: BOOLEAN);
VAR
  vw: ViewerWindowPtr;
  do: DirObjPtr;
  newPath: FileNameStr;
  p: ADDRESS;
  d: ADDRESS;
  l: CARDINAL;
BEGIN
  vw := GetUserDataVTWindow(VT);
  IF (pressed) THEN
    IF (IsItemSelected(VT, y)) THEN
      ClearItemSelected();
      do := FindDirObj(vw^, y);
      IF (NOT do^.doFileType) THEN
        strcpy(ADR(newPath[0]), ADR(vw^.vwDirPath[0]));
        l := strlen(ADR(newPath[0]));
        IF (l = 0) OR ((newPath[l-1] # ":") AND (newPath[l-1] # "/")) THEN
          strcat(ADR(newPath[0]), ADR("/"));
        END;
        strcat(ADR(newPath[0]), ADR(do^.doName[0]));
        OpenDirData(vw^, ADR(newPath[0]));
      END;
    ELSE
      NewItemSelected(VT, y);
    END;
  ELSE
    IF (NOT IsItemSelected(VT, y)) THEN
      NewItemSelected(VT, y);
    END;
  END;
END DirWinSelectProc;

PROCEDURE DirWinMenuProc(VT: VTWindow; MenuNum, ItemNum, SubNum: CARDINAL);
VAR
  vw: ViewerWindowPtr;
BEGIN
  vw := GetUserDataVTWindow(VT);
  IF NOT GlobalMenuHandler(MenuNum, ItemNum, SubNum) THEN
    CASE (ItemNum) OF
    | 0: OpenDirData(vw^, vDirPathStrBuf);
    | 1: CloseDirData(vw^);
    END;
  END;
END DirWinMenuProc;

PROCEDURE DirWinDestProc(VT: VTWindow);
END DirWinDestProc;

PROCEDURE DirWinKeyProc(VT: VTWindow; bufPtr: ADDRESS; bufLength: CARDINAL);
BEGIN
  printf("DirWinKeyProc:");
  WHILE (bufLength # 0) DO
    argo[0].W := CARDINAL(bufPtr^);
    printf("[%u]");
    INC(bufPtr);
    DEC(bufLength);
  END;
  printf("\n");
END DirWinKeyProc;

PROCEDURE DirWinCloseProc(VT: VTWindow);
VAR
  vw: ViewerWindowPtr;
BEGIN
  vw := GetUserDataVTWindow(VT);
  CloseViewerWindow(vw^);
END DirWinCloseProc;

PROCEDURE DirWinErrorProc(VT: VTWindow);
VAR
  vw: ViewerWindowPtr;
BEGIN
  vw := GetUserDataVTWindow(VT);
  CloseViewerWindow(vw^);
END DirWinErrorProc;

PROCEDURE AddMenuItemWithKey(name: ADDRESS; key: CHAR);
BEGIN
  AddMenuItem(name);
  MenuItemOpt(DefaultItemFlags + MenuItemFlagsSet{CommSeq},
              MenuItemMutualExcludeSet{}, key);
END AddMenuItemWithKey;

PROCEDURE Init();
VAR
  si: StringInfoPtr;
  b: BOOLEAN;
BEGIN
  b := InitDBMisc();
  LOOP
    vVTWPort := CreatePort(NIL, 0);
    IF (vVTWPort = NIL) THEN EXIT; END;

    vScreen := CreateScreen(ScrWIDTH, ScrHEIGHT, ScrDEPTH, ADR(ScrTITLE));
    IF (vScreen = NIL) THEN EXIT; END;

    BeginMenuStrip();
      AddMenu(ADR("Project "));
        AddMenuItemWithKey(ADR("Memory "), "M");
        AddMenuItemWithKey(ADR("Quit "), "Q");
      AddMenu(ADR("Windows "));
        AddMenuItemWithKey(ADR("Directory "), "1");
        AddMenuItemWithKey(ADR("Text "), "2");
    vStatusMenu := EndMenuStrip();
    IF (vStatusMenu = NIL) THEN EXIT; END;

    BeginMenuStrip();
      AddMenu(ADR("Project "));
        AddMenuItemWithKey(ADR("Memory "), "M");
        AddMenuItemWithKey(ADR("Quit "), "Q");
      AddMenu(ADR("Windows "));
        AddMenuItemWithKey(ADR("Directory "), "1");
        AddMenuItemWithKey(ADR("Text "), "2");
      AddMenu(ADR("Directory "));
        AddMenuItemWithKey(ADR("Get Directory Path "), "G");
        AddMenuItemWithKey(ADR("Clear Directory "), "D");
    vDirMenu := EndMenuStrip();
    IF (vDirMenu = NIL) THEN EXIT; END;

    BeginMenuStrip();
      AddMenu(ADR("Project "));
        AddMenuItemWithKey(ADR("Memory "), "M");
        AddMenuItemWithKey(ADR("Quit "), "Q");
      AddMenu(ADR("Windows "));
        AddMenuItemWithKey(ADR("Directory "), "1");
        AddMenuItemWithKey(ADR("Text "), "2");
      AddMenu(ADR("Text "));
        AddMenuItemWithKey(ADR("Find String "), "F");
        AddMenuItemWithKey(ADR("Clear Text "), "T");
    vTextMenu := EndMenuStrip();
    IF (vTextMenu = NIL) THEN EXIT; END;

    BeginGadgetList();
      AddGadgetString(8, 23, 32, MaxFileNameStr, NIL);
      IF (LastGadget # NIL) THEN
        si := LastGadget^.SpecialInfo;
        vDirPathStrBuf := si^.Buffer;
      END;
      AddGadgetString(8, 48, 32, MaxFindStr, NIL);
      IF (LastGadget # NIL) THEN
        si := LastGadget^.SpecialInfo;
        vSearchStrBuf := si^.Buffer;
      END;
    vStatusGads := EndGadgetList();
    IF (vStatusGads = NIL) THEN EXIT; END;    

    vStatusWindow := CreateWindow(SWinX, SWinY, SWinWIDTH, SWinHEIGHT,
                                  SWinIDCMP, SWinFLAGS, vStatusGads,
                                  vScreen, ADR(SWinTITLE));
    IF (vStatusWindow = NIL) THEN EXIT; END;

    SetMenuStrip(vStatusWindow^, vStatusMenu^);

    vStatusWindowRP := vStatusWindow^.RPort;

    Move(vStatusWindowRP^, 8, 18);
    Text(vStatusWindowRP^, ADR("Directory Path:"), 15);
    Move(vStatusWindowRP^, 8, 43);
    Text(vStatusWindowRP^, ADR("Search String:"), 14);

    vVTWInit := InitVirtualTextWindows(vScreen, MaxOutputWIDTH);
    IF (NOT vVTWInit) THEN EXIT; END;

    vVTWPortSig  := CARDINAL(vVTWPort^.mpSigBit);
    vSWinPortSig := CARDINAL(vStatusWindow^.UserPort^.mpSigBit);
    vWaitMask    := SignalSet{vVTWPortSig, vSWinPortSig};

    WITH vTextVTWParms DO
      vtpUpdateProc  := TextWinUpdateProc;
      vtpSelectProc  := TextWinSelectProc;
      vtpMenuProc    := TextWinMenuProc;
      vtpDestProc    := TextWinDestProc;
      vtpKeyProc     := TextWinKeyProc;
      vtpCloseProc   := TextWinCloseProc;
      vtpErrorProc   := TextWinErrorProc;
      vtpDefFgColor  := BYTE(TextWinDefFgColor);
      vtpDefBgColor  := BYTE(TextWinDefBgColor);
    END;

    WITH vDirVTWParms DO
      vtpUpdateProc  := DirWinUpdateProc;
      vtpSelectProc  := DirWinSelectProc;
      vtpMenuProc    := DirWinMenuProc;
      vtpDestProc    := DirWinDestProc;
      vtpKeyProc     := DirWinKeyProc;
      vtpCloseProc   := DirWinCloseProc;
      vtpErrorProc   := DirWinErrorProc;
      vtpDefFgColor  := BYTE(DirWinDefFgColor);
      vtpDefBgColor  := BYTE(DirWinDefBgColor);
    END;

    RETURN; (* Success! *)
  END; (* LOOP *)
  Abort();
END Init;

BEGIN (* main *)
  printf(MsgTitle);
  Init();
  MainLoop();
  Cleanup();
END Viewer.
