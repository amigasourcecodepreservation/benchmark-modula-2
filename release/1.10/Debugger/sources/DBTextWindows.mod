(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1988 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBTextWindows.MOD                 Version: Amiga.01.10             *
 * Created: 12/29/88   Updated: 03/21/94   Author: Leon Frenkel             *
 * Description: Debugger virtual text windows.                              *
 ****************************************************************************)

IMPLEMENTATION MODULE DBTextWindows;

(***************************************************************************
 * Changes for new release 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.2 -----
 * 
 * Support for Clipboard
 * 
 *   1. New proc: ClipVTWindow(VT)
 * 
 *   2. Open IFFParse library in InitVTTextWindows() if OSVersion2.
 *      InitVTTextWindows() still returns TRUE if the Openlibrary failed,
 *      but clipping won't be available.
 * 
 *   3. Module global var iff for IFF handle.
 * 
 *   4. Procs AccessClip(rwMode:LONGINT):BOOLEAN and UnaccessClip() as in
 *      M2Ed, random.c.
 * 
 *   5. Cleanup routine closes iff, closes clipboard, frees iff, and closes
 *      IFFParse library.
 * 
 ***************************************************************************
*)

FROM SYSTEM IMPORT
  ADDRESS, BYTE,
  ADR;
FROM CMemOp IMPORT
  movmem, setmem;
FROM CStrings IMPORT
  strlen;
FROM Drawing IMPORT
  RectFill, SetAPen, SetBPen;
FROM DBEnvironmentInquiry IMPORT
  OSVersion2;
FROM DBMisc IMPORT
  AllocMemory, DeallocMemory,
  DeadKeyConvert;
FROM IFFParse IMPORT
  AllocIFF, ClipboardHandlePtr, CloseClipboard, CloseIFF,
  FreeIFF, IDFORM, IFFFlagsType, IFFHandle, IFFHandlePtr, IFFParseBase,
  IFFParseName, IFFSizeUnknown, IFFWrite, InitIFFasClip, OpenClipboard,
  OpenIFF, PopChunk, PushChunk, WriteChunkBytes;
FROM InputEvents IMPORT
  IEQualifier, IEQualifierSet;
FROM Interrupts IMPORT
  Forbid, Permit;
FROM Intuition IMPORT
  Screen, ScreenPtr, WindowPtr, IntuiMessagePtr, IntuiMessage,
  Image, NewWindow, GadgetPtr, PropInfoPtr, MenuItemPtr, Menu,
  WindowFlags, WindowFlagsSet, IDCMPFlags, IDCMPFlagsSet, MenuNull,
  ScreenFlagsSet, WBenchScreen, CustomScreen, SmartRefresh, SelectDown, 
  SelectUp,
  GadgetFlags, GadgetFlagsSet, GadgetActivation, GadgetActivationSet,
  OpenWindow, CloseWindow, RemoveGList, ModifyIDCMP, OpenWorkBench,
  NewModifyProp, ItemAddress, MENUNUM, ITEMNUM, SUBNUM,
  SetWindowTitles, SetMenuStrip, ClearMenuStrip, RefreshWindowFrame;
FROM Libraries IMPORT
  OpenLibrary, CloseLibrary;
FROM Lists IMPORT
  Remove;
FROM Memory IMPORT
  MemReqSet, MemChip, MemClear,
  AllocMem, FreeMem;
FROM Ports IMPORT
  ReplyMsg;
FROM Rasters IMPORT
  ScrollRaster;
FROM SimpleGadgets IMPORT
  DefaultGadgetFlags, DefaultGadgetActivation, LastGadget,
  GlobalGadgetOpt,
  BeginGadgetList, EndGadgetList,
  AddGadgetProp, AddGadgetImageButton,
  FreeGadgetList;
FROM XText IMPORT
  XTextFlags, XTextFlagsSet,
  XTextSupportPtr,
  XText;
FROM XTextUtil IMPORT
  MakeXTextSupport, UnmakeXTextSupport;

CONST IDFTXT  =  LONGINT('F') *16777216D
                +LONGINT('T') *65536D
                +LONGINT('X') *256D
                +LONGINT('T');

      IDCHRS  =  LONGINT('C') *16777216D
                +LONGINT('H') *65536D
                +LONGINT('R') *256D
                +LONGINT('S');

TYPE
  (* Window text buffer *)
  VTCharBufPtr = POINTER TO VTCharBuf;
  VTCharBuf = ARRAY [0..32765] OF CHAR;

  (* Window color buffer *)
  VTColorBufPtr = POINTER TO VTColorBuf;
  VTColorBuf = ARRAY [0..32765] OF CHAR;

  (* Window text, color buffer line info *)
  VTLineArrayPtr = POINTER TO VTLineArray;
  VTLineRecord   = RECORD
                     vtlIndex   : CARDINAL;
                     vtlChanged : BOOLEAN;
                     vtlPad     : BYTE;
                   END;
  VTLineArray    = ARRAY [0..255] OF VTLineRecord;

  (* Virtual text window info record *)
  VTWindow = POINTER TO VTWindowRecord; (* Opaque type definition *)
  VTWindowRecord =
     RECORD
       vtNext             : VTWindow;         (* Pointer to next window *)
       vtIntuiWindow      : WindowPtr;        (* Intuition window *)
       vtGadList          : GadgetPtr;        (* Window gadget list *)
       vtHPropGad         : GadgetPtr;        (* Ptr to H-Prop gadget *)
       vtVPropGad         : GadgetPtr;        (* Ptr to V-Prop gadget *)
       vtCharBuf          : VTCharBufPtr;     (* Character buffer *)
       vtColorBuf         : VTColorBufPtr;    (* Color buffer *)
       vtLineArray        : VTLineArrayPtr;   (* Line info array *)
       vtTextWidth        : CARDINAL;         (* width < MaxColumns *)
       vtTextHeight       : CARDINAL;         (* height < TotalLines *)
       vtStartColumn      : CARDINAL;         (* current display column *)
       vtVirtualLine      : CARDINAL;         (* Top line virtual line *)
       vtCursorX          : CARDINAL;         (* Current cursor x pos *)
       vtCursorY          : CARDINAL;         (* Current cursor y pos *)
       vtUpdateProc       : VTUpdateProcType; (* window update proc *)
       vtSelectProc       : VTSelectProcType; (* mouse select proc *)
       vtMenuProc         : VTMenuProcType;   (* menu select proc *)
       vtDestProc         : VTDestProcType;   (* dest-gadget proc *)
       vtKeyProc          : VTKeyProcType;    (* kbd proc *)
       vtCloseProc        : VTCloseProcType;  (* close-box proc *)
       vtErrorProc        : VTErrorProcType;  (* alloc-error proc *)
       vtTotalLines       : CARDINAL;         (* total virtual lines *)
       vtMaxColumns       : CARDINAL;         (* maximum # columns *)
       vtUserData         : ADDRESS;          (* (Optional) Ptr to user data *)
       vtTextFgBgColor    : CHAR;             (* current fg/bg color *)
       vtDefTextFgBgColor : CHAR;             (* default fg/bg color *)
       vtSizeChanging     : BOOLEAN;          (* T: window resizing, F: normal*)
       vtScrollRedrawAll  : BOOLEAN;          (* TRUE = all, FALSE = partial *)
       vtQualifier        : IEQualifierSet;   (* Qualifier from last msg *)
     END;

CONST
  (* Default Window Flags Set *)
  DefWindowFlags = WindowFlagsSet{WindowClose, WindowDrag, WindowDepth, 
                          NoCareRefresh, WindowSizing, Activate} + SmartRefresh;
  (* Default IDCMP Flags Set *)
  DefIDCMPFlags  = IDCMPFlagsSet{Closewindow, MenuPick, NewSize, GadgetUp,
                                 SizeVerify, GadgetDown, MouseButtons, RawKey};
  (* Alternate IDCMP Flags Set (used after GadgetDown) *)
  AltIDCMPFlags  = DefIDCMPFlags + IDCMPFlagsSet{IntuiTicks};

  (* Adjustment to determine the visible part of the window's title-bar *)
  TitleBarWidthAdj = 84;

  (* These fg/bg colors are used by default. *)
  DefTextFgBgCOLOR   = CHAR(010H);

  (* Font size based on topaz-80 font *)
  DefCharWidth  = 8;
  DefCharHeight = 8;

  (* Text rendering begins at these coordinates *)
  DefTextStartX = 2;
  DefTextStartY = 10;

  (* Text rendering ends at these coordinates -relative to border *)
  DefTextEndX = 16;
  DefTextEndY = 9;

  (* Window size minus the adjustment equals drawing area *)
  DefTextSizeXAdj = DefTextStartX + DefTextEndX;
  DefTextSizeYAdj = DefTextStartY + DefTextEndY;

  (* Window gadget IDs *)
  GIDVProp      = 0;
  GIDUpArrow    = 1;
  GIDDownArrow  = 2;
  GIDHProp      = 3;
  GIDLeftArrow  = 4;
  GIDRightArrow = 5;
  GIDDestWindow = 6;

TYPE
  (* Arrow Gadget Imagery Data Definitions *)
  ImageDataPtr = POINTER TO ImageData;
  ImageData = ARRAY [0..8] OF CARDINAL;

VAR
  (* pointers to imagery for arrow gadgets allocated in CHIP ram *)
  UpArrowData    : ImageDataPtr;
  DownArrowData  : ImageDataPtr;
  LeftArrowData  : ImageDataPtr;
  RightArrowData : ImageDataPtr;
  DestButtonData : ImageDataPtr;

  (* Image structures used for arrow gadgets *)
  UpArrowImage    : Image;
  DownArrowImage  : Image;
  LeftArrowImage  : Image;
  RightArrowImage : Image;
  DestButtonImage : Image;

  VTWScreen       : ScreenPtr;       (* pointer to screen used for windows *)
  VTWScreenType   : ScreenFlagsSet;  (* screen flags *)
  VTWindowList    : VTWindow;        (* list of currently open windows *)
  XTextS          : XTextSupportPtr; (* XText support structure *)

  ActiveGadget : GadgetPtr; (* Current gadget used for continuos scrolling *)

  PrevXPos     : CARDINAL; (* previous x pos for button down *)
  PrevYPos     : CARDINAL; (* previous y pos for button down *)
  SelectIsDown : BOOLEAN;  (* T:select button has been pressed, F: Not *)

  iff          : IFFHandlePtr;
  IFFisOpen    : BOOLEAN;

(* ====== PRIVATE ======= *)

(*----------------------------------------------------------------------*)
 PROCEDURE AccessClip(VAR iff:IFFHandlePtr; rwMode:IFFFlagsType):BOOLEAN;
(*----------------------------------------------------------------------*)
(* AccessClip - prepare to put stream of chars to the clipboard *)

VAR error :LONGINT;

BEGIN

iff^.iffStream := LONGCARD(OpenClipboard(0));
IF iff^.iffStream = 0D THEN
   RETURN FALSE;
END;

InitIFFasClip(iff^);

error := OpenIFF(iff^, rwMode);
IF error # 0D THEN
   CloseClipboard(ClipboardHandlePtr(iff^.iffStream));
   iff^.iffStream := 0D;
   RETURN FALSE;
END;

RETURN TRUE;

END AccessClip;

(*-------------------------------------------*)
 PROCEDURE UnaccessClip(VAR iff:IFFHandlePtr);
(*-------------------------------------------*)
(* UnaccessClip - finished putting the clip *)
BEGIN

CloseIFF(iff^);

CloseClipboard(ClipboardHandlePtr(iff^.iffStream));
iff^.iffStream := 0D;

END UnaccessClip;

(* IsWindowOpen - check to see if window handle is valid *)
PROCEDURE IsWindowOpen(VT: VTWindow): BOOLEAN;
VAR
  list: VTWindow;
BEGIN
  list := VTWindowList;
  WHILE (list # NIL) DO
    IF (list = VT) THEN
      RETURN (TRUE); (* Found! *)
    END;
    list := list^.vtNext;
  END;
  RETURN (FALSE); (* Not open! *)
END IsWindowOpen;

(* LinkWindowNode - link window into linked list *)
PROCEDURE LinkWindowNode(VT: VTWindow);
BEGIN
  IF (VTWindowList # NIL) THEN
    VT^.vtNext := VTWindowList;
  END;
  VTWindowList := VT;
END LinkWindowNode;

(* UnLinkWindowNode - unlink window from linked list *)
PROCEDURE UnLinkWindowNode(VT: VTWindow);
VAR
  prev: VTWindow;
  cur: VTWindow;
BEGIN
  prev := NIL;
  cur  := VTWindowList;
  WHILE (cur # VT) DO
    prev := cur;
    cur  := cur^.vtNext;
  END;

  IF (prev = NIL) THEN (* First in list *)
    VTWindowList := VT^.vtNext;
  ELSE (* Not first in list *)
    prev^.vtNext := VT^.vtNext;
  END;
END UnLinkWindowNode;

(* SetupVPropGadget - setup vertical prop gadget *)
PROCEDURE SetupVPropGadget(VT: VTWindow);
VAR
  body: CARDINAL;
  pot: CARDINAL;
  propInfo: PropInfoPtr;
BEGIN
  WITH VT^ DO
    IF (vtTextHeight >= vtTotalLines) THEN (* data lines <= display lines *)
      body := 0FFFFH;
      pot  := 0;
    ELSE (* data lines > display lines *)
      body := (LONGCARD(vtTextHeight) * 65536D) DIV LONGCARD(vtTotalLines);
      pot  := (LONGCARD(vtVirtualLine) * 65535D) DIV
              LONGCARD(vtTotalLines - vtTextHeight);
    END;
    propInfo := vtVPropGad^.SpecialInfo;
    NewModifyProp(vtVPropGad^, vtIntuiWindow^, NIL,
                  propInfo^.Flags, 0, pot, 0, body, 1);
  END;
END SetupVPropGadget;

(* SetupHPropGadget - setup horizontal prop gadget *)
PROCEDURE SetupHPropGadget(VT: VTWindow);
VAR
  body: CARDINAL;
  pot: CARDINAL;
  propInfo: PropInfoPtr;
BEGIN
  WITH VT^ DO
    IF (vtTextWidth >= vtMaxColumns) THEN (* data cols <= display columns *)
      body := 0FFFFH;
      pot  := 0;
    ELSE (* data cols > display columns *)
      body := (LONGCARD(vtTextWidth) * 65536D) DIV LONGCARD(vtMaxColumns);
      pot  := (LONGCARD(vtStartColumn) * 65535D) DIV
              LONGCARD(vtMaxColumns - vtTextWidth);
    END;
    propInfo := vtHPropGad^.SpecialInfo;
    NewModifyProp(vtHPropGad^, vtIntuiWindow^, NIL,
                  propInfo^.Flags, pot, 0, body, 0, 1);
  END;
END SetupHPropGadget;

(* CalcTextSize - calculate text window size *)
PROCEDURE CalcTextSize(VT: VTWindow);
BEGIN
  WITH VT^ DO
    vtTextWidth := (vtIntuiWindow^.Width-DefTextSizeXAdj) DIV DefCharWidth;
    IF (vtTextWidth > vtMaxColumns) THEN
      vtTextWidth := vtMaxColumns;
    END;
    vtTextHeight := (vtIntuiWindow^.Height-DefTextSizeYAdj) DIV DefCharHeight;
    IF (vtTextHeight > vtTotalLines) THEN
      vtTextHeight := vtTotalLines;
    END;
  END;
END CalcTextSize;

(* FillEmptyRegion - fill in unused part of window with background color *)
PROCEDURE FillEmptyRegion(VT: VTWindow);
VAR
  Xmin, Ymin, Xmax, Ymax: INTEGER;
BEGIN
  WITH VT^ DO
    IF (NOT vtSizeChanging) THEN (* Not resizing window *)
      (* Fill in area below text all the way across window *)
      Xmin := DefTextStartX;
      Ymin := DefTextStartY + (vtTextHeight * DefCharHeight);
      Xmax := vtIntuiWindow^.Width - DefTextEndX - 1;
      Ymax := vtIntuiWindow^.Height - DefTextEndY - 1;
      IF (Ymin <= Ymax) THEN
        RectFill(vtIntuiWindow^.RPort^, Xmin, Ymin, Xmax, Ymax);
      END;
  
      (* Fill in area to the right of text down to last line of text *)
      Xmin := DefTextStartX + (vtTextWidth * DefCharWidth);
      Ymin := DefTextStartY;
      Xmax := vtIntuiWindow^.Width - DefTextEndX - 1;
      Ymax := DefTextStartY + (vtTextHeight * DefCharHeight) - 1;
      IF (Xmin <= Xmax) THEN
        RectFill(vtIntuiWindow^.RPort^, Xmin, Ymin, Xmax, Ymax);
      END;
    END; 
  END;
END FillEmptyRegion;

(* DeallocTextInfo - free text related buffers if allocated *)
PROCEDURE DeallocTextInfo(VT: VTWindow);
BEGIN
  WITH VT^ DO
    IF (vtCharBuf # NIL) THEN DeallocMemory(vtCharBuf); END;
    IF (vtColorBuf # NIL) THEN DeallocMemory(vtColorBuf) END;
    IF (vtLineArray # NIL) THEN DeallocMemory(vtLineArray) END;
    vtCharBuf   := NIL;
    vtColorBuf  := NIL;
    vtLineArray := NIL;
  END;
END DeallocTextInfo;

(* AllocTextInfo - allocate text related buf according to current window size *)
PROCEDURE AllocTextInfo(VT: VTWindow): BOOLEAN;
VAR
  y: CARDINAL;
  idx: CARDINAL;
BEGIN
  DeallocTextInfo(VT);

  CalcTextSize(VT);
  WITH VT^ DO
    vtCharBuf   := AllocMemory(vtMaxColumns * vtTextHeight, MemReqSet{});
    vtColorBuf  := AllocMemory(vtMaxColumns * vtTextHeight, MemReqSet{});
    vtLineArray := AllocMemory(vtTextHeight * SIZE(VTLineRecord), MemReqSet{MemClear});
    IF (vtCharBuf = NIL) OR (vtColorBuf = NIL) OR (vtLineArray = NIL) THEN
       DeallocTextInfo(VT);
       RETURN (FALSE); (* err *)
    END;

    idx := 0;
    FOR y := 0 TO vtTextHeight-1 DO
      vtLineArray^[y].vtlIndex := idx;
      INC(idx, vtMaxColumns);
    END;
  END;

  ClearVTWindow(VT);

  RETURN (TRUE);
END AllocTextInfo;

(* ScrollTextInfo - scroll text around char buffer *)
PROCEDURE ScrollTextInfo(VT: VTWindow; from, to, count: CARDINAL);
VAR
  clrSize, copySize: CARDINAL;
  fromIdx, toIdx, clrIdx: CARDINAL;
  clr: CARDINAL;
BEGIN
  WITH VT^ DO
    copySize := vtMaxColumns * count;
    clrSize  := vtMaxColumns * (vtTextHeight - count);
    IF (from < to) THEN
      clr := from;
    ELSE
      clr := count;
    END;
    clrIdx   := vtLineArray^[clr].vtlIndex;
    fromIdx  := vtLineArray^[from].vtlIndex;
    toIdx    := vtLineArray^[to].vtlIndex;

    (* movmem() handles overlaping memory block correctly! *)
    movmem(ADR(vtCharBuf^[fromIdx]), ADR(vtCharBuf^[toIdx]), copySize);
    movmem(ADR(vtColorBuf^[fromIdx]), ADR(vtColorBuf^[toIdx]), copySize);
    
    (* clear rows which text has been scrolled out from *)
    setmem(ADR(vtCharBuf^[clrIdx]), clrSize, " ");
    setmem(ADR(vtColorBuf^[clrIdx]), clrSize, vtDefTextFgBgColor);
  END;
END ScrollTextInfo;

(* MarkAllLinesChanged - mark all lines as changed *)
PROCEDURE MarkAllLinesChanged(VT: VTWindow);
VAR
  row: CARDINAL;
BEGIN
  WITH VT^ DO
    FOR row := 0 TO vtTextHeight - 1 DO
      vtLineArray^[row].vtlChanged := TRUE;
    END;
  END;
END MarkAllLinesChanged;

(* RenderTextLines - update window from character buffer *)
PROCEDURE RenderTextLines(VT: VTWindow; first, last: CARDINAL;
                          startx, widthx: CARDINAL);
VAR
  row: CARDINAL;
  width: CARDINAL;
  x: CARDINAL;
  cstart: CARDINAL;
  clen: CARDINAL;
  curColors: CHAR;
  i: CARDINAL;
BEGIN
  WITH VT^ DO
    IF (NOT vtSizeChanging) THEN (* Not resizing window *)
      XTextS^.OutputRPort := vtIntuiWindow^.RPort;
      FOR row := first TO last DO
        IF (vtLineArray^[row].vtlChanged) THEN
          vtLineArray^[row].vtlChanged := FALSE;
          x := DefTextStartX + ((startx - vtStartColumn) * DefCharWidth);
          cstart := vtLineArray^[row].vtlIndex + startx;
          width  := cstart + widthx;
          REPEAT
            curColors := vtColorBuf^[cstart];
            clen := 0;
            i := cstart;
            WHILE (i < width) AND (vtColorBuf^[i] = curColors) DO
              INC(i);
              INC(clen);
            END;
    
            WITH XTextS^ DO
              FrontPen := BYTE(CARDINAL(curColors) DIV 16);
              BackPen  := BYTE(CARDINAL(curColors) MOD 16);
            END;
    
            XText(XTextS^, ADR(vtCharBuf^[cstart]), clen, x,
                 (row * DefCharHeight) + DefTextStartY);
            INC(cstart, clen);
            INC(x, clen * DefCharWidth);
          UNTIL (cstart >= width);
        END; (* IF *)
      END; (* FOR *)
    END; (* IF *)
  END; (* WITH *)
END RenderTextLines;

(*----------------------------------------------------------*)
 PROCEDURE ClipTextLines(VT: VTWindow; first, last: CARDINAL);
(*----------------------------------------------------------*)
(* ClipTextLines - clip window from character buffer *)

VAR  row    :CARDINAL;
     width  :CARDINAL;
     cstart :CARDINAL;
     lires  :LONGINT;

BEGIN WITH VT^ DO

IF PushChunk(iff^, IDFTXT, IDFORM, IFFSizeUnknown) = 0D THEN
   IF PushChunk(iff^, 0, IDCHRS, IFFSizeUnknown) = 0D THEN
      FOR row := first TO last DO

         cstart := vtLineArray^[row].vtlIndex;

         width := vtTextWidth;
         (* don't output trailing blanks *)
         WHILE (width > 0) AND (vtCharBuf^[cstart+width-1] = ' ') DO
            DEC(width);
         END;

         IF width > 0 THEN
            lires := WriteChunkBytes(iff^, ADR(vtCharBuf^[cstart]), width);
         END;
         lires := WriteChunkBytes(iff^, ADR(12C), 1);  (* end of line *)

      END;
      lires := PopChunk(iff^);
   END;
   lires := PopChunk(iff^);
END;

END END ClipTextLines;

(* ScrollHorizVTWindow - scroll text horizontally *)
PROCEDURE ScrollHorizVTWindow(VT: VTWindow; offset: INTEGER; chgGad: BOOLEAN);
VAR
  row: CARDINAL;
  xmax, ymax: INTEGER;
  notVisibleCols: CARDINAL;
BEGIN
  WITH VT^ DO
    xmax := DefTextStartX + (vtTextWidth * DefCharWidth) - 1;
    ymax := DefTextStartY + (vtTextHeight * DefCharHeight) - 1;

    IF (offset < 0) THEN
      IF (vtStartColumn = 0) THEN
        RETURN; (* no change *)
      END;
      IF (CARDINAL(ABS(offset)) > vtStartColumn) THEN
        offset := - INTEGER(vtStartColumn);
      END;
      INC(vtStartColumn, offset);

      MarkAllLinesChanged(VT);

      RenderTextLines(VT, 0, vtTextHeight - 1, vtStartColumn, vtTextWidth);
(*********************** Another variation which uses ScrollRaster ********
      IF (CARDINAL(ABS(offset)) >= vtTextWidth) THEN (* Refresh entire window *)
        RenderTextLines(VT, 0, vtTextHeight - 1, vtStartColumn, vtTextWidth);
      ELSE (* Scroll text *)
        IF (NOT vtSizeChanging) THEN (* Not resizing window *)
          ScrollRaster(vtIntuiWindow^.RPort^, offset * DefCharWidth, 0,
                       DefTextStartX, DefTextStartY, xmax, ymax);
        END;
        RenderTextLines(VT, 0, vtTextHeight -1, vtStartColumn, ABS(offset));
      END;
***************************************************************************)
    ELSIF (offset > 0) THEN
      IF (vtStartColumn + vtTextWidth >= vtMaxColumns) THEN
        RETURN; (* no change *)
      END;
      notVisibleCols := vtMaxColumns - (vtStartColumn + vtTextWidth);
      IF (CARDINAL(offset) > notVisibleCols) THEN
        offset := notVisibleCols;
      END;
      INC(vtStartColumn, offset);

      MarkAllLinesChanged(VT);

      RenderTextLines(VT, 0, vtTextHeight - 1, vtStartColumn, vtTextWidth);
(*********************** Another variation which uses ScrollRaster ********
      IF (CARDINAL(offset) >= vtTextWidth) THEN (* Refresh entire window *)
        RenderTextLines(VT, 0, vtTextHeight - 1, vtStartColumn, vtTextWidth);
      ELSE (* Scroll text *)
        IF (NOT vtSizeChanging) THEN (* Not resizing window *)
          ScrollRaster(vtIntuiWindow^.RPort^, offset * DefCharWidth, 0,
                       DefTextStartX, DefTextStartY, xmax, ymax);
        END;
        RenderTextLines(VT, 0, vtTextHeight -1, 
                        vtStartColumn + vtTextWidth - CARDINAL(offset), offset);
      END;
***************************************************************************)
    ELSE (* offset = 0 *)
      RETURN; (* no change *)
    END;
  END; (* WITH *)

  IF (chgGad) THEN
    SetupHPropGadget(VT);
  END;
END ScrollHorizVTWindow;

(* ScrollVertVTWindow - scroll text vertically *)
PROCEDURE ScrollVertVTWindow(VT: VTWindow; offset: INTEGER; chgGad: BOOLEAN);
VAR
  xmax, ymax: CARDINAL;
  notVisibleRows: CARDINAL;
BEGIN
  WITH VT^ DO
    xmax := DefTextStartX + (vtTextWidth * DefCharWidth) - 1;
    ymax := DefTextStartY + (vtTextHeight * DefCharHeight) - 1;

    IF (offset < 0) THEN
      IF (vtVirtualLine = 0) THEN
        RETURN; (* no change *)
      END;
      IF (CARDINAL(ABS(offset)) > vtVirtualLine) THEN
        offset := - INTEGER(vtVirtualLine);
      END;
      INC(vtVirtualLine, offset);
      IF (vtScrollRedrawAll) OR
         (CARDINAL(ABS(offset)) >= vtTextHeight) THEN (* Redraw entire window *)
        ClearVTWindow(VT);
        RefreshVTWindow(VT);
      ELSE
        ScrollTextInfo(VT, 0, ABS(offset), vtTextHeight-CARDINAL(ABS(offset)));
        vtUpdateProc(VT, 0, vtVirtualLine, 
                     vtVirtualLine + CARDINAL(ABS(offset)) - 1);
        IF (NOT vtSizeChanging) THEN (* Not resizing window *)
          ScrollRaster(vtIntuiWindow^.RPort^, 0, offset * DefCharHeight,
                       DefTextStartX, DefTextStartY, xmax, ymax);
        END;
        RenderVTWindow(VT);
      END;
    ELSIF (offset > 0) THEN
      IF (vtVirtualLine + vtTextHeight >= vtTotalLines) THEN
        RETURN; (* no change *)
      END;

      notVisibleRows := vtTotalLines - (vtVirtualLine + vtTextHeight);
      IF (CARDINAL(offset) > notVisibleRows) THEN
        offset := notVisibleRows;
      END;
      INC(vtVirtualLine, offset);

      IF (vtScrollRedrawAll) OR
         (CARDINAL(offset) >= vtTextHeight) THEN (* Redraw entire window *)
        ClearVTWindow(VT);
        RefreshVTWindow(VT);
      ELSE
        ScrollTextInfo(VT, offset, 0, vtTextHeight-CARDINAL(offset));
        vtUpdateProc(VT, vtTextHeight - CARDINAL(offset),
                     vtVirtualLine + vtTextHeight - CARDINAL(offset),
                     vtVirtualLine + vtTextHeight - 1);
        IF (NOT vtSizeChanging) THEN (* Not resizing window *)
          ScrollRaster(vtIntuiWindow^.RPort^, 0, offset * DefCharHeight,
                       DefTextStartX, DefTextStartY, xmax, ymax);
        END;
        RenderVTWindow(VT);
      END;
    ELSE (* offset = 0 *)
      RETURN; (* no change *)
    END;
  END; (* WITH *)

  IF (chgGad) THEN
    SetupVPropGadget(VT);
  END;
END ScrollVertVTWindow;

(* AdjustVTWindow - avoid displaying blank lines if possible *)
PROCEDURE AdjustVTWindow(VT: VTWindow);
BEGIN
  WITH VT^ DO
    IF (vtVirtualLine + vtTextHeight > vtTotalLines) THEN
      IF (vtTotalLines <= vtTextHeight) THEN
        vtVirtualLine := 0;
      ELSE
        vtVirtualLine := vtTotalLines - vtTextHeight;
      END;
    END;
    IF (vtStartColumn + vtTextWidth > vtMaxColumns) THEN
      IF (vtMaxColumns <= vtTextWidth) THEN
       vtStartColumn := 0;
      ELSE
        vtStartColumn := vtMaxColumns - vtTextWidth;
      END;
    END;
  END;
END AdjustVTWindow;

(* DoGadgets - Handle window gadget commands *)
PROCEDURE DoGadgets(VTP: VTWindow; gad: GadgetPtr);
VAR
  prop: PropInfoPtr;
  gadID: CARDINAL;
  offset: INTEGER;
BEGIN
  WITH VTP^ DO
    gadID := gad^.GadgetID;
    prop := gad^.SpecialInfo;
    IF (gadID = GIDVProp) THEN
      IF (prop^.VertBody # 0FFFFH) THEN
        offset := (LONGCARD(prop^.VertPot) + 1D) *
                  LONGCARD(vtTotalLines - vtTextHeight) DIV 10000H;
        offset := offset - INTEGER(vtVirtualLine);
        ScrollVertVTWindow(VTP, offset, FALSE);
      END;
    ELSIF (gadID = GIDUpArrow) THEN
      ScrollVertVTWindow(VTP, -1, TRUE);
    ELSIF (gadID = GIDDownArrow) THEN
      ScrollVertVTWindow(VTP, +1, TRUE);
    ELSIF (gadID = GIDHProp) THEN
      IF (prop^.HorizBody # 0FFFFH) THEN
        offset := (LONGCARD(prop^.HorizPot) + 1D) *
                  LONGCARD(vtMaxColumns - vtTextWidth) DIV 10000H;
        offset := offset - INTEGER(vtStartColumn);
        ScrollHorizVTWindow(VTP, offset, FALSE);
      END;
    ELSIF (gadID = GIDLeftArrow) THEN
      ScrollHorizVTWindow(VTP, -1, TRUE);
    ELSIF (gadID = GIDRightArrow) THEN
      ScrollHorizVTWindow(VTP, +1, TRUE);
    END;
  END; (* WITH *)
END DoGadgets;

(* ====== PUBLIC ====== *)

(* OpenVTWindow - open a new virtual text window *)
PROCEDURE OpenVTWindow(VAR newVTW: NewVTWindow): VTWindow;
VAR
  vtw: VTWindow;
  nw: NewWindow;
BEGIN
  vtw := AllocMemory(SIZE(VTWindowRecord), MemReqSet{MemClear});
  IF (vtw # NIL) THEN
    LinkWindowNode(vtw);

    LOOP
      BeginGadgetList();
        GlobalGadgetOpt(DefaultGadgetFlags+GadgetFlagsSet{GRelHeight,GRelRight},
                        GadgetActivationSet{RelVerify, GadgImmediate});
        AddGadgetProp(-15, 26, 16, -35-8, FALSE, TRUE, 0, 0, 0, 0FFFFH);
        vtw^.vtVPropGad := LastGadget;
        GlobalGadgetOpt(DefaultGadgetFlags + GadgetFlagsSet{GRelRight},
                        GadgetActivationSet{RelVerify, GadgImmediate});
        AddGadgetImageButton(-15, 18, UpArrowImage);
        GlobalGadgetOpt(DefaultGadgetFlags+GadgetFlagsSet{GRelRight,GRelBottom},
                        GadgetActivationSet{RelVerify, GadgImmediate});
        AddGadgetImageButton(-15, -16, DownArrowImage);
        GlobalGadgetOpt(DefaultGadgetFlags+GadgetFlagsSet{GRelWidth,GRelBottom},
                        GadgetActivationSet{RelVerify, GadgImmediate});
        AddGadgetProp(18, -8, -50, 9, TRUE, FALSE, 0, 0, 0FFFFH, 0);
        vtw^.vtHPropGad := LastGadget;
        GlobalGadgetOpt(DefaultGadgetFlags + GadgetFlagsSet{GRelBottom},
                        GadgetActivationSet{RelVerify, GadgImmediate});
        AddGadgetImageButton(2, -8, LeftArrowImage);
        GlobalGadgetOpt(DefaultGadgetFlags + GadgetFlagsSet{GRelRight, GRelBottom},
                        GadgetActivationSet{RelVerify, GadgImmediate});
        AddGadgetImageButton(-31, -8, RightArrowImage);
        GlobalGadgetOpt(DefaultGadgetFlags + GadgetFlagsSet{GRelRight},
                        DefaultGadgetActivation);
        AddGadgetImageButton(-15, 10, DestButtonImage);
      vtw^.vtGadList := EndGadgetList();
      IF (vtw^.vtGadList = NIL) THEN
        EXIT; (* err *)
      END;

      WITH nw DO
        WITH newVTW DO
          LeftEdge    := nvtLeftEdge;
          TopEdge     := nvtTopEdge;
          Width       := nvtWidth;
          Height      := nvtHeight;
          DetailPen   := nvtDetailPen;
          BlockPen    := nvtBlockPen;
          IDCMPFlags  := IDCMPFlagsSet{};
          Flags       := DefWindowFlags;
          FirstGadget := vtw^.vtGadList;
          CheckMark   := NIL;
          Title       := nvtTitle;
          Screen      := VTWScreen;
          MinWidth    := nvtMinWidth;
          MinHeight   := nvtMinHeight;
          MaxWidth    := nvtMaxWidth;
          MaxHeight   := nvtMaxHeight;
          Type        := VTWScreenType;
        END;
      END;
      vtw^.vtIntuiWindow := OpenWindow(nw);
      IF (vtw^.vtIntuiWindow = NIL) THEN
        EXIT; (* err *)
      END;

      (* Set window user port to common window port and then set IDCMP *)
      vtw^.vtIntuiWindow^.UserPort := newVTW.nvtMsgPort;
      ModifyIDCMP(vtw^.vtIntuiWindow^, DefIDCMPFlags);

      (* Init return-link to VTWindow struct *)
      vtw^.vtIntuiWindow^.UserData := vtw;

      WITH vtw^ DO
      (*vtCharBuf          := NIL;*)
      (*vtColorBuf         := NIL;*)
      (*vtLineArray        := NIL;*)
      (*vtTextWidth        := 0;*)
      (*vtTextHeight       := 0;*)
      (*vtStartColumn      := 0;*)
      (*vtVirtualLine      := 0;*)
      (*vtCursorX          := 0;*)
      (*vtCursorY          := 0;*)
        vtUpdateProc       := DefaultUpdateProc;
        vtSelectProc       := DefaultSelectProc;
        vtMenuProc         := DefaultMenuProc;
        vtDestProc         := DefaultDestProc;
        vtKeyProc          := DefaultKeyProc;
        vtCloseProc        := DefaultCloseProc;
        vtErrorProc        := DefaultErrorProc;
        vtTotalLines       := 1;
        vtMaxColumns       := 1;
        vtUserData         := newVTW.nvtUserData;
        vtTextFgBgColor    := DefTextFgBgCOLOR;
        vtDefTextFgBgColor := DefTextFgBgCOLOR;
        vtScrollRedrawAll  := FALSE;
        vtQualifier        := IEQualifierSet{};
        vtSizeChanging     := FALSE;
      END;
      (* Allocate a 1 x 1 text info *)
      IF (NOT AllocTextInfo(vtw)) THEN
        EXIT; (* err *)
      END;

      RETURN (vtw); (* success! *)
    END; (* LOOP *)

    CloseVTWindow(vtw);

  END; (* IF *)
  RETURN (NIL); (* failure! *)
END OpenVTWindow;

(* CloseVTWindow - close a virtual text window (also used for cleanup on err) *)
PROCEDURE CloseVTWindow(VT: VTWindow);
VAR
  r: INTEGER;
  msg: IntuiMessagePtr;
  succ: IntuiMessagePtr;
BEGIN
  WITH VT^ DO
    IF (vtGadList # NIL) THEN
      IF (vtIntuiWindow # NIL) THEN
        r := RemoveGList(vtIntuiWindow^, vtGadList^, -1);
      END;
      FreeGadgetList(vtGadList^);
    END;

    IF (vtIntuiWindow # NIL) THEN

      Forbid(); (* Prevent any further messages from arriving at our port *)

      (* Clear all messages to current window from msg port *)
      msg := IntuiMessagePtr(vtIntuiWindow^.UserPort^.mpMsgList.lhHead);

      LOOP
        succ := IntuiMessagePtr(msg^.ExecMessage.mnNode.lnSucc);
        IF (succ = NIL) THEN EXIT; END;

        IF (msg^.IDCMPWindow = vtIntuiWindow) THEN
          Remove(msg^.ExecMessage.mnNode);
          ReplyMsg(msg);
        END;
        msg := succ;
      END;

      (* Clear user msg port to prevent Intuition from closing it *)
      vtIntuiWindow^.UserPort := NIL;
      ModifyIDCMP(vtIntuiWindow^, IDCMPFlagsSet{});

      Permit(); (* Resume task switching (but no further messages can appear) *)

      CloseWindow(vtIntuiWindow^);
    END;

    DeallocTextInfo(VT);
  END; (* WITH *)
  UnLinkWindowNode(VT);
  DeallocMemory(VT);
END CloseVTWindow;

(* InitParmVTWindow - modify current window configuration parameters *)
PROCEDURE InitParmsVTWindow(VT: VTWindow; VAR VTWparms: VTWindowParms): BOOLEAN;
VAR
  res: BOOLEAN;
  color: CHAR;
BEGIN
  WITH VT^ DO
    WITH VTWparms DO
      color := CHAR((CARDINAL(vtpDefFgColor) * 16) + CARDINAL(vtpDefBgColor));

      vtUpdateProc       := vtpUpdateProc;
      vtSelectProc       := vtpSelectProc;
      vtMenuProc         := vtpMenuProc;
      vtDestProc         := vtpDestProc;
      vtKeyProc          := vtpKeyProc;
      vtCloseProc        := vtpCloseProc;
      vtErrorProc        := vtpErrorProc;
      vtStartColumn      := vtpInitialColumn;
      vtVirtualLine      := vtpInitialLine;
      vtTotalLines       := vtpTotalLines;
      vtMaxColumns       := vtpMaxColumns;
      vtTextFgBgColor    := color;
      vtDefTextFgBgColor := color;
    END;
  END;
  res := AllocTextInfo(VT);
  IF (res) THEN
    (* Set new drawing color for FillEmptyRegion() function *)
    SetAPen(VT^.vtIntuiWindow^.RPort^, CARDINAL(VTWparms.vtpDefBgColor));

    (* Set new drawing color for ScrollRaster() function *)
    SetBPen(VT^.vtIntuiWindow^.RPort^, CARDINAL(VTWparms.vtpDefBgColor));

    AdjustVTWindow(VT);
    SetupVPropGadget(VT);
    SetupHPropGadget(VT);
    RefreshVTWindow(VT);
    FillEmptyRegion(VT);
  END;
  RETURN (res);
END InitParmsVTWindow;

(* SetCursorPosVTWindow - set a new cursor position *)
PROCEDURE SetCursorPosVTWindow(VT: VTWindow; x, y: CARDINAL);
BEGIN
  WITH VT^ DO
    vtCursorX := x;
    vtCursorY := y;
  END;
END SetCursorPosVTWindow;

(* GetCursorPosVTWIndow - get current cursor position *)
PROCEDURE GetCursorPosVTWindow(VT: VTWindow; VAR x, y: CARDINAL);
BEGIN
  WITH VT^ DO
    x := vtCursorX;
    y := vtCursorY;
  END;
END GetCursorPosVTWindow;

(* SetColorVTWindow - set new color for text output *)
PROCEDURE SetColorVTWindow(VT: VTWindow; fg, bg: BYTE);
BEGIN
  VT^.vtTextFgBgColor := CHAR((CARDINAL(fg) * 16) + CARDINAL(bg));
END SetColorVTWindow;

(* WriteColorVTWindow - change color of characters in a specified line *)
PROCEDURE WriteColorVTWindow(VT: VTWindow; y, startX, stopX: CARDINAL;
                             fg, bg: BYTE);
VAR
  color: CHAR;
  idx: CARDINAL;
  x: CARDINAL;
BEGIN
  color := CHAR((CARDINAL(fg) * 16) + CARDINAL(bg));
  WITH VT^ DO
    idx := vtLineArray^[y].vtlIndex;
    vtLineArray^[y].vtlChanged := TRUE;

    FOR x := startX TO stopX DO
      vtColorBuf^[idx + x] := color;
    END;
  END;
END WriteColorVTWindow;

(* WriteCharVTWindow - write one character to a window *)
PROCEDURE WriteCharVTWindow(VT: VTWindow; ch: CHAR);
VAR
  idx: CARDINAL;
BEGIN
  WITH VT^ DO
    IF (vtCursorX < vtMaxColumns) THEN
      idx := vtLineArray^[vtCursorY].vtlIndex + vtCursorX;
      vtLineArray^[vtCursorY].vtlChanged := TRUE;

      vtCharBuf^[idx]  := ch;
      vtColorBuf^[idx] := vtTextFgBgColor;
    END;
    INC(vtCursorX);
  END;
END WriteCharVTWindow;

(* WriteBufferVTWindow - write a specified number of chars to window *)
PROCEDURE WriteBufferVTWindow(VT: VTWindow; buf: ADDRESS; bufLen: CARDINAL);
VAR
  idx: CARDINAL;
BEGIN
  WITH VT^ DO
    idx := vtLineArray^[vtCursorY].vtlIndex;
    vtLineArray^[vtCursorY].vtlChanged := TRUE;

    WHILE (bufLen # 0) DO
      IF (vtCursorX < vtMaxColumns) THEN
        vtCharBuf^[idx + vtCursorX]  := CHAR(buf^);
        vtColorBuf^[idx + vtCursorX] := vtTextFgBgColor;
      END;
      INC(vtCursorX);
      INC(buf);
      DEC(bufLen);
    END;
  END;
END WriteBufferVTWindow;

(* WriteStringVTWindow - write a string of characters to a window *)
PROCEDURE WriteStringVTWindow(VT: VTWindow; str: ADDRESS);
VAR
  idx: CARDINAL;
BEGIN
  WITH VT^ DO
    idx := vtLineArray^[vtCursorY].vtlIndex;
    vtLineArray^[vtCursorY].vtlChanged := TRUE;

    WHILE (CHAR(str^) # 0C) DO
      IF (vtCursorX < vtMaxColumns) THEN
        vtCharBuf^[idx + vtCursorX]  := CHAR(str^);
        vtColorBuf^[idx + vtCursorX] := vtTextFgBgColor;
      END;
      INC(vtCursorX);
      INC(str);
    END;
  END;
END WriteStringVTWindow;

(* ClearVTWindow - clear window text to blank and default color *)
PROCEDURE ClearVTWindow(VT: VTWindow);
VAR
  row: CARDINAL;
  size: LONGCARD;
BEGIN
  MarkAllLinesChanged(VT);
  WITH VT^ DO
    size := vtMaxColumns * vtTextHeight;
    setmem(vtCharBuf, size, " ");
    setmem(vtColorBuf, size, vtDefTextFgBgColor);
  END;
END ClearVTWindow;

(* RefreshVTWindow - force the entire window to be refreshed and rendered *)
PROCEDURE RefreshVTWindow(VT: VTWindow);
BEGIN
  WITH VT^ DO
    vtUpdateProc(VT, 0, vtVirtualLine, vtVirtualLine + vtTextHeight - 1);
  END;
  RenderVTWindow(VT);
END RefreshVTWindow;

(* RefreshRangeVTWindow - refresh a specified range of lines a window *)
PROCEDURE RefreshRangeVTWindow(VT: VTWindow; first, last: CARDINAL);
BEGIN
  WITH VT^ DO
    vtUpdateProc(VT, first, vtVirtualLine + first, vtVirtualLine + last);
    RenderTextLines(VT, first, last, vtStartColumn, vtTextWidth);
  END;
END RefreshRangeVTWindow;

(* RenderVTWindow - render current contents of text buffer into window *)
PROCEDURE RenderVTWindow(VT: VTWindow);
VAR
  y: CARDINAL;
BEGIN
  WITH VT^ DO
    RenderTextLines(VT, 0, vtTextHeight - 1, vtStartColumn, vtTextWidth);
  END;
END RenderVTWindow;

(*==================================*)
 PROCEDURE ClipVTWindow(VT:VTWindow);
(*==================================*)
(* Output all text to the Amiga Clipboard *)
VAR y: CARDINAL;
BEGIN

IF (VT = NIL) OR (IFFParseBase = NIL) OR NOT AccessClip(iff, IFFWrite) THEN
   RETURN;
END;

WITH VT^ DO
   ClipTextLines(VT, 0, vtTextHeight - 1);
END;

UnaccessClip(iff);

END ClipVTWindow;

(* ScrollVTWindow - scroll the current window vert or horizontally *)
PROCEDURE ScrollVTWindow(VT: VTWindow; newCol, newLine: CARDINAL);
BEGIN
  WITH VT^ DO
    ScrollVertVTWindow(VT, INTEGER(newLine) - INTEGER(vtVirtualLine), TRUE);
    ScrollHorizVTWindow(VT, INTEGER(newCol) - INTEGER(vtStartColumn), TRUE);
  END;
END ScrollVTWindow;

(* HandleMsgVTWindow - handle an IDCMP message for any virtual text window *)
PROCEDURE HandleMsgVTWindow(msg: ADDRESS);
VAR
  imsg: IntuiMessagePtr;
  VTP: VTWindow;
  MsgClass: IDCMPFlagsSet;
  MsgMouseX: INTEGER;
  MsgMouseY: INTEGER;
  MsgCode: CARDINAL;
  gad: GadgetPtr;
  gadID: CARDINAL;
  row: CARDINAL;
  offset: INTEGER;
  x, y: CARDINAL;
  menuCode: CARDINAL;
  menuItemPtr: MenuItemPtr;
  keyLength: INTEGER;
  keyBuf: ARRAY [0..31] OF CHAR;
BEGIN
  imsg := IntuiMessagePtr(msg);

  (* Extract needed info from IDCMP message *)
  WITH imsg^ DO
    MsgClass         := Class;
    MsgCode          := Code;
    gad              := IAddress;
    MsgMouseX        := MouseX;   
    MsgMouseY        := MouseY;
    VTP              := IDCMPWindow^.UserData;
    VTP^.vtQualifier := Qualifier;
  END;

  (* All messages except Rawkey can be replied to immediately *)
  IF (NOT (RawKey IN MsgClass)) THEN
    ReplyMsg(imsg);
  END;

  WITH VTP^ DO
    IF (RawKey IN MsgClass) THEN
      IF (SelectIsDown) THEN
        SelectIsDown := FALSE;
        ModifyIDCMP(vtIntuiWindow^, DefIDCMPFlags);
      END;

      keyLength := DeadKeyConvert(imsg, ADR(keyBuf[0]), SIZE(keyBuf));
      ReplyMsg(imsg);
      (* Only call user proc for key-down and if valid conversion *)
      IF (MsgCode < 80H) AND (keyLength > 0) THEN
        vtKeyProc(VTP, ADR(keyBuf[0]), keyLength);
      END;
    ELSIF (GadgetDown IN MsgClass) THEN
      ActiveGadget := gad;
      DoGadgets(VTP, gad);
      ModifyIDCMP(vtIntuiWindow^, AltIDCMPFlags); (* Start IntuiTicks *)
    ELSIF (GadgetUp IN MsgClass) THEN
      gadID := gad^.GadgetID;
      IF (gadID = GIDDestWindow) THEN (* Dest-Window Button *)
        vtDestProc(VTP);
      ELSE (* All other gadgets *)
        IF (gadID = GIDVProp) OR (gadID = GIDHProp) THEN
          DoGadgets(VTP, gad);
        END;
        ActiveGadget := NIL;
        ModifyIDCMP(vtIntuiWindow^, DefIDCMPFlags); (* Stop IntuiTicks *)
      END;
    ELSIF (IntuiTicks IN MsgClass) THEN
      IF (ActiveGadget # NIL) THEN
        gad := ActiveGadget;
        IF (Selected IN gad^.Flags) THEN
          DoGadgets(VTP, gad);
        END;
      ELSIF (SelectIsDown) THEN
        x := MsgMouseX - DefTextStartX;
        y := MsgMouseY - DefTextStartY;
        IF (INTEGER(x) > 0) AND (INTEGER(y) > 0) THEN
          x := x DIV DefCharWidth;
          y := y DIV DefCharHeight;
          IF (x < vtTextWidth) AND (y < vtTextHeight) AND
             (x < vtMaxColumns - vtStartColumn) AND
             (y < vtTotalLines - vtVirtualLine) AND
             (NOT ((PrevXPos = x) AND (PrevYPos = y))) THEN
            PrevXPos := x;
            PrevYPos := y;
            vtSelectProc(VTP, x+vtStartColumn, y+vtVirtualLine, FALSE);
          END;
        END;
      END;
    ELSIF (MouseButtons IN MsgClass) THEN
      x := MsgMouseX - DefTextStartX;
      y := MsgMouseY - DefTextStartY;
      IF (MsgCode = SelectUp) THEN
        IF (ActiveGadget # NIL) THEN
          ActiveGadget := NIL;
          ModifyIDCMP(vtIntuiWindow^, DefIDCMPFlags);
        ELSIF (SelectIsDown) THEN
          SelectIsDown := FALSE;
          ModifyIDCMP(vtIntuiWindow^, DefIDCMPFlags);
        END;
      ELSIF (MsgCode = SelectDown) AND
         (INTEGER(x) > 0) AND (INTEGER(y) > 0) THEN
        x := x DIV DefCharWidth;
        y := y DIV DefCharHeight;
        IF (x < vtTextWidth) AND (y < vtTextHeight) AND
           (x < vtMaxColumns - vtStartColumn) AND
           (y < vtTotalLines - vtVirtualLine) THEN
          ModifyIDCMP(vtIntuiWindow^, AltIDCMPFlags);
          SelectIsDown := TRUE;
          PrevXPos := x;
          PrevYPos := y;
          vtSelectProc(VTP, x+vtStartColumn, y+vtVirtualLine, TRUE);
        END;
      END;
    ELSIF (MenuPick IN MsgClass) THEN
      IF (SelectIsDown) THEN
        SelectIsDown := FALSE;
        ModifyIDCMP(vtIntuiWindow^, DefIDCMPFlags);
      END;

      menuCode := MsgCode;
      WHILE (menuCode # MenuNull) DO 
        vtMenuProc(VTP,MENUNUM(menuCode),ITEMNUM(menuCode),SUBNUM(menuCode));

        (* Check if the window we are working with has been closed. *)
        IF (NOT IsWindowOpen(VTP)) THEN
          RETURN; (* Abort window has been closed! *)
        END;

        menuItemPtr := ItemAddress(vtIntuiWindow^.MenuStrip^, menuCode);
        menuCode    := menuItemPtr^.NextSelect;
      END;
    ELSIF (NewSize IN MsgClass) THEN
      VTP^.vtSizeChanging := FALSE;
      IF (AllocTextInfo(VTP)) THEN
        AdjustVTWindow(VTP);
        SetupVPropGadget(VTP);
        SetupHPropGadget(VTP);
        RefreshVTWindow(VTP);
        FillEmptyRegion(VTP);
      ELSE
        vtErrorProc(VTP);
      END;
    ELSIF (Closewindow IN MsgClass) THEN
      vtCloseProc(VTP);
    ELSIF (SizeVerify IN MsgClass) THEN
      (* Message used to avoid display damage due to user resizing window *)
      VTP^.vtSizeChanging := TRUE;
    ELSE (* Unexpected Message Type! (Bug) *)
    END;
  END; (* WITH *)
END HandleMsgVTWindow;

(* GetUserDataVTWindow - get private user data field *)
PROCEDURE GetUserDataVTWindow(VT: VTWindow): ADDRESS;
BEGIN
  RETURN (VT^.vtUserData);
END GetUserDataVTWindow;

(* GetIntuiWindowVTWindow - get ptr to actual window structure for vt window *)
PROCEDURE GetIntuiWindowVTWindow(VT: VTWindow): ADDRESS;
BEGIN
  RETURN (VT^.vtIntuiWindow);
END GetIntuiWindowVTWindow;

(* IsQualifierShiftVTWindow - check if last IDCMP qualifier was a SHIFT key *)
PROCEDURE IsQualifierShiftVTWindow(VT: VTWindow): BOOLEAN;
BEGIN
  WITH VT^ DO
    RETURN ((IEQualifierLShift IN vtQualifier) OR (IEQualifierRShift IN vtQualifier));
  END;
END IsQualifierShiftVTWindow;

(* GetDisplayRangeVTWindow - get range of lines currently being displayed *)
PROCEDURE GetDisplayRangeVTWindow(VT: VTWindow; VAR first, last: CARDINAL);
BEGIN
  WITH VT^ DO
    first := vtVirtualLine;
    last  := vtVirtualLine + vtTextHeight - 1;
  END;
END GetDisplayRangeVTWindow;

(* GetDisplayColumnsVTWindow - get range of columns currently being displayed *)
PROCEDURE GetDisplayColumnsVTWindow(VT: VTWindow; VAR first, last: CARDINAL);
BEGIN
  WITH VT^ DO
    first := vtStartColumn;
    last  := vtStartColumn + vtTextWidth - 1;
  END;
END GetDisplayColumnsVTWindow;

PROCEDURE GetActualDisplaySizeVTWindow(VT: VTWindow; VAR cols, lines: CARDINAL);
BEGIN
  WITH VT^.vtIntuiWindow^ DO
    cols  := (Width-DefTextSizeXAdj) DIV DefCharWidth;
    lines := (Height-DefTextSizeYAdj) DIV DefCharHeight;
  END;
END GetActualDisplaySizeVTWindow;

(* GetTitleBarWidthVTWindow - calc number of chars which fit into titlebar *)
PROCEDURE GetTitleBarWidthVTWindow(VT: VTWindow): CARDINAL;
BEGIN
  RETURN ((VT^.vtIntuiWindow^.Width - TitleBarWidthAdj) DIV 8);
END GetTitleBarWidthVTWindow;

(* GetInfoVTWindow - get current state info on a window *)
PROCEDURE GetInfoVTWindow(VT: VTWindow; VAR x, y, width, height: CARDINAL;
                          VAR detailPen, blockPen: BYTE);
BEGIN
  WITH VT^.vtIntuiWindow^ DO
    x         := LeftEdge;
    y         := TopEdge;
    width     := Width;
    height    := Height;
    detailPen := DetailPen;
    blockPen  := BlockPen;
  END;
END GetInfoVTWindow;

(* GetDetailPenBlockPenVTWindow - get current detail and block pen values *)
PROCEDURE GetDetailPenBlockPenVTWindow(VT: VTWindow; 
                                       VAR detailPen, blockPen: BYTE);
BEGIN
  WITH VT^.vtIntuiWindow^ DO
    detailPen := DetailPen;
    blockPen  := BlockPen;
  END;
END GetDetailPenBlockPenVTWindow;

(* SetDetailPenBlockPenVTWindow - set new detail and block pens and refresh *)
PROCEDURE SetDetailPenBlockPenVTWindow(VT: VTWindow; detailPen, blockPen: BYTE);
BEGIN
  WITH VT^.vtIntuiWindow^ DO
    DetailPen := detailPen;
    BlockPen  := blockPen;
  END;
  RefreshWindowFrame(VT^.vtIntuiWindow^);

  (* This is necessary because the frame refresh causes some display damage *)
  RefreshVTWindow(VT);
  FillEmptyRegion(VT);
END SetDetailPenBlockPenVTWindow;

(* SetDefaultColorVTWindow - set a new default text color used for clears *)
PROCEDURE SetDefaultColorVTWindow(VT: VTWindow; fg, bg: BYTE);
VAR
  color: CHAR;
BEGIN
  color := CHAR((CARDINAL(fg) * 16) + CARDINAL(bg));
  WITH VT^ DO
    vtTextFgBgColor    := color;
    vtDefTextFgBgColor := color;
  END;

  (* Set new drawing color for FillEmptyRegion() function *)
  SetAPen(VT^.vtIntuiWindow^.RPort^, CARDINAL(bg));

  (* Set new drawing color for ScrollRaster() function *)
  SetBPen(VT^.vtIntuiWindow^.RPort^, CARDINAL(bg));

  ClearVTWindow(VT);
  RefreshVTWindow(VT);
  FillEmptyRegion(VT);
END SetDefaultColorVTWindow;

(* SetScrollUpdateModeVTWindow - change status of redraw all mode *)
PROCEDURE SetScrollUpdateModeVTWindow(VT: VTWindow; redrawAll: BOOLEAN);
BEGIN
  VT^.vtScrollRedrawAll := redrawAll;
END SetScrollUpdateModeVTWindow;

(* SetSizeVerifyModeAllVTWindows - disable/enable size verify on all windows *)
PROCEDURE SetSizeVerifyModeAllVTWindows(verify: BOOLEAN);
VAR
  vt: VTWindow;
  flags: IDCMPFlagsSet;
BEGIN
  vt := VTWindowList;
  WHILE (vt # NIL) DO
    WITH vt^ DO
      flags := vtIntuiWindow^.IDCMPFlags;
      IF (verify) THEN
        INCL(flags, SizeVerify);
      ELSE
        EXCL(flags, SizeVerify);
      END;
      ModifyIDCMP(vtIntuiWindow^, flags);
    END;
    vt := vt^.vtNext;
  END;
END SetSizeVerifyModeAllVTWindows;

(* SetTitleVTWindow - set a new title for a specified window *)
PROCEDURE SetTitleVTWindow(VT: VTWindow; title: ADDRESS);
BEGIN
  SetWindowTitles(VT^.vtIntuiWindow^, title, -1D);
END SetTitleVTWindow;

(* SetMenuStripVTWindow - set a new menu strip for a specified window *)
PROCEDURE SetMenuStripVTWindow(VT: VTWindow; VAR menuStrip: Menu);
BEGIN
  SetMenuStrip(VT^.vtIntuiWindow^, menuStrip);
END SetMenuStripVTWindow;

(* ClearMenuStripVTWindow - clear the current menu strip from a window *)
PROCEDURE ClearMenuStripVTWindow(VT: VTWindow);
BEGIN
  ClearMenuStrip(VT^.vtIntuiWindow^);
END ClearMenuStripVTWindow;

(* CleanupVirtualTextWindows - cleanup internal resources for this module *)
PROCEDURE CleanupVirtualTextWindows();
BEGIN
  IF (XTextS # NIL) THEN
    UnmakeXTextSupport(XTextS^);
  END;

  IF (UpArrowData # NIL) THEN
    DeallocMemory(UpArrowData);
  END;

  IF (DownArrowData # NIL) THEN
    DeallocMemory(DownArrowData);
  END;

  IF (LeftArrowData # NIL) THEN
    DeallocMemory(LeftArrowData);
  END;

  IF (RightArrowData # NIL) THEN
    DeallocMemory(RightArrowData);
  END;

  IF (DestButtonData # NIL) THEN
    DeallocMemory(DestButtonData);
  END;

  IF IFFParseBase # NIL THEN
    IF iff # NIL THEN
      IF IFFisOpen THEN
         CloseIFF(iff^);
         IFFisOpen := FALSE;
      END;
      IF iff^.iffStream # 0D THEN
         CloseClipboard(ClipboardHandlePtr(iff^.iffStream));
         iff^.iffStream := 0D;
      END;
      FreeIFF(iff^);
      iff := NIL;
    END;
    CloseLibrary(IFFParseBase^);
  END;

END CleanupVirtualTextWindows;

(* InitVirtualTextWindows - initialize internal resources of module *)
PROCEDURE InitVirtualTextWindows(scr: ADDRESS; maxWidth: CARDINAL): BOOLEAN;
BEGIN
  IF (scr = NIL) THEN (* WorkBench *)
    VTWScreen     := OpenWorkBench();
    VTWScreenType := WBenchScreen;
  ELSE (* CustomScreen *)
    VTWScreen     := scr;
    VTWScreenType := CustomScreen;
  END;

  IF OSVersion2 THEN
    IFFParseBase := OpenLibrary(ADR(IFFParseName), 0D);
    IF IFFParseBase # NIL THEN
      iff := AllocIFF();
      IF iff # NIL THEN
         iff^.iffStream := 0D;
      ELSE
         CloseLibrary(IFFParseBase^);
         IFFParseBase := NIL;
      END;
    END;
  END;

  XTextS         := NIL;
  UpArrowData    := NIL;
  DownArrowData  := NIL;
  LeftArrowData  := NIL;
  RightArrowData := NIL;
  DestButtonData := NIL;

  LOOP
    XTextS := MakeXTextSupport(VTWScreen^.RastPort, NIL, maxWidth, XTextFlagsSet{});
    IF (XTextS = NIL) THEN
      EXIT; (* err *)
    END;

    UpArrowData    := AllocMemory(SIZE(UpArrowData^), MemReqSet{MemChip});
    DownArrowData  := AllocMemory(SIZE(DownArrowData^), MemReqSet{MemChip});
    LeftArrowData  := AllocMemory(SIZE(LeftArrowData^), MemReqSet{MemChip});
    RightArrowData := AllocMemory(SIZE(RightArrowData^), MemReqSet{MemChip});
    DestButtonData := AllocMemory(SIZE(DestButtonData^), MemReqSet{MemChip});

    IF (UpArrowData = NIL) OR (DownArrowData = NIL) OR
       (LeftArrowData = NIL) OR (RightArrowData = NIL) OR
       (DestButtonData = NIL) THEN
      EXIT; (* err *)
    END;

    UpArrowData^[00] := 08181H;
    UpArrowData^[01] := 083C1H;
    UpArrowData^[02] := 08FF1H;
    UpArrowData^[03] := 0BFFDH;
    UpArrowData^[04] := 0FFFFH;
    UpArrowData^[05] := 083C1H;
    UpArrowData^[06] := 083C1H;
    UpArrowData^[07] := 083C1H;
  
    DownArrowData^[00] := 083C1H;
    DownArrowData^[01] := 083C1H;
    DownArrowData^[02] := 083C1H;
    DownArrowData^[03] := 0FFFFH;
    DownArrowData^[04] := 0BFFDH;
    DownArrowData^[05] := 08FF1H;
    DownArrowData^[06] := 083C1H;
    DownArrowData^[07] := 08181H;
  
    LeftArrowData^[00] := 0FFFFH;
    LeftArrowData^[01] := 000E0H;
    LeftArrowData^[02] := 007E0H;
    LeftArrowData^[03] := 03FE0H;
    LeftArrowData^[04] := 0FFFFH;
    LeftArrowData^[05] := 03FE0H;
    LeftArrowData^[06] := 007E0H;
    LeftArrowData^[07] := 000E0H;
    LeftArrowData^[08] := 0FFFFH;

    RightArrowData^[00] := 0FFFFH;
    RightArrowData^[01] := 00700H;
    RightArrowData^[02] := 007E0H;
    RightArrowData^[03] := 007FCH;
    RightArrowData^[04] := 0FFFFH;
    RightArrowData^[05] := 007FCH;
    RightArrowData^[06] := 007E0H;
    RightArrowData^[07] := 00700H;
    RightArrowData^[08] := 0FFFFH;

    DestButtonData^[00] := 08001H;
    DestButtonData^[01] := 08FF1H;
    DestButtonData^[02] := 08811H;
    DestButtonData^[03] := 08811H;
    DestButtonData^[04] := 08811H;
    DestButtonData^[05] := 08FF1H;
    DestButtonData^[06] := 08001H;
    DestButtonData^[07] := 0FFFFH;

    WITH UpArrowImage DO
      LeftEdge   := 0;
      TopEdge    := 0;
      Width      := 16;
      Height     := 8;
      Depth      := 1;
      ImageData  := UpArrowData;
      PlanePick  := BYTE(1);
      PlaneOnOff := BYTE(1);
      NextImage  := NIL;
    END;

    WITH DownArrowImage DO
      LeftEdge   := 0;
      TopEdge    := 0;
      Width      := 16;
      Height     := 8;
      Depth      := 1;
      ImageData  := DownArrowData;
      PlanePick  := BYTE(1);
      PlaneOnOff := BYTE(1);
      NextImage  := NIL;
    END;

    WITH LeftArrowImage DO
      LeftEdge   := 0;
      TopEdge    := 0;
      Width      := 16;
      Height     := 9;
      Depth      := 1;
      ImageData  := LeftArrowData;
      PlanePick  := BYTE(1);
      PlaneOnOff := BYTE(1);
      NextImage  := NIL;
    END;

    WITH RightArrowImage DO
      LeftEdge   := 0;
      TopEdge    := 0;
      Width      := 16;
      Height     := 9;
      Depth      := 1;
      ImageData  := RightArrowData;
      PlanePick  := BYTE(1);
      PlaneOnOff := BYTE(1);
      NextImage  := NIL;
    END;

    WITH DestButtonImage DO
      LeftEdge   := 0;
      TopEdge    := 0;
      Width      := 16;
      Height     := 8;
      Depth      := 1;
      ImageData  := DestButtonData;
      PlanePick  := BYTE(1);
      PlaneOnOff := BYTE(1);
      NextImage  := NIL;
    END;

    RETURN (TRUE); (* Success! *)
  END; (* LOOP *)

  CleanupVirtualTextWindows();
  RETURN (FALSE); (* Failed! *)
END InitVirtualTextWindows;

(* DefaultUpdateProc - default do nothing procedure *)
PROCEDURE DefaultUpdateProc(VTP: VTWindow; OutputRow: CARDINAL;
                            FirstLine: CARDINAL; LastLine: CARDINAL);
END DefaultUpdateProc;

(* DefaultSelectProc - default do nothing procedure *)
PROCEDURE DefaultSelectProc(VTP: VTWindow; x, y: CARDINAL; pressed: BOOLEAN);
END DefaultSelectProc;

(* DefaultMenuProc - default do nothing procedure *)
PROCEDURE DefaultMenuProc(VTP: VTWindow; MenuNum, ItemNum, SubNum: CARDINAL);
END DefaultMenuProc;

(* DefaultDestProc - default do nothing procedure *)
PROCEDURE DefaultDestProc(VTP: VTWindow);
END DefaultDestProc;

(* DefaultKeyProc - default do nothing procedure *)
PROCEDURE DefaultKeyProc(VTP: VTWindow; bufPtr: ADDRESS; bufLength: CARDINAL);
END DefaultKeyProc;

(* DefaultCloseProc - default do nothing procedure *)
PROCEDURE DefaultCloseProc(VTP: VTWindow);
END DefaultCloseProc;

(* DefaultErrorProc - default do nothing procedure *)
PROCEDURE DefaultErrorProc(VTP: VTWindow);
END DefaultErrorProc;

(*----------------------*)
 BEGIN (* mod init code *)
(*----------------------*)

IFFParseBase := NIL;
IFFisOpen := FALSE;

END DBTextWindows.
