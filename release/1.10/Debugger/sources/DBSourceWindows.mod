(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBSourceWindows.MOD               Version: Amiga.01.10             *
 * Created: 01/08/89   Updated: 05/08/89   Author: Leon Frenkel             *
 * Description: Debugger source windows handling module.                    *
 ****************************************************************************)

IMPLEMENTATION MODULE DBSourceWindows;

FROM SYSTEM IMPORT
  ADDRESS, BYTE,
  ADR;
FROM CPrintBuffer IMPORT
  sprintf;
FROM CStrings IMPORT
  strlen;
FROM DBBreakPoints IMPORT
  CreateBreakPoint, DeleteBreakPoint, IsAdrBreakPoint,
  GetBreakPointOnLineNo;
FROM DBConfiguration IMPORT
  ConfigWindow;
FROM DBFiles IMPORT
  InvalidModNo, InvalidLineNo, InvalidStatNo, InvalidAbsAdr, FileType,
  GetSourceLineMod, GetSourceLineDef,
  GetSourceFileMod, GetSourceFileDef, GetFilePath, GetAdrFromLinePos,
  GetModNoModOfs, GetSourceLineInfoFromStatementNo,
  GetStatementNoFromModOffset, FindSourceStringDef, FindSourceStringMod;
FROM DBMenusKbd IMPORT
  LinkGlobalMenuStrip, DoGlobalMenuCommand, DoGlobalKbdCommand,
  AddMenuItemWithCheck, AddMenuItemWithCheckList, AddMenuItemNoHighlight,
  EnableMenuItem;
FROM DBMisc IMPORT
  AllocMemory, DeallocMemory, AllocString, DeallocString, GetMemoryLong;
FROM DBRequesters IMPORT
  RequestMaxStringLength,
  RequestTextFgBgColors, RequestContextLines, RequestSearchString,
  RequestAddress;
FROM DBSelection IMPORT
  SelectionData, ModuleFileType,
  GetSelection, SetSelection, ClearSelection,
  GetSelectionLineInfo, IsPosSelected, IfCurrentWindowClearSelection;
FROM DBStatusWindow IMPORT
  ShowErrMsgNoMemory, ShowErrMsgInvalidDest, ShowErrMsgProgIsExec,
  ShowError;
FROM DBTextWindows IMPORT
  DefMinWindowWidth, DefMinWindowHeight, DefMaxWindowWidth, DefMaxWindowHeight,
  VTWindow, NewVTWindow, VTWindowNIL, VTWindowParms,
  OpenVTWindow, CloseVTWindow, InitParmsVTWindow, GetDisplayRangeVTWindow,
  GetDisplayColumnsVTWindow, RefreshRangeVTWindow, ScrollVTWindow,
  GetUserDataVTWindow, RefreshVTWindow, SetCursorPosVTWindow, 
  WriteBufferVTWindow, WriteStringVTWindow, WriteColorVTWindow, ClearVTWindow,
  SetTitleVTWindow, GetInfoVTWindow, SetMenuStripVTWindow,
  ClearMenuStripVTWindow, SetDetailPenBlockPenVTWindow, SetDefaultColorVTWindow,
  GetDetailPenBlockPenVTWindow, IsQualifierShiftVTWindow,
  GetActualDisplaySizeVTWindow;
FROM DBUserProgram IMPORT
  ProcChainNodePtr,
  GetProcChainLength, GetProcChainNode, IsProgramLoaded, IsProgramWaiting;
FROM DBWindows IMPORT
  WindowClass,
  GetVTWindowsMsgPort;
FROM FormatString IMPORT
  FormatArg;
FROM Intuition IMPORT
  Menu, MenuPtr, MenuItemMutualExcludeSet;
FROM Memory IMPORT
  MemReqSet, MemClear;
FROM SimpleMenus IMPORT
  BeginMenuStrip, EndMenuStrip, FreeMenuStrip, AddMenu, AddMenuItem;

CONST
  (* Source Window Title displayed when no source file is displayed *)
  DefSourceWindowTitle = "Source";

  (* Source window keyboard commands *)
  KeyBeginSearch    = CHAR(27); (* ESC. Enter search string and begin search *)
  KeyContinueSearch = CHAR(9);  (* TAB. Continue search *)

  LocalMenus = 1; (* Number of window specific menus *)
  SourceMenu = 0; (* Local "Source" menu number *)

  BeginSearchItem    = 3; (* Local "Source" menu item number *)
  ContinueSearchItem = 4; (* Local "Source" menu item number *)

  LineNoInfoWidth = 6; (* width of line # field in source window *)

  (* String search error message *)
  SourceErrMsgStrNotFound = "Source file string search failed.";

TYPE
  (* Source window update mode *)
  UpdateMode = (UMPC, UMNone);

  (* Source window descriptor *)
  SourceWindowPtr = POINTER TO SourceWindow;
  SourceWindow = RECORD
                   swNext              : SourceWindowPtr; (* next window *)
                   swVTWindow          : VTWindow;      (* text window handle *)
                   swVTWindowParms     : VTWindowParms; (* text window parms *)
                   swMenuStrip         : MenuPtr;    (* local menu strip *)
                   swTitle             : ADDRESS;    (* ptr to title str *)
                   swDisplayOn         : BOOLEAN;    (* displaying data *)
                   swUpdateMode        : UpdateMode; (* display update mode*)
                   swFileIsMod         : BOOLEAN;    (* T: .MOD, F: .DEF *)
                   swModNo             : CARDINAL;   (* module number *)
                   swContextLines      : CARDINAL;   (* source context lines *)
                   swHighlightLineNo   : CARDINAL;   (* highlight line # *)
                   swHighlightStartPos : CARDINAL;   (* highlight start x pos *)
                   swHighlightStopPos  : CARDINAL;   (* highlight end x pos *)
                   swHighlightFgColor  : BYTE;       (* highlight fg color *)
                   swHighlightBgColor  : BYTE;       (* highlight bg color *)
                   swBreakPtFgColor    : BYTE;       (* break point fg color *)
                   swBreakPtBgColor    : BYTE;       (* break point bg color *)
                   swSearchString      : ADDRESS;    (* last search string *)
                   swSearchLineNo      : CARDINAL;   (* last search line # *)
                   swSearchStartPos    : CARDINAL;   (* last search start x pos *)
                   swSearchStopPos     : CARDINAL;   (* last search end x pos *)
                   swSearchFirst       : BOOLEAN;    (* T: initial search *)
                 END;

VAR
  SourceWindowList    : SourceWindowPtr; (* list of source windows *)

(* ===== PRIVATE ===== *)

(* AddWindowNode - create a window node and link into window list *)
PROCEDURE AddWindowNode(): SourceWindowPtr;
VAR
  sw: SourceWindowPtr;
BEGIN
  sw := AllocMemory(SIZE(SourceWindow), MemReqSet{MemClear});
  IF (sw # NIL) THEN
    IF (SourceWindowList # NIL) THEN
      sw^.swNext := SourceWindowList;
    END;
    SourceWindowList := sw;
  END;
  RETURN (sw);
END AddWindowNode;

(* DeleteWindowNode - delete window node and unlink from window list *)
PROCEDURE DeleteWindowNode(sw: SourceWindowPtr);
VAR
  prev: SourceWindowPtr;
  cur: SourceWindowPtr;
BEGIN
  prev := NIL;
  cur  := SourceWindowList;
  WHILE (cur # sw) DO
    prev := cur;
    cur  := cur^.swNext;
  END;

  IF (prev = NIL) THEN (* First in list *)
    SourceWindowList := sw^.swNext;
  ELSE (* Not first in list *)
    prev^.swNext := sw^.swNext;
  END;

  DeallocMemory(sw);
END DeleteWindowNode;

(* GetWindowIndex - get position of window node in list *)
PROCEDURE GetWindowIndex(sw: SourceWindowPtr): CARDINAL;
VAR
  index: CARDINAL;
  p: SourceWindowPtr;
BEGIN
  index := 0;
  p     := SourceWindowList;
  WHILE (sw # p) DO
    INC(index);
    p := p^.swNext;
  END;
  RETURN (index);
END GetWindowIndex;

(* CloseSourceWindow - close source window *)
PROCEDURE CloseSourceWindow(VAR sw: SourceWindow);
BEGIN
  WITH sw DO
    IfCurrentWindowClearSelection(swVTWindow);

    IF (swMenuStrip # NIL) THEN
      ClearMenuStripVTWindow(swVTWindow);
      FreeMenuStrip(swMenuStrip^);
    END;

    CloseVTWindow(swVTWindow);

    IF (swTitle # NIL) THEN
      DeallocString(swTitle);
    END;

    IF (swSearchString # NIL) THEN
      DeallocString(swSearchString);
    END;
  END;
  DeleteWindowNode(ADR(sw));
END CloseSourceWindow;

(* CloseSourceWindowNoMemory - close window and show no memory err msg *)
PROCEDURE CloseSourceWindowNoMemory(VAR sw: SourceWindow);
BEGIN
  CloseSourceWindow(sw);
  ShowError(ShowErrMsgNoMemory)
END CloseSourceWindowNoMemory;

(* CreateMenuStrip - create a local menu strip for a window *)
PROCEDURE CreateMenuStrip(VAR sw: SourceWindow): BOOLEAN;
TYPE
  M = MenuItemMutualExcludeSet;

VAR
  ms: MenuPtr;
  ok: BOOLEAN;
BEGIN
  BeginMenuStrip();
    AddMenu(                           ADR("Source "));
      AddMenuItemWithCheckList(          ADR(" PC Update Mode               "), (sw.swUpdateMode = UMPC), M{1});
      AddMenuItemWithCheckList(          ADR(" No Update Mode"), (sw.swUpdateMode = UMNone), M{0});
      AddMenuItemNoHighlight(            ADR(" ----------------------------"));
      AddMenuItem(                       ADR(" Begin String Search    [ESC]"));
      AddMenuItem(                       ADR(" Continue String Search [TAB]"));
      AddMenuItemNoHighlight(            ADR(" ----------------------------"));
      AddMenuItem(                       ADR(" Set Context Lines"));
      AddMenuItem(                       ADR(" Set Window Color"));
      AddMenuItem(                       ADR(" Set Text Color"));
      AddMenuItem(                       ADR(" Set Highlight Color"));
      AddMenuItem(                       ADR(" Set BreakPoint Color"));
  ms := EndMenuStrip();
  ok := (ms # NIL);

  IF (ok) THEN
    LinkGlobalMenuStrip(ms^);
    WITH sw DO
      swMenuStrip := ms;
      SetMenuStripVTWindow(swVTWindow, swMenuStrip^);
    END;
  ELSE
    CloseSourceWindowNoMemory(sw);
  END;
  RETURN (ok);
END CreateMenuStrip;

(* SetDefaultWindowTitle - set default source window title *)
PROCEDURE SetDefaultWindowTitle(VAR sw: SourceWindow);
BEGIN
  WITH sw DO
    SetTitleVTWindow(swVTWindow, ADR(DefSourceWindowTitle));
    IF (swTitle # NIL) THEN
      DeallocString(swTitle);
    END;
    swTitle := NIL;
  END;
END SetDefaultWindowTitle;

(* SetNewWindowTitle - set a new source window title *)
PROCEDURE SetNewWindowTitle(VAR sw: SourceWindow; title: ADDRESS);
VAR
  newTitle: ADDRESS;
BEGIN
  newTitle := AllocString(title);
  IF (newTitle # NIL) THEN
    WITH sw DO
      SetTitleVTWindow(swVTWindow, newTitle);
      IF (swTitle # NIL) THEN
        DeallocString(swTitle);
      END;
      swTitle := newTitle;
    END;
  END;
END SetNewWindowTitle;

(* EnableMenuSearchCommands - enable/disable "Source" menu search commands *)
PROCEDURE EnableMenuSearchCommands(VAR sw: SourceWindow; enabled: BOOLEAN);
BEGIN
  EnableMenuItem(sw.swMenuStrip^, SourceMenu, BeginSearchItem, enabled);
  EnableMenuItem(sw.swMenuStrip^, SourceMenu, ContinueSearchItem, enabled);
END EnableMenuSearchCommands;

(* ClearSourceWindow - disable display of data in window *)
PROCEDURE ClearSourceWindow(VAR sw: SourceWindow);
BEGIN
  WITH sw DO
    IF (swDisplayOn) THEN
      IfCurrentWindowClearSelection(swVTWindow);
      SetDefaultWindowTitle(sw);
      EnableMenuSearchCommands(sw, FALSE);
      swDisplayOn       := FALSE;
      swHighlightLineNo := InvalidLineNo;
      WITH swVTWindowParms DO
        vtpInitialLine   := 0;
        vtpTotalLines    := 1;
        vtpInitialColumn := 0;
        vtpMaxColumns    := 1;
      END;
      IF (NOT InitParmsVTWindow(swVTWindow, swVTWindowParms)) THEN
        CloseSourceWindowNoMemory(sw);
      END;
    END;
  END;
END ClearSourceWindow;

(* ClearHighlight - if a highlight line exists clear the highlighting *)
PROCEDURE ClearHighlight(VAR sw: SourceWindow);
VAR
  first, last: CARDINAL;
  line: CARDINAL;
BEGIN
  WITH sw DO
    IF (swDisplayOn) AND (swHighlightLineNo # InvalidLineNo) THEN
      GetDisplayRangeVTWindow(swVTWindow, first, last);
      line := swHighlightLineNo;
      swHighlightLineNo := InvalidLineNo;
      IF (first <= line) AND (last >= line) THEN
        DEC(line, first);
        RefreshRangeVTWindow(swVTWindow, line, line);
      END;
    END;
  END;
END ClearHighlight;

(* SetHighlight - set line to be highlighted in source window *)
PROCEDURE SetHighlight(VAR sw: SourceWindow; lineNo, startX, stopX: CARDINAL);
VAR
  first, last: CARDINAL;
  line: CARDINAL;
BEGIN
  WITH sw DO
    ClearHighlight(sw);

    swHighlightLineNo   := lineNo;
    swHighlightStartPos := startX;
    swHighlightStopPos  := stopX;

    IF (swDisplayOn) THEN
      GetDisplayRangeVTWindow(swVTWindow, first, last);
      IF (first <= swHighlightLineNo) AND (last >= swHighlightLineNo) THEN
        line := swHighlightLineNo - first;
        RefreshRangeVTWindow(swVTWindow, line, line);
      END;
    END;
  END;
END SetHighlight;

(* ResetSearchPosition - reset search position to begining of source file *)
PROCEDURE ResetSearchPosition(VAR sw: SourceWindow);
BEGIN
  WITH sw DO
    swSearchLineNo   := 0;
    swSearchStartPos := 0;
    swSearchStopPos  := 0;
    swSearchFirst    := TRUE;
  END;
END ResetSearchPosition;

(* SetSourceWindowFile - display module source file in source window *)
PROCEDURE SetSourceWindowFile(VAR sw: SourceWindow; modNo: CARDINAL;
                              fileIsMod: BOOLEAN; initWindow: BOOLEAN): BOOLEAN;
VAR
  ok: BOOLEAN;
  lineCount: CARDINAL;
  maxWidth: CARDINAL;
BEGIN
  ResetSearchPosition(sw);
  ClearHighlight(sw);

  WITH sw DO
    IfCurrentWindowClearSelection(swVTWindow);
    ok := (swDisplayOn) AND (swFileIsMod = fileIsMod) AND (swModNo = modNo);
    IF (NOT ok) THEN
      IF (fileIsMod) THEN
        ok := GetSourceFileMod(modNo, lineCount, maxWidth);
        IF (ok) THEN
          SetNewWindowTitle(sw, GetFilePath(modNo, FTMod));
        END;
      ELSE
        ok := GetSourceFileDef(modNo, lineCount, maxWidth);
        IF (ok) THEN
          SetNewWindowTitle(sw, GetFilePath(modNo, FTDef));
        END;
      END;
      IF (ok) THEN
        EnableMenuSearchCommands(sw, TRUE);
        swFileIsMod := fileIsMod;
        swModNo     := modNo;
        WITH swVTWindowParms DO
          vtpInitialLine   := 0;
          vtpTotalLines    := lineCount;
          vtpInitialColumn := 0;
          vtpMaxColumns    := LineNoInfoWidth + maxWidth;
        END;
        swDisplayOn := initWindow;
        IF (swDisplayOn) THEN
          ok := InitParmsVTWindow(swVTWindow, swVTWindowParms);
          IF (NOT ok) THEN
            CloseSourceWindowNoMemory(sw);
          END; (* IF *)
        END; (* IF *)
      END; (* IF *)
    END; (* IF *)
  END; (* WITH *)
  RETURN (ok);
END SetSourceWindowFile;

(* SetSourceWindowPos - position source window on highlighted line *)
PROCEDURE SetSourceWindowPos(VAR sw: SourceWindow);
VAR
  firstLine: CARDINAL;
  lastLine: CARDINAL;
  textHeight: CARDINAL;
  contHeight: CARDINAL;
  topOffset: CARDINAL;
  firstContext: CARDINAL;
  lastContext: CARDINAL;
  newLine: CARDINAL;
  newCol: CARDINAL;
  actualCols: CARDINAL;
  actualLines: CARDINAL;
  firstColumn: CARDINAL;
  lastColumn: CARDINAL;
  textWidth: CARDINAL;
  leftOffset: CARDINAL;
BEGIN
  WITH sw DO
    IF (swHighlightLineNo # InvalidLineNo) THEN
      IF (swDisplayOn) THEN
        GetDisplayRangeVTWindow(swVTWindow, firstLine, lastLine);
        GetDisplayColumnsVTWindow(swVTWindow, firstColumn, lastColumn);
      ELSE
        GetActualDisplaySizeVTWindow(swVTWindow, actualCols, actualLines);

        WITH swVTWindowParms DO
          firstLine := 0;
          lastLine  := actualLines - 1;
          IF (vtpTotalLines <= lastLine) THEN
            lastLine := vtpTotalLines - 1;
          END;

          firstColumn := 0;
          lastColumn  := actualCols - 1;
          IF (vtpMaxColumns <= lastColumn) THEN
            lastColumn := vtpMaxColumns - 1;
          END;
        END;
      END;

      textWidth  := lastColumn - firstColumn + 1;
      textHeight := lastLine - firstLine + 1;
      contHeight := (swContextLines * 2) + 1;

      (* Perform vertical scroll if necessary *)
      IF (textHeight < contHeight) THEN (* Context area smaller than window *)
        topOffset := textHeight DIV 2;
        IF (swHighlightLineNo < topOffset) THEN
          topOffset := swHighlightLineNo;
        END;
        newLine := swHighlightLineNo - topOffset;
      ELSE (* Context area larger or equal to window height *)
        IF (swHighlightLineNo < swContextLines) THEN
          firstContext := 0;
        ELSE
          firstContext := swHighlightLineNo - swContextLines;
        END;

        IF (swHighlightLineNo+swContextLines >= swVTWindowParms.vtpTotalLines) THEN
          lastContext := swVTWindowParms.vtpTotalLines - 1;
        ELSE
          lastContext := swHighlightLineNo + swContextLines;
        END;

        IF (firstLine > lastContext) OR (lastLine < firstContext) THEN
          topOffset := textHeight DIV 2;
          IF (swHighlightLineNo < topOffset) THEN
            topOffset := swHighlightLineNo;
          END;
          newLine := swHighlightLineNo - topOffset;
        ELSE
          IF (firstLine > firstContext) THEN (* Scroll Up *)
            newLine := firstContext;
          ELSIF (lastLine < lastContext) THEN (* Scroll Down *)
            newLine := lastContext - textHeight + 1;
          ELSE (* Stay at current pos *)
            newLine := firstLine;
          END;
        END;
      END;

      newCol := firstColumn;
      IF (firstColumn > swHighlightStartPos) OR
         (lastColumn < swHighlightStartPos) THEN
        leftOffset := textWidth DIV 2;
        IF (swHighlightStartPos < leftOffset) THEN
          leftOffset := swHighlightStartPos;
        END;
        newCol := swHighlightStartPos - leftOffset;
      END;
      
      IF (swDisplayOn) THEN (* Scroll window to show highlight *)
        ScrollVTWindow(swVTWindow, newCol, newLine);
      ELSE (* Set window parms for show highlight *)
        swVTWindowParms.vtpInitialColumn := newCol;
        swVTWindowParms.vtpInitialLine   := newLine;
      END;
    END; (* IF *)
  END; (* WITH *)
END SetSourceWindowPos;

(* SetSourceWindowOnProc - set source window on specified procedure in chain *)
PROCEDURE SetSourceWindowOnProc(VAR sw: SourceWindow; procChainNo: CARDINAL);
VAR
  pcn: ProcChainNodePtr;
  statNo: CARDINAL;
  firstInst: BOOLEAN;
  lineNo: CARDINAL;
  startX: CARDINAL;
  stopX: CARDINAL;
  chainLength: CARDINAL;
  chainNew: BOOLEAN;
BEGIN
  ResetSearchPosition(sw);
  ClearHighlight(sw);

  GetProcChainLength(chainNew, chainLength);
  IF (procChainNo < chainLength) THEN
    pcn := GetProcChainNode(procChainNo);
    WITH sw DO
      IF (SetSourceWindowFile(sw, pcn^.pcnModNo, TRUE, FALSE)) THEN
        GetStatementNoFromModOffset(pcn^.pcnModNo, pcn^.pcnModOffset,
                                    statNo, firstInst);
        IF (statNo # InvalidStatNo) THEN
          IF (procChainNo > 0) AND (firstInst) AND (statNo > 0) THEN
            DEC(statNo);
          END;

          GetSourceLineInfoFromStatementNo(pcn^.pcnModNo, statNo, 
                                           lineNo, startX, stopX);
          IF (lineNo # InvalidLineNo) THEN
            SetHighlight(sw, lineNo, startX + LineNoInfoWidth, 
                         stopX + LineNoInfoWidth);
            SetSourceWindowPos(sw);
          END; (* IF *)
        END; (* IF *)
        IF (NOT swDisplayOn) THEN
          swDisplayOn := TRUE;
          IF (NOT InitParmsVTWindow(swVTWindow, swVTWindowParms)) THEN
            CloseSourceWindowNoMemory(sw);
          END; (* IF *)
        END; (* IF *)
      END; (* IF *)
    END; (* WITH *)
  END;
END SetSourceWindowOnProc;

(* SetSourceWindowOnAdr - set source window on source which coresponds to adr *)
PROCEDURE SetSourceWindowOnAdr(VAR sw: SourceWindow; adr: ADDRESS);
VAR
  statNo: CARDINAL;
  firstInst: BOOLEAN;
  lineNo: CARDINAL;
  startX: CARDINAL;
  stopX: CARDINAL;
  modNo: CARDINAL;
  modOfs: LONGCARD;
BEGIN
  ResetSearchPosition(sw);
  ClearHighlight(sw);

  GetModNoModOfs(adr, modNo, modOfs);
  IF (modNo # InvalidModNo) THEN
    WITH sw DO
      IF (SetSourceWindowFile(sw, modNo, TRUE, FALSE)) THEN
        GetStatementNoFromModOffset(modNo, modOfs, statNo, firstInst);
        IF (statNo # InvalidStatNo) THEN
          GetSourceLineInfoFromStatementNo(modNo, statNo, lineNo, startX, stopX);
          IF (lineNo # InvalidLineNo) THEN
            SetHighlight(sw, lineNo, startX + LineNoInfoWidth, 
                         stopX + LineNoInfoWidth);
            SetSourceWindowPos(sw);
          END;
        END;
        IF (NOT swDisplayOn) THEN
          swDisplayOn := TRUE;
          IF (NOT InitParmsVTWindow(swVTWindow, swVTWindowParms)) THEN
            CloseSourceWindowNoMemory(sw);
          END; (* IF *)
        END; (* IF *)
      END; (* IF *)
    END; (* WITH *)
  END;
END SetSourceWindowOnAdr;

(* SetNewSourceSelection - setup new user selection based on x, y pos *)
PROCEDURE SetNewSourceSelection(VAR sw: SourceWindow; x, y: CARDINAL);
VAR
  data: SelectionData;
  absAdr: ADDRESS;
  modNo: CARDINAL;
  modOfs: LONGCARD;
  statNo: CARDINAL;
  firstInst: BOOLEAN;
  lineNo: CARDINAL;
  startX: CARDINAL;
  stopX: CARDINAL;
BEGIN
  IF (GetAdrFromLinePos(sw.swModNo, x - LineNoInfoWidth, y, absAdr)) THEN
    GetModNoModOfs(absAdr, modNo, modOfs);
    GetStatementNoFromModOffset(modNo, modOfs, statNo, firstInst);
    GetSourceLineInfoFromStatementNo(modNo, statNo, lineNo, startX, stopX);

    WITH data DO
      sdWindowClass := WCSource;
      sdVTWindow    := sw.swVTWindow;
      sdLineNo      := lineNo;
      sdStartPos    := startX + LineNoInfoWidth;
      sdEndPos      := stopX + LineNoInfoWidth;
      sdDefFgColor  := sw.swVTWindowParms.vtpDefFgColor;
      sdDefBgColor  := sw.swVTWindowParms.vtpDefBgColor;
      sdAdr         := absAdr;
    END;
    SetSelection(data);
  ELSE
    ClearSelection();
  END;
END SetNewSourceSelection;

(* ToggleBreakPoint - set or clear break point at selected statement *)
PROCEDURE ToggleBreakPoint();
VAR
  data: SelectionData;
BEGIN
  IF (IsProgramWaiting()) THEN
    IF (GetSelection(data)) THEN
      ClearSelection();
      WITH data DO
        IF (IsAdrBreakPoint(sdAdr)) THEN
          DeleteBreakPoint(sdAdr);
        ELSE
          CreateBreakPoint(sdAdr);
        END;
      END;
    END;
  ELSE (* Program is currently executing *)
    ClearSelection();
    ShowError(ShowErrMsgProgIsExec);
  END;
END ToggleBreakPoint;

(* SearchStringSourceWindow - search source file for a string *)
PROCEDURE SearchStringSourceWindow(VAR sw: SourceWindow);
BEGIN
  WITH sw DO
    IF (swSearchString # NIL) THEN
      ClearHighlight(sw);
      IF (NOT swSearchFirst) THEN
        swSearchStartPos := swSearchStopPos + 1;
      END;
      IF (swFileIsMod) THEN
        FindSourceStringMod(swModNo, swSearchString, swSearchLineNo,
                            swSearchStartPos, swSearchStopPos);
      ELSE
        FindSourceStringDef(swModNo, swSearchString, swSearchLineNo,
                            swSearchStartPos, swSearchStopPos);
      END;
      IF (swSearchLineNo = InvalidLineNo) THEN
        ResetSearchPosition(sw);
        ShowError(ADR(SourceErrMsgStrNotFound));
      ELSE
        swSearchFirst := FALSE;
        SetHighlight(sw, swSearchLineNo, swSearchStartPos + LineNoInfoWidth,
                     swSearchStopPos + LineNoInfoWidth);
        SetSourceWindowPos(sw);
      END;
    END;
  END;
END SearchStringSourceWindow;

(* DoSearchCommand - perform strings search command *)
PROCEDURE DoSearchCommand(VAR sw: SourceWindow; continue: BOOLEAN);
VAR
  str: ARRAY [0..RequestMaxStringLength] OF CHAR;
BEGIN
  WITH sw DO
    IF (swDisplayOn) THEN
      IF (continue) THEN (* Continue current search *)
        SearchStringSourceWindow(sw);
      ELSE (* Begin new search *)
        IF (RequestSearchString(ADR(str[0]))) THEN
          IF (swSearchString # NIL) THEN
            DeallocString(swSearchString);
            swSearchString := NIL;
          END;
          IF (strlen(ADR(str[0])) # 0) THEN
            swSearchString := AllocString(ADR(str[0]));
            ResetSearchPosition(sw);
            SearchStringSourceWindow(sw);
          END;
        END;
      END;
    END; (* IF *)
  END; (* WITH *)
END DoSearchCommand;

(* UpdateSourceWindow - update contents of source window *)
PROCEDURE UpdateSourceWindow(VAR sw: SourceWindow);
BEGIN
  IF (IsProgramLoaded()) THEN
    WITH sw DO
      IF (swUpdateMode = UMPC) THEN
        SetSourceWindowOnProc(sw, 0);
      ELSIF (swUpdateMode = UMNone) AND (NOT swDisplayOn) THEN
        SetSourceWindowOnProc(sw, 0);
      END;
    END;
  ELSE
    ClearSourceWindow(sw);
  END;
END UpdateSourceWindow;

(* VTUpdateProc - called to output new text to window *)
PROCEDURE VTUpdateProc(Window: VTWindow; OutputRow,FirstLine,LastLine: CARDINAL);
VAR
  sw: SourceWindowPtr;
  line: CARDINAL;
  outLength: CARDINAL;
  startPos: CARDINAL;
  endPos: CARDINAL;
  bpIndex: CARDINAL;
  argo: ARRAY [0..0] OF FormatArg;
  buf: ARRAY [0..9] OF CHAR;
  lineStr: ADDRESS;
BEGIN
  sw := GetUserDataVTWindow(Window);

  WITH sw^ DO
    IF (swDisplayOn) THEN
      FOR line := FirstLine TO LastLine DO
        SetCursorPosVTWindow(Window, 0, OutputRow);

        argo[0].W := line + 1;
        outLength := sprintf(ADR(buf[0]), ADR("%04u: "), argo);
        WriteBufferVTWindow(Window, ADR(buf[0]), outLength);

        IF (swFileIsMod) THEN
          lineStr := GetSourceLineMod(swModNo, line);
        ELSE
          lineStr := GetSourceLineDef(swModNo, line);
        END;
        WriteStringVTWindow(Window, lineStr);

        IF (swFileIsMod) THEN
          (* Display Break Points if any *)
          bpIndex := 0;
          WHILE (GetBreakPointOnLineNo(swModNo,line,bpIndex,startPos,endPos)) DO
            INC(startPos, LineNoInfoWidth);
            INC(endPos, LineNoInfoWidth);
            WriteColorVTWindow(Window, OutputRow, 
                               startPos, endPos,
                               swBreakPtFgColor, swBreakPtBgColor);
          END;
        END;

        (* Check if line requires hightlighting *)
        IF (swHighlightLineNo = line) THEN
          WriteColorVTWindow(Window, OutputRow, 
                             swHighlightStartPos, swHighlightStopPos,
                             swHighlightFgColor, swHighlightBgColor);
        END;

        IF (swFileIsMod) THEN
          IF (GetSelectionLineInfo(Window, line, startPos, endPos)) THEN
            WriteColorVTWindow(Window, OutputRow, startPos, endPos,
                               swVTWindowParms.vtpDefBgColor,
                               swVTWindowParms.vtpDefFgColor);
          END;
        END;

        INC(OutputRow);
      END; (* FOR *)
    END; (* IF *)
  END; (* WITH *)
END VTUpdateProc;

(* VTSelectProc - called when left mouse button click in window *)
PROCEDURE VTSelectProc(Window: VTWindow; x, y: CARDINAL; pressed: BOOLEAN);
VAR
  sw: SourceWindowPtr;
BEGIN
  sw := GetUserDataVTWindow(Window);

  IF (sw^.swDisplayOn) AND (sw^.swFileIsMod) THEN
    IF (pressed) THEN
      IF (IsPosSelected(Window, x, y)) THEN
        ToggleBreakPoint();
      ELSE
        SetNewSourceSelection(sw^, x, y);
      END;
    ELSE
      IF (NOT IsPosSelected(Window, x, y)) THEN
        SetNewSourceSelection(sw^, x, y);
      END;
    END;
  END;
END VTSelectProc;

(* VTMenuProc - called when menu selection made *)
PROCEDURE VTMenuProc(Window: VTWindow; MenuNum, ItemNum, SubNum: CARDINAL);
VAR
  sw: SourceWindowPtr;
  redraw: BOOLEAN;
  b: BOOLEAN;
  fg: BYTE;
  bg: BYTE;
BEGIN
  sw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalMenuCommand(MenuNum, ItemNum, SubNum, LocalMenus,
                              WCSource, GetWindowIndex(sw),
                              sw^.swVTWindow)) THEN
    WITH sw^ DO
      IfCurrentWindowClearSelection(swVTWindow);

      redraw := FALSE;
      CASE (ItemNum) OF
      | 0: (* PC Update Mode *)
           swUpdateMode := UMPC;
      | 1: (* No Update Mode *)
           swUpdateMode := UMNone;
      (*2: -------------------- *)
      | 3: (* Begin String Search *)
           DoSearchCommand(sw^, FALSE);
      | 4: (* Continue String Search *)
           DoSearchCommand(sw^, TRUE);
      (*5: -------------------- *)
      | 6: (* Set Context Lines *)
           b := RequestContextLines(swContextLines);
      | 7: (* Set Window Color *)
           GetDetailPenBlockPenVTWindow(swVTWindow, fg, bg);
           IF (RequestTextFgBgColors(fg, bg)) THEN
             SetDetailPenBlockPenVTWindow(swVTWindow, fg, bg);
           END;
      | 8: (* Set Text Color *)
           WITH swVTWindowParms DO
             IF (RequestTextFgBgColors(vtpDefFgColor, vtpDefBgColor)) THEN
               SetDefaultColorVTWindow(swVTWindow, vtpDefFgColor,vtpDefBgColor);
             END;
           END;
      | 9: (* Set Highlight Color *)
           redraw := RequestTextFgBgColors(swHighlightFgColor, swHighlightBgColor);
      |10: (* Set BreakPoint Color *)
           redraw := RequestTextFgBgColors(swBreakPtFgColor, swBreakPtBgColor);
      END;
      IF (redraw) THEN
        ClearVTWindow(swVTWindow);
        RefreshVTWindow(swVTWindow);
      END;
    END;
  END;
END VTMenuProc;

(* VTDestProc - called when user clicks on destination-data gadget *)
PROCEDURE VTDestProc(Window: VTWindow);
VAR
  sw: SourceWindowPtr;
  data: SelectionData;
  absAdr: ADDRESS;
  b: BOOLEAN;
  err: BOOLEAN;
BEGIN
  sw := GetUserDataVTWindow(Window);

  IF (IsProgramLoaded()) THEN
    err    := FALSE;
    absAdr := InvalidAbsAdr;
    IF (GetSelection(data)) THEN
      WITH data DO
        IF (sdWindowClass = WCSource) OR (sdWindowClass = WCDisassembly) OR
           (sdWindowClass = WCSymData) OR (sdWindowClass = WCMemory) OR
           (sdWindowClass = WCBreakPointList) OR (sdWindowClass = WCRegisters) THEN
          absAdr := sdAdr;
          err    := absAdr = InvalidAbsAdr; (* Invalid Selection *)
        ELSIF (sdWindowClass = WCModuleList) THEN
          IF (sdModFileType = MFTDef) THEN
            b := SetSourceWindowFile(sw^, sdModNo, FALSE, TRUE);
          ELSIF (sdModFileType = MFTMod) OR (sdModFileType = MFTNone) THEN
            b := SetSourceWindowFile(sw^, sdModNo, TRUE, TRUE);
          ELSE (* Invalid ModType *)
            err := TRUE;
          END;
        ELSIF (sdWindowClass = WCProcChain) THEN
          SetSourceWindowOnProc(sw^, sdProcChainNo);
        ELSE (* Invalid Destination *)
          err := TRUE;
        END;
      END;
    ELSE (* REQUESTER *)
      b := RequestAddress(0D, absAdr);
    END;
  
    IF (absAdr # InvalidAbsAdr) THEN
      IF (IsQualifierShiftVTWindow(Window)) THEN
        absAdr := GetMemoryLong(absAdr);
      END;
      SetSourceWindowOnAdr(sw^, absAdr);
    END;
  
    IF (err) THEN
      ShowError(ShowErrMsgInvalidDest);
    END;
  END; (* IF *)
END VTDestProc;

(* VTKeyProc - called when user types key into window *)
PROCEDURE VTKeyProc(Window: VTWindow; keyBuf: ADDRESS; keyLength: CARDINAL);
VAR
  sw: SourceWindowPtr;
  ch: CHAR;
BEGIN
  sw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalKbdCommand(keyBuf, keyLength, WCSource,
                             GetWindowIndex(sw))) THEN
    IF (sw^.swDisplayOn) THEN
      IF (keyLength = 1) THEN
        ch := CHAR(keyBuf^);
        IF (ch = KeyBeginSearch) THEN (* Begin new search *)
          DoSearchCommand(sw^, FALSE);
        ELSIF (ch = KeyContinueSearch) THEN (* Continue current search *)
          DoSearchCommand(sw^, TRUE);
        END
      END;
    END;
  END;
END VTKeyProc;

(* VTCloseProc - called when close-box hit *)
PROCEDURE VTCloseProc(Window: VTWindow);
VAR
  sw: SourceWindowPtr;
BEGIN
  sw := GetUserDataVTWindow(Window);
  CloseSourceWindow(sw^);
END VTCloseProc;

(* VTErrorProc - called when failed to alloc text info for resized window *)
PROCEDURE VTErrorProc(Window: VTWindow);
VAR
  sw: SourceWindowPtr;
BEGIN
  sw := GetUserDataVTWindow(Window);
  CloseSourceWindowNoMemory(sw^);
END VTErrorProc;

(* ===== PUBLIC ===== *)

(* OpenSourceWindow - open a new source window *)
PROCEDURE OpenSourceWindow(VAR config: ConfigWindow): BOOLEAN;
VAR
  sw: SourceWindowPtr;
  ok: BOOLEAN;
  nvt: NewVTWindow;
BEGIN
  ok := FALSE;
  sw := AddWindowNode();
  IF (sw # NIL) THEN
    WITH nvt DO
      WITH config DO
        nvtLeftEdge  := cwX;
        nvtTopEdge   := cwY;
        nvtWidth     := cwWidth;
        nvtHeight    := cwHeight;
        nvtDetailPen := cwDetailPen;
        nvtBlockPen  := cwBlockPen;
        nvtTitle     := ADR(DefSourceWindowTitle);
        nvtMinWidth  := DefMinWindowWidth;
        nvtMinHeight := DefMinWindowHeight;
        nvtMaxWidth  := DefMaxWindowWidth;
        nvtMaxHeight := DefMaxWindowHeight;
        nvtMsgPort   := GetVTWindowsMsgPort();
        nvtUserData  := sw;
      END; (* WITH *)
    END; (* WITH *)
    sw^.swVTWindow := OpenVTWindow(nvt);
    ok := sw^.swVTWindow # VTWindowNIL;

    IF (ok) THEN
      WITH sw^.swVTWindowParms DO
        WITH config DO
          vtpUpdateProc    := VTUpdateProc;
          vtpSelectProc    := VTSelectProc;
          vtpMenuProc      := VTMenuProc;
          vtpDestProc      := VTDestProc;
          vtpKeyProc       := VTKeyProc;
          vtpCloseProc     := VTCloseProc;
          vtpErrorProc     := VTErrorProc;
        (*vtpInitialLine   := 0;*)
        (*vtpTotalLines    := 1;*)
        (*vtpInitialColumn := 0;*)
        (*vtpMaxColumns    := 1;*)
          vtpDefFgColor    := cwFgColor;
          vtpDefBgColor    := cwBgColor;
        END;
      END;

      WITH sw^ DO
        WITH config DO
          swTitle            := NIL;
          swDisplayOn        := TRUE;
          swUpdateMode       := UpdateMode(cwSourceUpdateMode);
          swContextLines     := cwSourceContextLines;
          swHighlightFgColor := cwSourceHighlightFgColor;
          swHighlightBgColor := cwSourceHighlightBgColor;
          swBreakPtFgColor   := cwSourceBreakPtFgColor;
          swBreakPtBgColor   := cwSourceBreakPtBgColor;
          swSearchString     := NIL;
        END;
      END;
      ClearSourceWindow(sw^);
      UpdateSourceWindow(sw^);

      ok := CreateMenuStrip(sw^);
    ELSE (* NOT ok *)
      DeleteWindowNode(sw);
    END;
  END;
  RETURN (ok);
END OpenSourceWindow;

(* UpdateSourceWindows - update contents of all source windows *)
PROCEDURE UpdateSourceWindows();
VAR
  sw: SourceWindowPtr;
  p: SourceWindowPtr;
BEGIN
  sw := SourceWindowList;
  WHILE (sw # NIL) DO
    p  := sw;
    sw := sw^.swNext;
    UpdateSourceWindow(p^);
  END;
END UpdateSourceWindows;

(* UpdateSourceWindowsShowingLine - Update color of line containg break point *)
PROCEDURE UpdateSourceWindowsShowingLine(modNo: CARDINAL; lineNo: CARDINAL);
VAR
  sw: SourceWindowPtr;
  first: CARDINAL;
  last: CARDINAL;
  line: CARDINAL;
BEGIN
  sw := SourceWindowList;
  WHILE (sw # NIL) DO
    WITH sw^ DO
      IF (swDisplayOn) AND (swFileIsMod) AND (swModNo = modNo) THEN
        GetDisplayRangeVTWindow(swVTWindow, first, last);
        IF (first <= lineNo) AND (last >= lineNo) THEN
          line := lineNo - first;
          RefreshRangeVTWindow(swVTWindow, line, line);
        END;
      END;
    END;
    sw := sw^.swNext;
  END;
END UpdateSourceWindowsShowingLine;

(* ClearSourceWindowShowingFile - clear window before source file unloaded *)
PROCEDURE ClearSourceWindowsShowingFile(modNo: CARDINAL; modFile: BOOLEAN);
VAR
  sw: SourceWindowPtr;
BEGIN
  sw := SourceWindowList;
  WHILE (sw # NIL) DO
    WITH sw^ DO
      IF (swDisplayOn) AND (swModNo = modNo) AND (swFileIsMod = modFile) THEN
        ClearSourceWindow(sw^);
      END;
    END;
    sw := sw^.swNext;
  END;
END ClearSourceWindowsShowingFile;

(* CloseSourceWindows - close all source windows *)
PROCEDURE CloseSourceWindows();
BEGIN
  WHILE (SourceWindowList # NIL) DO
    CloseSourceWindow(SourceWindowList^);
  END;
END CloseSourceWindows;

(* GetSourceWindowInfo - get config parameters of a specified window *)
PROCEDURE GetSourceWindowInfo(VAR index: CARDINAL;
                              VAR config: ConfigWindow): BOOLEAN;
VAR
  sw: SourceWindowPtr;
  count: CARDINAL;
BEGIN
  count := index;
  sw    := SourceWindowList;
  WHILE (sw # NIL) DO
    IF (count = 0) THEN
      WITH config DO
        WITH sw^ DO
          GetInfoVTWindow(swVTWindow, cwX, cwY, cwWidth, cwHeight,
                          cwDetailPen, cwBlockPen);
          cwMaxCharWidth           := 0;
          cwFgColor                := swVTWindowParms.vtpDefFgColor;
          cwBgColor                := swVTWindowParms.vtpDefBgColor;
          cwSourceUpdateMode       := swUpdateMode;
          cwSourceContextLines     := swContextLines;
          cwSourceHighlightFgColor := swHighlightFgColor;
          cwSourceHighlightBgColor := swHighlightBgColor;
          cwSourceBreakPtFgColor   := swBreakPtFgColor;
          cwSourceBreakPtBgColor   := swBreakPtBgColor;
        END;
      END;
      INC(index);
      RETURN (TRUE); (* Window Found! *)
    END;
    DEC(count);
    sw := sw^.swNext;
  END;
  RETURN (FALSE); (* End of list *)
END GetSourceWindowInfo;

(* InitDBSourceWindows - initialize module *)
PROCEDURE InitDBSourceWindows(): BOOLEAN;
BEGIN
  RETURN (TRUE);
END InitDBSourceWindows;

(* CleanupDBSourceWindows - cleanup module *)
PROCEDURE CleanupDBSourceWindows();
BEGIN
  CloseSourceWindows();
END CleanupDBSourceWindows;

END DBSourceWindows.
