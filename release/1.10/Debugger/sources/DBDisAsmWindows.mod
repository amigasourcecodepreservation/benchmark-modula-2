
(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBDisAsmWindows.MOD               Version: Amiga.01.10             *
 * Created: 02/12/89   Updated: 06/08/89   Author: Leon Frenkel             *
 * Description: Debugger Disassembler windows handling module.              *
 ****************************************************************************)

IMPLEMENTATION MODULE DBDisAsmWindows;

FROM SYSTEM IMPORT
  ADDRESS, BYTE,
  ADR;
FROM CMemOp IMPORT
  setmem, movmem;
FROM CPrintBuffer IMPORT
  sprintf;
FROM CStrings IMPORT
  strcat, strlen;
FROM DBBreakPoints IMPORT
  GetBreakPointInAdrRange, IsAdrBreakPoint, CreateBreakPoint,
  DeleteBreakPoint;
FROM DBConfiguration IMPORT
  ConfigWindow;
FROM DBDisassembler IMPORT
  DisassembleInst;
FROM DBFiles IMPORT
  InvalidAbsAdr, InvalidModNo, FileType,
  GetModuleCodeBase, GetModuleCodeEnd, GetInitCodeBase, GetInitCodeEnd,
  GetModNo, IsAdrInitCode, GetModuleName;
FROM DBMenusKbd IMPORT
  LinkGlobalMenuStrip, DoGlobalMenuCommand, DoGlobalKbdCommand,
  AddMenuItemWithCheck, AddMenuItemWithCheckList, AddMenuItemNoHighlight;
FROM DBMisc IMPORT
  AllocMemory, DeallocMemory, AllocString, DeallocString, GetMemoryWord,
  GetMemoryLong;
FROM DBRequesters IMPORT
  RequestTextFgBgColors, RequestTextWidth, RequestAddress;
FROM DBSelection IMPORT
  SelectionData, ModuleFileType,
  ClearSelection, GetSelection, SetSelection,
  IsDisAsmPosSelected, GetDisAsmSelectionLineInfo,
  IfCurrentWindowClearSelection;
FROM DBStatusWindow IMPORT
  ShowErrMsgNoMemory, ShowErrMsgInvalidDest, ShowErrMsgProgIsExec,
  ShowError;
FROM DBTextWindows IMPORT
  DefMinWindowWidth, DefMinWindowHeight, DefMaxWindowWidth, DefMaxWindowHeight,
  VTWindow, NewVTWindow, VTWindowNIL, VTWindowParms,
  OpenVTWindow, CloseVTWindow, InitParmsVTWindow, WriteBufferVTWindow,
  SetCursorPosVTWindow, GetUserDataVTWindow, WriteColorVTWindow, ClearVTWindow,
  RefreshVTWindow, GetDisplayRangeVTWindow, WriteStringVTWindow,
  RefreshRangeVTWindow, GetIntuiWindowVTWindow, SetScrollUpdateModeVTWindow,
  ScrollVTWindow, SetTitleVTWindow, GetInfoVTWindow, SetMenuStripVTWindow,
  ClearMenuStripVTWindow, SetDetailPenBlockPenVTWindow, SetDefaultColorVTWindow,
  GetDetailPenBlockPenVTWindow, IsQualifierShiftVTWindow;
FROM DBUserProgram IMPORT
  ProcChainNodePtr,
  GetProcChainNode, GetProcChainLength, IsProgramLoaded, IsProgramWaiting,
  GetRegisterPCValue;
FROM DBWindows IMPORT
  WindowClass,
  GetVTWindowsMsgPort;
FROM FormatString IMPORT
  FormatArg;
FROM Intuition IMPORT
  Menu, MenuPtr, MenuItemMutualExcludeSet;
FROM Memory IMPORT
  MemReqSet, MemClear;
FROM SimpleMenus IMPORT
  BeginMenuStrip, EndMenuStrip, FreeMenuStrip, AddMenu, AddMenuItem;

CONST
  (* Disassembly window title when no data is being displayed *)
  DefDisAsmWindowTitle = "Disassembly";

  LocalMenus = 1; (* Number of window specific menus *)

  (* Disassembly window title when displaying program init code *)
  ProgramInitCodeTitle = "Program Initialization";

  ModuleBodyCodeOffset = 4D; (* Code offset to first inst of module body *)

TYPE
  (* Instruction address start end lookup record *)
  AdrLookup = RECORD
                alStart : ADDRESS;
                alEnd   : ADDRESS;
              END;

  (* Array of instruction address lookup records *)
  AdrLookupArrayPtr = POINTER TO AdrLookupArray;
  AdrLookupArray = ARRAY [0..1023] OF AdrLookup;

  (* Disassembly window update mode *)
  UpdateMode = (UMPC, UMNone);

  (* Disassembly window descriptor *)
  DisAsmWindowPtr = POINTER TO DisAsmWindow;
  DisAsmWindow = RECORD
                   awNext             : DisAsmWindowPtr; (* next window *)
                   awVTWindow         : VTWindow;      (* text window *)
                   awVTWindowParms    : VTWindowParms; (* window parms *)
                   awMenuStrip        : MenuPtr;       (* local menu strip *)
                   awTitle            : ADDRESS;       (* ptr to title string *)
                   awDisplayOn        : BOOLEAN;       (* displaying data *)
                   awUpdateMode       : UpdateMode;    (* window update mode *)
                   awShowAdr          : BOOLEAN;       (* Show absolute adr *)
                   awShowOffset       : BOOLEAN;       (* Show relative ofs *)
                   awShowObjCode      : BOOLEAN;       (* Show object code *)
                   awFirstLine        : CARDINAL;      (* first display line *)
                   awHeight           : CARDINAL;      (* current height *)
                   awAdrLookup        : AdrLookupArrayPtr; (* ptr to lookup *)
                   awMaxColumns       : CARDINAL;      (* # of columns *)
                   awRangeStartAdr    : ADDRESS;       (* disasm start adr *)
                   awRangeEndAdr      : ADDRESS;       (* disasm end adr *)
                   awRangeNamed       : BOOLEAN;       (* T: Named, F: Not *)
                   awDispStartAdr     : ADDRESS;       (* display start adr *)
                   awDispEndAdr       : ADDRESS;       (* display end adr *)
                   awHighlightAdr     : ADDRESS;       (* highlight adr *)
                   awHighlightFgColor : BYTE;          (* highlight color fg *)
                   awHighlightBgColor : BYTE;          (* highlight color bg *)
                   awBreakPtFgColor   : BYTE;          (* BreakPoint color fg *)
                   awBreakPtBgColor   : BYTE;          (* BreakPoint color bg *)
                 END;

VAR
  DisAsmWindowList : DisAsmWindowPtr; (* list of disassembly windows *)

(* ===== PRIVATE ===== *)

(* DeallocAdrLookupArray - deallocate adr lookup array if allocated *)
PROCEDURE DeallocAdrLookupArray(VAR aw: DisAsmWindow);
BEGIN
  WITH aw DO
    IF (awAdrLookup # NIL) THEN
      DeallocMemory(awAdrLookup);
      awAdrLookup := NIL;
    END;
  END;
END DeallocAdrLookupArray;

(* AllocAdrLookupArray - allocate dynamic adr lookup array *)
PROCEDURE AllocAdrLookupArray(VAR aw: DisAsmWindow; height: CARDINAL): BOOLEAN;
BEGIN
  DeallocAdrLookupArray(aw);
  WITH aw DO
    awHeight    := height;
    awAdrLookup := AllocMemory(SIZE(AdrLookup) * awHeight, MemReqSet{});
    RETURN (awAdrLookup # NIL);
  END;
END AllocAdrLookupArray;

(* FindDisplayLineFromAdr - given an address return a display line # *)
PROCEDURE FindDisplayLineFromAdr(VAR aw: DisAsmWindow; adr: ADDRESS;
                                 VAR dispLine: CARDINAL): BOOLEAN;
VAR
  i: CARDINAL;
BEGIN
  WITH aw DO
    IF (awDisplayOn) AND (awAdrLookup # NIL) AND 
       (awDispStartAdr <= adr) AND (awDispEndAdr > adr) THEN
      FOR i := 0 TO awHeight-1 DO
         IF (awAdrLookup^[i].alStart <= adr) AND 
            (awAdrLookup^[i].alEnd > adr) THEN
           dispLine := i;
           RETURN (TRUE); (* Found! *)
         END;
      END;
    END;
  END;
  RETURN (FALSE); (* Not found *)
END FindDisplayLineFromAdr;

(* AddWindowNode - create a window node and link into window list *)
PROCEDURE AddWindowNode(): DisAsmWindowPtr;
VAR
  aw: DisAsmWindowPtr;
BEGIN
  aw := AllocMemory(SIZE(DisAsmWindow), MemReqSet{MemClear});
  IF (aw # NIL) THEN
    IF (DisAsmWindowList # NIL) THEN
      aw^.awNext := DisAsmWindowList;
    END;
    DisAsmWindowList := aw;
  END;
  RETURN (aw);
END AddWindowNode;

(* DeleteWindowNode - delete window node and unlink from window list *)
PROCEDURE DeleteWindowNode(aw: DisAsmWindowPtr);
VAR
  prev: DisAsmWindowPtr;
  cur: DisAsmWindowPtr;
BEGIN
  prev := NIL;
  cur  := DisAsmWindowList;
  WHILE (cur # aw) DO
    prev := cur;
    cur  := cur^.awNext;
  END;

  IF (prev = NIL) THEN (* First in list *)
    DisAsmWindowList := aw^.awNext;
  ELSE (* Not first in list *)
    prev^.awNext := aw^.awNext;
  END;

  DeallocMemory(aw);
END DeleteWindowNode;

(* GetWindowIndex - get position of window node in list *)
PROCEDURE GetWindowIndex(aw: DisAsmWindowPtr): CARDINAL;
VAR
  index: CARDINAL;
  p: DisAsmWindowPtr;
BEGIN
  index := 0;
  p     := DisAsmWindowList;
  WHILE (aw # p) DO
    INC(index);
    p := p^.awNext;
  END;
  RETURN (index);
END GetWindowIndex;

(* CloseDisAsmWindow - close disassembly window *)
PROCEDURE CloseDisAsmWindow(VAR aw: DisAsmWindow);
BEGIN
  WITH aw DO
    IfCurrentWindowClearSelection(awVTWindow);

    DeallocAdrLookupArray(aw);

    IF (awMenuStrip # NIL) THEN
      ClearMenuStripVTWindow(awVTWindow);
      FreeMenuStrip(awMenuStrip^);
    END;

    CloseVTWindow(awVTWindow);

    IF (awTitle # NIL) THEN
      DeallocString(awTitle);
    END;
  END;
  DeleteWindowNode(ADR(aw));
END CloseDisAsmWindow;

(* CloseDisAsmWindowNoMemory - close window and show no memory err msg *)
PROCEDURE CloseDisAsmWindowNoMemory(VAR aw: DisAsmWindow);
BEGIN
  CloseDisAsmWindow(aw);
  ShowError(ShowErrMsgNoMemory);
END CloseDisAsmWindowNoMemory;

(* CreateMenuStrip - create a local menu strip for a window *)
PROCEDURE CreateMenuStrip(VAR aw: DisAsmWindow): BOOLEAN;
TYPE
  M = MenuItemMutualExcludeSet;

VAR
  ms: MenuPtr;
  ok: BOOLEAN;
BEGIN
  BeginMenuStrip();
    AddMenu(                           ADR("Disassembly "));
      AddMenuItemWithCheckList(          ADR(" PC Update Mode"), (aw.awUpdateMode = UMPC), M{1});
      AddMenuItemWithCheckList(          ADR(" No Update Mode"), (aw.awUpdateMode = UMNone), M{0});
      AddMenuItemNoHighlight(            ADR(" --------------------"));
      AddMenuItemWithCheck(              ADR(" Show Address"), aw.awShowAdr);
      AddMenuItemWithCheck(              ADR(" Show Offset"), aw.awShowOffset);
      AddMenuItemWithCheck(              ADR(" Show Object Code     "), aw.awShowObjCode);
      AddMenuItemNoHighlight(            ADR(" --------------------"));
      AddMenuItem(                       ADR(" Set Text Width"));
      AddMenuItem(                       ADR(" Set Window Color"));
      AddMenuItem(                       ADR(" Set Text Color"));
      AddMenuItem(                       ADR(" Set Highlight Color"));
      AddMenuItem(                       ADR(" Set BreakPoint Color"));
  ms := EndMenuStrip();
  ok := (ms # NIL);

  IF (ok) THEN
    LinkGlobalMenuStrip(ms^);
    WITH aw DO
      awMenuStrip := ms;
      SetMenuStripVTWindow(awVTWindow, awMenuStrip^);
    END;
  ELSE
    CloseDisAsmWindowNoMemory(aw);
  END;
  RETURN (ok);
END CreateMenuStrip;

(* SetNewTextWidth - change text width of window *)
PROCEDURE SetNewTextWidth(VAR aw: DisAsmWindow);
BEGIN
  WITH aw DO
    IF (awDisplayOn) THEN
      IfCurrentWindowClearSelection(awVTWindow);
      awVTWindowParms.vtpMaxColumns := awMaxColumns;
      IF (NOT InitParmsVTWindow(awVTWindow, awVTWindowParms)) THEN
        CloseDisAsmWindowNoMemory(aw);
      END;
    END;
  END;
END SetNewTextWidth;

(* SetDefaultWindowTitle - set default disassembly window title *)
PROCEDURE SetDefaultWindowTitle(VAR aw: DisAsmWindow);
BEGIN
  WITH aw DO
    SetTitleVTWindow(awVTWindow, ADR(DefDisAsmWindowTitle));
    IF (awTitle # NIL) THEN
      DeallocString(awTitle);
    END;
    awTitle := NIL;
  END;
END SetDefaultWindowTitle;

(* SetNewWindowTitle - set a new disassembly window title *)
PROCEDURE SetNewWindowTitle(VAR aw: DisAsmWindow; title: ADDRESS);
VAR
  newTitle: ADDRESS;
BEGIN
  newTitle := AllocString(title);
  IF (newTitle # NIL) THEN
    WITH aw DO
      SetTitleVTWindow(awVTWindow, newTitle);
      IF (awTitle # NIL) THEN
        DeallocString(awTitle);
      END;
      awTitle := newTitle;
    END;
  END;
END SetNewWindowTitle;

(* CreateNewWindowTitle - create and set a new window title string *)
PROCEDURE CreateNewWindowTitle(VAR aw: DisAsmWindow; name: ADDRESS);
VAR
  title: ADDRESS;
  i: CARDINAL;
  argo: ARRAY [0..9] OF FormatArg;
  str: ARRAY [0..31] OF CHAR;
BEGIN
  IF (name # NIL) THEN
    title := name;
  ELSE
    title := ADR(str[0]);
    WITH aw DO
      argo[0].L := awRangeStartAdr;
      argo[1].L := awRangeEndAdr;
      i         := sprintf(title, ADR("[%08lx..%08lx]"), argo);
    END;
  END;
  SetNewWindowTitle(aw, title);
END CreateNewWindowTitle;

(* ClearDisAsmWindow - disable display of data in window *)
PROCEDURE ClearDisAsmWindow(VAR aw: DisAsmWindow);
BEGIN
  WITH aw DO
    IF (awDisplayOn) THEN
      IfCurrentWindowClearSelection(awVTWindow);
      SetDefaultWindowTitle(aw);
      awDisplayOn := FALSE;
      WITH awVTWindowParms DO
        vtpInitialLine := 0;
        vtpTotalLines  := 1;
        vtpMaxColumns  := 1;
      END;
      IF (NOT InitParmsVTWindow(awVTWindow, awVTWindowParms)) THEN
        CloseDisAsmWindowNoMemory(aw);
      END;
    END;
  END;
END ClearDisAsmWindow;

(* ClearHighlight - if a highlight line exists clear the highlighting *)
PROCEDURE ClearHighlight(VAR aw: DisAsmWindow);
VAR
  dispLine: CARDINAL;
  adr: ADDRESS;
BEGIN
  WITH aw DO
    IF (awDisplayOn) AND (awHighlightAdr # InvalidAbsAdr) THEN
      adr := awHighlightAdr;
      awHighlightAdr := InvalidAbsAdr;
      IF (FindDisplayLineFromAdr(aw, adr, dispLine)) THEN
        RefreshRangeVTWindow(awVTWindow, dispLine, dispLine);
      END;
    END;
  END;
END ClearHighlight;

(* SetHighlight - set line to be highlighted in disassembly window *)
PROCEDURE SetHighlight(VAR aw: DisAsmWindow; adr: ADDRESS);
VAR
  dispLine: CARDINAL;
BEGIN
  WITH aw DO
    IF (awDisplayOn) THEN
      ClearHighlight(aw);

      awHighlightAdr := adr;
      IF (FindDisplayLineFromAdr(aw, awHighlightAdr, dispLine)) THEN
        RefreshRangeVTWindow(awVTWindow, dispLine, dispLine);
      END;
    END;
  END;
END SetHighlight;

(* SetDisAsmWindowRange - setup disassembly window to display adr range *)
PROCEDURE SetDisAsmWindowRange(VAR aw: DisAsmWindow; adr: ADDRESS): BOOLEAN;
VAR
  ok: BOOLEAN;
  rangeName: ADDRESS;
  totalLines: CARDINAL;
  initialLine: CARDINAL;
  dispFirst: CARDINAL;
  dispLast: CARDINAL;
  modNo: CARDINAL;
BEGIN
  WITH aw DO
    ok := (awDisplayOn) AND (awRangeStartAdr <= adr) AND (awRangeEndAdr > adr);

    GetModNo(adr, modNo);

    IF (ok) AND (NOT awRangeNamed) THEN (* Adr in unnamed memory range *)
      (* Determine if a new more accurate range should be shown *)
      ok := NOT ((modNo # InvalidModNo) OR (IsAdrInitCode(adr)));
    END;

    ClearHighlight(aw);
    IfCurrentWindowClearSelection(awVTWindow);

    IF (NOT ok) THEN
      IF (ODD(adr)) THEN
        DEC(adr);
      END;

      IF (modNo # InvalidModNo) THEN
        awRangeStartAdr := GetModuleCodeBase(modNo) + ModuleBodyCodeOffset;
        awRangeEndAdr   := GetModuleCodeEnd(modNo);
        rangeName       := GetModuleName(modNo);
      ELSIF (IsAdrInitCode(adr)) THEN
        awRangeStartAdr := GetInitCodeBase();
        awRangeEndAdr   := GetInitCodeEnd();
        rangeName       := ADR(ProgramInitCodeTitle);
      ELSE
        IF (adr < 32768D) THEN
          awRangeStartAdr := 0D;          
          awRangeEndAdr   := 65536D;
        ELSIF (adr >= (0FFFFFFFEH - 32768D)) THEN
          awRangeStartAdr := 0FFFFFFFEH - 65536D;
          awRangeEndAdr   := 0FFFFFFFEH;
        ELSE
          awRangeStartAdr := adr - 32768D;
          awRangeEndAdr   := adr + 32768D;
        END;

        rangeName     := NIL;
      END;
      awRangeNamed := rangeName # NIL;

      CreateNewWindowTitle(aw, rangeName);

      totalLines  := (awRangeEndAdr - awRangeStartAdr) DIV 2D;
      initialLine := (adr - awRangeStartAdr) DIV 2D;
      
      IF (awDisplayOn) AND (totalLines = awVTWindowParms.vtpTotalLines) THEN
        GetDisplayRangeVTWindow(awVTWindow, dispFirst, dispLast);
        IF (dispFirst # initialLine) THEN
          ScrollVTWindow(awVTWindow, 0, initialLine);
        ELSE
          RefreshVTWindow(awVTWindow);
        END;
        ok := TRUE;
      ELSE
        WITH awVTWindowParms DO
          vtpInitialLine := initialLine;
          vtpTotalLines  := totalLines;
          vtpMaxColumns  := awMaxColumns;
        END;
        awDisplayOn := TRUE;
        ok := InitParmsVTWindow(awVTWindow, awVTWindowParms);
        IF (NOT ok) THEN
          CloseDisAsmWindowNoMemory(aw);
        END;
      END;
    END;
  END;
  RETURN (ok);
END SetDisAsmWindowRange;

(* SetDisAsmWindowPos - position disassembly window on highlight address *)
PROCEDURE SetDisAsmWindowPos(VAR aw: DisAsmWindow);
VAR
  line: CARDINAL;
  newCol: CARDINAL;
  first: CARDINAL;
  last: CARDINAL;
BEGIN
  WITH aw DO
    IF (awHighlightAdr # InvalidAbsAdr) THEN
      IF (NOT FindDisplayLineFromAdr(aw, awHighlightAdr, line)) THEN
        newCol := 0;
        line   := (awHighlightAdr - awRangeStartAdr) DIV 2D;
        ScrollVTWindow(awVTWindow, newCol, line);
      END;
    END;
  END;
END SetDisAsmWindowPos;

(* SetDisAsmWindowOnAdr - setup for disassembly and highlight specified adr *)
PROCEDURE SetDisAsmWindowOnAdr(VAR aw: DisAsmWindow; adr: ADDRESS);
BEGIN
  IF (SetDisAsmWindowRange(aw, adr)) THEN
    SetHighlight(aw, adr);
    SetDisAsmWindowPos(aw);
  END;
END SetDisAsmWindowOnAdr;

(* UpdateDisAsmWindow - update contents of disassembly window *)
PROCEDURE UpdateDisAsmWindow(VAR aw: DisAsmWindow);
VAR
  new: BOOLEAN;
  length: CARDINAL;
  pcn: ProcChainNodePtr;
BEGIN
  IF (IsProgramLoaded()) THEN
    WITH aw DO
      IF (awUpdateMode = UMPC) THEN
        SetDisAsmWindowOnAdr(aw, GetRegisterPCValue());
      ELSIF (awUpdateMode = UMNone) AND (NOT awDisplayOn) THEN
        SetDisAsmWindowOnAdr(aw, 0D);
      END;
    END;
  ELSE
    ClearDisAsmWindow(aw);
  END;
END UpdateDisAsmWindow;

(* SetNewDisAsmSelection - setup new user selection based on y position *)
PROCEDURE SetNewDisAsmSelection(VAR aw: DisAsmWindow; y: CARDINAL);
VAR
  data: SelectionData;
  absAdr: ADDRESS;
BEGIN
  absAdr := aw.awAdrLookup^[y].alStart;
  IF (absAdr # InvalidAbsAdr) THEN (* Instruction selected *)
    WITH data DO
      sdWindowClass := WCDisassembly;
      sdVTWindow    := aw.awVTWindow;
      sdLineNo      := 0;  (* Disassembly window pos is adr not line # based *)
      sdStartPos    := 0;
      sdEndPos      := aw.awVTWindowParms.vtpMaxColumns - 1;
      sdDefFgColor  := aw.awVTWindowParms.vtpDefFgColor;
      sdDefBgColor  := aw.awVTWindowParms.vtpDefBgColor;
      sdAdr         := absAdr;
    END;
    SetSelection(data);
  ELSE (* Empty line. No instruction at this line. *)
    ClearSelection();
  END;
END SetNewDisAsmSelection;

(* ToggleBreakPoint - set or clear break point at selected instruction *)
PROCEDURE ToggleBreakPoint();
VAR
  data: SelectionData;
BEGIN
  IF (IsProgramWaiting()) THEN
    IF (GetSelection(data)) THEN
      ClearSelection();
      WITH data DO
        IF (IsAdrBreakPoint(sdAdr)) THEN
          DeleteBreakPoint(sdAdr);
        ELSE
          CreateBreakPoint(sdAdr);
        END;
      END;
    END;
  ELSE (* Program is currently executing *)
    ClearSelection();
    ShowError(ShowErrMsgProgIsExec);
  END;
END ToggleBreakPoint;

(* VTUpdateProc - called to output new text to window *)
PROCEDURE VTUpdateProc(Window: VTWindow; OutputRow,FirstLine,LastLine: CARDINAL);
VAR
  aw: DisAsmWindowPtr;
  line: CARDINAL;
  lineWidth: CARDINAL;
  redrawAll: BOOLEAN;
  dispFirst: CARDINAL;
  dispLast: CARDINAL;
  dispHeight: CARDINAL;
  updateHeight: CARDINAL;
  xPos: CARDINAL;
  startPos: CARDINAL;
  endPos: CARDINAL;
  instValid: BOOLEAN;
  instWordLength: CARDINAL;
  instAdr: ADDRESS;
  lineStartAdr: ADDRESS;
  lineEndAdr: ADDRESS;
  instEndAdr: ADDRESS;
  outLength: CARDINAL;
  instStr: ARRAY [0..63] OF CHAR;
  outStr: ARRAY [0..31] OF CHAR;
  argo: ARRAY [0..9] OF FormatArg;
BEGIN
  aw := GetUserDataVTWindow(Window);

  WITH aw^ DO
    IF (awDisplayOn) THEN

      lineWidth := awVTWindowParms.vtpMaxColumns;

      GetDisplayRangeVTWindow(awVTWindow, dispFirst, dispLast);
      dispHeight := dispLast - dispFirst + 1;

      redrawAll := (FirstLine # LastLine) OR (dispHeight = 1);

      IF (awAdrLookup = NIL) OR (awHeight # dispHeight) THEN
        IF (NOT AllocAdrLookupArray(aw^, dispHeight)) THEN
          ShowError(ShowErrMsgNoMemory);
          RETURN; (* Done *)
        END;
        awFirstLine := dispFirst;
      ELSIF (awFirstLine # dispFirst) THEN
        awFirstLine := dispFirst;
      END;

      IF (redrawAll) THEN
        instAdr        := awRangeStartAdr + (LONGCARD(dispFirst) * 2D);
        awDispStartAdr := instAdr;
      END;

      FOR line := FirstLine TO LastLine DO
        IF (redrawAll) THEN
          DisassembleInst(instAdr, ADR(instStr[0]), instValid, instWordLength);

          instEndAdr := instAdr + LONGCARD(instWordLength * 2);

          IF (instEndAdr > awRangeEndAdr) OR (instEndAdr < instAdr) THEN
            (* This will cause the same instruction to get disassembled *)
            instEndAdr := instAdr;

            awAdrLookup^[OutputRow].alStart := InvalidAbsAdr;
            awAdrLookup^[OutputRow].alEnd   := InvalidAbsAdr;
          ELSE
            awAdrLookup^[OutputRow].alStart := instAdr;
            awAdrLookup^[OutputRow].alEnd   := instEndAdr;

            SetCursorPosVTWindow(Window, 0, OutputRow);
            xPos := 0;

            IF (awShowOffset) THEN
              argo[0].W := CARDINAL(instAdr - awRangeStartAdr);
              outLength := sprintf(ADR(outStr[0]), ADR("[%04x]"), argo);
              WriteBufferVTWindow(Window, ADR(outStr[0]), outLength);
              INC(xPos, outLength);
            END;

            IF (awShowAdr) THEN
              argo[0].L := instAdr;
              outLength := sprintf(ADR(outStr[0]), ADR("%08lx"), argo);
              WriteBufferVTWindow(Window, ADR(outStr[0]), outLength);
              INC(xPos, outLength);
            END;

            IF (awShowAdr) OR (awShowOffset) THEN
              WriteBufferVTWindow(Window, ADR(": "), 2);
              INC(xPos, 2);
            END;

            IF (awShowObjCode) THEN
              WHILE (instWordLength # 0) DO
                argo[0].W := GetMemoryWord(instAdr);
                outLength := sprintf(ADR(outStr[0]), ADR("%04x "), argo);
                WriteBufferVTWindow(Window, ADR(outStr[0]), outLength);
                DEC(instWordLength);
                INC(instAdr, 2);
              END;
              INC(xPos, 5 * 5); (* max 5 words/inst * 5 chars each *)
            END;

            SetCursorPosVTWindow(Window, xPos, OutputRow);
            WriteStringVTWindow(Window, ADR(instStr[0]));
          END;
          instAdr := instEndAdr;
        ELSE (* Updating Color Only! *)
          WriteColorVTWindow(Window, OutputRow, 0, lineWidth - 1,
                                   awVTWindowParms.vtpDefFgColor,
                                   awVTWindowParms.vtpDefBgColor);
        END;

        lineStartAdr := awAdrLookup^[OutputRow].alStart;
        lineEndAdr   := awAdrLookup^[OutputRow].alEnd;

        (* Display break point if any *)
        IF (GetBreakPointInAdrRange(lineStartAdr, lineEndAdr)) THEN
          WriteColorVTWindow(Window, OutputRow, 0, lineWidth - 1,
                             awBreakPtFgColor, awBreakPtBgColor);
        END;

        (* Check if line requires highlighting *)
        IF (lineStartAdr <= awHighlightAdr) AND 
           (lineEndAdr > awHighlightAdr) THEN
          WriteColorVTWindow(Window, OutputRow, 0, lineWidth - 1,
                             awHighlightFgColor, awHighlightBgColor);
        END;

        (* Display selection if any *)
        IF (GetDisAsmSelectionLineInfo(Window, lineStartAdr, lineEndAdr,
                                       startPos, endPos)) THEN
            WriteColorVTWindow(Window, OutputRow, startPos, endPos,
                              awVTWindowParms.vtpDefBgColor,
                              awVTWindowParms.vtpDefFgColor);
        END;

        INC(OutputRow);
      END; (* FOR *)

      IF (redrawAll) THEN
        awDispEndAdr := instAdr;
      END;
    END;
  END;
END VTUpdateProc;

(* VTSelectProc - called when left mouse button click in window *)
PROCEDURE VTSelectProc(Window: VTWindow; x, y: CARDINAL; pressed: BOOLEAN);
VAR
  aw: DisAsmWindowPtr;
BEGIN
  aw := GetUserDataVTWindow(Window);

  WITH aw^ DO
    IF (awDisplayOn) AND (awAdrLookup # NIL) THEN
      DEC(y, awFirstLine); (* Convert "y" to a display relative value *)
      IF (pressed) THEN
        IF (IsDisAsmPosSelected(Window, awAdrLookup^[y].alStart,
                                awAdrLookup^[y].alEnd)) THEN
          ToggleBreakPoint();
        ELSE
          SetNewDisAsmSelection(aw^, y);
        END;
      ELSE
        IF (NOT IsDisAsmPosSelected(Window, awAdrLookup^[y].alStart,
                                    awAdrLookup^[y].alEnd)) THEN
          SetNewDisAsmSelection(aw^, y);
        END;
      END;
    END;
  END;
END VTSelectProc;

(* VTMenuProc - called when menu selection made *)
PROCEDURE VTMenuProc(Window: VTWindow; MenuNum, ItemNum, SubNum: CARDINAL);
VAR
  aw: DisAsmWindowPtr;
  redraw: BOOLEAN;
  fg: BYTE;
  bg: BYTE;
BEGIN
  aw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalMenuCommand(MenuNum, ItemNum, SubNum, LocalMenus,
                              WCDisassembly, GetWindowIndex(aw),
                              aw^.awVTWindow)) THEN
    WITH aw^ DO
      IfCurrentWindowClearSelection(awVTWindow);
      redraw := FALSE;

      CASE (ItemNum) OF
      | 0: (* PC Update Mode *)
           awUpdateMode := UMPC;
      | 1: (* No Update Mode *)
           awUpdateMode := UMNone;
      (*2: -------------------- *)
      | 3: (* Show Address *)
           awShowAdr := NOT awShowAdr;
           redraw := TRUE;
      | 4: (* Show Offset *)
           awShowOffset := NOT awShowOffset;
           redraw := TRUE;
      | 5: (* Show Object Code *)
           awShowObjCode := NOT awShowObjCode;
           redraw := TRUE;
      (*6: -------------------- *)
      | 7: (* Set Text Width *)
           IF (RequestTextWidth(awMaxColumns)) THEN
             SetNewTextWidth(aw^);
           END;
      | 8: (* Set Window Color *)
           GetDetailPenBlockPenVTWindow(awVTWindow, fg, bg);
           IF (RequestTextFgBgColors(fg, bg)) THEN
             SetDetailPenBlockPenVTWindow(awVTWindow, fg, bg);
           END;
      | 9: (* Set Text Color *)
           WITH awVTWindowParms DO
             IF (RequestTextFgBgColors(vtpDefFgColor, vtpDefBgColor)) THEN
               SetDefaultColorVTWindow(awVTWindow, vtpDefFgColor,vtpDefBgColor);
             END;
           END;
      |10: (* Set Highlight Color *)
           redraw := RequestTextFgBgColors(awHighlightFgColor, awHighlightBgColor);
      |11: (* Set BreakPoint Color *)
           redraw := RequestTextFgBgColors(awBreakPtFgColor, awBreakPtBgColor);
      END;
      IF (redraw) THEN
        ClearVTWindow(awVTWindow);
        RefreshVTWindow(awVTWindow);
      END;
    END;
  END;
END VTMenuProc;

(* VTDestProc - called when user clicks on destination-data gadget *)
PROCEDURE VTDestProc(Window: VTWindow);
VAR
  aw: DisAsmWindowPtr;
  data: SelectionData;
  chainNew: BOOLEAN;
  chainLength: CARDINAL;
  pcn: ProcChainNodePtr;
  ok: BOOLEAN;
  err: BOOLEAN;
  absAdr: ADDRESS;
BEGIN
  aw := GetUserDataVTWindow(Window);

  IF (IsProgramLoaded()) THEN
    err    := FALSE;
    absAdr := InvalidAbsAdr;
    IF (GetSelection(data)) THEN
      WITH data DO
        IF (sdWindowClass = WCSource) OR (sdWindowClass = WCDisassembly) OR
           (sdWindowClass = WCSymData) OR (sdWindowClass = WCMemory) OR
           (sdWindowClass = WCBreakPointList) OR (sdWindowClass = WCRegisters) THEN
          absAdr := sdAdr;
          err    := absAdr = InvalidAbsAdr; (* Invalid Selection *)
        ELSIF (sdWindowClass = WCModuleList) THEN
          IF (sdModFileType = MFTMod) OR (sdModFileType = MFTNone) THEN
            ok := SetDisAsmWindowRange(aw^, GetModuleCodeBase(sdModNo)+ModuleBodyCodeOffset);
          ELSE (* Invalid ModFileType *)
            err := TRUE;
          END;
        ELSIF (sdWindowClass = WCProcChain) THEN
          GetProcChainLength(chainNew, chainLength);
          IF (sdProcChainNo < chainLength) THEN
            pcn := GetProcChainNode(sdProcChainNo);
            SetDisAsmWindowOnAdr(aw^, pcn^.pcnAdr);
          END;
        ELSE (* Invalid Destination *)
          err := TRUE;
        END;
      END;
    ELSE (* REQUESTER *)
      ok := RequestAddress(0D, absAdr);
    END;
  
    IF (absAdr # InvalidAbsAdr) THEN
      IF (IsQualifierShiftVTWindow(Window)) THEN
        absAdr := GetMemoryLong(absAdr);
      END;
      SetDisAsmWindowOnAdr(aw^, absAdr);
    END;
  
    IF (err) THEN
      ShowError(ShowErrMsgInvalidDest);
    END;
  END; (* IF *)
END VTDestProc;

(* VTKeyProc - called when user types key into window *)
PROCEDURE VTKeyProc(Window: VTWindow; keyBuf: ADDRESS; keyLength: CARDINAL);
VAR
  aw: DisAsmWindowPtr;
BEGIN
  aw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalKbdCommand(keyBuf, keyLength, WCDisassembly,
                             GetWindowIndex(aw))) THEN
  END;
END VTKeyProc;

(* VTCloseProc - called when close-box hit *)
PROCEDURE VTCloseProc(Window: VTWindow);
VAR
  aw: DisAsmWindowPtr;
BEGIN
  aw := GetUserDataVTWindow(Window);
  CloseDisAsmWindow(aw^);
END VTCloseProc;

(* VTErrorProc - called when failed to alloc text info for resized window *)
PROCEDURE VTErrorProc(Window: VTWindow);
VAR
  aw: DisAsmWindowPtr;
BEGIN
  aw := GetUserDataVTWindow(Window);
  CloseDisAsmWindowNoMemory(aw^);
END VTErrorProc;

(* ===== PUBLIC ===== *)

(* OpenDisAsmWindow - open a new disassembly window *)
PROCEDURE OpenDisAsmWindow(VAR config: ConfigWindow): BOOLEAN;
VAR
  aw: DisAsmWindowPtr;
  ok: BOOLEAN;
  nvt: NewVTWindow;
BEGIN
  ok := FALSE;
  aw := AddWindowNode();
  IF (aw # NIL) THEN
    WITH nvt DO
      WITH config DO
        nvtLeftEdge  := cwX;
        nvtTopEdge   := cwY;
        nvtWidth     := cwWidth;
        nvtHeight    := cwHeight;
        nvtDetailPen := cwDetailPen;
        nvtBlockPen  := cwBlockPen;
        nvtTitle     := ADR(DefDisAsmWindowTitle);
        nvtMinWidth  := DefMinWindowWidth;
        nvtMinHeight := DefMinWindowHeight;
        nvtMaxWidth  := DefMaxWindowWidth;
        nvtMaxHeight := DefMaxWindowHeight;
        nvtMsgPort   := GetVTWindowsMsgPort();
        nvtUserData  := aw;
      END; (* WITH *)
    END; (* WITH *)
    aw^.awVTWindow := OpenVTWindow(nvt);
    ok := aw^.awVTWindow # VTWindowNIL;

    IF (ok) THEN
      SetScrollUpdateModeVTWindow(aw^.awVTWindow, TRUE);

      WITH aw^.awVTWindowParms DO
        WITH config DO
          vtpUpdateProc    := VTUpdateProc;
          vtpSelectProc    := VTSelectProc;
          vtpMenuProc      := VTMenuProc;
          vtpDestProc      := VTDestProc;
          vtpKeyProc       := VTKeyProc;
          vtpCloseProc     := VTCloseProc;
          vtpErrorProc     := VTErrorProc;
        (*vtpInitialLine   := 0;*)
        (*vtpTotalLines    := 1;*)
        (*vtpInitialColumn := 0;*)
        (*vtpMaxColumns    := 1;*)
          vtpDefFgColor    := cwFgColor;
          vtpDefBgColor    := cwBgColor;
        END;
      END;

      WITH aw^ DO
        WITH config DO
          awDisplayOn        := TRUE;
          awUpdateMode       := UpdateMode(cwDisAsmUpdateMode);
          awShowAdr          := cwDisAsmShowAdr;
          awShowOffset       := cwDisAsmShowOffset;
          awShowObjCode      := cwDisAsmShowObjCode;
          awFirstLine        := 0;
          awHeight           := 0;
          awAdrLookup        := NIL;
          awMaxColumns       := cwMaxCharWidth;
          awRangeStartAdr    := 0D;
          awRangeEndAdr      := 0D;
          awRangeNamed       := FALSE;
          awDispStartAdr     := 0D;
          awDispEndAdr       := 0D;
          awHighlightAdr     := InvalidAbsAdr;
          awHighlightFgColor := cwDisAsmHighlightFgColor;
          awHighlightBgColor := cwDisAsmHighlightBgColor;
          awBreakPtFgColor   := cwDisAsmBreakPtFgColor;
          awBreakPtBgColor   := cwDisAsmBreakPtBgColor;
        END;
      END;

      ClearDisAsmWindow(aw^);
      UpdateDisAsmWindow(aw^);

      ok := CreateMenuStrip(aw^);
    ELSE (* NOT ok *)
      DeleteWindowNode(aw);
    END;
  END;
  RETURN (ok);
END OpenDisAsmWindow;

(* UpdateDisAsmWindows - update contents of all disassembly windows *)
PROCEDURE UpdateDisAsmWindows();
VAR
  aw: DisAsmWindowPtr;
  p: DisAsmWindowPtr;
BEGIN
  aw := DisAsmWindowList;
  WHILE (aw # NIL) DO
    p  := aw;
    aw := aw^.awNext;
    UpdateDisAsmWindow(p^);
  END;
END UpdateDisAsmWindows;

(* UpdateDisAsmWindowsShowingAdr - Update color of line containg break point *)
PROCEDURE UpdateDisAsmWindowsShowingAdr(adr: ADDRESS);
VAR
  aw: DisAsmWindowPtr;
  dispLine: CARDINAL;
BEGIN
  aw := DisAsmWindowList;
  WHILE (aw # NIL) DO
    WITH aw^ DO
      IF (FindDisplayLineFromAdr(aw^, adr, dispLine)) THEN
        RefreshRangeVTWindow(awVTWindow, dispLine, dispLine);
      END;
    END;
    aw := aw^.awNext;
  END;
END UpdateDisAsmWindowsShowingAdr;

(* UpdateDisAsmSelection - update color of line containg selection cursor *)
PROCEDURE UpdateDisAsmSelection(VT: VTWindow; selAdr: ADDRESS);
VAR
  aw: DisAsmWindowPtr;
  dispLine: CARDINAL;
BEGIN
  aw := GetUserDataVTWindow(VT);
  IF (FindDisplayLineFromAdr(aw^, selAdr, dispLine)) THEN
    RefreshRangeVTWindow(VT, dispLine, dispLine);
  END;
END UpdateDisAsmSelection;

(* CloseDisAsmWindows - close all disassembly windows *)
PROCEDURE CloseDisAsmWindows();
BEGIN
  WHILE (DisAsmWindowList # NIL) DO
    CloseDisAsmWindow(DisAsmWindowList^);
  END;
END CloseDisAsmWindows;

(* GetDisAsmWindowInfo - get config parameters of a specified window *)
PROCEDURE GetDisAsmWindowInfo(VAR index: CARDINAL;
                              VAR config: ConfigWindow): BOOLEAN;
VAR
  aw: DisAsmWindowPtr;
  count: CARDINAL;
BEGIN
  count := index;
  aw    := DisAsmWindowList;
  WHILE (aw # NIL) DO
    IF (count = 0) THEN
      WITH config DO
        WITH aw^ DO
          GetInfoVTWindow(awVTWindow, cwX, cwY, cwWidth, cwHeight,
                          cwDetailPen, cwBlockPen);
          cwMaxCharWidth           := awMaxColumns;
          cwFgColor                := awVTWindowParms.vtpDefFgColor;
          cwBgColor                := awVTWindowParms.vtpDefBgColor;
          cwDisAsmUpdateMode       := awUpdateMode;
          cwDisAsmShowAdr          := awShowAdr;
          cwDisAsmShowOffset       := awShowOffset;
          cwDisAsmShowObjCode      := awShowObjCode;
          cwDisAsmHighlightFgColor := awHighlightFgColor;
          cwDisAsmHighlightBgColor := awHighlightBgColor;
          cwDisAsmBreakPtFgColor   := awBreakPtFgColor;
          cwDisAsmBreakPtBgColor   := awBreakPtBgColor;
        END;
      END;
      INC(index);
      RETURN (TRUE); (* Window Found! *)
    END;
    DEC(count);
    aw := aw^.awNext;
  END;
  RETURN (FALSE); (* End of list *)
END GetDisAsmWindowInfo;

(* InitDBDisAsmWindows - initialize module *)
PROCEDURE InitDBDisAsmWindows(): BOOLEAN;
BEGIN
  RETURN (TRUE);
END InitDBDisAsmWindows;

(* CleanupDBDisAsmWindows - cleanup module *)
PROCEDURE CleanupDBDisAsmWindows();
BEGIN
  CloseDisAsmWindows();
END CleanupDBDisAsmWindows;

END DBDisAsmWindows.
