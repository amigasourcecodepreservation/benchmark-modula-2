(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBMisc.MOD                        Version: Amiga.01.10             *
 * Created: 01/03/89   Updated: 03/21/94   Author: Leon Frenkel             *
 * Description: Debugger misc. utility functions.                           *
 ****************************************************************************)

IMPLEMENTATION MODULE DBMisc;

(***************************************************************************
 * Changes for new release 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.4 -----
 * 
 * Support ReadArgs()
 * 
 *    1. Added new utility proc, CatTag() for adding additional Tag items to
 *       an existing Tag list array.
 * 
 * SymWindow Display of LONGREAL Values
 * 
 *    1. Added an "Size8Ptr:ADDRESS" parameter to GetMemoryValue() which,
 *       if non-nil, is used to return an 8 byte (LONGREAL) value to the
 *       caller.
 *
 ***************************************************************************
*)

FROM SYSTEM IMPORT
  ADDRESS, BYTE, WORD, LONGWORD,
  ADR, INLINE, SETREG;
FROM ConsoleDevice IMPORT
  ConsoleName, ConsoleBase,
  RawKeyConvert;
FROM CStrings IMPORT
  strlen, strcpy;
FROM DBFiles IMPORT
  InvalidAbsAdr;
FROM ExecBase IMPORT
  ExecBasePtr;
FROM InputEvents IMPORT
  InputEvent, IEClass;
FROM Intuition IMPORT
  IntuiMessagePtr;
FROM Lists IMPORT
  FindName;
FROM Memory IMPORT
  MemReqSet,
  AllocMem, FreeMem;
FROM Utility IMPORT
  Tag, TagDone, TagItem;
FROM System IMPORT
  ExecBase;


TYPE 
  (* Header attached to each memory block allocated *)
  MemBlockPtr = POINTER TO MemBlock;
  MemBlock = RECORD
               Size: LONGCARD;
             END;

(* AllocMemory - dynamically allocate a block of memory *)
PROCEDURE AllocMemory(size: LONGCARD; flags: MemReqSet): ADDRESS;
VAR
  p : MemBlockPtr;
BEGIN
  INC(size, SIZE(MemBlock));  (* include memory block info in size calc *)
  size := ((size + 7D) DIV 8D) * 8D; (* calculate size as multiple of 8 *)
  p := AllocMem(size, flags);
  IF (p # NIL) THEN
    p^.Size := size;
    INC(ADDRESS(p), SIZE(MemBlock));
  END;
  RETURN(p);
END AllocMemory;

(* DeallocMemory - deallocate a dynamically allocated block of memory *)
PROCEDURE DeallocMemory(memBlock: ADDRESS);
VAR
  p : MemBlockPtr;
BEGIN
  p := memBlock - LONGCARD(SIZE(MemBlock));
  FreeMem(p, p^.Size);
END DeallocMemory;

(* ReduceMemory - reduce size of previously allocated memory block *)
PROCEDURE ReduceMemory(memBlock: ADDRESS; newSize: LONGCARD);
VAR
  p: MemBlockPtr;
BEGIN
  INC(newSize, SIZE(MemBlock)); (* include memory block info in size calc *)
  newSize := ((newSize + 7D) DIV 8D) * 8D;  (* calc size as multiple of 8 *)
  p := memBlock - LONGCARD(SIZE(MemBlock)); (* get current memory block size *)
  IF (newSize < p^.Size) THEN  (* reallocate if newsize is less than old size *)
    FreeMem(ADDRESS(p) + newSize, p^.Size - newSize);
    p^.Size := newSize;
  END;
END ReduceMemory;

(* GetMemorySize - get current size of memory block *)
PROCEDURE GetMemorySize(memBlock: ADDRESS): LONGCARD;
VAR
  p : MemBlockPtr;
BEGIN
  p := memBlock - LONGCARD(SIZE(MemBlock));
  RETURN (p^.Size);
END GetMemorySize;

(* AllocString - alloc memory for a string and copy the string *)
PROCEDURE AllocString(str: ADDRESS): ADDRESS;
VAR
  p: ADDRESS;
BEGIN
  p := AllocMemory(strlen(str) + 1, MemReqSet{});
  IF (p # NIL) THEN
    strcpy(p, str);
  END;
  RETURN(p);
END AllocString;

(* DeallocString - free dynamically allocated string *)
PROCEDURE DeallocString(str: ADDRESS);
BEGIN
  DeallocMemory(str);
END DeallocString;

(* GetMemoryByte - get a byte at a specified address *)
PROCEDURE GetMemoryByte(adr1: ADDRESS): BYTE;
BEGIN
  RETURN (adr1^);
END GetMemoryByte;

(* GetMemoryWord - get a word at a specified address *)
PROCEDURE GetMemoryWord(adr1: ADDRESS): CARDINAL;
VAR
  adr2: ADDRESS;
BEGIN
  adr2 := adr1 + 1D;
  RETURN (CARDINAL(adr1^) * 100H + CARDINAL(adr2^));
END GetMemoryWord;

(* GetMemoryLong - get a longword at a specified address *)
PROCEDURE GetMemoryLong(adr1: ADDRESS): LONGCARD;
VAR
  adr2, adr3, adr4: ADDRESS;
BEGIN
  adr2 := adr1 + 1D;
  adr3 := adr1 + 2D;
  adr4 := adr1 + 3D;
  RETURN (LONGCARD(adr1^) * LONGCARD(01000000H) + 
          LONGCARD(adr2^) * LONGCARD(00010000H) +
          LONGCARD(adr3^) * LONGCARD(00000100H) +
          LONGCARD(adr4^));
END GetMemoryLong;

(* GetMemoryValue - get a variable size value at a specified address *)
PROCEDURE GetMemoryValue(adr: ADDRESS; indirect: BOOLEAN; size: CARDINAL;
                         Size8Ptr:ADDRESS): LONGCARD;
VAR
  value: LONGCARD;
  i    : INTEGER;
BEGIN
  IF (indirect) THEN (* adr is a pointer to the actual value *)
    adr := GetMemoryLong(adr);
  END;
  value := 0D;
  IF (size = 1) THEN
    value := LONGCARD(GetMemoryByte(adr));
  ELSIF (size = 2) THEN
    value := LONGCARD(GetMemoryWord(adr));
  ELSIF (size = 4) THEN
    value := LONGCARD(GetMemoryLong(adr));
  ELSIF (size = 8) AND (Size8Ptr # NIL) THEN
    FOR i := 0 TO 7 DO
       Size8Ptr^ := adr^;
       INC(Size8Ptr, 1D);
       INC(adr, 1D);
    END;
  END;
  RETURN (value);
END GetMemoryValue;

(* GetMemoryString - get a string from a specified memory adr *)
PROCEDURE GetMemoryString(adr: ADDRESS; buf: ADDRESS; maxSize: CARDINAL);
BEGIN
  LOOP
    DEC(maxSize);
    IF (maxSize = 0) THEN EXIT; END;

    buf^ := adr^;
    IF (CHAR(buf^) = 0C) THEN EXIT; END;

    INC(buf);
    INC(adr);
  END;
  buf^ := 0C;
END GetMemoryString;

(* GetMemoryHIGH - get dynamic array HIGH() parameter value *)
PROCEDURE GetMemoryHIGH(adr: ADDRESS): CARDINAL;
BEGIN
  RETURN (GetMemoryWord(adr + 4D));
END GetMemoryHIGH;

(* PutMemoryByte - put a byte at a specified adr *)
PROCEDURE PutMemoryByte(adr: ADDRESS; value: BYTE);
BEGIN
  adr^ := value;
END PutMemoryByte;

(* PutMemoryWord - put a word at a specified adr *)
PROCEDURE PutMemoryWord(adr: ADDRESS; value: WORD);
BEGIN
  PutMemoryByte(adr     , BYTE(CARDINAL(value) DIV 100H));
  PutMemoryByte(adr + 1D, BYTE(value));
END PutMemoryWord;

(* PutMemoryLong - put a longword at a specified adr *)
PROCEDURE PutMemoryLong(adr: ADDRESS; value: LONGWORD);
BEGIN
  PutMemoryWord(adr     , WORD(LONGCARD(value) DIV 10000H));
  PutMemoryWord(adr + 2D, WORD(value));
END PutMemoryLong;

(* PutMemoryValue - put a variable size value at a specified adr *)
PROCEDURE PutMemoryValue(adr: ADDRESS; value: LONGWORD; size: CARDINAL);
BEGIN
  IF (size = 1) THEN
    PutMemoryByte(adr, BYTE(value));
  ELSIF (size = 2) THEN
    PutMemoryWord(adr, WORD(value));
  ELSIF (size = 4) THEN
    PutMemoryLong(adr, value);
  END;
END PutMemoryValue;

(* PutMemoryString - copy a string to a specified adr *)
PROCEDURE PutMemoryString(adr: ADDRESS; str: ADDRESS);
BEGIN
  strcpy(adr, str);
END PutMemoryString;

(* Compare two blocks of memory for equality *)
PROCEDURE CompareMemory(buf1, buf2: ADDRESS; length: LONGCARD): BOOLEAN;
CONST
  D1 = 1; A0 = 8; A1 = 9;
BEGIN
  SETREG(A0, buf1);
  SETREG(A1, buf2);
  SETREG(D1, length);
  INLINE(05341H);              (*       SUBQ.W #1,D1      *)
  INLINE(0B308H);              (* next: CMP.B (A0)+,(A1)+ *)
  INLINE(056C9H, 0FFFCH);      (*       DBNE  D1,next     *)
  INLINE(057C0H);              (*       SEQ.B D0          *)
  INLINE(04400H);              (*       NEG.B D0          *)
  (* Return value in D0 *)
END CompareMemory;

(* SearchMemoryValue - search memory range for a specified value *)
PROCEDURE SearchMemoryValue(start, end: ADDRESS; valueSize: CARDINAL;
                            value: LONGCARD): ADDRESS;
VAR
  curAdr: RECORD
            CASE : CARDINAL OF
            |0: A: ADDRESS;
            |1: B: POINTER TO CHAR;
            |2: W: POINTER TO CARDINAL;
            |3: L: POINTER TO LONGCARD;
            END;
          END;
BEGIN
  curAdr.A := start;
  LOOP
    IF (curAdr.A + LONGCARD(valueSize) > end) THEN
      RETURN (InvalidAbsAdr); (* Failed! *)
    END;

    IF (valueSize = 1) THEN
      IF (curAdr.B^ = CHAR(value)) THEN
        RETURN (curAdr.A);
      END;
    ELSIF (valueSize = 2) THEN
      IF (curAdr.W^ = CARDINAL(value)) THEN
        RETURN (curAdr.A);
      END;
    ELSIF (valueSize = 4) THEN
      IF (curAdr.L^ = value) THEN
        RETURN (curAdr.A);
      END;
    END;

    INC(curAdr.A, valueSize); (* Advance memory ptr by value size *)
  END; (* LOOP *)
END SearchMemoryValue;

(* BPTRtoAPTR - convert a BCPL ptr to an APTR *)
PROCEDURE BPTRtoAPTR(bptr: ADDRESS): ADDRESS;
BEGIN
  RETURN (bptr * 4D);
END BPTRtoAPTR;

(* DeadKeyConvert - translate an IDCMP RawKey msg into ascii key info *)
PROCEDURE DeadKeyConvert(msg: ADDRESS; kbuf: ADDRESS; ksize: LONGCARD): INTEGER;
VAR
  imsg: IntuiMessagePtr;
  ievent: InputEvent;
  ap: POINTER TO ADDRESS;
BEGIN
  imsg := msg;
  ap   := imsg^.IAddress;
  WITH ievent DO
    ieNextEvent    := NIL;
    ieClass        := IEClassRawKey;
    ieSubClass     := BYTE(0);
    ieCode         := imsg^.Code;
    ieQualifier    := imsg^.Qualifier;
    ieEventAddress := ap^;
 (* ieTimeStamp    := ? *)
  END;
  RETURN (RawKeyConvert(ievent, kbuf, ksize, NIL));
END DeadKeyConvert;

(*-------------------------------------------------------*)
 PROCEDURE FindTagsEnd(VAR Tags:ARRAY OF TagItem):INTEGER;
(*-------------------------------------------------------*)

VAR  i :INTEGER;

BEGIN

i := 0;
WHILE i <= INTEGER(HIGH(Tags)) DO
   IF Tags[i].tiTag = TagDone THEN
      RETURN i;
   END;
   INC(i);
END;

(*Assert(FALSE, "FindTagsEnd overrun");*)

END FindTagsEnd;

(*----------------------------------------------------------------------------*)
 PROCEDURE AsgTagPos(i:INTEGER; VAR Tags:ARRAY OF TagItem; t0:Tag; d0:LONGCARD);
(*----------------------------------------------------------------------------*)

BEGIN

Tags[i].tiTag := t0;
Tags[i].tiData := d0;
Tags[i+1].tiTag := TagDone;

END AsgTagPos;

(*================================================================*)
 PROCEDURE CatTag(VAR Tags:ARRAY OF TagItem; t0:Tag; d0:LONGCARD);
(*================================================================*)

BEGIN

AsgTagPos(FindTagsEnd(Tags), Tags, t0, d0);

END CatTag;

(* InitDBMisc - initialize DBMisc module. *)
PROCEDURE InitDBMisc(): BOOLEAN;
VAR
  execLib: ExecBasePtr;
BEGIN
  (* Find "console.device" library base for use by RawKeyConvert() *)
  execLib     := ExecBase;
  ConsoleBase := ADDRESS(FindName(ADR(execLib^.DeviceList),
                                  ADR(ConsoleName)));

  RETURN (ConsoleBase # NIL);
END InitDBMisc;

(* Cleanup DBMisc module. *)
PROCEDURE CleanupDBMisc();
END CleanupDBMisc;

END DBMisc.
