(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBFiles.DEF                       Version: Amiga.01.10             *
 * Created: 01/03/89   Updated: 03/18/89   Author: Leon Frenkel             *
 * Description: Debugger executable, symbol, source file handler module.    *
 ****************************************************************************)

DEFINITION MODULE DBFiles;

FROM SYSTEM IMPORT
  ADDRESS;

CONST
  (* Maximum fine name string length (incl. null) *)
  MaxFileNameLength = 128;

  (* Values used to indicate an undefined number *)
  InvalidModNo  = 65535;       (* Invalid module number *)
  InvalidProcNo = 65535;       (* Invalid procedure number *)
  InvalidLineNo = 65535;       (* Invalid source-line number *)
  InvalidStatNo = 65535;       (* Invalid statement number *)
  InvalidSrcPos = 0FFFFFFFFH;  (* Invalid source position *)
  InvalidModOfs = 0FFFFFFFFH;  (* Invalid module code offset *)
  InvalidAbsAdr = 0FFFFFFFFH;  (* Invalid absolute address *)

TYPE
  StructurePtr;  (* Internal type structure definition *)
  ObjectPtr;     (* Internal object definition *)

  (* Forms of structure definition *)
  StructureForm = (Undef, Bool, Char, Card, Int, Double, LCard, Real, LongReal,
                   String, Enum, Range, Pointer, Set, Array, Record, ProcTyp,
                   Opaque, Prc, Mod);

  (* "Mod" Object Types *)
  ModuleType = (MTDef, MTMod, MTLocal);

  (* A record used to refer to a specific object indirectly *)
  ItemPtr = POINTER TO Item;
  Item = RECORD
           iObj  : ObjectPtr;     (* private: Object definition *)
           iStr  : StructurePtr;  (* private: Strcture definition *)
           iBase : ADDRESS;       (* address of the object *)
           iHigh : CARDINAL;      (* HIGH() of a dynamic array *)
         END;

  (* Object information filled in by GetItemInfo() *)
  ItemInfoPtr = POINTER TO ItemInfo;
  ItemInfo = RECORD
               iiNameString  : ADDRESS;          (* Object identifier str *)
               iiTypeString  : ADDRESS;          (* Object type identifier str*)
               iiAdr         : LONGCARD;         (* Address of object *)
               iiSize        : CARDINAL;         (* Byte size of object *)
               iiIndirect    : BOOLEAN;          (* T: adr is ptr, F: real adr*)
               iiVarPar      : BOOLEAN;          (* T: VAR, F: value parm *)
               CASE iiForm   : StructureForm OF  (* Object form (tag) *)
               | Enum:  iiNofConst : CARDINAL;   (* # of enumarations *)
               | Range: iiMin      : INTEGER;    (* min value of range *)
                        iiMax      : INTEGER;    (* max value of range *)
               | Array: iiDyn      : BOOLEAN;    (* T: dynamic, F: static *)
               | Prc:   iiLevel    : CARDINAL;   (* scope of procedure *)
               | Mod:   iiModType  : ModuleType; (* module type *)
               END;
             END;

  (* Debugger data file types ".DEF",".MOD",".SBM",".RFM" *)
  FileType = (FTDef, FTMod, FTSbm, FTRfm);

  (* for FindAvailableFiles main status parameters *)
  FAFMainFileErrors = (FAFErrMainOk,                (* for MOD or RFM file *)
                       FAFErrMainNotFound,          (* for MOD or RFM file *)
                       FAFErrMainBadKey,            (* for RFM file *)
                       FAFErrMainBadDate);          (* for MOD file *)

PROCEDURE SetProgramInfo(progName: ADDRESS; progSegList: ADDRESS);
(* Setup externally loaded program info. (Post-Mortem Debug Mode).

   progName - execute file name string 
   progSegList - segment-list of program *)


PROCEDURE LoadProgram(progName: ADDRESS): BOOLEAN;
(* Load an execute file from disk. Display error msg if fails.

   progName - executable file name string

   result - TRUE = file loaded succesfully, FALSE = failed to load file *)


PROCEDURE UnLoadProgram();
(* UnLoad program if loaded via LoadProgram() else does nothing *)


PROCEDURE GetProgramSegList(): ADDRESS;
(* Get segment list of currently loaded program.

   result - program segment list *)


PROCEDURE GetModuleCount(): CARDINAL;
(* Get total number of modules contained in program.

   result - number of modules *)


PROCEDURE GetModuleName(modNo: CARDINAL): ADDRESS;
(* Get module name string for a given module number.

   modNo - module number

   result - ptr to module name string *)


PROCEDURE CreateModuleInfoArray(): BOOLEAN;
(* Read module information from executable file. Program must be in memory.
 * Display error message on failure.

   result - TRUE = success, FALSE = failed due to memory or disk error *)


PROCEDURE DeleteModuleInfoArray();
(* Free module source, symbolic info. Update source windows. *)


PROCEDURE FindAvailableFiles(VAR MainModFileStatus,
                             MainRfmFileStatus:FAFMainFileErrors): BOOLEAN;
(* Scan all source and symbol directories for program related files.
 * Display error message on error.

   MainModFileStatus   - Status of main MOD file.
   MainRfmFileStatus   - Status of main RFM file.
   result              - TRUE = success, FALSE = error encountered *)


PROCEDURE GetSourceFileDef(modNo: CARDINAL; 
                           VAR lineCount, maxWidth: CARDINAL): BOOLEAN;
(* Read .DEF source file or return info if previously loaded.  Update
 * module list window.  Display error msg if failed.

   modNo - module number
   lineCount - variable set to number of lines in file
   maxWidth - variable set to length of the longest line in file

   result - TRUE = file loaded, FALSE = file not loaded *)


PROCEDURE GetSourceFileMod(modNo: CARDINAL;
                           VAR lineCount, maxWidth: CARDINAL): BOOLEAN;
(* Read .MOD source file or return info if previously loaded.  Update
 * module list window.  Display error msg if failed.

   modNo - module number
   lineCount - variable set to number of lines in file
   maxWidth - variable set to length of the longest line in file

   result - TRUE = file loaded, FALSE = file not loaded *)


PROCEDURE FreeSourceFileDef(modNo: CARDINAL);
(* Unload .DEF file for a specified module. Update module and source windows.

   modNo - module number *)


PROCEDURE FreeSourceFileMod(modNo: CARDINAL);
(* Unload .MOD file for a specified module. Update module and source windows.

   modNo - module number *)


PROCEDURE GetSourceLineDef(modNo: CARDINAL; lineNo: CARDINAL): ADDRESS;
(* Get a .DEF source line string for a given line number.

  modNo - module number
  lineNo - line number of source file (not range checked)

  result - ptr to source line string *)


PROCEDURE GetSourceLineMod(modNo: CARDINAL; lineNo: CARDINAL): ADDRESS;
(* Get a .MOD source line string for a given line number.

  modNo - module number
  lineNo - line number of source file (not range checked)

  result - ptr to source line string *)


PROCEDURE FindSourceStringDef(modNo: CARDINAL; findStr: ADDRESS;
                              VAR lineNo, startX, stopX: CARDINAL);
(* Scan a .DEF source file for a given string.

   modNo - module number
   findStr - text string to local
   lineNo - start search on line #. variable set to line # where string found
            set to InvalidLineNo if search failed
   startX - variable set to first char in line where search string found
   stopX - variable set to last char in line where search string found *)


PROCEDURE FindSourceStringMod(modNo: CARDINAL; findStr: ADDRESS;
                              VAR lineNo, startX, stopX: CARDINAL);
(* Scan a .MOD source file for a given string.

   modNo - module number
   findStr - text string to local
   lineNo - start search on line #. variable set to line # where string found
            set to InvalidLineNo if search failed
   startX - variable set to first char in line where search string found
   stopX - variable set to last char in line where search string found *)


PROCEDURE IsAdrInitCode(adr: ADDRESS): BOOLEAN;
(* Check if a specified adr is inside the init-code segment.

   adr - address to check for init-code range

   result - TRUE = in init-code segment, FALSE = outside of init-code *)


PROCEDURE GetInitCodeBase(): LONGCARD;
(* Get adr of start of init-code segment.

   result - address of init-code segment *)


PROCEDURE GetInitCodeEnd(): LONGCARD;
(* Get adr of end of init-code segment.

   result - address of next byte after init-code segment *)


PROCEDURE GetStatementNoFromModOffset(modNo: CARDINAL; modOfs: LONGCARD; 
                                      VAR statNo: CARDINAL; 
                                      VAR firstInst: BOOLEAN);
(* Determine statement # from a module code offset. Load refrence file if
 * not already loaded.

   modNo - module number
   modOfs - module code offset
   statNo - variable set to statement number or InvalidStatNo if not found
   firstInst - variable set to TRUE if first byte of statement, FALSE if not*)


PROCEDURE GetModOffsetFromStatementNo(modNo: CARDINAL; statNo: CARDINAL;
                                      VAR modOfs: LONGCARD);
(* Determine module code offset from a statement #. Reference file must be
 * loaded.

   modNo - module number
   statNo - statement number
   modOfs - variable set to module offset or InvalidModOffset if not found *)


PROCEDURE GetSourceLineInfoFromStatementNo(modNo: CARDINAL; statNo: CARDINAL;
                                           VAR lineNo, startX, stopX: CARDINAL);
(* Get source file line info from statement number. Reference file must be
 * loaded.

   modNo - module number
   statNo - statement number
   lineNo - variable set to source line # or InvalidLineNo if not found
   startX - variable set to first character line position of statement
   stopX - variable set to last character line position of statement *)


PROCEDURE GetAdrFromLinePos(modNo: CARDINAL; lineX, lineNo: CARDINAL;
                            VAR absAdr: ADDRESS): BOOLEAN;
(* Determine absoulte code address from a lineNo, char position in .MOD file.

   modNo - module number
   lineX - source character position in line
   lineNo - source line number 
   absAdr - variable set to abs adr of instruction associated with source

   result - TRUE = found, FALSE = not found *)


PROCEDURE GetModNoFromName(name: ADDRESS; VAR modNo: CARDINAL);
(* Search list of modules for a given name and return its module number.

   name - module name string
   modNo - variable set to module number or InvalidModNo *)


PROCEDURE GetFilePath(modNo: CARDINAL; type: FileType): ADDRESS;
(* Get full file path for a given file type.

   modNo - module number
   type - type of file to get file path

   result - ptr to file path string or NIL if file not available *)


PROCEDURE GetFilesLoadStatus(modNo: CARDINAL; VAR mod, def, sbm, rfm: BOOLEAN);
(* For a given module determine which files are currently loaded.

   modNo - module number
   mod - variable set to TRUE if source .MOD file loaded
   def - variable set to TRUE if source .DEF file loaded
   sbm - variable set to TRUE if symbol .SBM file loaded
   rfm - variable set to TRUE if symbol .RFM file loaded *)


PROCEDURE GetModuleItem(modNo: CARDINAL; VAR mod: Item; sbmOk: BOOLEAN);
(* Get module object.  If symbol file not loaded then it will be loaded.

   modNo - module number
   mod - variable set to module object or empty
   sbmOk - TRUE = either .RFM or .SBM module is ok, FALSE = .RFM module only *)


PROCEDURE GetProcedureItem(VAR mod: Item; modOfs: LONGCARD; VAR proc: Item);
(* Get procedure object from a module and code offset.

   mod - module object
   modOfs - module code offset
   proc - variable set to procedure object or empty *)


PROCEDURE GetItemInfo(VAR item: Item; VAR info: ItemInfo);
(* Extract internal object information.

   item - any object or structure
   info - variable record setup with internal object information *)


PROCEDURE GetBrotherItem(VAR item: Item; count: CARDINAL);
(* Get new object from same group as specified object.  If object an
 * enumeration constant then find first constant with given value.  Otherwise
 * advance "count" number of "Var", "Field" or "Module" objects.

   item - starting item, variable set to new item
   count - enumeration value or number of objects to skip over *)


PROCEDURE GetSonItem(VAR item: Item);
(* Get object which is linked to the specified object or empty.
 * Var -> type of Var
 * Proc -> first "Var","Field" or "Module" Object local to procedure
 * Module -> first "Var","Field" or "Module" Object local to module
 * Enum -> first enumration constant object
 * Range -> range base type object
 * Pointer -> pointer base type object
 * Set -> Set base type object
 * Array -> Element type object
 * Record -> first field object

   item - variable set to the linked object or empty *)


PROCEDURE GetArrayIndexItem(VAR item: Item);
(* Given an array object return index object.

   item - variable set to array index object *)


PROCEDURE GetProcReturnItem(VAR item: Item);
(* Given a procedure object get the return type object.

   item - variable set to procedure return type object or empty *)


PROCEDURE EmptyItem(VAR item: Item): BOOLEAN;
(* Check if an object is empty.

   item - object to be checked

   result - TRUE = empty object, FALSE = not-empty object *)


PROCEDURE NumberOfSons(VAR item: Item): CARDINAL;
(* Count number of objects inside current object.

   item - parent object to check

   result - object count *)


PROCEDURE EqualItems(VAR item1, item2: Item): BOOLEAN;
(* Compare two objects for equality.

   item1 - first object
   item2 - second object

   result - TRUE = equal objects, FALSE = not equal objects *)


PROCEDURE GetModuleCodeBase(modNo: CARDINAL): LONGCARD;
(* Get adr of module code.

   modNo - module number

   result - address of module code *)


PROCEDURE GetModuleCodeEnd(modNo: CARDINAL): LONGCARD;
(* Get adr of end of module code.

   modNo - module number

   result - address of end of module code *)


PROCEDURE GetModuleDataStart(modNo: CARDINAL): LONGCARD;
(* Get adr of beginning of module data section.

   modNo - module number

   result - address of beginning of module data section *)


PROCEDURE GetModuleDataBase(modNo: CARDINAL): LONGCARD;
(* Get adr of module data base (between variables and strings)

   modNo - module number

   result - address of module data base *)


PROCEDURE GetModNo(adr: ADDRESS; VAR modNo: CARDINAL);
(* Given an address determine module number.

   adr - absolute address
   modNo - variable set to module number or InvalidModNo if not any module *)


PROCEDURE GetModNoModOfs(adr: ADDRESS; VAR modNo: CARDINAL; 
                         VAR modOfs: LONGCARD);
(* Given an address determine module number and module offset.

   adr - absolute address
   modNo - variable set to module number or InvalidModNo if not any module
   modOfs - variable set to module code offset *)


PROCEDURE InitDBFiles(): BOOLEAN;
(* Initialize DBFiles module.

   result - TRUE = ok, FALSE = failed *)


PROCEDURE CleanupDBFiles();
(* Cleanup DBFiles module. *)


END DBFiles.
