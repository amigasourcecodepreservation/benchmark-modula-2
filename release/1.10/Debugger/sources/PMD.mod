(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: PMD.MOD                           Version: Amiga.01.10             *
 * Created: 01/22/89   Updated: 03/31/89   Author: Leon Frenkel             *
 * Description: Post-Mortem Debugger exception handling module.             *
 *  WARNING: One signal-bit is used by this module.                         *
 ****************************************************************************)

IMPLEMENTATION MODULE PMD;

FROM SYSTEM IMPORT
  ADDRESS, BYTE,
  ADR, INLINE, REG, SETREG, SHIFT;
FROM System IMPORT
  DOSBase, ExecBase, StackPtr, argc, argv;

(*$L+*)

  (* ==================== AmigaDOS.DEF ==================== *)
TYPE
  BPTR = ADDRESS; (* long word pointer *)
  BSTR = ADDRESS; (* long word pointer to BCPL string *)

  (* ==================== AmigaDOS.DEF ==================== *)

  (* ==================== AmigaDOSExt.DEF ==================== *)
TYPE
  CommandLineInterfacePtr = POINTER TO CommandLineInterface;
  CommandLineInterface = 
         RECORD
           cliResult2        : LONGINT; (* Value of IoErr from last command *)
           cliSetName        : BSTR;    (* Name of current directory *)
           cliCommandDir     : BPTR;    (* Lock associated with command dir *)
           cliReturnCode     : LONGINT; (* Return code from last command *)
           cliCommandName    : BSTR;    (* Name of current command *)
           cliFailLevel      : LONGINT; (* Fail level (set by FAILAT) *)
           cliPrompt         : BSTR;    (* Current prompt (set by PROMPT) *)
           cliStandardInput  : BPTR;    (* Default (terminal) CLI input *)
           cliCurrentInput   : BPTR;    (* Current CLI input *)
           cliCommandFile    : BSTR;    (* Name of EXECUTE command file *)
           cliInteractive    : LONGINT; (* Boolean: True if prompts required *)
           cliBackground     : LONGINT; (* Boolean: True if CLI created by RUN*)
           cliCurrentOutput  : BPTR;    (* Current CLI output *)
           cliDefaultStack   : LONGINT; (* Stack size to be obtained in lwords*)
           cliStandardOutput : BPTR;    (* Default (terminal) CLI output *)
           cliModule         : BPTR;    (* SegList of currently loaded command*)
         END;
  (* ==================== AmigaDOSExt.DEF ==================== *)

  (* ==================== Nodes.DEF ==================== *)
TYPE
  NodePtr = POINTER TO Node;
  Node = RECORD
           lnSucc : NodePtr;  (* ptr to next node in the list *)
           lnPred : NodePtr;  (* ptr to previous node in the list *)
           lnType : BYTE;     (* defines the type of the node *)
           lnPri  : BYTE;     (* specifies the priority of the node *)
           lnName : ADDRESS;  (* points the name of the node or NIL *)
         END;

CONST
  NTMsgPort   = BYTE(4);
  NTMessage   = BYTE(5);

  (* ==================== Nodes.DEF ==================== *)

  (* ==================== Lists.DEF ==================== *)
TYPE
  ListPtr = POINTER TO List;
  List = RECORD
           lhHead     : NodePtr; (* points to the first node in the list *)
           lhTail     : NodePtr; (* is always NIL *)
           lhTailPred : NodePtr; (* points to the last node in the list *)
           lhType     : BYTE;    (* defines the type of nodes within the list *)
           lpad       : BYTE;    (* structure alignment byte *)
         END;
  (* ==================== Lists.DEF ==================== *)

  (* ==================== Tasks.DEF ==================== *)
CONST
  CurrentTask = NIL; (* Passed to FindTask or RemTask to indicate current task*)

  AnySignal = -1; (* Passed to AllocSignal to return any signal *)
  NoSignals = -1; (* Returned by AllocSignal to indicate no signals *)
  MaxSignal = 31; (* Highest signal number *)
  AnyTrap   = -1; (* Passed to AllocTrap to return any trap *)
  NoTraps   = -1; (* Returned by AllocTrap to indicate no traps *)
  MaxTrap   = 15; (* Highest trap number *)

  (* Predefined signal bits *)
  SigAbort  = 0;
  SigChild  = 1;
  SigBlit   = 4;
  SigSingle = 4;
  SigDos    = 8;

TYPE
  SignalRange = [AnySignal..MaxSignal];
  SignalSet   = SET OF [0..MaxSignal];

  TrapRange   = [AnyTrap..MaxTrap];
  TrapSet     = SET OF [0..MaxTrap];
  
  TaskFlags = (TProcTime, T1, T2, T3, TStackChk, TExcept, TSwitch,
               TLaunch);
  TaskFlagsSet = SET OF TaskFlags;

  TaskState = (TSInvalid, TSAdded, TSRun, TSReady, TSWait, TSExcept, TSRemoved);
  TaskStateSet = SET OF TaskState;

  TaskPtr = POINTER TO Task;
  Task = RECORD
           tcNode       : Node;
           tcFlags      : TaskFlagsSet;
           tcState      : TaskStateSet;
           tcIDNestCnt  : BYTE;      (* intr disabled nesting *)
           tcTDNestCnt  : BYTE;      (* task disabled nesting *)
           tcSigAlloc   : SignalSet; (* signals allocated *)
           tcSigWait    : SignalSet; (* signals we are waiting for *)
           tcSigRecvd   : SignalSet; (* signals we have received *)
           tcSigExcept  : SignalSet; (* signals we take excepts for *)
           tcTrapAlloc  : TrapSet;   (* traps allocated *)
           tcTrapAble   : TrapSet;   (* traps enabled *)
           tcExceptData : ADDRESS;   (* points to except data *)
           tcExceptCode : ADDRESS;   (* points to except code *)
           tcTrapData   : ADDRESS;   (* points to trap code *)
           tcTrapCode   : ADDRESS;   (* points to trap data *)
           tcSPReg      : ADDRESS;   (* stack pointer *)
           tcSPLower    : ADDRESS;   (* stack lower bound *)
           tcSPUpper    : ADDRESS;   (* stack upper bound + 2 *)
           tcSwitch     : ADDRESS;   (* task losing CPU *)
           tcLaunch     : ADDRESS;   (* task getting CPU *)
           tcMemEntry   : List;      (* allocated memory *)
           tcUserData   : ADDRESS;   (* per task data *)
         END;
  (* ==================== Tasks.DEF ==================== *)

  (* ==================== Ports.DEF ==================== *)
CONST
  PASignal  = BYTE(0); (* signal a specified task on the arrival of a new msg *)

TYPE
  MsgPortPtr = POINTER TO MsgPort;
  MsgPort = RECORD
              mpNode    : Node;
              mpFlags   : BYTE;
              mpSigBit  : BYTE;    (* signal bit number *)
              mpSigTask : TaskPtr; (* task to be signalled *)
              mpMsgList : List;    (* message linked list *)
            END;
  (* ==================== Ports.DEF ==================== *)

  (* ==================== AmigaDOSProcess.DEF ==================== *)
TYPE
  ProcessPtr = POINTER TO Process;
  Process = RECORD
              prTask           : Task;     (* Exec Task structure *)
              prMsgPort        : MsgPort;  (* BPTR address from DOS functions *)
              prPad            : INTEGER;  (* Remaining variables on 4 byte boundaries *) 
              prSegList        : BPTR;     (* Array of seg lists used by this process *)
              prStackSize      : LONGINT;  (* Size of process stack in bytes *)
              prGlobVec        : ADDRESS;  (* Global vector for this process *)
              prTaskNum        : LONGINT;  (* CLI task number or zero if a CLI *)
              prStackBase      : BPTR;     (* Ptrto high memory end of stack *)
              prResult2        : LONGINT;  (* Value of 2nd result from last call *)
              prCurrentDir     : BPTR;     (* Lock associated with current dir *)
              prCIS            : BPTR;     (* Current CLI Input Stream *)
              prCOS            : BPTR;     (* Current CLI Output Stream *)
              prConsoleTask    : ADDRESS;  (* Console handler process for current window *)
              prFileSystemTask : ADDRESS;  (* File handler process for current dirve *)
              prCLI            : BPTR;     (* pointer to ConsoleLineInterpreter*)
              prReturnAddr     : ADDRESS;  (* pointer to previous stack frame *)
              prPktWait        : ADDRESS;  (* function to be called when awaiting msg *)
              prWindowPtr      : ADDRESS;  (* Window for error printing *)
            END;
  (* ==================== AmigaDOSProcess.DEF ==================== *)

  (* ==================== Libraries.DEF ==================== *)
TYPE
  LibFlags = (LibSumming, (* we are currently checksumming *)
              LibChanged, (* we have just changed the library *)
              LibSumUsed, (* set if we should bother to sum *)
              LibDelExp); (* delayed expunge *)

  LibFlagsSet = SET OF LibFlags;
  
  LibraryPtr = POINTER TO Library;
  Library = RECORD
              libNode     : Node;
              libFlags    : LibFlagsSet;
              libpad      : BYTE;
              libNegSize  : CARDINAL; (* number of bytes before library *)
              libPosSize  : CARDINAL; (* number of bytes after library *)
              libVersion  : CARDINAL; 
              libRevision : CARDINAL;
              libIdString : ADDRESS;
              libSum      : LONGCARD; (* library checksum *)
              libOpenCnt  : CARDINAL; (* number of current opens *)
            END;
  (* ==================== Libraries.DEF ==================== *)

  (* ==================== Interrupts.DEF ==================== *)
TYPE
  IntVector = RECORD
                ivData : ADDRESS;
                ivCode : ADDRESS;
                ivNode : NodePtr;
              END;
  (* ==================== Interrupts.DEF ==================== *)

  (* ==================== Memory.DEF ==================== *)
CONST
  (* Memory Requirement Types *)
  MemPublic  =  0; (* Memory will be used by different tasks or interrup code *)
  MemChip    =  1; (* Memory within the range of the special Amiga chips *)
  MemFast    =  2; (* Memory outside the range of the special Amiga chips *)
  MemClear   = 16; (* Indicates clear memory to zero before returning *)
  MemLargest = 17; (* Get largest available memory block *)
  MemFailed  = 31; (* Returned by func AllocEntry when memory req fails *)

TYPE
  MemReqSet = SET OF [0..31];

  (* ==================== Memory.DEF ==================== *)

  (* ==================== ExecBase.DEF ==================== *)
CONST
  AF68010 = 0; (* also set for 68020 *)

TYPE
  ExecBasePtr = POINTER TO
    RECORD
      LibNode              : Library;

      SoftVer              : CARDINAL; (* kickstart release numbre *)
      LowMemChkSum         : INTEGER;
      ChkBase              : LONGCARD; (* system base pointer complement *)
      ColdCapture          : ADDRESS;     (* coldstart soft vector *)
      CoolCapture          : ADDRESS;
      WarmCapture          : ADDRESS;
      SysStkUpper          : ADDRESS;  (* system stack base (upper bound) *)
      SysStkLower          : ADDRESS;  (* top of system stack (lower bound) *)
      MaxLocMem            : LONGCARD; 
      DebugEntry           : ADDRESS;
      DebugData            : ADDRESS;
      AlertData            : ADDRESS;
      MaxExtMem            : ADDRESS;  (* top of extended mem or null if none *)

      ChkSum               : CARDINAL;

      (* Interrupt related *)
      IntVects             : ARRAY [0..15] OF IntVector;

      (* System Variables *)
      ThisTask             : TaskPtr;  (* pointer to current task *)
      IdleCount            : LONGCARD; (* idle counter *)
      DispCount            : LONGCARD; (* dispatch counter *)
      Quantum              : CARDINAL; (* time slice quantum *)
      Elapsed              : CARDINAL; (* current quantum ticks *)
      SysFlags             : BITSET;   (* misc system flags *)
      IDNestCnt            : BYTE;     (* interrupt disable nesting count *)
      TDNestCnt            : BYTE;     (* task disable nesting count *)

      AttnFlags            : BITSET;   (* special attention flags *)
    END;
  (* ==================== ExecBase.DEF ==================== *)

CONST
  DefDebuggerStackSize = 20000D;    (* Default debugger stack size *)
  DefDebuggerFilePath  = "M2Debug"; (* Default debugger filename *)
  DefDebuggerWaitSecs  = 60;        (* Default debugger wait time *)

  (* MC68000 instructions used in this module *)
  InstJMPAbs      = 04EF9H; (* JMP $xxxxxxxx *)
  InstJMPa0       = 04ED0H; (* JMP (A0) *)
  InstUNLKa5      = 04E5DH; (* UNLK A5 *)
  InstMOVEuspTOa0 = 04E68H; (* MOVE USP,A0 *)
  InstADDQ4TOa7   = 0588FH; (* ADDQ.L #4, A7 *)
  InstRTE         = 04E73H; (* RTE *)
  InstEXTLd0      = 048C0H; (* EXT.L D0 *)

  (* The tcTrapData field contains this value when running the debugger *)
  ProgExceptionData = 04D324442H; (* "M2DB" *)

  ExceptionBUSERR = 2; (* Address error exception number *)
  ExceptionADRERR = 3; (* Bus error exception number *)

  (* Register numbers *)
  D0 = 0; D1 = 1; D2 = 2; D3 = 3; D4 = 4; D5 = 5; D6 = 6; D7 = 7;
  A0 = 8; A1 = 9; A2 =10; A3 =11; A4 =12; A5 =13; A6 =14; A7 =15; 

TYPE
  (* CPU state information *)
  CPUInfo = RECORD
              cpuRegList   : ARRAY [D0..A7] OF LONGCARD;
              cpuPC        : LONGCARD;
              cpuSR        : BITSET;
              cpuException : CARDINAL;
            END;

  (* Post-Mortem Debugger information *)
  PMDInfoPtr = POINTER TO PMDInfo;
  PMDInfo = RECORD
              (* Parms passed to debugger from user program *)
              pmdCPUInfo  : CPUInfo;
              pmdName     : ADDRESS;
              pmdSegList  : ADDRESS;
              pmdProcess  : ADDRESS;
              pmdMsgPort  : MsgPortPtr;
              pmdStackTop : ADDRESS;

              (* Parms returned by debugger to user program *)
              pmdJumpAdr     : ADDRESS;
            END;

  LPTR = POINTER TO LONGCARD;

  (* Recor which maps memory for patching in jump instruction *)
  PatchPtr = POINTER TO Patch;
  Patch = RECORD
            pInst    : CARDINAL;
            pOperand : LONGCARD;
          END;


VAR
  ProgProc         : ProcessPtr;     (* Program Process Structure *)
  ProgOldTrapCode  : ADDRESS;        (* Previous Contents of TrapCode field *)
  ProgStackRetAdr  : LPTR;           (* Pointer into stack for return adr *)
  ProgExitCode     : PatchPtr;       (* Pointer to first inst of exit code *)
  ProgOldCode      : Patch;          (* old contents of patched location *)
  ProgFilePath     : ADDRESS;        (* ptr to progname or NIL (use CLI def) *)
  ProgPMDInfo      : PMDInfo;        (* PostMortemDebugger Info *)
  DebuggerFilePath : ADDRESS;        (* ptr to debugname or NIL (use M2Debug) *)
  DebuggerWaitSecs : CARDINAL;       (* # of secs wait for M2Debug to startup *)
  CPUType68000     : BOOLEAN;        (* TRUE = MC68000, FALSE = 68010 or 68020 *)

  TempA7           : ADDRESS;        (* Supervisor Stack Ptr (temporary) *)

(* ===== PRIVATE ===== *)

(* Execute - execute an external program *)
PROCEDURE Execute(cmdString: ADDRESS; input, output: ADDRESS): BOOLEAN;
CONST
  LVOExecute = 0FF22H;
VAR
  res: LONGINT;
BEGIN
  SETREG(A6, DOSBase);
  SETREG(D1, cmdString);
  SETREG(D2, input);
  SETREG(D3, output);
  INLINE(4EAEH, LVOExecute); (* JSR LVOExecute(A6) *)
  res := REG(D0);
  RETURN (res = -1D);
END Execute;

(* FindTask - return ptr to task structure *)
PROCEDURE FindTask(name: ADDRESS): TaskPtr;
CONST
  LVOFindTask = 0FEDAH;
BEGIN
  SETREG(A6, ExecBase);
  SETREG(A1, name);
  INLINE(4EAEH, LVOFindTask); (* JSR LVOFindTask(A6) *)
  (* result in D0 *)
END FindTask;

(* GetMsg - Get next message from a message port *)
PROCEDURE GetMsg(VAR port: MsgPort): ADDRESS;
CONST
  LVOGetMsg = 0FE8CH;
VAR
  mp: MsgPortPtr;
BEGIN
  mp := ADR(port);
  SETREG(A6, ExecBase);
  SETREG(A0, mp);
  INLINE(4EAEH, LVOGetMsg); (* JSR LVOGetMsg(A6) *)
  (* result in D0 *)
END GetMsg;

(* Delay - delay a process for a specified amount of time. *)
PROCEDURE Delay(ticks: LONGINT);
CONST
  LVODelay = 0FF3AH;
BEGIN
  SETREG(A6, DOSBase);
  SETREG(D1, ticks);
  INLINE(4EAEH, LVODelay); (* JSR LVODelay(A6) *)
END Delay;

(* AllocSignal - allocate a signal bit *)
PROCEDURE AllocSignal(signalNum: SignalRange): SignalRange;
CONST
  LVOAllocSignal = 0FEB6H;
BEGIN
  SETREG(A6, ExecBase);
  SETREG(D0, LONGINT(signalNum));
  INLINE(InstEXTLd0);
  INLINE(4EAEH, LVOAllocSignal); (* JSR LVOAllocSignal(A6) *)
  (* result in D0 *)
END AllocSignal;

(* FreeSignal - free a signal bit *)
PROCEDURE FreeSignal(signalNum: SignalRange);
CONST
  LVOFreeSignal = 0FEB0H;
BEGIN
  SETREG(A6, ExecBase);
  SETREG(D0, LONGINT(signalNum));
  INLINE(InstEXTLd0);
  INLINE(4EAEH, LVOFreeSignal); (* JSR LVOFreeSignal(A6) *)
END FreeSignal;

(* AllocMem - allocate dynamic memory *)
PROCEDURE AllocMem(byteSize: LONGCARD; requirements: MemReqSet): ADDRESS;
CONST
  LVOAllocMem = 0FF3AH;
BEGIN
  SETREG(A6, ExecBase);
  SETREG(D0, byteSize);
  SETREG(D1, requirements);
  INLINE(4EAEH, LVOAllocMem); (* JSR LVOAllocMem(A6) *)
  (* result in D0 *)
END AllocMem;

(* FreeMem - free dynamic memory *)
PROCEDURE FreeMem(memoryBlock: ADDRESS; byteSize: LONGCARD);
CONST
  LVOFreeMem = 0FF2EH;
BEGIN
  SETREG(A6, ExecBase);
  SETREG(A1, memoryBlock);
  SETREG(D0, byteSize);
  INLINE(4EAEH, LVOFreeMem); (* JSR LVOFreeMem(A6) *)
END FreeMem;

(* NewList - initialize a linked list *)
PROCEDURE NewList(VAR list: List);
BEGIN
  WITH list DO
    lhHead     := ADR(lhTail);
    lhTail     := NIL;
    lhTailPred := ADR(lhHead);
  END;
END NewList;

(* CreatePort - create a message port *)
PROCEDURE CreatePort(name: ADDRESS; pri: INTEGER): MsgPortPtr;
VAR sigBit : SignalRange;
    port   : MsgPortPtr;
BEGIN
  sigBit := AllocSignal(AnySignal);
  IF sigBit = NoSignals THEN RETURN (NIL) END;

  port := AllocMem(SIZE(MsgPort), MemReqSet{MemClear,MemPublic});
  IF port = NIL THEN
    FreeSignal(sigBit);
    RETURN (NIL);
  END;

  WITH port^ DO
    mpNode.lnName := name;
    mpNode.lnPri  := BYTE(pri);
    mpNode.lnType := NTMsgPort;

    mpFlags       := PASignal;
    mpSigBit      := BYTE(sigBit);
    mpSigTask     := FindTask(CurrentTask);
  END;

  NewList(port^.mpMsgList);
  RETURN (port);
END CreatePort;

(* DeletePort - delete a message port *)
PROCEDURE DeletePort(VAR port: MsgPort);
BEGIN
  WITH port DO
    mpNode.lnType    := BYTE(0FFH);
    mpMsgList.lhHead := NodePtr(0FFFFFFFFH);
    FreeSignal(SignalRange(mpSigBit));
    FreeMem(ADR(port), SIZE(MsgPort));
  END;
END DeletePort;

(* strcat - concatanate strings *)
PROCEDURE strcat(dst, src: ADDRESS);
BEGIN
  (* Advance to null-term of destination *)
  WHILE (CHAR(dst^) # 0C) DO
    INC(dst);
  END;

  (* Copy until null-term of source *)
  WHILE (CHAR(src^) # 0C) DO
    dst^ := src^;
    INC(dst);
    INC(src);
  END;

  (* null-term destination *)
  dst^ := 0C;
END strcat;

(* hexcat - convert number to hex string and concatanate to string *)
PROCEDURE hexcat(dst: ADDRESS; value: LONGCARD);
VAR
  i: CARDINAL;
  shift: INTEGER;
  Digits: POINTER TO ARRAY [0..16] OF CHAR;
  buf: ARRAY [0..15] OF CHAR;
BEGIN
  Digits := ADR("0123456789ABCDEF");

  shift := 32;
  i     := 0;
  REPEAT
    DEC(shift, 4);
    buf[i] := Digits^[CARDINAL(SHIFT(value, -shift)) MOD 16];
    INC(i);
  UNTIL (shift = 0);
  buf[i] := 0C;

  (* Concat hex string to dst *)
  strcat(dst, ADR(buf[0]));
END hexcat;

(* GetProgSegList - get segment list of current process *)
PROCEDURE GetProgSegList(): ADDRESS;
VAR
  segList: ADDRESS;
  segArray: POINTER TO ARRAY [0..3] OF ADDRESS;
  cli: CommandLineInterfacePtr;
BEGIN
  segList := NIL;

  segArray := (ProgProc^.prSegList * 4D);
  IF (segArray # NIL) THEN
    IF (segArray^[0] = 2D) THEN (* Program started from WorkBench *)
      segList := segArray^[3];
    ELSE
      IF (ProgProc^.prCLI # NIL) THEN
        cli := ProgProc^.prCLI * 4D;
        segList := cli^.cliModule;
      END;
    END;
  END;
  RETURN (segList);
END GetProgSegList;

(* StartupDebugger - invoke source-level debugger in PMD mode *)
PROCEDURE StartupDebugger();
VAR
  segList: ADDRESS;
  port: MsgPortPtr;
  cli: CommandLineInterfacePtr;
  i: CARDINAL;
  execStr: ARRAY [0..255] OF CHAR;
  strPtr: POINTER TO ARRAY [0..127] OF CHAR;
BEGIN
  cli := NIL;

  IF (argc = 0) THEN (* Running from WorkBench *)
    IF (ProgFilePath = NIL) THEN
      HALT; (* Failed! *)
    END;

    cli := AllocMem(SIZE(CommandLineInterface), MemReqSet{MemClear});
    IF (cli = NIL) THEN
      HALT; (* Failed! *)
    END;
    ProgProc^.prCLI := ADDRESS(cli) DIV 4D;
    cli^.cliDefaultStack := DefDebuggerStackSize;
  ELSE
    IF (ProgFilePath = NIL) THEN
      ProgFilePath := argv^[0];
    END;
  END;

  segList := GetProgSegList();
  IF (segList # NIL) THEN

    port := CreatePort(NIL, 0);
    IF (port # NIL) THEN

      WITH ProgPMDInfo DO
        pmdName     := ProgFilePath;
        pmdSegList  := segList;
        pmdProcess  := ProgProc;
        pmdMsgPort  := port;
        pmdStackTop := StackPtr;
      END;

      IF (DebuggerFilePath = NIL) THEN
        DebuggerFilePath := ADR(DefDebuggerFilePath);
      END;

      (* Create Execute String "RUN >NIL: <NIL: <DebuggerFilePath> $xxxxxxxx" *)
      execStr[0] := 0C;
      strcat(ADR(execStr[0]), ADR("RUN >NIL: <NIL: "));
      strcat(ADR(execStr[0]), DebuggerFilePath);
      strcat(ADR(execStr[0]), ADR(" $"));
      hexcat(ADR(execStr[0]), ADR(ProgPMDInfo));

      IF (Execute(ADR(execStr[0]), NIL,NIL)) THEN
        FOR i := 0 TO DebuggerWaitSecs DO
          IF (GetMsg(ProgPMDInfo.pmdMsgPort^) # NIL) THEN
            (* Free fake command line interface if one was created *)
            IF (cli # NIL) THEN
              ProgProc^.prCLI := NIL;
              FreeMem(cli, SIZE(CommandLineInterface));
            END;

            SETREG(A0, ProgPMDInfo.pmdJumpAdr);
            INLINE(InstJMPa0)
          END;
          Delay(50D); (* Wait for 1 second *)
        END;
      END;

      DeletePort(port^);
    END;
  END;

  (* Free fake command line interface if one was created *)
  IF (cli # NIL) THEN
    ProgProc^.prCLI := NIL;
    FreeMem(cli, SIZE(CommandLineInterface));
  END;

  HALT; (* Failed! *)
END StartupDebugger;

(* DoException - setup for return to USER mode and adjust stack ptr *)
PROCEDURE DoException(VAR SSP: ADDRESS);
TYPE
  ExceptionStackFramePtr = POINTER TO ExceptionStackFrame;
  ExceptionStackFrame = RECORD
                 esfException : LONGCARD;
                 esfSR        : BITSET;
                 esfPC        : LONGCARD;
               END;

VAR
  sf: ExceptionStackFramePtr;  
BEGIN
  sf := SSP;

  WITH ProgPMDInfo.pmdCPUInfo DO
    cpuException := sf^.esfException;

    IF ((cpuException = ExceptionBUSERR) OR (cpuException = ExceptionADRERR))
        AND (CPUType68000) THEN
      INC(ADDRESS(sf), 8); (* Skip over adr and bus error info *)
      (* Adjust SSP to skip over adr/bus error info *)
      INC(SSP, 8);
    END;

    cpuPC := sf^.esfPC;
    cpuSR := sf^.esfSR;
  END; (* WITH *)

  sf^.esfPC := ADR(StartupDebugger);
END DoException;

(* ExceptionHandler - called upon encountering exception *)
PROCEDURE ExceptionHandler();
BEGIN
  (* WARNING: Do not define any local variables in this procedure *)
  INLINE(InstUNLKa5);

  (* Get Supervisor Mode Stack Ptr *)
  TempA7 := REG(A7);

  (* Save Regs for use by the Post Mortem Debugger *)
  ProgPMDInfo.pmdCPUInfo.cpuRegList[D0] := REG(D0);
  ProgPMDInfo.pmdCPUInfo.cpuRegList[D1] := REG(D1);
  ProgPMDInfo.pmdCPUInfo.cpuRegList[D2] := REG(D2);
  ProgPMDInfo.pmdCPUInfo.cpuRegList[D3] := REG(D3);
  ProgPMDInfo.pmdCPUInfo.cpuRegList[D4] := REG(D4);
  ProgPMDInfo.pmdCPUInfo.cpuRegList[D5] := REG(D5);
  ProgPMDInfo.pmdCPUInfo.cpuRegList[D6] := REG(D6);
  ProgPMDInfo.pmdCPUInfo.cpuRegList[D7] := REG(D7);
  ProgPMDInfo.pmdCPUInfo.cpuRegList[A0] := REG(A0);
  ProgPMDInfo.pmdCPUInfo.cpuRegList[A1] := REG(A1);
  ProgPMDInfo.pmdCPUInfo.cpuRegList[A2] := REG(A2);
  ProgPMDInfo.pmdCPUInfo.cpuRegList[A3] := REG(A3);
  ProgPMDInfo.pmdCPUInfo.cpuRegList[A4] := REG(A4);
  ProgPMDInfo.pmdCPUInfo.cpuRegList[A5] := REG(A5);
  ProgPMDInfo.pmdCPUInfo.cpuRegList[A6] := REG(A6);

  (* Save User Stack Ptr for PMD *)
  INLINE(InstMOVEuspTOa0); (* MOVE USP,A0 *)
  ProgPMDInfo.pmdCPUInfo.cpuRegList[A7] := REG(A0);

  (* Setup for return and adjust stack ptr *)
  DoException(TempA7);

  (* Loads the adjusted stack ptr value *)
  SETREG(A7, TempA7);

  (* Return to User Mode and StartupDebugger() function *)
  INLINE(InstADDQ4TOa7);      (* ADDQ.L #4,A7 (pop exception # from stack) *)
  INLINE(InstRTE);
END ExceptionHandler;

(* CleanupProc - intercept routine jumped to from exit code *)
PROCEDURE CleanupProc();
BEGIN
  (* Restore previous contents of tcTrapCode field *)
  ProgProc^.prTask.tcTrapCode := ProgOldTrapCode;

  (* Restore previously patched code *)
  ProgExitCode^ := ProgOldCode;

  (* Jump to code which has just been restored *)
  SETREG(A0, ProgExitCode);
  INLINE(InstJMPa0);
END CleanupProc;

(* InitProc - setup exception handler and patch exit code *)
PROCEDURE InitProc();
VAR
  exec: ExecBasePtr;
BEGIN
  ProgProc := ProcessPtr(FindTask(CurrentTask));

  (* Don't setup exception handler if running under M2Debug *)
  IF (ProgProc^.prTask.tcTrapData # ProgExceptionData) THEN

    (* Setup exception handler *)
    WITH ProgProc^.prTask DO
      ProgOldTrapCode := tcTrapCode;
      tcTrapCode      := ADR(ExceptionHandler);
    END;

    (* From stack retrieve exit code adr *)
    ProgExitCode := PatchPtr(ProgStackRetAdr^);

    (* Copy code which will be patched *)
    ProgOldCode := ProgExitCode^;

    (* Patch exit code with a jump to our cleanup procedure *)
    WITH ProgExitCode^ DO
      pInst    := InstJMPAbs;
      pOperand := ADR(CleanupProc);
    END;

    exec         := ExecBase;
    CPUType68000 := NOT (AF68010 IN exec^.AttnFlags);
  END;
END InitProc;

(* ===== PUBLIC ===== *)

PROCEDURE SetPMDInfo(progFilePath: ADDRESS; debuggerFilePath: ADDRESS;
                     waitSecs: CARDINAL);
BEGIN
  ProgFilePath     := progFilePath;
  DebuggerFilePath := debuggerFilePath;
  DebuggerWaitSecs := waitSecs;
END SetPMDInfo;

BEGIN
  (* Find Stack Pos of Return Adr for cleanup-code *)
  (* +0 = A5, +4 = InitCodeRetAdr, +8 = CleanupCodeRetAdr *)
  ProgStackRetAdr := ADDRESS(REG(A7) + 8D);

  (* Default debugger startup up wait time *)
  DebuggerWaitSecs := DefDebuggerWaitSecs;

  InitProc();
END PMD.
