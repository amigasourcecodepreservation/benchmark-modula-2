(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Jim Olinger                          *
 ****************************************************************************
 * Name: ScanString.MOD                    Version: Amiga.00.00             *
 * Created: 01/19/87   Updated: 03/12/89   Author: Leon Frenkel             *
 * Description: Low level utility function for reading formating input,     *
 *  used as the basis for the high level functions scanf, fscanf, sscanf.   *
 * WARNING: The REAL format conversions: "%f, %e", are not avaialable       *
 *  unless the module "ScanStringReals" is imported.                        *
 *                                                                          *
 * This version of the module has been modified slightly for use with the   *
 * Source-Level Debugger.  Additional format types have been added "%b"     *
 * and "%B" these are used for binary format.                               *
 ****************************************************************************)

IMPLEMENTATION MODULE ScanString;

FROM SYSTEM          IMPORT ADR, ADDRESS, BYTE, WORD, LONGWORD;

(*$L-,D-*)

CONST
  LF = 012C;
  HT = 011C;
  BS = 010C;
  CR = 015C;
  FF = 014C;
  VT = 013C;
  SP = " ";
  EOF = -1;

TYPE
  CPTR  = POINTER TO CHAR;
  CIPTR = POINTER TO ARRAY [0..32765] OF CHAR;

VAR maxwidth : INTEGER;
    GetCh    : GetCharFuncType;
    list     : POINTER TO ARRAY [0..21] OF CHAR; (* "ABCDEFabcdef9876543210" *)
    vals     : ARRAY [0..21] OF CHAR; (* 10,11,12,13,14,15,10,11,12,13,14,15,
                                       * 09,08,07,06,05,04,03,02,01,00 *)

PROCEDURE index(p: CPTR; c: CHAR): CPTR;
BEGIN
  WHILE (p^ # 0C) DO
    IF p^ = c THEN RETURN p; END;
    INC(ADDRESS(p));
  END;
  RETURN NIL;
END index;

PROCEDURE skipblank(): BOOLEAN;
VAR
  ic: INTEGER;
BEGIN
  LOOP
    ic := GetCh(FALSE);
    IF (ic # ORD(SP)) AND ((ic < ORD(HT)) OR (ic > ORD(CR))) THEN EXIT; END;
  END;
  RETURN GetCh(TRUE) = EOF;
END skipblank;

(* getnum - read in a number. returns TRUE if valid number, FALSE if invalid *)
PROCEDURE getnum(list: CPTR; values: CIPTR; base: INTEGER; 
                 VAR valp: LONGCARD): BOOLEAN;
VAR 
  ch   : INTEGER;
  cp   : CPTR;
  c    : INTEGER;
  cnt  : INTEGER;
  val  : LONGCARD;
  sign : BOOLEAN;

BEGIN
  IF maxwidth <= 0 THEN
    RETURN FALSE;
  END;
  val := 0;
  cnt := 0;
  sign := FALSE;
  c := GetCh(FALSE);
  IF c = ORD("-") THEN
    sign := TRUE;
    INC(cnt);
  ELSIF c = ORD("+") THEN
    INC(cnt);
  ELSE
    ch := GetCh(TRUE);
  END;

  LOOP
    IF NOT (cnt < maxwidth) THEN EXIT; END;
    c := GetCh(FALSE);
    cp := index(list, CHR(c));
    IF cp = NIL THEN
      IF NOT ((base = 16 ) AND (val = 0D) AND ((c = ORD("x")) OR (c = ORD("X")))) THEN
        ch := GetCh(TRUE);
        EXIT;
      END;
    ELSE
      val := val * LONGCARD(base);
      val := val + LONGCARD(values^[CARDINAL(ADDRESS(cp)-ADDRESS(list))]);
    END;
    INC(cnt);
  END;
  IF sign THEN
    valp := -LONGINT(val);
  ELSE
    valp := val;
  END;
  RETURN (cnt # 0);
END getnum;

(* getflt - read in a floating point number and store into buffer for later
 *          processing by the ConvStringToReal() function. *)
PROCEDURE getflt(buffer: CPTR): BOOLEAN;
VAR
  cp:    CPTR;
  c:     INTEGER;
  ch:    INTEGER;
  decpt: BOOLEAN;
  sign:  BOOLEAN; 
  exp:   BOOLEAN;
BEGIN
  sign := FALSE;
  exp := FALSE;
  decpt := FALSE;

  cp := buffer;
  LOOP
    DEC(maxwidth);
    IF (maxwidth+1) = 0 THEN EXIT; END;
    c := GetCh(FALSE);
    IF NOT ((c >= ORD("0")) AND (c <= ORD("9"))) THEN
      IF (NOT decpt) AND (c = ORD(".")) THEN
        decpt := TRUE;
        sign  := TRUE; (* equivilant to else part of previous if *)
      ELSIF (NOT exp) AND ((c = ORD("e")) OR (c = ORD("E"))) AND (cp # buffer) THEN
        sign  := FALSE;
        exp   := TRUE;
        decpt := TRUE;
      ELSIF (sign) OR ((c # ORD("-")) AND (c # ORD("+"))) THEN
        ch := GetCh(TRUE);
        EXIT;
      ELSE
        sign := TRUE; (* equivilant to else part of previous if *)
      END;
    ELSE
      sign := TRUE;
    END;
    cp^ := CHR(c);
    INC(ADDRESS(cp));
  END;
  cp^ := 0C;
  RETURN (cp = buffer);
END getflt;


PROCEDURE Scan(fmt: ADDRESS; VAR args: ARRAY OF ScanArg;
               getFunc: GetCharFuncType): INTEGER;
TYPE
  ARGPTR = RECORD
             CASE :CARDINAL OF
               |0: B: POINTER TO BYTE;
               |1: W: POINTER TO WORD;
               |2: L: POINTER TO LONGWORD;
             END;
           END;
VAR
  lv       : LONGCARD;
  c        : CHAR;
  ch       : INTEGER;
  ic       : INTEGER;
  cc       : CHAR;
  flag     : BOOLEAN;
  base     : INTEGER;
  suppress : BOOLEAN;
  lflag    : BOOLEAN;
  widflg   : BOOLEAN;
  count    : CARDINAL;
  cp       : CPTR;
  action   : (none, matchit, getval, float, string, charstring);
  ArgPtr   : POINTER TO ARRAY [0..8190] OF ARGPTR;
  argIdx   : INTEGER;
  tlist    : ARRAY [0..129] OF CHAR;

BEGIN
  argIdx := 0;
  ArgPtr := ADR(args);
  count  := 0;
  GetCh  := getFunc;
  LOOP
    c := CHAR(fmt^); INC(fmt);
    IF c = 0C THEN EXIT; END;
    IF c = "%" THEN
      widflg   := FALSE;
      lflag    := FALSE;
      suppress := FALSE;
      maxwidth := 127;
      IF CHAR(fmt^) = "*" THEN
        INC(fmt);
        suppress := TRUE;
      END;
      IF (CHAR(fmt^) >= "0") AND (CHAR(fmt^) <= "9") THEN
        maxwidth := 0;
        REPEAT
          maxwidth := (maxwidth * 10) + (INTEGER(CHAR(fmt^)) - INTEGER("0"));
          INC(fmt);
        UNTIL NOT ((CHAR(fmt^) >= "0") AND (CHAR(fmt^) <= "9"));
        widflg := TRUE;
      END;
      IF CHAR(fmt^) = "l" THEN
        lflag := TRUE;
        INC(fmt);
      END;
      action := none;
      cc := CHAR(fmt^); INC(fmt);
      CASE cc OF
        | "%":     c      := "%";
                   action := matchit;
        | "h":     lflag  := FALSE;
                   c      := CHR(12);
                   base   := 10;
                   action := getval;
        | "D":     lflag  := TRUE;
                   c      := CHR(12);
                   base   := 10;
                   action := getval;
        | "d":     c      := CHR(12);
                   base   := 10;
                   action := getval;
        | "X":     lflag  := TRUE;
                   c      := CHR(0);
                   base   := 16;
                   action := getval;
        | "x":     c      := CHR(0);
                   base   := 16;
                   action := getval;
        | "O":     lflag  := TRUE;
                   c      := CHR(14);
                   base   := 8;
                   action := getval;
        | "o":     c      := CHR(14);
                   base   := 8;
                   action := getval;
        | "B":     lflag  := TRUE;
                   c      := CHR(20);
                   base   := 2;
                   action := getval;
        | "b":     c      := CHR(20);
                   base   := 2;
                   action := getval;
(* **** LONGREAL not supported!!! *****
        | "E","F": lflag  := TRUE; (* LONGREAL *)
                   action := float;
*)
        | "e","f": IF ADDRESS(ConvToRealFunc) = NIL THEN (* no real conv func *)
                     c := cc;
                     action := matchit;
                   ELSE (* real conversion func available *)
                     action := float;
                   END;
        | "[":     lflag := FALSE;
                   IF (CHAR(fmt^) = "^") OR (CHAR(fmt^) = "~") THEN
                     INC(fmt);
                     lflag := TRUE;
                   END;
                   cp := ADR(tlist);
                   LOOP
                     c := CHAR(fmt^); INC(fmt);
                     IF c = "]" THEN EXIT; END;
                     cp^ := c;
                     INC(ADDRESS(cp));
                   END;
                   cp^ := 0C;
                   action := string;
        | "s":     lflag := TRUE;
                   tlist[0] := " ";
                   tlist[1] := HT;
                   tlist[2] := LF;
                   tlist[3] := 0C;
                   action := string;
        | "c":     IF NOT widflg THEN
                     maxwidth := 1;
                   END;
                   tlist[0] := 0C;
                   lflag    := TRUE;
                   action   := charstring;
      ELSE (* DO NOT REMOVE ELSE! *)
      END;
      IF action = getval THEN
        IF skipblank() THEN
          EXIT; (* stopscan *)
        END;
        IF NOT getnum(ADR(list^[CARDINAL(c)]), ADR(vals[CARDINAL(c)]), base, lv)
          THEN EXIT; (* stopscan *)
        END;
        IF NOT suppress THEN
          IF lflag THEN
            ArgPtr^[argIdx].L^ := lv;
          ELSE
            ArgPtr^[argIdx].W^ := WORD(lv);
          END;
          INC(argIdx);
          INC(count);
        END;
      ELSIF action = matchit THEN
        IF GetCh(FALSE) # INTEGER(c) THEN
          ch := GetCh(TRUE);
          EXIT; (* stopscan *)
        END;
      ELSIF action = float THEN
        IF skipblank() THEN
          EXIT; (* stopscan *)
        END;
        IF getflt(ADR(tlist)) THEN
          EXIT; (* stopscan *)
        END;
        IF NOT suppress THEN 
          (* IF lflag THEN LONGREAL ELSE REAL END *)
          ArgPtr^[argIdx].L^ := ConvToRealFunc(tlist);
          INC(argIdx);
          INC(count);
        END;
      ELSIF (action = string) OR (action = charstring) THEN 
        IF (action = string) AND skipblank() THEN 
          EXIT; (* stopscan *)
        END;
        (* charstring: *)
        IF NOT suppress THEN
          cp := CPTR(ArgPtr^[argIdx].L); 
          INC(argIdx);
        END;
        widflg := FALSE;
        LOOP
          DEC(maxwidth);
          IF (maxwidth+1) = 0 THEN EXIT; END;
          ic := GetCh(FALSE);
          c := CHR(ic);
          IF ic = EOF THEN
            EXIT;
          END;
          IF lflag THEN
            flag := index(ADR(tlist),c) # NIL;
          ELSE
            flag := index(ADR(tlist),c) = NIL;
          END;
          IF flag THEN
            ch := GetCh(TRUE);
            EXIT;
          END;
          IF NOT suppress THEN
            cp^ := c;
            INC(ADDRESS(cp));
          END;
          widflg := TRUE;
        END;
        IF NOT widflg THEN
          EXIT; (* stopscan *)
        END;
        IF NOT suppress THEN
          IF cc # "c" THEN
            cp^ := 0C;
          END;
          INC(count);
        END;
      END;

    ELSIF (c = SP) OR ((c >= HT) AND (c <= CR)) THEN
      IF skipblank() THEN
        EXIT;
      END;
    ELSE
      IF GetCh(FALSE) # INTEGER(c) THEN
        ch := GetCh(TRUE);
        EXIT; (* stopscan *)
      END;
    END;
  END; (* LOOP *)

  (* stopscan: *)
  IF count = 0 THEN
    IF GetCh(FALSE) = EOF THEN
      RETURN EOF;
    END;
    ch := GetCh(TRUE);
  END;
  RETURN count;
END Scan;

VAR i, j : CARDINAL;
BEGIN
  list := ADR("ABCDEFabcdef9876543210");
  j := 55;
  FOR i := 0 TO 21 DO
    IF i = 6 THEN j := 87; END;
    IF i = 12 THEN j := 48; END;
    vals[i] := CHAR(CARDINAL(list^[i])-j);
  END;
END ScanString.
