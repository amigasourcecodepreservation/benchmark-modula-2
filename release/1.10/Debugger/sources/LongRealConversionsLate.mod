(* LongRealConversionsLate.mod *)

(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Jim Olinger                          *
 ****************************************************************************
 * Name: LongRealConversions.MOD           Version: Amiga.00.00             *
 * Created: 02/23/94   Updated:            Author: Tom Breeden              *
 * Description: Conversion of LONGREAL numbers to strings and conversion of *
 * strings to LONGREAL numbers.                                             *
 ****************************************************************************)

IMPLEMENTATION MODULE LongRealConversions;

FROM SYSTEM  IMPORT ADDRESS, ADR, LONG;
FROM RealConversions IMPORT RealToStringFormat;

(*$L-,D-*)

TYPE IEEEDOUBLE    = RECORD CASE :BOOLEAN OF
                        TRUE:  lr  :LONGREAL; |
                        FALSE: lw  :ARRAY[0..1] OF LONGCARD;
                     END END;

VAR  round : ARRAY [0..17] OF IEEEDOUBLE;
     zero  : LONGREAL;
     ten   : LONGREAL;

     InitDone :BOOLEAN;

(*-------------------*)
 PROCEDURE Initialize;
(*-------------------*)

BEGIN

  zero  := 0.0;
  ten   := 10.0;

  round[0].lr  := 1.0E1;
  round[1].lr  := 1.0E0;
  round[2].lr  := 5.0E-1;
(*round[3].lr  := 5.0E-2;*) round[3].lw[0] := 03FA99999H; round[3].lw[1] := 099999999H;
(*round[4].lr  := 5.0E-3;*) round[4].lw[0] := 03F747AE1H; round[4].lw[1] := 047AE147AH;
(*round[5].lr  := 5.0E-4;*) round[5].lw[0] := 03F40624DH; round[5].lw[1] := 0D2F1A9FBH;
(*round[6].lr  := 5.0E-5;*) round[6].lw[0] := 03F0A36E2H; round[6].lw[1] := 0EB1C432CH;
(*round[7].lr  := 5.0E-6;*) round[7].lw[0] := 03ED4F8B5H; round[7].lw[1] := 088E368F0H;
(*round[8].lr  := 5.0E-7;*) round[8].lw[0] := 03EA0C6F7H; round[8].lw[1] := 0A0B5ED8DH;
(*round[9].lr  := 5.0E-8;*) round[9].lw[0] := 03E6AD7F2H; round[9].lw[1] := 09ABCAF48H;
(*round[10].lr := 5.0E-9;*) round[10].lw[0] := 03E35798EH; round[10].lw[1] := 0E2308C39H;
(*round[11].lr := 5.0E-10;*) round[11].lw[0] := 03E012E0BH; round[11].lw[1] := 0E826D694H;
(*round[12].lr := 5.0E-11;*) round[12].lw[0] := 03DCB7CDFH; round[12].lw[1] := 0E34997C2H;
(*round[13].lr := 5.0E-12;*) round[13].lw[0] := 03D95FD7FH; round[13].lw[1] := 0E2FC3FC9H;
(*round[14].lr := 5.0E-13;*) round[14].lw[0] := 03D619799H; round[14].lw[1] := 08641C05AH;
(*round[15].lr := 5.0E-14;*) round[15].lw[0] := 03D2C25C2H; round[15].lw[1] := 06681E6A6H;
(*round[16].lr := 5.0E-15;*) round[16].lw[0] := 03CF6849BH; round[16].lw[1] := 08B8B5046H;
(*round[17].lr := 5.0E-16;*) round[17].lw[0] := 03C8CD2B2H; round[17].lw[1] := 09F6F44B4H;

InitDone := TRUE;

END Initialize;

(*==========================================================*)
 PROCEDURE ConvStringToLongReal(buffer: ARRAY OF CHAR): LONGREAL;
(*==========================================================*)

CONST TAB = 011C;

VAR 
  acc   : LONGREAL;
  msign : BOOLEAN;
  esign : BOOLEAN;
  dpflg : BOOLEAN;
  i     : INTEGER;
  dexp  : INTEGER;
  cp    : POINTER TO CHAR;
BEGIN

  IF NOT InitDone THEN
      Initialize;
  END;

  cp := ADR(buffer);
  WHILE (cp^ = " ") OR (cp^ = TAB) DO
    INC(ADDRESS(cp));
  END;
  IF cp^ = "-" THEN
    INC(ADDRESS(cp));
    msign := TRUE;
  ELSE
    msign := FALSE;
    IF (cp^ = "+") THEN
      INC(ADDRESS(cp));
    END;
  END;
  dpflg := FALSE;
  dexp  := 0;
  acc := zero;
  LOOP
    IF (cp^ >= "0") AND (cp^ <= "9") THEN
      acc := acc * ten;
      acc := acc + FLOATD(ORD(cp^) - ORD("0"));
      IF dpflg THEN
        DEC(dexp);
      END;
    ELSIF cp^ = "." THEN
      IF dpflg THEN
        EXIT;
      END;
      dpflg := TRUE;
    ELSE
      EXIT;
    END;
    INC(ADDRESS(cp));
  END; (* LOOP *)
  IF (cp^ = "e") OR (cp^ = "E") THEN
    INC(ADDRESS(cp));
    IF cp^ = "-" THEN
      INC(ADDRESS(cp));
      esign := TRUE;
    ELSE
      esign := FALSE;
      IF cp^ = "+" THEN
        INC(ADDRESS(cp));
      END;
    END;
    i := 0;
    WHILE (cp^ >= "0") AND (cp^ <= "9") DO
      i := (i*10) + (INTEGER(cp^) - INTEGER("0"));
      INC(ADDRESS(cp));
    END;
    IF esign THEN 
      i := -i;
    END;
    INC(dexp,i);
  END;
  IF dexp < 0 THEN
    WHILE dexp # 0 DO
      INC(dexp);
      acc := acc / ten;
    END;
  ELSIF dexp > 0 THEN
    WHILE dexp # 0 DO
      DEC(dexp);
      acc := acc * ten;
    END;
  END;
  IF msign THEN
    acc := -acc;
  END;
  RETURN acc;

END ConvStringToLongReal;

(*--------------------------------------------------------------*)
 PROCEDURE StrAsg(InStr:ARRAY OF CHAR; VAR OutStr:ARRAY OF CHAR);
(*--------------------------------------------------------------*)

(* >>> SPECIAL: DOES NOT CHECK HIGH AND ASSUMES 0C ENDS INSTR *)

VAR i  :CARDINAL;
   
BEGIN

i := 0;
LOOP
   (*IF i > HIGH(OutStr) THEN
      EXIT
   ELS*)IF (*(i > HIGH(InStr)) OR*) (InStr[i] = 0C) THEN
      OutStr[i] := 0C;
      RETURN;
   ELSE
     OutStr[i] := InStr[i];
     INC(i);
   END;
END;

END StrAsg;
   
(*========================================================================*)
 PROCEDURE ConvLongRealToString(number: LONGREAL; VAR buffer: ARRAY OF CHAR; 
                            maxwidth: CARDINAL; format: RealToStringFormat);
(*========================================================================*)

TYPE IEEEDOUBLE    = RECORD CASE :BOOLEAN OF
                        TRUE:  lr  :LONGREAL; |
                        FALSE: lw  :ARRAY[0..1] OF LONGCARD;
                     END END;
     IEEEDOUBLEPTR = POINTER TO IEEEDOUBLE;

VAR i, exp, digit, decpos, ndig, bidx: INTEGER;
    ptr  :IEEEDOUBLEPTR;

BEGIN

  IF NOT InitDone THEN
      Initialize;
  END;

  (*................................................................*)
  (* This code is for the Amiga IEEE double precision format only!  *)
  (* It checks for IEEE "Infinities" and "NANs", which can cause    *)
  (* code here to go into infinite loops                            *)
  ptr := ADR(number);
  IF ((ptr^.lw[0] DIV 1048576D) MOD 2048D) = 2047D THEN  (* Exp = all 1's *)
     IF (ptr^.lw[0] MOD 1048576D) = 0D THEN
        IF LONGINT(ptr^.lw[0]) >= 0D THEN
           StrAsg("+INF", buffer);
        ELSE
           StrAsg("-INF", buffer);
        END;
     ELSE
        StrAsg("NAN", buffer);
     END;
     RETURN;
  END;
  (*................................................................*)

  bidx := 0;
  ndig := maxwidth+1;
  exp  := 0;
  IF number < zero THEN
    number := -number;
    buffer[bidx] := "-"; INC(bidx);
  END;
  IF number > zero THEN
    WHILE number < round[1].lr DO
      number := number * round[0].lr;
      DEC(exp);
    END;
    WHILE number >= round[0].lr DO
      number := number / round[0].lr;
      INC(exp);
    END;
  END;

  IF format = ShortestForm THEN
    ndig := maxwidth;
    IF (exp < -4) OR (exp > INTEGER(maxwidth)) THEN
      format := Scientific;
    END;
  ELSIF format = Decimal THEN
    ndig := ndig + exp;
  END;

  IF ndig >= 0 THEN
    IF ndig > 16 THEN
      number := number + round[17].lr;
    ELSE
      number := number + round[ndig+1].lr;
    END;
    IF number >= round[0].lr THEN
      number := round[1].lr;
      INC(exp);
      IF (format = Decimal) OR (format = ShortestForm) THEN INC(ndig) END;
    END;
  END;

  IF (format = Decimal) OR (format = ShortestForm) THEN
    IF exp < 0 THEN
      buffer[bidx] := "0"; INC(bidx);
      buffer[bidx] := "."; INC(bidx);
      i := -exp - 1;
      IF ndig <= 0 THEN
        i := maxwidth;
      END;
      WHILE i # 0 DO
        DEC(i);
        buffer[bidx] := "0"; INC(bidx);
      END;
      decpos := 0;
    ELSE
      decpos := exp + 1;
    END;
  ELSE
    decpos := 1;
  END;

  IF ndig > 0 THEN
    i := 0;
    LOOP
      IF i < 16 THEN
        digit := TRUNCD(number);
        buffer[bidx] := CHAR(digit + ORD("0")); INC(bidx);
        number := (number - FLOATD(digit)) * round[0].lr;
      ELSE
        buffer[bidx] := "0"; INC(bidx);
      END;
      DEC(ndig);
      IF ndig = 0 THEN
        EXIT;
      END;
      IF (decpos # 0) AND (decpos - 1 = 0) THEN
        buffer[bidx] := "."; INC(bidx);
      END;
      DEC(decpos);
      INC(i);
    END;
  END;

  IF format = Scientific THEN
    buffer[bidx] := "E"; INC(bidx);
    IF exp < 0 THEN
      exp := -exp;
      buffer[bidx] := "-"; INC(bidx);
    ELSE
      buffer[bidx] := "+"; INC(bidx);
    END;
    IF exp >= 100 THEN
      buffer[bidx] := CHAR(exp DIV 100 + ORD("0")); INC(bidx);
      exp := exp MOD 100;
    END;
    buffer[bidx] := CHAR(exp DIV 10 + ORD("0")); INC(bidx);
    buffer[bidx] := CHAR(exp MOD 10 + ORD("0")); INC(bidx);
  END;
  buffer[bidx] := 0C;
END ConvLongRealToString;

BEGIN

  InitDone := FALSE;

END LongRealConversions.
