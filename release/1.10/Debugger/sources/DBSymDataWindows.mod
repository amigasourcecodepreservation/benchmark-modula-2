
(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBSymDataWindows.MOD              Version: Amiga.01.10             *
 * Created: 02/01/89   Updated: 03/21/94   Author: Leon Frenkel             *
 * Description: Debugger symbolic data windows handling module.             *
 ****************************************************************************)

IMPLEMENTATION MODULE DBSymDataWindows;

(***************************************************************************
 * Changes for new release 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.4 -----
 * 
 * SymWindow Display of LONGREAL Values
 * 
 *    1. Added another param to DisplayValue(), "lrvalue:LONGREAL", used only
 *       for longreal items. Using LongRealConversions on that value if
 *       a longreal item.
 * 
 *    2. Added lrZERO global variable for a LONGREAL const.
 * 
 *    3. Most places where DisplayValue() is called now also send a lrvalue
 *       local to GetMemoryValue, and to DisplayValue. Also, the
 *       CompareOrStoreValue() call has "OR (info.iiForm = LongReal)" added
 *       to the dontCmp expression so that longreal always displays.
 * 
 ***************************************************************************
*)

FROM SYSTEM IMPORT
  ADDRESS, BYTE, WORD, LONGWORD,
  ADR;
FROM CMemOp IMPORT
  movmem;
FROM CPrintBuffer IMPORT
  sprintf;
FROM CStrings IMPORT
  strlen, strcpy, strcat;
FROM DBConfiguration IMPORT
  ConfigWindow;
FROM DBFiles IMPORT
  InvalidModNo, InvalidStatNo, InvalidModOfs, InvalidAbsAdr,
  Item, ItemInfo, StructureForm, FileType, ModuleType,
  GetSonItem, GetArrayIndexItem, GetProcReturnItem, GetBrotherItem, EmptyItem,
  GetItemInfo, GetModuleItem, GetModuleDataBase, GetFilePath, GetModNoModOfs,
  GetProcedureItem, GetStatementNoFromModOffset, GetModOffsetFromStatementNo,
  EqualItems;
FROM DBMenusKbd IMPORT
  LinkGlobalMenuStrip, DoGlobalMenuCommand, DoGlobalKbdCommand,
  AddMenuItemWithCheck, AddMenuItemWithCheckList, AddMenuItemNoHighlight;
FROM DBMisc IMPORT
  AllocMemory, DeallocMemory, AllocString, DeallocString, GetMemoryLong,
  GetMemoryHIGH, GetMemoryValue;
FROM DBRequesters IMPORT
  RequestTextFgBgColors, RequestTextFgColor, RequestTextWidth,
  RequestValueWidth;
FROM DBSelection IMPORT
  SelectionData, StorageFormat, StorageNumber, ModuleFileType,
  ClearSelection, GetSelection, SetSelection, GetSelectionLineInfo,
  IsPosSelected, IfCurrentWindowClearSelection;
FROM DBStatusWindow IMPORT
  ShowErrMsgNoMemory, ShowErrMsgInvalidDest, ShowErrMsgNoSrcSelect,
  ShowError;
FROM DBTextWindows IMPORT
  DefMinWindowWidth, DefMinWindowHeight, DefMaxWindowWidth, DefMaxWindowHeight,
  VTWindow, NewVTWindow, VTWindowNIL, VTWindowParms,
  OpenVTWindow, CloseVTWindow, InitParmsVTWindow, WriteBufferVTWindow,
  SetCursorPosVTWindow, GetUserDataVTWindow, WriteColorVTWindow, ClearVTWindow,
  RefreshVTWindow, GetDisplayRangeVTWindow, WriteStringVTWindow, 
  SetColorVTWindow, WriteCharVTWindow, GetTitleBarWidthVTWindow,
  SetTitleVTWindow, GetInfoVTWindow, SetMenuStripVTWindow,
  ClearMenuStripVTWindow, SetDetailPenBlockPenVTWindow, SetDefaultColorVTWindow,
  GetDetailPenBlockPenVTWindow;
FROM DBUserProgram IMPORT
  ProcChainNodePtr, ProcChainNode,
  GetProcChainLength, GetProcChainNode, GetRegisterValue, IsProgramLoaded;
FROM DBWindows IMPORT
  WindowClass,
  GetVTWindowsMsgPort;
FROM FormatString IMPORT
  FormatArg;
FROM Intuition IMPORT
  Menu, MenuPtr, MenuItemMutualExcludeSet;
FROM LongRealConversions IMPORT
  ConvLongRealToString;
FROM Memory IMPORT
  MemReqSet, MemClear;
FROM RealConversions IMPORT RealToStringFormat;
FROM SimpleMenus IMPORT
  BeginMenuStrip, EndMenuStrip, FreeMenuStrip, AddMenu, AddMenuItem;

CONST
  (* Symbolic Data Window Title when no data displayed *)
  DefSymDataWindowTitle = "Symbolic Data";

  (* Symbolic data window keyboard commands *)
  KeyGotoParent = CHAR(8);  (* BackSpace. Goto parent structure *)
  KeyGotoStart  = CHAR(27); (* ESC. Goto root structure *)

  LocalMenus = 1; (* Number of window specific menus *)

  OutputBufSize = 1024D; (* Size of output buffer used in VTUpdateProc() *)
  SelectBufSize = 1024D; (* Size of selection string buffer *)
  TitleBufSize  = 1024D; (* Size of buffer used in CreateNewWindowTitle() *)

  (* Fixed number of lines for each symbolic data display type *)
  ShowPointerLinesAdj   = 2;
  ShowSetLinesAdj       = 1;
  ShowArrayLinesAdj     = 1;
  ShowRecordLinesAdj    = 2;
  ShowProcedureLinesAdj = 2;
  ShowModuleLinesAdj    = 2;
  ShowValueLinesAdj     = 2;

  (* Width of "VAR " string used to indicate VAR parameters *)
  ShowVarParWidth       = 4;

  ReturnValueName  = "[RETURN]"; (* Return value identifier *)
  ReturnValueWidth = 8;          (* String length of return value identifier *)

  VariableValueName  = "[VARIABLE]"; (* Symbolic value identifier *)

  (* Message displayed in window when no local stack frame *)
  NoLocalFrameMsg = "Stack Frame Has Not Been Created";

TYPE
  LookupValue = LONGCARD; (* Lookup array value type *)
  (* Array of lookup values which corespond to display line #s *)
  ValueLookupArrayPtr = POINTER TO ValueLookupArray;
  ValueLookupArray = ARRAY [0..1023] OF LookupValue;

  (* Symbolic data object descriptor *)
  SelectDataPtr = POINTER TO SelectData;
  SelectData = RECORD
                 sdNext        : SelectDataPtr; (* Next object *)
                 sdPrev        : SelectDataPtr; (* Previous object *)
                 sdItem        : Item;          (* Object record *)
                 sdString      : ADDRESS;       (* object title str *)
                 sdFirstLine   : CARDINAL;      (* first line displayed *)
                 sdNameWidth   : CARDINAL;      (* longest object identifier *)
                 sdTotalLines  : CARDINAL;      (* total lines of object *)
                 sdShowReturn  : BOOLEAN;       (* T: show return val, F: Not *)
                 sdCurrentProc : BOOLEAN;       (* object is current procedure*)
               END;

  (* Symbolic data window update mode *)
  UpdateMode = (UMProc, UMModule, UMNone);

  (* Symbolic data window descriptor *)
  SymDataWindowPtr = POINTER TO SymDataWindow;
  SymDataWindow = RECORD
                    dwNext            : SymDataWindowPtr; (* next window *)
                    dwVTWindow        : VTWindow;      (* text window handle *)
                    dwVTWindowParms   : VTWindowParms; (* text window parms *)
                    dwMenuStrip       : MenuPtr;    (* local menu strip *)
                    dwTitle           : ADDRESS;    (* ptr to title string*)
                    dwDisplayOn       : BOOLEAN;    (* displaying data *)
                    dwNoLocalFrame    : BOOLEAN;    (* T:no local frame, F:Not*)
                    dwUpdateMode      : UpdateMode; (* window update mode *)
                    dwMaxColumns      : CARDINAL;   (* # of display columns *)
                    dwValueWidth      : CARDINAL;   (* width of value field *)
                    dwValueLookup     : ValueLookupArrayPtr; (* value lookup *)
                    dwHeight          : CARDINAL;   (* current height *)
                    dwTitleWidth      : CARDINAL;   (* current title width *)
                    dwDataList        : SelectDataPtr; (* data objects list *)
                    dwRedrawAll       : BOOLEAN;    (* T: redraw entire window*)
                    dwRedrawValues    : BOOLEAN;    (* T: redraw values only *)
                    dwShowingModule   : BOOLEAN;    (* T: module, F: proc *)
                    dwShowingModNo    : CARDINAL;   (* displaying module no *)
                    dwAltFgColor      : BYTE;       (* Alternate fg color *)
                  END;

VAR
  SymDataWindowList : SymDataWindowPtr; (* list of symbolic data windows *)

  lrDummy : LONGREAL;

(* ===== PRIVATE ===== *)

(* AddSelectDataNode - create a new data object and link into list *)
PROCEDURE AddSelectDataNode(VAR dw: SymDataWindow): BOOLEAN;
VAR
  sd: SelectDataPtr;
BEGIN
  sd := AllocMemory(SIZE(SelectData), MemReqSet{MemClear});
  IF (sd # NIL) THEN
    WITH dw DO
      IF (dwDataList # NIL) THEN
        dwDataList^.sdPrev := sd;
      END;

      sd^.sdNext := dwDataList;
      dwDataList := sd;

      dwDisplayOn := TRUE;
      dwRedrawAll := TRUE; (* Force redraw of window contents *)
    END;
  END;
  RETURN (sd # NIL);
END AddSelectDataNode;

(* DeleteSelectDataNode - delete first data object in list *)
PROCEDURE DeleteSelectDataNode(VAR dw: SymDataWindow);
VAR
  sd: SelectDataPtr;
BEGIN
  WITH dw DO
    sd         := dwDataList;
    dwDataList := sd^.sdNext;

    dwRedrawAll := TRUE; (* Force redraw of window contents *)
    IF (dwDataList = NIL) THEN
      dwDisplayOn := FALSE;
    ELSE
      dwDataList^.sdPrev := NIL;
    END;

    WITH sd^ DO
      IF (sdString # NIL) THEN
        DeallocString(sdString);
      END;
    END;

    DeallocMemory(sd);
  END; (* WITH *)
END DeleteSelectDataNode;

(* DeleteSelectDataNodes - delete all data objects in list *)
PROCEDURE DeleteSelectDataNodes(VAR dw: SymDataWindow);
BEGIN
  WHILE (dw.dwDataList # NIL) DO
    DeleteSelectDataNode(dw);
  END;
END DeleteSelectDataNodes;

(* DeallocValueLookupArray - deallocate value lookup array *)
PROCEDURE DeallocValueLookupArray(VAR dw: SymDataWindow);
BEGIN
  WITH dw DO
    IF (dwValueLookup # NIL) THEN
      DeallocMemory(dwValueLookup);
      dwValueLookup := NIL;
    END;
  END;
END DeallocValueLookupArray;

(* AllocValueLookupArray - allocate a value lookup array of specified size *)
PROCEDURE AllocValueLookupArray(VAR dw: SymDataWindow; height: CARDINAL): BOOLEAN;
BEGIN
  DeallocValueLookupArray(dw);
  WITH dw DO
    dwHeight      := height;
    dwValueLookup := AllocMemory(SIZE(LookupValue) * dwHeight, MemReqSet{});
    RETURN (dwValueLookup # NIL);
  END;
END AllocValueLookupArray;

(*------------------------------ NOT USED ------------------------------
(* ScrollValueLookupArray - scroll lookup array a specified number of lines *)
PROCEDURE ScrollValueLookupArray(VAR dw: SymDataWindow; offset: INTEGER);
VAR
  absOffset: CARDINAL;
  fromIdx: CARDINAL;
  toIdx: CARDINAL;
  copySize: LONGCARD;
BEGIN
  WITH dw DO
    absOffset := ABS(offset);
    IF (offset # 0) AND (dwHeight > absOffset) THEN
      IF (offset > 0) THEN (* (scroll down) *)
        fromIdx := 0;
        toIdx   := dwHeight - absOffset;
      ELSE (* offset < 0 (scroll up) *)
        fromIdx := absOffset;
        toIdx   := 0;
      END;
      copySize := (dwHeight - absOffset) * SIZE(LookupValue);

      (* movmem() handles overlaping memory block correctly! *)
      movmem(ADR(dwValueLookup^[fromIdx]),ADR(dwValueLookup^[toIdx]), copySize);
    END;
  END;
END ScrollValueLookupArray;
------------------------------ NOT USED ------------------------------ *)

(* CompareOrStoreValue - compare number to value in lookup array for equality *)
PROCEDURE CompareOrStoreValue(VAR dw: SymDataWindow; lineIdx: CARDINAL; 
                              value: LONGCARD; dontCmp: BOOLEAN): BOOLEAN;
VAR
  equal: BOOLEAN;
BEGIN
  WITH dw DO
    equal := (NOT dontCmp) AND (dwValueLookup^[lineIdx] = value);
    IF (NOT equal) THEN
      dwValueLookup^[lineIdx] := value;
    END;
  END;
  RETURN (equal);
END CompareOrStoreValue;

(* AddWindowNode - create a window node and link into window list *)
PROCEDURE AddWindowNode(): SymDataWindowPtr;
VAR
  dw: SymDataWindowPtr;
BEGIN
  dw := AllocMemory(SIZE(SymDataWindow), MemReqSet{MemClear});
  IF (dw # NIL) THEN
    IF (SymDataWindowList # NIL) THEN
      dw^.dwNext := SymDataWindowList;
    END;
    SymDataWindowList := dw;
  END;
  RETURN (dw);
END AddWindowNode;

(* DeleteWindowNode - delete window node and unlink from window list *)
PROCEDURE DeleteWindowNode(dw: SymDataWindowPtr);
VAR
  prev: SymDataWindowPtr;
  cur: SymDataWindowPtr;
BEGIN
  prev := NIL;
  cur  := SymDataWindowList;
  WHILE (cur # dw) DO
    prev := cur;
    cur  := cur^.dwNext;
  END;

  IF (prev = NIL) THEN (* First in list *)
    SymDataWindowList := dw^.dwNext;
  ELSE (* Not first in list *)
    prev^.dwNext := dw^.dwNext;
  END;

  DeallocMemory(dw);
END DeleteWindowNode;

(* GetWindowIndex - get position of window node in list *)
PROCEDURE GetWindowIndex(dw: SymDataWindowPtr): CARDINAL;
VAR
  index: CARDINAL;
  p: SymDataWindowPtr;
BEGIN
  index := 0;
  p     := SymDataWindowList;
  WHILE (dw # p) DO
    INC(index);
    p := p^.dwNext;
  END;
  RETURN (index);
END GetWindowIndex;

(* CloseSymDataWindow - close symbolic data window *)
PROCEDURE CloseSymDataWindow(VAR dw: SymDataWindow);
BEGIN
  WITH dw DO
    IfCurrentWindowClearSelection(dwVTWindow);

    DeallocValueLookupArray(dw);
    DeleteSelectDataNodes(dw);

    IF (dwMenuStrip # NIL) THEN
      ClearMenuStripVTWindow(dwVTWindow);
      FreeMenuStrip(dwMenuStrip^);
    END;

    CloseVTWindow(dwVTWindow);

    IF (dwTitle # NIL) THEN
      DeallocString(dwTitle);
    END;
  END;
  DeleteWindowNode(ADR(dw));
END CloseSymDataWindow;

(* CloseSymDataWindowNoMemory - close window and show no memory err msg *)
PROCEDURE CloseSymDataWindowNoMemory(VAR dw: SymDataWindow);
BEGIN
  CloseSymDataWindow(dw);
  ShowError(ShowErrMsgNoMemory);
END CloseSymDataWindowNoMemory;

(* CreateMenuStrip - create a local menu strip for a window *)
PROCEDURE CreateMenuStrip(VAR dw: SymDataWindow): BOOLEAN;
TYPE
  M = MenuItemMutualExcludeSet;

VAR
  ms: MenuPtr;
  ok: BOOLEAN;
BEGIN
  BeginMenuStrip();
    AddMenu(                           ADR("Symbolic-Data "));
      AddMenuItemWithCheckList(          ADR(" Proc Update Mode"), (dw.dwUpdateMode = UMProc), M{1,2});
      AddMenuItemWithCheckList(          ADR(" Module Update Mode  "), (dw.dwUpdateMode = UMModule), M{0,2});
      AddMenuItemWithCheckList(          ADR(" No Update Mode"), (dw.dwUpdateMode = UMNone), M{0,1});
      AddMenuItemNoHighlight(            ADR(" -------------------"));
      AddMenuItem(                       ADR(" Set Text Width"));
      AddMenuItem(                       ADR(" Set Value Width"));
      AddMenuItem(                       ADR(" Set Window Color"));
      AddMenuItem(                       ADR(" Set Text Color"));
      AddMenuItem(                       ADR(" Set Alternate Color"));
  ms := EndMenuStrip();
  ok := (ms # NIL);

  IF (ok) THEN
    LinkGlobalMenuStrip(ms^);
    WITH dw DO
      dwMenuStrip := ms;
      SetMenuStripVTWindow(dwVTWindow, dwMenuStrip^);
    END;
  ELSE
    CloseSymDataWindowNoMemory(dw);
  END;
  RETURN (ok);
END CreateMenuStrip;

(* SetNewTextWidth - change text width of window *)
PROCEDURE SetNewTextWidth(VAR dw: SymDataWindow);
BEGIN
  WITH dw DO
    IF (dwDisplayOn) THEN
      IfCurrentWindowClearSelection(dwVTWindow);
      dwVTWindowParms.vtpMaxColumns := dwMaxColumns;
      IF (NOT InitParmsVTWindow(dwVTWindow, dwVTWindowParms)) THEN
        CloseSymDataWindowNoMemory(dw);
      END;
    END;
  END;
END SetNewTextWidth;

(* SetDefaultWindowTitle - set default symbolic data window title *)
PROCEDURE SetDefaultWindowTitle(VAR dw: SymDataWindow);
BEGIN
  WITH dw DO
    SetTitleVTWindow(dwVTWindow, ADR(DefSymDataWindowTitle));
    IF (dwTitle # NIL) THEN
      DeallocString(dwTitle);
    END;
    dwTitle := NIL;
  END;
END SetDefaultWindowTitle;

(* SetNewWindowTitle - set a new symbolic data window title *)
PROCEDURE SetNewWindowTitle(VAR dw: SymDataWindow; title: ADDRESS);
VAR
  newTitle: ADDRESS;
BEGIN
  newTitle := AllocString(title);
  IF (newTitle # NIL) THEN
    WITH dw DO
      SetTitleVTWindow(dwVTWindow, newTitle);
      IF (dwTitle # NIL) THEN
        DeallocString(dwTitle);
      END;
      dwTitle := newTitle;
    END;
  END;
END SetNewWindowTitle;

(* ClearSymDataWindow - disable display of data in window *)
PROCEDURE ClearSymDataWindow(VAR dw: SymDataWindow);
BEGIN
  WITH dw DO
    IF (dwDisplayOn) THEN
      IfCurrentWindowClearSelection(dwVTWindow);
      SetDefaultWindowTitle(dw);
      dwDisplayOn := FALSE;
      WITH dwVTWindowParms DO
        vtpInitialLine := 0;
        vtpTotalLines  := 1;
        vtpMaxColumns  := 1;
      END;
      IF (NOT InitParmsVTWindow(dwVTWindow, dwVTWindowParms)) THEN
        CloseSymDataWindowNoMemory(dw);
      END;
    END;
  END;
END ClearSymDataWindow;

(* CreateNewWindowTitle - create and set a new symbolic window title string *)
PROCEDURE CreateNewWindowTitle(VAR dw: SymDataWindow);
VAR
  titleBuf: ADDRESS;
  selData: SelectDataPtr;
  lastData: SelectDataPtr;
  curWidth: CARDINAL;
  maxWidth: CARDINAL;
  len: CARDINAL;
BEGIN
  titleBuf := AllocMemory(TitleBufSize, MemReqSet{});
  IF (titleBuf # NIL) THEN
    titleBuf^ := 0C;
    WITH dw DO
      curWidth := 0;
      maxWidth := dwTitleWidth - 2;

      selData  := dwDataList;
      lastData := selData;

      (* Traverse object data list until maximum title width exceeded *)
      LOOP
        IF (selData = NIL) THEN EXIT; END;
        len := strlen(selData^.sdString);
        IF (curWidth + len > maxWidth) THEN EXIT; END;

        INC(curWidth, len);
        lastData := selData;
        selData  := selData^.sdNext;
      END;
      selData := lastData;

      (* If not last object in list then begin with "more data" symbol *)
      IF (selData^.sdNext # NIL) THEN
        strcpy(titleBuf, ADR("< "));
      END;

      (* Build title string from data object strings *)
      WHILE (selData # NIL) DO
        strcat(titleBuf, selData^.sdString);
        selData := selData^.sdPrev;
      END;
    END;
    SetNewWindowTitle(dw, titleBuf);
    DeallocMemory(titleBuf);
  END;
END CreateNewWindowTitle;

(* SetSymDataWindowParms - initialize window from first data object in list *)
PROCEDURE SetSymDataWindowParms(VAR dw: SymDataWindow);
BEGIN
  WITH dw DO
    IfCurrentWindowClearSelection(dwVTWindow);
    dwDisplayOn    := TRUE;
    dwNoLocalFrame := FALSE;
    dwTitleWidth   := 0;
    WITH dwVTWindowParms DO
      vtpInitialLine := dwDataList^.sdFirstLine;
      vtpTotalLines  := dwDataList^.sdTotalLines;
      vtpMaxColumns  := dwMaxColumns;
    END;
    IF (NOT InitParmsVTWindow(dwVTWindow, dwVTWindowParms)) THEN
      CloseSymDataWindowNoMemory(dw);
    END;
  END;
END SetSymDataWindowParms;

(* GetNoSonsAndNameWidth - determine number of sons in object and longest name*)
PROCEDURE GetNoSonsAndNameWidth(VAR item: Item; VAR noSons, nameWidth: CARDINAL);
VAR
  count: CARDINAL;
  maxWidth: CARDINAL;
  myItem: Item;
  myInfo: ItemInfo;
  len: CARDINAL;
  checkVarPar: BOOLEAN;
BEGIN
  myItem   := item;
  GetItemInfo(myItem, myInfo);
  checkVarPar := myInfo.iiForm = Prc;

  count    := 0;
  maxWidth := 0;
  GetSonItem(myItem);
  WHILE (NOT EmptyItem(myItem)) DO
    GetItemInfo(myItem, myInfo);
    len := strlen(myInfo.iiNameString);
    IF (checkVarPar) AND (myInfo.iiForm # Mod) AND (myInfo.iiVarPar) THEN
      INC(len, ShowVarParWidth);
    END;

    IF (len > maxWidth) THEN
      maxWidth := len;
    END;
    GetBrotherItem(myItem, 1);
    INC(count);
  END;
  noSons    := count;
  nameWidth := maxWidth;
END GetNoSonsAndNameWidth;

(* GetDataTotalLinesAndNameWidth - determine data height and name width *)
PROCEDURE GetDataTotalLinesAndNameWidth(VAR sd: SelectData;
                                        VAR totalLines, nameWidth: CARDINAL);
VAR
  info: ItemInfo;
  item: Item;
  noSons: CARDINAL;
  setMin: CARDINAL;
  setMax: CARDINAL;
  arrayMin: CARDINAL;
  arrayMax: CARDINAL;
BEGIN
  WITH sd DO
    IF (NOT EmptyItem(sdItem)) THEN
      GetItemInfo(sdItem, info);

      CASE (info.iiForm) OF
      | Pointer: totalLines := ShowPointerLinesAdj;
      | Set:     item := sdItem;
                 GetSonItem(item);
                 GetItemInfo(item, info);
                 WITH info DO
                   IF (iiForm = Enum) THEN
                     setMin := 0;
                     setMax := iiNofConst-1;
                   ELSIF (iiForm = Range) THEN
                     setMin := iiMin;
                     setMax := iiMax;
                   END;
                 END;
                 totalLines := (setMax-setMin) + 1 + ShowSetLinesAdj;
      | Array:   IF (info.iiDyn) THEN
                   arrayMin := 0;
                   arrayMax := sdItem.iHigh;
                 ELSE
                   item := sdItem;
                   GetArrayIndexItem(item);
                   GetItemInfo(item, info);
                   WITH info DO
                     arrayMin := iiMin;
                     arrayMax := iiMax;
                   END;
                 END;
                 totalLines := (arrayMax-arrayMin) + 1 + ShowArrayLinesAdj;
      | Record:  GetNoSonsAndNameWidth(sdItem, noSons, nameWidth);
                 totalLines := noSons + ShowRecordLinesAdj;
      | Prc:     GetNoSonsAndNameWidth(sdItem, noSons, nameWidth);
                 totalLines := noSons + ShowProcedureLinesAdj;
                 IF (sdShowReturn) THEN
                   IF (ReturnValueWidth > nameWidth) THEN
                     nameWidth := ReturnValueWidth;
                   END;
                   INC(totalLines);
                 END;
      | Mod:     GetNoSonsAndNameWidth(sdItem, noSons, nameWidth);
                 totalLines := noSons + ShowModuleLinesAdj;
      ELSE
        totalLines := ShowValueLinesAdj;
      END;
    END;
  END;
END GetDataTotalLinesAndNameWidth;

(* SetWindowToProc - display locals of specified procedure chain node no *)
PROCEDURE SetWindowToProc(VAR dw: SymDataWindow; procNo: CARDINAL);
VAR
  retItem: Item;
  pcn: ProcChainNodePtr;
  procInfo: ItemInfo;
BEGIN
  DeleteSelectDataNodes(dw);
  pcn := GetProcChainNode(procNo);
  IF (AddSelectDataNode(dw)) THEN
    GetItemInfo(pcn^.pcnProcItem, procInfo);
    WITH dw.dwDataList^ DO
      sdItem       := pcn^.pcnProcItem;
      sdItem.iBase := pcn^.pcnDataBase;
      sdString     := AllocString(procInfo.iiNameString);
      sdFirstLine  := 0;

      retItem := sdItem;
      GetProcReturnItem(retItem);
      sdShowReturn  := NOT EmptyItem(retItem);
      sdCurrentProc := procNo = 0;

      GetDataTotalLinesAndNameWidth(dw.dwDataList^,sdTotalLines,sdNameWidth);
    END;

    IF (pcn^.pcnLocalAlloc) THEN
      WITH dw DO
        dwShowingModule   := FALSE;
      END;
      SetSymDataWindowParms(dw);
    ELSE
      WITH dw DO
        IfCurrentWindowClearSelection(dwVTWindow);
        dwDisplayOn    := TRUE;
        dwNoLocalFrame := TRUE;
        dwTitleWidth   := 0;
        WITH dwVTWindowParms DO
          vtpInitialLine := 0;
          vtpTotalLines  := 1;
          vtpMaxColumns  := dwMaxColumns;
        END;
        IF (NOT InitParmsVTWindow(dwVTWindow, dwVTWindowParms)) THEN
          CloseSymDataWindowNoMemory(dw);
        END;
      END;
    END;
  ELSE
    ShowError(ShowErrMsgNoMemory);
  END;
END SetWindowToProc;

(* SetWindowToModule - display objects of specified module number *)
PROCEDURE SetWindowToModule(VAR dw: SymDataWindow; modNo: CARDINAL);
VAR
  modItem: Item;
  modInfo: ItemInfo;
BEGIN
  DeleteSelectDataNodes(dw);
  GetModuleItem(modNo, modItem, TRUE);
  IF (NOT EmptyItem(modItem)) THEN
    IF (AddSelectDataNode(dw)) THEN
      GetItemInfo(modItem, modInfo);
      WITH dw.dwDataList^ DO
        sdItem       := modItem;
        sdItem.iBase := GetModuleDataBase(modNo);
        sdString     := AllocString(modInfo.iiNameString);
        sdFirstLine  := 0;
        sdShowReturn := FALSE;
        GetDataTotalLinesAndNameWidth(dw.dwDataList^,sdTotalLines,sdNameWidth);
      END;
      WITH dw DO
        dwShowingModule := TRUE;
        dwShowingModNo  := modNo;
      END;
      SetSymDataWindowParms(dw);
    ELSE
      ShowError(ShowErrMsgNoMemory);
    END;
  END;
END SetWindowToModule;

(* UpdateSymDataWindow - update contents of symbolic data window *)
PROCEDURE UpdateSymDataWindow(VAR dw: SymDataWindow);
VAR
  chainNew: BOOLEAN;
  chainLength: CARDINAL;
  updateValues: BOOLEAN;
  curProcNode: ProcChainNodePtr;
BEGIN
  IF (IsProgramLoaded()) THEN
    GetProcChainLength(chainNew, chainLength);
  
    WITH dw DO
      updateValues := FALSE;
      IF (dwDisplayOn) AND (NOT chainNew) THEN
        updateValues := TRUE;
      ELSE
        IF (dwUpdateMode = UMModule) THEN
          IF (chainLength = 0) THEN (* Not in any module *)
            ClearSymDataWindow(dw);
          ELSE
            curProcNode := GetProcChainNode(0);
            WITH curProcNode^ DO
              IF (dwDisplayOn) AND (dwShowingModule) AND 
                 (pcnModNo = dwShowingModNo) THEN
                updateValues := TRUE;
              ELSE
                SetWindowToModule(dw, pcnModNo);
              END;
            END;
          END;
        ELSIF (dwUpdateMode = UMProc) THEN
          IF (chainLength = 0) THEN
            ClearSymDataWindow(dw);
          ELSE
            curProcNode := GetProcChainNode(0);
            WITH curProcNode^ DO
              IF (pcnIsProc) THEN
                SetWindowToProc(dw, 0);
              ELSE
                SetWindowToModule(dw, pcnModNo);
              END;
            END;
          END;
        ELSIF (dwUpdateMode = UMNone) THEN
          IF (dwDisplayOn) THEN
            IF (dwShowingModule) THEN
              updateValues := TRUE;
            ELSE (* Showing Procedure *)
              ClearSymDataWindow(dw);
            END;
          END;
        END;
      END;
      
      IF (updateValues) THEN
        dwRedrawValues := TRUE;
        RefreshVTWindow(dwVTWindow);
      END;
    END;
  ELSE
    ClearSymDataWindow(dw);
  END;
END UpdateSymDataWindow;

(* GetAddress - get address of object directly or indirectly *)
PROCEDURE GetAddress(adr: ADDRESS; indirect: BOOLEAN): ADDRESS;
BEGIN
  IF (indirect) THEN
    adr := GetMemoryLong(adr);
  END;
  RETURN (adr);  
END GetAddress;

(* LastStatementOfProcedure - determine if on last statement of procedure *)
PROCEDURE LastStatementOfProcedure(VAR sd: SelectData): BOOLEAN;
VAR
  last: BOOLEAN;
  pcn: ProcChainNodePtr;
  statNo: CARDINAL;
  modItem: Item;
  procItem: Item;
  firstInst: BOOLEAN;
  modOfs: LONGCARD;
BEGIN
  last := FALSE;
  WITH sd DO
    IF (sdCurrentProc) THEN
      pcn := GetProcChainNode(0);
      WITH pcn^ DO
        GetStatementNoFromModOffset(pcnModNo, pcnModOffset, statNo, firstInst);
        IF (statNo # InvalidStatNo) THEN
          INC(statNo);
          GetModOffsetFromStatementNo(pcnModNo, statNo, modOfs);
          IF (modOfs # InvalidModOfs) THEN
            GetModuleItem(pcnModNo, modItem, FALSE);
            IF (NOT EmptyItem(modItem)) THEN
              GetProcedureItem(modItem, modOfs, procItem);
              last := NOT EqualItems(sdItem, procItem);
            END;
          END;
        END;
      END
    END;
  END;
  RETURN (last);
END LastStatementOfProcedure;

(* GetProcAndModuleName - determine module and procedure name from abs adr *)
PROCEDURE GetProcAndModuleName(procAdr: LONGCARD; 
                               VAR modName, procName: ADDRESS): BOOLEAN;
VAR
  ok: BOOLEAN;
  modOfs: LONGCARD;
  modNo: CARDINAL;
  procItem: Item;
  modItem: Item;
  procInfo: ItemInfo;
  modInfo: ItemInfo;
BEGIN
  ok := FALSE;
  GetModNoModOfs(procAdr, modNo, modOfs);
  IF (modNo # InvalidModNo) THEN
    GetModuleItem(modNo, modItem, FALSE);
    IF (NOT EmptyItem(modItem)) THEN
      GetProcedureItem(modItem, modOfs, procItem);
      IF (NOT EmptyItem(procItem)) THEN
        GetItemInfo(modItem, modInfo);
        GetItemInfo(procItem, procInfo);
        IF (procInfo.iiForm = Prc) THEN
          modName  := modInfo.iiNameString;
          procName := procInfo.iiNameString;
          ok := TRUE;
        END;
      END;
    END;
  END;
  RETURN (ok);
END GetProcAndModuleName;

(* DisplayValue - output data object value in its original type format *)
PROCEDURE DisplayValue(VAR dw: SymDataWindow; outBuf: ADDRESS; value: LONGCARD;
                       lrvalue:LONGREAL; VAR item: Item; maxWidth: CARDINAL;
                       rightAdj: BOOLEAN; display: BOOLEAN);
TYPE
  ValueData = RECORD
                CASE : CARDINAL OF
                | 1: B0, B1, B2, B: BYTE;
                | 2: W0, W: CARDINAL;
                | 4: L: LONGCARD;
                END;
              END;

VAR
  info: ItemInfo;
  myItem: Item;
  myInfo: ItemInfo;
  modName: ADDRESS;
  procName: ADDRESS;
  fmtStr: ADDRESS;
  outLength: CARDINAL;
  data: ValueData;
  lrstring: ARRAY[0..20] OF CHAR;
  i: CARDINAL;
  argo: ARRAY [0..9] OF FormatArg;
BEGIN
  fmtStr := ADR("");
  data := ValueData(value);
  WITH dw DO
    GetItemInfo(item, info);
    WITH info DO
      CASE (iiForm) OF
      | Undef:    IF (iiSize = 1) THEN (* BYTE *)
                    argo[0].W := CARDINAL(data.B);
                    fmtStr    := ADR("$%02x");
                  ELSIF (iiSize = 2) THEN (* WORD *)
                    argo[0].W := data.W;
                    fmtStr    := ADR("$%04x");
                  ELSIF (iiSize = 4) THEN (* LONGWORD *)
                    argo[0].L := data.L;
                    fmtStr    := ADR("$%08lx");
                  END;
      | Bool:     IF (BOOLEAN(data.B) = FALSE) THEN
                    fmtStr := ADR("FALSE");
                  ELSIF (BOOLEAN(data.B) = TRUE) THEN
                    fmtStr := ADR("TRUE");
                  ELSE
                    argo[0].W := CARDINAL(data.B);
                    fmtStr    := ADR("$%02x");
                  END;
      | Char:     IF (CHAR(data.B) >= " ") AND (CHAR(data.B) <= "~") THEN
                    argo[0].B := data.B;
                    fmtStr    := ADR("'%c'");
                  ELSE
                    argo[0].W := CARDINAL(data.B);
                    fmtStr    := ADR("%u");
                  END;
      | Card:     argo[0].W := data.W;
                  fmtStr    := ADR("%u");
      | Int:      argo[0].W := data.W;
                  fmtStr    := ADR("%d");
      | Double:   argo[0].L := data.L;
                  fmtStr    := ADR("%ld");
      | LCard:    argo[0].L := data.L;
                  fmtStr    := ADR("%lu");
      | Real:     argo[0].L := data.L;
                  fmtStr    := ADR("%g");

      | LongReal: fmtStr    := ADR(lrstring);
                  ConvLongRealToString(lrvalue, lrstring, 10(*digits*), ShortestForm);

      | String:   (* --- Not Possible Here! --- *)
      | Enum:     IF (CARDINAL(data.B) < iiNofConst) THEN    
                    myItem := item;
                    GetSonItem(myItem);
                    GetBrotherItem(myItem, CARDINAL(data.B));
                    GetItemInfo(myItem, myInfo);
                    argo[0].L := myInfo.iiNameString;
                    fmtStr    := ADR("%s");
                  ELSE
                    argo[0].W := CARDINAL(data.B);
                    fmtStr    := ADR("$%02x");
                  END;
      | Range:    myItem := item;
                  GetSonItem(myItem);
                  DisplayValue(dw, outBuf, value, lrDummy, myItem, maxWidth,
                               rightAdj, display);
                  RETURN; (* Done! *)
      | Pointer:  IF (ADDRESS(data.L) = NIL) THEN
                    fmtStr := ADR("NIL");
                  ELSE
                    argo[0].L := data.L;
                    fmtStr    := ADR("$%08lx");
                  END;
      | Set:      IF (iiSize = 1) THEN (* 8-bit set *)
                    argo[0].W := CARDINAL(data.B);
                    fmtStr    := ADR("$%02x");
                  ELSIF (iiSize = 2) THEN (* 16-bit set *)
                    argo[0].W := data.W;
                    fmtStr    := ADR("$%04x");
                  ELSIF (iiSize = 4) THEN (* 32-bit set *)
                    argo[0].L := data.L;
                    fmtStr    := ADR("$%08lx");
                  END;
      | Array:    IF (iiDyn) THEN
                    fmtStr    := ADR("SIZE(Dynamic)");
                  ELSE
                    argo[0].W := iiSize;
                    fmtStr    := ADR("SIZE(%u)");
                  END;
      | Record:   argo[0].W := iiSize;
                  fmtStr    := ADR("SIZE(%u)");
      | ProcTyp:  IF (GetProcAndModuleName(data.L, modName, procName)) THEN
                    argo[0].L := modName;
                    argo[1].L := procName;
                    fmtStr    := ADR("%s.%s");
                  ELSE
                    argo[0].L := data.L;
                    fmtStr    := ADR("$%08lx");
                  END;
      | Opaque:   argo[0].L := data.L;
                  fmtStr    := ADR("$%08lx");
      | Prc:      (* --- Not Possible Here! --- *)
      | Mod:      (* LOCAL MODULE *)
      END;
    END;
    outLength := sprintf(outBuf, fmtStr, argo);
    IF (display) THEN
      IF (rightAdj) THEN
        IF (outLength < maxWidth) THEN
          FOR i := 1 TO (maxWidth - outLength) DO
            WriteCharVTWindow(dwVTWindow, " ");
          END;
          WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
        ELSE
          WriteBufferVTWindow(dwVTWindow, outBuf, maxWidth);
        END;
      ELSE
        WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
      END;
    END;
  END;
END DisplayValue;

(* DisplayDataValue - display unnamed data object in window *)
PROCEDURE DisplayDataValue(VAR dw: SymDataWindow; outBuf: ADDRESS;
                           OutputRow, FirstLine, LastLine: CARDINAL);
VAR
  line: CARDINAL;
  value: LONGCARD;
  lrvalue: LONGREAL;
  info: ItemInfo;
BEGIN
  WITH dw DO
    WITH dwDataList^ DO
      GetItemInfo(sdItem, info);

      FOR line := FirstLine TO LastLine DO
        SetCursorPosVTWindow(dwVTWindow, 0, OutputRow);

        IF (line = 0) THEN
          IF (dwRedrawAll) THEN
            WriteStringVTWindow(dwVTWindow, ADR(VariableValueName));
            WriteStringVTWindow(dwVTWindow, ADR(" = "));
            WriteStringVTWindow(dwVTWindow, info.iiTypeString);
            WriteCharVTWindow(dwVTWindow, ";");
          END;
        ELSE (* line = 1 *)
          value := GetMemoryValue(sdItem.iBase, FALSE, info.iiSize, ADR(lrvalue));
          IF (NOT CompareOrStoreValue(dw, OutputRow, value,
                                      dwRedrawAll OR (info.iiForm = LongReal))) THEN
            DisplayValue(dw, outBuf, value, lrvalue, sdItem, dwValueWidth, TRUE, TRUE);
          END;
        END;
        INC(OutputRow);
      END;
    END;
  END;
END DisplayDataValue;

(* DisplayPointer - display pointer data object in window *)
PROCEDURE DisplayPointer(VAR dw: SymDataWindow; outBuf: ADDRESS;
                         OutputRow, FirstLine, LastLine: CARDINAL);
VAR
  line: CARDINAL;
  ptrInfo: ItemInfo;
  dataItem: Item;
  dataInfo: ItemInfo;
  value: LONGCARD;
BEGIN
  WITH dw DO
    WITH dwDataList^ DO
      GetItemInfo(sdItem, ptrInfo);
      dataItem := sdItem;
      GetSonItem(dataItem);
      GetItemInfo(dataItem, dataInfo);

      FOR line := FirstLine TO LastLine DO
        SetCursorPosVTWindow(dwVTWindow, 0, OutputRow);

        IF (line = 0) THEN
          IF (dwRedrawAll) THEN
            WriteStringVTWindow(dwVTWindow, ptrInfo.iiTypeString);
            WriteStringVTWindow(dwVTWindow, ADR(" = POINTER TO "));
            WriteStringVTWindow(dwVTWindow, dataInfo.iiTypeString);
            WriteCharVTWindow(dwVTWindow, ";");
          END;
        ELSE (* line = 1 *)
          WITH ptrInfo DO
            value := GetMemoryValue(sdItem.iBase, FALSE, iiSize, NIL);
            IF (NOT CompareOrStoreValue(dw, OutputRow, value, dwRedrawAll)) THEN
              DisplayValue(dw, outBuf, value, lrDummy, sdItem, dwValueWidth, TRUE, TRUE);
            END;
          END;
        END;
        INC(OutputRow);
      END;
    END;
  END;
END DisplayPointer;

(* DisplaySet - display set data object in window *)
PROCEDURE DisplaySet(VAR dw: SymDataWindow; outBuf: ADDRESS;
                     OutputRow, FirstLine, LastLine: CARDINAL);
TYPE
  LONGSET = SET OF [0..31];
VAR
  line: CARDINAL;
  setInfo: ItemInfo;
  rangeItem: Item;
  rangeInfo: ItemInfo;
  elemItem: Item;
  outLength: CARDINAL;
  argo: ARRAY [0..9] OF FormatArg;
  setValue: LONGSET;
  setMin: CARDINAL;
  setMax: CARDINAL;
  idx: CARDINAL;
  value: LONGCARD;
BEGIN
  WITH dw DO
    WITH dwDataList^ DO
      GetItemInfo(sdItem, setInfo);
      rangeItem := sdItem;
      GetSonItem(rangeItem);
      GetItemInfo(rangeItem, rangeInfo);
      elemItem := rangeItem;
      WITH rangeInfo DO
        IF (iiForm = Enum) THEN
          setMin := 0;
          setMax := iiNofConst;
        ELSIF (iiForm = Range) THEN
          setMin := iiMin;
          setMax := iiMax;
          GetSonItem(elemItem);
        END;
      END;
      setValue := LONGSET(GetMemoryValue(sdItem.iBase, FALSE, setInfo.iiSize, NIL));

      FOR line := FirstLine TO LastLine DO
        SetCursorPosVTWindow(dwVTWindow, 0, OutputRow);
        IF (line = 0) THEN
          IF (dwRedrawAll) THEN
            argo[0].L := setInfo.iiTypeString;
            outLength := sprintf(outBuf, ADR("%s = SET OF "), argo);
            WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
            WriteStringVTWindow(dwVTWindow, rangeInfo.iiTypeString);
            IF (rangeInfo.iiForm = Range) THEN
              WriteCharVTWindow(dwVTWindow, "[");
              DisplayValue(dw, outBuf, setMin, lrDummy, elemItem, 0, FALSE, TRUE);
              WriteStringVTWindow(dwVTWindow, ADR(".."));
              DisplayValue(dw, outBuf, setMax, lrDummy, elemItem, 0, FALSE, TRUE);
              WriteCharVTWindow(dwVTWindow, "]");
            END;
            WriteCharVTWindow(dwVTWindow, ";");
          END;
        ELSE
          idx   := setMin + line - 1;
          value := LONGCARD(idx IN setValue);
          IF (NOT CompareOrStoreValue(dw, OutputRow, value, dwRedrawAll)) THEN
            WITH dwVTWindowParms DO
              IF (idx IN setValue) THEN
                SetColorVTWindow(dwVTWindow, dwAltFgColor, vtpDefBgColor);
              END;
              DisplayValue(dw, outBuf, idx, lrDummy, elemItem, dwValueWidth, TRUE, TRUE);
              IF (idx IN setValue) THEN
                SetColorVTWindow(dwVTWindow, vtpDefFgColor, vtpDefBgColor);
              END;
            END;
          END;
        END;
        INC(OutputRow);
      END;
    END;
  END;
END DisplaySet;

(* DisplayArray - display array data object in window *)
PROCEDURE DisplayArray(VAR dw: SymDataWindow; outBuf: ADDRESS;
                       OutputRow, FirstLine, LastLine: CARDINAL);
VAR
  line: CARDINAL;
  elemItem: Item;
  elemInfo: ItemInfo;
  idxItem: Item;
  idxInfo: ItemInfo;
  baseItem: Item;
  baseInfo: ItemInfo;
  arrayInfo: ItemInfo;
  outLength: CARDINAL;
  idx: CARDINAL;
  value: LONGCARD;
  lrvalue: LONGREAL;
  argo: ARRAY [0..9] OF FormatArg;
BEGIN
  WITH dw DO
    WITH dwDataList^ DO
      elemItem := sdItem;
      idxItem  := sdItem;
      GetSonItem(elemItem);
      GetItemInfo(elemItem, elemInfo);
      GetArrayIndexItem(idxItem);
      GetItemInfo(idxItem, idxInfo);
      baseItem := idxItem;
      GetSonItem(baseItem);
      GetItemInfo(baseItem, baseInfo);
      GetItemInfo(sdItem, arrayInfo);

      FOR line := FirstLine TO LastLine DO
        SetCursorPosVTWindow(dwVTWindow, 0, OutputRow);
        IF (line = 0) THEN
          IF (dwRedrawAll) THEN
            argo[0].L := arrayInfo.iiTypeString;
            outLength := sprintf(outBuf, ADR("%s = ARRAY ["), argo);
            WriteBufferVTWindow(dwVTWindow, outBuf, outLength);

            IF (arrayInfo.iiDyn) THEN
              argo[0].W := sdItem.iHigh;
              outLength := sprintf(outBuf, ADR("0..%u"), argo);
              WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
            ELSE
              DisplayValue(dw, outBuf, LONGCARD(idxInfo.iiMin), lrDummy,
                           baseItem, 0, FALSE, TRUE);
              WriteStringVTWindow(dwVTWindow, ADR(".."));
              DisplayValue(dw, outBuf, LONGCARD(idxInfo.iiMax), lrDummy,
                           baseItem, 0, FALSE, TRUE);
            END;
            argo[0].L := elemInfo.iiTypeString;
            outLength := sprintf(outBuf, ADR("] OF %s;"), argo);
            WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
          END;
        ELSE
          value := GetMemoryValue(sdItem.iBase+LONGCARD((line-1)*elemInfo.iiSize),
                                  FALSE, elemInfo.iiSize, ADR(lrvalue));
          IF (NOT CompareOrStoreValue(dw, OutputRow, value,
                                      dwRedrawAll OR (elemInfo.iiForm = LongReal))) THEN
             DisplayValue(dw, outBuf, value, lrvalue, elemItem, dwValueWidth, TRUE, TRUE);
          END;

          IF (dwRedrawAll) THEN
            SetCursorPosVTWindow(dwVTWindow, dwValueWidth + 2, OutputRow);
            WriteCharVTWindow(dwVTWindow, "[");
            IF (arrayInfo.iiDyn) THEN
              argo[0].W := line - 1;
              outLength := sprintf(outBuf, ADR("%u"), argo);
              WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
            ELSE
              idx := CARDINAL(idxInfo.iiMin) + line - 1;
              DisplayValue(dw, outBuf, LONGCARD(idx), lrDummy, baseItem, 0, FALSE, TRUE);
            END;
            WriteCharVTWindow(dwVTWindow, "]");
          END;
        END;
        INC(OutputRow);
      END;
    END;
  END;
END DisplayArray;

(* DisplayRecord - display record data object in window *)
PROCEDURE DisplayRecord(VAR dw: SymDataWindow; outBuf: ADDRESS;
                        OutputRow, FirstLine, LastLine: CARDINAL);
VAR
  line: CARDINAL;
  info: ItemInfo;
  item: Item;
  outLength: CARDINAL;
  value: LONGCARD;
  lrvalue: LONGREAL;
  argo: ARRAY [0..9] OF FormatArg;
BEGIN
  WITH dw DO
    WITH dwDataList^ DO
      item := sdItem;
      GetSonItem(item);
      IF (FirstLine > 0) THEN
        GetBrotherItem(item, FirstLine - 1);
      END;

      FOR line := FirstLine TO LastLine DO
        SetCursorPosVTWindow(dwVTWindow, 0, OutputRow);
        IF (line = 0) THEN
          IF (dwRedrawAll) THEN
            GetItemInfo(sdItem, info);
            WITH info DO
              argo[0].L := info.iiTypeString;
              outLength := sprintf(outBuf, ADR("%s = RECORD "), argo);
              WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
            END;
          END;
        ELSIF (line = sdTotalLines - 1) THEN
          IF (dwRedrawAll) THEN
            WriteStringVTWindow(dwVTWindow, ADR("END;"));
          END;
        ELSE
          GetItemInfo(item, info);

          WITH info DO
            value := GetMemoryValue(sdItem.iBase + iiAdr, iiIndirect, iiSize,
                                    ADR(lrvalue));
            IF (NOT CompareOrStoreValue(dw, OutputRow, value,
                                        dwRedrawAll OR (iiForm = LongReal))) THEN
              DisplayValue(dw, outBuf, value, lrvalue, item, dwValueWidth, TRUE, TRUE);
            END;
          END;

          IF (dwRedrawAll) THEN
            argo[0].W := sdNameWidth;
            argo[1].L := info.iiNameString;
            argo[2].L := info.iiTypeString;
            outLength := sprintf(outBuf, ADR("%-*s : %s;"), argo);

            SetCursorPosVTWindow(dwVTWindow, dwValueWidth + 2, OutputRow);
            WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
          END;
          GetBrotherItem(item, 1);
        END;
        INC(OutputRow);
      END;
    END;
  END;
END DisplayRecord;

(* DisplayProcedure - display procedure data object in window *)
PROCEDURE DisplayProcedure(VAR dw: SymDataWindow; outBuf: ADDRESS;
                           OutputRow, FirstLine, LastLine: CARDINAL);
VAR
  line: CARDINAL;
  info: ItemInfo;
  item: Item;
  retItem: Item;
  outLength: CARDINAL;
  value: LONGCARD;
  lrvalue: RECORD CASE :INTEGER OF 0: L0, L1:LONGCARD | 1: lr:LONGREAL; END END;
  argo: ARRAY [0..9] OF FormatArg;
BEGIN
  WITH dw DO
    WITH dwDataList^ DO
      item := sdItem;
      GetSonItem(item);
      IF (FirstLine > 1) THEN
        IF (sdShowReturn) THEN
          GetBrotherItem(item, FirstLine - 2);
        ELSE
          GetBrotherItem(item, FirstLine - 1);
        END;
      END;

      FOR line := FirstLine TO LastLine DO
        SetCursorPosVTWindow(dwVTWindow, 0, OutputRow);
        IF (line = 0) THEN
          IF (dwRedrawAll) THEN
            GetItemInfo(sdItem, info);
            argo[0].L := info.iiNameString;
            outLength := sprintf(outBuf, ADR("PROCEDURE %s();"), argo);
            WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
          END;
        ELSIF (line = sdTotalLines - 1) THEN
          IF (dwRedrawAll) THEN
            GetItemInfo(sdItem, info);
            argo[0].L := info.iiNameString;
            outLength := sprintf(outBuf, ADR("END %s;"), argo);
            WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
          END;
        ELSIF (sdShowReturn) AND (line = 1) THEN
          retItem := sdItem;
          GetProcReturnItem(retItem);
          IF (LastStatementOfProcedure(dwDataList^)) THEN
            GetItemInfo(retItem, info);
            value := GetRegisterValue(0); (* D0 *)
            IF info.iiForm = LongReal THEN
              lrvalue.L0 := value;
              lrvalue.L1 := GetRegisterValue(1); (* D1 *)
            END;
            IF (NOT CompareOrStoreValue(dw, OutputRow, value,
                                        dwRedrawAll OR (info.iiForm = LongReal))) THEN
              DisplayValue(dw, outBuf, value, lrvalue.lr, retItem, dwValueWidth, TRUE, TRUE);
            END;
          ELSE
            argo[0].W := dwValueWidth;
            argo[1].W := dwValueWidth;
            argo[2].L := ADR("[NOT SET]");
            outLength := sprintf(outBuf, ADR("%*.*s"), argo);
            WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
          END;
          IF (dwRedrawAll) THEN
            GetItemInfo(retItem, info);
            argo[0].W := sdNameWidth;
            argo[1].L := ADR(ReturnValueName);
            argo[2].L := info.iiTypeString;
            SetCursorPosVTWindow(dwVTWindow, dwValueWidth+2, OutputRow);
            outLength := sprintf(outBuf, ADR("%-*s : %s;"), argo);
            WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
          END;
        ELSE
          GetItemInfo(item, info);
          WITH info DO
            IF (LONGINT(iiAdr) > 0D) THEN
              WITH dwVTWindowParms DO
                SetColorVTWindow(dwVTWindow, dwAltFgColor, vtpDefBgColor);
              END;
            END;
            value := GetMemoryValue(sdItem.iBase + iiAdr, iiIndirect, iiSize,
                                    ADR(lrvalue));
            IF (NOT CompareOrStoreValue(dw, OutputRow, value,
                                        dwRedrawAll OR (iiForm = LongReal))) THEN
              DisplayValue(dw, outBuf, value, lrvalue.lr, item, dwValueWidth, TRUE, TRUE);
            END;

            IF (dwRedrawAll) THEN
              IF (iiForm # Mod) AND (iiVarPar) THEN
                argo[0].W := sdNameWidth - ShowVarParWidth;
                argo[1].L := iiNameString;
                argo[2].L := iiTypeString;
                outLength := sprintf(outBuf, ADR("VAR %-*s : %s;"), argo);
              ELSE
                argo[0].W := sdNameWidth;
                argo[1].L := iiNameString;
                argo[2].L := iiTypeString;
                outLength := sprintf(outBuf, ADR("%-*s : %s;"), argo);
              END;

              SetCursorPosVTWindow(dwVTWindow, dwValueWidth+2, OutputRow);
              WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
            END;
            IF (LONGINT(iiAdr) > 0D) THEN
              WITH dwVTWindowParms DO
                SetColorVTWindow(dwVTWindow, vtpDefFgColor, vtpDefBgColor);
              END;
            END;
          END;
          GetBrotherItem(item, 1);
        END;
        INC(OutputRow);
      END;
    END;
  END;
END DisplayProcedure;

(* DisplayModule - display module data object in window *)
PROCEDURE DisplayModule(VAR dw: SymDataWindow; outBuf: ADDRESS;
                        OutputRow, FirstLine, LastLine: CARDINAL);
VAR
  line: CARDINAL;
  info: ItemInfo;
  item: Item;
  outLength: CARDINAL;
  value: LONGCARD;
  lrvalue: LONGREAL;
  argo: ARRAY [0..9] OF FormatArg;
BEGIN
  WITH dw DO
    WITH dwDataList^ DO
      item := sdItem;
      GetSonItem(item);
      IF (FirstLine > 0) THEN
        GetBrotherItem(item, FirstLine - 1);
      END;

      FOR line := FirstLine TO LastLine DO
        SetCursorPosVTWindow(dwVTWindow, 0, OutputRow);
        IF (line = 0) THEN
          IF (dwRedrawAll) THEN
            GetItemInfo(sdItem, info);
            WITH info DO
              IF (iiModType = MTDef) THEN
                argo[0].L := ADR("DEFINITION ");
              ELSIF (iiModType = MTMod) THEN
                IF (GetFilePath(dwShowingModNo, FTDef) # NIL) THEN
                  argo[0].L := ADR("IMPLEMENTATION ");
                ELSE
                  argo[0].L := ADR("");
                END;
              ELSIF (iiModType = MTLocal) THEN
                argo[0].L := ADR("[LOCAL] ");
              END;
              argo[1].L := info.iiNameString;
              outLength := sprintf(outBuf, ADR("%sMODULE %s;"), argo);
              WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
            END;
          END;
        ELSIF (line = sdTotalLines - 1) THEN
          IF (dwRedrawAll) THEN
            GetItemInfo(sdItem, info);
            WITH info DO
              argo[0].L := info.iiNameString;
              IF (iiModType = MTDef) OR (iiModType = MTMod) THEN
                argo[1].B := ".";
              ELSIF (iiModType = MTLocal) THEN
                argo[1].B := ";";
              END;
              outLength := sprintf(outBuf, ADR("END %s%c"), argo);
              WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
            END;
          END;
        ELSE
          GetItemInfo(item, info);
          WITH info DO
            value := GetMemoryValue(sdItem.iBase + iiAdr, iiIndirect, iiSize,
                                    ADR(lrvalue));
            IF (NOT CompareOrStoreValue(dw, OutputRow, value,
                                        dwRedrawAll OR (iiForm = LongReal))) THEN
              DisplayValue(dw, outBuf, value, lrvalue, item, dwValueWidth, TRUE, TRUE);
            END;
          END;

          IF (dwRedrawAll) THEN
            argo[0].W := sdNameWidth;
            argo[1].L := info.iiNameString;
            argo[2].L := info.iiTypeString;
            outLength := sprintf(outBuf, ADR("%-*s : %s;"), argo);

            SetCursorPosVTWindow(dwVTWindow, dwValueWidth+2, OutputRow);
            WriteBufferVTWindow(dwVTWindow, outBuf, outLength);
          END;
          GetBrotherItem(item, 1);
        END;
        INC(OutputRow);
      END;
    END;
  END;
END DisplayModule;

(* GetSelectionItem - select data object from specified line number *)
PROCEDURE GetSelectionItem(VAR dw: SymDataWindow; line: CARDINAL; selBuf: ADDRESS;
                           VAR selItem: Item; VAR parent: BOOLEAN;
                           VAR itemAdr: ADDRESS; VAR bitNo: CARDINAL): BOOLEAN;
VAR
  ok: BOOLEAN;
  info: ItemInfo;
  myItem: Item;
  baseItem: Item;
  offset: LONGCARD;
  setMin: CARDINAL;
  idx: CARDINAL;
  r: CARDINAL;
  argo: ARRAY [0..9] OF FormatArg;
BEGIN
  ok     := FALSE;
  parent := FALSE;
  WITH dw.dwDataList^ DO
    myItem := sdItem;
    GetItemInfo(sdItem, info);
    CASE (info.iiForm) OF
    | Pointer: IF (line = 0) THEN
                 ok      := TRUE;
                 parent  := TRUE;
               ELSE (* line = 1 *)
                 ok      := TRUE;
                 itemAdr := sdItem.iBase;
                 selBuf^ := 0C;
               END;
    | Set:     IF (line = 0) THEN
                 ok      := TRUE;
                 parent  := TRUE;
               ELSE
                 ok      := TRUE;
                 itemAdr := sdItem.iBase;
                 selBuf^ := 0C;
                 (* Get Set Base Type (this will prevent further selection) *)
                 GetSonItem(myItem);
                 GetItemInfo(myItem, info);
                 WITH info DO
                   IF (iiForm = Enum) THEN
                     setMin := 0;
                   ELSIF (iiForm = Range) THEN
                     setMin := iiMin;
                   END;
                 END;
                 bitNo := setMin + line - 1;
               END;
    | Array:   IF (line = 0) THEN
                 ok      := TRUE;
                 parent  := TRUE;
               ELSE
                 ok := TRUE;
                 GetArrayIndexItem(myItem);
                 baseItem := myItem;
                 GetSonItem(baseItem);
                 GetItemInfo(myItem, info);
                 selBuf^ := "["; INC(selBuf);
                 IF (info.iiDyn) THEN
                   idx := line - 1;
                   argo[0].W := idx;
                   r := sprintf(selBuf, ADR("%u"), argo);
                 ELSE
                   idx := line - 1;
                   DisplayValue(dw, selBuf, LONGCARD(CARDINAL(info.iiMin)+idx),
                                lrDummy, baseItem, 0, FALSE, FALSE);
                 END;
                 strcat(selBuf, ADR("]"));
                 myItem := sdItem;
                 GetSonItem(myItem);
                 GetItemInfo(myItem, info);
                 offset := idx * info.iiSize;
                 myItem.iBase := GetAddress(sdItem.iBase+offset, info.iiIndirect);
                 itemAdr      := myItem.iBase;
               END;
    | Record:  IF (line = 0) THEN
                 ok      := TRUE;
                 parent  := TRUE;
               ELSIF (line = sdTotalLines - 1) THEN
                 (* Do nothing *)
               ELSE
                 ok := TRUE;
                 GetSonItem(myItem);
                 GetBrotherItem(myItem, line - 1);
                 GetItemInfo(myItem, info);
                 WITH info DO
                   myItem.iBase := GetAddress(sdItem.iBase+iiAdr, iiIndirect);
                   itemAdr      := myItem.iBase;
                   strcpy(selBuf, ADR("."));
                   strcat(selBuf, iiNameString);
                 END;
               END;
    | Prc:     IF (line = 0) OR (line = sdTotalLines - 1) OR
                  ((sdShowReturn) AND (line = 1)) THEN
                 (* Do nothing *)
               ELSE
                 ok := TRUE;
                 GetSonItem(myItem);
                 IF (sdShowReturn) THEN
                   GetBrotherItem(myItem, line - 2);
                 ELSE
                   GetBrotherItem(myItem, line - 1);
                 END;
                 GetItemInfo(myItem, info);
                 WITH info DO
                   myItem.iBase := GetAddress(sdItem.iBase+iiAdr, iiIndirect);
                   itemAdr      := myItem.iBase;
                   IF (iiForm = Array) AND (iiDyn) THEN
                     myItem.iHigh := GetMemoryHIGH(sdItem.iBase + iiAdr);
                   END;
                   strcpy(selBuf, ADR("."));
                   strcat(selBuf, iiNameString);
                 END;
               END;
    | Mod:     IF (line = 0) THEN
                 IF (info.iiModType = MTLocal) THEN
                   ok      := TRUE;
                   parent  := TRUE;
                 END;
               ELSIF (line = sdTotalLines - 1) THEN
                 (* Do nothing *)
               ELSE
                 ok := TRUE;
                 GetSonItem(myItem);
                 GetBrotherItem(myItem, line - 1);
                 GetItemInfo(myItem, info);
                 WITH info DO
                   myItem.iBase := GetAddress(sdItem.iBase+iiAdr, iiIndirect);
                   itemAdr      := myItem.iBase;
                   strcpy(selBuf, ADR("."));
                   strcat(selBuf, iiNameString);
                 END;
               END;
    ELSE
      IF (line = 0) THEN
        ok     := TRUE;
        parent := TRUE;
      ELSE (* line = 1 *)
        ok      := TRUE;
        itemAdr := sdItem.iBase;
      END;
    END; (* CASE *)
  END; (* WITH *)
  selItem := myItem;
  RETURN (ok);
END GetSelectionItem;

(* CheckSelection - create a new data object if valid selection *)
PROCEDURE CheckSelection(VAR dw: SymDataWindow; VAR selItem: Item; selBuf: ADDRESS);
VAR
  item: Item;
  info: ItemInfo;
  str: ADDRESS;
  do: BOOLEAN;
BEGIN
  do := FALSE;
  item := selItem;
  GetItemInfo(item, info);
  WITH info DO
    IF (iiForm = Pointer) THEN
      item.iBase := GetAddress(item.iBase, TRUE);
      GetSonItem(item);
      strcat(selBuf, ADR("^"));
      do := (NOT EmptyItem(item)) AND (item.iBase # NIL);
    ELSIF (iiForm = Set) OR (iiForm = Array) OR
          (iiForm = Record) OR (iiForm = Mod) THEN
      do := TRUE;
    END;
  END;
  IF (do) THEN
    str := AllocString(selBuf);
    IF (str # NIL) THEN
      IF (AddSelectDataNode(dw)) THEN
        WITH dw DO
          WITH dwDataList^ DO
            sdItem       := item;
            sdString     := str;
            sdFirstLine  := 0;
            sdShowReturn := FALSE;
            GetDataTotalLinesAndNameWidth(dwDataList^,sdTotalLines,sdNameWidth);
          END;
          SetSymDataWindowParms(dw);
        END;
      ELSE
        DeallocString(str);
        ShowError(ShowErrMsgNoMemory);
      END;
    ELSE
      ShowError(ShowErrMsgNoMemory);
    END;
  END;
END CheckSelection;

(* SelectDataObject - select object based on current display and y position *)
PROCEDURE SelectDataObject(VAR dw: SymDataWindow; y: CARDINAL);
VAR
  selItem: Item;
  parent: BOOLEAN;
  itemAdr: ADDRESS;
  bitNo: CARDINAL;
  selBuf: ADDRESS;
BEGIN
  selBuf := AllocMemory(SelectBufSize, MemReqSet{});
  IF (selBuf = NIL) THEN RETURN; END;

  WITH dw DO
    IF (GetSelectionItem(dw, y, selBuf, selItem, parent, itemAdr, bitNo)) THEN
      IF (parent) THEN
        DeleteSelectDataNode(dw);
        SetSymDataWindowParms(dw);
      ELSE
        CheckSelection(dw, selItem, selBuf);
      END;
    END;
  END;

  DeallocMemory(selBuf);
END SelectDataObject;

(* GetStorageFormat - get storage format of object *)
PROCEDURE GetStorageFormat(VAR item: Item; VAR storageFormat: StorageFormat;
                           VAR storageNumber: StorageNumber);
VAR
  info: ItemInfo;
  myItem: Item;
  format: StorageFormat;
  number: StorageNumber;
  elemItem: Item;
  elemInfo: ItemInfo;
BEGIN
  storageFormat := SFNone;

  GetItemInfo(item, info);
  CASE (info.iiForm) OF
  | Undef, Bool, Enum, Pointer,
    Set, ProcTyp, Opaque:  storageFormat := SFNumber;
                           storageNumber := SNHex;
  | Char:                  storageFormat := SFNumber;
                           storageNumber := SNChar;
  | Card, LCard:           storageFormat := SFNumber;
                           storageNumber := SNDecU;
  | Int, Double:           storageFormat := SFNumber;
                           storageNumber := SNDecS;
  | Range:                 myItem := item;
                           GetSonItem(myItem);
                           GetStorageFormat(myItem,storageFormat,storageNumber);
                           RETURN; (* Done! *)
  | Real:                  storageFormat := SFReal;
  | Array: (* an array of CHARs can be changed as a string *) 
           elemItem := item;
           GetSonItem(elemItem);
           GetItemInfo(elemItem, elemInfo);
           IF (elemInfo.iiForm = Char) THEN
             storageFormat := SFString;
           END;
  END;
END GetStorageFormat;

(* SetNewLineSelection - Setup new user selection based on y position *)
PROCEDURE SetNewLineSelection(VAR dw: SymDataWindow; y: CARDINAL);
VAR
  data: SelectionData;
  itemAdr: ADDRESS;
  bitNo: CARDINAL;
  info: ItemInfo;
  selItem: Item;
  parent: BOOLEAN;
  selBuf: ADDRESS;
BEGIN
  selBuf := AllocMemory(SelectBufSize, MemReqSet{});
  IF (selBuf = NIL) THEN RETURN; END;

  WITH data DO
    sdWindowClass := WCSymData;
    sdVTWindow    := dw.dwVTWindow;
    sdLineNo      := y;
    sdStartPos    := 0;
    sdEndPos      := dw.dwVTWindowParms.vtpMaxColumns - 1;
    sdDefFgColor  := dw.dwVTWindowParms.vtpDefFgColor;
    sdDefBgColor  := dw.dwVTWindowParms.vtpDefBgColor;

    sdAdr           := InvalidAbsAdr;
    sdStorageFormat := SFNone;

    IF (GetSelectionItem(dw, y, selBuf, selItem, parent, itemAdr, bitNo)) AND
       (NOT parent) THEN
      GetItemInfo(dw.dwDataList^.sdItem, info);
      IF (info.iiForm = Set) THEN
        sdAdr           := itemAdr;
        sdStorageFormat := SFSet;
        sdStorageSize   := info.iiSize;
        sdStorageBitNo  := bitNo;
      ELSE
        GetItemInfo(selItem, info);
        sdAdr           := itemAdr;
        GetStorageFormat(selItem, sdStorageFormat, sdStorageNumber);
        sdStorageSize   := info.iiSize;
      END;
    END;
  END; (* WITH *)
  SetSelection(data);

  DeallocMemory(selBuf);
END SetNewLineSelection;

(* VTUpdateProc - called to output new text to window *)
PROCEDURE VTUpdateProc(Window: VTWindow; OutputRow,FirstLine,LastLine: CARDINAL);
VAR
  dw: SymDataWindowPtr;
  dispFirst: CARDINAL;
  dispLast: CARDINAL;
  dispHeight: CARDINAL;
  startPos: CARDINAL;
  endPos: CARDINAL;
  lineNo: CARDINAL;
  titleWidth: CARDINAL;
  selData: SelectionData;
  info: ItemInfo;
  outBuf: ADDRESS;
BEGIN
  dw := GetUserDataVTWindow(Window);
  WITH dw^ DO
    IF (dwDisplayOn) THEN
      outBuf := AllocMemory(OutputBufSize, MemReqSet{});
      IF (outBuf = NIL) THEN RETURN; END;

      (* Check if new window title should be generated *)
      titleWidth := GetTitleBarWidthVTWindow(dwVTWindow);
      IF (titleWidth # dwTitleWidth) THEN
        dwTitleWidth := titleWidth;
        CreateNewWindowTitle(dw^);
      END;

      IF (dwNoLocalFrame) THEN (* No local stack frame msg *)
        SetCursorPosVTWindow(dwVTWindow, 0, 0);
        SetColorVTWindow(dwVTWindow,dwAltFgColor,dwVTWindowParms.vtpDefBgColor);
        WriteStringVTWindow(dwVTWindow, ADR(NoLocalFrameMsg));
      ELSE (* Symbolic data display *)
        GetDisplayRangeVTWindow(dwVTWindow, dispFirst, dispLast);
        dispHeight := dispLast - dispFirst + 1;
  
        WITH dwDataList^ DO
          IF (dwValueLookup = NIL) OR (dwHeight # dispHeight) THEN
            IF (NOT AllocValueLookupArray(dw^, dispHeight)) THEN
              ShowError(ShowErrMsgNoMemory);
              DeallocMemory(outBuf);
              RETURN; (* Done! *)
            END;
            sdFirstLine := dispFirst;
            dwRedrawAll := TRUE;
          ELSIF (sdFirstLine # dispFirst) THEN
            dwRedrawAll := TRUE;

            (*-------------------- NOT USED --------------------
            IF (dispFirst <= sdFirstLine) AND (dispLast >= sdFirstLine) THEN
              ScrollValueLookupArray(dw^, dispFirst - sdFirstLine);
            END;
            -------------------- NOT USED ----------------------*)

            sdFirstLine := dispFirst;
          ELSIF (NOT dwRedrawValues) THEN
            dwRedrawAll := TRUE;
          END;
        END;
  
        GetItemInfo(dwDataList^.sdItem, info);
        CASE (info.iiForm) OF
        | Pointer: DisplayPointer(dw^, outBuf, OutputRow, FirstLine, LastLine);
        | Set:     DisplaySet(dw^, outBuf, OutputRow, FirstLine, LastLine);
        | Array:   DisplayArray(dw^, outBuf, OutputRow, FirstLine, LastLine);
        | Record:  DisplayRecord(dw^, outBuf, OutputRow, FirstLine, LastLine);
        | Prc:     DisplayProcedure(dw^, outBuf, OutputRow, FirstLine, LastLine);
        | Mod:     DisplayModule(dw^, outBuf, OutputRow, FirstLine, LastLine);
        ELSE
          DisplayDataValue(dw^, outBuf, OutputRow, FirstLine, LastLine);
        END;
  
        (* Display current selection *)
        IF (GetSelection(selData)) THEN
          lineNo := selData.sdLineNo;
          IF (FirstLine <= lineNo) AND (LastLine >= lineNo) THEN
            IF (GetSelectionLineInfo(Window, lineNo, startPos, endPos)) THEN
              WITH dwVTWindowParms DO
                WriteColorVTWindow(Window, lineNo - dispFirst, startPos, endPos,
                                   vtpDefBgColor, vtpDefFgColor);
              END;
            END;
          END;
        END;
        dwRedrawAll    := FALSE;
        dwRedrawValues := FALSE;
      END;

      DeallocMemory(outBuf);
    END; (* IF *)
  END; (* WITH *)
END VTUpdateProc;

(* VTSelectProc - called when left mouse button click in window *)
PROCEDURE VTSelectProc(Window: VTWindow; x, y: CARDINAL; pressed: BOOLEAN);
VAR
  dw: SymDataWindowPtr;
BEGIN
  dw := GetUserDataVTWindow(Window);
  IF (dw^.dwDisplayOn) AND (NOT dw^.dwNoLocalFrame) THEN
    IF (pressed) THEN
      IF (IsPosSelected(Window, x, y)) THEN
        ClearSelection();
        SelectDataObject(dw^, y);
      ELSE
        SetNewLineSelection(dw^, y);
      END;
    ELSE
      IF (NOT IsPosSelected(Window, x, y)) THEN
        SetNewLineSelection(dw^, y);
      END;
    END;
  END;
END VTSelectProc;

(* VTMenuProc - called when menu selection made *)
PROCEDURE VTMenuProc(Window: VTWindow; MenuNum, ItemNum, SubNum: CARDINAL);
VAR
  dw: SymDataWindowPtr;
  redraw: BOOLEAN;
  update: BOOLEAN;
  fg: BYTE;
  bg: BYTE;
BEGIN
  dw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalMenuCommand(MenuNum, ItemNum, SubNum, LocalMenus,
                              WCSymData, GetWindowIndex(dw),
                              dw^.dwVTWindow)) THEN
    WITH dw^ DO
      IfCurrentWindowClearSelection(dwVTWindow);

      update := FALSE;
      redraw := FALSE;
      CASE (ItemNum) OF
      | 0: (* Proc Update Mode *)
           dwUpdateMode := UMProc;
           update := TRUE;
      | 1: (* Module Update Mode *)
           dwUpdateMode := UMModule;
           update := TRUE;
      | 2: (* No Update Mode *)
           dwUpdateMode := UMNone;
           update := TRUE;
      (*3: -------------------- *)
      | 4: (* Set Text Width *)
           IF (RequestTextWidth(dwMaxColumns)) THEN
             SetNewTextWidth(dw^);
           END;
      | 5: (* Set Value Width *)
           redraw := RequestValueWidth(dwValueWidth);
      | 6: (* Set Window Color *)
           GetDetailPenBlockPenVTWindow(dwVTWindow, fg, bg);
           IF (RequestTextFgBgColors(fg, bg)) THEN
             SetDetailPenBlockPenVTWindow(dwVTWindow, fg, bg);
           END;
      | 7: (* Set Text Color *)
           WITH dwVTWindowParms DO
             IF (RequestTextFgBgColors(vtpDefFgColor, vtpDefBgColor)) THEN
               SetDefaultColorVTWindow(dwVTWindow, vtpDefFgColor,vtpDefBgColor);
             END;
           END;
      | 8: (* Set Alternate Color *)
           redraw := RequestTextFgColor(dwAltFgColor, dwVTWindowParms.vtpDefBgColor);
      END;
      IF (redraw) THEN
        ClearVTWindow(dwVTWindow);
        RefreshVTWindow(dwVTWindow);
      ELSIF (update) THEN
        IF (dwUpdateMode = UMNone) THEN
          ClearSymDataWindow(dw^);
        ELSE
          dwDisplayOn := FALSE;
        END;
        UpdateSymDataWindow(dw^);
      END;
    END;
  END;
END VTMenuProc;

(* VTDestProc - called when user clicks on destination-data gadget *)
PROCEDURE VTDestProc(Window: VTWindow);
VAR
  dw: SymDataWindowPtr;
  data: SelectionData;
  pcn: ProcChainNodePtr;
  err: BOOLEAN;
BEGIN
  dw := GetUserDataVTWindow(Window);

  IF (IsProgramLoaded()) THEN
    err := FALSE;
    IF (GetSelection(data)) THEN
      WITH data DO
        IF (sdWindowClass = WCProcChain) THEN
          pcn := GetProcChainNode(sdProcChainNo);
          WITH pcn^ DO
            IF (pcnIsProc) THEN
              SetWindowToProc(dw^, sdProcChainNo);
            ELSE
              SetWindowToModule(dw^, pcnModNo);
            END;
          END;
        ELSIF (sdWindowClass = WCModuleList) THEN
          IF (sdModFileType = MFTSbm) OR (sdModFileType = MFTRfm) OR
             (sdModFileType = MFTNone) THEN
            SetWindowToModule(dw^, sdModNo);
          ELSE (* Invalid ModType *)
            err := TRUE;
          END;
        ELSE (* Invalid Destination *)
          err := TRUE;
        END;
      END; (* WITH *)
  
      IF (err) THEN
        ShowError(ShowErrMsgInvalidDest);
      END;
    ELSE (* No Selection *)
      ShowError(ShowErrMsgNoSrcSelect);
    END;
  END; (* IF *)
END VTDestProc;

(* VTKeyProc - called when user types key into window *)
PROCEDURE VTKeyProc(Window: VTWindow; keyBuf: ADDRESS; keyLength: CARDINAL);
VAR
  dw: SymDataWindowPtr;
  ch: CHAR;
  parent: BOOLEAN;
  itemAdr: ADDRESS;
  bitNo: CARDINAL;
  selItem: Item;
  selBuf: ARRAY [0..1] OF CHAR;
BEGIN
  dw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalKbdCommand(keyBuf, keyLength, WCSymData, 
                             GetWindowIndex(dw))) THEN
    IF (dw^.dwDisplayOn) AND (NOT dw^.dwNoLocalFrame) THEN
      IF (keyLength = 1) THEN
        ch := CHAR(keyBuf^);
        IF (ch = KeyGotoParent) THEN
          IF (GetSelectionItem(dw^, 0, ADR(selBuf[0]), selItem, parent,
                               itemAdr, bitNo))  THEN
            IF (parent) THEN
              DeleteSelectDataNode(dw^);
              SetSymDataWindowParms(dw^);
            END;
          END;
        ELSIF (ch = KeyGotoStart) THEN
          IF (dw^.dwDataList # NIL) AND (dw^.dwDataList^.sdNext # NIL) THEN
            WHILE (dw^.dwDataList^.sdNext # NIL) DO
              DeleteSelectDataNode(dw^);
            END;
            SetSymDataWindowParms(dw^);
          END;
        END;
      END;
    END;
  END;
END VTKeyProc;

(* VTCloseProc - called when close-box hit *)
PROCEDURE VTCloseProc(Window: VTWindow);
VAR
  dw: SymDataWindowPtr;
BEGIN
  dw := GetUserDataVTWindow(Window);
  CloseSymDataWindow(dw^);
END VTCloseProc;

(* VTErrorProc - called when failed to alloc text info for resized window *)
PROCEDURE VTErrorProc(Window: VTWindow);
VAR
  dw: SymDataWindowPtr;
BEGIN
  dw := GetUserDataVTWindow(Window);
  CloseSymDataWindowNoMemory(dw^);
END VTErrorProc;

(* ===== PUBLIC ===== *)

(* OpenSymDataWindow - open symbolic data window *)
PROCEDURE OpenSymDataWindow(VAR config: ConfigWindow): BOOLEAN;
VAR
  dw: SymDataWindowPtr;
  ok: BOOLEAN;
  nvt: NewVTWindow;
BEGIN
  ok := FALSE;
  dw := AddWindowNode();
  IF (dw # NIL) THEN
    WITH nvt DO
      WITH config DO
        nvtLeftEdge  := cwX;
        nvtTopEdge   := cwY;
        nvtWidth     := cwWidth;
        nvtHeight    := cwHeight;
        nvtDetailPen := cwDetailPen;
        nvtBlockPen  := cwBlockPen;
        nvtTitle     := ADR(DefSymDataWindowTitle);
        nvtMinWidth  := DefMinWindowWidth;
        nvtMinHeight := DefMinWindowHeight;
        nvtMaxWidth  := DefMaxWindowWidth;
        nvtMaxHeight := DefMaxWindowHeight;
        nvtMsgPort   := GetVTWindowsMsgPort();
        nvtUserData  := dw;
      END; (* WITH *)
    END; (* WITH *)
    dw^.dwVTWindow := OpenVTWindow(nvt);
    ok := dw^.dwVTWindow # VTWindowNIL;

    IF (ok) THEN
      WITH dw^.dwVTWindowParms DO
        WITH config DO
          vtpUpdateProc    := VTUpdateProc;
          vtpSelectProc    := VTSelectProc;
          vtpMenuProc      := VTMenuProc;
          vtpDestProc      := VTDestProc;
          vtpKeyProc       := VTKeyProc;
          vtpCloseProc     := VTCloseProc;
          vtpErrorProc     := VTErrorProc;
        (*vtpInitialLine   := 0;*)
        (*vtpTotalLines    := 1;*)
        (*vtpInitialColumn := 0;*)
        (*vtpMaxColumns    := 1;*)
          vtpDefFgColor    := cwFgColor;
          vtpDefBgColor    := cwBgColor;
        END;
      END;

      WITH dw^ DO
        WITH config DO
          dwTitle           := NIL;
          dwMaxColumns      := cwMaxCharWidth;
          dwDisplayOn       := TRUE;
          dwUpdateMode      := UpdateMode(cwSymDataUpdateMode);
          dwValueWidth      := cwSymDataValueWidth;
          dwValueLookup     := NIL;
          dwHeight          := 0;
          dwTitleWidth      := 0;
          dwDataList        := NIL;
          dwRedrawAll       := FALSE;
          dwRedrawValues    := FALSE;
          dwShowingModule   := FALSE;
          dwShowingModNo    := 0;
          dwAltFgColor      := cwSymDataAltFgColor;
        END;
      END;

      ClearSymDataWindow(dw^);
      UpdateSymDataWindow(dw^);

      ok := CreateMenuStrip(dw^);
    ELSE (* NOT ok *)
      DeleteWindowNode(dw);
    END;
  END;
  RETURN (ok);
END OpenSymDataWindow;

(* UpdateSymDataWindows - update contents of all symbolic data windows *)
PROCEDURE UpdateSymDataWindows();
VAR
  dw: SymDataWindowPtr;
  p: SymDataWindowPtr;
BEGIN
  dw := SymDataWindowList;
  WHILE (dw # NIL) DO
    p  := dw;
    dw := dw^.dwNext;
    UpdateSymDataWindow(p^);
  END;
END UpdateSymDataWindows;

(* RefreshSymDataWindows - update only values of all symbolic data windows *)
PROCEDURE RefreshSymDataWindows();
VAR
  dw: SymDataWindowPtr;
BEGIN
  dw := SymDataWindowList;
  WHILE (dw # NIL) DO
    WITH dw^ DO
      IF (dwDisplayOn) THEN
        dwRedrawValues := TRUE;
        RefreshVTWindow(dwVTWindow);
      END;
    END;
    dw := dw^.dwNext;
  END;
END RefreshSymDataWindows;

(* CloseSymDataWindows - close all symbolic data windows *)
PROCEDURE CloseSymDataWindows();
BEGIN
  WHILE (SymDataWindowList # NIL) DO
    CloseSymDataWindow(SymDataWindowList^);
  END;
END CloseSymDataWindows;

(* GetSymDataWindowInfo - get config parameters of a specified window *)
PROCEDURE GetSymDataWindowInfo(VAR index: CARDINAL;
                               VAR config: ConfigWindow): BOOLEAN;
VAR
  dw: SymDataWindowPtr;
  count: CARDINAL;
BEGIN
  count := index;
  dw    := SymDataWindowList;
  WHILE (dw # NIL) DO
    IF (count = 0) THEN
      WITH config DO
        WITH dw^ DO
          GetInfoVTWindow(dwVTWindow, cwX, cwY, cwWidth, cwHeight,
                          cwDetailPen, cwBlockPen);
          cwMaxCharWidth      := dwMaxColumns;
          cwFgColor           := dwVTWindowParms.vtpDefFgColor;
          cwBgColor           := dwVTWindowParms.vtpDefBgColor;
          cwSymDataUpdateMode := dwUpdateMode;
          cwSymDataValueWidth := dwValueWidth;
          cwSymDataAltFgColor := dwAltFgColor;
        END;
      END;
      INC(index);
      RETURN (TRUE); (* Window Found! *)
    END;
    DEC(count);
    dw := dw^.dwNext;
  END;
  RETURN (FALSE); (* End of list *)
END GetSymDataWindowInfo;

(* InitDBSymDataWindows - initialize module *)
PROCEDURE InitDBSymDataWindows(): BOOLEAN;
BEGIN
  RETURN (TRUE);
END InitDBSymDataWindows;

(* CleanupDBSymDataWindows - cleanup module *)
PROCEDURE CleanupDBSymDataWindows();
BEGIN
  CloseSymDataWindows;
END CleanupDBSymDataWindows;

BEGIN

(* NO,NO, THIS GENS CODE ASSUMING IEEEdpTrans ALREADY OPEN
   >>>>> lrDummy := 0.0;
*)

END DBSymDataWindows.
