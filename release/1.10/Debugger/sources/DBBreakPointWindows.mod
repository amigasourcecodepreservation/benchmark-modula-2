(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBBreakPointWindows.MOD           Version: Amiga.01.10             *
 * Created: 01/29/89   Updated: 03/23/89   Author: Leon Frenkel             *
 * Description: Debugger break point list windows handling module.          *
 ****************************************************************************)

IMPLEMENTATION MODULE DBBreakPointWindows;

FROM SYSTEM IMPORT
  ADDRESS, BYTE,
  ADR;
FROM CPrintBuffer IMPORT
  sprintf;
FROM DBBreakPoints IMPORT
  GetBreakPointCount, GetBreakPointInfo, GetBreakPointAdr;
FROM DBConfiguration IMPORT
  ConfigWindow;
FROM DBFiles IMPORT
  InvalidLineNo;
FROM DBMenusKbd IMPORT
  DoGlobalMenuCommand, DoGlobalKbdCommand, LinkGlobalMenuStrip;
FROM DBMisc IMPORT
  AllocMemory, DeallocMemory;
FROM DBRequesters IMPORT
  RequestTextFgBgColors, RequestTextWidth;
FROM DBSelection IMPORT
  SelectionData,
  ClearSelection, GetSelection, SetSelection, GetSelectionLineInfo,
  IsPosSelected, IfCurrentWindowClearSelection;
FROM DBStatusWindow IMPORT
  ShowErrMsgNoMemory, ShowErrMsgBadDest,
  ShowError;
FROM DBTextWindows IMPORT
  DefMinWindowWidth, DefMinWindowHeight, DefMaxWindowWidth, DefMaxWindowHeight,
  VTWindow, NewVTWindow, VTWindowNIL, VTWindowParms,
  OpenVTWindow, CloseVTWindow, InitParmsVTWindow, WriteBufferVTWindow,
  SetCursorPosVTWindow, GetUserDataVTWindow, WriteColorVTWindow,
  GetInfoVTWindow, SetMenuStripVTWindow, ClearMenuStripVTWindow,
  GetDetailPenBlockPenVTWindow, SetDetailPenBlockPenVTWindow,
  SetDefaultColorVTWindow, ClearVTWindow, RefreshVTWindow;
FROM DBUserProgram IMPORT
  IsProgramLoaded;
FROM DBWindows IMPORT
  WindowClass,
  GetVTWindowsMsgPort;
FROM FormatString IMPORT
  FormatArg;
FROM Intuition IMPORT
  Menu, MenuPtr;
FROM Memory IMPORT
  MemReqSet, MemClear;
FROM SimpleMenus IMPORT
  BeginMenuStrip, EndMenuStrip, FreeMenuStrip, AddMenu, AddMenuItem;

CONST
  (* Break Point Window Title *)
  DefBreakPointWindowTitle = "BreakPoint List";

  LocalMenus = 1; (* Number of window specific menus *)

  OutputBufSize = 1024D; (* Output buffer size in VTUpdateProc() *)

TYPE
  (* Break Point window descriptor *)
  BreakPointWindowPtr = POINTER TO BreakPointWindow;
  BreakPointWindow = RECORD
                       bwNext          : BreakPointWindowPtr; (* next window *)
                       bwVTWindow      : VTWindow;      (* text window *)
                       bwVTWindowParms : VTWindowParms; (* window parms *)
                       bwDisplayOn     : BOOLEAN;       (* displaying data *)
                       bwMaxColumns    : CARDINAL;      (* # of columns *)
                     END;

VAR
  BreakPointWindowList : BreakPointWindowPtr; (* list of break point windows *)
  BreakPointMenuStrip  : MenuPtr;             (* break point window menu strip*)

(* ===== PRIVATE ===== *)

(* AddWindowNode - create a window node and link into window list *)
PROCEDURE AddWindowNode(): BreakPointWindowPtr;
VAR
  bw: BreakPointWindowPtr;
BEGIN
  bw := AllocMemory(SIZE(BreakPointWindow), MemReqSet{MemClear});
  IF (bw # NIL) THEN
    IF (BreakPointWindowList # NIL) THEN
      bw^.bwNext := BreakPointWindowList;
    END;
    BreakPointWindowList := bw;
  END;
  RETURN (bw);
END AddWindowNode;

(* DeleteWindowNode - delete window node and unlink from window list *)
PROCEDURE DeleteWindowNode(bw: BreakPointWindowPtr);
VAR
  prev: BreakPointWindowPtr;
  cur: BreakPointWindowPtr;
BEGIN
  prev := NIL;
  cur  := BreakPointWindowList;
  WHILE (cur # bw) DO
    prev := cur;
    cur  := cur^.bwNext;
  END;

  IF (prev = NIL) THEN (* First in list *)
    BreakPointWindowList := bw^.bwNext;
  ELSE (* Not first in list *)
    prev^.bwNext := bw^.bwNext;
  END;

  DeallocMemory(bw);
END DeleteWindowNode;

(* GetWindowIndex - get position of window node in list *)
PROCEDURE GetWindowIndex(bw: BreakPointWindowPtr): CARDINAL;
VAR
  index: CARDINAL;
  p: BreakPointWindowPtr;
BEGIN
  index := 0;
  p     := BreakPointWindowList;
  WHILE (bw # p) DO
    INC(index);
    p := p^.bwNext;
  END;
  RETURN (index);
END GetWindowIndex;

(* CloseBreakPointWindow - close break point window *)
PROCEDURE CloseBreakPointWindow(VAR bw: BreakPointWindow);
BEGIN
  WITH bw DO
    IfCurrentWindowClearSelection(bwVTWindow);

    ClearMenuStripVTWindow(bwVTWindow);

    CloseVTWindow(bwVTWindow);
  END;
  DeleteWindowNode(ADR(bw));
END CloseBreakPointWindow;

(* CloseBreakPointWindowNoMemory - close window and show no memory err msg *)
PROCEDURE CloseBreakPointWindowNoMemory(VAR bw: BreakPointWindow);
BEGIN
  CloseBreakPointWindow(bw);
  ShowError(ShowErrMsgNoMemory);
END CloseBreakPointWindowNoMemory;

(* SetNewTextWidth - change text width of window *)
PROCEDURE SetNewTextWidth(VAR bw: BreakPointWindow);
BEGIN
  WITH bw DO
    IF (bwDisplayOn) THEN
      IfCurrentWindowClearSelection(bwVTWindow);
      bwVTWindowParms.vtpMaxColumns := bwMaxColumns;
      IF (NOT InitParmsVTWindow(bwVTWindow, bwVTWindowParms)) THEN
        CloseBreakPointWindowNoMemory(bw);
      END;
    END;
  END;
END SetNewTextWidth;

(* ClearBreakPointWindow - disable display of data in window *)
PROCEDURE ClearBreakPointWindow(VAR bw: BreakPointWindow);
BEGIN
  WITH bw DO
    IF (bwDisplayOn) THEN
      IfCurrentWindowClearSelection(bwVTWindow);
      bwDisplayOn := FALSE;
      WITH bwVTWindowParms DO
        vtpInitialLine := 0;
        vtpTotalLines  := 1;
        vtpMaxColumns  := 1;
      END;
      IF (NOT InitParmsVTWindow(bwVTWindow, bwVTWindowParms)) THEN
        CloseBreakPointWindowNoMemory(bw);
      END;
    END;
  END;
END ClearBreakPointWindow;

(* SetNewBPSelection - setup new user selection based on y position *)
PROCEDURE SetNewBPSelection(VAR bw: BreakPointWindow; y: CARDINAL);
VAR
  data: SelectionData;
  bpAdr: ADDRESS;
BEGIN
  WITH data DO
    sdWindowClass := WCBreakPointList;
    sdVTWindow    := bw.bwVTWindow;
    sdLineNo      := y;
    sdStartPos    := 0;
    sdEndPos      := bw.bwVTWindowParms.vtpMaxColumns - 1;
    sdDefFgColor  := bw.bwVTWindowParms.vtpDefFgColor;
    sdDefBgColor  := bw.bwVTWindowParms.vtpDefBgColor;
    sdAdr         := GetBreakPointAdr(y);
  END;
  SetSelection(data);
END SetNewBPSelection;

(* UpdateBreakPointWindow - update contents of break point window *)
PROCEDURE UpdateBreakPointWindow(VAR bw: BreakPointWindow);
VAR
  count: CARDINAL;
BEGIN
  IF (IsProgramLoaded()) THEN
    WITH bw DO
      IfCurrentWindowClearSelection(bwVTWindow);

      count := GetBreakPointCount();
      IF (count = 0) THEN (* No break points *)
        ClearBreakPointWindow(bw);
      ELSIF (bwDisplayOn) AND (count = bwVTWindowParms.vtpTotalLines) THEN
        ClearVTWindow(bwVTWindow);
        RefreshVTWindow(bwVTWindow);
      ELSE (* # of breakpoints has changed *)
        bwVTWindowParms.vtpTotalLines := count;
        bwVTWindowParms.vtpMaxColumns := bwMaxColumns;
        bwDisplayOn := TRUE;
        IF (NOT InitParmsVTWindow(bwVTWindow, bwVTWindowParms)) THEN
          CloseBreakPointWindowNoMemory(bw);
        END;
      END;
    END;
  ELSE
    ClearBreakPointWindow(bw);
  END;
END UpdateBreakPointWindow;

(* VTUpdateProc - called to output new text to window *)
PROCEDURE VTUpdateProc(Window: VTWindow; OutputRow,FirstLine,LastLine: CARDINAL);
VAR
  bw: BreakPointWindowPtr;
  outBuf: ADDRESS;
  curPos: ADDRESS;
  outLength: CARDINAL;
  line: CARDINAL;
  bpAdr: ADDRESS;
  bpEnabled: BOOLEAN;
  bpModNo: CARDINAL;
  bpLineNo: CARDINAL;
  bpModName: ADDRESS;
  bpProcName: ADDRESS;
  bpCount: LONGCARD;
  bpLimit: LONGCARD;
  startPos: CARDINAL;
  endPos: CARDINAL;
  argo: ARRAY [0..1] OF FormatArg;
BEGIN
  bw := GetUserDataVTWindow(Window);
  IF (bw^.bwDisplayOn) THEN
    outBuf := AllocMemory(OutputBufSize, MemReqSet{});
    IF (outBuf = NIL) THEN RETURN; END;

    FOR line := FirstLine TO LastLine DO
      SetCursorPosVTWindow(Window, 0, OutputRow);

      GetBreakPointInfo(line, bpAdr, bpEnabled, bpModNo, bpLineNo,
                        bpModName, bpProcName, bpCount, bpLimit);
      IF (bpEnabled) THEN
        argo[0].B := "+";
      ELSE
        argo[0].B := "-";
      END;
      argo[1].L := bpAdr;
      outLength := sprintf(outBuf, ADR("%c $%08lx"), argo);

      IF (bpModName # NIL) THEN
        argo[0].L := bpModName;
        INC(outLength, sprintf(outBuf+LONGCARD(outLength), ADR(".%s"), argo));

        IF (bpProcName # NIL) THEN
          argo[0].L := bpProcName;
          INC(outLength, sprintf(outBuf+LONGCARD(outLength), ADR(".%s"), argo));

          IF (bpLineNo # InvalidLineNo) THEN
            argo[0].W := bpLineNo + 1;
            INC(outLength, sprintf(outBuf+LONGCARD(outLength), ADR(".%u"), argo));

          END;
        END;
      END;

      argo[0].L := bpCount;
      argo[1].L := bpLimit;
      INC(outLength, sprintf(outBuf+LONGCARD(outLength), ADR("  (%lu/%lu)"), argo));

      WriteBufferVTWindow(Window, outBuf, outLength);

      IF (GetSelectionLineInfo(Window, line, startPos, endPos)) THEN
        WITH bw^.bwVTWindowParms DO
          WriteColorVTWindow(Window, OutputRow, startPos, endPos,
                             vtpDefBgColor, vtpDefFgColor);
        END;
      END;

      INC(OutputRow);
    END; (* FOR *)
    DeallocMemory(outBuf);
  END;
END VTUpdateProc;

(* VTSelectProc - called when left mouse button click in window *)
PROCEDURE VTSelectProc(Window: VTWindow; x, y: CARDINAL; pressed: BOOLEAN);
VAR
  bw: BreakPointWindowPtr;
BEGIN
  bw := GetUserDataVTWindow(Window);

  IF (bw^.bwDisplayOn) THEN
    IF (pressed) THEN
      IF (IsPosSelected(Window, x, y)) THEN
        ClearSelection();
      ELSE
        SetNewBPSelection(bw^, y);
      END;
    ELSE
      IF (NOT IsPosSelected(Window, x, y)) THEN
        SetNewBPSelection(bw^, y);
      END;
    END;
  END;
END VTSelectProc;

(* VTMenuProc - called when menu selection made *)
PROCEDURE VTMenuProc(Window: VTWindow; MenuNum, ItemNum, SubNum: CARDINAL);
VAR
  bw: BreakPointWindowPtr;
  fg: BYTE;
  bg: BYTE;
BEGIN
  bw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalMenuCommand(MenuNum, ItemNum, SubNum, LocalMenus,
                              WCBreakPointList, GetWindowIndex(bw),
                              bw^.bwVTWindow)) THEN
    WITH bw^ DO
      IfCurrentWindowClearSelection(bwVTWindow);
      CASE (ItemNum) OF
      | 0: (* Set Text Width *)
           IF (RequestTextWidth(bwMaxColumns)) THEN
             SetNewTextWidth(bw^);
           END;
      | 1: (* Set Window Color *)
           GetDetailPenBlockPenVTWindow(bwVTWindow, fg, bg);
           IF (RequestTextFgBgColors(fg, bg)) THEN
             SetDetailPenBlockPenVTWindow(bwVTWindow, fg, bg);
           END;
      | 2: (* Set Text Color *)
           WITH bwVTWindowParms DO
             IF (RequestTextFgBgColors(vtpDefFgColor, vtpDefBgColor)) THEN
               SetDefaultColorVTWindow(bwVTWindow, vtpDefFgColor,vtpDefBgColor);
             END;
           END;
      END;
    END;
  END;
END VTMenuProc;

(* VTDestProc - called when user clicks on destination-data gadget *)
PROCEDURE VTDestProc(Window: VTWindow);
BEGIN
  ShowError(ShowErrMsgBadDest);
END VTDestProc;

(* VTKeyProc - called when user types key into window *)
PROCEDURE VTKeyProc(Window: VTWindow; keyBuf: ADDRESS; keyLength: CARDINAL);
VAR
  bw: BreakPointWindowPtr;
BEGIN
  bw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalKbdCommand(keyBuf, keyLength, WCBreakPointList,
                             GetWindowIndex(bw))) THEN
  END;
END VTKeyProc;

(* VTCloseProc - called when close-box hit *)
PROCEDURE VTCloseProc(Window: VTWindow);
VAR
  bw: BreakPointWindowPtr;
BEGIN
  bw := GetUserDataVTWindow(Window);
  CloseBreakPointWindow(bw^);
END VTCloseProc;

(* VTErrorProc - called when failed to alloc text info for resized window *)
PROCEDURE VTErrorProc(Window: VTWindow);
VAR
  bw: BreakPointWindowPtr;
BEGIN
  bw := GetUserDataVTWindow(Window);
  CloseBreakPointWindowNoMemory(bw^);
END VTErrorProc;

(* ===== PUBLIC ===== *)

(* OpenBreakPointWindow - open a new break point list window *)
PROCEDURE OpenBreakPointWindow(VAR config: ConfigWindow): BOOLEAN;
VAR
  bw: BreakPointWindowPtr;
  ok: BOOLEAN;
  nvt: NewVTWindow;
BEGIN
  ok := FALSE;
  bw := AddWindowNode();
  IF (bw # NIL) THEN
    WITH nvt DO
      WITH config DO
        nvtLeftEdge  := cwX;
        nvtTopEdge   := cwY;
        nvtWidth     := cwWidth;
        nvtHeight    := cwHeight;
        nvtDetailPen := cwDetailPen;
        nvtBlockPen  := cwBlockPen;
        nvtTitle     := ADR(DefBreakPointWindowTitle);
        nvtMinWidth  := DefMinWindowWidth;
        nvtMinHeight := DefMinWindowHeight;
        nvtMaxWidth  := DefMaxWindowWidth;
        nvtMaxHeight := DefMaxWindowHeight;
        nvtMsgPort   := GetVTWindowsMsgPort();
        nvtUserData  := bw;
      END; (* WITH *)
    END; (* WITH *)
    bw^.bwVTWindow := OpenVTWindow(nvt);
    ok := bw^.bwVTWindow # VTWindowNIL;

    IF (ok) THEN
      WITH bw^.bwVTWindowParms DO
        WITH config DO
          vtpUpdateProc    := VTUpdateProc;
          vtpSelectProc    := VTSelectProc;
          vtpMenuProc      := VTMenuProc;
          vtpDestProc      := VTDestProc;
          vtpKeyProc       := VTKeyProc;
          vtpCloseProc     := VTCloseProc;
          vtpErrorProc     := VTErrorProc;
        (*vtpInitialLine   := 0;*)
        (*vtpTotalLines    := 1;*)
        (*vtpInitialColumn := 0;*)
        (*vtpMaxColumns    := 1;*)
          vtpDefFgColor    := cwFgColor;
          vtpDefBgColor    := cwBgColor;
        END;
      END;

      WITH bw^ DO
        bwDisplayOn  := TRUE;
        bwMaxColumns := config.cwMaxCharWidth;
      END;
      SetMenuStripVTWindow(bw^.bwVTWindow, BreakPointMenuStrip^);

      ClearBreakPointWindow(bw^);
      UpdateBreakPointWindow(bw^);
    ELSE (* NOT ok *)
      DeleteWindowNode(bw);
    END;
  END;
  RETURN (ok);
END OpenBreakPointWindow;

(* UpdateBreakPointWindows - update contents of all break point windows *)
PROCEDURE UpdateBreakPointWindows();
VAR
  bw: BreakPointWindowPtr;
  p: BreakPointWindowPtr;
BEGIN
  bw := BreakPointWindowList;
  WHILE (bw # NIL) DO
    p  := bw;
    bw := bw^.bwNext;
    UpdateBreakPointWindow(p^);
  END;
END UpdateBreakPointWindows;

(* CloseBreakPointWindows - close all break point windows *)
PROCEDURE CloseBreakPointWindows();
BEGIN
  WHILE (BreakPointWindowList # NIL) DO
    CloseBreakPointWindow(BreakPointWindowList^);
  END;
END CloseBreakPointWindows;

(* GetBreakPointWindowInfo - get config parameters of a specified window *)
PROCEDURE GetBreakPointWindowInfo(VAR index: CARDINAL;
                                  VAR config: ConfigWindow): BOOLEAN;
VAR
  bw: BreakPointWindowPtr;
  count: CARDINAL;
BEGIN
  count := index;
  bw    := BreakPointWindowList;
  WHILE (bw # NIL) DO
    IF (count = 0) THEN
      WITH config DO
        WITH bw^ DO
          GetInfoVTWindow(bwVTWindow, cwX, cwY, cwWidth, cwHeight,
                          cwDetailPen, cwBlockPen);
          cwMaxCharWidth := bwMaxColumns;
          cwFgColor      := bwVTWindowParms.vtpDefFgColor;
          cwBgColor      := bwVTWindowParms.vtpDefBgColor;
        END;
      END;
      INC(index);
      RETURN (TRUE); (* Window Found! *)
    END;
    DEC(count);
    bw := bw^.bwNext;
  END;
  RETURN (FALSE); (* End of list *)
END GetBreakPointWindowInfo;

(* InitDBBreakPointWindows - initialize module *)
PROCEDURE InitDBBreakPointWindows(): BOOLEAN;
BEGIN
  BeginMenuStrip();
    AddMenu(                           ADR("BreakPoint-List "));
      AddMenuItem(                       ADR(" Set Text Width"));
      AddMenuItem(                       ADR(" Set Window Color "));
      AddMenuItem(                       ADR(" Set Text Color"));
  BreakPointMenuStrip := EndMenuStrip();
  IF (BreakPointMenuStrip # NIL) THEN
    LinkGlobalMenuStrip(BreakPointMenuStrip^);
  END;
  RETURN (BreakPointMenuStrip # NIL);
END InitDBBreakPointWindows;

(* CleanupDBBreakPointWindows - cleanup module *)
PROCEDURE CleanupDBBreakPointWindows();
BEGIN
  CloseBreakPointWindows();

  IF (BreakPointMenuStrip # NIL) THEN
    FreeMenuStrip(BreakPointMenuStrip^);
  END;
END CleanupDBBreakPointWindows;

END DBBreakPointWindows.
