(* RealConversionsSwitch.mod *)

(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: RealConversions.MOD               Version: Amiga.00.00             *
 * Created: 12/25/86   Updated:  1/29/94   Author: Leon Frenkel             *
 * Description: Conversion of REAL numbers to strings and conversion of     *
 *  strings to REAL numbers.                                                *
 * NOTE: The ConvRealToString() function contains implementation specific   *
 *       code!                                                              *
 ****************************************************************************)

IMPLEMENTATION MODULE RealConversions;

FROM SYSTEM  IMPORT ADDRESS, BYTE, ADR;
FROM System  IMPORT REALflavor;

FROM RealOps IMPORT FltAdd, FltCmp, FltDiv, FltFloat, FltMax, FltMul, FltNeg,
                    FltSub, FltTrunc;
(*$L-,D-*)

VAR
  round : ARRAY [0..17] OF REAL; (* Used by ConvRealToString *)

(*--------------------*)
 PROCEDURE AssignRound;
(*--------------------*)

VAR  one,
     two,
     ten :REAL;

BEGIN

one := FltFloat(1);
two := FltFloat(2);
ten := FltFloat(10);

round[0]  := ten;
round[1]  := one;
round[2]  := FltDiv(round[1],  two);
round[3]  := FltDiv(round[2],  ten);
round[4]  := FltDiv(round[3],  ten);
round[5]  := FltDiv(round[4],  ten);
round[6]  := FltDiv(round[5],  ten);
round[7]  := FltDiv(round[6],  ten);
round[8]  := FltDiv(round[7],  ten);
round[9]  := FltDiv(round[8],  ten);
round[10] := FltDiv(round[9],  ten);
round[11] := FltDiv(round[10], ten);
round[12] := FltDiv(round[11], ten);
round[13] := FltDiv(round[12], ten);
round[14] := FltDiv(round[13], ten);
round[15] := FltDiv(round[14], ten);
round[16] := FltDiv(round[15], ten);
round[17] := FltDiv(round[16], ten);

END AssignRound;

(*======================================================*)
 PROCEDURE ConvStringToReal(buffer: ARRAY OF CHAR): REAL;
(*======================================================*)
CONST
  TAB = 011C;
VAR 
  acc   : REAL;
  zero  : REAL;
  ten   : REAL;
  msign : BOOLEAN;
  esign : BOOLEAN;
  dpflg : BOOLEAN;
  i     : INTEGER;
  dexp  : INTEGER;
  cp    : POINTER TO CHAR;
BEGIN
  zero := FltFloat(0);  (* 0.0 *)
  ten  := FltFloat(10); (* 10.0 *)
  cp := ADR(buffer);
  WHILE (cp^ = " ") OR (cp^ = TAB) DO
    INC(ADDRESS(cp));
  END;
  IF cp^ = "-" THEN
    INC(ADDRESS(cp));
    msign := TRUE;
  ELSE
    msign := FALSE;
    IF (cp^ = "+") THEN
      INC(ADDRESS(cp));
    END;
  END;
  dpflg := FALSE;
  dexp  := 0;
  acc := zero;
  LOOP
    IF (cp^ >= "0") AND (cp^ <= "9") THEN
      acc := FltMul(acc, ten);
      acc := FltAdd(acc, FltFloat(ORD(cp^) - ORD("0")));
      IF dpflg THEN
        DEC(dexp);
      END;
    ELSIF cp^ = "." THEN
      IF dpflg THEN
        EXIT;
      END;
      dpflg := TRUE;
    ELSE
      EXIT;
    END;
    INC(ADDRESS(cp));
  END; (* LOOP *)
  IF (cp^ = "e") OR (cp^ = "E") THEN
    INC(ADDRESS(cp));
    IF cp^ = "-" THEN
      INC(ADDRESS(cp));
      esign := TRUE;
    ELSE
      esign := FALSE;
      IF cp^ = "+" THEN
        INC(ADDRESS(cp));
      END;
    END;
    i := 0;
    WHILE (cp^ >= "0") AND (cp^ <= "9") DO
      i := (i*10) + (INTEGER(cp^) - INTEGER("0"));
      INC(ADDRESS(cp));
    END;
    IF esign THEN 
      i := -i;
    END;
    INC(dexp,i);
  END;
  IF dexp < 0 THEN
    WHILE dexp # 0 DO
      INC(dexp);
      acc := FltDiv(acc, ten);
    END;
  ELSIF dexp > 0 THEN
    WHILE dexp # 0 DO
      DEC(dexp);
      acc := FltMul(acc, ten);
    END;
  END;
  IF msign THEN
    acc := FltNeg(acc);
  END;
  RETURN acc;
END ConvStringToReal;

(*--------------------------------------------------------------*)
 PROCEDURE StrAsg(InStr:ARRAY OF CHAR; VAR OutStr:ARRAY OF CHAR);
(*--------------------------------------------------------------*)

(* >>> SPECIAL: DOES NOT CHECK HIGH AND ASSUMES 0C ENDS INSTR *)

VAR i  :CARDINAL;
   
BEGIN

i := 0;
LOOP
   (*IF i > HIGH(OutStr) THEN
      EXIT
   ELS*)IF (*(i > HIGH(InStr)) OR*) (InStr[i] = 0C) THEN
      OutStr[i] := 0C;
      RETURN;
   ELSE
     OutStr[i] := InStr[i];
     INC(i);
   END;
END;

END StrAsg;
   
(*========================================================================*)
 PROCEDURE ConvRealToString(number: REAL; VAR buffer: ARRAY OF CHAR; 
                            maxwidth: CARDINAL; format: RealToStringFormat);
(*========================================================================*)
VAR i, exp, digit, decpos, ndig, bidx: INTEGER;
BEGIN

  AssignRound;

  (*................................................................*)
  (* This code is for the Amiga fast floating point format only!    *)
  (* It checks for invalid floating point numbers, to avoid a crash!*)
  (* An invalid number is $xxxxxx00 or $xxxxxx80 where x is non-zero*)
  IF REALflavor = 0 THEN
    IF (CARDINAL(BYTE(LONGCARD(number))) = 00H) OR
       (CARDINAL(BYTE(LONGCARD(number))) = 80H) THEN
      number := FltFloat(0);
    END;
  END;
  (*................................................................*)

  (*................................................................*)
  (* This code is for the Amiga IEEE single precision format only!  *)
  (* It checks for IEEE "Infinities" and "NANs", which can cause    *)
  (* code here to go into infinite loops                            *)
  IF REALflavor = 1 THEN
    IF ((LONGCARD(number) DIV 8388608D) MOD 256D) = 255D THEN  (* Exp = all 1's *)
       IF (LONGCARD(number) MOD 8388608D) = 0D THEN
          IF LONGINT(number) >= 0D THEN
             StrAsg("+INF", buffer);
          ELSE
             StrAsg("-INF", buffer);
          END;
       ELSE
          StrAsg("NAN", buffer);
       END;
       RETURN;
    END;
  END;
  (*................................................................*)

  bidx := 0;
  ndig := maxwidth+1;
  exp  := 0;
  IF FltCmp(number, FltFloat(0)) < 0 THEN
    number := FltNeg(number);
    buffer[bidx] := "-"; INC(bidx);
  END;
  IF FltCmp(number, FltFloat(0)) > 0 THEN
    WHILE FltCmp(number, round[1]) < 0 DO
      number := FltMul(number, round[0]);
      DEC(exp);
    END;
    WHILE FltCmp(number, round[0]) >= 0 DO
      number := FltDiv(number, round[0]);
      INC(exp);
    END;
  END;

  IF format = ShortestForm THEN
    ndig := maxwidth;
    IF (exp < -4) OR (exp > INTEGER(maxwidth)) THEN
      format := Scientific;
    END;
  ELSIF format = Decimal THEN
    ndig := ndig + exp;
  END;

  IF ndig >= 0 THEN
    IF ndig > 16 THEN
      number := FltAdd(number, round[17]);
    ELSE
      number := FltAdd(number, round[ndig+1]);
    END;
    IF FltCmp(number, round[0]) >= 0 THEN
      number := round[1];
      INC(exp);
      IF (format = Decimal) OR (format = ShortestForm) THEN INC(ndig) END;
    END;
  END;

  IF (format = Decimal) OR (format = ShortestForm) THEN
    IF exp < 0 THEN
      buffer[bidx] := "0"; INC(bidx);
      buffer[bidx] := "."; INC(bidx);
      i := -exp - 1;
      IF ndig <= 0 THEN
        i := maxwidth;
      END;
      WHILE i # 0 DO
        DEC(i);
        buffer[bidx] := "0"; INC(bidx);
      END;
      decpos := 0;
    ELSE
      decpos := exp + 1;
    END;
  ELSE
    decpos := 1;
  END;

  IF ndig > 0 THEN
    i := 0;
    LOOP
      IF i < 16 THEN
        digit := FltTrunc(number);
        buffer[bidx] := CHAR(digit + ORD("0")); INC(bidx);
        number := FltMul(FltSub(number, FltFloat(digit)), round[0]);
      ELSE
        buffer[bidx] := "0"; INC(bidx);
      END;
      DEC(ndig);
      IF ndig = 0 THEN
        EXIT;
      END;
      IF (decpos # 0) AND (decpos - 1 = 0) THEN
        buffer[bidx] := "."; INC(bidx);
      END;
      DEC(decpos);
      INC(i);
    END;
  END;

  IF format = Scientific THEN
    buffer[bidx] := "E"; INC(bidx);
    IF exp < 0 THEN
      exp := -exp;
      buffer[bidx] := "-"; INC(bidx);
    ELSE
      buffer[bidx] := "+"; INC(bidx);
    END;
    IF exp >= 100 THEN
      buffer[bidx] := CHAR(exp DIV 100 + ORD("0")); INC(bidx);
      exp := exp MOD 100;
    END;
    buffer[bidx] := CHAR(exp DIV 10 + ORD("0")); INC(bidx);
    buffer[bidx] := CHAR(exp MOD 10 + ORD("0")); INC(bidx);
  END;
  buffer[bidx] := 0C;
END ConvRealToString;

BEGIN

AssignRound;

(* (Same as above floating point numbers but compile time REAL flavor 
  round[00] := 1.0E1;
  round[01] := 1.0E0;
  round[02] := 5.0E-1;
  round[03] := 5.0E-2;
  round[04] := 5.0E-3;
  round[05] := 5.0E-4;
  round[06] := 5.0E-5;
  round[07] := 5.0E-6;
  round[08] := 5.0E-7;
  round[09] := 5.0E-8;
  round[10] := 5.0E-9;
  round[11] := 5.0E-10;
  round[12] := 5.0E-11;
  round[13] := 5.0E-12;
  round[14] := 5.0E-13;
  round[15] := 5.0E-14;
  round[16] := 5.0E-15;
  round[17] := 5.0E-16;
*)

(* (Same as above floating point numbers but in hex format)
  round[00] := REAL(0A0000044H);
  round[01] := REAL(080000041H);
  round[02] := REAL(080000040H);
  round[03] := REAL(0CCCCCD3CH);
  round[04] := REAL(0A3D70A39H);
  round[05] := REAL(083126E36H);
  round[06] := REAL(0D1B71632H);
  round[07] := REAL(0A7C5AB2FH);
  round[08] := REAL(08637BC2CH);
  round[09] := REAL(0D6BF9328H);
  round[10] := REAL(0ABCC7625H);
  round[11] := REAL(089705E22H);
  round[12] := REAL(0DBE6FD1EH);
  round[13] := REAL(0AFEBFE1BH);
  round[14] := REAL(08CBCCB18H);
  round[15] := REAL(0E12E1214H);
  round[16] := REAL(0B424DB11H);
  round[17] := REAL(0901D7C0EH);
*)
END RealConversions.
