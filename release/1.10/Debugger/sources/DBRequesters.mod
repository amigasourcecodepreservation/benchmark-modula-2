(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBRequesters.MOD                  Version: Amiga.01.10             *
 * Created: 02/28/89   Updated: 06/08/89   Author: Leon Frenkel             *
 * Description: Debugger user input requesters module.                      *
 ****************************************************************************)

IMPLEMENTATION MODULE DBRequesters;

FROM SYSTEM IMPORT
  ADDRESS, BYTE, WORD, LONGWORD,
  ADR;
FROM AmigaDOSProcess IMPORT
  Delay;
FROM CPrintBuffer IMPORT
  sprintf;
FROM CScanBuffer IMPORT
  sscanf;
FROM CStrings IMPORT
  strlen, strcpy;
FROM DBSelection IMPORT
  StorageNumber;
FROM DBWindows IMPORT
  GetScreen;
FROM DBTextWindows IMPORT
  SetSizeVerifyModeAllVTWindows;
FROM FormatString IMPORT
  FormatArg;
FROM Intuition IMPORT
  IDCMPFlags, IDCMPFlagsSet,
  WindowFlags, WindowFlagsSet,
  Window, WindowPtr, GadgetPtr, Gadget, IntuiMessagePtr, Image, Border,
  IntuiText, PropInfoPtr,
  StringInfoPtr, ScreenPtr, GadgetActivation, GadgetActivationSet,
  CloseWindow, DrawBorder, PrintIText, ActivateGadget, NewModifyProp;
FROM Ports IMPORT
  WaitPort, GetMsg, ReplyMsg;
FROM Rasters IMPORT
  Jam1, Jam2;
FROM ScanString IMPORT
  ScanArg;
FROM SimpleGadgets IMPORT
  LastGadget, DefaultGadgetFlags,
  BeginGadgetList, EndGadgetList, FreeGadgetList, AddGadgetTextButton,
  AddGadgetImageButton, AddGadgetString, AddGadgetProp, GlobalGadgetOpt;
FROM SimpleWindows IMPORT
  CreateWindow;
FROM Views IMPORT
  GetRGB4, LoadRGB4, SetRGB4;

CONST
  (* ===== Text Fg/Bg color selection requester parameters ===== *)
  (* Window parameters *)
  WinTextColorX       = 0;
  WinTextColorY       = 0;
  WinTextColorWidth   = 256;
  WinTextColorHeight  = 75;
  WinTextColorIDCMP   = IDCMPFlagsSet{GadgetUp};
  WinTextColorFlags   = WindowFlagsSet{WindowDrag, WindowDepth, Activate};
  WinTextColorTitle   = "Select Color";

  (* OK and CANCEL gadget parameters *)
  GadTextColorOkX     = 2 + 20;
  GadTextColorOkY     = 55;
  GadTextColorCancelX = WinTextColorWidth - 2 - 20 - 58;
  GadTextColorCancelY = 55;

  (* Fg/Bg color select box parameters *)
  GadTextColorWidth   = 32;
  GadTextColorHeight  = 10;
  GadTextColorFgBaseX = 62;
  GadTextColorFgBaseY = 13;
  GadTextColorBgBaseX = 62;
  GadTextColorBgBaseY = 24;

  (* Position for drawing sample text string *)
  SampleTextColorX = 82;
  SampleTextColorY = 40;

  (* ===== Screen color request parameters ===== *) 
  (* Window parameters *)
  WinScrColorX       = 0;
  WinScrColorY       = 0;
  WinScrColorWidth   = 256;
  WinScrColorHeight  = 75;
  WinScrColorIDCMP   = IDCMPFlagsSet{GadgetUp, MouseMove};
  WinScrColorFlags   = WindowFlagsSet{WindowDrag, WindowDepth, Activate};
  WinScrColorTitle   = "Select Screen Colors";

  (* OK and CANCEL gadget parameters *)
  GadScrColorOkX     = 2 + 20;
  GadScrColorOkY     = 60;
  GadScrColorCancelX = WinScrColorWidth - 2 - 20 - 58;
  GadScrColorCancelY = 60;

  (* Color number selection box parameters *)
  GadScrColorWidth   = 32;
  GadScrColorHeight  = 10;
  GadScrColorFgBaseX = 62;
  GadScrColorFgBaseY = 13;

  (* Color RGB proportional gads parameters *)
  GadScrColorRedX        = 62;
  GadScrColorRedY        = 25;
  GadScrColorRedWidth    = 128;
  GadScrColorRedHeight   = 9;
  GadScrColorGreenX      = 62;
  GadScrColorGreenY      = 35;
  GadScrColorGreenWidth  = 128;
  GadScrColorGreenHeight = 9;
  GadScrColorBlueX       = 62;
  GadScrColorBlueY       = 45;
  GadScrColorBlueWidth   = 128;
  GadScrColorBlueHeight  = 9;

  (* Proportional gads parameters *)
  GadScrColorPot  = 0;
  GadScrColorBody = 01000H;
  GadScrColorMult = 01111H;

  (* ===== String/number requester parameters *)
  (* Window parameters *)
  WinStringX       = 0;
  WinStringY       = 0;
  WinStringWidth   = 256;
  WinStringHeight  = 75;
  WinStringIDCMP   = IDCMPFlagsSet{GadgetUp};
  WinStringFlags   = WindowFlagsSet{WindowDrag, WindowDepth, Activate};

  (* OK and CANCEL gadget parameters *)
  GadStringOkX     = 2 + 20;
  GadStringOkY     = 55;
  GadStringCancelX = WinStringWidth - 2 - 20 - 58;
  GadStringCancelY = 55;

  (* String gadget parameters *)
  GadStringStrX         = 22;
  GadStringStrY         = 30;
  GadStringStrDispField = 26;
  GadStringStrBufSize   = RequestMaxStringLength;

  (* Position for displaying error messages *)
  ErrorStringX = 22;
  ErrorStringY = 13;

  (* Error message strings *)
  ErrorInvalidString = "Invalid Input      ";
  ErrorRangeString   = "Value Out Of Bounds";

  (* ===== Memory operation verifacation requester ===== *)
  WinVerifyX       = 0;
  WinVerifyY       = 0;
  WinVerifyWidth   = 256;
  WinVerifyHeight  = 75;
  WinVerifyIDCMP   = IDCMPFlagsSet{GadgetUp};
  WinVerifyFlags   = WindowFlagsSet{WindowDrag, WindowDepth, Activate};

  (* OK and CANCEL gadget parameters *)
  GadVerifyOkX     = 2 + 20;
  GadVerifyOkY     = 55;
  GadVerifyCancelX = WinVerifyWidth - 2 - 20 - 58;
  GadVerifyCancelY = 55;

  (* Starting position for text *)
  TextVerifyX = 36;
  TextVerifyY = 12;


  (* Strings used as titles in various requesters *)
  ReqTextWidthTitle      = "Enter Text Width";
  ReqValueWidthTitle     = "Enter Value Width";
  ReqContextLinesTitle   = "Enter Context Lines";
  ReqBreakCountTitle     = "Enter Break Count";
  ReqPriorityTitle       = "Enter Process Priority";
  ReqStackSizeTitle      = "Enter Process Stack Size";
  ReqCurrentDirTitle     = "Enter Current Directory";
  ReqMaxModFilesTitle    = "Enter Max # .MOD Files";
  ReqSymbolDirListTitle  = "Enter Symbol Dir List";
  ReqSourceDirListTitle  = "Enter Source Dir List";
  ReqFileNameTitle       = "Enter File Name";
  ReqProgParmsTitle      = "Enter Program Parameters";
  ReqSearchStringTitle   = "Enter Search String";
  ReqRealValueTitle      = "Enter REAL Value";
  ReqStringValueTitle    = "Enter STRING Value";
  ReqNumber8ValueTitle   = "Enter 8-BIT Value";
  ReqNumber16ValueTitle  = "Enter 16-BIT Value";
  ReqNumber32ValueTitle  = "Enter 32-BIT Value";
  ReqRegisterTitle       = "Enter Register Value";
  ReqAddressTitle        = "Enter Address";
  ReqCopyMemoryTitle     = "Verify Memory Copy";
  ReqFillMemoryTitle     = "Verify Memory Fill";
  ReqSearchMemoryTitle   = "Verify Memory Search";
  ReqContinueSearchTitle = "Continue Memory Search";

  (* Text for standard OK and CANCEL gadgets *)
  OKString     = "  OK  ";
  CANCELString = "CANCEL";

TYPE
  (* String/Number requester data types *)

  (* User input error code *)
  InputError  = (IEInvalid, IERange);

  (* User input data type *)
  InputFormat = (IFReal, IFString, IFNumber);

  (* User input IFNumber data size *)
  NumberSize  = (NSByte, NSWord, NSLong);

  (* String/number requester input/output descriptor record *)
  InputData = RECORD
                idInitialString : ADDRESS;    (* Initial display string *)
                idError         : InputError; (* Internal error code *)
                CASE idFormat : InputFormat OF
                | IFReal:   idReal       : REAL;       (* return REAL value *)
                | IFString: idString     : ADDRESS;    (* ptr to output buf *)
                | IFNumber: idNumber     : LONGCARD;   (* return binary value *)
                            idNumberSize : NumberSize; (* size of input value *)
                END;
              END;

(* ===== PRIVATE ===== *)

(* IsValidInputData - check if buffer contains a valid input value *)
PROCEDURE IsValidInputData(VAR id: InputData; buffer: ADDRESS): BOOLEAN;
VAR
  ok: BOOLEAN;
  scan: BOOLEAN;
  ch: CHAR;
  value: LONGCARD;
  ivalue: LONGINT;
  scanStr: ADDRESS;
  argi: ARRAY [0..0] OF ScanArg;
BEGIN
  ok := FALSE;
  WITH id DO
    idError := IEInvalid;
    CASE (idFormat) OF
    | IFReal:   argi[0] := ADR(idReal);
                ok := sscanf(buffer, ADR("%f"), argi) = 1;
    | IFString: ok := TRUE;
                strcpy(idString, buffer);
    | IFNumber: scan := TRUE;
                ch := CHAR(buffer^);
                IF (ch = "$") THEN (* Hex *)
                  scanStr := ADR("%X");
                  INC(buffer);
                ELSIF (ch = "@") THEN (* Octal *)
                  scanStr := ADR("%O");
                  INC(buffer);
                ELSIF (ch = "%") THEN (* Binary *)
                  scanStr := ADR("%B");
                  INC(buffer);
                ELSIF (ch = "'") THEN (* Char: valid NSByte, NSWord, NSLong *)
                  scan := FALSE;
                  INC(buffer);
                  IF (CHAR(buffer^) # 0C) THEN
                    idNumber := LONGCARD(CHAR(buffer^));
                    INC(buffer);
                    ok := (CHAR(buffer^) = "'");
                  END;
                ELSE (* Decimal *)
                  scanStr := ADR("%D");
                END;
                argi[0] := ADR(value);
                IF (scan) AND (sscanf(buffer, scanStr, argi) = 1) THEN
                  idNumber := value;
                  ivalue   := value;
                  IF (idNumberSize = NSByte) THEN
                    ok := (value >= 0D) AND (value <= 255D);
                    IF (NOT ok) THEN
                      ok := (ivalue >= -128D) AND (ivalue <= 127D);
                      idNumber := LONGCARD(BYTE(ivalue));
                    END;
                  ELSIF (idNumberSize = NSWord) THEN
                    ok := (value >= 0D) AND (value <= 65535D);
                    IF (NOT ok) THEN
                      ok := (ivalue >= -32768D) AND (ivalue <= 32767D);
                      idNumber := LONGCARD(WORD(ivalue));
                    END;
                  ELSIF (idNumberSize = NSLong) THEN
                    ok := TRUE;
                  END;
                  IF (NOT ok) THEN
                    idError := IERange;
                  END;
                END;
    END; (* CASE *)
  END;
  RETURN (ok);
END IsValidInputData;

(* InitTextColorImage - initialize image structure to display filled box *)
PROCEDURE InitTextColorImage(VAR image: Image; color: CARDINAL);
BEGIN
  WITH image DO
    LeftEdge   := 0;
    TopEdge    := 0;
    Width      := GadTextColorWidth;
    Height     := GadTextColorHeight;
    Depth      := 2;
    ImageData  := NIL;
    PlanePick  := BYTE(0);
    PlaneOnOff := BYTE(color);
    NextImage  := NIL;
  END;
END InitTextColorImage;

(* ShowString - draw a string in a specified window at x, y position *)
PROCEDURE ShowString(VAR win: Window; x, y: CARDINAL; str: ADDRESS);
VAR
  iText: IntuiText;
BEGIN
  IF (str # NIL) THEN
    WITH iText DO
      FrontPen  := BYTE(1);
      BackPen   := BYTE(0);
      DrawMode  := Jam2;
      LeftEdge  := 0;
      TopEdge   := 0;
      ITextFont := NIL;
      IText     := str;
      NextText  := NIL;
    END;
    PrintIText(win.RPort^, iText, x, y);
  END;
END ShowString;

(* DoTextFgBgColorRequester - display text fg/bg color requester *)
PROCEDURE DoTextFgBgColorRequester(VAR fg, bg: BYTE; fgOnly: BOOLEAN): BOOLEAN;
VAR
  gads: GadgetPtr;
  win: WindowPtr;
  done: BOOLEAN;
  ok: BOOLEAN;
  gad: GadgetPtr;
  gadId: CARDINAL;
  msg: IntuiMessagePtr;
  imageTable: ARRAY [0..7] OF Image;
  colorBorder: Border;
  sampleText: IntuiText;
  XYList: ARRAY [0..7],[0..1] OF CARDINAL;
  i: CARDINAL;
BEGIN
  SetSizeVerifyModeAllVTWindows(FALSE);

  ok := FALSE;
  BeginGadgetList();
    AddGadgetTextButton(GadTextColorOkX, GadTextColorOkY, ADR(OKString));
    AddGadgetTextButton(GadTextColorCancelX, GadTextColorCancelY, ADR(CANCELString));

    XYList[0][0] := 0;
    XYList[0][1] := 0;
    XYList[1][0] := (GadTextColorWidth * 4) + 1;
    XYList[1][1] := 0;
    XYList[2][0] := (GadTextColorWidth * 4) + 1;
    XYList[2][1] := GadTextColorHeight + 1;
    XYList[3][0] := 0;
    XYList[3][1] := GadTextColorHeight + 1;
    XYList[4][0] := 0;
    XYList[4][1] := 0;
    XYList[5][0] := 0;
    XYList[5][1] := (GadTextColorHeight * 2) + 2;
    XYList[6][0] := (GadTextColorWidth * 4) + 1;
    XYList[6][1] := (GadTextColorHeight * 2) + 2;
    XYList[7][0] := (GadTextColorWidth * 4) + 1;
    XYList[7][1] := GadTextColorHeight + 1;

    WITH colorBorder DO
      LeftEdge   := GadTextColorFgBaseX-1;
      TopEdge    := GadTextColorFgBaseY-1;
      FrontPen   := BYTE(1);
      BackPen    := BYTE(0);
      DrawMode   := Jam1;
      IF (fgOnly) THEN
        Count    := BYTE(5);
      ELSE
        Count    := BYTE(8);
      END;
      XY         := ADR(XYList);
      NextBorder := NIL;
    END;

    WITH sampleText DO
      FrontPen  := BYTE(fg);
      BackPen   := BYTE(bg);
      DrawMode  := Jam2;
      LeftEdge  := SampleTextColorX;
      TopEdge   := SampleTextColorY;
      ITextFont := NIL;
      IText     := ADR("Sample Text");
      NextText  := NIL;
    END;

    FOR i := 0 TO 3 DO
      InitTextColorImage(imageTable[i], i);
      AddGadgetImageButton(GadTextColorFgBaseX + (GadTextColorWidth * i),
                           GadTextColorFgBaseY,
                           imageTable[i]);
    END;
    IF (NOT fgOnly) THEN
      FOR i := 4 TO 7 DO
        InitTextColorImage(imageTable[i], i-4);
        AddGadgetImageButton(GadTextColorBgBaseX + (GadTextColorWidth * (i-4)),
                             GadTextColorBgBaseY,
                             imageTable[i]);
      END;
    END;
  gads := EndGadgetList();
  IF (gads # NIL) THEN
    win := CreateWindow(WinTextColorX, WinTextColorY, 
                        WinTextColorWidth, WinTextColorHeight,
                        WinTextColorIDCMP, WinTextColorFlags,
                        gads, GetScreen(), ADR(WinTextColorTitle));
    IF (win # NIL) THEN
      DrawBorder(win^.RPort^, colorBorder, 0, 0);
      ShowString(win^, 20, GadTextColorFgBaseY + 1, ADR("Fg"));
      IF (NOT fgOnly) THEN
        ShowString(win^, 20, GadTextColorBgBaseY + 1, ADR("Bg"));
      END;
      PrintIText(win^.RPort^, sampleText, 0, 0);
      done := FALSE;
      LOOP
        IF (done) THEN EXIT; END;

        msg := WaitPort(win^.UserPort^);
        LOOP
          msg := GetMsg(win^.UserPort^);
          IF (msg = NIL) THEN EXIT; END;

          gad   := msg^.IAddress;
          gadId := gad^.GadgetID;
          ReplyMsg(msg);

          CASE (gadId) OF
          | 0:    (* OK Gadget Selected *)
                  ok   := TRUE;
                  done := TRUE;
                  EXIT; (* Wait loop *)
          | 1:    (* CANCEL Gadget Selected *)
                  done := TRUE;
                  EXIT; (* Wait loop *)
          | 2..5: (* Fg Color Selected *)
                  sampleText.FrontPen := BYTE(gadId-2);
                  PrintIText(win^.RPort^, sampleText, 0, 0);
          | 6..9: (* Bg Color Selected *)
                  sampleText.BackPen := BYTE(gadId-6);
                  PrintIText(win^.RPort^, sampleText, 0, 0);
          END; (* CASE *)
        END; (* LOOP *)
      END; (* LOOP *)
      CloseWindow(win^);
    END;
    FreeGadgetList(gads^);
  END;

  IF (ok) THEN
    WITH sampleText DO
      fg := FrontPen;
      bg := BackPen;
    END;
  END;

  SetSizeVerifyModeAllVTWindows(TRUE);

  RETURN (ok);
END DoTextFgBgColorRequester;

(* DoScreenColorRequester - display screen color requester *)
PROCEDURE DoScreenColorRequester(scr: ScreenPtr; VAR newColors: ARRAY OF CARDINAL);
VAR
  ok: BOOLEAN;
  gads: GadgetPtr;
  win: WindowPtr;
  done: BOOLEAN;
  gad: GadgetPtr;
  gadId: CARDINAL;
  msg: IntuiMessagePtr;
  msgClass: IDCMPFlagsSet;
  imageTable: ARRAY [0..3] OF Image;
  colorBorder: Border;
  XYList: ARRAY [0..4],[0..1] OF CARDINAL;
  i: CARDINAL;
  redGad: GadgetPtr;
  greenGad: GadgetPtr;
  blueGad: GadgetPtr;
  curColor: CARDINAL;

  (* SetupProgGadget - set prop position relative to 0..15 range *)
  PROCEDURE SetupPropGadget(VAR gad: Gadget; pos: CARDINAL);
  VAR
    propInfo: PropInfoPtr;
  BEGIN
    propInfo := gad.SpecialInfo;
    NewModifyProp(gad, win^, NIL, propInfo^.Flags, (pos * GadScrColorMult), 0, 
                  propInfo^.HorizBody, 0, 1);
  END SetupPropGadget;

  (* SelectNewColor - setup props according to new color selection *)
  PROCEDURE SelectNewColor(newColor: CARDINAL);
  VAR
    RGB: CARDINAL;
  BEGIN
    RGB := CARDINAL(GetRGB4(scr^.ViewPort.ColorMap^, newColor));
    SetupPropGadget(redGad^,   (RGB DIV 256) MOD 16);
    SetupPropGadget(greenGad^, (RGB DIV 16) MOD 16);
    SetupPropGadget(blueGad^,   RGB MOD 16);
    curColor := newColor;
  END SelectNewColor;

  (* UpdateColor - update screen colors according to prop positions *)
  PROCEDURE UpdateColor();
  VAR
    Rprop: PropInfoPtr;
    Gprop: PropInfoPtr;
    Bprop: PropInfoPtr;
  BEGIN
    Rprop := redGad^.SpecialInfo;
    Gprop := greenGad^.SpecialInfo;
    Bprop := blueGad^.SpecialInfo;
    SetRGB4(scr^.ViewPort, curColor,
            Rprop^.HorizPot DIV GadScrColorBody,
            Gprop^.HorizPot DIV GadScrColorBody,
            Bprop^.HorizPot DIV GadScrColorBody);
  END UpdateColor;

BEGIN (* DoScreenColorRequester *)
  SetSizeVerifyModeAllVTWindows(FALSE);

  ok := FALSE;
  BeginGadgetList();
    AddGadgetTextButton(GadScrColorOkX, GadScrColorOkY, ADR(OKString));
    AddGadgetTextButton(GadScrColorCancelX, GadScrColorCancelY, ADR(CANCELString));

    XYList[0][0] := 0;
    XYList[0][1] := 0;
    XYList[1][0] := (GadScrColorWidth * 4) + 1;
    XYList[1][1] := 0;
    XYList[2][0] := (GadScrColorWidth * 4) + 1;
    XYList[2][1] := GadScrColorHeight + 1;
    XYList[3][0] := 0;
    XYList[3][1] := GadScrColorHeight + 1;
    XYList[4][0] := 0;
    XYList[4][1] := 0;

    WITH colorBorder DO
      LeftEdge   := GadScrColorFgBaseX-1;
      TopEdge    := GadScrColorFgBaseY-1;
      FrontPen   := BYTE(1);
      BackPen    := BYTE(0);
      DrawMode   := Jam1;
      Count      := BYTE(5);
      XY         := ADR(XYList);
      NextBorder := NIL;
    END;

    FOR i := 0 TO 3 DO
      InitTextColorImage(imageTable[i], i);
      AddGadgetImageButton(GadScrColorFgBaseX + (GadScrColorWidth * i),
                           GadScrColorFgBaseY,
                           imageTable[i]);
    END;

    GlobalGadgetOpt(DefaultGadgetFlags, 
                    GadgetActivationSet{RelVerify, GadgImmediate, FollowMouse});
    AddGadgetProp(GadScrColorRedX, GadScrColorRedY,
                  GadScrColorRedWidth, GadScrColorRedHeight,
                  TRUE, FALSE, GadScrColorPot, 0, GadScrColorBody, 0);
    redGad := LastGadget;
    AddGadgetProp(GadScrColorGreenX, GadScrColorGreenY,
                  GadScrColorGreenWidth, GadScrColorGreenHeight,
                  TRUE, FALSE, GadScrColorPot, 0, GadScrColorBody, 0);
    greenGad := LastGadget;
    AddGadgetProp(GadScrColorBlueX, GadScrColorBlueY,
                  GadScrColorBlueWidth, GadScrColorBlueHeight,
                  TRUE, FALSE, GadScrColorPot, 0, GadScrColorBody, 0);
    blueGad := LastGadget;

  gads := EndGadgetList();
  IF (gads # NIL) THEN
    win := CreateWindow(WinScrColorX, WinScrColorY, 
                        WinScrColorWidth, WinScrColorHeight,
                        WinScrColorIDCMP, WinScrColorFlags,
                        gads, GetScreen(), ADR(WinScrColorTitle));
    IF (win # NIL) THEN
      DrawBorder(win^.RPort^, colorBorder, 0, 0);
      ShowString(win^, 15, GadScrColorRedY + 1, ADR("Red"));
      ShowString(win^, 15, GadScrColorGreenY + 1, ADR("Green"));
      ShowString(win^, 15, GadScrColorBlueY + 1, ADR("Blue"));
       
      SelectNewColor(0);

      done := FALSE;
      LOOP
        IF (done) THEN EXIT; END;

        msg := WaitPort(win^.UserPort^);
        LOOP
          msg := GetMsg(win^.UserPort^);
          IF (msg = NIL) THEN EXIT; END;

          msgClass := msg^.Class;
          gad      := msg^.IAddress;
          ReplyMsg(msg);

          IF (GadgetUp IN msgClass) THEN
            gadId := gad^.GadgetID;
            CASE (gadId) OF
            | 0:    (* OK Gadget Selected *)
                    ok   := TRUE;
                    done := TRUE;
                    EXIT; (* Wait loop *)
            | 1:    (* CANCEL Gadget Selected *)
                    done := TRUE;
                    EXIT; (* Wait loop *)
            | 2..5: (* Select Color To Be Changed *)
                    SelectNewColor(gadId - 2);
            | 6..8: (* Select Red, Green or Blue prop *)
                    UpdateColor();
            END; (* CASE *)
          ELSIF (MouseMove IN msgClass) THEN (* Prop Gadget Moved! *)
            UpdateColor();
          END;
        END; (* LOOP *)
      END; (* LOOP *)
      CloseWindow(win^);
    END;
    FreeGadgetList(gads^);
  END;

  IF (ok) THEN (* Save new colors *)
    newColors[0] := GetRGB4(scr^.ViewPort.ColorMap^, 0);
    newColors[1] := GetRGB4(scr^.ViewPort.ColorMap^, 1);
    newColors[2] := GetRGB4(scr^.ViewPort.ColorMap^, 2);
    newColors[3] := GetRGB4(scr^.ViewPort.ColorMap^, 3);
  ELSE (* Restore old colors *)
    LoadRGB4(scr^.ViewPort, ADR(newColors[0]), 4);
  END;

  SetSizeVerifyModeAllVTWindows(TRUE);
END DoScreenColorRequester;

(* DoStringRequester - display string/number input requester *)
PROCEDURE DoStringRequester(winTitle: ADDRESS; VAR id: InputData): BOOLEAN;
VAR
  gads: GadgetPtr;
  win: WindowPtr;
  done: BOOLEAN;
  ok: BOOLEAN;
  gad: GadgetPtr;
  gadId: CARDINAL;
  msg: IntuiMessagePtr;
  gadString: GadgetPtr;
  gadStringInfo: StringInfoPtr;
  errStr: ADDRESS;
  b: BOOLEAN;
BEGIN
  SetSizeVerifyModeAllVTWindows(FALSE);

  ok := FALSE;
  BeginGadgetList();
    AddGadgetTextButton(GadStringOkX, GadStringOkY, ADR(OKString));
    AddGadgetTextButton(GadStringCancelX, GadStringCancelY, ADR(CANCELString));
    AddGadgetString(GadStringStrX, GadStringStrY,
                    GadStringStrDispField, GadStringStrBufSize,
                    id.idInitialString);
    gadString := LastGadget;
    IF (gadString # NIL) THEN
      gadStringInfo := gadString^.SpecialInfo;
      WITH gadStringInfo^ DO
        BufferPos := strlen(Buffer); (* position cursor at end of string *)
      END;
    END;
  gads := EndGadgetList();
  IF (gads # NIL) THEN
    win := CreateWindow(WinStringX, WinStringY, 
                        WinStringWidth, WinStringHeight,
                        WinStringIDCMP, WinStringFlags,
                        gads, GetScreen(), winTitle);
    IF (win # NIL) THEN
      Delay(5D); (* Wait a bit for the window to be ready! *)
      b := ActivateGadget(gadString^, win^, NIL);
      done := FALSE;
      LOOP
        IF (done) THEN EXIT; END;

        msg := WaitPort(win^.UserPort^);
        LOOP
          msg := GetMsg(win^.UserPort^);
          IF (msg = NIL) THEN EXIT; END;

          gad   := msg^.IAddress;
          gadId := gad^.GadgetID;
          ReplyMsg(msg);

          (* Check if OK Gadget Hit or <RETURN> pressed in string gadget *)
          IF (gadId = 0) OR (gadId = 2) THEN
            IF (IsValidInputData(id, gadStringInfo^.Buffer)) THEN
              done := TRUE;
              ok   := TRUE;
              EXIT; (* Wait loop *)
            ELSE
              IF (id.idError = IEInvalid) THEN
                errStr := ADR(ErrorInvalidString);
              ELSIF (id.idError = IERange) THEN
                errStr := ADR(ErrorRangeString);
              END;
              ShowString(win^, ErrorStringX, ErrorStringY, errStr);
              (* Reactivate string gadget *)
              b := ActivateGadget(gadString^, win^, NIL);
            END;
          ELSIF (gadId = 1) THEN (* CANCEL Gadget Selected *)
            done := TRUE;
            EXIT; (* Wait Loop *)
          END;
        END; (* LOOP *)
      END; (* LOOP *)
      CloseWindow(win^);
    END;
    FreeGadgetList(gads^);
  END;

  SetSizeVerifyModeAllVTWindows(TRUE);

  RETURN (ok);
END DoStringRequester;

(* DoVerifyRequester - display an operation verification requester *)
PROCEDURE DoVerifyRequester(winTitle, line1, line2, line3, line4: ADDRESS): BOOLEAN;
VAR
  gads: GadgetPtr;
  win: WindowPtr;
  done: BOOLEAN;
  ok: BOOLEAN;
  gad: GadgetPtr;
  gadId: CARDINAL;
  msg: IntuiMessagePtr;
BEGIN
  SetSizeVerifyModeAllVTWindows(FALSE);

  ok := FALSE;
  BeginGadgetList();
    AddGadgetTextButton(GadStringOkX, GadStringOkY, ADR(OKString));
    AddGadgetTextButton(GadStringCancelX, GadStringCancelY, ADR(CANCELString));
  gads := EndGadgetList();
  IF (gads # NIL) THEN
    win := CreateWindow(WinStringX, WinStringY, 
                        WinStringWidth, WinStringHeight,
                        WinStringIDCMP, WinStringFlags,
                        gads, GetScreen(), winTitle);
    IF (win # NIL) THEN
      ShowString(win^, TextVerifyX, TextVerifyY,    line1);
      ShowString(win^, TextVerifyX, TextVerifyY+10, line2);
      ShowString(win^, TextVerifyX, TextVerifyY+20, line3);
      ShowString(win^, TextVerifyX, TextVerifyY+30, line4);
      done := FALSE;
      LOOP
        IF (done) THEN EXIT; END;

        msg := WaitPort(win^.UserPort^);
        LOOP
          msg := GetMsg(win^.UserPort^);
          IF (msg = NIL) THEN EXIT; END;

          gad   := msg^.IAddress;
          gadId := gad^.GadgetID;
          ReplyMsg(msg);

          IF (gadId = 0) THEN (* OK Gadget Selected *)
            ok   := TRUE;
            done := TRUE;
            EXIT; (* Wait loop *)
          ELSIF (gadId = 1) THEN (* CANCEL Gadget Selected *)
            done := TRUE;
            EXIT; (* Wait loop *)
          END;
        END; (* LOOP *)
      END; (* LOOP *)
      CloseWindow(win^);
    END;
    FreeGadgetList(gads^);
  END;

  SetSizeVerifyModeAllVTWindows(TRUE);

  RETURN (ok);
END DoVerifyRequester;

(* ===== PUBLIC ===== *)

(* RequestTextFgBgColors - request user to specify fg, bg color combination *)
PROCEDURE RequestTextFgBgColors(VAR fg, bg: BYTE): BOOLEAN;
BEGIN
  RETURN (DoTextFgBgColorRequester(fg, bg, FALSE));
END RequestTextFgBgColors;

(* RequestTextFgColor - Request user to specify fg color only *)
PROCEDURE RequestTextFgColor(VAR fg: BYTE; bg: BYTE): BOOLEAN;
BEGIN
  RETURN (DoTextFgBgColorRequester(fg, bg, TRUE));
END RequestTextFgColor;

(* RequestTextWidth - request user to specify text width [1..255] *)
PROCEDURE RequestTextWidth(VAR textWidth: CARDINAL): BOOLEAN;
VAR
  ok: BOOLEAN;
  value: CARDINAL;
  r: CARDINAL;
  id: InputData;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..31] OF CHAR;
BEGIN
  argo[0].W := textWidth;
  r := sprintf(ADR(str[0]), ADR("%u"), argo);
  WITH id DO
    idInitialString := ADR(str[0]);
    idFormat        := IFNumber;
    idNumberSize    := NSByte;
  END;
  ok := DoStringRequester(ADR(ReqTextWidthTitle), id);
  IF (ok) THEN
    value := CARDINAL(BYTE(id.idNumber));
    ok    := (value # textWidth);
    IF (ok) THEN
      IF (value = 0) THEN
        value := 1;
      END;
      textWidth := value;
    END;
  END;
  RETURN (ok);
END RequestTextWidth;

(* RequestValueWidth - request user to specify value field width. [1..255] *)
PROCEDURE RequestValueWidth(VAR valueWidth: CARDINAL): BOOLEAN;
VAR
  ok: BOOLEAN;
  value: CARDINAL;
  r: CARDINAL;
  id: InputData;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..31] OF CHAR;
BEGIN
  argo[0].W := valueWidth;
  r := sprintf(ADR(str[0]), ADR("%u"), argo);
  WITH id DO
    idInitialString := ADR(str[0]);
    idFormat        := IFNumber;
    idNumberSize    := NSByte;
  END;
  ok := DoStringRequester(ADR(ReqValueWidthTitle), id);
  IF (ok) THEN
    value := CARDINAL(BYTE(id.idNumber));
    ok    := (value # valueWidth);
    IF (ok) THEN
      IF (value = 0) THEN
        value := 1;
      END;
      valueWidth := value;
    END;
  END;
  RETURN (ok);
END RequestValueWidth;

(* RequestContextLines - Request user to specify context lines [1..255] *)
PROCEDURE RequestContextLines(VAR contextLines: CARDINAL): BOOLEAN;
VAR
  ok: BOOLEAN;
  value: CARDINAL;
  r: CARDINAL;
  id: InputData;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..31] OF CHAR;
BEGIN
  argo[0].W := contextLines;
  r := sprintf(ADR(str[0]), ADR("%u"), argo);
  WITH id DO
    idInitialString := ADR(str[0]);
    idFormat        := IFNumber;
    idNumberSize    := NSByte;
  END;
  ok := DoStringRequester(ADR(ReqContextLinesTitle), id);
  IF (ok) THEN
    value := CARDINAL(BYTE(id.idNumber));
    ok    := (value # contextLines);
    IF (ok) THEN
      IF (value = 0) THEN
        value := 1;
      END;
      contextLines := value;
    END;
  END;
  RETURN (ok);
END RequestContextLines;

(* RequestBreakCount - request user to specify break count [1..$FFFFFFFF] *)
PROCEDURE RequestBreakCount(VAR breakCount: LONGCARD): BOOLEAN;
VAR
  ok: BOOLEAN;
  value: LONGCARD;
  r: CARDINAL;
  id: InputData;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..31] OF CHAR;
BEGIN
  argo[0].L := breakCount;
  r := sprintf(ADR(str[0]), ADR("%lu"), argo);
  WITH id DO
    idInitialString := ADR(str[0]);
    idFormat        := IFNumber;
    idNumberSize    := NSLong;
  END;
  ok := DoStringRequester(ADR(ReqBreakCountTitle), id);
  IF (ok) THEN
    value := id.idNumber;
    IF (value = 0D) THEN
      value := 1D;
    END;
    breakCount := value;
  END;
  RETURN (ok);
END RequestBreakCount;

(* RequestMaxModFilesLoaded - Request user to specify max .MODs [0..65535] *)
PROCEDURE RequestMaxModFilesLoaded(VAR maxFiles: CARDINAL);
VAR
  ok: BOOLEAN;
  value: CARDINAL;
  r: CARDINAL;
  id: InputData;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..31] OF CHAR;
BEGIN
  argo[0].W := maxFiles;
  r := sprintf(ADR(str[0]), ADR("%u"), argo);
  WITH id DO
    idInitialString := ADR(str[0]);
    idFormat        := IFNumber;
    idNumberSize    := NSWord;
  END;
  IF (DoStringRequester(ADR(ReqMaxModFilesTitle), id)) THEN
    maxFiles := CARDINAL(id.idNumber);
  END;
END RequestMaxModFilesLoaded;

(* RequestProcessPriority - Request user to specify priority [-128..127] *)
PROCEDURE RequestProcessPriority(VAR priority: INTEGER);
VAR
  ok: BOOLEAN;
  value: INTEGER;
  r: CARDINAL;
  id: InputData;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..31] OF CHAR;
BEGIN
  argo[0].W := priority;
  r := sprintf(ADR(str[0]), ADR("%d"), argo);
  WITH id DO
    idInitialString := ADR(str[0]);
    idFormat        := IFNumber;
    idNumberSize    := NSByte;
  END;
  IF (DoStringRequester(ADR(ReqPriorityTitle), id)) THEN
    value := INTEGER(id.idNumber);
    IF (value > 127) THEN
      DEC(value, 256);
    END;
    priority := value;
  END;
END RequestProcessPriority;

(* RequestProcessStackSize - Request user to specify stack [4000..$FFFFFFFF] *)
PROCEDURE RequestProcessStackSize(VAR stackSize: LONGCARD);
VAR
  ok: BOOLEAN;
  value: LONGCARD;
  r: CARDINAL;
  id: InputData;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..31] OF CHAR;
BEGIN
  argo[0].L := stackSize;
  r := sprintf(ADR(str[0]), ADR("%lu"), argo);
  WITH id DO
    idInitialString := ADR(str[0]);
    idFormat        := IFNumber;
    idNumberSize    := NSLong;
  END;
  IF (DoStringRequester(ADR(ReqStackSizeTitle), id)) THEN
    value := (id.idNumber DIV 4D) * 4D;
    IF (value < 4000D) THEN
      value := 4000D;
    END;
    stackSize := value;
  END;
END RequestProcessStackSize;

(* RequestProcessCurrentDir - Request user to specify current dir string *)
PROCEDURE RequestProcessCurrentDir(oldDirStr, newDirStr: ADDRESS): BOOLEAN;
VAR
  id: InputData;
BEGIN
  WITH id DO
    idInitialString := oldDirStr;
    idFormat        := IFString;
    idString        := newDirStr;
  END;
  RETURN (DoStringRequester(ADR(ReqCurrentDirTitle), id));
END RequestProcessCurrentDir;

(* RequestSymbolFileDirList - Request user to specify symbol dir list string *)
PROCEDURE RequestSymbolFileDirList(oldDirStr, newDirStr: ADDRESS): BOOLEAN;
VAR
  id: InputData;
BEGIN
  WITH id DO
    idInitialString := oldDirStr;
    idFormat        := IFString;
    idString        := newDirStr;
  END;
  RETURN (DoStringRequester(ADR(ReqSymbolDirListTitle), id));
END RequestSymbolFileDirList;

(* RequestSourceFileDirList - Request user to specify source dir list string *)
PROCEDURE RequestSourceFileDirList(oldDirStr, newDirStr: ADDRESS): BOOLEAN;
VAR
  id: InputData;
BEGIN
  WITH id DO
    idInitialString := oldDirStr;
    idFormat        := IFString;
    idString        := newDirStr;
  END;
  RETURN (DoStringRequester(ADR(ReqSourceDirListTitle), id));
END RequestSourceFileDirList;

(* RequestFileName - Request user to specify file name string *)
PROCEDURE RequestFileName(fileName: ADDRESS): BOOLEAN;
VAR
  id: InputData;
BEGIN
  WITH id DO
    idInitialString := fileName;
    idFormat        := IFString;
    idString        := fileName;
  END;
  RETURN (DoStringRequester(ADR(ReqFileNameTitle), id));
END RequestFileName;

(* RequestProgParms - Request user to specify program parameters string *)
PROCEDURE RequestProgParms(progParms: ADDRESS): BOOLEAN;
VAR
  id:  InputData;
BEGIN
  WITH id DO
    idInitialString := NIL;
    idFormat        := IFString;
    idString        := progParms;
  END;
  RETURN (DoStringRequester(ADR(ReqProgParmsTitle), id));
END RequestProgParms;

(* RequestSearchString - Request user to specify search string *)
PROCEDURE RequestSearchString(searchStr: ADDRESS): BOOLEAN;
VAR
  id: InputData;
BEGIN
  WITH id DO
    idInitialString := NIL;
    idFormat        := IFString;
    idString        := searchStr;
  END;
  RETURN (DoStringRequester(ADR(ReqSearchStringTitle), id));
END RequestSearchString;

(* RequestChangeRealValue - Request user to specify REAL value *)
PROCEDURE RequestChangeRealValue(oldReal: REAL; VAR newReal: REAL): BOOLEAN;
VAR
  id: InputData;
  ok: BOOLEAN;
  value: REAL;
  r: CARDINAL;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..31] OF CHAR;
BEGIN
  argo[0].L := oldReal;
  r := sprintf(ADR(str[0]), ADR("%g"), argo);
  WITH id DO
    idInitialString := ADR(str[0]);
    idFormat        := IFReal;
  END;
  ok := DoStringRequester(ADR(ReqRealValueTitle), id);
  IF (ok) THEN
    newReal := id.idReal;
  END;
  RETURN (ok);
END RequestChangeRealValue;

(* RequestChangeStringValue - Request user to specify STRING value *)
PROCEDURE RequestChangeStringValue(oldStr: ADDRESS; newStr: ADDRESS): BOOLEAN;
VAR
  id: InputData;
BEGIN
  WITH id DO
    idInitialString := oldStr;
    idFormat        := IFString;
    idString        := newStr;
  END;
  RETURN (DoStringRequester(ADR(ReqStringValueTitle), id));
END RequestChangeStringValue;

(* RequestChangeNumberValue - Request user to specify binary number *)
PROCEDURE RequestChangeNumberValue(oldNumber: LONGCARD; numberSize: CARDINAL;
                                   numberFormat: StorageNumber;
                                   VAR newNumber: LONGCARD): BOOLEAN;
VAR
  id: InputData;
  ok: BOOLEAN;
  r: CARDINAL;
  title: ADDRESS;
  fmtStr: ADDRESS;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..31] OF CHAR;
BEGIN
  CASE (numberFormat) OF
  | SNBin:  fmtStr := ADR("%%%lb");
  | SNOct:  fmtStr := ADR("@%lo");
  | SNHex:  fmtStr := ADR("$%lx");
  | SNDecS: fmtStr := ADR("%ld");
            IF (numberSize = 1) AND (oldNumber > 127D) THEN
              INC(oldNumber, 0FFFFFF00H);
            ELSIF (numberSize = 2) AND (oldNumber > 32767D) THEN
              INC(oldNumber, 0FFFF0000H);
            END;
  | SNDecU: fmtStr := ADR("%lu");
  | SNChar: IF (CHAR(oldNumber) >= " ") AND (CHAR(oldNumber) <= "~") THEN
              fmtStr := ADR("'%c'");
            ELSE
              fmtStr := ADR("%lu");
            END;
  END;

  argo[0].L := oldNumber;
  r := sprintf(ADR(str[0]), fmtStr, argo);

  WITH id DO
    idInitialString := ADR(str[0]);
    idFormat        := IFNumber;

    IF (numberSize = 1) THEN
      idNumberSize := NSByte;
      title := ADR(ReqNumber8ValueTitle);
    ELSIF (numberSize = 2) THEN
      idNumberSize := NSWord;
      title := ADR(ReqNumber16ValueTitle);
    ELSIF (numberSize = 4) THEN
      idNumberSize := NSLong;
      title := ADR(ReqNumber32ValueTitle);
    END;
  END;
  ok := DoStringRequester(title, id);
  IF (ok) THEN
    newNumber := id.idNumber;
  END;
  RETURN (ok);
END RequestChangeNumberValue;

(* RequestChangeRegister - Request user to specify register value *)
PROCEDURE RequestChangeRegister(oldNumber: LONGCARD;
                                VAR newNumber: LONGCARD): BOOLEAN;
VAR
  ok: BOOLEAN;
  r: CARDINAL;
  id: InputData;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..31] OF CHAR;
BEGIN
  argo[0].L := oldNumber;
  r := sprintf(ADR(str[0]), ADR("$%lx"), argo);
  WITH id DO
    idInitialString := ADR(str[0]);
    idFormat        := IFNumber;
    idNumberSize    := NSLong;
  END;
  ok := DoStringRequester(ADR(ReqRegisterTitle), id);
  IF (ok) THEN
    newNumber := id.idNumber;
  END;
  RETURN (ok);
END RequestChangeRegister;

(* RequestAddress - Request user to specify address *)
PROCEDURE RequestAddress(oldAdr: ADDRESS; VAR newAdr: ADDRESS): BOOLEAN;
VAR
  ok: BOOLEAN;
  value: ADDRESS;
  r: CARDINAL;
  id: InputData;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..31] OF CHAR;
BEGIN
  argo[0].L := oldAdr;
  r := sprintf(ADR(str[0]), ADR("$%lx"), argo);
  WITH id DO
    idInitialString := ADR(str[0]);
    idFormat        := IFNumber;
    idNumberSize    := NSLong;
  END;
  ok := DoStringRequester(ADR(ReqAddressTitle), id);
  IF (ok) THEN
    newAdr := id.idNumber;
  END;
  RETURN (ok);
END RequestAddress;

(* RequestCopyMemory - Request user to verify copy memory operation *)
PROCEDURE RequestCopyMemory(srcAdr, dstAdr, endAdr: ADDRESS): BOOLEAN;
VAR
  r: CARDINAL;
  argo: ARRAY [0..2] OF FormatArg;
  line1: ARRAY [0..39] OF CHAR;
  line2: ARRAY [0..39] OF CHAR;
  line3: ARRAY [0..39] OF CHAR;
  line4: ARRAY [0..39] OF CHAR;
BEGIN
  argo[0].L := srcAdr;
  r := sprintf(ADR(line1[0]), ADR("Source      = $%08lx"), argo);
  argo[0].L := endAdr;
  r := sprintf(ADR(line2[0]), ADR("End         = $%08lx"), argo);
  argo[0].L := dstAdr;
  r := sprintf(ADR(line3[0]), ADR("Destination = $%08lx"), argo);
  argo[0].L := endAdr - srcAdr;
  r := sprintf(ADR(line4[0]), ADR("Size        = $%08lx"), argo);
  RETURN (DoVerifyRequester(ADR(ReqCopyMemoryTitle), ADR(line1[0]),
                            ADR(line2[0]), ADR(line3[0]), ADR(line4[0])));
END RequestCopyMemory;

(* RequestFillMemory - Request user to verify fill memory operation *)
PROCEDURE RequestFillMemory(dstAdr, endAdr: ADDRESS; size: CARDINAL;
                            VAR fillValue: LONGCARD): BOOLEAN;
VAR
  ok: BOOLEAN;
  newValue: LONGCARD;
  r: CARDINAL;
  argo: ARRAY [0..2] OF FormatArg;
  line1: ARRAY [0..39] OF CHAR;
  line2: ARRAY [0..39] OF CHAR;
  line3: ARRAY [0..39] OF CHAR;
  line4: ARRAY [0..39] OF CHAR;
BEGIN
  ok := RequestChangeNumberValue(0D, size, SNHex, newValue);
  IF (ok) THEN
    argo[0].L := dstAdr;
    r := sprintf(ADR(line1[0]), ADR("Destination = $%08lx"), argo);
    argo[0].L := endAdr;
    r := sprintf(ADR(line2[0]), ADR("End         = $%08lx"), argo);
    argo[0].L := endAdr - dstAdr;
    r := sprintf(ADR(line3[0]), ADR("Size        = $%08lx"), argo);
    argo[0].L := newValue;
    r := sprintf(ADR(line4[0]), ADR("Value       = $%08lx"), argo);
    ok := DoVerifyRequester(ADR(ReqFillMemoryTitle), ADR(line1[0]),
                            ADR(line2[0]), ADR(line3[0]), ADR(line4[0]));
    IF (ok) THEN
      fillValue := newValue;
    END;
  END;
  RETURN (ok);
END RequestFillMemory;

(* RequestSearchMemory - Request user to specify search memory operation *)
PROCEDURE RequestSearchMemory(srcAdr, endAdr: ADDRESS; size: CARDINAL;
                              VAR searchValue: LONGCARD): BOOLEAN;
VAR
  ok: BOOLEAN;
  newValue: LONGCARD;
  r: CARDINAL;
  argo: ARRAY [0..2] OF FormatArg;
  line1: ARRAY [0..39] OF CHAR;
  line2: ARRAY [0..39] OF CHAR;
  line3: ARRAY [0..39] OF CHAR;
  line4: ARRAY [0..39] OF CHAR;
BEGIN
  ok := RequestChangeNumberValue(0D, size, SNHex, newValue);
  IF (ok) THEN
    argo[0].L := srcAdr;
    r := sprintf(ADR(line1[0]), ADR("Source      = $%08lx"), argo);
    argo[0].L := endAdr;
    r := sprintf(ADR(line2[0]), ADR("End         = $%08lx"), argo);
    argo[0].L := endAdr - srcAdr;
    r := sprintf(ADR(line3[0]), ADR("Size        = $%08lx"), argo);
    argo[0].L := newValue;
    r := sprintf(ADR(line4[0]), ADR("Value       = $%08lx"), argo);
    ok := DoVerifyRequester(ADR(ReqSearchMemoryTitle), ADR(line1[0]),
                            ADR(line2[0]), ADR(line3[0]), ADR(line4[0]));
    IF (ok) THEN
      searchValue := newValue;
    END;
  END;
  RETURN (ok);
END RequestSearchMemory;

(* RequestContinueSearch - Request user to verify a continue search operation *)
PROCEDURE RequestContinueSearch(foundAdr: ADDRESS; found: BOOLEAN;
                                searchValue: LONGCARD): BOOLEAN;
VAR
  r: CARDINAL;
  argo: ARRAY [0..2] OF FormatArg;
  line2: ARRAY [0..39] OF CHAR;
  line3: ARRAY [0..39] OF CHAR;
  line3Adr: ADDRESS;
BEGIN
  argo[0].L := searchValue;
  r := sprintf(ADR(line2[0]), ADR("Value       = $%08lx"), argo);

  IF (found) THEN
    argo[0].L := foundAdr;
    r := sprintf(ADR(line3[0]), ADR("Found @ Adr = $%08lx"), argo);
    line3Adr  := ADR(line3[0]);
  ELSE
    line3Adr := ADR(" Memory Search Failed!");              
  END;
  RETURN (DoVerifyRequester(ADR(ReqContinueSearchTitle), NIL, ADR(line2[0]),
                            line3Adr, NIL));
END RequestContinueSearch;

(*  RequestScreenColors - Request user to change screen colors *)
PROCEDURE RequestScreenColors(scr: ADDRESS; VAR colors: ARRAY OF CARDINAL);
BEGIN
  DoScreenColorRequester(scr, colors);
END RequestScreenColors;

(* InitDDBRequesters - initialize DBRequesters module *)
PROCEDURE InitDBRequesters(): BOOLEAN;
BEGIN
  RETURN (TRUE);
END InitDBRequesters;

(* CleanupDBRequesters - cleanup DBRequesters module *)
PROCEDURE CleanupDBRequesters();
END CleanupDBRequesters;

END DBRequesters.
