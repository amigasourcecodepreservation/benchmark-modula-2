(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1993 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBEnvironmentInquiry.MOD          Version: Amiga.01.10             *
 * Created: 01/08/94   Updated:            Author: Tom Breeden              *
 * Description: Debugger Amiga and hardware runtime environment inquiry.    *
 ****************************************************************************)

IMPLEMENTATION MODULE DBEnvironmentInquiry;

FROM ExecBase IMPORT
  AF68020, (*AF68030, AF68040,*) ExecBasePtr;   (* IMPORT after Simple... recompiled *)
FROM System IMPORT
  ExecBase;

CONST AF68030   = 2;     (* also set for 68040 *)
      AF68040   = 3;

(*===========================================*)
 PROCEDURE InitDBEnvironmentInquiry():BOOLEAN;
(*===========================================*)

VAR  exec: ExecBasePtr;

BEGIN

exec := ExecBase;
IF (AF68040 IN exec^.AttnFlags) THEN
   WhichCPU := M68040;
ELSIF (AF68030 IN exec^.AttnFlags) THEN
   WhichCPU := M68030;
ELSIF (AF68020 IN exec^.AttnFlags) THEN
   WhichCPU := M68020;
ELSE
   WhichCPU := M68000;
END;

OSVersion2 := exec^.LibNode.libVersion >= 37;   (* NOT 36, too many bugs *)

RETURN TRUE;

END InitDBEnvironmentInquiry;

(*======================================*)
 PROCEDURE CleanupDBEnvironmentInquiry();
(*======================================*)

BEGIN
END CleanupDBEnvironmentInquiry;

END DBEnvironmentInquiry.
