(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBStatusWindow.MOD                Version: Amiga.01.10             *
 * Created: 01/09/89   Updated: 03/23/89   Author: Leon Frenkel             *
 * Description: Debugger status window module.                              *
 ****************************************************************************)

IMPLEMENTATION MODULE DBStatusWindow;

FROM SYSTEM IMPORT
  ADDRESS, BYTE,
  ADR;
FROM CMemOp IMPORT
  movmem, setmem;
FROM CPrintBuffer IMPORT
  sprintf;
FROM DBConfiguration IMPORT
  ConfigScreenWidth, ConfigScreenHeight,
  ConfigStatusWindowX, ConfigStatusWindowY, 
  ConfigStatusWindowDetailPen, ConfigStatusWindowBlockPen,
  ConfigStatusWindowFgColor, ConfigStatusWindowBgColor;
FROM DBMenusKbd IMPORT
  InvalidWindowIdx,
  LinkGlobalMenuStrip, DoGlobalMenuCommand, DoGlobalKbdCommand;
FROM DBMisc IMPORT
  AllocString, DeallocString, DeadKeyConvert;
FROM DBRequesters IMPORT
  RequestTextFgBgColors;
FROM DBWindows IMPORT
  WindowClass,
  GetScreen;
FROM Drawing IMPORT
  SetAPen, SetBPen, SetDrMd, RectFill, Move;
FROM FormatString IMPORT
  FormatArg;
FROM Intuition IMPORT
  NewWindow, WindowPtr, ScreenPtr, MenuPtr, CustomScreen, WBenchScreen,
  IntuiMessagePtr, MenuItemPtr, MenuNull,
  IDCMPFlags, IDCMPFlagsSet, WindowFlags, WindowFlagsSet,
  OpenWindow, CloseWindow, WindowToFront, SetMenuStrip, ClearMenuStrip,
  MENUNUM, ITEMNUM, SUBNUM, ItemAddress, MoveWindow, RefreshWindowFrame,
  DisplayBeep;
FROM Ports IMPORT
  MsgPortPtr,
  ReplyMsg;
FROM Rasters IMPORT
  RastPortPtr, Jam2;
FROM SimpleMenus IMPORT
  BeginMenuStrip, EndMenuStrip, FreeMenuStrip, AddMenu, AddMenuItem;
FROM Text IMPORT
  Text;

CONST
  LocalMenus = 1; (* Number of window specific menus *)

  (* Status window parameters *)
  SWindowWIDTH  = 320;
  SWindowHEIGHT = 37;
  SWindowIDCMP  = IDCMPFlagsSet{MenuPick, RawKey};
  SWindowFLAGS  = WindowFlagsSet{WindowDrag, WindowDepth, Activate};
  SWindowTITLE  = "Status";

  (* Status window area inside borders *)
  InsideXMIN = 2;
  InsideYMIN = 10;
  InsideXMAX = SWindowWIDTH - 3;
  InsideYMAX = SWindowHEIGHT - 2;

  (* Font character size parameters *)
  CharWidth  = 8;
  CharHeight = 8;

  StatusTextX = 4;  (* Status message x position *)
  StatusTextY = 17; (* Status message y position *)
  ErrorTextX  = 4;  (* Error message x position *)
  ErrorTextY  = 25; (* Error message y position *)

  StatusTextFirstChar = 2;  (* First char index for status message *)
  ErrorTextFirstChar  = 7;  (* First char index for error message *)
  StatusTextMaxChars  = 37; (* Maximum number of chars in status message *)
  ErrorTextMaxChars   = 71; (* Maximum number of chars in error message *)

  ErrorTextFirstLineLength  = 32; (* Maximum number of chars in first line *)
  ErrorTextSecondLineLength = 39; (* Maximum number of chars in second line *)

  MaxFormatBufferLength = 256; (* Size of formated output buffer *)


VAR
  StatusMenuStrip     : MenuPtr;     (* Status window menu strip *)
  SWindow             : WindowPtr;   (* Status window ptr *)
  SWindowRP           : RastPortPtr; (* Status window raster port *)
  ShowingError        : BOOLEAN;     (* T: showing error msg, F: Not *)
  ShowingStatusString : ADDRESS;     (* Current status message string *)

(* ===== PRIVATE ===== *)

(* AdjustStatusWindowPos - make status window fit in screen boundaries *)
PROCEDURE AdjustStatusWindowPos();
BEGIN
  IF (ConfigStatusWindowX + SWindowWIDTH > ConfigScreenWidth) THEN
    ConfigStatusWindowX := 0;
  END;
  IF (ConfigStatusWindowY + SWindowHEIGHT > ConfigScreenHeight) THEN
    ConfigStatusWindowY := 0;
  END;
END AdjustStatusWindowPos;

(* SetupStatusWindow - fill status window background and draw text *)
PROCEDURE SetupStatusWindow();
VAR
  textFgColor: CARDINAL;
  textBgColor: CARDINAL;
BEGIN
  WITH SWindow^ DO
    DetailPen := ConfigStatusWindowDetailPen;
    BlockPen  := ConfigStatusWindowBlockPen;
  END;
  RefreshWindowFrame(SWindow^);

  textFgColor := CARDINAL(ConfigStatusWindowFgColor);
  textBgColor := CARDINAL(ConfigStatusWindowBgColor);
  SetAPen(SWindowRP^, textBgColor);
  RectFill(SWindowRP^, InsideXMIN, InsideYMIN, InsideXMAX, InsideYMAX);

  SetAPen(SWindowRP^, textFgColor);
  SetBPen(SWindowRP^, textBgColor);
  SetDrMd(SWindowRP^, Jam2);

  Move(SWindowRP^, StatusTextX, StatusTextY);
  Text(SWindowRP^, ADR(">"), 1);

  Move(SWindowRP^, ErrorTextX, ErrorTextY);
  Text(SWindowRP^, ADR("ERROR:"), 6);

  ShowStatus(ShowingStatusString);
END SetupStatusWindow;

(* FindWordLength - determine length of current word *)
PROCEDURE FindWordLength(VAR str: ARRAY OF CHAR; idx: CARDINAL): CARDINAL;
VAR
  length: CARDINAL;
BEGIN
  length := 0;
  WHILE (str[idx] # 0C) AND (str[idx] # " ") DO
    INC(length);
    INC(idx);
  END;
  RETURN (length);
END FindWordLength;

(* ===== PUBLIC ===== *)

(* OpenStatusWindow - read configuration and open status window *)
PROCEDURE OpenStatusWindow(): BOOLEAN;
VAR
  nw: NewWindow;
  scr: ScreenPtr;
BEGIN
  scr := GetScreen();

  (* Make sure window is inside screen boundaries *)
  AdjustStatusWindowPos();

  WITH nw DO
    LeftEdge    := ConfigStatusWindowX;
    TopEdge     := ConfigStatusWindowY;
    Width       := SWindowWIDTH;
    Height      := SWindowHEIGHT;
    DetailPen   := ConfigStatusWindowDetailPen;
    BlockPen    := ConfigStatusWindowBlockPen;
    IDCMPFlags  := SWindowIDCMP;
    Flags       := SWindowFLAGS;
    FirstGadget := NIL;
    CheckMark   := NIL;
    Title       := ADR(SWindowTITLE);
    Screen      := scr;
    MinWidth    := 0;
    MinHeight   := 0;
    MaxWidth    := 0;
    MaxHeight   := 0;
    IF (scr # NIL) THEN
      Type := CustomScreen;
    ELSE
      Type := WBenchScreen;
    END;
  END; (* WITH *)
  SWindow := OpenWindow(nw);
  IF (SWindow # NIL) THEN
    SetMenuStrip(SWindow^, StatusMenuStrip^);

    SWindowRP   := SWindow^.RPort;
    SetupStatusWindow();
  END;
  RETURN (SWindow # NIL);
END OpenStatusWindow;

(* ConfigStatusWindow - reposition status window based on conifg and redraw *)
PROCEDURE ConfigStatusWindow();
VAR
  dx: INTEGER;
  dy: INTEGER;
BEGIN
  AdjustStatusWindowPos();

  WITH SWindow^ DO
    dx := INTEGER(ConfigStatusWindowX) - LeftEdge;
    dy := INTEGER(ConfigStatusWindowY) - TopEdge;
  END;
  MoveWindow(SWindow^, dx, dy);

  SetupStatusWindow();
END ConfigStatusWindow;

(* GetStatusWindowInfo - save current status window pos into config variables *)
PROCEDURE GetStatusWindowInfo();
BEGIN
  WITH SWindow^ DO
    ConfigStatusWindowX := LeftEdge;
    ConfigStatusWindowY := TopEdge;
  END;
END GetStatusWindowInfo;

(* ShowStatus - display current debugger status *)
PROCEDURE ShowStatus(fmtStr: ADDRESS);
VAR
  buf: ARRAY [0..StatusTextMaxChars-1] OF CHAR;
  fmtBuf: ARRAY [0..MaxFormatBufferLength-1] OF CHAR;
  len: CARDINAL;
  str: ADDRESS;
BEGIN
  IF (fmtStr # NIL) THEN
    str := ADR(fmtBuf[0]);
    len := sprintf(ADR(fmtBuf[0]), fmtStr, ShowArgs);
  ELSE (* Clear status message *)
    str := ADR("");
    len := 0;
  END;

  IF (ShowingStatusString # NIL) THEN (* Free current status msg string *)
    DeallocString(ShowingStatusString);
  END;
  ShowingStatusString := AllocString(ADR(fmtBuf[0]));

  IF (len < StatusTextMaxChars) THEN (* Fill in " " to end of line *)
    setmem(ADR(buf[len]), StatusTextMaxChars - len, " ");
  ELSE
    len := StatusTextMaxChars;
  END;
  IF (len > 0) THEN (* Copy formated string to start of output buffer *)
    movmem(str, ADR(buf[0]), len);
  END;

  (* Display status message *)
  Move(SWindowRP^, StatusTextX + (StatusTextFirstChar*CharWidth), StatusTextY);
  Text(SWindowRP^, ADR(buf[0]), StatusTextMaxChars);
END ShowStatus;

(* ShowError - display error message, move window to front, flash screen *)
PROCEDURE ShowError(fmtStr: ADDRESS);
VAR
  buf: ARRAY [0..ErrorTextMaxChars-1] OF CHAR;
  fmtBuf: ARRAY [0..MaxFormatBufferLength-1] OF CHAR;
  len: CARDINAL;
  str: ADDRESS;
  srcIdx: CARDINAL;
  dstIdx: CARDINAL;
  wordLength: CARDINAL;
BEGIN
  IF (fmtStr # NIL) THEN
    len := sprintf(ADR(fmtBuf[0]), fmtStr, ShowArgs);
    ShowingError := TRUE;
  ELSE (* Clear error message *)
    IF (NOT ShowingError) THEN (* Already clear so do nothing *) 
      RETURN;
    END;
    fmtBuf[0] := 0C;
    len       := 0;
    ShowingError := FALSE;
  END;

  IF (len # 0) THEN
    WindowToFront(SWindow^); (* Pop window to front *)
    DisplayBeep(NIL);        (* Flash screen to alert user *)
  END;

  (* Fill output buffer with " " character *)
  setmem(ADR(buf[0]), ErrorTextMaxChars, " ");

  srcIdx     := 0;
  dstIdx     := 0;
  wordLength := 0;

  LOOP
    (* Exit if end of output buffer *)
    IF (dstIdx >= ErrorTextMaxChars) THEN EXIT; END;

    (* Exit if end of source buffer *)
    IF (srcIdx >= len) THEN EXIT; END;

    IF (wordLength # 0) THEN
      buf[dstIdx] := fmtBuf[srcIdx];
      INC(dstIdx);
      INC(srcIdx);
      DEC(wordLength);
    ELSE (* wordLength = 0 *)
      IF (fmtBuf[srcIdx] # " ") THEN
        wordLength := FindWordLength(fmtBuf, srcIdx);
        IF (dstIdx < ErrorTextFirstLineLength) AND
           (dstIdx + wordLength >= ErrorTextFirstLineLength) THEN
          dstIdx := ErrorTextFirstLineLength;
        END;
      ELSE (* fmtBuf[srcIdx] = " " *)
        INC(srcIdx);
        INC(dstIdx);
      END;
    END;
  END; (* LOOP *)

  (* Display first line of error message *)
  Move(SWindowRP^, ErrorTextX + (ErrorTextFirstChar*CharWidth), ErrorTextY);
  Text(SWindowRP^, ADR(buf[0]), ErrorTextFirstLineLength);

  (* Display second line of error message *)
  Move(SWindowRP^, ErrorTextX, ErrorTextY + 8);
  Text(SWindowRP^,ADR(buf[ErrorTextFirstLineLength]),ErrorTextSecondLineLength);
END ShowError;

(* GetStatusWindowMsgPort - get ptr to status window message port *)
PROCEDURE GetStatusWindowMsgPort(): MsgPortPtr;
BEGIN
  RETURN (SWindow^.UserPort);
END GetStatusWindowMsgPort;

(* HandleMsgStatusWindow - handle IDCMP message for status window *)
PROCEDURE HandleMsgStatusWindow(msg: ADDRESS);
VAR
  imsg: IntuiMessagePtr;
  MsgClass: IDCMPFlagsSet;
  MsgCode: CARDINAL;
  menuCode: CARDINAL;
  menuItemPtr: MenuItemPtr;
  keyLength: CARDINAL;
  keyBuf: ARRAY [0..31] OF CHAR;
  b: BOOLEAN;
  menuNum: CARDINAL;
  itemNum: CARDINAL;
  subNum: CARDINAL;
BEGIN
  imsg := IntuiMessagePtr(msg);
  WITH imsg^ DO
    MsgClass := Class;
    MsgCode  := Code;
  END;
  IF (NOT (RawKey IN MsgClass)) THEN (* If not RawKey then reply immediately *)
    ReplyMsg(imsg);
  END;
 
  IF (MenuPick IN MsgClass) THEN
    menuCode := MsgCode;
    WHILE (menuCode # MenuNull) DO
      menuNum := MENUNUM(menuCode);
      itemNum := ITEMNUM(menuCode);
      subNum  := SUBNUM(menuCode);
      IF (NOT DoGlobalMenuCommand(menuNum, itemNum, subNum, LocalMenus,
                                  WindowClass(0), InvalidWindowIdx, NIL)) THEN
        CASE (itemNum) OF
        | 0: (* Set Window Color *)
             IF (RequestTextFgBgColors(ConfigStatusWindowDetailPen,
                                       ConfigStatusWindowBlockPen)) THEN
               SetupStatusWindow();
             END;
        | 1: (* Set Text Color *)
             IF (RequestTextFgBgColors(ConfigStatusWindowFgColor,
                                       ConfigStatusWindowBgColor)) THEN
               SetupStatusWindow();
             END;
        END;
      END;
      menuItemPtr := ItemAddress(SWindow^.MenuStrip^, menuCode);
      menuCode    := menuItemPtr^.NextSelect;
    END;
  ELSIF (RawKey IN MsgClass) THEN
    keyLength := DeadKeyConvert(imsg, ADR(keyBuf[0]), SIZE(keyBuf));
    ReplyMsg(imsg);
    IF (MsgCode < 80H) AND (keyLength > 0) THEN
      b := DoGlobalKbdCommand(ADR(keyBuf[0]), keyLength, 
                              WindowClass(0), InvalidWindowIdx);
    END;
  END;
END HandleMsgStatusWindow;

(* InitDBStatusWindow - allocate module resources *)
PROCEDURE InitDBStatusWindow(): BOOLEAN;
BEGIN
  BeginMenuStrip();
    AddMenu(                           ADR("Status "));
      AddMenuItem(                       ADR(" Set Window Color "));
      AddMenuItem(                       ADR(" Set Text Color"));
  StatusMenuStrip := EndMenuStrip();
  IF (StatusMenuStrip # NIL) THEN
    LinkGlobalMenuStrip(StatusMenuStrip^);
  END;
  RETURN (StatusMenuStrip # NIL);
END InitDBStatusWindow;

(* CleanupDBStatusWindow - cleanup module resourecs *)
PROCEDURE CleanupDBStatusWindow();
BEGIN
  IF (SWindow # NIL) THEN
    ClearMenuStrip(SWindow^);
    CloseWindow(SWindow^);
  END;

  IF (StatusMenuStrip # NIL) THEN
    FreeMenuStrip(StatusMenuStrip^);
  END;

  IF (ShowingStatusString # NIL) THEN
    DeallocString(ShowingStatusString);
  END;
END CleanupDBStatusWindow;

BEGIN
  ShowErrMsgNoMemory    := ADR("Insufficient memory.");
  ShowErrMsgInvalidDest := ADR("Invalid destination window for current selection.");
  ShowErrMsgNoSrcSelect := ADR("A selection has not been made.");
  ShowErrMsgBadDest     := ADR("Destination window does not accept any selections.");
  ShowErrMsgProgIsExec  := ADR("Invalid operation during program execution.");
END DBStatusWindow.
