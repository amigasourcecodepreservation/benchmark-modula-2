(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBInitCleanup.MOD                 Version: Amiga.01.10             *
 * Created: 01/02/89   Updated: 03/21/94   Author: Leon Frenkel             *
 * Description: Debugger module initialization, cleanup and error handling. *
 ****************************************************************************)

IMPLEMENTATION MODULE DBInitCleanup;

(***************************************************************************
 * Changes for new release 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.2 -----
 * 
 * Add standard Amiga version strings
 * 
 *    1. Added CONST AmigaStdVersion ="$VER: M2Debug-Beta 0.2 (02.01.94)"
 * 
 * Centralize Amiga Runtime Environment Sense and Inquiry Code
 * 
 *    1. Added call to InitDBEnvironmentInquiry and Cleanup...
 *       InitDBEnvironmentInquiry() is the first one called.
 * 
 ****************************************************************************)

FROM SYSTEM IMPORT
  ADDRESS,
  ADR;
IMPORT CPrintTerminal;
FROM DBBreakPoints IMPORT
  InitDBBreakPoints, CleanupDBBreakPoints;
FROM DBBreakPointWindows IMPORT
  InitDBBreakPointWindows, CleanupDBBreakPointWindows;
FROM DBConfiguration IMPORT
  InitDBConfiguration, CleanupDBConfiguration;
FROM DBDirectoryWindows IMPORT
  InitDBDirectoryWindows, CleanupDBDirectoryWindows;
FROM DBDisassembler IMPORT
  InitDBDisassembler, CleanupDBDisassembler;
FROM DBDisAsmWindows IMPORT
  InitDBDisAsmWindows, CleanupDBDisAsmWindows;
FROM DBEnvironmentInquiry IMPORT
  InitDBEnvironmentInquiry, CleanupDBEnvironmentInquiry;
FROM DBFiles IMPORT
  InitDBFiles, CleanupDBFiles;
FROM DBInfoWindows IMPORT
  InitDBInfoWindows, CleanupDBInfoWindows;
FROM DBMemoryWindows IMPORT
  InitDBMemoryWindows, CleanupDBMemoryWindows;
FROM DBModuleListWindows IMPORT
  InitDBModuleListWindows, CleanupDBModuleListWindows;
FROM DBMenusKbd IMPORT
  InitDBMenusKbd, CleanupDBMenusKbd;
FROM DBMisc IMPORT
  InitDBMisc, CleanupDBMisc;
FROM DBProcChainWindows IMPORT
  InitDBProcChainWindows, CleanupDBProcChainWindows;
FROM DBRegistersWindows IMPORT
  InitDBRegistersWindows, CleanupDBRegistersWindows;
FROM DBRequesters IMPORT
  InitDBRequesters, CleanupDBRequesters;
FROM DBScreenSwap IMPORT
  InitDBScreenSwap, CleanupDBScreenSwap;
FROM DBSelection IMPORT
  InitDBSelection, CleanupDBSelection;
FROM DBSourceWindows IMPORT
  InitDBSourceWindows, CleanupDBSourceWindows;
FROM DBStatusWindow IMPORT
  InitDBStatusWindow, CleanupDBStatusWindow;
FROM DBSymDataWindows IMPORT
  InitDBSymDataWindows, CleanupDBSymDataWindows;
FROM DBUserProgram IMPORT
  InitDBUserProgram, CleanupDBUserProgram;
FROM DBWindows IMPORT
  InitDBWindows, CleanupDBWindows;
IMPORT FormatStringReals;
IMPORT ScanStringReals;
FROM System IMPORT StackSize;

CONST 
  AmigaStdVersion ="$VER: M2Debug 1.10 (3.21.94)";
  ScreenTitle = "M2SCS: Source-Level Debugger  Amiga v1.10 Copyright \xA9 1993 Jim Olinger";

CONST
  MinStackSize = 10000D;

  NewLine = "\n";

VAR
  FErrSmallStack : ADDRESS;

(* ===== PRIVATE ===== *)

PROCEDURE printf(fmtStr: ADDRESS);
VAR
  str: POINTER TO ARRAY [0..32765] OF CHAR;
BEGIN
  str := fmtStr;
  CPrintTerminal.printf(str^, ErrArgs);
END printf;

(* CleanupModules - call the cleanup function of each debugger module *)
PROCEDURE CleanupModules();
BEGIN
  CleanupDBScreenSwap();
  CleanupDBRequesters();
  CleanupDBSelection();
  CleanupDBDirectoryWindows();
  CleanupDBInfoWindows();
  CleanupDBMemoryWindows();
  CleanupDBRegistersWindows();
  CleanupDBSymDataWindows();
  CleanupDBSourceWindows();
  CleanupDBDisAsmWindows();
  CleanupDBProcChainWindows();
  CleanupDBBreakPointWindows();
  CleanupDBModuleListWindows();
  CleanupDBStatusWindow();
  CleanupDBWindows();
  CleanupDBMenusKbd();
  CleanupDBUserProgram();
  CleanupDBBreakPoints();
  CleanupDBFiles();
  CleanupDBDisassembler();
  CleanupDBConfiguration();
  CleanupDBMisc();
  CleanupDBEnvironmentInquiry();
END CleanupModules;

(* ===== PUBLIC ===== *)

(* Abort - cleanup all debugger modules and exit debugger *)
PROCEDURE Abort();
BEGIN
  CleanupModules();
  HALT;
END Abort;

(* FatalError - display a fatal error msg and the abort debugger *)
PROCEDURE FatalError(fmtStr: ADDRESS);
BEGIN
  printf(ADR("ERROR: "));
  printf(fmtStr);
  printf(ADR(NewLine));
  Abort();
END FatalError;

(* InitModules - initialize all debugger modules *)
PROCEDURE InitModules();
BEGIN
  printf(M2DebugTitle);
  printf(ADR(NewLine));

  IF (StackSize < MinStackSize) THEN
    FatalError(FErrSmallStack);
  END;

  IF (NOT InitDBEnvironmentInquiry()) OR
     (NOT InitDBMisc()) OR
     (NOT InitDBConfiguration()) OR
     (NOT InitDBDisassembler()) OR
     (NOT InitDBFiles()) OR
     (NOT InitDBBreakPoints()) OR
     (NOT InitDBUserProgram()) OR
     (NOT InitDBMenusKbd()) OR
     (NOT InitDBWindows()) OR
     (NOT InitDBStatusWindow()) OR
     (NOT InitDBModuleListWindows()) OR
     (NOT InitDBBreakPointWindows()) OR
     (NOT InitDBProcChainWindows()) OR
     (NOT InitDBDisAsmWindows()) OR
     (NOT InitDBSourceWindows()) OR
     (NOT InitDBSymDataWindows()) OR
     (NOT InitDBRegistersWindows()) OR
     (NOT InitDBMemoryWindows()) OR
     (NOT InitDBInfoWindows()) OR
     (NOT InitDBDirectoryWindows()) OR
     (NOT InitDBSelection()) OR
     (NOT InitDBRequesters()) OR
     (NOT InitDBScreenSwap()) THEN
    FatalError(FErrNoMemory);
  END;
END InitModules;

BEGIN
  M2DebugTitle := ADR(ScreenTitle);

  (* Initialize fatal error messages *)
  FErrNoMemory          := ADR("Insufficient memory.");
  FErrInvalidCLIParms   := ADR("Invalid CLI parameters.");
  FErrInvalidConfigFile := ADR("Invalid configuration file.");
  FErrSmallStack        := ADR("Insufficient stack size. Minimum size is 10,000 bytes.");
END DBInitCleanup.
