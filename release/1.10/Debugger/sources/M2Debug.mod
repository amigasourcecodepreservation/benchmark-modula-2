
(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: M2Debug.MOD                       Version: Amiga.01.10             *
 * Created: 01/02/89   Updated: 03/21/94   Author: Leon Frenkel             *
 * Description: Modula-2 Source-Level/Post-Mortem Debugger main module      *
 ****************************************************************************)

MODULE M2Debug;

(***************************************************************************
 * Changes for new release 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.2 -----
 * 
 * Support for Clipboard
 * 
 *   1. Still linking with old (v1.2) amiga library interface from Leon F.,
 *      except that IFFParse and Utility from v3.1 interface are needed.
 *      As of now, these need to be recompiled specially for M2Debug for
 *      M2 key compatibility. See the build file "MakeIt.s"
 * 
 * Support for IEEE and FPP REALs
 * 
 *   1. Link with "switch" version of RealConversions module, which responds
 *      at run-time to the value of System.REALflavor. This is a kludge, but
 *      easy way to get M2Debug to support both REAL flavors with almost no
 *      effort.
 * 
 ***************************************************************************
*)

FROM SYSTEM IMPORT
  ADDRESS,
  ADR;
FROM DBConfiguration IMPORT
  CLIPostMortemDebug, CLIProgFileName, CLIProgParmsPtr, CLIProgParmsLength,
  CLIConfigFileName, CLIProgPMDInfo,
  GetCLIParms, LoadConfigFile;
FROM DBInitCleanup IMPORT
  FErrNoMemory, FErrInvalidCLIParms, FErrInvalidConfigFile, FatalError,
  InitModules;
FROM DBMenusKbd IMPORT
  CommandMode,
  SetCommandMode, SetupOptionsMenu;
FROM DBScreenSwap IMPORT
  GetScreenSwapSignal, HandleSignalScreenSwap;
FROM DBStatusWindow IMPORT
  GetStatusWindowMsgPort, HandleMsgStatusWindow, ShowError;
FROM DBTextWindows IMPORT
  HandleMsgVTWindow;
FROM DBUserProgram IMPORT
  GetDebugMsgPort, HandleMsgUserProgram, RunProgramAsPMD, RunProgramAsSLD;
FROM DBWindows IMPORT
  GetVTWindowsMsgPort,
  SetupScreenAndStatusWindow, OpenConfigWindows;
FROM Intuition IMPORT
  IntuiMessagePtr, IDCMPFlags, IDCMPFlagsSet, SelectUp;
FROM Ports IMPORT
  MsgPortPtr,
  GetMsg;
FROM System IMPORT
  argc;
FROM Tasks IMPORT
  SignalSet,
  Wait;

(* ClearErrorMsg - clear current error message if user performs new operation *)
PROCEDURE ClearErrorMsg(imsg: IntuiMessagePtr);
VAR
  class: IDCMPFlagsSet;
  code: CARDINAL;
BEGIN
  class := imsg^.Class;
  code  := imsg^.Code;

  (* Do not clear msg on RawKey release, MouseButtons release, IntuiTicks *)
  IF NOT (((RawKey IN class) AND (code >= 80H)) OR (IntuiTicks IN class) OR
          ((MouseButtons IN class) AND (code = SelectUp))) THEN
    ShowError(NIL); (* Clear error msg *)
  END;
END ClearErrorMsg;

(* MainLoop - process messages until user quits debugger *)
PROCEDURE MainLoop();
VAR
  debugPort: MsgPortPtr;
  vtwPort: MsgPortPtr;
  swPort: MsgPortPtr;
  scrSig: CARDINAL;
  debugSig: CARDINAL;
  vtwSig: CARDINAL;
  swSig: CARDINAL;
  waitMask: SignalSet;
  retMask: SignalSet;
  msg: ADDRESS;
BEGIN
  debugPort := GetDebugMsgPort();
  vtwPort   := GetVTWindowsMsgPort();
  swPort    := GetStatusWindowMsgPort();
  scrSig    := GetScreenSwapSignal();
  debugSig  := CARDINAL(debugPort^.mpSigBit);
  vtwSig    := CARDINAL(vtwPort^.mpSigBit);
  swSig     := CARDINAL(swPort^.mpSigBit);
  waitMask  := SignalSet{scrSig, debugSig, vtwSig, swSig};

  LOOP
    retMask := Wait(waitMask);
    IF (scrSig IN retMask) THEN (* Screen Swap Signal *)
      HandleSignalScreenSwap();
    END;
    IF (vtwSig IN retMask) THEN (* Virtual Text Windows Msgs *)
      LOOP
        msg := GetMsg(vtwPort^);
        IF (msg = NIL) THEN EXIT; END;

        ClearErrorMsg(msg);
        HandleMsgVTWindow(msg);
      END;
    END;
    IF (swSig IN retMask) THEN (* Status Window Msgs *)
      LOOP
        msg := GetMsg(swPort^);
        IF (msg = NIL) THEN EXIT; END;

        ClearErrorMsg(msg);
        HandleMsgStatusWindow(msg);
      END;
    END;
    IF (debugSig IN retMask) THEN (* Debug Port Msg *)
      msg := GetMsg(debugPort^);
      IF (msg # NIL) THEN
        HandleMsgUserProgram(msg);
      END;
    END;
  END;
END MainLoop;

(* Debugger - main debugger procedure *)
PROCEDURE Debugger();
BEGIN
  (* Initialize all debugger modules *)
  InitModules();

  (* Get CLI parameter to determine environment *)
  IF (NOT GetCLIParms()) THEN
    FatalError(FErrInvalidCLIParms);
  END;

  (* Read default configuration file *)
  IF (NOT LoadConfigFile(CLIConfigFileName)) THEN
    FatalError(FErrInvalidConfigFile);
  END;

  (* Setup debugger screen and status window *)
  IF (NOT SetupScreenAndStatusWindow()) THEN
    FatalError(FErrNoMemory);
  END;

  (* Setup options menu according to current configuration *)
  SetupOptionsMenu();

  (* Set initial command mode as no program loaded *)
  SetCommandMode(CMNoProg);

  (* Open windows specified in configuration *)
  OpenConfigWindows();

  IF (CLIPostMortemDebug) THEN (* Start debugger as Post Mortem Debug *)
    RunProgramAsPMD(CLIProgPMDInfo);
  ELSIF (CLIProgFileName # NIL) THEN (* Start debugger with specified prog *)
    RunProgramAsSLD(CLIProgFileName, CLIProgParmsPtr, CLIProgParmsLength);
  END;

  (* Enter main debugger loop *)
  MainLoop();
END Debugger;

BEGIN (* main *)
  IF (argc > 0) THEN (* Only CLI Startup Allowed *)
    Debugger();
  END;
END M2Debug.
