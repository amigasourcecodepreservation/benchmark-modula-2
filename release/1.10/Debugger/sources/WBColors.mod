(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1988 by Jim Olinger                      *
 ****************************************************************************
 * Name: WBColors.MOD                      Version: Amiga.01.00             *
 * Created: 03/31/89   Updated: 03/31/89   Author: Leon Frenkel             *
 * Description: Toggle WorkBench screen colors from 2 -> 4 or 4 -> 2.       *
 ****************************************************************************)

MODULE WBColors;

FROM SYSTEM IMPORT
  BYTE, ADDRESS;
FROM Blit IMPORT
  BltClearFlagsSet,
  BltClear;
FROM Intuition IMPORT
  ScreenPtr,
  OpenWorkBench, RemakeDisplay;
FROM Rasters IMPORT
  AllocRaster, FreeRaster;

VAR
  scr: ScreenPtr;
  plane: ADDRESS;
BEGIN
  scr := OpenWorkBench();
  IF (scr # NIL) THEN
    WITH scr^.RastPort.BitMap^ DO
      IF (CARDINAL(Depth) = 1) THEN
        plane := AllocRaster(scr^.Width, scr^.Height);
        IF (plane # NIL) THEN
          BltClear(plane, BytesPerRow * Rows, BltClearFlagsSet{0});
          Planes[1] := plane;
          Depth     := BYTE(2);
        END;
      ELSIF (CARDINAL(Depth) = 2) THEN
        Depth := BYTE(1);
        FreeRaster(Planes[1], scr^.Width, scr^.Height);
        Planes[1] := NIL;
      END;
    END;
    RemakeDisplay();
  END;
END WBColors.
