(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBMemoryWindows.MOD               Version: Amiga.01.10             *
 * Created: 02/18/89   Updated: 03/21/94   Author: Leon Frenkel             *
 * Description: Debugger memory windows handling module.                    *
 ****************************************************************************)

IMPLEMENTATION MODULE DBMemoryWindows;

(***************************************************************************
 * Changes for new release 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.4 -----
 * 
 * Better Initial Adr for a New Memory Window *
 * 
 *    1. In UpdateMemoryWindow(), for a new window the SetMemoryWindowOnAdr()
 *       was setting to addr 0, now to DBFiles.GetModuleDataStart(0).
 * 
 ***************************************************************************
*)

FROM SYSTEM IMPORT
  ADDRESS, BYTE,
  ADR;
FROM CMemOp IMPORT
  movmem;
FROM CPrintBuffer IMPORT
  sprintf;
FROM DBConfiguration IMPORT
  ConfigWindow;
FROM DBFiles IMPORT
  InvalidAbsAdr,
  GetModuleDataStart;
FROM DBMenusKbd IMPORT
  LinkGlobalMenuStrip, DoGlobalMenuCommand, DoGlobalKbdCommand,
  AddMenuItemWithCheck, AddMenuItemWithCheckList, AddMenuItemNoHighlight;
FROM DBMisc IMPORT
  AllocMemory, DeallocMemory, AllocString, DeallocString,
  GetMemoryLong, GetMemoryValue, CompareMemory;
FROM DBRequesters IMPORT
  RequestTextFgBgColors, RequestTextWidth, RequestAddress;
FROM DBSelection IMPORT
  SelectionData, StorageFormat, StorageNumber, ModuleFileType,
  ClearSelection, GetSelection, SetSelection, GetSelectionLineInfo,
  IsPosSelected, IfCurrentWindowClearSelection;
FROM DBStatusWindow IMPORT
  ShowErrMsgNoMemory, ShowErrMsgInvalidDest,
  ShowError;
FROM DBTextWindows IMPORT
  DefMinWindowWidth, DefMinWindowHeight, DefMaxWindowWidth, DefMaxWindowHeight,
  VTWindow, NewVTWindow, VTWindowNIL, VTWindowParms,
  OpenVTWindow, CloseVTWindow, InitParmsVTWindow, WriteBufferVTWindow,
  WriteCharVTWindow, SetCursorPosVTWindow, GetUserDataVTWindow,
  WriteColorVTWindow, ClearVTWindow, RefreshVTWindow, GetDisplayRangeVTWindow,
  RefreshRangeVTWindow, ScrollVTWindow, SetTitleVTWindow, SetMenuStripVTWindow,
  ClearMenuStripVTWindow, SetDetailPenBlockPenVTWindow, SetDefaultColorVTWindow,
  GetDetailPenBlockPenVTWindow, GetInfoVTWindow, IsQualifierShiftVTWindow;
FROM DBUserProgram IMPORT
  ProcChainNodePtr,
  IsProgramLoaded, GetProcChainNode;
FROM DBWindows IMPORT
  WindowClass,
  GetVTWindowsMsgPort;
FROM FormatString IMPORT
  FormatArg;
FROM Intuition IMPORT
  Menu, MenuPtr, MenuItemMutualExcludeSet;
FROM Memory IMPORT
  MemReqSet, MemClear;
FROM SimpleMenus IMPORT
  BeginMenuStrip, EndMenuStrip, FreeMenuStrip, AddMenu, AddMenuItem;

CONST
  (* Memory window title when no data is being displayed *)
  DefMemoryWindowTitle = "Memory";

  LocalMenus = 1; (* Number of window specific menus *)

  (* Number of lines always displayed in the memory window *)
  MemoryWindowTotalLines = 32768;
  MemoryWindowCenterLine = MemoryWindowTotalLines DIV 2;

  AdrInfoWidth       = 10; (* Char width of address information *)
  AdrInfoNumberWidth = 8;  (* Char width of address component only *)

  ColumnWidthREAL   = 13; (* Char width of one column of a REAL display *)
  ColumnWidthSTRING = 1;  (* Char width of one column of a STRING display *)
  ColumnWidthCHAR   = 4;  (* Char width of one column of a CHAR display *)

  (* ASCII key information *)
  CSI           = CHAR(09BH); (* Start of Left|Right arrow key sequence *)
  LeftArrowKey  = "D";  (* CSI + "D" = left arrow *)
  RightArrowKey = "C";  (* CSI + "C" = right arrow *)

TYPE
  (* Memory window display format *)
  DispFormat   = (DFReal, DFString, DFChar, DFNumber);

  (* Memory window DFNumber number size mode *)
  NumberSize   = (NSByte, NSWord, NSLong);

  (* Memory window DFNumber number radix mode *)
  NumberFormat = (NFBin, NFOct, NFHex, NFDecS, NFDecU);

TYPE
  (* Memory window descriptor *)
  MemoryWindowPtr = POINTER TO MemoryWindow;
  MemoryWindow = RECORD
                   mwNext             : MemoryWindowPtr; (* next window *)
                   mwVTWindow         : VTWindow;      (* text window *)
                   mwVTWindowParms    : VTWindowParms; (* window parms *)
                   mwMenuStrip        : MenuPtr;       (* local menu strip *)
                   mwTitle            : ADDRESS;       (* ptr to title str *)
                   mwDisplayOn        : BOOLEAN;       (* displaying data *)
                   mwRedrawMemory     : BOOLEAN;       (* redraw values only *)
                   mwDispFormat       : DispFormat;    (* display format *)
                   mwDispNumberSize   : NumberSize;    (* number size *)
                   mwDispNumberFormat : NumberFormat;  (* number format *)
                   mwFirstLine        : CARDINAL;      (* first display line*)
                   mwHeight           : CARDINAL;      (* current height *)
                   mwMemoryLookup     : ADDRESS;       (* ptr to lookup buf *)
                   mwMaxColumns       : CARDINAL;      (* # of columns *)
                   mwRangeStartAdr    : ADDRESS;       (* memory start adr *)
                   mwRangeEndAdr      : ADDRESS;       (* memory end adr *)
                   mwColumnWidth      : CARDINAL;      (* width of data column*)
                   mwColumns          : CARDINAL;      (* # of columns/line *)
                   mwValueSize        : CARDINAL;      (* value size in bytes*)
                   mwBytesPerLine     : CARDINAL;      (* # of bytes/line *)
                 END;

VAR
  MemoryWindowList : MemoryWindowPtr; (* list of memory windows *)

  (* NumberSize -> number of bytes lookup table *)
  ValueSizeNumberFormat   : ARRAY NumberSize OF CARDINAL;

  (* NumberFormat, NumberSize -> column width table *)
  ColumnWidthNumberFormat : ARRAY NumberFormat, NumberSize OF CARDINAL;

  (* NumberFormat, NumberSize -> format field width *)
  FieldWidthNumberFormat  : ARRAY NumberFormat, NumberSize OF CARDINAL;

  (* NumberFormat -> format string *)
  FmtStringNumberFormat   : ARRAY NumberFormat OF ADDRESS;

(* ===== PRIVATE ===== *)

(* DeallocMemoryLookupBuffer - deallocate memory lookup buffer *)
PROCEDURE DeallocMemoryLookupBuffer(VAR mw: MemoryWindow);
BEGIN
  WITH mw DO
    IF (mwMemoryLookup # NIL) THEN
      DeallocMemory(mwMemoryLookup);
      mwMemoryLookup := NIL;
    END;
  END;
END DeallocMemoryLookupBuffer;

(* AllocMemoryLookupBuffer - allocate memory lookup buffer *)
PROCEDURE AllocMemoryLookupBuffer(VAR mw: MemoryWindow; height: CARDINAL): BOOLEAN;
BEGIN
  DeallocMemoryLookupBuffer(mw);
  WITH mw DO
    mwHeight       := height;
    mwMemoryLookup := AllocMemory(mwBytesPerLine * mwHeight, MemReqSet{});
    RETURN (mwMemoryLookup # NIL);
  END;
END AllocMemoryLookupBuffer;

(*------------------------------ NOT USED ------------------------------
(* ScrollMemoryLookupBuffer - scroll up/down memory lookup buffer *)
PROCEDURE ScrollMemoryLookupBuffer(VAR mw: MemoryWindow; offset: INTEGER);
VAR
  absOffset: CARDINAL;
  fromIdx: CARDINAL;
  toIdx: CARDINAL;
  copySize: LONGCARD;
  src: ADDRESS;
  dst: ADDRESS;
BEGIN
  WITH mw DO
    absOffset := ABS(offset);
    IF (offset # 0) AND (mwHeight > absOffset) THEN
      IF (offset > 0) THEN (* (scroll down) *)
        fromIdx := 0;
        toIdx   := mwHeight - absOffset;
      ELSE (* offset < 0 (scroll up) *)
        fromIdx := absOffset;
        toIdx   := 0;
      END;
      copySize := (mwHeight - absOffset) * mwBytesPerLine;
      src      := mwMemoryLookup + LONGCARD(fromIdx * mwBytesPerLine);
      dst      := mwMemoryLookup + LONGCARD(toIdx * mwBytesPerLine);

      (* movmem() handles overlaping memory block correctly! *)
      movmem(src, dst, copySize);
    END;
  END;
END ScrollMemoryLookupBuffer;
------------------------------ NOT USED ------------------------------ *)

(* CompareOrStoreMemory - compare two lines of data for equality *)
PROCEDURE CompareOrStoreMemory(VAR mw: MemoryWindow; lineIdx: CARDINAL;
                               srcAdr: ADDRESS; dontCmp: BOOLEAN): BOOLEAN;
VAR
  equal: BOOLEAN;
  bufAdr: ADDRESS;
BEGIN
  WITH mw DO
    bufAdr := mwMemoryLookup + LONGCARD(lineIdx * mwBytesPerLine);
    equal  := (NOT dontCmp) AND (CompareMemory(srcAdr, bufAdr, mwBytesPerLine));
    IF (NOT equal) THEN
      movmem(srcAdr, bufAdr, mwBytesPerLine);
    END;
  END;
  RETURN (equal);
END CompareOrStoreMemory;

(* AddWindowNode - create a window node and link into window list *)
PROCEDURE AddWindowNode(): MemoryWindowPtr;
VAR
  mw: MemoryWindowPtr;
BEGIN
  mw := AllocMemory(SIZE(MemoryWindow), MemReqSet{MemClear});
  IF (mw # NIL) THEN
    IF (MemoryWindowList # NIL) THEN
      mw^.mwNext := MemoryWindowList;
    END;
    MemoryWindowList := mw;
  END;
  RETURN (mw);
END AddWindowNode;

(* DeleteWindowNode - delete window node and unlink from window list *)
PROCEDURE DeleteWindowNode(mw: MemoryWindowPtr);
VAR
  prev: MemoryWindowPtr;
  cur: MemoryWindowPtr;
BEGIN
  prev := NIL;
  cur  := MemoryWindowList;
  WHILE (cur # mw) DO
    prev := cur;
    cur  := cur^.mwNext;
  END;

  IF (prev = NIL) THEN (* First in list *)
    MemoryWindowList := mw^.mwNext;
  ELSE (* Not first in list *)
    prev^.mwNext := mw^.mwNext;
  END;

  DeallocMemory(mw);
END DeleteWindowNode;

(* GetWindowIndex - get position of window node in list *)
PROCEDURE GetWindowIndex(mw: MemoryWindowPtr): CARDINAL;
VAR
  index: CARDINAL;
  p: MemoryWindowPtr;
BEGIN
  index := 0;
  p     := MemoryWindowList;
  WHILE (mw # p) DO
    INC(index);
    p := p^.mwNext;
  END;
  RETURN (index);
END GetWindowIndex;

(* CloseMemoryWindow - close memory window *)
PROCEDURE CloseMemoryWindow(VAR mw: MemoryWindow);
BEGIN
  WITH mw DO
    IfCurrentWindowClearSelection(mwVTWindow);

    DeallocMemoryLookupBuffer(mw);

    IF (mwMenuStrip # NIL) THEN
      ClearMenuStripVTWindow(mwVTWindow);
      FreeMenuStrip(mwMenuStrip^);
    END;

    CloseVTWindow(mwVTWindow);

    IF (mwTitle # NIL) THEN
      DeallocString(mwTitle);
    END;
  END;
  DeleteWindowNode(ADR(mw));
END CloseMemoryWindow;

(* CloseMemoryWindowNoMemory - close window and show no memory err msg *)
PROCEDURE CloseMemoryWindowNoMemory(VAR mw: MemoryWindow);
BEGIN
  CloseMemoryWindow(mw);
  ShowError(ShowErrMsgNoMemory);
END CloseMemoryWindowNoMemory;

(* CreateMenuStrip - create a local menu strip for a window *)
PROCEDURE CreateMenuStrip(VAR mw: MemoryWindow): BOOLEAN;
TYPE
  M = MenuItemMutualExcludeSet;

VAR
  ms: MenuPtr;
  ok: BOOLEAN;
BEGIN
  BeginMenuStrip();
    AddMenu(                           ADR("Memory "));
      AddMenuItemWithCheckList(          ADR(" Show Real"), (mw.mwDispFormat = DFReal), M{1,2,3});
      AddMenuItemWithCheckList(          ADR(" Show String"), (mw.mwDispFormat = DFString), M{0,2,3});
      AddMenuItemWithCheckList(          ADR(" Show Char"), (mw.mwDispFormat = DFChar), M{0,1,3});
      AddMenuItemWithCheckList(          ADR(" Show Number"), (mw.mwDispFormat = DFNumber), M{0,1,2});
      AddMenuItemNoHighlight(            ADR(" ---------------- "));
      AddMenuItemWithCheckList(          ADR(" Byte"), (mw.mwDispNumberSize = NSByte), M{6,7});
      AddMenuItemWithCheckList(          ADR(" Word"), (mw.mwDispNumberSize = NSWord), M{5,7});
      AddMenuItemWithCheckList(          ADR(" Long"), (mw.mwDispNumberSize = NSLong), M{5,6});
      AddMenuItemNoHighlight(            ADR(" ---------------- "));
      AddMenuItemWithCheckList(          ADR(" Binary"), (mw.mwDispNumberFormat = NFBin), M{10,11,12,13});
      AddMenuItemWithCheckList(          ADR(" Octal"), (mw.mwDispNumberFormat = NFOct), M{9,11,12,13});
      AddMenuItemWithCheckList(          ADR(" Hexadecimal"), (mw.mwDispNumberFormat = NFHex), M{9,10,12,13});
      AddMenuItemWithCheckList(          ADR(" Decimal Signed"), (mw.mwDispNumberFormat = NFDecS), M{9,10,11,13});
      AddMenuItemWithCheckList(          ADR(" Decimal Unsigned "), (mw.mwDispNumberFormat = NFDecU), M{9,10,11,12});
      AddMenuItemNoHighlight(            ADR(" ----------------"));
      AddMenuItem(                       ADR(" Set Text Width"));
      AddMenuItem(                       ADR(" Set Window Color"));
      AddMenuItem(                       ADR(" Set Text Color"));
  ms := EndMenuStrip();
  ok := (ms # NIL);

  IF (ok) THEN
    LinkGlobalMenuStrip(ms^);
    WITH mw DO
      mwMenuStrip := ms;
      SetMenuStripVTWindow(mwVTWindow, mwMenuStrip^);
    END;
  ELSE
    CloseMemoryWindowNoMemory(mw);
  END;
  RETURN (ok);
END CreateMenuStrip;

(* SetDefaultWindowTitle - set default memory window title *)
PROCEDURE SetDefaultWindowTitle(VAR mw: MemoryWindow);
BEGIN
  WITH mw DO
    SetTitleVTWindow(mwVTWindow, ADR(DefMemoryWindowTitle));
    IF (mwTitle # NIL) THEN
      DeallocString(mwTitle);
    END;
    mwTitle := NIL;
  END;
END SetDefaultWindowTitle;

(* SetNewWindowTitle - set a new memory window title *)
PROCEDURE SetNewWindowTitle(VAR mw: MemoryWindow; title: ADDRESS);
VAR
  newTitle: ADDRESS;
BEGIN
  newTitle := AllocString(title);
  IF (newTitle # NIL) THEN
    WITH mw DO
      SetTitleVTWindow(mwVTWindow, newTitle);
      IF (mwTitle # NIL) THEN
        DeallocString(mwTitle);
      END;
      mwTitle := newTitle;
    END;
  END;
END SetNewWindowTitle;

(* ClearMemoryWindow - disable display of data in window *)
PROCEDURE ClearMemoryWindow(VAR mw: MemoryWindow);
BEGIN
  WITH mw DO
    IF (mwDisplayOn) THEN
      IfCurrentWindowClearSelection(mwVTWindow);
      SetDefaultWindowTitle(mw);
      mwDisplayOn := FALSE;
      WITH mwVTWindowParms DO
        vtpInitialLine := 0;
        vtpTotalLines  := 1;
        vtpMaxColumns  := 1;
      END;
      IF (NOT InitParmsVTWindow(mwVTWindow, mwVTWindowParms)) THEN
        CloseMemoryWindowNoMemory(mw);
      END;
    END;
  END;
END ClearMemoryWindow;

(* SetMemoryWindowParms - setup window parms according to display parms *)
PROCEDURE SetMemoryWindowParms(VAR mw: MemoryWindow);
VAR
  lineWidth: CARDINAL;
  calcWidth: CARDINAL;
BEGIN
  DeallocMemoryLookupBuffer(mw);

  WITH mw DO
    IF (mwDispFormat = DFReal) THEN
      mwColumnWidth := ColumnWidthREAL;
      mwValueSize   := 4;
    ELSIF (mwDispFormat = DFString) THEN
      mwColumnWidth := ColumnWidthSTRING;
      mwValueSize   := 1;
    ELSIF (mwDispFormat = DFChar) THEN
      mwColumnWidth := ColumnWidthCHAR;
      mwValueSize   := 1;
    ELSIF (mwDispFormat = DFNumber) THEN
      mwColumnWidth := ColumnWidthNumberFormat[mwDispNumberFormat, mwDispNumberSize];
      mwValueSize   := ValueSizeNumberFormat[mwDispNumberSize];
    END;

    calcWidth := mwMaxColumns;
    (* Regardless of maxColumns one column of data is always displayed! *)
    IF (calcWidth < AdrInfoWidth) OR 
       (calcWidth - AdrInfoWidth < mwColumnWidth) THEN
      calcWidth := AdrInfoWidth + mwColumnWidth;
      lineWidth := mwColumnWidth;
    ELSE
      lineWidth := calcWidth - AdrInfoWidth;
    END;

    (* Calculate number of columns which fit the window and bytes per line *)
    mwColumns      := lineWidth DIV mwColumnWidth;
    mwBytesPerLine := mwColumns * mwValueSize;

    mwVTWindowParms.vtpMaxColumns := calcWidth;
      
    mwRedrawMemory := FALSE;
    mwDisplayOn    := TRUE;
  END;
END SetMemoryWindowParms;

(* SetMemoryWindowOnAdr - display memory starting at specified address *)
PROCEDURE SetMemoryWindowOnAdr(VAR mw: MemoryWindow; adr: ADDRESS);
VAR
  center: LONGCARD;
  newFirstLine: LONGCARD;
  before: CARDINAL;
  after: CARDINAL;
  i: CARDINAL;
  argo: ARRAY [0..9] OF FormatArg;
  str: ARRAY [0..31] OF CHAR;
BEGIN
  WITH mw DO
    IfCurrentWindowClearSelection(mwVTWindow);

    center := LONGCARD(mwBytesPerLine) * LONGCARD(MemoryWindowCenterLine);
    IF (adr < center) THEN
      before := adr DIV LONGCARD(mwBytesPerLine);
      after  := MemoryWindowTotalLines - before;
      mwRangeStartAdr := adr - (LONGCARD(mwBytesPerLine) * LONGCARD(before));
      mwRangeEndAdr   := adr + (LONGCARD(mwBytesPerLine) * LONGCARD(after));
      newFirstLine    := before;
    ELSIF (adr >= (0D - center)) THEN
      after  := (0D - adr) DIV LONGCARD(mwBytesPerLine);
      before := MemoryWindowTotalLines - after;
      mwRangeStartAdr := adr - (LONGCARD(mwBytesPerLine) * LONGCARD(before));
      mwRangeEndAdr   := adr + (LONGCARD(mwBytesPerLine) * LONGCARD(after));
      newFirstLine    := before;
      IF (mwRangeEndAdr = 0D) THEN
        mwRangeEndAdr := 0FFFFFFFFH;
      END;
    ELSE
      mwRangeStartAdr := adr - center;
      mwRangeEndAdr   := adr + center;
      newFirstLine    := MemoryWindowCenterLine;
    END;

    argo[0].L := mwRangeStartAdr;
    argo[1].L := mwRangeEndAdr;
    i := sprintf(ADR(str[0]), ADR("[%08lx..%08lx]"), argo);
    SetNewWindowTitle(mw, ADR(str[0]));

    WITH mwVTWindowParms DO
      vtpInitialLine := newFirstLine;
      vtpTotalLines  := MemoryWindowTotalLines;
    END;
    IF (NOT InitParmsVTWindow(mwVTWindow, mwVTWindowParms)) THEN
      CloseMemoryWindowNoMemory(mw);
    END;
  END;
END SetMemoryWindowOnAdr;

PROCEDURE UpdateMemoryWindow(VAR mw: MemoryWindow);
BEGIN
  IF (IsProgramLoaded()) THEN
    IF (mw.mwDisplayOn) THEN
      mw.mwRedrawMemory := TRUE;
      RefreshVTWindow(mw.mwVTWindow);
    ELSE
      SetMemoryWindowParms(mw);
      SetMemoryWindowOnAdr(mw, (*0D*)GetModuleDataStart(0));
    END;
  ELSE
    ClearMemoryWindow(mw);
  END;
END UpdateMemoryWindow;

(* SetNewMemorySelection - setup new user selection based on y position *)
PROCEDURE SetNewMemorySelection(VAR mw: MemoryWindow; x, y: CARDINAL);
VAR
  data: SelectionData;
  startX: CARDINAL;
  stopX: CARDINAL;
  ok: BOOLEAN;
  col: CARDINAL;
  absAdr: ADDRESS;
  storageSize: CARDINAL;
  storageFormat: StorageFormat;
  storageNumber: StorageNumber;
BEGIN
  WITH mw DO
    absAdr := mwRangeStartAdr + LONGCARD(y * mwBytesPerLine);

    ok := FALSE;
    IF (x < AdrInfoNumberWidth) THEN
      ok            := TRUE;
      startX        := 0;
      stopX         := AdrInfoNumberWidth-1;
      storageFormat := SFNone;
    ELSIF (x >= AdrInfoWidth) THEN
      DEC(x, AdrInfoWidth);
      col := x DIV mwColumnWidth;
      IF (col < mwColumns) THEN
        ok     := TRUE;
        startX := AdrInfoWidth + (col * mwColumnWidth);
        stopX  := startX + (mwColumnWidth-1);

        INC(absAdr, col * mwValueSize);

        storageSize := mwValueSize;

        IF (mwDispFormat = DFReal) THEN
          storageFormat := SFReal;
        ELSIF (mwDispFormat = DFString) THEN
          storageFormat := SFString;
        ELSIF (mwDispFormat = DFChar) THEN
          storageFormat := SFNumber;
          storageNumber := SNChar;
        ELSIF (mwDispFormat = DFNumber) THEN
          storageFormat := SFNumber;
          IF (mwDispNumberFormat = NFBin) THEN
            storageNumber := SNBin;
          ELSIF (mwDispNumberFormat = NFOct) THEN
            storageNumber := SNOct;
          ELSIF (mwDispNumberFormat = NFHex) THEN
            storageNumber := SNHex;
          ELSIF (mwDispNumberFormat = NFDecS) THEN
            storageNumber := SNDecS;
          ELSIF (mwDispNumberFormat = NFDecU) THEN
            storageNumber := SNDecU;
          END;
        END;
      END;
    END;
  END; (* WITH *)

  IF (ok) THEN
    WITH data DO
      sdWindowClass   := WCMemory;
      sdVTWindow      := mw.mwVTWindow;
      sdLineNo        := y;
      sdStartPos      := startX;
      sdEndPos        := stopX;
      sdDefFgColor    := mw.mwVTWindowParms.vtpDefFgColor;
      sdDefBgColor    := mw.mwVTWindowParms.vtpDefBgColor;
      sdAdr           := absAdr;
      sdStorageFormat := storageFormat;
      sdStorageNumber := storageNumber;
      sdStorageSize   := storageSize;
    END;
    SetSelection(data);
  ELSE
    ClearSelection();
  END;
END SetNewMemorySelection;

(* VTUpdateProc - called to output new text to window *)
PROCEDURE VTUpdateProc(Window: VTWindow; OutputRow,FirstLine,LastLine: CARDINAL);
VAR
  mw: MemoryWindowPtr;
  line: CARDINAL;
  col: CARDINAL;
  redrawAll: BOOLEAN;
  absAdr: ADDRESS;
  value: LONGCARD;
  chValue: CHAR;
  dispFirst: CARDINAL;
  dispLast: CARDINAL;
  dispHeight: CARDINAL;
  startPos: CARDINAL;
  endPos: CARDINAL;
  outLength: CARDINAL;
  outBuf: ARRAY [0..63] OF CHAR;
  argo: ARRAY [0..9] OF FormatArg;
BEGIN
  mw := GetUserDataVTWindow(Window);
  WITH mw^ DO
    IF (mwDisplayOn) THEN
      GetDisplayRangeVTWindow(Window, dispFirst, dispLast);
      dispHeight := dispLast - dispFirst + 1;
      
      redrawAll := TRUE;

      IF (mwMemoryLookup = NIL) OR (mwHeight # dispHeight) THEN
        IF (NOT AllocMemoryLookupBuffer(mw^, dispHeight)) THEN
          ShowError(ShowErrMsgNoMemory);
          RETURN; (* Abort! *)
        END;
        mwFirstLine := dispFirst;
      ELSIF (mwFirstLine # dispFirst) THEN

        (*-------------------- NOT USED --------------------
        IF (dispFirst <= mwFirstLine) AND (dispLast >= mwFirstLine) THEN
          ScrollMemoryLookupBuffer(mw^, dispFirst - mwFirstLine);
        END;
        -------------------- NOT USED ----------------------*)

        mwFirstLine := dispFirst;
      ELSIF (mwRedrawMemory) THEN
        redrawAll := FALSE;
      END;

      absAdr := mwRangeStartAdr + LONGCARD(FirstLine * mwBytesPerLine);

      FOR line := FirstLine TO LastLine DO
        IF (CompareOrStoreMemory(mw^, OutputRow, absAdr, redrawAll)) THEN
          INC(absAdr, mwBytesPerLine);
        ELSE
          SetCursorPosVTWindow(Window, 0, OutputRow);
          argo[0].L := absAdr;
          outLength := sprintf(ADR(outBuf[0]), ADR("%08lx: "), argo);
          WriteBufferVTWindow(Window, ADR(outBuf[0]), AdrInfoWidth);

          IF (mwDispFormat = DFString) THEN
            WriteBufferVTWindow(Window, absAdr, mwBytesPerLine);
            INC(absAdr, mwBytesPerLine);
          ELSE
            FOR col := 0 TO mwColumns-1 DO
              value := GetMemoryValue(absAdr, FALSE, mwValueSize, NIL);
              INC(absAdr, mwValueSize);
              IF (mwDispFormat = DFReal) THEN              
                argo[0].L := value;
                outLength := sprintf(ADR(outBuf[0]), ADR("%12g"), argo);
              ELSIF (mwDispFormat = DFChar) THEN
                chValue := CHAR(value);
                IF (chValue >= " ") AND (chValue <= "~") THEN
                  argo[0].B := chValue;
                  outLength := sprintf(ADR(outBuf[0]), ADR("'%c'"), argo);
                ELSE
                  argo[0].W := CARDINAL(value);
                  outLength := sprintf(ADR(outBuf[0]), ADR("%3u"), argo);
                END;
              ELSIF (mwDispFormat = DFNumber) THEN
                IF (mwDispNumberFormat = NFDecS) THEN
                  IF (mwDispNumberSize = NSByte) AND (value > 127D) THEN
                    INC(value, 0FFFFFF00H);
                  ELSIF (mwDispNumberSize = NSWord) AND (value > 32767D) THEN
                    INC(value, 0FFFF0000H);
                  END;
                END;
                argo[0].W := FieldWidthNumberFormat[mwDispNumberFormat,mwDispNumberSize];
                argo[1].L := value;
                outLength := sprintf(ADR(outBuf[0]), FmtStringNumberFormat[mwDispNumberFormat], argo);
              END;

              outBuf[outLength] := " ";
              INC(outLength);
              WriteBufferVTWindow(Window, ADR(outBuf[0]), outLength);
            END;
          END;
        END;

        IF (GetSelectionLineInfo(Window, line, startPos, endPos)) THEN
          WriteColorVTWindow(Window, OutputRow, startPos, endPos,
                             mwVTWindowParms.vtpDefBgColor,
                             mwVTWindowParms.vtpDefFgColor);
        END;

        INC(OutputRow);
      END;

      mwRedrawMemory := FALSE;
    END;
  END;
END VTUpdateProc;

(* VTSelectProc - called when left mouse button click in window *)
PROCEDURE VTSelectProc(Window: VTWindow; x, y: CARDINAL; pressed: BOOLEAN);
VAR
  mw: MemoryWindowPtr;
BEGIN
  mw := GetUserDataVTWindow(Window);

  IF (mw^.mwDisplayOn) THEN
    IF (pressed) THEN
      IF (IsPosSelected(Window, x, y)) THEN
        ClearSelection();
      ELSE
        SetNewMemorySelection(mw^, x, y);
      END;
    ELSE
      IF (NOT IsPosSelected(Window, x, y)) THEN
        SetNewMemorySelection(mw^, x, y);
      END;
    END;
  END;
END VTSelectProc;

(* VTMenuProc - called when menu selection made *)
PROCEDURE VTMenuProc(Window: VTWindow; MenuNum, ItemNum, SubNum: CARDINAL);
VAR
  mw: MemoryWindowPtr;
  redraw: BOOLEAN;
  fg: BYTE;
  bg: BYTE;
  absAdr: ADDRESS;
  numberFormat: BOOLEAN;
BEGIN
  mw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalMenuCommand(MenuNum, ItemNum, SubNum, LocalMenus,
                              WCMemory, GetWindowIndex(mw),
                              mw^.mwVTWindow)) THEN
    WITH mw^ DO
      IfCurrentWindowClearSelection(mwVTWindow);

      redraw := FALSE;
      absAdr := mwRangeStartAdr + LONGCARD(mwFirstLine * mwBytesPerLine);

      numberFormat := mwDispFormat = DFNumber;

      CASE (ItemNum) OF
      | 0: (* Show Real *)
           mwDispFormat := DFReal;
           redraw := TRUE;
      | 1: (* Show String *)
           mwDispFormat := DFString;
           redraw := TRUE;
      | 2: (* Show Char *)
           mwDispFormat := DFChar;
           redraw := TRUE;
      | 3: (* Show Number *)
           mwDispFormat := DFNumber;
           redraw := TRUE;
      (*4: -------------------- *)
      | 5: (* Byte *)
           mwDispNumberSize := NSByte;
           redraw := numberFormat;
      | 6: (* Word *)
           mwDispNumberSize := NSWord;
           redraw := numberFormat;
      | 7: (* Long *)
           mwDispNumberSize := NSLong;
           redraw := numberFormat;
      (*8: -------------------- *)
      | 9: (* Binary *)
           mwDispNumberFormat := NFBin;
           redraw := numberFormat;
      |10: (* Octal *)
           mwDispNumberFormat := NFOct;
           redraw := numberFormat;
      |11: (* Hexadecimal *)
           mwDispNumberFormat := NFHex;
           redraw := numberFormat;
      |12: (* Decimal Signed *)
           mwDispNumberFormat := NFDecS;
           redraw := numberFormat;
      |13: (* Decimal Unsigned *)
           mwDispNumberFormat := NFDecU;
           redraw := numberFormat;
      (*14: -------------------- *)
      |15: (* Set Text Width *)
           redraw := RequestTextWidth(mwMaxColumns);
      |16: (* Set Window Color *)
           GetDetailPenBlockPenVTWindow(mwVTWindow, fg, bg);
           IF (RequestTextFgBgColors(fg, bg)) THEN
             SetDetailPenBlockPenVTWindow(mwVTWindow, fg, bg);
           END;
      |17: (* Set Text Color *)
           WITH mwVTWindowParms DO
             IF (RequestTextFgBgColors(vtpDefFgColor, vtpDefBgColor)) THEN
               SetDefaultColorVTWindow(mwVTWindow, vtpDefFgColor,vtpDefBgColor);
             END;
           END;
      END;
      IF (redraw) AND (mwDisplayOn) THEN
        SetMemoryWindowParms(mw^);
        SetMemoryWindowOnAdr(mw^, absAdr);
      END;
    END;
  END;
END VTMenuProc;

(* VTDestProc - called when user clicks on destination-data gadget *)
PROCEDURE VTDestProc(Window: VTWindow);
VAR
  mw: MemoryWindowPtr;
  data: SelectionData;
  pcn: ProcChainNodePtr;
  absAdr: ADDRESS;
  err: BOOLEAN;
  b: BOOLEAN;
BEGIN
  mw := GetUserDataVTWindow(Window);

  IF (IsProgramLoaded()) THEN
    err    := FALSE;
    absAdr := InvalidAbsAdr;
    IF (GetSelection(data)) THEN
      WITH data DO
        IF (sdWindowClass = WCSource) OR (sdWindowClass = WCDisassembly) OR
           (sdWindowClass = WCSymData) OR (sdWindowClass = WCMemory) OR
           (sdWindowClass = WCBreakPointList) OR (sdWindowClass = WCRegisters) THEN
          absAdr := sdAdr;
          err    := absAdr = InvalidAbsAdr; (* Invalid Selection *)
        ELSIF (sdWindowClass = WCModuleList) THEN
          IF (sdModFileType = MFTSbm) OR (sdModFileType = MFTRfm) OR
             (sdModFileType = MFTNone) THEN
            SetMemoryWindowOnAdr(mw^, GetModuleDataStart(sdModNo));
          ELSE (* Invalid ModFileType *)
            err := TRUE;
          END;
        ELSIF (sdWindowClass = WCProcChain) THEN
          pcn := GetProcChainNode(sdProcChainNo);
          WITH pcn^ DO
            IF (pcnIsProc) THEN
              IF (pcnLocalAlloc) THEN
                SetMemoryWindowOnAdr(mw^, pcn^.pcnDataBase);
              END;
            ELSE
              SetMemoryWindowOnAdr(mw^, GetModuleDataStart(pcn^.pcnModNo));
            END;
          END;
        ELSE (* Invalid Destination *)
          err := TRUE;
        END;
      END;
    ELSE (* REQUESTER *)
      b := RequestAddress(0D, absAdr);
    END;
  
    IF (absAdr # InvalidAbsAdr) THEN
      IF (IsQualifierShiftVTWindow(Window)) THEN
        absAdr := GetMemoryLong(absAdr);
      END;
      SetMemoryWindowOnAdr(mw^, absAdr);
    END;
  
    IF (err) THEN
      ShowError(ShowErrMsgInvalidDest);
    END;
  END; (* IF *)
END VTDestProc;

(* VTKeyProc - called when user types key into window *)
PROCEDURE VTKeyProc(Window: VTWindow; keyBuf: ADDRESS; keyLength: CARDINAL);
VAR
  mw: MemoryWindowPtr;
  ch: CHAR;
  absAdr: ADDRESS;
  first: CARDINAL;
  last: CARDINAL;
BEGIN
  mw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalKbdCommand(keyBuf, keyLength, WCMemory,
                            GetWindowIndex(mw))) THEN
    IF (mw^.mwDisplayOn) THEN
      IF (keyLength = 2) THEN
        IF (CHAR(keyBuf^) = CSI) THEN
          INC(keyBuf);
          ch := CHAR(keyBuf^);
  
          WITH mw^ DO
            absAdr := mwRangeStartAdr + LONGCARD(mwFirstLine * mwBytesPerLine);
          END;
  
          IF (ch = LeftArrowKey) THEN (* Left Arrow (Decrement Address) *)
            IF (absAdr > 0D) THEN
              SetMemoryWindowOnAdr(mw^, absAdr - 1D);
            END;
          ELSIF (ch = RightArrowKey) THEN (* Right Arrow (Increment Address) *)
            GetDisplayRangeVTWindow(mw^.mwVTWindow, first, last);
            IF (mw^.mwRangeEndAdr # 0FFFFFFFFH) OR 
               (last < MemoryWindowTotalLines - 1) THEN
              SetMemoryWindowOnAdr(mw^, absAdr + 1D);
            END;
          END;
        END;
      END;
    END;
  END;
END VTKeyProc;

(* VTCloseProc - called when close-box hit *)
PROCEDURE VTCloseProc(Window: VTWindow);
VAR
  mw: MemoryWindowPtr;
BEGIN
  mw := GetUserDataVTWindow(Window);
  CloseMemoryWindow(mw^);
END VTCloseProc;

(* VTErrorProc - called when failed to alloc text info for resized window *)
PROCEDURE VTErrorProc(Window: VTWindow);
VAR
  mw: MemoryWindowPtr;
BEGIN
  mw := GetUserDataVTWindow(Window);
  CloseMemoryWindowNoMemory(mw^);
END VTErrorProc;

(* ===== PUBLIC ===== *)

(* OpenMemoryWindow - open a memory window *)
PROCEDURE OpenMemoryWindow(VAR config: ConfigWindow): BOOLEAN;
VAR
  mw: MemoryWindowPtr;
  ok: BOOLEAN;
  nvt: NewVTWindow;
BEGIN
  ok := FALSE;
  mw := AddWindowNode();
  IF (mw # NIL) THEN
    WITH nvt DO
      WITH config DO
        nvtLeftEdge  := cwX;
        nvtTopEdge   := cwY;
        nvtWidth     := cwWidth;
        nvtHeight    := cwHeight;
        nvtDetailPen := cwDetailPen;
        nvtBlockPen  := cwBlockPen;
        nvtTitle     := ADR(DefMemoryWindowTitle);
        nvtMinWidth  := DefMinWindowWidth;
        nvtMinHeight := DefMinWindowHeight;
        nvtMaxWidth  := DefMaxWindowWidth;
        nvtMaxHeight := DefMaxWindowHeight;
        nvtMsgPort   := GetVTWindowsMsgPort();
        nvtUserData  := mw;
      END; (* WITH *)
    END; (* WITH *)
    mw^.mwVTWindow := OpenVTWindow(nvt);
    ok := mw^.mwVTWindow # VTWindowNIL;

    IF (ok) THEN
      WITH mw^.mwVTWindowParms DO
        WITH config DO
          vtpUpdateProc    := VTUpdateProc;
          vtpSelectProc    := VTSelectProc;
          vtpMenuProc      := VTMenuProc;
          vtpDestProc      := VTDestProc;
          vtpKeyProc       := VTKeyProc;
          vtpCloseProc     := VTCloseProc;
          vtpErrorProc     := VTErrorProc;
        (*vtpInitialLine   := 0;*)
        (*vtpTotalLines    := 1;*)
        (*vtpInitialColumn := 0;*)
        (*vtpMaxColumns    := 1;*)
          vtpDefFgColor    := cwFgColor;
          vtpDefBgColor    := cwBgColor;
        END;
      END;

      WITH mw^ DO
        WITH config DO
          mwDisplayOn        := TRUE;
          mwRedrawMemory     := FALSE;
          mwMaxColumns       := cwMaxCharWidth;
          mwDispFormat       := DispFormat(cwMemoryDispFormat);
          mwDispNumberSize   := NumberSize(cwMemoryNumberSize);
          mwDispNumberFormat := NumberFormat(cwMemoryNumberFormat);
        END;
      END;

      ClearMemoryWindow(mw^);
      UpdateMemoryWindow(mw^);

      ok := CreateMenuStrip(mw^);
    ELSE (* NOT ok *)
      DeleteWindowNode(mw);
    END;
  END;
  RETURN (ok);
END OpenMemoryWindow;

(* UpdateMemoryWindows - update contents of all memory windows *)
PROCEDURE UpdateMemoryWindows();
VAR
  mw: MemoryWindowPtr;
  p: MemoryWindowPtr;
BEGIN
  mw := MemoryWindowList;
  WHILE (mw # NIL) DO
    p  := mw;
    mw := mw^.mwNext;
    UpdateMemoryWindow(p^);
  END;
END UpdateMemoryWindows;

(* CloseMemoryWindows - close all memory windows *)
PROCEDURE CloseMemoryWindows();
BEGIN
  WHILE (MemoryWindowList # NIL) DO
    CloseMemoryWindow(MemoryWindowList^);
  END;
END CloseMemoryWindows;

(* GetMemoryWindowInfo - get config parameters of a specified window *)
PROCEDURE GetMemoryWindowInfo(VAR index: CARDINAL;
                              VAR config: ConfigWindow): BOOLEAN;
VAR
  mw: MemoryWindowPtr;
  count: CARDINAL;
BEGIN
  count := index;
  mw    := MemoryWindowList;
  WHILE (mw # NIL) DO
    IF (count = 0) THEN
      WITH config DO
        WITH mw^ DO
          GetInfoVTWindow(mwVTWindow, cwX, cwY, cwWidth, cwHeight,
                          cwDetailPen, cwBlockPen);
          cwMaxCharWidth           := mwMaxColumns;
          cwFgColor                := mwVTWindowParms.vtpDefFgColor;
          cwBgColor                := mwVTWindowParms.vtpDefBgColor;
          cwMemoryDispFormat       := mwDispFormat;
          cwMemoryNumberSize       := mwDispNumberSize;
          cwMemoryNumberFormat     := mwDispNumberFormat;
        END;
      END;
      INC(index);
      RETURN (TRUE); (* Window Found! *)
    END;
    DEC(count);
    mw := mw^.mwNext;
  END;
  RETURN (FALSE); (* End of list *)
END GetMemoryWindowInfo;

(* InitDBMemoryWindows - initialize module *)
PROCEDURE InitDBMemoryWindows(): BOOLEAN;
BEGIN
  ValueSizeNumberFormat[NSByte] := 1;
  ValueSizeNumberFormat[NSWord] := 2;
  ValueSizeNumberFormat[NSLong] := 4;

  ColumnWidthNumberFormat[NFBin,  NSByte] := 10; (* %11111111 *)
  ColumnWidthNumberFormat[NFOct,  NSByte] :=  5; (* @377 *)
  ColumnWidthNumberFormat[NFHex,  NSByte] :=  4; (* $FF  *)
  ColumnWidthNumberFormat[NFDecS, NSByte] :=  5; (* -128 *)
  ColumnWidthNumberFormat[NFDecU, NSByte] :=  4; (* 255  *)

  ColumnWidthNumberFormat[NFBin,  NSWord] := 18; (* %1111111111111111 *)
  ColumnWidthNumberFormat[NFOct,  NSWord] :=  8; (* @177777 *)
  ColumnWidthNumberFormat[NFHex,  NSWord] :=  6; (* $FFFF   *)
  ColumnWidthNumberFormat[NFDecS, NSWord] :=  7; (* -32768  *)
  ColumnWidthNumberFormat[NFDecU, NSWord] :=  6; (* 65535   *)

  ColumnWidthNumberFormat[NFBin,  NSLong] := 34; (* %11111111111111111111111111111111 *)
  ColumnWidthNumberFormat[NFOct,  NSLong] := 13; (* @37777777777 *)
  ColumnWidthNumberFormat[NFHex,  NSLong] := 10; (* $FFFFFFFF    *)
  ColumnWidthNumberFormat[NFDecS, NSLong] := 12; (* -2147483648  *)
  ColumnWidthNumberFormat[NFDecU, NSLong] := 11; (* 4294967295   *)

  FieldWidthNumberFormat[NFBin,  NSByte] := 8; (* %11111111 *)
  FieldWidthNumberFormat[NFOct,  NSByte] := 3; (* @377 *)
  FieldWidthNumberFormat[NFHex,  NSByte] := 2; (* $FF  *)
  FieldWidthNumberFormat[NFDecS, NSByte] := 4; (* -128 *)
  FieldWidthNumberFormat[NFDecU, NSByte] := 3; (* 255  *)

  FieldWidthNumberFormat[NFBin,  NSWord] := 16; (* %1111111111111111 *)
  FieldWidthNumberFormat[NFOct,  NSWord] :=  6; (* @177777 *)
  FieldWidthNumberFormat[NFHex,  NSWord] :=  4; (* $FFFF   *)
  FieldWidthNumberFormat[NFDecS, NSWord] :=  6; (* -32768  *)
  FieldWidthNumberFormat[NFDecU, NSWord] :=  5; (* 65535   *)

  FieldWidthNumberFormat[NFBin,  NSLong] := 32; (* %11111111111111111111111111111111 *)
  FieldWidthNumberFormat[NFOct,  NSLong] := 11; (* @37777777777 *)
  FieldWidthNumberFormat[NFHex,  NSLong] := 8;  (* $FFFFFFFF    *)
  FieldWidthNumberFormat[NFDecS, NSLong] := 11; (* -2147483648  *)
  FieldWidthNumberFormat[NFDecU, NSLong] := 10; (* 4294967295   *)

  FmtStringNumberFormat[NFBin]  := ADR("%%%0*lb");
  FmtStringNumberFormat[NFOct]  := ADR("@%0*lo");
  FmtStringNumberFormat[NFHex]  := ADR("$%0*lx");
  FmtStringNumberFormat[NFDecS] := ADR("%*ld");
  FmtStringNumberFormat[NFDecU] := ADR("%*lu");

  RETURN (TRUE);
END InitDBMemoryWindows;

(* CleanupDBMemoryWindows - cleanup module *)
PROCEDURE CleanupDBMemoryWindows();
BEGIN
  CloseMemoryWindows();
END CleanupDBMemoryWindows;

END DBMemoryWindows.
