(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                      (c) 1986, 1987 by Jim Olinger                       *
 ****************************************************************************
 * Name: SimpleMenus.MOD                   Version: Amiga.00.00             *
 * Created: 04/03/87   Updated: 03/01/89   Author: Leon Frenkel             *
 * Description: Simplified Intuition Menu functions.                        *
 *                                                                          * 
 * This version of the module has been modified to handle switch of window  *
 * colors.                                                                  *
 ****************************************************************************)

IMPLEMENTATION MODULE SimpleMenus;

FROM SYSTEM IMPORT
  ADDRESS, BYTE, 
  ADR, TSIZE;
FROM GraphicsBase IMPORT
  GfxBasePtr;
FROM Intuition IMPORT
  Menu, MenuPtr,
  MenuItem, MenuItemPtr,	
  MenuItemFlags, MenuItemFlagsSet,
  MenuItemMutualExcludeSet,
  IntuiText, IntuiTextPtr,
  MenuFlags, MenuFlagsSet,
  CheckWidth, LowCheckWidth, CommWidth, LowCommWidth,
  HighComp,
  IntuiTextLength;
FROM Memory IMPORT
  AllocMem, FreeMem, MemReqSet, MemClear;
FROM Rasters IMPORT
  Jam1, Complement;
FROM System IMPORT
  GfxBase;

CONST
  MenuFrontPen = BYTE(0);
  MenuBackPen  = BYTE(1);
  MenuDrawMode = Jam1;

TYPE
  MemBlockPtr = POINTER TO MemBlock;
  MemBlock = RECORD
               Next : MemBlockPtr;
               Size : CARDINAL;
             END;


CONST
  ScreenTitleBarHeight = 10;
  LastRowBottomOffset = 20;

VAR
  gfx : GfxBasePtr;		(* ptr to graphics.library base *)

VAR
  MemAllocList   : MemBlockPtr;	(* list of structures currently allocated *)
  MemListTail    : MemBlockPtr;

  MemoryOk       : BOOLEAN; (* TRUE = memory ok, FALSE = ran out of memory *)

  MenuHead       : MenuPtr; (* head of Menu list *)
  MenuTail       : MenuPtr; (* tail of Menu list *)
  MenuLeftEdge   : INTEGER; (* next menu header goes at this position *)

  ItemFlags      : MenuItemFlagsSet;

  ItemHead       : MenuItemPtr; (* head of MenuItem list *)
  ItemTail       : MenuItemPtr; (* tail of MenuItem list *)
  ItemLeftColumn : INTEGER;     (* column # for next item *)
  ItemLeftOffset : INTEGER;     (* current left offset *)
  ItemMaxWidth   : INTEGER;     (* the largest item width in current menu *)
  ItemTopEdge    : INTEGER;     (* next menu item goes at this vertical ofs *)
  ItemTopBase    : INTEGER;     (* base for current TopEdge *)
  ItemTopOffset  : INTEGER;     (* offset from menu header *)
  ItemCheckFlag  : BOOLEAN;	(* TRUE = check mark, FALSE = no check mark *)

  (* Temporary storage for variables while working on sub-menu *)
  MainItemHead       : MenuItemPtr;
  MainItemTail       : MenuItemPtr;
  MainItemLeftColumn : INTEGER;
  MainItemLeftOffset : INTEGER;
  MainItemMaxWidth   : INTEGER;
  MainItemTopEdge    : INTEGER;
  MainItemTopBase    : INTEGER;
  MainItemTopOffset  : INTEGER;
  MainItemCheckFlag  : BOOLEAN;

  CurCheckWidth  : INTEGER; (* the CheckMark is of this width *)
  CurCommWidth   : INTEGER; (* the Command is of this width   *)
  CurItemLastRow : INTEGER; (* check if crossed and should begin new column *)



(* ====== PRIVATE ======= *)
PROCEDURE Free(blockList: MemBlockPtr);
VAR
  block : MemBlockPtr;
BEGIN
  WHILE (blockList # NIL) DO
    block := blockList;
    blockList := blockList^.Next;
    FreeMem(block, block^.Size);
  END;
END Free;

PROCEDURE Alloc(size: CARDINAL): ADDRESS;
VAR
  block : MemBlockPtr;
BEGIN
  IF MemoryOk THEN
    INC(size, TSIZE(MemBlock));
    block := AllocMem(size, MemReqSet{MemClear});
    IF block # NIL THEN
      block^.Next := MemAllocList;
      block^.Size := size;
      MemAllocList := block;
    ELSE
      Free(MemAllocList);
      MemoryOk := FALSE;
    END;
  END;
  RETURN (ADDRESS(block) + LONGCARD(TSIZE(MemBlock)));
END Alloc;

PROCEDURE strlen(str: ADDRESS): CARDINAL;
VAR 
  l : CARDINAL;
BEGIN
  l := 0;
  WHILE (CHAR(str^) # 0C) DO
    INC(str);
    INC(l);
  END;
  RETURN (l);
END strlen;

PROCEDURE PositionItems;
VAR
  il : MenuItemPtr;
  it : IntuiTextPtr;
  left : INTEGER;
BEGIN
  left := 0;
  IF ItemCheckFlag THEN
    left := CurCheckWidth;
  END;
  il := ItemHead;
  WHILE (il # NIL) DO
    il^.LeftEdge := (il^.LeftEdge * ItemMaxWidth) + il^.Width;
    il^.Width := ItemMaxWidth;
    it := il^.ItemFill;
    it^.LeftEdge := left;
    il := il^.NextItem;
  END;
END PositionItems;


(* ====== PUBLIC ======== *)

PROCEDURE AddMenu(name: ADDRESS);
VAR
  newMenu : MenuPtr;
  testText : IntuiText;
  nameWidth : INTEGER;
BEGIN
  WITH testText DO
    FrontPen  := MenuFrontPen;
    BackPen   := MenuBackPen;
    DrawMode  := MenuDrawMode;
    LeftEdge  := 0;
    TopEdge   := 0;
    ITextFont := NIL;
    IText     := name;
    NextText  := NIL;
  END;
  nameWidth := IntuiTextLength(testText);

  newMenu := Alloc(TSIZE(Menu));
  IF MemoryOk THEN
    WITH newMenu^ DO
      NextMenu  := NIL;
      LeftEdge  := MenuLeftEdge;
      TopEdge   := 0;
      Width     := nameWidth;
      Height    := 0;
      Flags     := MenuFlagsSet{MenuEnabled};
      MenuName  := name;
      FirstItem := NIL;
    END;

    IF MenuHead = NIL THEN
      MenuHead := newMenu;
      MemListTail := ADR(newMenu^) - LONGCARD(TSIZE(MemBlock));
    ELSE
      MenuTail^.NextMenu := newMenu;
      MenuTail^.FirstItem := ItemHead;
      PositionItems;
    END;
    MenuTail := newMenu;

    INC(MenuLeftEdge, nameWidth);

    ItemHead       := NIL;
    ItemLeftColumn := 0;
    ItemMaxWidth   := nameWidth;
    ItemLeftOffset := 0;
    ItemTopEdge    := 0;
    ItemTopBase    := ScreenTitleBarHeight;
    ItemTopOffset  := 0;
    ItemCheckFlag  := FALSE;
  END;
END AddMenu;

PROCEDURE AddMenuItem(name: ADDRESS);
VAR
  newItem : MenuItemPtr;
  newText : IntuiTextPtr;
  textWidth : INTEGER;
BEGIN
  newItem := Alloc(TSIZE(MenuItem));
  newText := Alloc(TSIZE(IntuiText));
  IF MemoryOk THEN
    WITH newText^ DO
      FrontPen  := MenuFrontPen;
      BackPen   := MenuBackPen;
      DrawMode  := MenuDrawMode;
      LeftEdge  := 0;
      TopEdge   := 0;
      ITextFont := MenuItemTextAttr;
      IText     := name;
      NextText  := NIL;
    END;
    textWidth := IntuiTextLength(newText^);

    IF CheckIt IN ItemFlags THEN
      ItemCheckFlag := TRUE;
      INC(textWidth, CurCheckWidth);
    END;

    (* Test if item larger than any previous item? *)
    IF textWidth > ItemMaxWidth THEN
      ItemMaxWidth := textWidth;
    END;

    (* Check if we should begin a new column? *)
    IF (ItemTopBase + ItemTopEdge) > CurItemLastRow THEN
      ItemTopEdge := ItemTopOffset;
      INC(ItemLeftOffset, NextItemColumnOffset);
      INC(ItemLeftColumn);
    END;

    WITH newItem^ DO
      NextItem      := NIL;
      LeftEdge      := ItemLeftColumn;
      TopEdge       := ItemTopEdge;
      Width         := ItemLeftOffset;
      Height        := MenuItemHeight;
      Flags         := ItemFlags;
      MutualExclude := MenuItemMutualExcludeSet{};
      ItemFill      := newText;
      SelectFill    := NIL;
      Command       := BYTE(0);
      SubItem       := NIL;
      (* NextSelect *)
    END;

    INC(ItemTopEdge, MenuItemHeight);

    IF ItemHead = NIL THEN
      ItemHead := newItem;
    ELSE
      ItemTail^.NextItem := newItem;
    END;
    ItemTail := newItem;
  END;
END AddMenuItem;

PROCEDURE BeginMenuStrip;
BEGIN
  (* Initialize internal variables based on global parameters *)
  IF ScreenHires THEN
    CurCheckWidth := CheckWidth;
    CurCommWidth  := 37; (* CommWidth *)
    MenuLeftEdge  := 0;
  ELSE
    CurCheckWidth := LowCheckWidth;
    CurCommWidth  := 27; (* LowCommWidth *)
    MenuLeftEdge  := 2;
  END;
  IF ScreenLace THEN
    CurItemLastRow := (gfx^.NormalDisplayRows * 2) - LastRowBottomOffset;
  ELSE
    CurItemLastRow := (gfx^.NormalDisplayRows) - LastRowBottomOffset;
  END;
  IF MenuItemLastRow > 0 THEN
    CurItemLastRow := MenuItemLastRow;
  END;
  IF CheckImageWidth > 0 THEN
    CurCheckWidth := CheckImageWidth;
  END;

  MenuHead := NIL;
  ItemHead := NIL;
  MemAllocList := NIL;
  ItemLeftOffset := 0;
  ItemFlags      := DefaultItemFlags;
  MemoryOk       := TRUE;

END BeginMenuStrip;

PROCEDURE BeginSubMenu(leftEdge, topEdge: INTEGER);
BEGIN
  (* preserve menu variables *)
  MainItemHead       := ItemHead;
  MainItemTail       := ItemTail;
  MainItemLeftColumn := ItemLeftColumn;
  MainItemLeftOffset := ItemLeftOffset;
  MainItemMaxWidth   := ItemMaxWidth;
  MainItemTopEdge    := ItemTopEdge;
  MainItemTopBase    := ItemTopBase;
  MainItemTopOffset  := ItemTopOffset;
  MainItemCheckFlag  := ItemCheckFlag;

  ItemHead       := NIL;
  ItemLeftColumn := 0;
  ItemLeftOffset := leftEdge;
  ItemMaxWidth   := 0;
  ItemTopEdge    := topEdge;
  ItemTopBase    := MainItemTopBase + MainItemTopEdge - MenuItemHeight;
  ItemTopOffset  := topEdge;
  ItemCheckFlag  := FALSE;
END BeginSubMenu;

PROCEDURE EndMenuStrip(): MenuPtr;
BEGIN
  IF MemoryOk THEN
    MenuTail^.FirstItem := ItemHead;
    PositionItems;

    MemListTail^.Next := MemAllocList;

    RETURN (MenuHead);
  ELSE
    RETURN (NIL);
  END;
END EndMenuStrip;

PROCEDURE EndSubMenu;
BEGIN
  PositionItems;
  MainItemTail^.SubItem := ItemHead;
  ItemHead       := MainItemHead;
  ItemTail       := MainItemTail;
  ItemLeftColumn := MainItemLeftColumn;
  ItemLeftOffset := MainItemLeftOffset;
  ItemMaxWidth   := MainItemMaxWidth;
  ItemTopEdge    := MainItemTopEdge;
  ItemTopBase    := MainItemTopBase;
  ItemTopOffset  := MainItemTopOffset;
  ItemCheckFlag  := MainItemCheckFlag;
END EndSubMenu;

PROCEDURE FreeMenuStrip(VAR menu: Menu);
VAR
  start, end : MemBlockPtr;
BEGIN
  end := ADR(menu) - LONGCARD(TSIZE(MemBlock));
  start := end^.Next;
  end^.Next := NIL;
  Free(start);
END FreeMenuStrip;

PROCEDURE GlobalMenuItemOpt(flags: MenuItemFlagsSet);
BEGIN
  ItemFlags := flags;
END GlobalMenuItemOpt;


PROCEDURE MenuItemOpt(flags: MenuItemFlagsSet;
                      exclude: MenuItemMutualExcludeSet;
                      kbdCommand: CHAR);
VAR
  textWidth : INTEGER;
  it : IntuiTextPtr;
BEGIN
  IF (MemoryOk) THEN
    it := ItemTail^.ItemFill;
    textWidth := IntuiTextLength(it^);

    IF (CheckIt IN flags) THEN
      ItemCheckFlag := TRUE;
      INC(textWidth, CurCheckWidth);
    END;

    IF CommSeq IN flags THEN
      INC(textWidth, CurCommWidth);
    END;

    (* Test if item larger than any previous item? *)
    IF textWidth > ItemMaxWidth THEN
      ItemMaxWidth := textWidth;
    END;

    WITH ItemTail^ DO
      Flags         := flags;
      MutualExclude := exclude;
      Command       := kbdCommand;
    END;
  END; (* MemoryOk *)
END MenuItemOpt;

PROCEDURE MenuOpt(enabled: BOOLEAN; leftEdge, topEdge: INTEGER);
BEGIN
  IF (MemoryOk) THEN
    IF NOT enabled THEN
      MenuTail^.Flags := MenuFlagsSet{};
    END;
    ItemLeftOffset := leftEdge;
    ItemTopEdge    := topEdge;
    ItemTopOffset  := topEdge;
  END; (* MemoryOk *)
END MenuOpt;

BEGIN
  gfx := GfxBase;

  ScreenHires          := TRUE;
(*ScreenLace           := FALSE;*)
(*MenuItemLastRow      := 0;*)
(*CheckImageWidth      := 0;*)
  DefaultItemFlags     := MenuItemFlagsSet{ItemText, ItemEnabled} + HighComp;
  MenuItemHeight       := 9;
  NextItemColumnOffset := 12;
(*MenuItemTextAttr     := NIL;*)
END SimpleMenus.
