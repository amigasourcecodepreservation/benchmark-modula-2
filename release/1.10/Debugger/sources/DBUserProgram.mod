
(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBUserProgram.MOD                 Version: Amiga.01.10             *
 * Created: 01/07/89   Updated: 03/21/94   Author: Leon Frenkel             *
 * Description: Debugger user program handling module.                      *
 ****************************************************************************)

IMPLEMENTATION MODULE DBUserProgram;

(***************************************************************************
 * Changes for new release 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.2 -----
 * 
 * Centralize Amiga Runtime Environment Sense and Inquiry Code
 * 
 *    1. Changed to use DBEnvironmentInquiry's WhichCPU variable to test
 *       for 68000 vs higher CPU.
 * 
 * BUGFIX: Crash if CPU Cache Enabled on M68040
 * 
 *    1. IMPORTing variable OSVersion2 boolean from DBEnvironmentInquiry.def
 * 
 *    2. CacheClearU() called:
 * 
 *          In InitProcess() at (* Restore patched location with original data *)
 * 
 *          In RunProgramAsSLD() at (* Patch first instruction of program to
 *          jump to InitProcess proc *)
 * 
 * Support for 68020+ Code
 * 
 *    1. In DBUserProgram.MOD, IsInstProcEnd() now checking for RTD as well as
 *       RTS and JMP (A0).
 * 
 *               ----- Beta.3 -----
 * 
 * Improve User Error Messages
 * 
 *    1. In RunProgramAsSLD(), added MainModFileStatus, MainRfmFileStatus vars
 *       as params to FindAvailableFiles(). Replaced ErrMsgNoMainStartup with
 *       a number of more specific error messages based on the values of these
 *       status variables.
 * 
 *    2. In RunProgramAsPMD(), currently ignoring the MainModFileStatus,
 *       and MainRfmFileStatus values from FindAvailableFiles().
 * 
 *    3. Added new Status message, "StatusLoading". Displaying at
 *       top of RunProgramAsSLD().
 * 
 *               ----- Beta.4 -----
 * 
 * Support ReadArgs()
 * 
 *    1. Importing TagItem from Utillity.
 * 
 *    2. RunProgramAsSLD() has a local Tags[] variable.
 * 
 *    3. SetupProgEnvironment() now returns another parameter, Tags array, which
 *       it initializes appropriated for CreateNewProc.
 * 
 *       This fills tags for NPCurrentDir, NPInput, NPOutput, NPArguments,
 *       NPCloseInput(FALSE), NPCloseOutput(FALSE).
 * 
 *    4. If OSVersion2, calling CreateNewProc(..., Tags[]) instead of CreateProc.
 * 
 *       Before the call, filling tags for NPSeglist, NPFreeSeglist, NPStackSize,
 *       NPName, NPPriority.
 * 
 *    5. InitProcess() now does not fill .prCurrentDir, .prCIS, .prCOS process
 *       fields if OSVersion2.
 * 
 *    6. CleanUpProcess() now does not restore .prCurrentDir, .prCIS, .prCOS
 *       process fields if OSVersion2.
 * 
 *       NOTE: Since M2Debug is currently build with the AmigaDOS v1.3 library
 *             interface, the CreateNewProc is called via SETREG() and INLINE()
 *             statements.
 * 
 ***************************************************************************
*)

FROM SYSTEM IMPORT
  ADDRESS,
  ADR, INLINE, REG, SETREG;
FROM AmigaDOS IMPORT
  SharedLock,
  Lock, UnLock, Open, Close, ModeOldFile, ModeNewFile;
FROM AmigaDOSExt IMPORT
  CommandLineInterfacePtr;
FROM AmigaDOSProcess IMPORT
  ProcessPtr,
  CreateProc(*, CreateNewProc*);  (* CreateNewProc called INLINE() for now *)
FROM AmigaDOSTags IMPORT
  NPArguments, NPCloseInput, NPCloseOutput, NPCurrentDir, NPFreeSeglist,
  NPInput, NPName, NPOutput, NPPriority, NPSeglist, NPStackSize;
FROM CStrings IMPORT
  strcpy, strcat;
FROM DBBreakPoints IMPORT
  InstallBreakPoints, RemoveBreakPoints, StopOnBreakPoint, 
  IsAdrEnabledBreakPoint, CreateTempBreakPoint, DeleteTempBreakPoint,
  DeleteBreakPoints;
FROM DBConfiguration IMPORT
  ConfigProgLanguageMode, ConfigProgStartupMode,
  ConfigProgStackSize, ConfigProgPriority, ConfigProgCurrentDir;
FROM DBEnvironmentInquiry IMPORT
  CPUType, WhichCPU, OSVersion2;
FROM DBFiles IMPORT
  Item, ItemInfo, StructureForm, FileType,
  InvalidModNo, InvalidStatNo, InvalidAbsAdr,
  SetProgramInfo,
  GetProgramSegList, GetModNoModOfs, GetModuleItem, GetProcedureItem, 
  GetModuleCodeBase, EmptyItem, GetItemInfo, GetFilePath, GetModNo,
  LoadProgram, UnLoadProgram, CreateModuleInfoArray, DeleteModuleInfoArray,
  FindAvailableFiles, FAFMainFileErrors, GetModNoFromName,
  GetStatementNoFromModOffset, IsAdrInitCode, EqualItems, GetModuleDataBase;
FROM DBMenusKbd IMPORT
  CommandMode,
  SetCommandMode;
FROM DBMisc IMPORT
  AllocMemory, CatTag, DeallocMemory, GetMemoryWord, GetMemoryLong, BPTRtoAPTR;
FROM DBSaveRegs IMPORT
  SaveRegs, RestoreRegs;
FROM DBStatusWindow IMPORT
  ShowErrMsgNoMemory, ShowArgs,
  ShowError, ShowStatus;
FROM DBWindows IMPORT
  WindowClass, WindowClassSet,
  UpdateWindows;
FROM Exec IMPORT
   CacheClearU;
FROM Interrupts IMPORT
  Forbid;
FROM Memory IMPORT
  MemReqSet, MemClear;
FROM Ports IMPORT
  MsgPortPtr, Message,
  WaitPort, PutMsg, GetMsg, ReplyMsg;
FROM PortsUtil IMPORT
  CreatePort, DeletePort;
FROM Nodes IMPORT
  NTMessage;
FROM System IMPORT
  DOSBase, StdInput, StdOutput;
FROM Tasks IMPORT
  CurrentTask,
  FindTask;
FROM Utility IMPORT
  TagDone, TagIgnore, TagItem;

CONST
  (* Error Message to be displayed in status window *)
  ErrMsgLockCurrentDir = "Locking current directory.";
  ErrMsgOpenStdInput   = "Opening standard input.";
  ErrMsgOpenStdOutput  = "Opening standard output.";

  ErrMsgNoFirstInstStartup =
        "Unable to stop on first instruction in Modula-2 mode.";

  ErrMsgNoMainMod = 
        "A '.MOD' file was not found for the program module.";
  ErrMsgNoMainRfm = 
        "An '.RFM' file was not found for the program module.";
  ErrMsgMainRfmBadKey = 
        "Key check fails for the program module executable and '.RFM' files.";
  ErrMsgMainModBadDate = 
        "Date check fails for the program module '.MOD' and '.RFM' files.";
  ErrMsgNoMainStartup = 
        "Problem with the '.RFM' and/or '.MOD' file for the program module.";

  ErrMsgNoLibStartup  =
        "An '.RFM' and/or '.MOD' file was not found for any module.";

  ErrMsgNoSystem =
        "Unable to halt program linked without a 'System' module.";

  ErrMsgNoHalt = 
        "Unable to halt program until startup code has been executed."; 

  (* Program Status Messages *)
  StatusMsgBreakPoint = "Stopped On Break Point";
  StatusMsgStopped    = "Stopped";
  StatusMsgStepping   = "Single Stepping";
  StatusMsgExecuting  = "Executing";
  StatusMsgLoading    = "Loading...";

  DefProcessWindowPtr = 0D; (* Set program prWindowPtr to same value as CLI *)

  ModuleBodyCodeOffset   = 4D; (* Code offset to first inst of module body *)
  GlobalAbsAdrA5Offset   = 4D; (* Procedure abs addressing mode A5 offset *)
  GlobalRelAdrA5Offset   = 8D; (* Procedure rel addressing mode A5 offset *)
  LocalA5Offset          = 4D; (* Procedure local to procedure A5 offset *)
  GlobalRelAdrLinkOffset = 6D; (* Rel addressing mode LINK inst offset *)

  (* Program System module related information *)
  SystemModuleName     = "System"; (* Name of module *)
  SystemHALTProcOffset = 8D;       (* Relative code offset of HALT procedure *)
  SystemMathBaseOffset = -12D;     (* Relative data offset of MathBase var *)

  (* Stored by debugger in tcTrapData field to allow program to check if *)
  (* it is running under the debugger or as a normal process.            *)
  ProgExceptionData = 04D324442H; (* "M2DB" *)

  (* MC68000 CPU registers *)
  D0 = 0; D1 = 1; D2 = 2; D3 = 3; D4 = 4; D5 = 5; D6 = 6; D7 = 7;
  A0 = 8; A1 = 9; A2 =10; A3 =11; A4 =12; A5 =13; A6 =14; A7 =15; 

  (* OpCodes for instructions used in this module *)
  InstILLEGAL     = 04AFCH; (* ILLEGAL *)
  InstLINKa5      = 04E55H; (* LINK A5 *)
  InstUNLKa5      = 04E5DH; (* UNLK A5 *)
  InstPEAa0       = 04850H; (* MOVE.L A0, -(SP) *)
  InstJMPa1       = 04ED1H; (* JMP (A1) *)
  InstJMPAbs      = 04EF9H; (* JMP $xxxxxxxx *)
  InstADDQ4TOa7   = 0588FH; (* ADDQ.L #4, A7 *)
  InstRTE         = 04E73H; (* RTE *)
  InstBRA         = 06000H; (* BRA.W offset *)

  (* MC68000 Status Register. Trace Bit definition *)
  SRTraceBit = 15;

  (* MC68000 CPU Exception codes *)
  ExceptionBUSERR  = 2;
  ExceptionADRERR  = 3;
  ExceptionILLEGAL = 4;
  ExceptionTRACE   = 9;

  (* List of windows to update when program is started *)
  WindowsForStartup  = WindowClassSet{WCModuleList};

  (* List of windows to update when program has stoped *)
  WindowsForNewPos   = WindowClassSet{WCSource, WCDisassembly, WCSymData,
                                      WCMemory, WCProcChain, WCRegisters,
                                      WCInfo};

  (* List of windows to update when program has exited *)
  WindowsForProgDone = WindowsForNewPos + WindowClassSet{WCModuleList};

TYPE
  (* Program has sent debugger message because of exception or on exit *)
  ProgProcCommands = (PPCException, PPCDone);

  (* Program CPU register and exception information *)
  CPUInfo = RECORD
              cpuRegList   : ARRAY [D0..A7] OF LONGCARD; (* Register dump *)
              cpuPC        : LONGCARD;                   (* Program Counter *)
              cpuSR        : BITSET;                     (* Status Register *)
              cpuException : CARDINAL;                   (* Exception Vector *)
            END;

  (* Message passed between debugger and user program to communicate info *)
  DebugMessagePtr = POINTER TO DebugMessage;
  DebugMessage = RECORD
                   dmMessage : Message;  (* Exec Message Node *)

                   (* Program Output *)
                   dmProgCmd        : ProgProcCommands; (* Reason for message *)
                   dmProgReturnCode : LONGINT;          (* Prog exit code val *)

                   (* Debug/Program Input-Output *)
                   dmCPUInfo        : CPUInfo;          (* Register info *)

                   (* Exception Handler Private *)
                   dmRestart        : BOOLEAN; (* T: Restart exception,F: real*)
                 END;

  (* Post Mortem Debugger info passed in by crashed user program *)
  PMDInfoPtr = POINTER TO PMDInfo;
  PMDInfo = RECORD
              (* Parms passed to debugger from user program *)
              pmdCPUInfo  : CPUInfo;    (* Register and exception info *)
              pmdName     : ADDRESS;    (* Name of program executable file *)
              pmdSegList  : ADDRESS;    (* Segment List of program *)
              pmdProcess  : ADDRESS;    (* Ptr to process structure *)
              pmdMsgPort  : MsgPortPtr; (* Program message port for comm *)
              pmdStackTop : ADDRESS;    (* Program original stack top *)

              (* Parms returned by debugger to user program *)
              pmdJumpAdr     : ADDRESS; (* Adr for user program to goto *)
            END;

  (* Record used to map memory for patching in a jump instruction *)
  PatchDataPtr = POINTER TO PatchData;
  PatchData = RECORD
                pdJmp  : CARDINAL;
                pdProc : PROC;
              END;

  (* Record used to map Exception stack frame *)
  ExceptionStackFramePtr = POINTER TO ExceptionStackFrame;
  ExceptionStackFrame = RECORD
                 esfException : LONGCARD;
                 esfSR        : BITSET;
                 esfPC        : LONGCARD;
               END;

  LPTR = POINTER TO ADDRESS;

VAR
  (* User program related variables *)
  ProgProc             : ProcessPtr; (* ptr to user process structure *)
  ProgPort             : MsgPortPtr; (* ptr to user message port *)
  ProgCurrentDir       : ADDRESS;    (* program current dir lock *)
  ProgStdInput         : ADDRESS;    (* program standard input *)
  ProgStdOutput        : ADDRESS;    (* program standard output *)
  ProgCloseStdInput    : BOOLEAN;    (* TRUE = close stdInput, FALSE = not *)
  ProgCloseStdOutput   : BOOLEAN;    (* TRUE = close stdOutput, FALSE = not *)
  ProgUnLockCurrentDir : BOOLEAN;    (* TRUE = unlock lock, FALSE = not *)
  ProgIsPMDProcess     : BOOLEAN;    (* TRUE = started as PMD, FALSE = as SLD *)
  ProgPMDStackTop      : LPTR;       (* PMD program stack top ptr *)
  ProgPMDExitCode      : ADDRESS;    (* adr of PMD program exit code *)
  ProgCmdLinePtr       : ADDRESS;    (* program command line parameters *)
  ProgCmdLineLength    : LONGCARD;   (* program command line length *)

  (* Original Process structure values which must be restored on exit *)
  OriginalCurrentDir : ADDRESS; (* old current directory lock *)
  OriginalCIS        : ADDRESS; (* old std input *)
  OriginalCOS        : ADDRESS; (* old std output *)
  OriginalCLI        : ADDRESS; (* old CLI structure ptr *)
  OriginalWindowPtr  : ADDRESS; (* old Window ptr *)

  (* Debugger related variables *)
  DebugProc      : ProcessPtr;   (* Debugger process structure ptr *)
  DebugPort      : MsgPortPtr;   (* Debugger communications msg port *)
  DebugMsg       : DebugMessage; (* Message passed between debugger and prog *)
  DebugRegList   : ADDRESS;      (* Ptr to a register dump area *)

  DebugWindowPtr       : ADDRESS;
  DebugCLI             : CommandLineInterfacePtr; (* debugger CLI struct ptr *)
  DebugCLIModule       : LONGCARD; (* Original value for cliModule field *)
  DebugCLIDefaultStack : LONGCARD; (* Original value for DefaultStack *)

  (* Patching process startup variables *)
  PData    : PatchData;    (* original code where patched is applied *) 
  PDataPtr : PatchDataPtr; (* ptr to patch location *)

  (* Procedure call chain related variables *)
  ProcChainListHead : ProcChainNodePtr; (* procedure call chain head *)
  ProcChainListTail : ProcChainNodePtr; (* procedure call chain tail *)
  ProcChainLength   : CARDINAL;         (* length of procedure call chain *)
  ProcChainNew      : BOOLEAN;          (* T: rebuild chain, F: use old chain *)

  (* Program execution control related variables *)
  ProgLoaded         : BOOLEAN;       (* T: debugging program, F: no program *)
  ProgExecMode       : ExecutionMode; (* Current execution mode (step, go) *)
  ProgLangMode       : LanguageMode;  (* Current language mode (m2, asm) *)
  ProgLastInstCall   : BOOLEAN;       (* T: last inst was a JSR,BSR, F: Not *)
  ProgLastInstReturn : BOOLEAN;       (* T: last inst was RTS,RTD,JMP(a0), F: Not *)
  ProgSteppingOver   : BOOLEAN;       (* T: steping over proc, F: Not *)
  ProgSteppingOverSP : ADDRESS;       (* Stack Ptr value before steping over *)

(* ===== PRIVATE ===== *)

(* AddProcChainNode - allocate and link in a procedure call chain node *)
PROCEDURE AddProcChainNode(): ProcChainNodePtr;
VAR
  pcn: ProcChainNodePtr;
BEGIN
  pcn := AllocMemory(SIZE(ProcChainNode), MemReqSet{MemClear});
  IF (pcn # NIL) THEN
    IF (ProcChainListHead = NIL) THEN
      ProcChainListHead := pcn;
    ELSE
      ProcChainListTail^.pcnNext := pcn;
    END;
    ProcChainListTail := pcn;

    INC(ProcChainLength);
  END;
  RETURN (pcn);
END AddProcChainNode;

(* DeleteProcedureChain - delete the entire procedure call chain *)
PROCEDURE DeleteProcedureChain();
VAR
  pcn: ProcChainNodePtr;
  fpcn: ProcChainNodePtr;
BEGIN
  pcn := ProcChainListHead;
  WHILE (pcn # NIL) DO
    fpcn := pcn;
    pcn  := pcn^.pcnNext;
    DeallocMemory(fpcn);
  END;
  ProcChainListHead := NIL;
  ProcChainLength   := 0;
  ProcChainNew      := TRUE;
END DeleteProcedureChain;

(* WaitForDebugger - executed from user program to wait for debugger restart *)
PROCEDURE WaitForDebugger();
VAR
  msg: ADDRESS;
BEGIN
  (* Send message to debugger indicating program has stopped *)
  ReplyMsg(ADR(DebugMsg));

  (* Wait for message from debugger to resum execution *)
  msg := WaitPort(ProgPort^);
  msg := GetMsg(ProgPort^);

  (* Cause an exception to allow restarting of user program *)
  INLINE(InstILLEGAL);
END WaitForDebugger;

(* DoException - called from main exception handler to adjust stack, etc. *)
PROCEDURE DoException(VAR SSP: ADDRESS);
VAR
  sf: ExceptionStackFramePtr;  
BEGIN
  sf := SSP;

  WITH DebugMsg.dmCPUInfo DO
    cpuException := sf^.esfException;

    (* The MC68000 places extra data on top of Supervisor stack for *)
    (* ADDRESS ERROR and BUS ERROR exceptions.  This data must be removed *)
    (* from the stack.  No such data is placed by the MC68010/20/30 cpus *)
    IF ((cpuException = ExceptionBUSERR) OR (cpuException = ExceptionADRERR))
        AND (WhichCPU = M68000) THEN
      INC(ADDRESS(sf), 8); (* Skip over adr and bus error info *)
      (* Adjust SSP to skip over adr/bus error info *)
      INC(SSP, 8);
    END;

    cpuPC := sf^.esfPC;
    cpuSR := sf^.esfSR;
  END; (* WITH *)

  DebugMsg.dmProgCmd := PPCException;

  EXCL(sf^.esfSR, SRTraceBit);

  (* Patch return address to cause a jump to our user-mode wait function *)
  sf^.esfPC := ADR(WaitForDebugger);
END DoException;

(* RestorePCandSR - copy new values for PC and SR into exception stack frame *)
PROCEDURE RestorePCandSR(SSP: ADDRESS);
VAR
  sf: ExceptionStackFramePtr;  
BEGIN
  sf := SSP;
  WITH DebugMsg.dmCPUInfo DO
    sf^.esfPC := cpuPC;
    sf^.esfSR := cpuSR;
  END;
END RestorePCandSR;

(*$L+ (Abs Adr) *)
VAR
  (* Private exception handler variable *)
  TempA7 : ADDRESS; (* Current Supervisor Stack Ptr *)

(* ExceptionHandler - called in user program upon encountering an exception *)
PROCEDURE ExceptionHandler();
BEGIN
  (* WARNING: Do not define any local variables in this procedure *)
  INLINE(InstUNLKa5);

  (* Get Supervisor Mode Stack Ptr *)
  TempA7 := REG(A7);

  IF (DebugMsg.dmRestart) THEN (* ILLEGAL exception used to restart program. *)
    DebugMsg.dmRestart := FALSE;
    RestorePCandSR(TempA7);
    RestoreRegs(DebugRegList);
  ELSE (* Real exception occured in program *)
    SaveRegs(DebugRegList);

    DebugMsg.dmRestart := TRUE;

    DoException(TempA7);

    (* Loads the adjusted stack ptr value *)
    SETREG(A7, TempA7);        (* MOVE.L TempA7, A7 *)
  END;

  (* Return To User Program *)
  INLINE(InstADDQ4TOa7);      (* ADDQ.L #4,A7 (pop exception # from stack) *)
  INLINE(InstRTE);
END ExceptionHandler;
(*$L- (Rel Adr) *)

(*$L+ (Abs Adr) *)
(* CleanupProcess - called in after program has completed execution *)
PROCEDURE CleanupProcess();
BEGIN
  (* Send message to debugger indicating program has exited *)
  WITH DebugMsg DO
    dmProgReturnCode := REG(D0);  (* Exit code passed by user program *)
    dmProgCmd        := PPCDone;
  END;

  (* Only restore Process structure if launched by the debugger *)
  IF (NOT ProgIsPMDProcess) THEN
    WITH ProgProc^ DO
      IF NOT OSVersion2 THEN
        prCurrentDir := OriginalCurrentDir;
        prCIS        := OriginalCIS;
        prCOS        := OriginalCOS;
      END;
      prCLI        := OriginalCLI;
      prWindowPtr  := OriginalWindowPtr;
    END;
  END;

  (* Delete message port used to communicate with debugger *)
  DeletePort(ProgPort^);

  (* If debugger launched program then prevent it from unloading us until done*)
  IF (NOT ProgIsPMDProcess) THEN
    Forbid();
  END;

  (* Send message to debugger indicating that program has completed execution *)
  ReplyMsg(ADR(DebugMsg));

  (* If Post Mortem Debug mode then jump to its exit code *)
  IF (ProgIsPMDProcess) THEN
    INLINE(InstUNLKa5);
    SETREG(A1, ProgPMDExitCode);
    INLINE(InstJMPa1);
  END;

  (* The user program has now exits to the AmigaDOS process kill code *)
END CleanupProcess;
(*$L- (Rel Adr) *)

(*$L+ (Abs Adr) *)
(* InitProcess - called in user program to setup environment *)
PROCEDURE InitProcess();
BEGIN
  (* Restore stack to its original state on entry into procedure *)
  (* WARNING: This procedure can't allocate any local variables *)
  INLINE(InstUNLKa5);

  ProgProc := ProcessPtr(FindTask(CurrentTask));

  (* Create a communications port for the user program *)
  (* WARNING: No error checking is performed here      *)
  ProgPort := CreatePort(NIL, 0);

  (* Save key variables from Process structure *)
  WITH ProgProc^ DO
    OriginalCurrentDir := prCurrentDir;
    OriginalCIS        := prCIS;
    OriginalCOS        := prCOS;
    OriginalCLI        := prCLI;
    OriginalWindowPtr  := prWindowPtr;
  END;

  (* Initialize key process variables *)
  WITH ProgProc^ DO
    IF NOT OSVersion2 THEN
      prCurrentDir := ProgCurrentDir;
      prCIS        := ProgStdInput;
      prCOS        := ProgStdOutput;
    END;
    prCLI        := DebugProc^.prCLI;
    prWindowPtr  := DefProcessWindowPtr;
  END;

  (* Restore patched location with original data *)
  PDataPtr^ := PData;
  IF OSVersion2 THEN
     CacheClearU();
  END;

  (* Install exception handler *)
  WITH ProgProc^.prTask DO
    tcTrapCode := ADR(ExceptionHandler);
    tcTrapData := ProgExceptionData;
  END;

  (* Setup all break points *)
  InstallBreakPoints();

  (* Push address of procedure to be called on exit from user program *)
  SETREG(A0, ADR(CleanupProcess));
  INLINE(InstPEAa0);

  (* Setup Command Line Parameters *)
  SETREG(D0, ProgCmdLineLength);
  SETREG(A0, ProgCmdLinePtr);

  (* JMP to beginning of program *)
  SETREG(A1, PDataPtr);
  INLINE(InstJMPa1);
END InitProcess;
(*$L- (Rel Adr) *)

(* CreateProcedureChain - create a procedure call chain from current code pos *)
PROCEDURE CreateProcedureChain();
VAR
  procPC: ADDRESS;
  procA5: ADDRESS;
  A5Ofs: LONGCARD;
  A7Ofs: LONGCARD;
  modNo: CARDINAL;
  modOfs: LONGCARD;
  modItem: Item;
  modInfo: ItemInfo;
  procItem: Item;
  procInfo: ItemInfo;
  procName: ADDRESS;
  procOfs: LONGCARD;
  pcn: ProcChainNodePtr;
  b: BOOLEAN;
  rfmFile: BOOLEAN;
  localAlloc: BOOLEAN;
  chainEnd: BOOLEAN;
BEGIN
  WITH DebugMsg.dmCPUInfo DO
    procPC := cpuPC;
    procA5 := cpuRegList[A5];
  END;

  (* Check if the same procedure call chain can be updated without rebuilding *)
  (* a complete new chain.  This will be possible if the users has been       *)
  (* single stepping in current procedure.                                    *)
  IF (NOT ProcChainNew) THEN (* Procedure call chain assumed to be valid *)
    IF (ProcChainListHead # NIL) THEN (* Continue if not empty chain *)
      GetModNoModOfs(procPC, modNo, modOfs);
      IF (modNo # InvalidModNo) THEN (* Continue if in a program module *)
        GetModuleItem(modNo, modItem, FALSE);
        IF (NOT EmptyItem(modItem)) THEN (* Continue if reference file ok *)
          GetProcedureItem(modItem, modOfs, procItem);
          IF (NOT EmptyItem(procItem)) THEN (* Continue if procedure found *)
            (* Check if still in same procedure *)
            IF (EqualItems(procItem, ProcChainListHead^.pcnProcItem)) THEN
              (* Check if locals have been allocated.  If not then must *)
              (* rebuild chain everytime.                               *)
              IF (ProcChainListHead^.pcnLocalAlloc) THEN
                (* Update procedure call chain *)
                WITH ProcChainListHead^ DO
                  pcnModOffset   := modOfs;
                  pcnAdr         := procPC;
                  pcnDataBase    := procA5;
                END;
                RETURN; (* Done! *)
              END;
            END;
          END;
        END;
      END;
    END;
  END;

  (* Delete current procedure call chain *)
  DeleteProcedureChain();

  (* Continue to build procedure call chain until no more info available *)
  LOOP
    (* Check if adr found in any module. *)
    GetModNoModOfs(procPC, modNo, modOfs);
    IF (modNo = InvalidModNo) THEN EXIT; END;

    (* Check if reference file is available. Stop if not available. *)
    GetModuleItem(modNo, modItem, FALSE);
    IF (EmptyItem(modItem)) THEN EXIT; END;
    GetItemInfo(modItem, modInfo);

    (* Find procedure associated with address. Stop if not found. *)
    GetProcedureItem(modItem, modOfs, procItem);
    IF (EmptyItem(procItem)) THEN EXIT; END;
    GetItemInfo(procItem, procInfo);

    (* Create a procedure chain node and link into chain *)
    pcn := AddProcChainNode();
    IF (pcn = NIL) THEN EXIT; END;

    (* Check if adr actualy in module body or regular procedure *)
    IF (procInfo.iiForm = Prc) THEN (* Procedure *)
      procName := procInfo.iiNameString; (* procedure name *)
      procOfs  := procInfo.iiAdr;        (* start of procedure adr *)
    ELSIF (procInfo.iiForm = Mod) THEN (* Module Body *)
      procName := ADR("MODULE_BODY");    (* module body name *)
      procOfs  := ModuleBodyCodeOffset;  (* module body adr *)
    END; (* No other forms possible *)

    chainEnd := FALSE;
    IF (procInfo.iiForm = Mod) THEN (* Module body *)
      localAlloc := TRUE;
      chainEnd   := TRUE;
    ELSIF ((procInfo.iiForm = Prc) AND (procInfo.iiLevel = 0)) THEN
      (* Procedure at scope level 0 *)
      (* If first instruction of procedure is LINK then absolute adr mode *)
      IF (GetMemoryWord(GetModuleCodeBase(modNo) + procOfs) = InstLINKa5) THEN
        (* If code position beyond first instruction then stack frame setup *)
        localAlloc := modOfs > procOfs;
        IF (localAlloc) THEN (* Local stack frame setup *)
          A5Ofs := GlobalAbsAdrA5Offset;
        ELSE (* no local stack frame *)
          A7Ofs := 0D; (* stack offset where return adr can be found *)
        END;
      ELSE (* Relative address mode *)
        (* If code pos beyound LINK inst then stack frame setup *)
        localAlloc := modOfs > procOfs + GlobalRelAdrLinkOffset;
        IF (localAlloc) THEN (* Local stack frame setup *)
          A5Ofs := GlobalRelAdrA5Offset;
        ELSE (* no local stack frame *)
          IF (modOfs = procOfs) THEN (* on first instruction *)
            A7Ofs := 0D; (* stack offset where return adr can be found *)
          ELSE (* on next instruction (MOVE.L A4,-(SP)) has been executed *)
            A7Ofs := 4D; (* stack offset where return adr can be found *)
          END;
        END;
      END;
    ELSE (* Procedure at scope > 0 (assumes A4 is already loaded) *)
      (* LINK instruction is first in procedure.  If adr > start of procedure *)
      (* then LINK instruction has been executed and local stack frame setup. *)
      localAlloc := modOfs > procOfs;
      IF (localAlloc) THEN (* Local stack frame setup *)
        A5Ofs := LocalA5Offset;
      ELSE (* No local stack frame *)
        A7Ofs := 0D; (* stack offset where return adr can be found *)
      END;
    END;

    (* Initialize current procedure call chain node with info *)
    WITH pcn^ DO
      pcnIsProc      := procInfo.iiForm = Prc;
      pcnLocalAlloc  := localAlloc;
      pcnProcItem    := procItem;
      pcnModNo       := modNo;
      pcnModOffset   := modOfs;
      pcnAdr         := procPC;
      pcnDataBase    := procA5;
      pcnModName     := modInfo.iiNameString;
      pcnProcName    := procName;
    END;

    (* End of chain has been reached. Stop search. *)
    IF (chainEnd) THEN EXIT; END;

    IF (localAlloc) THEN (* Local stack frame setup *)
      procPC := GetMemoryLong(procA5 + A5Ofs);
      procA5 := GetMemoryLong(procA5);
    ELSE (* No local stack frame *)
      procPC := GetMemoryLong(DebugMsg.dmCPUInfo.cpuRegList[A7] + A7Ofs);
      (* procA5 - is already the A5 register of the previous procedure *)
    END;
  END; (* LOOP *)
END CreateProcedureChain;

(* FindFirstRfmModPair - find the first module with .MOD/.RFM file pair *)
PROCEDURE FindFirstRfmModPair(): CARDINAL;
VAR
  p: ADDRESS;
  mod: CARDINAL;
BEGIN
  (* Convert segment list to ptr to first instruction - (6) *)
  p := (GetProgramSegList() * 4D) - 2D;

  (* Traverse init-code in the order of module initialization *)
  REPEAT
    (* Advance to next instruction *)
    INC(p, 6);

    GetModNo(GetMemoryLong(p + 2D), mod);
    IF (mod # InvalidModNo) THEN
      IF (GetFilePath(mod,FTRfm) # NIL) AND (GetFilePath(mod,FTMod) # NIL) THEN
        RETURN (mod); (* Success! *)
      END;
    END;

  UNTIL (GetMemoryWord(p) = InstJMPAbs);

  RETURN (InvalidModNo); (* Failed! *)
END FindFirstRfmModPair;

(* CleanupProgEnvironment - cleanup user program has been finished *)
PROCEDURE CleanupProgEnvironment();
BEGIN
  IF (ProgCloseStdInput) THEN
    Close(ProgStdInput);
    ProgCloseStdInput := FALSE;
  END;
  IF (ProgCloseStdOutput) THEN
    Close(ProgStdOutput);
    ProgCloseStdOutput := FALSE;
  END;
  IF (ProgUnLockCurrentDir) THEN
    UnLock(ProgCurrentDir);
    ProgUnLockCurrentDir := FALSE;
  END;
END CleanupProgEnvironment;

(*---------------------------------------------------------------------------*)
 PROCEDURE SetupProgEnvironment(parmsPtr: ADDRESS; parmsLength: LONGCARD;
                                VAR Tags:ARRAY OF TagItem): BOOLEAN;
(*---------------------------------------------------------------------------*)

(* SetupProgEnvironment - setup program parameters and handle i/o redirection *)

VAR
  str: POINTER TO CHAR;
  i: CARDINAL;
  err: BOOLEAN;
  ch: CHAR;
  parmsStart: ADDRESS;
  nameBuf: ARRAY [0..127] OF CHAR;

BEGIN

  Tags[0].tiTag := NPCurrentDir;
  Tags[1].tiTag := NPInput;
  Tags[2].tiTag := NPOutput;
  Tags[3].tiTag := NPArguments;
  Tags[4].tiTag := NPCloseInput;      Tags[4].tiData := ORD(FALSE);
  Tags[5].tiTag := NPCloseOutput;     Tags[5].tiData := ORD(FALSE);
  Tags[6].tiTag := TagDone;

  LOOP
    (* Setup current directory lock *)
    ProgCurrentDir := DebugProc^.prCurrentDir;
    Tags[0].tiTag := TagIgnore;
    IF (ConfigProgCurrentDir # NIL) THEN
      ProgCurrentDir := Lock(ConfigProgCurrentDir, SharedLock);
      IF (ProgCurrentDir = NIL) THEN
        ShowError(ADR(ErrMsgLockCurrentDir));
        EXIT; (* Error *)
      END;
      ProgUnLockCurrentDir := TRUE;
    END;
    Tags[0].tiData := ProgCurrentDir;

    (* By default use debugger's standard input and output handles *)
    ProgStdInput  := StdInput;
    ProgStdOutput := StdOutput;
    Tags[1].tiData := ProgStdInput;
    Tags[2].tiData := ProgStdOutput;

    parmsStart := parmsPtr;
    str        := parmsPtr;
    err        := FALSE;

    (* Parse any i/o redirection parameters *)
    LOOP
      ch := str^;
      INC(ADDRESS(str)); (* Next char *)
      IF (ch < " ") THEN (* End of parameters *)
        EXIT; (* Done! *)
      END;

      IF (ch # " ") THEN
        IF (ch = "<") THEN
          IF (ProgCloseStdInput) THEN (* Std input already defined *)
            EXIT; (* Done! *)
          END;
          i := 0;
          WHILE (str^ > " ") DO
            nameBuf[i] := str^;
            INC(i);
            INC(ADDRESS(str));
          END;
          nameBuf[i] := 0C;
          parmsStart := str;

          ProgStdInput := Open(ADR(nameBuf[0]), ModeOldFile);
          IF (ProgStdInput = NIL) THEN
            ShowError(ADR(ErrMsgOpenStdInput));
            err := TRUE;
            EXIT; (* Failed! *)
          END;
          Tags[1].tiData := ProgStdInput;
          ProgCloseStdInput := TRUE;

        ELSIF (ch = ">") THEN
          IF (ProgCloseStdOutput) THEN (* Std output already defined *)
            EXIT; (* Done! *)
          END;
          i := 0;
          WHILE (str^ > " ") DO
            nameBuf[i] := str^;
            INC(i);
            INC(ADDRESS(str));
          END;
          nameBuf[i] := 0C;
          parmsStart := str;

          ProgStdOutput := Open(ADR(nameBuf[0]), ModeNewFile);
          IF (ProgStdOutput = NIL) THEN
            ShowError(ADR(ErrMsgOpenStdOutput));
            err := TRUE;
            EXIT; (* Failed! *)
          END;
          Tags[2].tiData := ProgStdOutput;
          ProgCloseStdOutput := TRUE;

        ELSE (* A Program Parameter *)
          EXIT; (* Done! *)
        END;
      END;
    END; (* LOOP *)

    IF (err) THEN
      EXIT; (* Failed! *)
    END;

    (* Setup new adjusted program parameters *)
    ProgCmdLinePtr    := parmsStart;
    ProgCmdLineLength := parmsLength - (parmsStart - parmsPtr);

    Tags[3].tiData := ProgCmdLinePtr; (*NPArguments*)

    RETURN (TRUE); (* Success! *)
  END;
  CleanupProgEnvironment();

  RETURN (FALSE); (* Failed! *)
END SetupProgEnvironment;

(* UserProgramDone - cleanup after program has completed execution *)
PROCEDURE UserProgramDone();
BEGIN
  ProgLoaded := FALSE;
  RemoveBreakPoints();
  DeleteBreakPoints();
  DeleteTempBreakPoint();
  DeleteProcedureChain();
  UpdateWindows(WindowsForProgDone);
  DeleteModuleInfoArray();
  UnLoadProgram();
  CleanupProgEnvironment();

  ShowArgs[0].L := DebugMsg.dmProgReturnCode;
  ShowStatus(ADR("Program Exited (%ld)"));

  SetCommandMode(CMNoProg);
END UserProgramDone;

(* ShowException - display status line with exception info *)
PROCEDURE ShowException(exception: CARDINAL);
VAR
  str: ADDRESS;
  buf: ARRAY [0..127] OF CHAR;
BEGIN
  CASE (exception) OF
  | 2:        str := ADR("Bus Error");
  | 3:        str := ADR("Address Error");
  | 4:        str := ADR("Illegal Instruction");
  | 5:        str := ADR("Zero Divide");
  | 6:        str := ADR("CHK Instruction");
  | 7:        str := ADR("TRAPV Instruction");
  | 8:        str := ADR("Privilege Violation");
  | 9:        str := ADR("Trace");
  | 10:       str := ADR("Line 1010 Emulator");
  | 11:       str := ADR("Line 1111 Emulator");
  ELSE
    IF (exception >= 32) AND (exception <= 47) THEN
      str := ADR("TRAP #%u Instruction");
      ShowArgs[1].W := exception - 32;
    ELSE
      str := ADR("Miscellaneous");
    END;
  END;
  strcpy(ADR(buf[0]), ADR("Exception %u:"));
  strcat(ADR(buf[0]), str);

  ShowArgs[0].W := exception;
  ShowStatus(ADR(buf[0]));
END ShowException;

(* ProgramStopped - update windows after program has stopped temporarily *)
PROCEDURE ProgramStopped();
BEGIN
  ProgExecMode := EMWait;
  RemoveBreakPoints();
  DeleteTempBreakPoint();
  CreateProcedureChain();
  UpdateWindows(WindowsForNewPos);
  SetCommandMode(CMWait);
END ProgramStopped;

(* HandleExecModeGo - program has encountered exception in GO mode *)
PROCEDURE HandleExecModeGo();
VAR
  isBP: BOOLEAN;
  bpTemp: BOOLEAN;
  stopBP: BOOLEAN;
BEGIN
  isBP := FALSE;
  WITH DebugMsg.dmCPUInfo DO
    isBP := StopOnBreakPoint(cpuPC, stopBP, bpTemp);
    IF (cpuException = ExceptionILLEGAL) OR (cpuException = ExceptionTRACE) THEN
      IF (isBP) THEN (* Address has an enabled break point *)
        IF (stopBP) THEN (* Stop break point trigered! *)
          ShowStatus(ADR(StatusMsgBreakPoint));
        ELSE (* Break Limit Not Passed *)
          (* The Remove func does nothing if not installed. *)
          RemoveBreakPoints();

          (* Single step over current instruction *)
          INCL(DebugMsg.dmCPUInfo.cpuSR, SRTraceBit);
          PutMsg(ProgPort^, ADR(DebugMsg));
          RETURN (* Done *)
        END;
      ELSIF (cpuException = ExceptionTRACE) THEN (* Previous inst had a bp *)
        (* Continue execution at full-speed *)
        InstallBreakPoints();
        PutMsg(ProgPort^, ADR(DebugMsg));
        RETURN; (* Done *)
      END;
    END;

    (* If not a break point then display exception information *)
    IF (NOT isBP) THEN
      ShowException(cpuException);
    END;
  END; (* WITH *)

  (* Program has stopped update all windows *)
  ProgramStopped();
END HandleExecModeGo;

(* IsInstProcCall - determine if current instruction is a procedure call *)
PROCEDURE IsInstProcCall(pc: ADDRESS): BOOLEAN;
CONST
  InstBSR = {14,13,8};
  InstJSR = {14,11,10,9,7};
VAR
  p: POINTER TO BITSET;
  iBSR: BITSET;
  iJSR: BITSET;
BEGIN
  p := pc;
  iBSR := p^ * {8..15};
  iJSR := p^ * {6..15};

  IF (iBSR = InstBSR) THEN (* BSR instruction *)
    RETURN (TRUE); (* call *)
  ELSIF (iJSR = InstJSR) THEN (* JSR instruction *)
    IF (p^ * {3..5} = {5,4}) THEN (* JSR (An,Xi) *)
      p := ADDRESS(p) - 8D;
      IF (p^ * {12..15,0..8} = {14,13,12,0..7}) THEN (* MOVEQ #-1,Dn *)
        p := ADDRESS(p) - 2D;
        IF (p^ = {14,13,9,8,1}) THEN (* BLS.S *+2 *)
          RETURN (FALSE); (* case statement *)
        END;
      END;
    END;

    RETURN (TRUE); (* call *)
  END;

  RETURN (FALSE); (* Not Subroutine call *)
END IsInstProcCall;

(* IsInstProcEnd - determine if current instruction is end of procedure *)
PROCEDURE IsInstProcEnd(pc: ADDRESS): BOOLEAN;
CONST
  InstRTS   = {14,11,10,9,6,5,4,2,0};
  InstRTD   = {14,11,10,9,6,5,4,2};
  InstJMPa0 = {14,11,10,9,7,6,4};
VAR
  inst: BITSET;
BEGIN
  inst := BITSET(GetMemoryWord(pc));
  RETURN ((inst = InstRTS) OR (inst = InstRTD) OR (inst = InstJMPa0));
END IsInstProcEnd;

(* HandleExecModeStepOrRun - program encountered exception in Step/Run Mode *)
PROCEDURE HandleExecModeStepOrRun();
VAR
  stopBP: BOOLEAN;
  bpTemp: BOOLEAN;
  isBP: BOOLEAN;
  retAdr: ADDRESS;
  modNo: CARDINAL;
  statNo: CARDINAL;
  modOfs: LONGCARD;
  firstInst: BOOLEAN;
  updateDisp: BOOLEAN;
BEGIN
  WITH DebugMsg.dmCPUInfo DO
    IF (ProgSteppingOver) THEN (* Executing procedure at full speed *)
      ProgSteppingOver := FALSE;
      isBP := StopOnBreakPoint(cpuPC, stopBP, bpTemp);
      IF (cpuException = ExceptionILLEGAL) OR (cpuException = ExceptionTRACE) THEN
        IF (isBP) THEN
          IF (stopBP) THEN (* Stop! *)
            IF (bpTemp) THEN (* Return break point hit *)
              (* If the stack pointer before the step-over is less than after   *)
              (* the call then the current procedure chain is valid.  Otherwise *)
              (* this must be a recursive procedure and so invalidate chain     *)
              ProcChainNew := NOT (ProgSteppingOverSP <= cpuRegList[A7]);
  
              (* The Remove func does nothing if not installed. *)
              RemoveBreakPoints();
              DeleteTempBreakPoint();
              ShowStatus(ADR(StatusMsgStepping));
            ELSE (* User break point hit *)
              ProcChainNew := TRUE; (* Invalidate procedure chain *)
              ShowStatus(ADR(StatusMsgBreakPoint));
              ProgramStopped();
              RETURN; (* Done *)
            END;
          ELSE (* User break point encountered (limit not passed) *)
            (* The Remove func does nothing if not installed. *)
            RemoveBreakPoints();

            (* Single step over current instruction *)
            INCL(DebugMsg.dmCPUInfo.cpuSR, SRTraceBit);
            PutMsg(ProgPort^, ADR(DebugMsg));
            RETURN (* Done *)
          END;
        ELSIF (cpuException = ExceptionTRACE) THEN
          (* Continue execution at full-speed *)
          InstallBreakPoints();
          PutMsg(ProgPort^, ADR(DebugMsg));
          RETURN; (* Done *)
        ELSE (* Unexpected exception *)
          ProcChainNew := TRUE; (* Invalidate procedure chain *)
          ShowException(cpuException);
          ProgramStopped();
          RETURN; (* Done *)
        END;
      ELSE (* Unexpected exception *)
        ProcChainNew := TRUE; (* Invalidate procedure chain *)
        ShowException(cpuException);
        ProgramStopped();
        RETURN; (* Done *)
      END;
    ELSE (* Single stepping *)
      IF (cpuException = ExceptionTRACE) THEN
        (* Check if currently on break point pos *)
        isBP := StopOnBreakPoint(cpuPC, stopBP, bpTemp);
        IF (isBP) AND (stopBP) THEN
          ShowStatus(ADR(StatusMsgBreakPoint));
          ProgramStopped();
          RETURN; (* Done *)
        END;

        IF (ProgLastInstCall) THEN
          GetModNo(cpuPC, modNo);
          IF ((ProgLangMode = LMAsm) AND
              ((ProgExecMode = EMStepOver) OR
               (ProgExecMode = EMRunOver)))
             OR
             ((ProgLangMode = LMM2) AND
              ((ProgExecMode = EMStepOver) OR
               (ProgExecMode = EMRunOver) OR
               (modNo = InvalidModNo) OR
               (GetFilePath(modNo, FTRfm) = NIL) OR
               (GetFilePath(modNo, FTMod) = NIL))) THEN
            ProgSteppingOver   := TRUE;
            ProgSteppingOverSP := cpuRegList[A7];
            retAdr := GetMemoryLong(cpuRegList[A7]);
            CreateTempBreakPoint(retAdr);
            InstallBreakPoints();

            ShowStatus(ADR(StatusMsgExecuting));
            PutMsg(ProgPort^, ADR(DebugMsg));
            RETURN; (* Done *)
          END;

          (* Stepping into new procedure *)
          ProcChainNew := TRUE; (* Invalidate procedure chain *)

        ELSIF (ProgLastInstReturn) THEN
          ProcChainNew := TRUE; (* Invalidate procedure chain *)
        END;
      ELSE (* Unexpected exception *)
        ProcChainNew := TRUE; (* Invalidate procedure chain *)
        ShowException(cpuException);
        ProgramStopped();
        RETURN; (* Done *)
      END;
    END;

    ProgLastInstCall   := IsInstProcCall(cpuPC);
    ProgLastInstReturn := IsInstProcEnd(cpuPC);

    firstInst := FALSE;
    IF (ProgLangMode = LMM2) THEN
      GetModNoModOfs(cpuPC, modNo, modOfs);
      IF (modNo # InvalidModNo) THEN
        GetStatementNoFromModOffset(modNo, modOfs, statNo, firstInst);
        IF (statNo = InvalidStatNo) OR (NOT firstInst) THEN
          IF (statNo = InvalidStatNo) THEN
            ProcChainNew := TRUE; (* Invalidate procedure chain *)
          END;

          INCL(DebugMsg.dmCPUInfo.cpuSR, SRTraceBit);
          PutMsg(ProgPort^, ADR(DebugMsg));
          RETURN; (* Done *)
        END;
      ELSE (* Current PC is not any module *)
        ProcChainNew := TRUE; (* Invalidate procedure chain *)
        (* Check if actually in init code *)
        IF (IsAdrInitCode(cpuPC)) THEN
          (* Step until-next module-body with source and reference info *)
          INCL(DebugMsg.dmCPUInfo.cpuSR, SRTraceBit);
          PutMsg(ProgPort^, ADR(DebugMsg));
          RETURN; (* Done *)
        ELSE (* Not init-code *)
          (* Continue execution at full-speed *)
          ProgExecMode := EMGo;
          InstallBreakPoints();

          ShowStatus(ADR(StatusMsgExecuting));
          PutMsg(ProgPort^, ADR(DebugMsg));
          RETURN; (* Done *)
        (* ELSE continue to single step throught init code *)
        END;
      END;
    END;

    IF (ProgExecMode = EMRunIn) OR (ProgExecMode = EMRunOver) THEN
      IF (ProgLangMode = LMAsm) OR 
         ((ProgLangMode = LMM2) AND (firstInst)) THEN
        CreateProcedureChain();
        UpdateWindows(WindowsForNewPos);
        ProcChainNew := FALSE; (* Reset proc-chain *)
      END;
      INCL(DebugMsg.dmCPUInfo.cpuSR, SRTraceBit);
      PutMsg(ProgPort^, ADR(DebugMsg));
    ELSE
      ShowStatus(ADR(StatusMsgStopped));
      ProgramStopped();
    END;
  END; (* WITH *)
END HandleExecModeStepOrRun;

(* ===== PUBLIC ===== *)

(* RunProgramAsPMD - Start debugger as a Post Mortem Debugger on a process *)
PROCEDURE RunProgramAsPMD(pmd: ADDRESS);
VAR
  pmdInfo              :PMDInfoPtr;
  MainModFileStatus,
  MainRfmFileStatus    :FAFMainFileErrors;   (* ignored in this proc *)

BEGIN
  pmdInfo := pmd;

  SetProgramInfo(pmdInfo^.pmdName, pmdInfo^.pmdSegList);
  IF (CreateModuleInfoArray()) THEN
    IF (FindAvailableFiles(MainModFileStatus, MainRfmFileStatus)) THEN

      WITH pmdInfo^ DO
        (* Extract post-mortem info *)
        ProgProc        := pmdProcess;
        ProgPort        := pmdMsgPort;
        ProgPMDStackTop := pmdStackTop;

        (* set adr to jump to for user program *)
        pmdJumpAdr      := ADR(WaitForDebugger);
      END;

      (* Save adr of exit code in user program stack *)
      ProgPMDExitCode  := ProgPMDStackTop^;

      (* Patch adr of our debugger cleanup routine as exit code *)
      ProgPMDStackTop^ := ADR(CleanupProcess);

      (* Set debugger exception handler for user program *)
      WITH ProgProc^.prTask DO
        tcTrapCode := ADR(ExceptionHandler);
      END;

      (* Setup debugger for Go Mode *)
      ProgExecMode     := EMGo;
      ProgLangMode     := LMM2;
      ProcChainNew     := TRUE; (* Invalidate Procedure Chain *)
      ProgLoaded       := TRUE;
      ProgIsPMDProcess := TRUE;

      SetCommandMode(CMExec);

      (* Display initial windows *)
      UpdateWindows(WindowsForStartup);

      (* Setup Debug Msg and send to user program *)
      WITH DebugMsg DO
        dmProgCmd := PPCException;
        dmCPUInfo := pmdInfo^.pmdCPUInfo;
        dmRestart := TRUE;
      END;
      PutMsg(ProgPort^, ADR(DebugMsg));

      RETURN; (* Success! *)
    END;
    DeleteModuleInfoArray();
  END;
END RunProgramAsPMD;

(*--------------------------------------------------------------------*)
 PROCEDURE RunProgramAsSLD(progName: ADDRESS; progCmdLinePtr: ADDRESS;
                           progCmdLineLength: LONGCARD);
(*--------------------------------------------------------------------*)
(* RunProgramAsSLD - Start debugger as Source-Level Debugger on a new process.*)
VAR
  segList              :ADDRESS;
  firstRfmMod          :CARDINAL;
  firstInst            :ADDRESS;
  bpAdr                :ADDRESS;
  stdInput             :ADDRESS;
  stdOutput            :ADDRESS;
  MainModFileStatus,
  MainRfmFileStatus    :FAFMainFileErrors;
  CPResult             :ADDRESS;
  Tags                 :ARRAY[0..19] OF TagItem;

BEGIN
  IF (SetupProgEnvironment(progCmdLinePtr, progCmdLineLength, Tags)) THEN
    ShowStatus(ADR(StatusMsgLoading));
    IF (LoadProgram(progName)) THEN
      IF (CreateModuleInfoArray()) THEN
        IF (FindAvailableFiles(MainModFileStatus, MainRfmFileStatus)) THEN
          segList := GetProgramSegList();

          (* Setup CLI structure for user program *)
          WITH DebugCLI^ DO
            cliModule       := segList;
            cliDefaultStack := ConfigProgStackSize DIV 4D; (* LW Size *)
          END;

          firstInst := (segList * 4D) + 4D;
    
          firstRfmMod := FindFirstRfmModPair();
    
          IF (ConfigProgLanguageMode = LMM2) THEN
            CASE (ConfigProgStartupMode) OF
            | SMFirstInst:  bpAdr := NIL;
                            ShowError(ADR(ErrMsgNoFirstInstStartup));
            | SMLibModule:  IF (firstRfmMod # InvalidModNo) THEN
                              bpAdr := GetModuleCodeBase(firstRfmMod) + ModuleBodyCodeOffset;
                            ELSE
                              bpAdr  := NIL;
                              ShowError(ADR(ErrMsgNoLibStartup));
                            END;
            | SMMainModule: IF (GetFilePath(0, FTRfm) # NIL) AND
                               (GetFilePath(0, FTMod) # NIL) THEN
                              bpAdr := GetModuleCodeBase(0) + ModuleBodyCodeOffset;
                            ELSE
                              bpAdr  := NIL;
                              IF MainModFileStatus = FAFErrMainNotFound THEN
                                 ShowError(ADR(ErrMsgNoMainMod));
                              ELSIF MainRfmFileStatus = FAFErrMainNotFound THEN
                                 ShowError(ADR(ErrMsgNoMainRfm));
                              ELSIF MainRfmFileStatus = FAFErrMainBadKey THEN
                                 ShowError(ADR(ErrMsgMainRfmBadKey));
                              ELSIF MainModFileStatus = FAFErrMainBadDate THEN
                                 ShowError(ADR(ErrMsgMainModBadDate));
                              ELSE
                                 ShowError(ADR(ErrMsgNoMainStartup));
                              END;
                            END;
            END;
          ELSIF (ConfigProgLanguageMode = LMAsm) THEN
            CASE (ConfigProgStartupMode) OF
            | SMFirstInst:  bpAdr := firstInst;
            | SMLibModule:  IF (firstRfmMod # InvalidModNo) THEN
                              bpAdr := GetModuleCodeBase(firstRfmMod) + ModuleBodyCodeOffset;
                            ELSE (* firstRfmMod = InvalidModNo *)
                              bpAdr := firstInst;
                            END;
            | SMMainModule: bpAdr := GetModuleCodeBase(0) + ModuleBodyCodeOffset;
            END;
          END;
    
          (* Save contents of area to be patched with jump *)
          PDataPtr := firstInst;
          PData    := PDataPtr^;
    
          (* Patch first instruction of program to jump to InitProcess proc *)
          WITH PDataPtr^ DO
            pdJmp  := InstJMPAbs;
            pdProc := InitProcess;
          END;
          IF OSVersion2 THEN
             CacheClearU();
          END;

          (* Setup debugger for Go Mode *)
          ProgExecMode     := EMGo;
          ProgLangMode     := LMM2;
          ProcChainNew     := TRUE; (* Invalidate Procedure Chain *)
          ProgIsPMDProcess := FALSE;

          (* Continue if an initial break point location has been found *)
          IF (bpAdr # NIL) THEN
            CreateTempBreakPoint(bpAdr);
            ShowStatus(ADR(StatusMsgExecuting));

            IF OSVersion2 THEN

              CatTag(Tags, NPSeglist, segList);
              CatTag(Tags, NPFreeSeglist, ORD(FALSE));
              CatTag(Tags, NPStackSize, ConfigProgStackSize);
              CatTag(Tags, NPName, progName);
              CatTag(Tags, NPPriority, ConfigProgPriority);

            (*CPResult := CreateNewProc(Tags[0]);*)
              SETREG(A6, DOSBase);
              SETREG(D1, ADR(Tags));
              INLINE(4EAEH, -01F2H);              (* JSR LVO(A6)  *)
              CPResult := REG(D0);

            ELSE

              (* CreateProc() makes a copy of the progName string *)
              CPResult := CreateProc(progName, ConfigProgPriority, segList, 
                                     ConfigProgStackSize);
            END;

            (* CreateProc() makes a copy of the progName string *)
            IF CPResult # NIL THEN 
              ProgLoaded := TRUE;
              UpdateWindows(WindowsForStartup);
              SetCommandMode(CMExec);
              RETURN; (* Success! *)
            END;
          END;
     
          DeleteTempBreakPoint();
    
          (* If bpAdr # NIL then error due to insufficient memory *)
          IF (bpAdr # NIL) THEN
            ShowError(ShowErrMsgNoMemory);
          END;
        END; (* IF FindAvailableFiles() *)
        DeleteModuleInfoArray();
      END;
      UnLoadProgram();
    END;

    (* Clear status message *)
    ShowStatus(NIL);
  
    CleanupProgEnvironment();
  END;

  (* Failed! *)
END RunProgramAsSLD;

(* GetDebugMsgPort - Get ptr to msg port used to communicate with program *)
PROCEDURE GetDebugMsgPort(): MsgPortPtr;
BEGIN
  RETURN (DebugPort);
END GetDebugMsgPort;

(* GetRegisterValue - get the contents of the programs register *)
PROCEDURE GetRegisterValue(regNo: CARDINAL): LONGCARD;
BEGIN
  RETURN (DebugMsg.dmCPUInfo.cpuRegList[regNo]);
END GetRegisterValue;

(* GetRegisterPCValue - get the contents of the programs PC register *)
PROCEDURE GetRegisterPCValue(): LONGCARD;
BEGIN
  RETURN (DebugMsg.dmCPUInfo.cpuPC);
END GetRegisterPCValue;

(* GetRegisterSRValue - get the contents of the programs SR register *)
PROCEDURE GetRegisterSRValue(): BITSET;
BEGIN
  RETURN (DebugMsg.dmCPUInfo.cpuSR);
END GetRegisterSRValue;

(* PutRegisterValue - put a value into the programs register *)
PROCEDURE PutRegisterValue(regNo: CARDINAL; value: LONGCARD);
BEGIN
  IF (ProgExecMode = EMWait) THEN
    DebugMsg.dmCPUInfo.cpuRegList[regNo] := value;
    IF (regNo = A5) OR (regNo = A7) THEN

      ProcChainNew := TRUE; (* Invalidate procedure chain *)
      CreateProcedureChain();
      UpdateWindows(WindowsForNewPos);
    END;
  END;
END PutRegisterValue;

(* PutRegisterPCValue - put a value into the PC register *)
PROCEDURE PutRegisterPCValue(value: LONGCARD);
BEGIN
  IF (ProgExecMode = EMWait) THEN
    DebugMsg.dmCPUInfo.cpuPC := value;

    ProcChainNew := TRUE; (* Invalidate procedure chain *)
    CreateProcedureChain();
    UpdateWindows(WindowsForNewPos);
  END;
END PutRegisterPCValue;

(* PutRegisterSRValue - Put a value into the SR register *)
PROCEDURE PutRegisterSRValue(value: BITSET);
BEGIN
  DebugMsg.dmCPUInfo.cpuSR := value;
END PutRegisterSRValue;

(* GetProcChainLength - Get length of procedure call chain *)
PROCEDURE GetProcChainLength(VAR newChain: BOOLEAN; VAR length: CARDINAL);
BEGIN
  newChain := ProcChainNew;
  length   := ProcChainLength;
END GetProcChainLength;

(* GetProcChainNode - get procedure call chain node *)
PROCEDURE GetProcChainNode(nodeNo: CARDINAL): ProcChainNodePtr;
VAR
  pcn: ProcChainNodePtr;
BEGIN
  pcn := ProcChainListHead;
  WHILE (nodeNo # 0) DO
    pcn := pcn^.pcnNext;
    DEC(nodeNo);
  END;
  RETURN (pcn);
END GetProcChainNode;

(* IsProgramLoaded - check if program is currently loaded *)
PROCEDURE IsProgramLoaded(): BOOLEAN;
BEGIN
  RETURN (ProgLoaded);
END IsProgramLoaded;

PROCEDURE IsProgramWaiting(): BOOLEAN;
BEGIN
  RETURN ((ProgLoaded) AND (ProgExecMode = EMWait));
END IsProgramWaiting;

(* ContinueUserProgram - Resume program in exec mode and language mode *)
PROCEDURE ContinueUserProgram(execMode: ExecutionMode; langMode: LanguageMode;
                              tempBP: ADDRESS);
BEGIN
  IF (ProgLoaded) AND (ProgExecMode = EMWait) THEN
    IF (execMode = EMGo) THEN (* Setup for GO Mode *)
      ShowStatus(ADR(StatusMsgExecuting));

      (* Setup temporary break point if one is specified *)
      IF (tempBP # InvalidAbsAdr) THEN
        CreateTempBreakPoint(tempBP);
      END;
  
      (* Check if current address has a break point *)
      IF (IsAdrEnabledBreakPoint(DebugMsg.dmCPUInfo.cpuPC)) THEN (* bp found *)
        INCL(DebugMsg.dmCPUInfo.cpuSR, SRTraceBit);
      ELSE (* no break point at current adr so safe to install bps *)
        InstallBreakPoints();
      END;
    ELSIF (execMode = EMStepIn) OR (execMode = EMStepOver) OR
          (execMode = EMRunIn) OR (execMode = EMRunOver) THEN (* Setup Step/Run mode *)
      ShowStatus(ADR(StatusMsgStepping));
      INCL(DebugMsg.dmCPUInfo.cpuSR, SRTraceBit);
    END;

    IF (execMode = EMGo) OR (execMode = EMStepIn) OR (execMode = EMStepOver) THEN
       SetCommandMode(CMExec);
    ELSIF (execMode = EMRunIn) OR (execMode = EMRunOver) THEN
      SetCommandMode(CMRun);
    END;

    (* Setup debugger for any execution mode *)
    ProcChainNew       := execMode = EMGo; (* GO Mode Invalidates call chain *)
    ProgExecMode       := execMode;
    ProgLangMode       := langMode;
    ProgLastInstCall   := IsInstProcCall(DebugMsg.dmCPUInfo.cpuPC);
    ProgLastInstReturn := IsInstProcEnd(DebugMsg.dmCPUInfo.cpuPC);
    ProgSteppingOver   := FALSE;

    (* Send message to user program to resume execution *)
    PutMsg(ProgPort^, ADR(DebugMsg));
  END;
END ContinueUserProgram;

(* StopUserProgram - Stop program in either RunOver or RunIn mode *)
PROCEDURE StopUserProgram();
BEGIN
  IF (ProgLoaded) THEN
    IF (ProgExecMode = EMRunIn) THEN
      ProgExecMode := EMStepIn;
    ELSIF (ProgExecMode = EMRunOver) THEN
      ProgExecMode := EMStepOver;
    END; (* Any other mode is not possible *)
  END;
END StopUserProgram;

(* ExitUserProgram - exit user program via HALT *)
PROCEDURE ExitUserProgram();
VAR
  modNo: CARDINAL;
  systemDataBase: ADDRESS;
BEGIN
  IF (ProgLoaded) AND (ProgExecMode = EMWait) THEN
    GetModNoFromName(ADR(SystemModuleName), modNo);
    IF (modNo = InvalidModNo) THEN
      ShowError(ADR(ErrMsgNoSystem));
    ELSE
      (* Check if MathBase in System module has is not NIL, then exit *)
      systemDataBase := GetModuleDataBase(modNo);
      IF (GetMemoryLong(systemDataBase + SystemMathBaseOffset) = 0D) THEN
        ShowError(ADR(ErrMsgNoHalt));
      ELSE
        ShowStatus(ADR(StatusMsgExecuting));

        SetCommandMode(CMExec);

        (* Setup debugger for GO mode *)
        ProcChainNew       := TRUE; (* Invalidate procedure chain *)
        ProgExecMode       := EMGo;
        ProgLangMode       := LMAsm;
        ProgLastInstCall   := FALSE;
        ProgLastInstReturn := FALSE;
        ProgSteppingOver   := FALSE;

        (* Set Program Counter to System.HALT procedure *)
        DebugMsg.dmCPUInfo.cpuPC := GetModuleCodeBase(modNo) + SystemHALTProcOffset;

        (* Send message to user program to resume execution *)
        PutMsg(ProgPort^, ADR(DebugMsg));
      END;
    END;
  END;
END ExitUserProgram;

(* HandleMsgUserProgram - handle message to debugger msg port *)
PROCEDURE HandleMsgUserProgram(msg: ADDRESS);
BEGIN
  IF (DebugMsg.dmProgCmd = PPCDone) THEN
    UserProgramDone();
  ELSE (* PPCExecption *)
    EXCL(DebugMsg.dmCPUInfo.cpuSR, SRTraceBit);
    IF (ProgExecMode = EMGo) THEN
      HandleExecModeGo();
    ELSE (* EMStepIn, EMStepOver, EMRunIn, EMRunOver *)
      HandleExecModeStepOrRun();
    END
  END;
END HandleMsgUserProgram;

(* InitDBUserProgram - initialize DBUserProgram module *)
PROCEDURE InitDBUserProgram(): BOOLEAN;
BEGIN
  (* Get pointer to debugger's process structure *)
  DebugProc    := ProcessPtr(FindTask(CurrentTask));

  WITH DebugProc^ DO
    DebugWindowPtr := prWindowPtr; (* Preserve prWindowPtr value *)
    prWindowPtr    := -1D;         (* Don't display requesters! *)
  END;

  (* Get pointer to debug message register dump area *)
  DebugRegList := ADR(DebugMsg.dmCPUInfo.cpuRegList[0]);

  (* Save contents of Debugger's command line interface fields *)
  DebugCLI := BPTRtoAPTR(DebugProc^.prCLI);
  WITH DebugCLI^ DO
    DebugCLIModule       := cliModule;
    DebugCLIDefaultStack := cliDefaultStack;
  END;

  (* Create a debugger communications port *)
  DebugPort := CreatePort(NIL, 0);

  (* Initialize debugger message structure *)
  WITH DebugMsg.dmMessage DO
    mnNode.lnType := NTMessage;
    mnLength      := SIZE(DebugMsg);
    mnReplyPort   := DebugPort;
  END;
  RETURN (DebugPort # NIL);
END InitDBUserProgram;

(* CleanupDBUserProgram - cleanup DBUserProgram module *)
PROCEDURE CleanupDBUserProgram();
BEGIN
  DeleteProcedureChain();

  (* Restore original prWindowPtr value *)
  IF (DebugProc # NIL) THEN
    DebugProc^.prWindowPtr := DebugWindowPtr;
  END;

  (* Delete Debugger's communications port *)
  IF (DebugPort # NIL) THEN
    DeletePort(DebugPort^);
  END;

  (* Restore Debugger's command line interface fields *)
  IF (DebugCLI # NIL) THEN
    WITH DebugCLI^ DO
      cliModule       := DebugCLIModule;
      cliDefaultStack := DebugCLIDefaultStack;
    END;
  END;
END CleanupDBUserProgram;

END DBUserProgram.
