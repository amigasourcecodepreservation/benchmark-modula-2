(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBBreakPointWindows.DEF           Version: Amiga.01.10             *
 * Created: 01/29/89   Updated: 03/21/89   Author: Leon Frenkel             *
 * Description: Debugger break point list windows handling module.          *
 ****************************************************************************)

DEFINITION MODULE DBBreakPointWindows;

FROM DBConfiguration IMPORT
  ConfigWindow;


PROCEDURE OpenBreakPointWindow(VAR config: ConfigWindow): BOOLEAN;
(* Open a break point list window.

   config - new window parameters

   result - TRUE = window opened, FALSE = failed to open *)


PROCEDURE UpdateBreakPointWindows();
(* Update contents of all break point windows. *)


PROCEDURE CloseBreakPointWindows();
(* Close all break point windows. *)


PROCEDURE GetBreakPointWindowInfo(VAR index: CARDINAL;
                                  VAR config: ConfigWindow): BOOLEAN;
(* Get window parameters of a specified window.

   index - variable incremented (index of window to get info from passed in)
   config - variable record set with all window parameters

   result - TRUE = valid window index, FALSE = invalid index *)


PROCEDURE InitDBBreakPointWindows(): BOOLEAN;
(* Initialize DBBreakPointWindows module.

   result - TRUE = successful init, FALSE = init failed *)


PROCEDURE CleanupDBBreakPointWindows();
(* Cleanup DBBreakPointWindows module. *)


END DBBreakPointWindows.
