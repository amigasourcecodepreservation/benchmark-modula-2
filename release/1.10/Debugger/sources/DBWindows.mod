(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBWindows.MOD                     Version: Amiga.01.10             *
 * Created: 01/08/89   Updated: 03/26/89   Author: Leon Frenkel             *
 * Description: Debugger windows management module.                         *
 ****************************************************************************)

IMPLEMENTATION MODULE DBWindows;

FROM SYSTEM IMPORT
  ADDRESS, BYTE,
  ADR;
FROM DBBreakPointWindows IMPORT
  OpenBreakPointWindow, UpdateBreakPointWindows, CloseBreakPointWindows;
FROM DBConfiguration IMPORT
  ConfigWindow, ConfigWindowPtr, ConfigStatusWindowX, ConfigStatusWindowY,
  ConfigStatusWindowDetailPen, ConfigStatusWindowBlockPen,
  ConfigStatusWindowFgColor, ConfigStatusWindowBgColor,
  ConfigPrivateScreen, ConfigScreenWidth, ConfigScreenHeight,ConfigScreenColors,
  ConfigNewWindowTable, ConfigWindowList, WindowInfoProcTable;
FROM DBDirectoryWindows IMPORT
  OpenDirectoryWindow, UpdateDirectoryWindows, CloseDirectoryWindows;
FROM DBDisAsmWindows IMPORT
  OpenDisAsmWindow, UpdateDisAsmWindows, CloseDisAsmWindows;
FROM DBInfoWindows IMPORT
  OpenInfoWindow, UpdateInfoWindows, CloseInfoWindows;
FROM DBInitCleanup IMPORT
  M2DebugTitle;
FROM DBMemoryWindows IMPORT
  OpenMemoryWindow, UpdateMemoryWindows, CloseMemoryWindows;
FROM DBModuleListWindows IMPORT
  OpenModuleListWindow, UpdateModuleListWindows, CloseModuleListWindows;
FROM DBProcChainWindows IMPORT
  OpenProcChainWindow, UpdateProcChainWindows, CloseProcChainWindows;
FROM DBRegistersWindows IMPORT
  OpenRegistersWindow, UpdateRegistersWindows, CloseRegistersWindows;
FROM DBSourceWindows IMPORT
  OpenSourceWindow, UpdateSourceWindows, CloseSourceWindows;
FROM DBStatusWindow IMPORT
  ShowError,
  ShowErrMsgNoMemory,
  OpenStatusWindow;
FROM DBSymDataWindows IMPORT
  OpenSymDataWindow, UpdateSymDataWindows, CloseSymDataWindows;
FROM DBTextWindows IMPORT
  InitVirtualTextWindows, CleanupVirtualTextWindows;
FROM Intuition IMPORT
  ScreenPtr,
  CloseScreen;
FROM Ports IMPORT
  MsgPortPtr;
FROM PortsUtil IMPORT
  CreatePort, DeletePort;
FROM SimpleScreens IMPORT
  CreateScreen;
FROM Views IMPORT
  LoadRGB4;

CONST
  DefMaxCharWidth = 132; (* Max number of chars in one window display line*)
  DefScreenDepth  = 2;   (* Debugger screen depth *)

TYPE
  (* Open any window type procedure definition *)
  OpenWindowProcType = PROCEDURE(VAR ConfigWindow): BOOLEAN;

VAR
  VTWindowsPort        : MsgPortPtr; (* Debugger windows message port *)
  DebugScreen          : ScreenPtr;  (* Debugger screen ptr or NIL(WorkBench) *)
  VTWindowsInitialized : BOOLEAN;    (* T: Virtual windows initialized, F: Not*)

  (* Table of procedures called to open a window.  Indexed by window type *)
  OpenWindowProcTable  : ARRAY WindowClass OF OpenWindowProcType;

    
(* UpdateWindows - update windows of specified types *)
PROCEDURE UpdateWindows(windows: WindowClassSet);
BEGIN
  IF (WCSource IN windows) THEN
    UpdateSourceWindows();
  END;
  IF (WCDisassembly IN windows) THEN
    UpdateDisAsmWindows();
  END;
  IF (WCSymData IN windows) THEN
    UpdateSymDataWindows();
  END;
  IF (WCMemory IN windows) THEN
    UpdateMemoryWindows();
  END;
  IF (WCModuleList IN windows) THEN
    UpdateModuleListWindows();
  END;
  IF (WCProcChain IN windows) THEN
    UpdateProcChainWindows();
  END;
  IF (WCBreakPointList IN windows) THEN
    UpdateBreakPointWindows();
  END;
  IF (WCRegisters IN windows) THEN
    UpdateRegistersWindows();
  END;
  IF (WCInfo IN windows) THEN
    UpdateInfoWindows();
  END;
  IF (WCDirectory IN windows) THEN
    UpdateDirectoryWindows();
  END;
END UpdateWindows;

(* GetVTWindowsMsgPort - get debugger windows message port *)
PROCEDURE GetVTWindowsMsgPort(): MsgPortPtr;
BEGIN
  RETURN (VTWindowsPort);
END GetVTWindowsMsgPort;

(* GetScreen - get pointer to debugger screen structure or NIL for workbench *)
PROCEDURE GetScreen(): ADDRESS;
BEGIN
  RETURN (DebugScreen);
END GetScreen;

(* SetupScreenAndStatusWindow - setup a debugger screen and status window *)
PROCEDURE SetupScreenAndStatusWindow(): BOOLEAN;
VAR
  cw: ConfigWindowPtr;
  openOk: BOOLEAN;
BEGIN
  DebugScreen := NIL;
  IF (ConfigPrivateScreen) THEN
    DebugScreen := CreateScreen(ConfigScreenWidth, ConfigScreenHeight, 
                                DefScreenDepth, M2DebugTitle);
    IF (DebugScreen = NIL) THEN
      RETURN (FALSE); (* error *)
    END;
    LoadRGB4(DebugScreen^.ViewPort, ADR(ConfigScreenColors[0]), 4);
  END;

  IF (NOT OpenStatusWindow()) THEN
    RETURN (FALSE); (* error *)
  END;

  VTWindowsInitialized := InitVirtualTextWindows(DebugScreen, DefMaxCharWidth);
  RETURN (VTWindowsInitialized);
END SetupScreenAndStatusWindow;

(* ConfigScreen - read configuration variables and set debugger screen colors *)
PROCEDURE ConfigScreen();
BEGIN
  IF (DebugScreen # NIL) THEN
    LoadRGB4(DebugScreen^.ViewPort, ADR(ConfigScreenColors[0]), 4);
  END;
END ConfigScreen;

(* OpenConfigWindows - open all windows specified in configuration file *)
PROCEDURE OpenConfigWindows();
VAR
  cw: ConfigWindowPtr;
BEGIN
  cw := ConfigWindowList;
  WHILE (cw # NIL) DO
    IF (NOT OpenWindowProcTable[cw^.cwClass](cw^)) THEN
      ShowError(ShowErrMsgNoMemory);
      RETURN; (* Error! *)
    END;
    cw := cw^.cwNext;
  END;
END OpenConfigWindows;

(* OpenNewWindow - open a window of a specified type *)
PROCEDURE OpenNewWindow(class: WindowClass);
BEGIN
  IF (NOT OpenWindowProcTable[class](ConfigNewWindowTable[class])) THEN
    ShowError(ShowErrMsgNoMemory);
  END;
END OpenNewWindow;

(* DuplicateWindow - open a new window using a specified window as template *)
PROCEDURE DuplicateWindow(class: WindowClass; index: CARDINAL);
VAR
  config: ConfigWindow;
BEGIN
  IF (WindowInfoProcTable[class](index, config)) THEN
    WITH config DO
      cwX := 0;
      cwY := 0;
    END;
    IF (NOT OpenWindowProcTable[class](config)) THEN
      ShowError(ShowErrMsgNoMemory);
    END;
  END;
END DuplicateWindow;

(* CloseUserWindows - close all user created windows *)
PROCEDURE CloseUserWindows();
BEGIN
  CloseSourceWindows();
  CloseDisAsmWindows();
  CloseSymDataWindows();
  CloseMemoryWindows();
  CloseModuleListWindows();
  CloseProcChainWindows();
  CloseBreakPointWindows();
  CloseRegistersWindows();
  CloseInfoWindows();
  CloseDirectoryWindows();
END CloseUserWindows;

(* InitDBWindows - Initialize DBWindows module *)
PROCEDURE InitDBWindows(): BOOLEAN;
BEGIN
  OpenWindowProcTable[WCSource]         := OpenSourceWindow;
  OpenWindowProcTable[WCDisassembly]    := OpenDisAsmWindow;
  OpenWindowProcTable[WCSymData]        := OpenSymDataWindow;
  OpenWindowProcTable[WCMemory]         := OpenMemoryWindow;
  OpenWindowProcTable[WCModuleList]     := OpenModuleListWindow;
  OpenWindowProcTable[WCProcChain]      := OpenProcChainWindow;
  OpenWindowProcTable[WCBreakPointList] := OpenBreakPointWindow;
  OpenWindowProcTable[WCRegisters]      := OpenRegistersWindow;
  OpenWindowProcTable[WCInfo]           := OpenInfoWindow;
  OpenWindowProcTable[WCDirectory]      := OpenDirectoryWindow;

  VTWindowsPort := CreatePort(NIL, 0);
  RETURN (VTWindowsPort # NIL);
END InitDBWindows;

(* CleanupDBWindows - cleanup DBWindows module *)
PROCEDURE CleanupDBWindows();
BEGIN
  IF (VTWindowsInitialized) THEN
    CleanupVirtualTextWindows();
  END;
  IF (DebugScreen # NIL) THEN
    CloseScreen(DebugScreen^);
  END;
  IF (VTWindowsPort # NIL) THEN
    DeletePort(VTWindowsPort^);
  END;
END CleanupDBWindows;

END DBWindows.
