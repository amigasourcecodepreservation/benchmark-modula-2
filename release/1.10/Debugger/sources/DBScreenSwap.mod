(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBScreenSwap.MOD                  Version: Amiga.01.10             *
 * Created: 03/24/89   Updated: 03/28/89   Author: Leon Frenkel             *
 * Description: Debugger screen swapping module.                            *
 ****************************************************************************)

IMPLEMENTATION MODULE DBScreenSwap;


FROM SYSTEM IMPORT
  ADR, INLINE, REG,
  ADDRESS, BYTE;
FROM AmigaDOSProcess IMPORT
  ProcessPtr;
FROM ConsoleDevice IMPORT
  ConsoleBase,
  RawKeyConvert;
FROM DBWindows IMPORT
  GetScreen;
FROM InputDevice IMPORT
  INDAddHandler, INDRemHandler;
FROM InputEvents IMPORT
  InputEventPtr, IEClassRawKey, IEQualifierLCommand;
FROM Interrupts IMPORT
  Interrupt,
  Forbid, Permit;
FROM Intuition IMPORT
  ScreenPtr,
  ScreenToFront, ScreenToBack, OpenWorkBench;
FROM IntuitionBase IMPORT
  IntuitionBasePtr,
  LockIBase, UnlockIBase;
FROM IODevices IMPORT
  IOStdReqPtr,
  OpenDevice, CloseDevice, DoIO;
FROM IODevicesUtil IMPORT
  CreateStdIO, DeleteStdIO;
FROM Ports IMPORT
  MsgPortPtr;
FROM PortsUtil IMPORT
  CreatePort, DeletePort;
FROM System IMPORT
  IntuitionBase;
FROM Tasks IMPORT
  AnySignal, NoSignals, CurrentTask, SignalSet,
  AllocSignal, FreeSignal, Signal, FindTask;

CONST
  SwapScreenKey = "S"; (* Key pressed to perform a screen swap *)

  InputHandlerPriority = 51; (* Priority of handler must be above Intuition *)

VAR
  DebugProc              : ProcessPtr;
  InputDeviceOpen        : BOOLEAN;
  HandlerSignalAllocated : BOOLEAN;
  InputDevicePort        : MsgPortPtr;
  HandlerSignal          : INTEGER;
  InputDeviceRequest     : IOStdReqPtr;
  InputDeviceInterrupt   : Interrupt;
  IntuitionBaseAdr       : IntuitionBasePtr;
  PrevScreen             : ScreenPtr;
  DebugScreen            : ScreenPtr;

(* ===== PRIVATE ===== *)

(* WARNING:
 * The input handler performs RawKey -> Ascii conversion using RawKeyConvert()
 * which require ConsoleBase to be setup.  This setup is performed in DBMisc()
 * which should be the first module initialized
 *)

(* InputHandler - input event handler used for screen swapping *)
PROCEDURE InputHandler(): InputEventPtr;
CONST
  A0 = 8;

  KeyBufSize = 32;

VAR
  eventList: InputEventPtr;
  eventPrev: InputEventPtr;
  event: InputEventPtr;
  keyBuf: ARRAY [0..KeyBufSize-1] OF CHAR;
  keySize: INTEGER;
BEGIN
  INLINE(48E7H, 03F3EH); (* MOVEM.L D2-D7/A2-A6,-(SP) *)

  eventList := ADDRESS(REG(A0)); (* Get input event chain ptr *)
  eventPrev := NIL;

  event := eventList;
  WHILE (event # NIL) DO
    WITH event^ DO
      (* Handle only RawKey down events with Left Amiga Qualifier *)
      IF (ieClass = IEClassRawKey) AND (ieCode < 80H) AND 
         (IEQualifierLCommand IN ieQualifier) THEN

        IF (ConsoleBase # NIL) THEN (* ConsoleBase initialized *)
          keySize := RawKeyConvert(event^, ADR(keyBuf[0]), KeyBufSize, NIL);

          IF (keySize = 1) AND (CAP(keyBuf[0]) = SwapScreenKey) THEN
            (* Valid screen swap command hit *)

            (* Unlink current event from chain *)
            IF (eventPrev # NIL) THEN (* Link next event to previous *)
              eventPrev^.ieNextEvent := ieNextEvent;
            ELSE (* current event is first in list *)
              eventList := ieNextEvent; (* make next event first in list *)
            END;

            Signal(DebugProc^.prTask, SignalSet{CARDINAL(HandlerSignal)});
          END; (* IF *)
        END; (* IF *)
      END; (* IF *)
    END; (* WITH *)
    event := event^.ieNextEvent; (* advance to next event in chain *)
  END;

  INLINE(4CDFH, 07CFCH); (* MOVEM.L (SP)+,D2-D7/A2-A6 *)
  RETURN (eventList);
END InputHandler;

(* ===== PUBLIC ===== *)

(* GetScreenSwapSignal - return signal used by input handler *)
PROCEDURE GetScreenSwapSignal(): CARDINAL;
BEGIN
  RETURN (HandlerSignal);
END GetScreenSwapSignal;

(* HandleSignalScreenSwap - called when input handler has signaled debugger *)
PROCEDURE HandleSignalScreenSwap();
VAR
  scr: ScreenPtr;
  lock: LONGCARD;
  firstScr: ScreenPtr;
BEGIN
  IF (DebugScreen = NIL) THEN (* Initialize ptr to debugger screen *)
    DebugScreen := GetScreen();
    IF (DebugScreen = NIL) THEN (* Debugger on WorkBench *)
      DebugScreen := OpenWorkBench();
    END;
  END;

  Forbid(); (* Disable task switching *)

  (* Lock Intuition base *)
  lock := LockIBase(0D);

  (* Get ptr to top screen *)
  firstScr := IntuitionBaseAdr^.FirstScreen;

  IF (firstScr = DebugScreen) THEN (* Swap to previous screen *)
    IF (PrevScreen = NIL) THEN (* No previous screen *)
      ScreenToBack(DebugScreen^); (* move debugger screen to back *)
    ELSE
      (* Determine if previous screen still exists *)
      WHILE (firstScr # NIL) AND (firstScr # PrevScreen) DO
        firstScr := firstScr^.NextScreen;
      END;
      IF (firstScr # NIL) THEN (* screen still open *)
        ScreenToFront(PrevScreen^); (* move previous screen to front *)
      ELSE (* prev screen is gone *)
        PrevScreen := NIL; (* No previous screen *)
        ScreenToBack(DebugScreen^); (* move debugger screen to back *)
      END;
    END;
  ELSE (* firstScr # DebugScreen *)
    PrevScreen := firstScr;      (* Save ptr to current screen *)
    ScreenToFront(DebugScreen^); (* Move debugger screen to front *)
  END;

  (* Unlock Intuition base *)
  UnlockIBase(lock);

  Permit(); (* Enable task switching *)
END HandleSignalScreenSwap;

(* InitDBScreenSwap - initialize module *)
PROCEDURE InitDBScreenSwap(): BOOLEAN;
VAR
  ok: BOOLEAN;
  r: LONGINT;
BEGIN
  ok := FALSE;

  (* Get ptr to debugger's process structure *)
  DebugProc := ProcessPtr(FindTask(CurrentTask));

  (* Get ptr to intuition base *)
  IntuitionBaseAdr := IntuitionBase;

  LOOP
    HandlerSignal          := AllocSignal(AnySignal);
    HandlerSignalAllocated := HandlerSignal # NoSignals;
    IF (NOT HandlerSignalAllocated) THEN
      EXIT; (* Failed! *)
    END;

    InputDevicePort := CreatePort(NIL, 0);
    IF (InputDevicePort = NIL) THEN
      EXIT; (* Failed! *)
    END;

    InputDeviceRequest := CreateStdIO(InputDevicePort^);
    IF (InputDeviceRequest = NIL) THEN
      EXIT; (* Failed! *)
    END;

    InputDeviceOpen := OpenDevice(ADR("input.device"),0D,InputDeviceRequest,0D) = 0D;
    IF (NOT InputDeviceOpen) THEN
      EXIT; (* Failed! *)
    END;

    WITH InputDeviceInterrupt DO
      isData       := NIL;
      isCode       := ADR(InputHandler);
      isNode.lnPri := BYTE(InputHandlerPriority);
    END;

    WITH InputDeviceRequest^ DO
      ioCommand := INDAddHandler;
      ioData    := ADR(InputDeviceInterrupt);
    END;

    r := DoIO(InputDeviceRequest);

    ok := TRUE;
    EXIT; (* Success! *)
  END; (* LOOP *)

  RETURN (ok);
END InitDBScreenSwap;

(* CleanupDBScreenSwap - cleanup module *)
PROCEDURE CleanupDBScreenSwap();
VAR
  r: LONGINT;
BEGIN
  IF (InputDeviceOpen) THEN
    WITH InputDeviceRequest^ DO
      ioCommand := INDRemHandler;
    END;
    r := DoIO(InputDeviceRequest);

    CloseDevice(InputDeviceRequest);
  END;

  IF (InputDeviceRequest # NIL) THEN
    DeleteStdIO(InputDeviceRequest);
  END;

  IF (InputDevicePort # NIL) THEN
    DeletePort(InputDevicePort^);
  END;

  IF (HandlerSignalAllocated) THEN
    FreeSignal(HandlerSignal);
  END;
END CleanupDBScreenSwap;

END DBScreenSwap.
