(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBModuleListWindows.MOD           Version: Amiga.01.10             *
 * Created: 01/29/89   Updated: 03/25/89   Author: Leon Frenkel             *
 * Description: Debugger module list windows handling module.               *
 ****************************************************************************)

IMPLEMENTATION MODULE DBModuleListWindows;

FROM SYSTEM IMPORT
  ADDRESS, BYTE,
  ADR;
FROM CPrintBuffer IMPORT
  sprintf;
FROM CStrings IMPORT
  strlen;
FROM DBConfiguration IMPORT
  ConfigWindow;
FROM DBFiles IMPORT
  FileType, Item,
  GetModuleCount, GetModuleName, GetFilePath, GetFilesLoadStatus,
  GetSourceFileDef, GetSourceFileMod, FreeSourceFileDef, FreeSourceFileMod;
FROM DBMenusKbd IMPORT
  DoGlobalMenuCommand, DoGlobalKbdCommand, LinkGlobalMenuStrip,
  AddMenuItemNoHighlight;
FROM DBMisc IMPORT
  AllocMemory, DeallocMemory;
FROM DBRequesters IMPORT
  RequestTextFgBgColors, RequestTextWidth;
FROM DBSelection IMPORT
  SelectionData, ModuleFileType,
  ClearSelection, GetSelection, SetSelection, GetSelectionLineInfo,
  IsPosSelected, IfCurrentWindowClearSelection;
FROM DBStatusWindow IMPORT
  ShowErrMsgNoMemory, ShowErrMsgBadDest,
  ShowError;
FROM DBTextWindows IMPORT
  DefMinWindowWidth, DefMinWindowHeight, DefMaxWindowWidth, DefMaxWindowHeight,
  VTWindow, NewVTWindow, VTWindowNIL, VTWindowParms,
  OpenVTWindow, CloseVTWindow, InitParmsVTWindow, WriteBufferVTWindow,
  SetCursorPosVTWindow, GetUserDataVTWindow, WriteColorVTWindow, ClearVTWindow,
  RefreshVTWindow, GetInfoVTWindow, SetMenuStripVTWindow, 
  ClearMenuStripVTWindow, GetDetailPenBlockPenVTWindow,
  SetDetailPenBlockPenVTWindow, SetDefaultColorVTWindow;
FROM DBUserProgram IMPORT
  IsProgramLoaded;
FROM DBWindows IMPORT
  WindowClass,
  GetVTWindowsMsgPort;
FROM FormatString IMPORT
  FormatArg;
FROM Intuition IMPORT
  Menu, MenuPtr;
FROM Memory IMPORT
  MemReqSet, MemClear;
FROM SimpleMenus IMPORT
  BeginMenuStrip, EndMenuStrip, FreeMenuStrip, AddMenu, AddMenuItem;

CONST
  (* Module List Window Title *)
  DefModuleListWindowTitle = "Module List";

  LocalMenus = 1; (* Number of window specific menus *)

  OutputBufSize = 1024D; (* Output buffer size in VTUpdateProc() *)

  (* File Status Indicators *)
  LDEFFileExt = "DEF"; (* def file loaded *)
  LMODFileExt = "MOD"; (* mod file loaded *)
  LSBMFileExt = "SBM"; (* sbm file loaded *)
  LRFMFileExt = "RFM"; (* rfm file loaded *)
  UDEFFileExt = "def"; (* def file not loaded *)
  UMODFileExt = "mod"; (* mod file not loaded *)
  USBMFileExt = "sbm"; (* sbm file not loaded *)
  URFMFileExt = "rfm"; (* rfm file not loaded *)
  NILFileExt  = "---"; (* file not available *)

  (* File status indicator positions *) 
  DefInfoStartPos  = 5;  (* start of def file indicator *)
  DefInfoEndPos    = 7;  (* end of def file indicator *)
  ModInfoStartPos  = 9;  (* start of mod file indicator *)
  ModInfoEndPos    = 11; (* end of mod file indicator *)
  SymInfoStartPos  = 13; (* start of sbm/rfm file indicator *)
  SymInfoEndPos    = 15; (* end of sbm/rfm file indicator *)
  NameInfoStartPos = 17; (* start of module name *)

  ModuleInfoWidth  = 17; (* Width of text before module name *)

TYPE
  (* Module List window descriptor *)
  ModuleListWindowPtr = POINTER TO ModuleListWindow;
  ModuleListWindow = RECORD
                       mwNext          : ModuleListWindowPtr; (* next window *)
                       mwVTWindow      : VTWindow;      (* text window handle *)
                       mwVTWindowParms : VTWindowParms; (* text window parms *)
                       mwDisplayOn     : BOOLEAN;       (* displaying data *)
                     END;

VAR
  ModuleListWindowList : ModuleListWindowPtr; (* list of module list windows *)
  ModuleListMenuStrip  : MenuPtr;             (* module list window menu strip*)

(* ===== PRIVATE ===== *)

(* AddWindowNode - create a window node and link into window list *)
PROCEDURE AddWindowNode(): ModuleListWindowPtr;
VAR
  mw: ModuleListWindowPtr;
BEGIN
  mw := AllocMemory(SIZE(ModuleListWindow), MemReqSet{MemClear});
  IF (mw # NIL) THEN
    IF (ModuleListWindowList # NIL) THEN
      mw^.mwNext := ModuleListWindowList;
    END;
    ModuleListWindowList := mw;
  END;
  RETURN (mw);
END AddWindowNode;

(* DeleteWindowNode - delete window node and unlink from window list *)
PROCEDURE DeleteWindowNode(mw: ModuleListWindowPtr);
VAR
  prev: ModuleListWindowPtr;
  cur: ModuleListWindowPtr;
BEGIN
  prev := NIL;
  cur  := ModuleListWindowList;
  WHILE (cur # mw) DO
    prev := cur;
    cur  := cur^.mwNext;
  END;

  IF (prev = NIL) THEN (* First in list *)
    ModuleListWindowList := mw^.mwNext;
  ELSE (* Not first in list *)
    prev^.mwNext := mw^.mwNext;
  END;

  DeallocMemory(mw);
END DeleteWindowNode;

(* GetWindowIndex - get position of window node in list *)
PROCEDURE GetWindowIndex(mw: ModuleListWindowPtr): CARDINAL;
VAR
  index: CARDINAL;
  p: ModuleListWindowPtr;
BEGIN
  index := 0;
  p     := ModuleListWindowList;
  WHILE (mw # p) DO
    INC(index);
    p := p^.mwNext;
  END;
  RETURN (index);
END GetWindowIndex;

(* CloseModuleListWindow - close module list window *)
PROCEDURE CloseModuleListWindow(VAR mw: ModuleListWindow);
BEGIN
  WITH mw DO
    IfCurrentWindowClearSelection(mwVTWindow);

    ClearMenuStripVTWindow(mwVTWindow);

    CloseVTWindow(mwVTWindow);
  END;
  DeleteWindowNode(ADR(mw));
END CloseModuleListWindow;

(* CloseModuleListWindowNoMemory - close window and show no memory err msg *)
PROCEDURE CloseModuleListWindowNoMemory(VAR mw: ModuleListWindow);
BEGIN
  CloseModuleListWindow(mw);
  ShowError(ShowErrMsgNoMemory);
END CloseModuleListWindowNoMemory;

(* ClearModuleListWindow - disable display of data in window *)
PROCEDURE ClearModuleListWindow(VAR mw: ModuleListWindow);
BEGIN
  WITH mw DO
    IF (mwDisplayOn) THEN
      IfCurrentWindowClearSelection(mwVTWindow);
      mwDisplayOn := FALSE;
      WITH mwVTWindowParms DO
        vtpInitialLine := 0;
        vtpTotalLines  := 1;
        vtpMaxColumns  := 1;
      END;
      IF (NOT InitParmsVTWindow(mwVTWindow, mwVTWindowParms)) THEN
        CloseModuleListWindowNoMemory(mw);
      END;
    END;
  END;
END ClearModuleListWindow;

(* UnLoadSelectedFile - unload currently selected file *)
PROCEDURE UnLoadSelectedFile();
VAR
  selData: SelectionData;
BEGIN
  IF (GetSelection(selData)) THEN
    WITH selData DO
      IF (sdWindowClass = WCModuleList) THEN
        IF (sdModFileType = MFTDef) THEN
          FreeSourceFileDef(sdModNo);
        ELSIF (sdModFileType = MFTMod) THEN
          FreeSourceFileMod(sdModNo);
        END;
      END;
    END;
  END;
END UnLoadSelectedFile;

(* SetNewModuleSelection - setup new user selection based on x, y position *)
PROCEDURE SetNewModuleSelection(VAR mw: ModuleListWindow; x, y: CARDINAL);
VAR
  data: SelectionData;
  startPos: CARDINAL;
  endPos: CARDINAL;
  ok: BOOLEAN;
  modType: ModuleFileType;
BEGIN
  ok := FALSE;
  IF ((DefInfoStartPos <= x) AND (DefInfoEndPos >= x)) THEN
    IF (GetFilePath(y, FTDef) # NIL) THEN
      modType  := MFTDef;
      startPos := DefInfoStartPos;
      endPos   := DefInfoEndPos;
      ok       := TRUE;
    END;
  ELSIF ((ModInfoStartPos <= x) AND (ModInfoEndPos >= x)) THEN
    IF (GetFilePath(y, FTMod) # NIL) THEN
      modType  := MFTMod;
      startPos := ModInfoStartPos;
      endPos   := ModInfoEndPos;
      ok       := TRUE;
    END;
  ELSIF ((SymInfoStartPos <= x) AND (SymInfoEndPos >= x)) THEN
    IF (GetFilePath(y, FTRfm) # NIL) THEN
      modType := MFTRfm;
      ok      := TRUE;
    ELSIF (GetFilePath(y, FTSbm) # NIL) THEN
      modType := MFTSbm;
      ok      := TRUE;
    END;

    startPos := SymInfoStartPos;
    endPos   := SymInfoEndPos;
  ELSIF (NameInfoStartPos <= x) THEN
    modType  := MFTNone;
    startPos := NameInfoStartPos;
    endPos   := mw.mwVTWindowParms.vtpMaxColumns - 1;
    ok       := TRUE;
  END;

  IF (ok) THEN
    WITH data DO
      sdWindowClass := WCModuleList;
      sdVTWindow    := mw.mwVTWindow;
      sdLineNo      := y;
      sdStartPos    := startPos;
      sdEndPos      := endPos;
      sdDefFgColor  := mw.mwVTWindowParms.vtpDefFgColor;
      sdDefBgColor  := mw.mwVTWindowParms.vtpDefBgColor;
      sdModNo       := y;
      sdModFileType := modType;
    END;
    SetSelection(data);
  ELSE
    ClearSelection();
  END;
END SetNewModuleSelection;

(* CalcModuleListWidthAndLength - calculate length and longest line in list *)
PROCEDURE CalcModuleListWidthAndLength(VAR width, length: CARDINAL);
VAR
  modNo: CARDINAL;
  modCount: CARDINAL;
  maxWidth: CARDINAL;
  curWidth: CARDINAL;
BEGIN
  modCount := GetModuleCount();

  maxWidth := 0;
  FOR modNo := 0 TO modCount - 1 DO
    curWidth := strlen(GetModuleName(modNo));
    IF (curWidth > maxWidth) THEN
      maxWidth := curWidth;
    END;
  END;

  width  := maxWidth;
  length := modCount;
END CalcModuleListWidthAndLength;

PROCEDURE UpdateModuleListWindow(VAR mw: ModuleListWindow);
VAR
  listWidth: CARDINAL;
  listLength: CARDINAL;
BEGIN
  IF (IsProgramLoaded()) THEN
    WITH mw DO
      IF (mwDisplayOn) THEN
        ClearVTWindow(mwVTWindow);
        RefreshVTWindow(mwVTWindow);
      ELSE
        CalcModuleListWidthAndLength(listWidth, listLength);
        mwVTWindowParms.vtpTotalLines := listLength;
        mwVTWindowParms.vtpMaxColumns := ModuleInfoWidth + listWidth;
        mwDisplayOn := TRUE;
        IF (NOT InitParmsVTWindow(mwVTWindow, mwVTWindowParms)) THEN
          CloseModuleListWindowNoMemory(mw);
        END;
      END;
    END;
  ELSE
    ClearModuleListWindow(mw);
  END;
END UpdateModuleListWindow;

(* VTUpdateProc - called to output new text to window *)
PROCEDURE VTUpdateProc(Window: VTWindow; OutputRow,FirstLine,LastLine: CARDINAL);
VAR
  mw: ModuleListWindowPtr;
  outBuf: ADDRESS;
  outLength: CARDINAL;
  line: CARDINAL;
  startPos: CARDINAL;
  endPos: CARDINAL;
  modLoaded: BOOLEAN;
  defLoaded: BOOLEAN;
  sbmLoaded: BOOLEAN;
  rfmLoaded: BOOLEAN;
  argo: ARRAY [0..9] OF FormatArg;
BEGIN
  mw := GetUserDataVTWindow(Window);
  IF (mw^.mwDisplayOn) THEN
    outBuf := AllocMemory(OutputBufSize, MemReqSet{});
    IF (outBuf = NIL) THEN RETURN; END;

    FOR line := FirstLine TO LastLine DO
      SetCursorPosVTWindow(Window, 0, OutputRow);

      GetFilesLoadStatus(line, modLoaded, defLoaded, sbmLoaded, rfmLoaded);

      argo[0].W := line + 1;
      IF (GetFilePath(line, FTDef) # NIL) THEN
        IF (defLoaded) THEN
          argo[1].L := ADR(LDEFFileExt);
        ELSE
          argo[1].L := ADR(UDEFFileExt);
        END;
      ELSE
        argo[1].L := ADR(NILFileExt);
      END;

      IF (GetFilePath(line, FTMod) # NIL) THEN
        IF (modLoaded) THEN
          argo[2].L := ADR(LMODFileExt);
        ELSE
          argo[2].L := ADR(UMODFileExt);
        END;
      ELSE
        argo[2].L := ADR(NILFileExt);
      END;

      IF (GetFilePath(line, FTRfm) # NIL) THEN
        IF (rfmLoaded) THEN
          argo[3].L := ADR(LRFMFileExt);
        ELSE
          argo[3].L := ADR(URFMFileExt);
        END;
      ELSE
        IF (GetFilePath(line, FTSbm) # NIL) THEN
          IF (sbmLoaded) THEN
            argo[3].L := ADR(LSBMFileExt);
          ELSE
            argo[3].L := ADR(USBMFileExt);
          END;
        ELSE
          argo[3].L := ADR(NILFileExt);
        END;
      END;

      argo[4].L := GetModuleName(line);
      outLength := sprintf(outBuf, ADR("%03u: %s %s %s %s"), argo);

      WriteBufferVTWindow(Window, outBuf, outLength);

      IF (GetSelectionLineInfo(Window, line, startPos, endPos)) THEN
        WITH mw^.mwVTWindowParms DO
          WriteColorVTWindow(Window, OutputRow, startPos, endPos,
                             vtpDefBgColor, vtpDefFgColor);
        END;
      END;

      INC(OutputRow);
    END;
    DeallocMemory(outBuf);
  END;
END VTUpdateProc;

(* VTSelectProc - called when left mouse button click in window *)
PROCEDURE VTSelectProc(Window: VTWindow; x, y: CARDINAL; pressed: BOOLEAN);
VAR
  mw: ModuleListWindowPtr;
BEGIN
  mw := GetUserDataVTWindow(Window);

  IF (mw^.mwDisplayOn) THEN
    IF (pressed) THEN
      IF (IsPosSelected(Window, x, y)) THEN
        ClearSelection();
      ELSE
        SetNewModuleSelection(mw^, x, y);
      END;
    ELSE
      IF (NOT IsPosSelected(Window, x, y)) THEN
        SetNewModuleSelection(mw^, x, y);
      END;
    END;
  END;
END VTSelectProc;

(* VTMenuProc - called when menu selection made *)
PROCEDURE VTMenuProc(Window: VTWindow; MenuNum, ItemNum, SubNum: CARDINAL);
VAR
  mw: ModuleListWindowPtr;
  fg: BYTE;
  bg: BYTE;
BEGIN
  mw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalMenuCommand(MenuNum, ItemNum, SubNum, LocalMenus,
                              WCModuleList, GetWindowIndex(mw),
                              mw^.mwVTWindow)) THEN
    WITH mw^ DO
      IF (ItemNum # 0) THEN (* If NOT UnLoad Source File *)
        IfCurrentWindowClearSelection(mwVTWindow);
      END;

      CASE (ItemNum) OF
      | 0: (* UnLoad Source File *)
           UnLoadSelectedFile();
      (*1: -------------------- *)
      | 2: (* Set Window Color *)
           GetDetailPenBlockPenVTWindow(mwVTWindow, fg, bg);
           IF (RequestTextFgBgColors(fg, bg)) THEN
             SetDetailPenBlockPenVTWindow(mwVTWindow, fg, bg);
           END;
      | 3: (* Set Text Color *)
           WITH mwVTWindowParms DO
             IF (RequestTextFgBgColors(vtpDefFgColor, vtpDefBgColor)) THEN
               SetDefaultColorVTWindow(mwVTWindow, vtpDefFgColor,vtpDefBgColor);
             END;
           END;
      END;
    END;
  END;
END VTMenuProc;

(* VTDestProc - called when user clicks on destination-data gadget *)
PROCEDURE VTDestProc(Window: VTWindow);
BEGIN
  ShowError(ShowErrMsgBadDest);
END VTDestProc;

(* VTKeyProc - called when user types key into window *)
PROCEDURE VTKeyProc(Window: VTWindow; keyBuf: ADDRESS; keyLength: CARDINAL);
VAR
  mw: ModuleListWindowPtr;
BEGIN
  mw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalKbdCommand(keyBuf, keyLength, WCModuleList,
                             GetWindowIndex(mw))) THEN
  END;
END VTKeyProc;

(* VTCloseProc - called when close-box hit *)
PROCEDURE VTCloseProc(Window: VTWindow);
VAR
  mw: ModuleListWindowPtr;
BEGIN
  mw := GetUserDataVTWindow(Window);
  CloseModuleListWindow(mw^);
END VTCloseProc;

(* VTErrorProc - called when failed to alloc text info for resized window *)
PROCEDURE VTErrorProc(Window: VTWindow);
VAR
  mw: ModuleListWindowPtr;
BEGIN
  mw := GetUserDataVTWindow(Window);
  CloseModuleListWindowNoMemory(mw^);
END VTErrorProc;

(* ===== PUBLIC ===== *)

(* OpenModuleListWindow - open a new module list window *)
PROCEDURE OpenModuleListWindow(VAR config: ConfigWindow): BOOLEAN;
VAR
  mw: ModuleListWindowPtr;
  ok: BOOLEAN;
  nvt: NewVTWindow;
BEGIN
  ok := FALSE;
  mw := AddWindowNode();
  IF (mw # NIL) THEN
    WITH nvt DO
      WITH config DO
        nvtLeftEdge  := cwX;
        nvtTopEdge   := cwY;
        nvtWidth     := cwWidth;
        nvtHeight    := cwHeight;
        nvtDetailPen := cwDetailPen;
        nvtBlockPen  := cwBlockPen;
        nvtTitle     := ADR(DefModuleListWindowTitle);
        nvtMinWidth  := DefMinWindowWidth;
        nvtMinHeight := DefMinWindowHeight;
        nvtMaxWidth  := DefMaxWindowWidth;
        nvtMaxHeight := DefMaxWindowHeight;
        nvtMsgPort   := GetVTWindowsMsgPort();
        nvtUserData  := mw;
      END; (* WITH *)
    END; (* WITH *)
    mw^.mwVTWindow := OpenVTWindow(nvt);
    ok := mw^.mwVTWindow # VTWindowNIL;

    IF (ok) THEN
      WITH mw^.mwVTWindowParms DO
        WITH config DO
          vtpUpdateProc    := VTUpdateProc;
          vtpSelectProc    := VTSelectProc;
          vtpMenuProc      := VTMenuProc;
          vtpDestProc      := VTDestProc;
          vtpKeyProc       := VTKeyProc;
          vtpCloseProc     := VTCloseProc;
          vtpErrorProc     := VTErrorProc;
        (*vtpInitialLine   := 0;*)
        (*vtpTotalLines    := 1;*)
        (*vtpInitialColumn := 0;*)
        (*vtpMaxColumns    := 1;*)
          vtpDefFgColor    := cwFgColor;
          vtpDefBgColor    := cwBgColor;
        END;
      END;

      WITH mw^ DO
        mwDisplayOn  := TRUE;
      END;
      SetMenuStripVTWindow(mw^.mwVTWindow, ModuleListMenuStrip^);

      ClearModuleListWindow(mw^);
      UpdateModuleListWindow(mw^);
    ELSE (* NOT ok *)
      DeleteWindowNode(mw);
    END;
  END;
  RETURN (ok);
END OpenModuleListWindow;

(* UpdateModuleListWindows - update contents of all module list windows *)
PROCEDURE UpdateModuleListWindows();
VAR
  mw: ModuleListWindowPtr;
  p: ModuleListWindowPtr;
BEGIN
  mw := ModuleListWindowList;
  WHILE (mw # NIL) DO
    p  := mw;
    mw := mw^.mwNext;
    UpdateModuleListWindow(p^);
  END;
END UpdateModuleListWindows;

(* CloseModuleListWindows - close all module list windows *)
PROCEDURE CloseModuleListWindows();
BEGIN
  WHILE (ModuleListWindowList # NIL) DO
    CloseModuleListWindow(ModuleListWindowList^);
  END;
END CloseModuleListWindows;

(* GetModuleListWindowInfo - get config parameters of a specified window *)
PROCEDURE GetModuleListWindowInfo(VAR index: CARDINAL;
                                  VAR config: ConfigWindow): BOOLEAN;
VAR
  mw: ModuleListWindowPtr;
  count: CARDINAL;
BEGIN
  count := index;
  mw    := ModuleListWindowList;
  WHILE (mw # NIL) DO
    IF (count = 0) THEN
      WITH config DO
        WITH mw^ DO
          GetInfoVTWindow(mwVTWindow, cwX, cwY, cwWidth, cwHeight,
                          cwDetailPen, cwBlockPen);
          cwMaxCharWidth := 0;
          cwFgColor      := mwVTWindowParms.vtpDefFgColor;
          cwBgColor      := mwVTWindowParms.vtpDefBgColor;
        END;
      END;
      INC(index);
      RETURN (TRUE); (* Window Found! *)
    END;
    DEC(count);
    mw := mw^.mwNext;
  END;
  RETURN (FALSE); (* End of list *)
END GetModuleListWindowInfo;

(* InitDBModuleListWindows - initialize module *)
PROCEDURE InitDBModuleListWindows(): BOOLEAN;
BEGIN
  BeginMenuStrip();
    AddMenu(                           ADR("Module-List "));
      AddMenuItem(                       ADR(" UnLoad Source File "));
      AddMenuItemNoHighlight(            ADR(" ------------------"));
      AddMenuItem(                       ADR(" Set Window Color"));
      AddMenuItem(                       ADR(" Set Text Color"));
  ModuleListMenuStrip := EndMenuStrip();
  IF (ModuleListMenuStrip # NIL) THEN
    LinkGlobalMenuStrip(ModuleListMenuStrip^);
  END;
  RETURN (ModuleListMenuStrip # NIL);
END InitDBModuleListWindows;

(* CleanupDBModuleListWindows - cleanup module *)
PROCEDURE CleanupDBModuleListWindows();
BEGIN
  CloseModuleListWindows();

  IF (ModuleListMenuStrip # NIL) THEN
    FreeMenuStrip(ModuleListMenuStrip^);
  END;
END CleanupDBModuleListWindows;

END DBModuleListWindows.
