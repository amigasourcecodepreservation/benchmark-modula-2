(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBProcChainWindows.MOD            Version: Amiga.01.10             *
 * Created: 01/13/89   Updated: 03/23/89   Author: Leon Frenkel             *
 * Description: Debugger procedure chain windows handling module.           *
 ****************************************************************************)

IMPLEMENTATION MODULE DBProcChainWindows;

FROM SYSTEM IMPORT
  ADDRESS, BYTE,
  ADR;
FROM CPrintBuffer IMPORT
  sprintf;
FROM DBConfiguration IMPORT
  ConfigWindow;
FROM DBMenusKbd IMPORT
  DoGlobalMenuCommand, DoGlobalKbdCommand, LinkGlobalMenuStrip;
FROM DBMisc IMPORT
  AllocMemory, DeallocMemory;
FROM DBRequesters IMPORT
  RequestTextFgBgColors, RequestTextWidth;
FROM DBUserProgram IMPORT
  ProcChainNodePtr,
  GetProcChainLength, GetProcChainNode;
FROM DBSelection IMPORT
  SelectionData,
  ClearSelection, GetSelection, SetSelection, GetSelectionLineInfo,
  IsPosSelected, IfCurrentWindowClearSelection;
FROM DBStatusWindow IMPORT
  ShowErrMsgNoMemory, ShowErrMsgBadDest,
  ShowError;
FROM DBTextWindows IMPORT
  DefMinWindowWidth, DefMinWindowHeight, DefMaxWindowWidth, DefMaxWindowHeight,
  VTWindow, NewVTWindow, VTWindowNIL, VTWindowParms,
  OpenVTWindow, CloseVTWindow, InitParmsVTWindow, WriteBufferVTWindow,
  SetCursorPosVTWindow, GetUserDataVTWindow, WriteColorVTWindow, ClearVTWindow,
  RefreshVTWindow, GetDisplayRangeVTWindow, RefreshRangeVTWindow,
  ScrollVTWindow, GetInfoVTWindow, SetMenuStripVTWindow, ClearMenuStripVTWindow,
  GetDetailPenBlockPenVTWindow, SetDetailPenBlockPenVTWindow,
  SetDefaultColorVTWindow;
FROM DBUserProgram IMPORT
  IsProgramLoaded;
FROM DBWindows IMPORT
  WindowClass,
  GetVTWindowsMsgPort;
FROM FormatString IMPORT
  FormatArg;
FROM Intuition IMPORT
  Menu, MenuPtr;
FROM Memory IMPORT
  MemReqSet, MemClear;
FROM SimpleMenus IMPORT
  BeginMenuStrip, EndMenuStrip, FreeMenuStrip, AddMenu, AddMenuItem;

CONST
  (* Procedure Call Chain Window Title *)
  DefProcChainWindowTitle = "Procedure Call Chain";

  LocalMenus = 1; (* Number of window specific menus *)

  OutputBufSize = 1024D; (* Output buffer size in VTUpdateProc() *)

TYPE
  (* Procedure call chain window descriptor *)
  ProcChainWindowPtr = POINTER TO ProcChainWindow;
  ProcChainWindow = RECORD
                      pwNext          : ProcChainWindowPtr; (* next window *)
                      pwVTWindow      : VTWindow;      (* text window *)
                      pwVTWindowParms : VTWindowParms; (* window parms *)
                      pwDisplayOn     : BOOLEAN;       (* displaying data *)
                      pwMaxColumns    : CARDINAL;      (* # of columns *)
                    END;

VAR
  ProcChainWindowList : ProcChainWindowPtr; (* list of procedure call windows *)
  ProcChainMenuStrip  : MenuPtr;            (* proc call window menu strip *)

(* ===== PRIVATE ===== *)

(* AddWindowNode - create a window node and link into window list *)
PROCEDURE AddWindowNode(): ProcChainWindowPtr;
VAR
  pw: ProcChainWindowPtr;
BEGIN
  pw := AllocMemory(SIZE(ProcChainWindow), MemReqSet{MemClear});
  IF (pw # NIL) THEN
    IF (ProcChainWindowList # NIL) THEN
      pw^.pwNext := ProcChainWindowList;
    END;
    ProcChainWindowList := pw;
  END;
  RETURN (pw);
END AddWindowNode;

(* DeleteWindowNode - delete window node and unlink from window list *)
PROCEDURE DeleteWindowNode(pw: ProcChainWindowPtr);
VAR
  prev: ProcChainWindowPtr;
  cur: ProcChainWindowPtr;
BEGIN
  prev := NIL;
  cur  := ProcChainWindowList;
  WHILE (cur # pw) DO
    prev := cur;
    cur  := cur^.pwNext;
  END;

  IF (prev = NIL) THEN (* First in list *)
    ProcChainWindowList := pw^.pwNext;
  ELSE (* Not first in list *)
    prev^.pwNext := pw^.pwNext;
  END;

  DeallocMemory(pw);
END DeleteWindowNode;

(* GetWindowIndex - get position of window node in list *)
PROCEDURE GetWindowIndex(pw: ProcChainWindowPtr): CARDINAL;
VAR
  index: CARDINAL;
  p: ProcChainWindowPtr;
BEGIN
  index := 0;
  p     := ProcChainWindowList;
  WHILE (pw # p) DO
    INC(index);
    p := p^.pwNext;
  END;
  RETURN (index);
END GetWindowIndex;

(* CloseProcChainWindow - close procedure call chain window *)
PROCEDURE CloseProcChainWindow(VAR pw: ProcChainWindow);
BEGIN
  WITH pw DO
    IfCurrentWindowClearSelection(pwVTWindow);

    ClearMenuStripVTWindow(pwVTWindow);

    CloseVTWindow(pwVTWindow);
  END;
  DeleteWindowNode(ADR(pw));
END CloseProcChainWindow;

(* CloseProcChainWindowNoMemory - close window and show no memory err msg *)
PROCEDURE CloseProcChainWindowNoMemory(VAR pw: ProcChainWindow);
BEGIN
  CloseProcChainWindow(pw);
  ShowError(ShowErrMsgNoMemory);
END CloseProcChainWindowNoMemory;


(* SetNewTextWidth - change text width of window *)
PROCEDURE SetNewTextWidth(VAR pw: ProcChainWindow);
BEGIN
  WITH pw DO
    IF (pwDisplayOn) THEN
      IfCurrentWindowClearSelection(pwVTWindow);
      pwVTWindowParms.vtpMaxColumns := pwMaxColumns;
      IF (NOT InitParmsVTWindow(pwVTWindow, pwVTWindowParms)) THEN
        CloseProcChainWindowNoMemory(pw);
      END;
    END;
  END;
END SetNewTextWidth;

(* ClearProcChainWindow - disable display of data in window *)
PROCEDURE ClearProcChainWindow(VAR pw: ProcChainWindow);
BEGIN
  WITH pw DO
    IF (pwDisplayOn) THEN
      IfCurrentWindowClearSelection(pwVTWindow);
      pwDisplayOn := FALSE;
      WITH pwVTWindowParms DO
        vtpInitialLine := 0;
        vtpTotalLines  := 1;
        vtpMaxColumns  := 1;
      END;
      IF (NOT InitParmsVTWindow(pwVTWindow, pwVTWindowParms)) THEN
        CloseProcChainWindowNoMemory(pw);
      END;
    END;
  END;
END ClearProcChainWindow;

(* SetNewProcSelection - setup new user selection base on y position *)
PROCEDURE SetNewProcSelection(VAR pw: ProcChainWindow; y: CARDINAL);
VAR
  data: SelectionData;
BEGIN
  WITH data DO
    sdWindowClass := WCProcChain;
    sdVTWindow    := pw.pwVTWindow;
    sdLineNo      := y;
    sdStartPos    := 0;
    sdEndPos      := pw.pwVTWindowParms.vtpMaxColumns - 1;
    sdDefFgColor  := pw.pwVTWindowParms.vtpDefFgColor;
    sdDefBgColor  := pw.pwVTWindowParms.vtpDefBgColor;
    sdProcChainNo := y;
  END;
  SetSelection(data);
END SetNewProcSelection;

(* UpdateProcChainWindow - update constant of procedure call chain window *)
PROCEDURE UpdateProcChainWindow(VAR pw: ProcChainWindow);
VAR
  new: BOOLEAN;
  length: CARDINAL;
  first: CARDINAL;
  last: CARDINAL;
BEGIN
  IF (IsProgramLoaded()) THEN
    WITH pw DO
      IfCurrentWindowClearSelection(pwVTWindow);

      GetProcChainLength(new, length);
      IF (pwDisplayOn) AND (NOT new) THEN (* Old procedure chain *)
        GetDisplayRangeVTWindow(pwVTWindow, first, last);
        IF (first = 0) THEN
          RefreshRangeVTWindow(pwVTWindow, 0, 0);
        END;
      ELSE (* New procedure chain or new display *)
        IF (pwDisplayOn) AND (pwVTWindowParms.vtpTotalLines = length) THEN
          ScrollVTWindow(pwVTWindow, 0, 0);
          ClearVTWindow(pwVTWindow);
          RefreshVTWindow(pwVTWindow);
        ELSIF (length = 0) THEN
          ClearProcChainWindow(pw);
        ELSE
          pwVTWindowParms.vtpTotalLines := length;
          pwVTWindowParms.vtpMaxColumns := pwMaxColumns;
          pwDisplayOn := TRUE;
          IF (NOT InitParmsVTWindow(pwVTWindow, pwVTWindowParms)) THEN
            CloseProcChainWindowNoMemory(pw);
          END;
        END;
      END; (* IF *)
    END; (* WITH *)
  ELSE
    ClearProcChainWindow(pw);
  END;
END UpdateProcChainWindow;

(* VTUpdateProc - called to output new text to window *)
PROCEDURE VTUpdateProc(Window: VTWindow; OutputRow,FirstLine,LastLine: CARDINAL);
VAR
  pw: ProcChainWindowPtr;
  pcn: ProcChainNodePtr;
  line: CARDINAL;
  outBuf: ADDRESS;
  outLength: CARDINAL;
  startPos: CARDINAL;
  endPos: CARDINAL;
  argo: ARRAY [0..9] OF FormatArg;
BEGIN
  pw := GetUserDataVTWindow(Window);
  IF (pw^.pwDisplayOn) THEN
    outBuf := AllocMemory(OutputBufSize, MemReqSet{});
    IF (outBuf = NIL) THEN RETURN; END;
  
    FOR line := FirstLine TO LastLine DO
      SetCursorPosVTWindow(Window, 0, OutputRow);
      pcn := GetProcChainNode(line);
  
      argo[0].W := pw^.pwVTWindowParms.vtpTotalLines - line;
      argo[1].L := pcn^.pcnAdr;
      argo[2].L := pcn^.pcnModName;
      argo[3].L := pcn^.pcnProcName;
      outLength := sprintf(outBuf, ADR("%04u: $%08lx.%s.%s"), argo);
  
      WriteBufferVTWindow(Window, outBuf, outLength);
  
      IF (GetSelectionLineInfo(Window, line, startPos, endPos)) THEN
        WITH pw^.pwVTWindowParms DO
          WriteColorVTWindow(Window, OutputRow, startPos, endPos,
                             vtpDefBgColor, vtpDefFgColor);
          END;
      END;
  
      INC(OutputRow);
    END; (* FOR *)
    DeallocMemory(outBuf);
  END;
END VTUpdateProc;

(* VTSelectProc - called when left mouse button click in window *)
PROCEDURE VTSelectProc(Window: VTWindow; x, y: CARDINAL; pressed: BOOLEAN);
VAR
  pw: ProcChainWindowPtr;
BEGIN
  pw := GetUserDataVTWindow(Window);

  IF (pw^.pwDisplayOn) THEN
    IF (pressed) THEN
      IF (IsPosSelected(Window, x, y)) THEN
        ClearSelection();
      ELSE
        SetNewProcSelection(pw^, y);
      END;
    ELSE
      IF (NOT IsPosSelected(Window, x, y)) THEN
        SetNewProcSelection(pw^, y);
      END;
    END;
  END;
END VTSelectProc;

(* VTMenuProc - called when menu selection made *)
PROCEDURE VTMenuProc(Window: VTWindow; MenuNum, ItemNum, SubNum: CARDINAL);
VAR
  pw: ProcChainWindowPtr;
  fg: BYTE;
  bg: BYTE;
BEGIN
  pw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalMenuCommand(MenuNum, ItemNum, SubNum, LocalMenus,
                              WCProcChain, GetWindowIndex(pw),
                              pw^.pwVTWindow)) THEN
    WITH pw^ DO
      IfCurrentWindowClearSelection(pwVTWindow);

      CASE (ItemNum) OF
      | 0: (* Set Text Width *)
           IF (RequestTextWidth(pwMaxColumns)) THEN
             SetNewTextWidth(pw^);
           END;
      | 1: (* Set Window Color *)
           GetDetailPenBlockPenVTWindow(pwVTWindow, fg, bg);
           IF (RequestTextFgBgColors(fg, bg)) THEN
             SetDetailPenBlockPenVTWindow(pwVTWindow, fg, bg);
           END;
      | 2: (* Set Text Color *)
           WITH pwVTWindowParms DO
             IF (RequestTextFgBgColors(vtpDefFgColor, vtpDefBgColor)) THEN
               SetDefaultColorVTWindow(pwVTWindow, vtpDefFgColor,vtpDefBgColor);
             END;
           END;
      END;
    END;
  END;
END VTMenuProc;

(* VTDestProc - called when user clicks on destination-data gadget *)
PROCEDURE VTDestProc(Window: VTWindow);
BEGIN
  ShowError(ShowErrMsgBadDest);
END VTDestProc;

(* VTKeyProc - called when user types key into window *)
PROCEDURE VTKeyProc(Window: VTWindow; keyBuf: ADDRESS; keyLength: CARDINAL);
VAR
  pw: ProcChainWindowPtr;
BEGIN
  pw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalKbdCommand(keyBuf, keyLength, WCProcChain,
                             GetWindowIndex(pw))) THEN
  END;
END VTKeyProc;

(* VTCloseProc - called when close-box hit *)
PROCEDURE VTCloseProc(Window: VTWindow);
VAR
  pw: ProcChainWindowPtr;
BEGIN
  pw := GetUserDataVTWindow(Window);
  CloseProcChainWindow(pw^);
END VTCloseProc;

(* VTErrorProc - called when failed to alloc text info for resized window *)
PROCEDURE VTErrorProc(Window: VTWindow);
VAR
  pw: ProcChainWindowPtr;
BEGIN
  pw := GetUserDataVTWindow(Window);
  CloseProcChainWindowNoMemory(pw^);
END VTErrorProc;

(* ===== PUBLIC ===== *)

(* OpenProcChainWindow - open a new procedure call chain window *)
PROCEDURE OpenProcChainWindow(VAR config: ConfigWindow): BOOLEAN;
VAR
  pw: ProcChainWindowPtr;
  ok: BOOLEAN;
  nvt: NewVTWindow;
BEGIN
  ok := FALSE;
  pw := AddWindowNode();
  IF (pw # NIL) THEN
    WITH nvt DO
      WITH config DO
        nvtLeftEdge  := cwX;
        nvtTopEdge   := cwY;
        nvtWidth     := cwWidth;
        nvtHeight    := cwHeight;
        nvtDetailPen := cwDetailPen;
        nvtBlockPen  := cwBlockPen;
        nvtTitle     := ADR(DefProcChainWindowTitle);
        nvtMinWidth  := DefMinWindowWidth;
        nvtMinHeight := DefMinWindowHeight;
        nvtMaxWidth  := DefMaxWindowWidth;
        nvtMaxHeight := DefMaxWindowHeight;
        nvtMsgPort   := GetVTWindowsMsgPort();
        nvtUserData  := pw;
      END; (* WITH *)
    END; (* WITH *)
    pw^.pwVTWindow := OpenVTWindow(nvt);
    ok := pw^.pwVTWindow # VTWindowNIL;

    IF (ok) THEN
      WITH pw^.pwVTWindowParms DO
        WITH config DO
          vtpUpdateProc    := VTUpdateProc;
          vtpSelectProc    := VTSelectProc;
          vtpMenuProc      := VTMenuProc;
          vtpDestProc      := VTDestProc;
          vtpKeyProc       := VTKeyProc;
          vtpCloseProc     := VTCloseProc;
          vtpErrorProc     := VTErrorProc;
        (*vtpInitialLine   := 0;*)
        (*vtpTotalLines    := 1;*)
        (*vtpInitialColumn := 0;*)
        (*vtpMaxColumns    := 1;*)
          vtpDefFgColor    := cwFgColor;
          vtpDefBgColor    := cwBgColor;
        END;
      END;

      WITH pw^ DO
        pwDisplayOn  := TRUE;
        pwMaxColumns := config.cwMaxCharWidth;
      END;
      SetMenuStripVTWindow(pw^.pwVTWindow, ProcChainMenuStrip^);

      ClearProcChainWindow(pw^);
      UpdateProcChainWindow(pw^);
    ELSE (* NOT ok *)
      DeleteWindowNode(pw);
    END;
  END;
  RETURN (ok);
END OpenProcChainWindow;

(* UpdateProcChainWindows - update contents of all procedure call windows *)
PROCEDURE UpdateProcChainWindows();
VAR
  pw: ProcChainWindowPtr;
  p: ProcChainWindowPtr;
BEGIN
  pw := ProcChainWindowList;
  WHILE (pw # NIL) DO
    p  := pw;
    pw := pw^.pwNext;
    UpdateProcChainWindow(p^);
  END;
END UpdateProcChainWindows;

(* CloseProcChainWindows - close all procedure call chain windows *)
PROCEDURE CloseProcChainWindows();
BEGIN
  WHILE (ProcChainWindowList # NIL) DO
    CloseProcChainWindow(ProcChainWindowList^);
  END;  
END CloseProcChainWindows;

(* GetProcChainWindowInfo - get config parameters of a specified window *)
PROCEDURE GetProcChainWindowInfo(VAR index: CARDINAL;
                                 VAR config: ConfigWindow): BOOLEAN;
VAR
  pw: ProcChainWindowPtr;
  count: CARDINAL;
BEGIN
  count := index;
  pw    := ProcChainWindowList;
  WHILE (pw # NIL) DO
    IF (count = 0) THEN
      WITH config DO
        WITH pw^ DO
          GetInfoVTWindow(pwVTWindow, cwX, cwY, cwWidth, cwHeight,
                          cwDetailPen, cwBlockPen);
          cwMaxCharWidth := pwMaxColumns;
          cwFgColor      := pwVTWindowParms.vtpDefFgColor;
          cwBgColor      := pwVTWindowParms.vtpDefBgColor;
        END;
      END;
      INC(index);
      RETURN (TRUE); (* Window Found! *)
    END;
    DEC(count);
    pw := pw^.pwNext;
  END;
  RETURN (FALSE); (* End of list *)
END GetProcChainWindowInfo;

(* InitDBProcChainWindows - initialize module *)
PROCEDURE InitDBProcChainWindows(): BOOLEAN;
BEGIN
  BeginMenuStrip();
    AddMenu(                           ADR("Procedure-Call-Chain "));
      AddMenuItem(                       ADR(" Set Text Width"));
      AddMenuItem(                       ADR(" Set Window Color "));
      AddMenuItem(                       ADR(" Set Text Color"));
  ProcChainMenuStrip := EndMenuStrip();
  IF (ProcChainMenuStrip # NIL) THEN
    LinkGlobalMenuStrip(ProcChainMenuStrip^);
  END;
  RETURN (ProcChainMenuStrip # NIL);
END InitDBProcChainWindows;

(* CleanupDBProcChainWindows - cleanup module *)
PROCEDURE CleanupDBProcChainWindows();
BEGIN
  CloseProcChainWindows();

  IF (ProcChainMenuStrip # NIL) THEN
    FreeMenuStrip(ProcChainMenuStrip^);
  END;
END CleanupDBProcChainWindows;

END DBProcChainWindows.
