(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBRegistersWindows.MOD            Version: Amiga.01.10             *
 * Created: 02/08/89   Updated: 03/25/89   Author: Leon Frenkel             *
 * Description: Debugger CPU registers windows handling module.             *
 ****************************************************************************)

IMPLEMENTATION MODULE DBRegistersWindows;

FROM SYSTEM IMPORT
  ADDRESS, BYTE,
  ADR;
FROM CPrintBuffer IMPORT
  sprintf;
FROM DBConfiguration IMPORT
  ConfigWindow;
FROM DBFiles IMPORT
  InvalidAbsAdr;
FROM DBMenusKbd IMPORT
  DoGlobalMenuCommand, DoGlobalKbdCommand, LinkGlobalMenuStrip;
FROM DBMisc IMPORT
  AllocMemory, DeallocMemory;
FROM DBRequesters IMPORT
  RequestTextFgBgColors;
FROM DBSelection IMPORT
  SelectionData,
  ClearSelection, GetSelection, SetSelection, GetSelectionLineInfo,
  IsPosSelected, IfCurrentWindowClearSelection;
FROM DBStatusWindow IMPORT
  ShowErrMsgNoMemory, ShowErrMsgBadDest,
  ShowError;
FROM DBTextWindows IMPORT
  DefMinWindowWidth, DefMinWindowHeight, DefMaxWindowWidth, DefMaxWindowHeight,
  VTWindow, NewVTWindow, VTWindowNIL, VTWindowParms,
  OpenVTWindow, CloseVTWindow, InitParmsVTWindow, WriteBufferVTWindow,
  SetCursorPosVTWindow, GetUserDataVTWindow, WriteColorVTWindow, ClearVTWindow,
  RefreshVTWindow, WriteStringVTWindow, GetInfoVTWindow, SetMenuStripVTWindow,
  ClearMenuStripVTWindow, GetDetailPenBlockPenVTWindow, WriteCharVTWindow,
  SetDetailPenBlockPenVTWindow, SetDefaultColorVTWindow;
FROM DBUserProgram IMPORT
  IsProgramLoaded, GetRegisterValue, GetRegisterPCValue, GetRegisterSRValue;
FROM DBWindows IMPORT
  WindowClass,
  GetVTWindowsMsgPort;
FROM FormatString IMPORT
  FormatArg;
FROM Intuition IMPORT
  Menu, MenuPtr;
FROM Memory IMPORT
  MemReqSet, MemClear;
FROM SimpleMenus IMPORT
  BeginMenuStrip, EndMenuStrip, FreeMenuStrip, AddMenu, AddMenuItem;

CONST
  (* Registers Window Title *)
  DefRegistersWindowTitle = "Registers";

  LocalMenus = 1; (* Number of window specific menus *)

  OutputBufSize = 1024D; (* Output buffer size in VTUpdateProc() *)

  RegistersWindowColumns = 47; (* register window char columns *)
  RegistersWindowLines   = 5;  (* register window char lines *)

  (* Data and address register numbers *)
  D0 = 0; D1 = 1; D2 = 2; D3 = 3; D4 = 4; D5 = 5; D6 = 6; D7 = 7;
  A0 = 8; A1 = 9; A2 =10; A3 =11; A4 =12; A5 =13; A6 =14; A7 =15; 

  (* Status register flags *)
  SRFlagX = 4;
  SRFlagN = 3;
  SRFlagZ = 2;
  SRFlagV = 1;
  SRFlagC = 0;

  (* Register window text *)
  Line0Text = "D0=******** D1=******** D2=******** D3=********";
  Line1Text = "D4=******** D5=******** D6=******** D7=********";
  Line2Text = "A0=******** A1=******** A2=******** A3=********";
  Line3Text = "A4=******** A5=******** A6=******** A7=********";
  Line4Text = "PC=******** SR=****     X=*  N=*  Z=*  V=*  C=*";

  DispRegBase  = 3;  (* Data and address register base x pos *)
  DispRegWidth = 8;  (* Data and address register width *)
  DispRegInc   = 12; (* Data and address register x increment *)
  DispRegPCPos = 3;  (* Program Counter register x pos *)
  DispRegSRPos = 15; (* Status Register x pos *)
  DispFlagXPos = 26; (* Status FlagX x pos *)
  DispFlagNPos = 31; (* Status FlagN x pos *)
  DispFlagZPos = 36; (* Status FlagZ x pos *)
  DispFlagVPos = 41; (* Status FlagV x pos *)
  DispFlagCPos = 46; (* Status FlagC x pos *)

TYPE
  (* Registers window descriptor *)
  RegistersWindowPtr = POINTER TO RegistersWindow;
  RegistersWindow = RECORD
                      rwNext          : RegistersWindowPtr; (* next window *)
                      rwVTWindow      : VTWindow;      (* text window *)
                      rwVTWindowParms : VTWindowParms; (* window parms *)
                      rwDisplayOn     : BOOLEAN;       (* displaying data *)
                      rwRedrawValues  : BOOLEAN;       (* redraw values only*)
                      rwRegList       : ARRAY [D0..A7] OF LONGCARD; (*last regs*)
                      rwRegPC         : LONGCARD;       (* last PC reg *)
                      rwRegSR         : BITSET;         (* last SR reg *)
                    END;

VAR
  RegistersWindowList : RegistersWindowPtr; (* list of registers windows *)
  RegistersMenuStrip  : MenuPtr;            (* registers window menu strip *)

(* ===== PRIVATE ===== *)

(* AddWindowNode - create a window node and link into window list *)
PROCEDURE AddWindowNode(): RegistersWindowPtr;
VAR
  rw: RegistersWindowPtr;
BEGIN
  rw := AllocMemory(SIZE(RegistersWindow), MemReqSet{MemClear});
  IF (rw # NIL) THEN
    IF (RegistersWindowList # NIL) THEN
      rw^.rwNext := RegistersWindowList;
    END;
    RegistersWindowList := rw;
  END;
  RETURN (rw);
END AddWindowNode;

(* DeleteWindowNode - delete window node and unlink from window list *)
PROCEDURE DeleteWindowNode(rw: RegistersWindowPtr);
VAR
  prev: RegistersWindowPtr;
  cur: RegistersWindowPtr;
BEGIN
  prev := NIL;
  cur  := RegistersWindowList;
  WHILE (cur # rw) DO
    prev := cur;
    cur  := cur^.rwNext;
  END;

  IF (prev = NIL) THEN (* First in list *)
    RegistersWindowList := rw^.rwNext;
  ELSE (* Not first in list *)
    prev^.rwNext := rw^.rwNext;
  END;

  DeallocMemory(rw);
END DeleteWindowNode;

(* GetWindowIndex - get position of window node in list *)
PROCEDURE GetWindowIndex(rw: RegistersWindowPtr): CARDINAL;
VAR
  index: CARDINAL;
  p: RegistersWindowPtr;
BEGIN
  index := 0;
  p     := RegistersWindowList;
  WHILE (rw # p) DO
    INC(index);
    p := p^.rwNext;
  END;
  RETURN (index);
END GetWindowIndex;

(* CloseRegistersWindow - close registers window *)
PROCEDURE CloseRegistersWindow(VAR rw: RegistersWindow);
BEGIN
  WITH rw DO
    IfCurrentWindowClearSelection(rwVTWindow);

    ClearMenuStripVTWindow(rwVTWindow);

    CloseVTWindow(rwVTWindow);
  END;
  DeleteWindowNode(ADR(rw));
END CloseRegistersWindow;

(* CloseRegistersWindowNoMemory - close window and show no memory err msg *)
PROCEDURE CloseRegistersWindowNoMemory(VAR rw: RegistersWindow);
BEGIN
  CloseRegistersWindow(rw);
  ShowError(ShowErrMsgNoMemory);
END CloseRegistersWindowNoMemory;

(* ClearRegistersWindow - disable display of data in window *)
PROCEDURE ClearRegistersWindow(VAR rw: RegistersWindow);
BEGIN
  WITH rw DO
    IF (rwDisplayOn) THEN
      IfCurrentWindowClearSelection(rwVTWindow);
      rwDisplayOn := FALSE;
      WITH rwVTWindowParms DO
        vtpInitialLine := 0;
        vtpTotalLines  := 1;
        vtpMaxColumns  := 1;
      END;
      IF (NOT InitParmsVTWindow(rwVTWindow, rwVTWindowParms)) THEN
        CloseRegistersWindowNoMemory(rw);
      END;
    END;
  END;
END ClearRegistersWindow;

(* SetNewRegistersSelection - setup new user selection based on x, y position *)
PROCEDURE SetNewRegistersSelection(VAR rw: RegistersWindow; x, y: CARDINAL);
VAR
  data: SelectionData;
  ok: BOOLEAN;
  ofs: CARDINAL;
  col: CARDINAL;
  startX: CARDINAL;
  stopX: CARDINAL;
  regNo: CARDINAL;
  bitNo: CARDINAL;
  absAdr: ADDRESS;
BEGIN
  ok := FALSE;
  IF (y >= 0) AND (y <= 3) THEN (* Lines containg D0-D7 and A0-A7 *)
    IF (x >= DispRegBase) THEN
      DEC(x, DispRegBase);
      ofs := x MOD DispRegInc;
      IF (ofs < DispRegWidth) THEN
        ok     := TRUE;
        col    := x DIV DispRegInc;
        regNo  := (y * 4) + col;
        absAdr := GetRegisterValue(regNo);
        startX := DispRegBase + (col * DispRegInc);
        stopX  := startX + (DispRegWidth - 1);
      END;
    END;
  ELSIF (y = 4) THEN
    (* Setup for a flag selection *)
    ok     := TRUE;
    regNo  := SRRegNo;
    absAdr := InvalidAbsAdr;
    startX := x;
    stopX  := x;

    IF (x >= DispRegPCPos) AND (x < DispRegPCPos + DispRegWidth) THEN
      regNo  := PCRegNo;
      absAdr := GetRegisterPCValue();
      startX := DispRegPCPos;
      stopX  := startX + (DispRegWidth - 1);
    ELSIF (x = DispFlagXPos) THEN
      bitNo := SRFlagX;
    ELSIF (x = DispFlagNPos) THEN
      bitNo := SRFlagN;
    ELSIF (x = DispFlagZPos) THEN
      bitNo := SRFlagZ;
    ELSIF (x = DispFlagVPos) THEN
      bitNo := SRFlagV;
    ELSIF (x = DispFlagCPos) THEN
      bitNo := SRFlagC;
    ELSE
      ok := FALSE;
    END;
  END;

  IF (ok) THEN
    WITH data DO
      sdWindowClass   := WCRegisters;
      sdVTWindow      := rw.rwVTWindow;
      sdLineNo        := y;
      sdStartPos      := startX;
      sdEndPos        := stopX;
      sdDefFgColor    := rw.rwVTWindowParms.vtpDefFgColor;
      sdDefBgColor    := rw.rwVTWindowParms.vtpDefBgColor;
      sdAdr           := absAdr;
      sdStorageRegNo  := regNo;
      sdStorageBitNo  := bitNo;
    END;
    SetSelection(data);
  ELSE
    ClearSelection();
  END;
END SetNewRegistersSelection;

(* UpdateRegistersWindow - update contents of registers window *)
PROCEDURE UpdateRegistersWindow(VAR rw: RegistersWindow);
BEGIN
  IF (IsProgramLoaded()) THEN
    WITH rw DO
      IfCurrentWindowClearSelection(rwVTWindow);

      IF (rwDisplayOn) THEN
        rwRedrawValues := TRUE;
        RefreshVTWindow(rwVTWindow);
      ELSE
        rwVTWindowParms.vtpTotalLines := RegistersWindowLines;
        rwVTWindowParms.vtpMaxColumns := RegistersWindowColumns;
        rwDisplayOn    := TRUE;
        rwRedrawValues := FALSE;
        IF (NOT InitParmsVTWindow(rwVTWindow, rwVTWindowParms)) THEN
          CloseRegistersWindowNoMemory(rw);
        END;
      END;
    END;
  ELSE
    ClearRegistersWindow(rw);
  END;
END UpdateRegistersWindow;

(* CompareOrStoreValue - compare two values for equality *)
PROCEDURE CompareOrStoreValue(VAR oldValue: LONGCARD; newValue: LONGCARD;
                              dontCmp: BOOLEAN): BOOLEAN;
VAR
  equal: BOOLEAN;
BEGIN
  equal := (NOT dontCmp) AND (oldValue = newValue);
  IF (NOT equal) THEN
    oldValue := newValue;
  END;
  RETURN (equal);
END CompareOrStoreValue;

(* VTUpdateProc - called to output new text to window *)
PROCEDURE VTUpdateProc(Window: VTWindow; OutputRow,FirstLine,LastLine: CARDINAL);
VAR
  rw: RegistersWindowPtr;
  line: CARDINAL;
  outBuf: ADDRESS;
  outLength: CARDINAL;
  redrawAll: BOOLEAN;
  ch: CHAR;
  startPos: CARDINAL;
  endPos: CARDINAL;
  lineText: ADDRESS;
  argo: ARRAY [0..9] OF FormatArg;
  col: CARDINAL;
  value: LONGCARD;
  valueSR: BITSET;
  regNo: CARDINAL;
BEGIN
  rw := GetUserDataVTWindow(Window);
  WITH rw^ DO
    IF (rwDisplayOn) THEN
      outBuf := AllocMemory(OutputBufSize, MemReqSet{});
      IF (outBuf = NIL) THEN RETURN; END;

      redrawAll := NOT rwRedrawValues;

      FOR line := FirstLine TO LastLine DO
        IF (redrawAll) THEN (* Redraw All! *)
          CASE (line) OF
          | 0: lineText := ADR(Line0Text);
          | 1: lineText := ADR(Line1Text);
          | 2: lineText := ADR(Line2Text);
          | 3: lineText := ADR(Line3Text);
          | 4: lineText := ADR(Line4Text);
          END;
          SetCursorPosVTWindow(Window, 0, OutputRow);
          WriteStringVTWindow(Window, lineText)
        END;

        IF (line = 4) THEN
          value := GetRegisterPCValue();
          IF (NOT CompareOrStoreValue(rwRegPC, value, redrawAll)) THEN
            argo[0].L := value;
            outLength := sprintf(outBuf, ADR("%08lx"), argo);
            SetCursorPosVTWindow(Window, DispRegPCPos, OutputRow);
            WriteBufferVTWindow(Window, outBuf, outLength);
          END;
          valueSR := GetRegisterSRValue();
          IF (redrawAll) OR (valueSR # rwRegSR) THEN
            rwRegSR := valueSR;
            argo[0].W := valueSR;
            outLength := sprintf(outBuf, ADR("%04x"), argo);
            SetCursorPosVTWindow(Window, DispRegSRPos, OutputRow);
            WriteBufferVTWindow(Window, outBuf, outLength);

            SetCursorPosVTWindow(Window, DispFlagXPos, OutputRow);
            ch := CHAR(CARDINAL("0") + CARDINAL(SRFlagX IN valueSR));
            WriteCharVTWindow(Window, ch);

            SetCursorPosVTWindow(Window, DispFlagNPos, OutputRow);
            ch := CHAR(CARDINAL("0") + CARDINAL(SRFlagN IN valueSR));
            WriteCharVTWindow(Window, ch);

            SetCursorPosVTWindow(Window, DispFlagZPos, OutputRow);
            ch := CHAR(CARDINAL("0") + CARDINAL(SRFlagZ IN valueSR));
            WriteCharVTWindow(Window, ch);

            SetCursorPosVTWindow(Window, DispFlagVPos, OutputRow);
            ch := CHAR(CARDINAL("0") + CARDINAL(SRFlagV IN valueSR));
            WriteCharVTWindow(Window, ch);

            SetCursorPosVTWindow(Window, DispFlagCPos, OutputRow);
            ch := CHAR(CARDINAL("0") + CARDINAL(SRFlagC IN valueSR));
            WriteCharVTWindow(Window, ch);
          END;
        ELSE (* Display data and address registers *)
          regNo := line * 4;
          FOR col := 0 TO 3 DO
            value := GetRegisterValue(regNo);
            IF (NOT CompareOrStoreValue(rwRegList[regNo], value, redrawAll)) THEN
              argo[0].L := value;
              outLength := sprintf(outBuf, ADR("%08lx"), argo);
              SetCursorPosVTWindow(Window, DispRegBase+(col * DispRegInc), OutputRow);
              WriteBufferVTWindow(Window, outBuf, outLength);
            END;
            INC(regNo);
          END;
        END;

        IF (GetSelectionLineInfo(Window, line, startPos, endPos)) THEN
          WriteColorVTWindow(Window, OutputRow, startPos, endPos,
                             rwVTWindowParms.vtpDefBgColor,
                             rwVTWindowParms.vtpDefFgColor);
        END;

        INC(OutputRow);
      END; (* FOR *)

      rwRedrawValues := FALSE;

      DeallocMemory(outBuf);
    END;
  END;
END VTUpdateProc;

(* VTSelectProc - called when left mouse button click in window *)
PROCEDURE VTSelectProc(Window: VTWindow; x, y: CARDINAL; pressed: BOOLEAN);
VAR
  rw: RegistersWindowPtr;
BEGIN
  rw := GetUserDataVTWindow(Window);

  IF (rw^.rwDisplayOn) THEN
    IF (pressed) THEN
      IF (IsPosSelected(Window, x, y)) THEN
        ClearSelection();
      ELSE
        SetNewRegistersSelection(rw^, x, y);
      END;
    ELSE
      IF (NOT IsPosSelected(Window, x, y)) THEN
        SetNewRegistersSelection(rw^, x, y);
      END;
    END;
  END;
END VTSelectProc;

(* VTMenuProc - called when menu selection made *)
PROCEDURE VTMenuProc(Window: VTWindow; MenuNum, ItemNum, SubNum: CARDINAL);
VAR
  rw: RegistersWindowPtr;
  fg: BYTE;
  bg: BYTE;
BEGIN
  rw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalMenuCommand(MenuNum, ItemNum, SubNum, LocalMenus,
                              WCRegisters, GetWindowIndex(rw),
                              rw^.rwVTWindow)) THEN
    WITH rw^ DO
      IfCurrentWindowClearSelection(rwVTWindow);

      CASE (ItemNum) OF
      | 0: (* Set Window Color *)
           GetDetailPenBlockPenVTWindow(rwVTWindow, fg, bg);
           IF (RequestTextFgBgColors(fg, bg)) THEN
             SetDetailPenBlockPenVTWindow(rwVTWindow, fg, bg);
           END;
      | 1: (* Set Text Color *)
           WITH rwVTWindowParms DO
             IF (RequestTextFgBgColors(vtpDefFgColor, vtpDefBgColor)) THEN
               SetDefaultColorVTWindow(rwVTWindow, vtpDefFgColor,vtpDefBgColor);
             END;
           END;
      END;
    END;
  END;
END VTMenuProc;

(* VTDestProc - called when user clicks on destination-data gadget *)
PROCEDURE VTDestProc(Window: VTWindow);
BEGIN
  ShowError(ShowErrMsgBadDest);
END VTDestProc;

(* VTKeyProc - called when user types key into window *)
PROCEDURE VTKeyProc(Window: VTWindow; keyBuf: ADDRESS; keyLength: CARDINAL);
VAR
  rw: RegistersWindowPtr;
BEGIN
  rw := GetUserDataVTWindow(Window);
  IF (NOT DoGlobalKbdCommand(keyBuf, keyLength, WCRegisters,
                             GetWindowIndex(rw))) THEN
  END;
END VTKeyProc;

(* VTCloseProc - called when close-box hit *)
PROCEDURE VTCloseProc(Window: VTWindow);
VAR
  rw: RegistersWindowPtr;
BEGIN
  rw := GetUserDataVTWindow(Window);
  CloseRegistersWindow(rw^);
END VTCloseProc;

(* VTErrorProc - called when failed to alloc text info for resized window *)
PROCEDURE VTErrorProc(Window: VTWindow);
VAR
  rw: RegistersWindowPtr;
BEGIN
  rw := GetUserDataVTWindow(Window);
  CloseRegistersWindowNoMemory(rw^);
END VTErrorProc;

(* ===== PUBLIC ===== *)

(* OpenRegistersWindow - open a new registers window *)
PROCEDURE OpenRegistersWindow(VAR config: ConfigWindow): BOOLEAN;
VAR
  rw: RegistersWindowPtr;
  ok: BOOLEAN;
  nvt: NewVTWindow;
BEGIN
  ok := FALSE;
  rw := AddWindowNode();
  IF (rw # NIL) THEN
    WITH nvt DO
      WITH config DO
        nvtLeftEdge  := cwX;
        nvtTopEdge   := cwY;
        nvtWidth     := cwWidth;
        nvtHeight    := cwHeight;
        nvtDetailPen := cwDetailPen;
        nvtBlockPen  := cwBlockPen;
        nvtTitle     := ADR(DefRegistersWindowTitle);
        nvtMinWidth  := DefMinWindowWidth;
        nvtMinHeight := DefMinWindowHeight;
        nvtMaxWidth  := DefMaxWindowWidth;
        nvtMaxHeight := DefMaxWindowHeight;
        nvtMsgPort   := GetVTWindowsMsgPort();
        nvtUserData  := rw;
      END; (* WITH *)
    END; (* WITH *)
    rw^.rwVTWindow := OpenVTWindow(nvt);
    ok := rw^.rwVTWindow # VTWindowNIL;

    IF (ok) THEN
      WITH rw^.rwVTWindowParms DO
        WITH config DO
          vtpUpdateProc    := VTUpdateProc;
          vtpSelectProc    := VTSelectProc;
          vtpMenuProc      := VTMenuProc;
          vtpDestProc      := VTDestProc;
          vtpKeyProc       := VTKeyProc;
          vtpCloseProc     := VTCloseProc;
          vtpErrorProc     := VTErrorProc;
        (*vtpInitialLine   := 0;*)
        (*vtpTotalLines    := 1;*)
        (*vtpInitialColumn := 0;*)
        (*vtpMaxColumns    := 1;*)
          vtpDefFgColor    := cwFgColor;
          vtpDefBgColor    := cwBgColor;
        END;
      END;

      WITH rw^ DO
        rwDisplayOn := TRUE;
      END;
      SetMenuStripVTWindow(rw^.rwVTWindow, RegistersMenuStrip^);

      ClearRegistersWindow(rw^);
      UpdateRegistersWindow(rw^);
    ELSE (* NOT ok *)
      DeleteWindowNode(rw);
    END;
  END;
  RETURN (ok);
END OpenRegistersWindow;

(* UpdateRegistersWindows - update contents of all registers windows *)
PROCEDURE UpdateRegistersWindows();
VAR
  rw: RegistersWindowPtr;
  p: RegistersWindowPtr;
BEGIN
  rw := RegistersWindowList;
  WHILE (rw # NIL) DO
    p  := rw;
    rw := rw^.rwNext;
    UpdateRegistersWindow(p^);
  END;
END UpdateRegistersWindows;

(* CloseRegistersWindows - close all registers windows *)
PROCEDURE CloseRegistersWindows();
BEGIN
  WHILE (RegistersWindowList # NIL) DO
    CloseRegistersWindow(RegistersWindowList^);
  END;
END CloseRegistersWindows;

(* GetRegistersWindowInfo - get config parameters of a specified window *)
PROCEDURE GetRegistersWindowInfo(VAR index: CARDINAL;
                                 VAR config: ConfigWindow): BOOLEAN;
VAR
  rw: RegistersWindowPtr;
  count: CARDINAL;
BEGIN
  count := index;
  rw    := RegistersWindowList;
  WHILE (rw # NIL) DO
    IF (count = 0) THEN
      WITH config DO
        WITH rw^ DO
          GetInfoVTWindow(rwVTWindow, cwX, cwY, cwWidth, cwHeight,
                          cwDetailPen, cwBlockPen);
          cwMaxCharWidth := 0;
          cwFgColor      := rwVTWindowParms.vtpDefFgColor;
          cwBgColor      := rwVTWindowParms.vtpDefBgColor;
        END;
      END;
      INC(index);
      RETURN (TRUE); (* Window Found! *)
    END;
    DEC(count);
    rw := rw^.rwNext;
  END;
  RETURN (FALSE); (* End of list *)
END GetRegistersWindowInfo;

(* InitDBRegistersWindows - initialize module *)
PROCEDURE InitDBRegistersWindows(): BOOLEAN;
BEGIN
  BeginMenuStrip();
    AddMenu(                           ADR("Registers "));
      AddMenuItem(                       ADR(" Set Window Color "));
      AddMenuItem(                       ADR(" Set Text Color"));
  RegistersMenuStrip := EndMenuStrip();
  IF (RegistersMenuStrip # NIL) THEN
    LinkGlobalMenuStrip(RegistersMenuStrip^);
  END;
  RETURN (RegistersMenuStrip # NIL);
END InitDBRegistersWindows;

(* CleanupDBRegistersWindows - cleanup module *)
PROCEDURE CleanupDBRegistersWindows();
BEGIN
  CloseRegistersWindows();

  IF (RegistersMenuStrip # NIL) THEN
    FreeMenuStrip(RegistersMenuStrip^);
  END;
END CleanupDBRegistersWindows;

END DBRegistersWindows.
