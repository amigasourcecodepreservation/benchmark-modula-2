
(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBDisassembler.MOD                Version: Amiga.01.10             *
 * Created: 02/09/89   Updated: 03/21/94   Author: Leon Frenkel             *
 * Description: Debugger disassembler module.                               *
 * WARNING: The current implementation will improperly disasm a few         *
 *          incorrect instructions.  This could be eliminated by            *
 *          ungrouping a few of the instructions into seperate strings.     *
 ****************************************************************************)

IMPLEMENTATION MODULE DBDisassembler;

(***************************************************************************
 * Changes for new release 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.2 -----
 * 
 * Support for 68020+ Code
 * 
 *    1. Importing WhichCPU and CPUType from DBEnvironmentInquiry.def.
 * 
 *    2. RTD instruction: I() call for "RTDz" in empty spot after RTE
 *       only if WhichCPU > M68000.
 * 
 *    3. Scale Index Field: OutEA() gets scl value from inx word. Writes
 *       out "*2", "*4", or "*8" if scl > 0. Let it do so for any CPU.
 *       NOTE: 68000 encountering a non-zero scl will silently treat it
 *       as if 0.
 * 
 *    4. 32 bit MUL/DIV:
 * 
 *       a. Define '{' to mean a MUL32 type operand.
 *          Define '}' to mean a DIV32 type operand.
 *          Define '+' to mean, before the size indicator, one of the
 *          MUL32/DIV32 instructions.
 * 
 *       b. In OutOpCode(), add a "w1" local var, for the second word
 *          of MUL32/DIV32 instructions.
 * 
 *       c. In OutOpCode(), just after the identifier read, and before
 *          checking for '.', add
 *              i) assignment of w to w1
 *             ii) check for a '+', if so,
 *                   GetWord(w1);
 *                   Write out either 'S' or 'U' for signed or unsigned, as
 *                   appropriate.
 *                   check for it being a DIV instru (in w).
 *                   if so, Write out an 'L' (to get DIVSL or DIVUL) if
 *                   appropriate.
 *            iii) Change 2nd call to OutOperand to use w1 instead of w.
 * 
 *       d. In OutOperand(), add an "r12" local var. Assign it from bits 12-16
 *          at the top.
 * 
 *       e. In OutOperand(), add case for '{'. This prints "Rx-Ry" if bit 11 is
 *          1, and just "Rx" if 0.
 * 
 *       f. In OutOperand(), add case for '}'. This prints "Rx:Ry" if bit 11 is
 *          1 or Rx # r12, and just "Rx" otherwise.
 * 
 *       g. In InitDBAssembler() add 
 *          I(0FF80H, 04C00H, {0,2..11},    64, 2, ADR('MUL+.Ld{ DIV+.Ld} '));
 * 
 *       h. In InitDBAssembler(), change the line for 16 bit MUL/DIV to include
 *          a ".W" at the end, since now these need specification.
 * 
 *       i. Change CONST MaxOpcodes to 56.
 * 
 *       j. Added locar var "extraChars", in OutOpcode(), since spacing code
 *          assumed that the number of chars in the instru mnemonic is exactly
 *          equal to the number of chars before the first operand in the
 *          "opcodes[]" array. That is no longer true for DIVSL/DIVUL.
 * 
 *    5. CMP2/CHK2 Instructions:
 * 
 *       a. Define '&' to mean, before the size indicator, one of CMP2/CHK2
 *          instructions.
 *          Define '|' to mean a 2nd instru work bit 12 register operand.
 * 
 *       b. In OutOpCode, ELSIF from the '+' test, if ch is '&', then write
 *          either "HK2" or "MP2" to complete the CHK2/CMP2 instructions.
 * 
 *       c. In OutOperand(), for case '|'
 *          OutEA(DDIR + r12, undf);   (* writes out A reg as well *)
 * 
 *       d. In InitDBAssembler() add 
 *          I(0F9C0H, 000C0H, {0,5..8,10,11}, 512, 4, ADR('C&.Bd|   C&.Wd|   C&.L|             '); 
 * 
 *       f. Change CONST MaxOpcodes to 57.
 * 
 *       g. "extraChars" is 2 for these.
 * 
 *    6. EXTB.L Instruction
 * 
 *       a. In InitDBAssembler() add 
 *          I(0FEF8H, 049C0H, NONE,          1, 1, ADR('EXTB.Lk  '));
 * 
 *       b. Change CONST MaxOpcodes to 58.
 * 
 ***************************************************************************
*)

FROM SYSTEM IMPORT
  ADDRESS, WORD,
  ADR, LONG;
FROM CPrintBuffer IMPORT
  sprintf;
FROM FormatString IMPORT
  FormatArg;
FROM DBEnvironmentInquiry IMPORT
  CPUType, WhichCPU;

CONST
  (* Mneumonic used for invalid instructions *)
  DefInvalidInstText = "????";

  (* Total number of opcode slots in static opcode table array *)
  MaxOpcodes = 58;

  (* MC68000 effective address modes *)
  DDIR = 00B; (* Dn       *)
  ADIR = 10B; (* An       *)
  AIDR = 20B; (* (An)     *)
  AINC = 30B; (* (An)+    *)
  ADEC = 40B; (* -(An)    *)
  AOFF = 50B; (* d(An)    *)
  AIDX = 60B; (* d(An,Xi) *)
  XXXW = 70B; (* Abs.W    *)
  XXXL = 71B; (* Abs.L    *)
  POFF = 72B; (* d(PC)    *)
  PIDX = 73B; (* d(PC,Xi) *)
  IMME = 74B; (* Imm      *)

  (* Size of operation *)
  byte = 0;
  word = 1;
  long = 2;
  undf = 3;

TYPE
  (* MC68000 Instruction OpCode Record *)
  OpCode = RECORD
             mask : BITSET;   (* Mask to be applied to OpCode *)
             code : BITSET;   (* Match code for OpCode after masking *)
             vea  : BITSET;   (* Mask of valid EA modes for the OpCode *)
             d    : CARDINAL; (* DIV value for shifting bits of OpCode *)
             m    : CARDINAL; (* MOD value for masking bits of OpCode *)
             off  : ADDRESS;  (* Instruction string *)
           END;

VAR
  maxOpcd : CARDINAL;                          (* OpCode init index *)
  opcodes : ARRAY [0..MaxOpcodes-1] OF OpCode; (* OpCode table *)
  size    : ARRAY [byte..long] OF CHAR;        (* Size Value 'B','W','L'*)

  CodeAdr      : POINTER TO CARDINAL; (* pointer to next word of data *)
  CodeAdrBase2 : ADDRESS;             (* pointer to 2nd word of data  *)

  InstWordCount : CARDINAL; (* Word count for inst being decoded *)
  OutputBuffer  : ADDRESS;  (* Output text buffer ptr *)
  OutputBase    : ADDRESS;  (* Output text buffer base *)

(* ===== PRIVATE ===== *)

(* GetWord - get the next word from memory *)
PROCEDURE GetWord(VAR w: WORD);
BEGIN
  w := CodeAdr^;
  INC(ADDRESS(CodeAdr), 2);
  INC(InstWordCount);
END GetWord;

(* Write - output one character *)
PROCEDURE Write(c: CHAR);
BEGIN
  OutputBuffer^ := c;
  INC(OutputBuffer);
END Write;

(* ResetBuffer - reset to beginning of operand for instruction *)
PROCEDURE ResetBuffer();
BEGIN
  CodeAdr       := CodeAdrBase2;
  OutputBuffer  := OutputBase;
  InstWordCount := 1;
END ResetBuffer;

(* WriteString - output string to buffer *)
(*$D- disable dyn value parms *)
PROCEDURE WriteString(s: ARRAY OF CHAR);
VAR
  i: CARDINAL;
BEGIN
  i := 0;
  WHILE s[i] # 0C DO
    Write(s[i]);
    INC(i);
  END;
END WriteString;
(*$D+ enable dyn value parms *)

(* WriteRegNo - output number from 0..7 *)
PROCEDURE WriteRegNo(n: CARDINAL);
BEGIN
  Write(CHAR(CARDINAL("0") + (n MOD 8)));
END WriteRegNo;

(* WriteDecU8 - write decimal unsigned 8-bit value *)
PROCEDURE WriteDecU8(x: CARDINAL);
VAR
  r: CARDINAL;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..15] OF CHAR;
BEGIN
  argo[0].W := (x MOD 100H);
  r := sprintf(ADR(str[0]), ADR("%u"), argo);
  WriteString(str);
END WriteDecU8;

(* WriteHexS8 - write hex signed 8-bit value *)
PROCEDURE WriteHexS8(i: INTEGER);
VAR
  r: CARDINAL;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..15] OF CHAR;
BEGIN
  IF (i < 0) THEN
    argo[0].W := (CARDINAL(-i) MOD 100H);
    r := sprintf(ADR(str[0]), ADR("$-%02x"), argo);
  ELSE
    argo[0].W := (CARDINAL(i) MOD 100H);
    r := sprintf(ADR(str[0]), ADR("$%02x"), argo);
  END;
  WriteString(str);
END WriteHexS8;

(* WriteHexU8 - write hex unsigned 8-bit value *)
PROCEDURE WriteHexU8(x: CARDINAL);
VAR
  r: CARDINAL;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..15] OF CHAR;
BEGIN
  argo[0].W := (x MOD 100H);
  r := sprintf(ADR(str[0]), ADR("$%02x"), argo);
  WriteString(str);
END WriteHexU8;

(* WriteHexS16 - write hex signed 16-bit value *)
PROCEDURE WriteHexS16(i: INTEGER);
VAR
  r: CARDINAL;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..15] OF CHAR;
BEGIN
  IF (i < 0) THEN
    IF (i = (-32767-1)) THEN
      str := "$-8000";
    ELSE
      argo[0].W := -i;
      r := sprintf(ADR(str[0]), ADR("$-%04x"), argo);
    END;
  ELSE
    argo[0].W := i;
    r := sprintf(ADR(str[0]), ADR("$%04x"), argo);
  END;
  WriteString(str);
END WriteHexS16;

(* WriteHexU16 - write hex unsigned 16-bit value *)
PROCEDURE WriteHexU16(x: CARDINAL);
VAR
  r: CARDINAL;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..15] OF CHAR;
BEGIN
  argo[0].W := x;
  r := sprintf(ADR(str[0]), ADR("$%04x"), argo);
  WriteString(str);
END WriteHexU16;

(* WriteHexU32 - write hex signed 16-bit value *)
PROCEDURE WriteHexU32(l: LONGCARD);
VAR
  r: CARDINAL;
  argo: ARRAY [0..1] OF FormatArg;
  str: ARRAY [0..15] OF CHAR;
BEGIN
  argo[0].L := l;
  r := sprintf(ADR(str[0]), ADR("$%08lx"), argo);
  WriteString(str);
END WriteHexU32;

(* OutEA - output effective address *)
PROCEDURE OutEA(ea, size: CARDINAL);
VAR
  i    :INTEGER;
  w    :CARDINAL;
  d0   :CARDINAL;
  d1   :CARDINAL;
  mode :CARDINAL;
  reg  :CARDINAL;
  scl  :CARDINAL;
BEGIN
  mode := ea DIV 8;
  reg  := ea MOD 8;
  CASE (mode) OF
  | 0: (* Dn *)
       Write('D'); WriteRegNo(reg);
  | 1: (* An *)
       Write('A'); WriteRegNo(reg);
  | 2: (* (An) *)
       WriteString('(A'); WriteRegNo(reg); Write(')');
  | 3: (* (An)+ *)
       WriteString('(A'); WriteRegNo(reg); WriteString(')+');
  | 4: (* -(An) *)
       WriteString('-(A'); WriteRegNo(reg); Write(')');
  | 5: (* d(An) *)
       GetWord(w); WriteHexS16(INTEGER(w));
       WriteString('(A'); WriteRegNo(reg); Write(')');

  | 6: (* d(An,Xi) *)
       GetWord(w);

       d0 := w DIV 4096;            (* decode Xi type D0 or A0 *)
       i  := w MOD 256;             (* mask off 8-bit index *)
       IF (i > 127) THEN DEC(i, 256); END; (* translate to -128..+127 *)
       scl := (w DIV 512) MOD 4;    (* 68020 scale field *)

       WriteHexS8(i);
       WriteString('(A'); WriteRegNo(reg);
       IF (d0 < 8) THEN 
         WriteString(',D');
       ELSE
         WriteString(',A');
       END;
       WriteRegNo(d0 MOD 8);
       IF (ODD(w DIV 2048)) THEN
         WriteString('.L');
       ELSE
         WriteString('.W');
       END;
       CASE scl OF                  (* Check for 68020 here?? *)
          0: |
          1: WriteString('*2'); |
          2: WriteString('*4'); |
          3: WriteString('*8');
       END;
       Write(')');

  | 7: CASE (reg) OF
       | 0: (* Abs.W *)
            GetWord(w); WriteHexS16(INTEGER(w));
       | 1: (* Abs.L *)
            GetWord(d0); GetWord(d1); WriteHexU32(LONG(d0, d1));
       | 2: (* d(PC) *)
            GetWord(w); WriteHexS16(INTEGER(w));
            WriteString('(PC)');
       | 3: (* d(PC,Xi) *)
            GetWord(w);
            d0 := w DIV 4096; (* decode Xi type D0 or A0 *)
            i  := w MOD 256;  (* mask off 8-bit index *)
            IF (i > 127) THEN DEC(i, 256); END; (* translate to -128..+127 *)
            WriteHexS8(i);
            WriteString('(PC');
            IF (d0 < 8) THEN
              WriteString(',D');
            ELSE 
              WriteString(',A');
            END;
            WriteRegNo(d0 MOD 8);
            IF ODD(w DIV 2048) THEN
              WriteString('.L)');
            ELSE
              WriteString('.W)');
            END;
       | 4: (* Imm *)
            Write('#');
            CASE (size) OF
            | byte: GetWord(w); WriteHexU8(w);
            | word: GetWord(w); WriteHexU16(w);
            | long: GetWord(d0); GetWord(d1); WriteHexU32(LONG(d0, d1));
            END; (* CASE *)
       END; (* CASE *)
  END; (* CASE *)
END OutEA;

(* OutRegMask - output register list from mask *)
PROCEDURE OutRegMask(dir: BOOLEAN; mask: BITSET);
VAR
  rmask: BITSET;
  prev: BOOLEAN;
  i: CARDINAL;
  j: CARDINAL;
BEGIN
  (* dir: TRUE = reverse, FALSE = forward *)
  IF (dir) THEN (* Reverse mask *)
    rmask := {};
    j := 0;
    FOR i := 15 TO 0 BY -1 DO
      IF (i IN mask) THEN
        INCL(rmask, j);
      END;
      INC(j);
    END;
    mask := rmask;
  END;

  prev := FALSE;
  FOR i := 0 TO 15 DO
    IF (i IN mask) THEN
      IF (prev) THEN
        Write('/');
      END;
      prev := TRUE;
      IF (i < 8) THEN (* Dn *)
        Write('D');
        WriteRegNo(i);
      ELSE (* An *)
        Write('A');
        WriteRegNo(i-8);
      END;
    END;
  END;
END OutRegMask;

(* IsValidEA - check if address is valid for a given instruction *)
PROCEDURE IsValidEA(w: CARDINAL; mask: BITSET): BOOLEAN;
VAR
  ea: CARDINAL;
  mode: CARDINAL;
  reg: CARDINAL;
BEGIN
  ea   := w MOD 64;
  mode := ea DIV 8;
  reg  := ea MOD 8;
  IF (mode = 7) THEN
    IF (reg > 4) THEN
      RETURN (FALSE); (* Invalid! *)
    END;
    INC(mode, reg);
  END;
  RETURN (mode IN mask);
END IsValidEA;

(* OutOperand - output operand *)
PROCEDURE OutOperand(w, s: CARDINAL; ch: CHAR; validEAMask: BITSET): BOOLEAN;
VAR
  b: CARDINAL;
  v: CARDINAL;
  x: CARDINAL;
  y: CARDINAL;
  z: CARDINAL;
  i: INTEGER;
  base: LONGCARD;
  disp: LONGCARD;
  ea: CARDINAL;
  regMask: BITSET;
  direction: BOOLEAN;
  r12 :CARDINAL;
BEGIN
  v := w MOD 256;
  x := w DIV 512 MOD 8;
  y := w MOD 8;
  r12 := w DIV 4096 MOD 16;
  CASE (ch) OF
  | 'a': (* #eb (Imm BYTE) *)
         OutEA(IMME, byte);
  | 'b': (* #ew (Imm WORD) *)
         OutEA(IMME, word);
  | 'c': (* #es (Imm SIZE) *)
         OutEA(IMME, s);
  | 'd': (* <ea> (EA Any) *)
         IF (IsValidEA(w, validEAMask)) THEN
           OutEA(w MOD 64, s);
         ELSE
           RETURN (FALSE); (* Invalid Inst! *)
         END;
  | 'e': (* <ed> (EA Any MOVE inst dest only) *)
         w := (((w DIV 64) MOD 8) * 8) + x;
         IF (IsValidEA(w, validEAMask)) THEN
           OutEA(w MOD 64, s);
         ELSE
           RETURN (FALSE); (* Invalid Inst! *)
         END;
  | 'f': (* b (Bcc Inst) *)
         base := ADDRESS(CodeAdr);
         i    := v; (* load 8-bit offset *)
         IF (i = 0) THEN (* 16-bit offset *)
           GetWord(i);
         ELSIF (i > 127) THEN (* 8-bit negative offset *)
           DEC(i, 256);
         END;
         IF (i < 0) THEN (* negative *)
           disp := LONG(0FFFFH, i);
         ELSE
           disp := i;
         END;
         WriteHexU32(base + disp);
         WriteString('  [');
         WriteHexS16(i);
         Write(']');
  | 'g': (* #d (MOVEQ Imm) *)
         Write('#');
         IF (v > 127) THEN DEC(v, 256); END; (* Translate to -128..+127 *)
         WriteHexS8(INTEGER(v));
  | 'h': (* #v (TRAP #) *)
         Write('#'); WriteDecU8(w MOD 16);
  | 'i': (* #x (1..8 Const) *)
         Write('#');
         IF (x = 0) THEN
           z := 8;
         ELSE
           z := x;
         END;
         WriteDecU8(z);
  | 'j': (* Dx DEST *)
         OutEA(DDIR+x, undf);
  | 'k': (* Dy SRC *)
         OutEA(DDIR+y, undf);
  | 'l': (* Ax DEST *)
         OutEA(ADIR+x, undf);
  | 'm': (* Ay SRC *)
         OutEA(ADIR+y, undf);
  | 'n': (* -(Ax) DEST *)
         OutEA(ADEC+x, undf);
  | 'o': (* -(Ay) SRC *)
         OutEA(ADEC+y, undf);
  | 'p': (* (Ax)+ DEST *)
         OutEA(AINC+x, undf);
  | 'q': (* (Ay)+ SRC *)
         OutEA(AINC+y, undf);
  | 'r': (* SR *)
         WriteString('SR');
 (* 's' WARNING: Is used to represent dynamic size and cant be used here! *)
  | 't': (* CCR *)
         WriteString('CCR');
  | 'u': (* USP *)
         WriteString('USP');
  | 'v': (* Register List MOVEM inst *)
         direction := (((w DIV 8) MOD 8) = 4);
         GetWord(CARDINAL(regMask));
         IF (10 IN BITSET(w)) THEN (* MEMORY -> REGISTER *)
           IF (IsValidEA(w, {2,3,5..10})) THEN
             OutEA(w MOD 64, s);
             Write(',');
             OutRegMask(direction, regMask);
           ELSE
             RETURN (FALSE); (* Invalid Inst! *)
           END;
         ELSE (* REGISTER -> MEMORY *)
           IF (IsValidEA(w, {2,4..8})) THEN
             OutRegMask(direction, regMask);
             Write(',');
             OutEA(w MOD 64, s);
           ELSE
             RETURN (FALSE); (* Invalid Inst! *)
           END;
         END;
  | 'w': (* 16-disp (DBcc Inst) *)
         base := ADDRESS(CodeAdr);
         GetWord(i);
         IF (i < 0) THEN (* negative *)
           disp := LONG(0FFFFH, i);
         ELSE (* positive *)
           disp := i;
         END;
         WriteHexU32(base + disp);
         WriteString('  [');
         WriteHexS16(i);
         Write(']');
  | 'x': (* <ea> (EA All modes MOVE inst) *)
         OutEA(w MOD 64, s);
  | 'y': (* d(An) (MOVEP inst) *)
         OutEA(AOFF + y, s);
  | 'z': (* 16-bit value (LINK Inst) *)
         GetWord(w);
         WriteHexS16(INTEGER(w));
  | '{': (* for MUL32 *)
         IF ((w DIV 1024) MOD 2) = 1 THEN
           OutEA(DDIR + y, undf);
           Write('-');
         END;
         OutEA(DDIR + r12, undf);
  | '}': (* for DIV32 *)
         IF (((w DIV 1024) MOD 2) = 1) OR (y # r12) THEN
           OutEA(DDIR + y, undf);
           Write(':');
         END;
         OutEA(DDIR + r12, undf);
  | '|': (* for Reg is bit 12 *)
         OutEA(DDIR + r12, undf);   (* writes out A reg as well *)
  END; (* CASE *)
  RETURN (TRUE); (* Valid Inst! *)
END OutOperand;

(* OutOpcode - output instruction *)
PROCEDURE OutOpcode(w: CARDINAL): BOOLEAN;
VAR
  i: CARDINAL;
  j: CARDINAL;
  s: CARDINAL;
  ch: CHAR;
  oldI: CARDINAL;
  offset: POINTER TO ARRAY [0..255] OF CHAR;
  w1 :CARDINAL;
  extraChars :CARDINAL;

  (* GetCh - get next char from instruction string *)
  PROCEDURE GetCh; 
  BEGIN
    ch := offset^[i]; 
    INC(i)
  END GetCh;

BEGIN (* OutOpcode *)
  i := 0;
  LOOP
    IF i >= maxOpcd THEN
      ResetBuffer();
      WriteString(DefInvalidInstText);
      RETURN (FALSE); (* Invalid Instruction! *)
    END;
    WITH opcodes[i] DO
      offset := off;
      IF (BITSET(w) * mask = code) THEN (* mask and compare inst *)
        oldI := i; (* save current opcode table index *)
        LOOP
          i := ((w DIV d MOD m) * 9); (* calc inst string index *)
          GetCh;
          IF (ch = ' ') THEN (* No instruction at this string pos *)
            i := oldI;
            ResetBuffer();
            EXIT; (* return to scan loop *)
          ELSE
            s := word; (* Default size of operation *)
            WHILE (ch >= 'A') AND (ch <= 'Z') DO (* Copy instruction to out *)
              Write(ch);
              GetCh;
            END;
            extraChars := 0;

            w1 := w;
            IF ch = '+' THEN      (* MUL/DIVS.L or MUL/DIVU.L *)
              GetCh;
              GetWord(w1);
              IF ((w1 DIV 2048) MOD 2) = 1 THEN
                Write('S');
              ELSE
                Write('U');
              END;
              IF ((w DIV 64) MOD 2) = 1 THEN   (* divide *)
                IF (((w1 DIV 1024) MOD 2) = 1)
                      AND ((w1 MOD 8) = ((w1 DIV 4096) MOD 8)) THEN
                  Write('L');
                  extraChars := 1;
                END;
              END;
            ELSIF ch = '&' THEN    (* CHK2 or CMP2 *)
              GetCh;
              GetWord(w1);
              IF ((w1 DIV 2048) MOD 2) = 1 THEN
                WriteString('HK2');
              ELSE
                WriteString('MP2');
              END;
              extraChars := 2;
            END;

            IF (ch = '.') THEN (* Size specification? *)
              Write(ch); 
              GetCh;
              IF (ch = 'B') THEN (* BYTE *)
                s := byte;
              ELSIF (ch = 'W') THEN (* WORD *)
                s := word;
              ELSIF (ch = 'L') THEN (* LONG *)
                s := long;
              ELSE (* Dynamic size *)
                s := w DIV 64 MOD 4;
                IF (s = undf) THEN (* Invalid Size Bits *)
                  i := oldI;
                  ResetBuffer();
                  EXIT; (* return to scan loop *)
                END;
              END;
              GetCh;
              Write(size[s]); (* Output Size Char *)
            END;

            IF (ch # ' ') THEN (* Check for first operand type *)
              j := i+extraChars;
              WHILE (j MOD 9 # 0) DO (* Align output for first operand *)
                Write(' ');
                INC(j);
              END;
              IF (NOT OutOperand(w, s, ch, vea)) THEN (* Valid Inst? *)
                i := oldI;
                ResetBuffer();
                EXIT; (* return to scan loop *)
              END;
              GetCh;
              IF (ch # ' ') THEN (* Check for second operand type *)
                Write(',');
                IF (NOT OutOperand(w1, s, ch, vea)) THEN (* Valid Inst? *)
                  i := oldI;
                  ResetBuffer();
                  EXIT; (* return to scan loop *)
                END;
              END;
            END;
          END;
          RETURN (TRUE); (* Valid Instruction! *)
        END; (* LOOP *)
      END; (* IF *)
    END;
    INC(i); (* Next opcode table entry *)
  END;
END OutOpcode;

(* I - initialize fields of opcode record *)
PROCEDURE I(a, b: CARDINAL; c: BITSET; x, y: CARDINAL; s: ADDRESS);
BEGIN
  WITH opcodes[maxOpcd] DO
    mask := BITSET(a);
    code := BITSET(b); 
    vea  := c;
    d    := x;
    m    := y;
    off  := s;
  END;
  INC(maxOpcd);
END I;

(* ===== PUBLIC ===== *)

(* DisassembleInst - disassemble one instruction at adr *)
PROCEDURE DisassembleInst(adr: ADDRESS; outBuf: ADDRESS; VAR validInst: BOOLEAN;
                          VAR wordLen: CARDINAL);
VAR
  w: CARDINAL;
BEGIN
  CodeAdr       := adr;
  CodeAdrBase2  := adr + 2D;
  InstWordCount := 0;
  OutputBuffer  := outBuf;
  OutputBase    := outBuf;
  GetWord(w);
  validInst     := OutOpcode(w);
  wordLen       := InstWordCount;
  OutputBuffer^ := 0C;
END DisassembleInst;

(* InitDBDisassembler - initialize DBDisassembler module *)
PROCEDURE InitDBDisassembler(): BOOLEAN;
CONST
  NONE = {};
BEGIN

  maxOpcd    := 0;
  size[byte] := 'B'; 
  size[word] := 'W'; 
  size[long] := 'L';
  I(0FFFFH, 04AFCH, NONE,            1, 1, ADR('ILLEGAL  '));
  I(0FFBFH, 0003CH, NONE,           64, 2, ADR('ORIat    ORIbr    '));
  I(0FFBFH, 0023CH, NONE,           64, 2, ADR('ANDIat   ANDIbr   '));
  I(0FFBFH, 00A3CH, NONE,           64, 2, ADR('EORIat   EORIbr   '));

  IF WhichCPU > M68000 THEN
    I(0FFF8H, 04E70H, NONE,            1, 8, ADR('RESET    NOP      STOPb    RTE      RTDz     RTS      TRAPV    RTR      '));
  ELSE
    I(0FFF8H, 04E70H, NONE,            1, 8, ADR('RESET    NOP      STOPb    RTE               RTS      TRAPV    RTR      '));
  END;

  I(0FFF8H, 04840H, NONE,            1, 1, ADR('SWAPk    '));
  I(0FFB8H, 04880H, NONE,           64, 2, ADR('EXT.Wk   EXT.Lk   '));

  IF WhichCPU > M68000 THEN
    I(0FEF8H, 049C0H, NONE,          1, 1, ADR('EXTB.Lk  '));
  END;

  I(0FFF0H, 04E40H, NONE,            1, 1, ADR('TRAPh    '));
  I(0FFF0H, 04E50H, NONE,            8, 2, ADR('LINKmz   UNLKm    '));
  I(0FFF0H, 04E60H, NONE,            8, 2, ADR('MOVEmu   MOVEum   '));
  I(0F8F8H, 050C8H, NONE,          256, 8, ADR('DBTkw    DBFkw    DBHIkw   DBLSkw   DBCCkw   DBCSkw   DBNEkw   DBEQkw   '));
  I(0F8F8H, 058C8H, NONE,          256, 8, ADR('DBVCkw   DBVSkw   DBPLkw   DBMIkw   DBGEkw   DBLTkw   DBGTkw   DBLEkw   '));
  I(0FFC0H, 040C0H, {0,2..8},        1, 1, ADR('MOVErd   '));
  I(0FFC0H, 04AC0H, {0,2..8},        1, 1, ADR('TASd     '));

  IF WhichCPU > M68000 THEN
    I(0FF80H, 04C00H, {0,2..11},    64, 2, ADR('MUL+.Ld{ DIV+.Ld} '));
  END;

  I(0FDC0H, 044C0H, {0,2..11},     512, 2, ADR('MOVEdt   MOVEdr   '));
  I(0FF80H, 04880H, NONE,           64, 2, ADR('MOVEM.Wv MOVEM.Lv '));
  I(0FF80H, 04C80H, NONE,           64, 2, ADR('MOVEM.Wv MOVEM.Lv '));
  I(0FF80H, 04E80H, {2,5..10},      64, 2, ADR('JSRd     JMPd     '));
(**)  I(0FF00H, 00800H, {0,2..11},  64, 4, ADR('BTSTad   BCHGad   BCLRad   BSETad   '));
(**)  I(0FF00H, 04800H, {0,2..11},  64, 2, ADR('NBCDd    PEAd     '));
  I(0F8C0H, 050C0H, {0,2..8},      256, 8, ADR('STd      SFd      SHId     SLSd     SCCd     SCSd     SNEd     SEQd     '));
  I(0F8C0H, 058C0H, {0,2..8},      256, 8, ADR('SVCd     SVSd     SPLd     SMId     SGEd     SLTd     SGTd     SLEd     '));
  I(0F8C0H, 0E0C0H, {2..8},        256, 8, ADR('ASRd     ASLd     LSRd     LSLd     ROXRd    ROXLd    RORd     ROLd     '));
  I(0F1F8H, 0C188H, NONE,            1, 1, ADR('EXGjm    '));
  I(0F1F0H, 08100H, NONE,            8, 2, ADR('SBCDkj   SBCDon   '));
  I(0F1F0H, 0C100H, NONE,            8, 2, ADR('ABCDkj   ABCDon   '));
  I(0F1F0H, 0C140H, NONE,            8, 2, ADR('EXGjk    EXGlm    '));
  I(0F100H, 00000H, {0,2..8},      512, 8, ADR('ORI.scd  ANDI.scd SUBI.scd ADDI.scd          EORI.scd CMPI.scd          '));
  I(0F900H, 04000H, {0,2..8},      512, 4, ADR('NEGX.sd  CLR.sd   NEG.sd   NOT.sd   '));
  I(0FF00H, 04A00H, {0,2..8},        1, 1, ADR('TST.sd   '));
  I(0F138H, 0B108H, NONE,            1, 1, ADR('CMPM.sqp '));
  I(0F130H, 09100H, NONE,            8, 2, ADR('SUBX.skj SUBX.son '));
  I(0F130H, 0D100H, NONE,            8, 2, ADR('ADDX.skj ADDX.son '));
(**)  I(0F180H, 04180H, {0,2..11},  64, 2, ADR('CHKdj    LEAdl    '));
  I(0F0C0H, 080C0H, {0,2..11},     256, 2, ADR('DIVU.Wdj DIVS.Wdj '));
  I(0F0C0H, 090C0H, {0..11},       256, 2, ADR('SUBA.Wdl SUBA.Ldl '));
  I(0F0C0H, 0B0C0H, {0..11},       256, 2, ADR('CMPA.Wdl CMPA.Ldl '));
  I(0F0C0H, 0C0C0H, {0,2..11},     256, 2, ADR('MULU.Wdj MULS.Wdj '));
  I(0F0C0H, 0D0C0H, {0..11},       256, 2, ADR('ADDA.Wdl ADDA.Ldl '));
  I(0F800H, 06000H, NONE,          256, 8, ADR('BRAf     BSRf     BHIf     BLSf     BCCf     BCSf     BNEf     BEQf     '));
  I(0F800H, 06800H, NONE,          256, 8, ADR('BVCf     BVSf     BPLf     BMIf     BGEf     BLTf     BGTf     BLEf     '));
(**)  I(0F100H, 00100H, {0,2..11},  64, 4, ADR('BTSTjd   BCHGjd   BCLRjd   BSETjd   '));
  I(0F100H, 07000H, NONE,            1, 1, ADR('MOVEQgj  '));
  I(0F100H, 0E000H, NONE,            8, 8, ADR('ASR.sik  LSR.sik  ROXR.sik ROR.sik  ASR.sjk  LSR.sjk  ROXR.sjk ROR.sjk  '));
  I(0F100H, 0E100H, NONE,            8, 8, ADR('ASL.sik  LSL.sik  ROXL.sik ROL.sik  ASL.sjk  LSL.sjk  ROXL.sjk ROL.sjk  '));
  I(0F000H, 05000H, {0..8},        256, 2, ADR('ADDQ.sid SUBQ.sid '));
(**)  I(0F000H, 08000H, {0,2..11}, 256, 2, ADR('OR.sdj   OR.sjd   '));
(**)  I(0F000H, 09000H, {0..11},   256, 2, ADR('SUB.sdj  SUB.sjd  '));
(**)  I(0F000H, 0B000H, {0..11},   256, 2, ADR('CMP.sdj  EOR.sjd  '));
(**)  I(0F000H, 0C000H, {0,2..11}, 256, 2, ADR('AND.sdj  AND.sjd  '));
(**)  I(0F000H, 0D000H, {0..11},   256, 2, ADR('ADD.sdj  ADD.sjd  '));
  I(0C000H, 00000H, {0,2..8},     4096, 4, ADR('         MOVE.Bxe MOVE.Lxe MOVE.Wxe '));

  IF WhichCPU > M68000 THEN
    I(0F9C0H, 000C0H, {0,5..8,10,11}, 512, 4, ADR('C&.Bd|   C&.Wd|   C&.L|             '));
  END;

  I(0E0C0H, 02040H, {0..11},      4096, 2, ADR('MOVEA.LdlMOVEA.Wdl'));
  I(0F1B8H, 00108H, NONE,           64, 2, ADR('MOVEP.WyjMOVEP.Lyj'));
  I(0F1B8H, 00188H, NONE,           64, 2, ADR('MOVEP.WjyMOVEP.Ljy'));

  RETURN (TRUE);
END InitDBDisassembler;

(* CleanupDBDisassembler - Cleanup DBDissasembler module. *)
PROCEDURE CleanupDBDisassembler();
END CleanupDBDisassembler;

END DBDisassembler.
