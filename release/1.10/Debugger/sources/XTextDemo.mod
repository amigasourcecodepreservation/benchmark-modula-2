(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                      (c) 1986, 1987 by Jim Olinger                       *
 ****************************************************************************
 * Name: XTextDemo.MOD                     Version: Amiga.00.00             *
 * Created: 10/14/87   Updated: 10/14/87   Author: Leon Frenkel             *
 * Description: Modula-2 interface to high speed text output routines,      *
 * XText by R.J. Mical.  Simple demonstration of XText().                   *
 ****************************************************************************)

MODULE XTextDemo;

FROM SYSTEM IMPORT
	ADDRESS, BYTE,
	ADR, TSIZE;
FROM CPrintBuffer IMPORT
	sprintf;
FROM FormatString IMPORT
	FormatArg;
FROM Drawing IMPORT
	SetAPen, SetBPen, Move, RectFill;
FROM Intuition IMPORT
	WindowPtr,
	IDCMPFlagsSet, IDCMPFlags,
	WindowFlagsSet, WindowFlags, SmartRefresh,
	CloseWindow, SetWindowTitles;
FROM Ports IMPORT
	GetMsg;
FROM Rasters IMPORT
	RastPortPtr;
FROM SimpleWindows IMPORT
	CreateWindow;
FROM Text IMPORT
	TextAttr, TextAttrPtr,
	FontStyle, FontStyleSet, NormalFontStyle,
	FontFlags, FontFlagsSet,
	SetSoftStyle, Text;
FROM XText IMPORT
	NormalFont, BoldFont, ULineFont, ULineBoldFont, ItalicFont,
	ItalicBoldFont, ItalicULineFont, ItalicULineBoldFont,
	XTextFlags, XTextFlagsSet,
	XTextSupport, XTextSupportPtr,
	XText;
FROM XTextUtil IMPORT
	DefaultXTextFont,
	MakeXTextFont, MakeXTextSupport, UnmakeXTextSupport;

CONST
  XTexting         = TRUE;  (* TRUE = use XText(), FALSE = use Text() *)
  ShowSlimXTexting = FALSE;  (* TRUE = slim, FALSE = fat *)
  Topaz11XTexting  = FALSE; (* TRUE = topaz 11, FALSE = topaz 8 *)

VAR
  SafeFont : TextAttr;
  Diskfont : TextAttr;

PROCEDURE infoLine(VAR xtext: XTextSupport; line: INTEGER);
VAR
  selectsave: INTEGER;
  pen: BYTE;
BEGIN
  selectsave := xtext.FontSelect;

  pen := xtext.FrontPen;
  xtext.FrontPen := xtext.BackPen;
  xtext.BackPen := pen;

  xtext.FontSelect := NormalFont;
  XText(xtext,
     ADR("- Every time the color changes the text has been entirely redrawn "), 
      66, 0, line);
  xtext.FontSelect := BoldFont;
  XText(xtext, ADR("eight times! -"), -1, 66 * 8, line);

  pen := xtext.FrontPen;
  xtext.FrontPen := xtext.BackPen;
  xtext.BackPen := pen;
  xtext.FontSelect := selectsave;
END infoLine;

PROCEDURE advancePens(VAR xtext: XTextSupport);
VAR
  frontpen, backpen: INTEGER;
BEGIN
  frontpen := INTEGER(xtext.FrontPen);
  backpen  := INTEGER(xtext.BackPen);

  INC(backpen);
  IF (backpen = frontpen) THEN INC(backpen); END;
  IF (backpen > 3) THEN
    backpen := 0;
    INC(frontpen);
    IF (frontpen > 3) THEN
      frontpen := 0;
      backpen := 1;
    END;
  END;
  xtext.FrontPen := BYTE(frontpen);
  xtext.BackPen  := BYTE(backpen);
END advancePens;

PROCEDURE myAddFont(VAR xtext: XTextSupport; style: FontStyleSet;
                    index: INTEGER; font: TextAttrPtr);
VAR
  p: ADDRESS;
BEGIN
  IF (font = NIL) THEN
    font := ADR(SafeFont);
  END;
  font^.taStyle := style;

  (* I could check if the next routine returned NULL, but since it
   * doesn't hurt anything if it fails, I just won't bother.  It's ok.
   *)
  p := MakeXTextFont(font, xtext, index);
END myAddFont;

PROCEDURE main();
VAR
  testfont: TextAttrPtr;
  window: WindowPtr;
  xtext: XTextSupportPtr;
  rp: RastPortPtr;
  buffer: ARRAY [0..22],[0..79] OF BYTE;
  count, i, i2, line, lineindex: INTEGER;
  res: FontStyleSet;
  nextchar: CHAR;
  argo: ARRAY [0..9] OF FormatArg;
  r: INTEGER;
BEGIN
  LOOP
    window := CreateWindow(0,0,640,200, IDCMPFlagsSet{Closewindow},
                           WindowFlagsSet{WindowDepth,WindowClose,Activate,
                           Borderless,NoCareRefresh}+SmartRefresh,
                           NIL, NIL, ADR("XText Window"));
    IF (window = NIL) THEN
      EXIT; (* error *)
    END;

    rp := window^.RPort;

    SetAPen(rp^, 1);
    RectFill(rp^, 28, 0, 586, 9);
    SetWindowTitles(window^,
                  ADR("                      XText Demonstration Window"), -1D);
    Move(rp^, 216, 32);
    Text(rp^, ADR("Please wait a moment ..."), 24);

    IF (Topaz11XTexting) THEN
      testfont := ADR(Diskfont);
    ELSE
      testfont := NIL;
    END;

    IF (ShowSlimXTexting) THEN
      xtext := MakeXTextSupport(rp^, testfont, 80, XTextFlagsSet{SlimXText});
    ELSE
      xtext := MakeXTextSupport(rp^, testfont, 80, XTextFlagsSet{});
    END;
    IF (xtext = NIL) THEN
      EXIT; (* error *)
    END;

    IF (XTexting) THEN
      myAddFont(xtext^, FontStyleSet{Bold}, BoldFont, testfont);
      myAddFont(xtext^, FontStyleSet{Underlined}, ULineFont, testfont);
      myAddFont(xtext^, FontStyleSet{Underlined,Bold}, ULineBoldFont, testfont);
      myAddFont(xtext^, FontStyleSet{Italic}, ItalicFont, testfont);
      myAddFont(xtext^, FontStyleSet{Italic,Bold}, ItalicBoldFont, testfont);
      myAddFont(xtext^, FontStyleSet{Italic,Underlined}, ItalicULineFont, testfont);
      myAddFont(xtext^, FontStyleSet{Italic,Underlined,Bold}, 
                ItalicULineBoldFont, testfont);
    END;

    nextchar := CHAR(32);
    FOR i := 0 TO 22 DO
      FOR i2 := 0 TO 79 DO
        buffer[i][i2] := nextchar;
        INC(nextchar);
      END;
    END;

    line := 12;
    lineindex := 0;
    count := 0;

    WHILE (GetMsg(window^.UserPort^) = NIL) DO
      IF (XTexting) THEN
        IF (lineindex = 2) THEN
          infoLine(xtext^, line);
        ELSE
          XText(xtext^, ADR(buffer[lineindex][0]), 80, 0, line);
        END;
      ELSE
        Move(rp^, 0, line+6); (* Adjust since it's base line relative *)
        Text(rp^, ADR(buffer[lineindex][0]), 80);
      END;

      INC(line, xtext^.CharHeight);
      INC(lineindex);

      IF (line > 192) THEN
        INC(count);

        argo[0].W := count;
        r := sprintf(ADR(buffer[3][33]), ADR("-  %d  -"), argo);

        line := 12;
        lineindex := 0;

        INC(xtext^.FontSelect);
        IF (xtext^.FontSelect >= 8) THEN
          xtext^.FontSelect := 0;
          advancePens(xtext^);
        END;

        IF (NOT XTexting) THEN
          SetAPen(rp^, CARDINAL(xtext^.FrontPen));
          SetBPen(rp^, CARDINAL(xtext^.BackPen));
          res := SetSoftStyle(rp^, FontStyleSet(xtext^.FontSelect), 
                            FontStyleSet{Underlined,Bold,Italic,Extended});
        END;
      END;
    END; (* WHILE *)

    EXIT;
  END; (* LOOP *)
  IF (xtext # NIL) THEN UnmakeXTextSupport(xtext^); END;
  IF (window # NIL) THEN CloseWindow(window^); END;
END main;

BEGIN
  WITH SafeFont DO
    taName  := ADR("topaz.font");
    taYSize := 8; (* TopazEighty *)
    taStyle := NormalFontStyle;
    taFlags := FontFlagsSet{};
  END;
  WITH Diskfont DO
    taName  := ADR("topaz.font");
    taYSize := 11;
    taStyle := NormalFontStyle;
    taFlags := FontFlagsSet{DiskFont};
  END;
  main();
END XTextDemo.
