(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: DBMenusKbd.MOD                    Version: Amiga.01.10             *
 * Created: 01/08/89   Updated: 03/21/94   Author: Leon Frenkel             *
 * Description: Debugger pull-down menu and keyboard module.                *
 ****************************************************************************)

IMPLEMENTATION MODULE DBMenusKbd;

(***************************************************************************
 * Changes for new release 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.2 -----
 *
 * Support for Clipboard
 * 
 *   1. Added a new WCommand: "WCmdClip".
 * 
 *   2. Added a "Clip Window" menu item to global menus in InitDBMenusKbd(),
 *      just after "Duplicate Window". Disabled if not OSVersion2.
 *      No kbd command for it.
 * 
 *   3. Changed DoGlobalMenu to include a VTWindow param as well as class
 *      and Index. This is for a VTWindow generic procedure like "clip".
 * 
 *      Changed all the window types to call DoGlobalMenu() with the vtwindow
 *      parameter, except Source or Status window send NIL.
 * 
 *   4. DBTextWindows.ClipWindow() is called from DoWindowCommand() when 
 *      menu item Clip Window is chosen.
 * 
 *   5. Added ClipErrMsgBadWin error message, displays when the VT sent back
 *      via DoGlobalMenu() is NIL and Clip Window was the menu selection.
 *      (ie, for Status Window and Source Windows).
 *
 *               ----- Beta.4 -----
 * 
 * Support ReadArgs()
 * 
 *    1. In DoLoadProg(), if RequestProgParms() is done and there is room,
 *       a LF is appended to the ProgParmsBuffer.
 * 
 ***************************************************************************
*)

FROM SYSTEM IMPORT
  ADDRESS, BYTE, WORD, LONGWORD,
  ADR;
FROM CMemOp IMPORT
  movmem, repmem;
FROM CStrings IMPORT
  strlen;
FROM DBBreakPoints IMPORT
  DeleteBreakPoints, DisableBreakPoints, EnableBreakPoints, DisableBreakPoint,
  EnableBreakPoint, CreateBreakPoint, DeleteBreakPoint, IsAdrBreakPoint,
  SetBreakPointLimit, GetBreakPointLimit;
FROM DBConfiguration IMPORT
  ConfigPrivateScreen, ConfigInterlaceScreen, ConfigCheckModFileDate,
  ConfigStringSearchCase, ConfigMaxModFilesLoaded, ConfigProgPriority,
  ConfigProgStackSize, ConfigProgCurrentDir, ConfigProgStartupMode,
  ConfigProgLanguageMode, ConfigSymbolFileDirList, ConfigSourceFileDirList,
  ConfigScreenColors, DirNodePtr,
  SetNewWindowParms, SaveConfigFile, LoadConfigFile, ConvDirListToString,
  ConvStringToDirList, SetNewDirList, SetNewProgCurrentDir;
FROM DBEnvironmentInquiry IMPORT
  OSVersion2;
FROM DBFiles IMPORT
  InvalidAbsAdr;
FROM DBInitCleanup IMPORT
  Abort;
FROM DBMisc IMPORT
  DeallocString, GetMemoryLong, GetMemoryValue, GetMemoryString, PutMemoryLong,
  PutMemoryValue, PutMemoryString, SearchMemoryValue;
FROM DBRegistersWindows IMPORT
  PCRegNo, SRRegNo;
FROM DBRequesters IMPORT
  RequestMaxStringLength,
  RequestBreakCount, RequestFileName, RequestProgParms,
  RequestSymbolFileDirList, RequestSourceFileDirList, RequestProcessPriority,
  RequestProcessStackSize, RequestProcessCurrentDir, RequestMaxModFilesLoaded,
  RequestScreenColors, RequestChangeRealValue, RequestChangeStringValue,
  RequestChangeNumberValue, RequestChangeRegister, RequestAddress,
  RequestSearchMemory, RequestFillMemory, RequestCopyMemory, 
  RequestContinueSearch;
FROM DBSelection IMPORT
  SelectionData, StorageFormat,
  ClearSelection, GetSelection;
FROM DBStatusWindow IMPORT
  ShowError, ConfigStatusWindow;
FROM DBSymDataWindows IMPORT
  RefreshSymDataWindows;
FROM DBTextWindows IMPORT
  VTWindow, ClipVTWindow,
  GetIntuiWindowVTWindow;
FROM DBUserProgram IMPORT
  ExecutionMode, LanguageMode, StartupMode,
  ContinueUserProgram, StopUserProgram, ExitUserProgram, IsProgramLoaded,
  RunProgramAsSLD, GetRegisterValue, GetRegisterPCValue, GetRegisterSRValue,
  PutRegisterValue, PutRegisterPCValue, PutRegisterSRValue;
FROM DBWindows IMPORT
  WindowClass, WindowClassSet,
  OpenConfigWindows, OpenNewWindow, DuplicateWindow, CloseUserWindows,
  GetScreen, ConfigScreen, UpdateWindows;
FROM Interrupts IMPORT
  Forbid, Permit;
FROM Intuition IMPORT
  MenuFlags,
  MenuItemFlags, MenuItemFlagsSet,
  MenuItemMutualExcludeSet,
  MenuPtr, Menu, MenuItemPtr, WindowPtr,
  ItemAddress, OnMenu, OffMenu;
FROM SimpleMenus IMPORT
  DefaultItemFlags, MenuItemHeight,
  BeginMenuStrip, EndMenuStrip, BeginSubMenu, EndSubMenu, FreeMenuStrip,
  AddMenu, AddMenuItem, MenuItemOpt;

CONST
  (* User selection error messages *)
  SelErrMsgBadTempBP = "Invalid temporary break point selection.";
  SelErrMsgNoTempBP  = "A temporary break point selection has not been made.";
  SelErrMsgBadValue  = "Invalid value change selection.";
  SelErrMsgBadBP     = "Invalid break point address selection.";
  SelErrMsgNoBP      = "A break point selection has not been made.";
  MemErrMsgBadAdr    = "Invalid memory address selection.";
  MemErrMsgBadEnd    = "End address must be greater than start address.";
  ClipErrMsgBadWin   = "Clipping not implemented for selected window type.";

  (* Configuration file error messages *)
  LoadConfigErrMsg = "Loading configuration file.";
  SaveConfigErrMsg = "Saving configuration file.";

  (* Global menu strip menu numbers *)
  ProjectMenu     = 0;
  CommandMenu     = 1;
  BreakPointsMenu = 2;
  OpenWindowMenu  = 3;
  OptionsMenu     = 4;

  (* Project menu strip item numbers *)
  LoadProgItem   = 0;
  LoadConfigItem = 2;
  SaveConfigItem = 3;
  QuitItem       = 5;

  (* Command menu strip item numbers *)
  GoItem           =  0;
  GoTempItem       =  1;
  HaltItem         =  2;
  StepInItem       =  4;
  StepOverItem     =  5;
  RunInItem        =  7;
  RunOverItem      =  8;
  StopItem         =  9;
  SetSrcAdrItem    = 11;
  SetDstAdrItem    = 12;
  SetEndAdrItem    = 13;
  SetSizeItem      = 14;
  SearchMemoryItem = 15;
  FillMemoryItem   = 16;
  CopyMemoryItem   = 17;

  (* OpenWindowMenu menu strip item numbers *)
  ClipWindowItem   = 14;

  (* Options menu strip item numbers *)
  M2Item          =  0;
  AsmItem         =  1;
  FirstInstItem   =  7;
  LibModItem      =  8;
  MainModItem     =  9;
  WorkBenchItem   = 11;
  CustomItem      = 12;
  InterlacedItem  = 13;
  SetColorsItem   = 14;
  CaseSearchItem  = 16;
  VerifyModItem   = 17;

  (* Start of special key sequence *)
  CSI = CHAR(09BH);

  (* Keyboard independant keycodes *)
  DELKey  = 127;
  F1Key   = 256; 
  F2Key   = 257;
  F3Key   = 258;
  F4Key   = 259;
  F5Key   = 260;
  F6Key   = 261;
  F7Key   = 262;
  F8Key   = 263;
  F9Key   = 264;
  F10Key  = 265;
  F1SKey  = 266;
  F2SKey  = 267;
  F3SKey  = 268;
  F4SKey  = 269;
  F5SKey  = 270;
  F6SKey  = 271;
  F7SKey  = 272;
  F8SKey  = 273;
  F9SKey  = 274;
  F10SKey = 275;

  (* MC68000 CPU registers *)
  D0 = 0; D1 = 1; D2 = 2; D3 = 3; D4 = 4; D5 = 5; D6 = 6; D7 = 7;
  A0 = 8; A1 = 9; A2 =10; A3 =11; A4 =12; A5 =13; A6 =14; A7 =15; 

  (* List of windows to update for Update All Windows command *)
  UpdateAllWindows = WindowClassSet{WCSource, WCDisassembly, WCSymData,
                                    WCMemory, WCInfo, WCDirectory};


TYPE
  (* DoBreakPointCommand() command codes *)
  BPCommand     = (BPCmdSet, BPCmdClear, BPCmdEnable, BPCmdDisable, BPCmdCount);

  (* DoWindowCommand() command codes *)
  WCommand      = (WCmdDuplicate, WCmdSetDefaults, WCmdClip);

  (* DoConfigFileCommand() command codes *)
  ConfigCommand = (CCmdLoad, CCmdSave);

  (* DoMemoryCommand() command codes *)
  MemCommand    = (MCmdSrcAdr, MCmdDstAdr, MCmdEndAdr,
                   MCmdSearch, MCmdFill, MCmdCopy);

VAR
  GlobalMenuStrip   : MenuPtr;  (* Global menu strip or NIL *)
  LocalMenuLeftEdge : CARDINAL; (* x pos of first window specific menu *)

  UserCommandMode : CommandMode; (* current kbd/menu command mode *)

  (* Memory operation selections *)
  MemSrcAdr    : ADDRESS;   (* source address for memory operation *)
  MemDstAdr    : ADDRESS;   (* destination address for memory operation *)
  MemEndAdr    : ADDRESS;   (* end address of source or destination *)
  MemValueSize : CARDINAL;  (* value size for fill/sarch (1,2 or 4) *)

  (* Storage buffer for user program parameters *)
  ProgParmsBuffer : ARRAY [0..RequestMaxStringLength] OF CHAR;

(* ===== PRIVATE ===== *)

(* CalcMenuCode - convert menu number into a menu code *)
PROCEDURE CalcMenuCode(menuNum: CARDINAL): CARDINAL;
BEGIN
  RETURN (menuNum + 0FFE0H);
END CalcMenuCode;

(* CalcMenuItemCode - convert menu number and item number into menu code *)
PROCEDURE CalcMenuItemCode(menuNum, itemNum: CARDINAL): CARDINAL;
VAR
  code: CARDINAL;
BEGIN
  code := menuNum + (itemNum * 20H);
  RETURN (code + 0F800H);
END CalcMenuItemCode;

(* EnableProjectMenuConfig - enable/disable project/configuration items *)
PROCEDURE EnableProjectMenuConfig(enabled: BOOLEAN);
BEGIN
  EnableMenuItem(GlobalMenuStrip^, ProjectMenu, LoadConfigItem, enabled);
  EnableMenuItem(GlobalMenuStrip^, ProjectMenu, SaveConfigItem, enabled);
END EnableProjectMenuConfig;

(* EnableProjectMenuProg - enable/disable project/load,quit items *)
PROCEDURE EnableProjectMenuProg(enabled: BOOLEAN);
BEGIN
  EnableMenuItem(GlobalMenuStrip^, ProjectMenu, LoadProgItem, enabled);
  EnableMenuItem(GlobalMenuStrip^, ProjectMenu, QuitItem, enabled);
END EnableProjectMenuProg;

(* EnableCommandMenuExec - enable/disable command/execution items *)
PROCEDURE EnableCommandMenuExec(enabled: BOOLEAN);
BEGIN
  EnableMenuItem(GlobalMenuStrip^, CommandMenu, GoItem, enabled);
  EnableMenuItem(GlobalMenuStrip^, CommandMenu, GoTempItem, enabled);
  EnableMenuItem(GlobalMenuStrip^, CommandMenu, HaltItem, enabled);
  EnableMenuItem(GlobalMenuStrip^, CommandMenu, StepInItem, enabled);
  EnableMenuItem(GlobalMenuStrip^, CommandMenu, StepOverItem, enabled);
  EnableMenuItem(GlobalMenuStrip^, CommandMenu, RunInItem, enabled);
  EnableMenuItem(GlobalMenuStrip^, CommandMenu, RunOverItem, enabled);
END EnableCommandMenuExec;

(* EnableCommandMenuMemory - enable/disable command/memory items *)
PROCEDURE EnableCommandMenuMemory(enabled: BOOLEAN);
BEGIN
  EnableMenuItem(GlobalMenuStrip^, CommandMenu, SetSrcAdrItem, enabled);
  EnableMenuItem(GlobalMenuStrip^, CommandMenu, SetDstAdrItem, enabled);
  EnableMenuItem(GlobalMenuStrip^, CommandMenu, SetEndAdrItem, enabled);
  EnableMenuItem(GlobalMenuStrip^, CommandMenu, SetSizeItem, enabled);
  EnableMenuItem(GlobalMenuStrip^, CommandMenu, SearchMemoryItem, enabled);
  EnableMenuItem(GlobalMenuStrip^, CommandMenu, FillMemoryItem, enabled);
  EnableMenuItem(GlobalMenuStrip^, CommandMenu, CopyMemoryItem, enabled);
END EnableCommandMenuMemory;

(* CheckOptionsMenuItem - set/clear check mark in Options menu item *)
PROCEDURE CheckOptionsMenuItem(itemNum: CARDINAL; setCheck: BOOLEAN);
VAR
  mi: MenuItemPtr;
BEGIN
  mi := ItemAddress(GlobalMenuStrip^, CalcMenuItemCode(OptionsMenu, itemNum));
  IF (mi # NIL) THEN
    IF (setCheck) THEN
      INCL(mi^.Flags, Checked);
    ELSE
      EXCL(mi^.Flags, Checked);
    END;
  END;
END CheckOptionsMenuItem;

(* CalcKeyCode - convert an ascii keystring into a keycode *)
PROCEDURE CalcKeyCode(buf: ADDRESS; length: CARDINAL): CARDINAL;
VAR
  res: CARDINAL;
  charBuf: POINTER TO ARRAY [0..255] OF CHAR;
  ch0: CHAR;
  ch1: CHAR;
  ch2: CHAR;
  ch3: CHAR;
BEGIN
  charBuf := buf;
  ch0     := charBuf^[0];
  ch1     := charBuf^[1];
  ch2     := charBuf^[2];
  ch3     := charBuf^[3];

  IF (length = 1) THEN (* Normal keyboard character *)
    res := CARDINAL(ch0);
  ELSIF (length = 3) AND (ch0 = CSI) AND
        (ch1 >= "0") AND (ch1 <= "9") AND (ch2 = "~") THEN (* Function Key *)
    res := F1Key + (CARDINAL(ch1) - CARDINAL("0"));
  ELSIF (length = 4) AND (ch0 = CSI) AND (ch1 = "1") AND
        (ch2 >= "0") AND (ch2 <= "9") AND (ch3 = "~") THEN (* Shifted Func Key*)
    res := F1SKey + (CARDINAL(ch2) - CARDINAL("0"));
  ELSE (* unknown keyboard character *)
    res := 0;
  END;
  RETURN (res);
END CalcKeyCode;

(* DoGoAndSetTempBP - set a temporary break point and execute program *)
PROCEDURE DoGoAndSetTempBP();
VAR
  selData: SelectionData;
BEGIN
  IF (UserCommandMode = CMWait) THEN
    IF (GetSelection(selData)) THEN
      WITH selData DO
        IF (sdWindowClass = WCSource) OR (sdWindowClass = WCDisassembly) OR
           (sdWindowClass = WCRegisters) THEN
          IF (NOT ODD(sdAdr)) THEN (* Must be even address *)
            ClearSelection();
            ContinueUserProgram(EMGo, ConfigProgLanguageMode, sdAdr);
          END;
        ELSE (* invalid selection for temporary bp *)
          ShowError(ADR(SelErrMsgBadTempBP));
        END;
      END;
    ELSE (* No user selection  for temporary bp *)
      ShowError(ADR(SelErrMsgNoTempBP));
    END;
  END;
END DoGoAndSetTempBP;

(* DoBreakPointCommand - perform break point command on a selected address *)
PROCEDURE DoBreakPointCommand(cmd: BPCommand);
VAR
  selData: SelectionData;
  win: WindowClass;
  absAdr: ADDRESS;
  limit: LONGCARD;
  bpSet: BOOLEAN;
BEGIN
  IF (UserCommandMode = CMWait) THEN
    IF (GetSelection(selData)) THEN
      WITH selData DO
        win    := sdWindowClass;
        absAdr := sdAdr;
      END;
      IF (win = WCSource) OR (win = WCDisassembly) OR
         (win = WCBreakPointList) OR (win = WCRegisters) THEN
        IF (NOT ODD(absAdr)) THEN (* Must be even address *)
          ClearSelection();

          bpSet := IsAdrBreakPoint(absAdr);
          CASE (cmd) OF
          | BPCmdSet:     IF (NOT bpSet) THEN
                            CreateBreakPoint(absAdr);
                          END;
          | BPCmdClear:   IF (bpSet) THEN
                            DeleteBreakPoint(absAdr);
                          END;
          | BPCmdEnable:  IF (bpSet) THEN
                            EnableBreakPoint(absAdr);
                          END;
          | BPCmdDisable: IF (bpSet) THEN
                            DisableBreakPoint(absAdr);
                          END;
          | BPCmdCount:   IF (bpSet) THEN
                            limit := GetBreakPointLimit(absAdr);
                            IF (RequestBreakCount(limit)) THEN
                              SetBreakPointLimit(absAdr, limit);
                            END;
                          END;
          END;
        END;
      ELSE (* Invalid break point command selection *)
        ShowError(ADR(SelErrMsgBadBP));
      END;
    ELSE (* No break point command selection *)
      ShowError(ADR(SelErrMsgNoBP));
    END;
  END;
END DoBreakPointCommand;

(* DoWindowCommand - perform window command on the current window *)
PROCEDURE DoWindowCommand(cmd: WCommand; winClass: WindowClass;
                          winIndex:CARDINAL; vtWin:VTWindow);
BEGIN
  IF (winIndex # InvalidWindowIdx) THEN
    CASE (cmd) OF
    | WCmdDuplicate:   DuplicateWindow(winClass, winIndex);
    | WCmdSetDefaults: SetNewWindowParms(winClass, winIndex);
    | WCmdClip:        IF winClass = WCSource THEN
                         ShowError(ADR(ClipErrMsgBadWin));
                       ELSE
                         ClipVTWindow(vtWin);
                       END;
    END;
  END;
END DoWindowCommand;

(* DoConfigFileCommand - perform config command on selected file *)
PROCEDURE DoConfigFileCommand(cmd: ConfigCommand);
VAR
  selData: SelectionData;
  nameBuf: ARRAY [0..RequestMaxStringLength] OF CHAR;
  fileName: ADDRESS;
BEGIN
  IF (UserCommandMode = CMNoProg) OR (UserCommandMode = CMWait) THEN
    fileName := NIL;
    IF (GetSelection(selData)) THEN
      WITH selData DO
        IF (sdWindowClass = WCDirectory) THEN
          fileName := sdFilePath;
        END;
      END;
    END;
    IF (fileName = NIL) THEN
      nameBuf[0] := 0C;
      IF (RequestFileName(ADR(nameBuf[0]))) THEN
        fileName := ADR(nameBuf[0]);
      END;
    END;
    IF (fileName # NIL) THEN
      CASE (cmd) OF
      | CCmdLoad: IF (LoadConfigFile(fileName)) THEN
                    CloseUserWindows();
                    ConfigScreen();
                    ConfigStatusWindow();
                    SetupOptionsMenu();
                    OpenConfigWindows();
                  ELSE
                    ShowError(ADR(LoadConfigErrMsg));
                  END;
      | CCmdSave: IF (NOT SaveConfigFile(fileName)) THEN
                    ShowError(ADR(SaveConfigErrMsg));
                  END;
      END;
    END;
  END;
END DoConfigFileCommand;

(* DoLoadProg - load a user selected program or prompt for name *)
PROCEDURE DoLoadProg();
VAR
  selData: SelectionData;
  nameBuf: ARRAY [0..RequestMaxStringLength] OF CHAR;
  fileName: ADDRESS;
  len: INTEGER;
BEGIN
  IF (UserCommandMode = CMNoProg) THEN
    fileName := NIL;
    IF (GetSelection(selData)) THEN
      WITH selData DO
        IF (sdWindowClass = WCDirectory) THEN
          fileName := sdFilePath;
        END;
      END;
    END;
    IF (fileName = NIL) THEN
      nameBuf[0] := 0C;
      IF (RequestFileName(ADR(nameBuf[0]))) THEN
        fileName := ADR(nameBuf[0]);
      END;
    END;
    IF (fileName # NIL) THEN
      ProgParmsBuffer[0] := 0C;
      IF (RequestProgParms(ADR(ProgParmsBuffer[0]))) THEN
        len := strlen(ADR(ProgParmsBuffer[0]));
        IF len < (HIGH(ProgParmsBuffer)-2) THEN
          ProgParmsBuffer[len] := 12C;    (* LF for ReadArgs() *)
          ProgParmsBuffer[len+1] := 0C;   (* necessary? *)
        END;
        RunProgramAsSLD(fileName, ADR(ProgParmsBuffer[0]),
                                  strlen(ADR(ProgParmsBuffer[0]))+1);
      END;
    END;
  END;
END DoLoadProg;

(* DoChangeValue - change a selected memory or register value *)
PROCEDURE DoChangeValue(ignoreMode: BOOLEAN);
TYPE
  LONGSET = SET OF [0..31];

VAR
  selData: SelectionData;
  err: BOOLEAN;
  changed: BOOLEAN;
  SRValue: BITSET;
  setValue: LONGSET;
  oldReal: REAL;
  newReal: REAL;
  oldNumber: LONGCARD;
  newNumber: LONGCARD;
  oldStr: ARRAY [0..RequestMaxStringLength] OF CHAR;
  newStr: ARRAY [0..RequestMaxStringLength] OF CHAR;
BEGIN
  IF (UserCommandMode = CMWait) OR ((ignoreMode) AND
     ((UserCommandMode = CMRun) OR (UserCommandMode = CMExec))) THEN
    IF (GetSelection(selData)) THEN
      WITH selData DO
        err     := FALSE;
        changed := FALSE;
        IF (sdWindowClass = WCSymData) OR (sdWindowClass = WCMemory) THEN
          IF (sdStorageFormat = SFReal) THEN
            oldReal := REAL(GetMemoryLong(sdAdr));
            changed := RequestChangeRealValue(oldReal, newReal);
            IF (changed) THEN
              PutMemoryLong(sdAdr, newReal);
            END;
          ELSIF (sdStorageFormat = SFString) THEN
            GetMemoryString(sdAdr, ADR(oldStr[0]), RequestMaxStringLength);
            changed := RequestChangeStringValue(ADR(oldStr[0]), ADR(newStr[0]));
            IF (changed) THEN
              PutMemoryString(sdAdr, ADR(newStr[0]));
            END;
          ELSIF (sdStorageFormat = SFNumber) THEN
            oldNumber := GetMemoryValue(sdAdr, FALSE, sdStorageSize, NIL);
            changed   := RequestChangeNumberValue(oldNumber, sdStorageSize,
                                                  sdStorageNumber, newNumber);
            IF (changed) THEN
              PutMemoryValue(sdAdr, newNumber, sdStorageSize);
            END;
          ELSIF (sdStorageFormat = SFSet) THEN
            changed  := TRUE;
            setValue := LONGSET(GetMemoryValue(sdAdr, FALSE, sdStorageSize, NIL));
            IF (sdStorageBitNo IN setValue) THEN
              EXCL(setValue, sdStorageBitNo);
            ELSE
              INCL(setValue, sdStorageBitNo);
            END;
            PutMemoryValue(sdAdr, setValue, sdStorageSize);
          ELSIF (sdStorageFormat = SFNone) THEN
            err := TRUE;
          END;
          IF (changed) THEN
            UpdateWindows(WindowClassSet{WCMemory});
            RefreshSymDataWindows();
          END;
        ELSIF (sdWindowClass = WCRegisters) THEN
          IF (UserCommandMode = CMWait) THEN
            IF (sdStorageRegNo >= D0) AND (sdStorageRegNo <= A7) THEN
              oldNumber := GetRegisterValue(sdStorageRegNo);
              changed   := RequestChangeRegister(oldNumber, newNumber);
              IF (changed) THEN
                IF (sdStorageRegNo = A7) AND (ODD(newNumber)) THEN
                  DEC(newNumber);
                END;
                (* If register is A5 or A7 then all windows updated here *)
                PutRegisterValue(sdStorageRegNo, newNumber);
                changed := NOT ((sdStorageRegNo = A7) OR (sdStorageRegNo = A5));
              END;
            ELSIF (sdStorageRegNo = PCRegNo) THEN
              oldNumber := GetRegisterPCValue();
              IF (RequestChangeRegister(oldNumber, newNumber)) THEN
                IF (ODD(newNumber)) THEN
                  DEC(newNumber);
                END;
                (* All windows are updated by this procedure *)
                PutRegisterPCValue(newNumber);
              END;
            ELSIF (sdStorageRegNo = SRRegNo) THEN
              changed := TRUE;
              SRValue := GetRegisterSRValue();
              IF (sdStorageBitNo IN SRValue) THEN
                EXCL(SRValue, sdStorageBitNo);
              ELSE
                INCL(SRValue, sdStorageBitNo);
              END;
              PutRegisterSRValue(SRValue);
            END;
            IF (changed) THEN
              UpdateWindows(WindowClassSet{WCRegisters});
              RefreshSymDataWindows();
            END;
          END;
        ELSE
          err := TRUE;
        END;
        IF (err) THEN
          ShowError(ADR(SelErrMsgBadValue));
        END;
      END;
    END;
  END;
END DoChangeValue;

(* DoMemoryCommand - perform a memory block operation *)
PROCEDURE DoMemoryCommand(cmd: MemCommand);
TYPE
  MemoryValue = RECORD
                  CASE : CARDINAL OF
                  |0: B0, B1, B2, B: BYTE;
                  |1: W0,         W: CARDINAL;
                  |2:             L: LONGCARD;
                  END;
                END;
VAR
  selData: SelectionData;
  validAdr: BOOLEAN;
  selected: BOOLEAN;
  absAdr: ADDRESS;
  oldSrcAdr: ADDRESS;
  newValue: LONGCARD;
  badSelErr: BOOLEAN;
  badEndErr: BOOLEAN;
  memValue: MemoryValue;
  searchValue: LONGCARD;
  memValueAdr: ADDRESS;
  repCount: LONGCARD;
  changed: BOOLEAN;
  b: BOOLEAN;
BEGIN
  validAdr := FALSE;
  selected := GetSelection(selData);
  IF (selected) THEN
    WITH selData DO
      IF (sdWindowClass = WCMemory) OR (sdWindowClass = WCRegisters) THEN
        absAdr    := sdAdr;
        validAdr  := absAdr # InvalidAbsAdr;
      END;
    END;
  END;

  IF (MemValueSize = 1) THEN
    memValueAdr := ADR(memValue.B);
  ELSIF (MemValueSize = 2) THEN
    memValueAdr := ADR(memValue.W);
  ELSIF (MemValueSize = 4) THEN
    memValueAdr := ADR(memValue.L);
  END;

  changed   := FALSE;
  badSelErr := FALSE;
  badEndErr := FALSE;

  CASE (cmd) OF
  | MCmdSrcAdr:  IF (selected) THEN
                   IF (validAdr) THEN
                     MemSrcAdr := absAdr;
                   ELSE
                     badSelErr := TRUE;
                   END;
                 ELSE
                   b := RequestAddress(MemSrcAdr, MemSrcAdr);
                 END;
  | MCmdDstAdr:  IF (selected) THEN
                   IF (validAdr) THEN
                     MemDstAdr := absAdr;
                   ELSE
                     badSelErr := TRUE;
                   END;
                 ELSE
                   b := RequestAddress(MemDstAdr, MemDstAdr);
                 END;
  | MCmdEndAdr:  IF (selected) THEN
                   IF (validAdr) THEN
                     MemEndAdr := absAdr;
                   ELSE
                     badSelErr := TRUE;
                   END;
                 ELSE
                   b := RequestAddress(MemEndAdr, MemEndAdr);
                 END;
  | MCmdSearch:  IF (MemValueSize # 1) THEN
                   IF (ODD(MemSrcAdr)) THEN INC(MemSrcAdr); END;
                   IF (ODD(MemEndAdr)) THEN INC(MemEndAdr); END;
                 END;
                 badEndErr := MemEndAdr <= MemSrcAdr;
                 IF (NOT badEndErr) THEN
                   IF (RequestSearchMemory(MemSrcAdr, MemEndAdr, MemValueSize,
                                           searchValue)) THEN
                     oldSrcAdr := MemSrcAdr;
                     LOOP
                       absAdr := SearchMemoryValue(MemSrcAdr, MemEndAdr,
                                                   MemValueSize, searchValue);
                       IF (absAdr # InvalidAbsAdr) THEN
                         MemSrcAdr := absAdr;
                         IF (NOT RequestContinueSearch(MemSrcAdr, TRUE, searchValue)) THEN
                           EXIT; (* User Canceled Search *)
                         END;
                         INC(MemSrcAdr, MemValueSize);
                       ELSE
                         IF (NOT RequestContinueSearch(MemSrcAdr, FALSE, searchValue)) THEN
                           EXIT; (* User Cancel Search *)
                         END;
                         MemSrcAdr := oldSrcAdr;
                       END;
                     END; (* LOOP *)
                     MemSrcAdr := oldSrcAdr;
                   END;
                 END;
  | MCmdFill:    badEndErr := MemEndAdr <= MemDstAdr;
                 IF (NOT badEndErr) THEN
                   IF (RequestFillMemory(MemDstAdr, MemEndAdr, MemValueSize,
                                         memValue.L)) THEN
                     repCount := (MemEndAdr-MemDstAdr) DIV LONGCARD(MemValueSize); 
                     IF (repCount # 0D) THEN
                       repmem(MemDstAdr, memValueAdr, MemValueSize, repCount);
                       changed := TRUE;
                     END;
                   END;
                 END;
  | MCmdCopy:    badEndErr := MemEndAdr <= MemSrcAdr;
                 IF (NOT badEndErr) THEN
                   IF (RequestCopyMemory(MemSrcAdr, MemDstAdr, MemEndAdr)) THEN
                     movmem(MemSrcAdr, MemDstAdr, MemEndAdr - MemSrcAdr);
                     changed := TRUE;
                   END;
                 END;
  END; (* CASE *)

  IF (changed) THEN
    UpdateWindows(WindowClassSet{WCMemory});
    RefreshSymDataWindows();
  END;

  IF (badSelErr) THEN
    ShowError(ADR(MemErrMsgBadAdr));
  ELSIF (badEndErr) THEN
    ShowError(ADR(MemErrMsgBadEnd));
  END;
END DoMemoryCommand;

(* ===== PUBLIC ===== *)

(* AddMenuItemWithKey - add menu item with a keyboard short-cut *)
PROCEDURE AddMenuItemWithKey(name: ADDRESS; key: CHAR);
BEGIN
  AddMenuItem(name);
  MenuItemOpt(DefaultItemFlags + MenuItemFlagsSet{CommSeq},
              MenuItemMutualExcludeSet{}, key);
END AddMenuItemWithKey;

(* AddMenuItemWithCheck - Add menu item toggle on/off check mark *)
PROCEDURE AddMenuItemWithCheck(name: ADDRESS; set: BOOLEAN);
VAR
  flags: MenuItemFlagsSet;
BEGIN
  AddMenuItem(name);
  flags := DefaultItemFlags + MenuItemFlagsSet{CheckIt, MenuToggle};
  IF (set) THEN
    INCL(flags, Checked);
  END;
  MenuItemOpt(flags, MenuItemMutualExcludeSet{}, 0C);
END AddMenuItemWithCheck;

(* AddMenuItemWithCheckList - Add menu item with a check mark and mutual excl *)
PROCEDURE AddMenuItemWithCheckList(name: ADDRESS; set: BOOLEAN;
                                   excl: MenuItemMutualExcludeSet);
VAR
  flags: MenuItemFlagsSet;
BEGIN
  AddMenuItem(name);
  flags := DefaultItemFlags + MenuItemFlagsSet{CheckIt};
  IF (set) THEN
    INCL(flags, Checked);
  END;
  MenuItemOpt(flags, excl, 0C);
END AddMenuItemWithCheckList;

(* AddMenuItemNoHighlight - add menu item with no highlighting *)
PROCEDURE AddMenuItemNoHighlight(name: ADDRESS);
BEGIN
  AddMenuItem(name);
  MenuItemOpt(MenuItemFlagsSet{ItemText, ItemEnabled},
              MenuItemMutualExcludeSet{}, 0C);
END AddMenuItemNoHighlight;

(* EnableMenu - enable/disable a specified menu *)
PROCEDURE EnableMenu(VAR ms: Menu; menuNum: CARDINAL; enabled: BOOLEAN);
VAR
  mp: MenuPtr;
BEGIN
  mp := ADR(ms);
  WHILE (menuNum # 0) DO
    mp := mp^.NextMenu;
    DEC(menuNum);
  END;
  IF (mp # NIL) THEN
    WITH mp^ DO
      IF (enabled) THEN
        INCL(Flags, MenuEnabled);
      ELSE
        EXCL(Flags, MenuEnabled);
      END;
    END;
  END;
END EnableMenu;

(* EnableMenuItem - enable/disable menu item *)
PROCEDURE EnableMenuItem(VAR ms: Menu; menuNum, itemNum: CARDINAL;
                         enabled: BOOLEAN);
VAR
  mip: MenuItemPtr;
BEGIN
  mip := ItemAddress(ms, CalcMenuItemCode(menuNum, itemNum));
  IF (mip # NIL) THEN
    WITH mip^ DO
      IF (enabled) THEN
        INCL(Flags, ItemEnabled);
      ELSE
        EXCL(Flags, ItemEnabled);
      END;
    END;
  END;
END EnableMenuItem;

(* GetBlobalMenuStrip - Get ptr to the first menu in global menu strip *)
PROCEDURE GetGlobalMenuStrip(): MenuPtr;
BEGIN
  RETURN (GlobalMenuStrip);
END GetGlobalMenuStrip;

(* LinkGlobalMenuStrip - link global menu strip to local menus and adjust pos *)
PROCEDURE LinkGlobalMenuStrip(VAR menu: Menu);
VAR
  ms: MenuPtr;
  left: CARDINAL;
  lastMenu: MenuPtr;
BEGIN
  left := LocalMenuLeftEdge;
  ms   := ADR(menu);
  WHILE (ms # NIL) DO
    lastMenu := ms;
    WITH ms^ DO
      LeftEdge := left;
      INC(left, Width);
    END;
    ms := ms^.NextMenu;
  END;

  lastMenu^.NextMenu := GlobalMenuStrip;
END LinkGlobalMenuStrip;

(* DoGlobalMenuCommand - Handle a global menu selection or ignore if local *)
PROCEDURE DoGlobalMenuCommand(menuNum, itemNum, subNum, localMenus: CARDINAL;
                              winClass: WindowClass; winIndex: CARDINAL;
                              vtWin:VTWindow): BOOLEAN;
VAR
  waitMode: BOOLEAN;
  scr: ADDRESS;
  newDirStr: ADDRESS;
  dirList: DirNodePtr;
  curDir: ARRAY [0..RequestMaxStringLength] OF CHAR;
  newDir: ARRAY [0..RequestMaxStringLength] OF CHAR;
BEGIN
  IF (menuNum < localMenus) THEN
    RETURN (FALSE); (* Local Menu! *)
  END;

  waitMode := UserCommandMode = CMWait;

  DEC(menuNum, localMenus);

  CASE (menuNum) OF
  | 0: (* Project *)
       CASE (itemNum) OF
       | 0: (* Load Program *)
            DoLoadProg();
       (*1: -------------------- *)
       | 2: (* Load Configuration *)
            DoConfigFileCommand(CCmdLoad);
       | 3: (* Save Configuration *)
            DoConfigFileCommand(CCmdSave);
       (*4: -------------------- *)
       | 5: (* Quit *)
            IF (UserCommandMode = CMNoProg) THEN
              Abort();
            END;
       END; (* CASE *)

  | 1: (* Command *)
       CASE (itemNum) OF
       | 0: (* Go *)
            ContinueUserProgram(EMGo, ConfigProgLanguageMode, InvalidAbsAdr);
       | 1: (* Go/Set Temporary BP *)
            DoGoAndSetTempBP();
       | 2: (* Halt *)
            ExitUserProgram();
       (*3: -------------------- *)
       | 4: (* Step-In *)
            ContinueUserProgram(EMStepIn, ConfigProgLanguageMode, NIL);
       | 5: (* Step-Over *)
            ContinueUserProgram(EMStepOver, ConfigProgLanguageMode, NIL);
       (*6: -------------------- *)
       | 7: (* Run-In *)
            ContinueUserProgram(EMRunIn, ConfigProgLanguageMode, NIL);
       | 8: (* Run-Over *)
            ContinueUserProgram(EMRunOver, ConfigProgLanguageMode, NIL);
       | 9: (* Stop *)
            StopUserProgram();
       (*10: -------------------- *)
       |11: (* Set Source Address *)
            DoMemoryCommand(MCmdSrcAdr);
       |12: (* Set Destination Address *)
            DoMemoryCommand(MCmdDstAdr);
       |13: (* Set Ending Address *)
            DoMemoryCommand(MCmdEndAdr);
       |14: (* Set Search/Fill Size *)
            CASE (subNum) OF
            | 0: (* Byte *)
                 MemValueSize := 1;
            | 1: (* Word *)
                 MemValueSize := 2;
            | 2: (* Long *)
                 MemValueSize := 4;
            END;

       |15: (* Search Memory *)
            DoMemoryCommand(MCmdSearch);
       |16: (* Fill Memory *)
            DoMemoryCommand(MCmdFill);
       |17: (* Copy Memory *)
            DoMemoryCommand(MCmdCopy);
       END; (* CASE *)

  | 2: (* BreakPoints *)
       CASE (itemNum) OF
       | 0: (* Set *)
            DoBreakPointCommand(BPCmdSet);
       | 1: (* Clear *)
            DoBreakPointCommand(BPCmdClear);
       | 2: (* Clear All *)
            IF (waitMode) THEN
              DeleteBreakPoints();
            END;
       (*3: -------------------- *)
       | 4: (* Enable *)
            DoBreakPointCommand(BPCmdEnable);
       | 5: (* Enable All *)
            IF (waitMode) THEN
              EnableBreakPoints();
            END;
       | 6: (* Disable *)
            DoBreakPointCommand(BPCmdDisable);
       | 7: (* Disable All *)
            IF (waitMode) THEN
              DisableBreakPoints();
            END;
       (*8: -------------------- *)
       | 9: (* Set Break Count *)
            DoBreakPointCommand(BPCmdCount);
       END; (* CASE *)

  | 3: (* Window *)
       CASE (itemNum) OF
       | 0: (* Source *)
            OpenNewWindow(WCSource);
       | 1: (* Symbolic Data *)
            OpenNewWindow(WCSymData);
       | 2: (* Memory *)
            OpenNewWindow(WCMemory);
       | 3: (* Disassembly *)
            OpenNewWindow(WCDisassembly);
       | 4: (* Registers *)
            OpenNewWindow(WCRegisters);
       | 5: (* Module List *)
            OpenNewWindow(WCModuleList);
       | 6: (* Procedure Call Chain *)
            OpenNewWindow(WCProcChain);
       | 7: (* BreakPoint List *)
            OpenNewWindow(WCBreakPointList);
       | 8: (* Info *)
            OpenNewWindow(WCInfo);
       | 9: (* Directory *)
            OpenNewWindow(WCDirectory);
       (*10: -------------------- *)
       |11: (* Duplicate Window *)
            DoWindowCommand(WCmdDuplicate, winClass, winIndex, vtWin);
       |12: (* Set Window Defaults *)
            DoWindowCommand(WCmdSetDefaults, winClass, winIndex, vtWin);
       |13: (* Update All Windows *)
            UpdateWindows(UpdateAllWindows);
       |14: (* Clip Window *)
            DoWindowCommand(WCmdClip, winClass, winIndex, vtWin);
       END; (* CASE *)

  | 4: (* Options *)
       CASE (itemNum) OF
       | 0: (* Modula-2 Language Mode *)
            ConfigProgLanguageMode := LMM2;
       | 1: (* Assembly Language Mode *)
            ConfigProgLanguageMode := LMAsm;
       (*2: -------------------- *)
       | 3: (* Set Process Priority *)
            RequestProcessPriority(ConfigProgPriority);
       | 4: (* Set Process Stack Size *)
            RequestProcessStackSize(ConfigProgStackSize);
       | 5: (* Set Process Current Dir *)
            IF (RequestProcessCurrentDir(ConfigProgCurrentDir, ADR(newDir[0]))) THEN
              IF (newDir[0] = 0C) THEN
                SetNewProgCurrentDir(NIL);
              ELSE
                SetNewProgCurrentDir(ADR(newDir[0]));
              END;
            END;
       (*6: -------------------- *)
       | 7: (* Stop on First Instruction *)
            ConfigProgStartupMode := SMFirstInst;
       | 8: (* Stop on Library Module *)
            ConfigProgStartupMode := SMLibModule;
       | 9: (* Stop on Main Module *)
            ConfigProgStartupMode := SMMainModule;
       (*10: -------------------- *)
       |11: (* WorkBench Screen *)
            ConfigPrivateScreen := FALSE;
       |12: (* Custom Screen *)
            ConfigPrivateScreen := TRUE;
       |13: (* Interlaced Custom Screen *)
            ConfigInterlaceScreen := NOT ConfigInterlaceScreen;
       |14: (* Set Custom Screen Colors *)
            scr := GetScreen();
            IF (scr # NIL) THEN
              RequestScreenColors(scr, ConfigScreenColors);
            END;
       (*15: -------------------- *)
       |16: (* Case Sensitive Search *)
            ConfigStringSearchCase := NOT ConfigStringSearchCase;
       |17: (* Verify .MOD File Dates *)
            ConfigCheckModFileDate := NOT ConfigCheckModFileDate;
       |18: (* Set Maximum # .MOD Files *)
            RequestMaxModFilesLoaded(ConfigMaxModFilesLoaded);
       |19: (* Set Symbol Directory List *)
            ConvDirListToString(ConfigSymbolFileDirList, ADR(curDir[0]));
            IF (RequestSymbolFileDirList(ADR(curDir[0]), ADR(newDir[0]))) THEN
              dirList := ConvStringToDirList(ADR(newDir[0]));
              SetNewDirList(ConfigSymbolFileDirList, dirList);
            END;
       |20: (* Set Source Directory List *)
            ConvDirListToString(ConfigSourceFileDirList, ADR(curDir[0]));
            IF (RequestSourceFileDirList(ADR(curDir[0]), ADR(newDir[0]))) THEN
              dirList := ConvStringToDirList(ADR(newDir[0]));
              SetNewDirList(ConfigSourceFileDirList, dirList);
            END;
       END; (* CASE *)

  END; (* CASE *)

  RETURN (TRUE); (* Global Menu! *)
END DoGlobalMenuCommand;

(* DoGlobalKbdCommand - Handle global keyboard command. Ignore unknown cmds *)
PROCEDURE DoGlobalKbdCommand(bufPtr: ADDRESS; bufLength: CARDINAL;
                             winClass: WindowClass;
                             winIndex: CARDINAL): BOOLEAN;
VAR
  key: CARDINAL;
  global: BOOLEAN;
BEGIN
  key := CalcKeyCode(bufPtr, bufLength);
  CASE (key) OF
  | F1Key:   ContinueUserProgram(EMStepIn, ConfigProgLanguageMode, NIL);
  | F2Key:   ContinueUserProgram(EMStepOver, ConfigProgLanguageMode, NIL);
  | F3Key:   ContinueUserProgram(EMRunIn, ConfigProgLanguageMode, NIL);
  | F4Key:   ContinueUserProgram(EMRunOver, ConfigProgLanguageMode, NIL);
  | F5Key:   StopUserProgram();
  | F6Key:   ContinueUserProgram(EMGo, ConfigProgLanguageMode, InvalidAbsAdr);
  | F7Key:   DoGoAndSetTempBP();
  | F8Key:
  | F9Key:   DoChangeValue(FALSE);
  | F10Key:  ConfigProgLanguageMode := LMM2;
             SetupOptionsMenu();
  | F1SKey:  ContinueUserProgram(EMStepIn, LMAsm, NIL);
  | F2SKey:  ContinueUserProgram(EMStepOver, LMAsm, NIL);
  | F3SKey:  ContinueUserProgram(EMRunIn, LMAsm, NIL);
  | F4SKey:  ContinueUserProgram(EMRunOver, LMAsm, NIL);
  | F5SKey:
  | F6SKey:
  | F7SKey:
  | F8SKey:
  | F9SKey:  DoChangeValue(TRUE);
  | F10SKey: ConfigProgLanguageMode := LMAsm;
             SetupOptionsMenu();
  END;

  global := (key >= F1Key) AND (key <= F10SKey);

  IF (key = DELKey) THEN (* Clear current selection command *)
    global := TRUE;
    ClearSelection();
  END;

  RETURN (global);
END DoGlobalKbdCommand;

(* SetupOptionsMenu - setup options menu according to current configuration *)
PROCEDURE SetupOptionsMenu();
BEGIN
  CheckOptionsMenuItem(M2Item, ConfigProgLanguageMode = LMM2);
  CheckOptionsMenuItem(AsmItem, ConfigProgLanguageMode = LMAsm);
  CheckOptionsMenuItem(FirstInstItem, ConfigProgStartupMode = SMFirstInst);
  CheckOptionsMenuItem(LibModItem, ConfigProgStartupMode = SMLibModule);
  CheckOptionsMenuItem(MainModItem, ConfigProgStartupMode = SMMainModule);
  CheckOptionsMenuItem(WorkBenchItem, NOT ConfigPrivateScreen);
  CheckOptionsMenuItem(CustomItem, ConfigPrivateScreen);
  CheckOptionsMenuItem(InterlacedItem, ConfigInterlaceScreen);
  CheckOptionsMenuItem(CaseSearchItem, ConfigStringSearchCase);
  CheckOptionsMenuItem(VerifyModItem, ConfigCheckModFileDate);
  EnableMenuItem(GlobalMenuStrip^, OptionsMenu, SetColorsItem, GetScreen() # NIL)
END SetupOptionsMenu;

(* SetCommandMode - set debugger menus and keyboard command mode *)
PROCEDURE SetCommandMode(mode: CommandMode);
CONST
  T = TRUE;
  F = FALSE;
BEGIN
  UserCommandMode := mode;

  Forbid();
  CASE (UserCommandMode) OF
  | CMNoProg: EnableProjectMenuConfig(TRUE);
              EnableProjectMenuProg(TRUE);
              EnableCommandMenuExec(FALSE);
              EnableCommandMenuMemory(FALSE);
              EnableMenuItem(GlobalMenuStrip^, CommandMenu, StopItem, F);
              EnableMenu(GlobalMenuStrip^, BreakPointsMenu, F);
              EnableMenu(GlobalMenuStrip^, OpenWindowMenu, T);
              EnableMenu(GlobalMenuStrip^, OptionsMenu, T);
  | CMWait:   EnableProjectMenuConfig(TRUE);
              EnableProjectMenuProg(FALSE);
              EnableCommandMenuExec(TRUE);
              EnableCommandMenuMemory(TRUE);
              EnableMenuItem(GlobalMenuStrip^, CommandMenu, StopItem, F);
              EnableMenu(GlobalMenuStrip^, BreakPointsMenu, T);
              EnableMenu(GlobalMenuStrip^, OpenWindowMenu, T);
              EnableMenu(GlobalMenuStrip^, OptionsMenu, T);
  | CMRun:    EnableProjectMenuConfig(FALSE);
              EnableProjectMenuProg(FALSE);
              EnableCommandMenuExec(FALSE);
              EnableCommandMenuMemory(TRUE);
              EnableMenuItem(GlobalMenuStrip^, CommandMenu, StopItem, T);
              EnableMenu(GlobalMenuStrip^, BreakPointsMenu, F);
              EnableMenu(GlobalMenuStrip^, OpenWindowMenu, T);
              EnableMenu(GlobalMenuStrip^, OptionsMenu, T);
  | CMExec:   EnableProjectMenuConfig(FALSE);
              EnableProjectMenuProg(FALSE);
              EnableCommandMenuExec(FALSE);
              EnableCommandMenuMemory(TRUE);
              EnableMenuItem(GlobalMenuStrip^, CommandMenu, StopItem, F);
              EnableMenu(GlobalMenuStrip^, BreakPointsMenu, F);
              EnableMenu(GlobalMenuStrip^, OpenWindowMenu, T);
              EnableMenu(GlobalMenuStrip^, OptionsMenu, T);
  END;
  Permit();
END SetCommandMode;

(* InitDBMenusKbd - initialize DBMenusKbd module *)
PROCEDURE InitDBMenusKbd(): BOOLEAN;
TYPE
  M = MenuItemMutualExcludeSet;

CONST
  T = TRUE;
  F = FALSE;

VAR
  ms: MenuPtr;
BEGIN
  MenuItemHeight := 8;
  BeginMenuStrip();
    AddMenu(                           ADR("Project "));
      AddMenuItemWithKey(                ADR(" Load Program"), "P");
      AddMenuItemNoHighlight(            ADR(" ----------------------- "));
      AddMenuItemWithKey(                ADR(" Load Configuration"), "L");
      AddMenuItemWithKey(                ADR(" Save Configuration"), "S");
      AddMenuItemNoHighlight(            ADR(" -----------------------"));
      AddMenuItemWithKey(                ADR(" Quit"), "Q");
    AddMenu(                           ADR("Command "));
      AddMenuItem(                       ADR(" Go                  [F6] "));
      AddMenuItem(                       ADR(" Go/Set Temporary BP [F7]"));
      AddMenuItem(                       ADR(" Halt"));
      AddMenuItemNoHighlight(            ADR(" ------------------------"));
      AddMenuItem(                       ADR(" Step-In             [F1]"));
      AddMenuItem(                       ADR(" Step-Over           [F2]"));
      AddMenuItemNoHighlight(            ADR(" ------------------------"));
      AddMenuItem(                       ADR(" Run-In              [F3]"));
      AddMenuItem(                       ADR(" Run-Over            [F4]"));
      AddMenuItem(                       ADR(" Stop                [F5]"));
      AddMenuItemNoHighlight(            ADR(" ------------------------"));
      AddMenuItem(                       ADR(" Set Source Address"));
      AddMenuItem(                       ADR(" Set Destination Address"));
      AddMenuItem(                       ADR(" Set Ending Address"));
      AddMenuItem(                       ADR(" Set Search/Fill Size"));
        BeginSubMenu(25*8 ,0);
          AddMenuItemWithCheckList(ADR(" Byte "), T, M{1,2});
          AddMenuItemWithCheckList(ADR(" Word"),  F, M{0,2});
          AddMenuItemWithCheckList(ADR(" Long"),  F, M{0,1});
        EndSubMenu();
      AddMenuItem(                       ADR(" Search Memory"));
      AddMenuItem(                       ADR(" Fill Memory"));
      AddMenuItem(                       ADR(" Copy Memory"));
    AddMenu(                           ADR("BreakPoints "));
      AddMenuItemWithKey(                ADR(" Set"), "I");
      AddMenuItemWithKey(                ADR(" Clear"), "C");
      AddMenuItemWithKey(                ADR(" Clear All"), "A");
      AddMenuItemNoHighlight(            ADR(" -------------------- "));
      AddMenuItemWithKey(                ADR(" Enable"), "E");
      AddMenuItem(                       ADR(" Enable All"));
      AddMenuItemWithKey(                ADR(" Disable"), "D");
      AddMenuItem(                       ADR(" Disable All"));
      AddMenuItemNoHighlight(            ADR(" --------------------"));
      AddMenuItemWithKey(                ADR(" Set Break Count"), "B");
    AddMenu(                           ADR("Window "));
      AddMenuItemWithKey(                ADR(" Source"), "1");
      AddMenuItemWithKey(                ADR(" Symbolic Data"), "2");
      AddMenuItemWithKey(                ADR(" Memory"), "3");
      AddMenuItemWithKey(                ADR(" Disassembly"), "4");
      AddMenuItemWithKey(                ADR(" Registers"), "5");
      AddMenuItemWithKey(                ADR(" Module List"), "6");
      AddMenuItemWithKey(                ADR(" Procedure Call Chain"), "7");
      AddMenuItemWithKey(                ADR(" BreakPoint List"), "8");
      AddMenuItemWithKey(                ADR(" Info"), "9");
      AddMenuItemWithKey(                ADR(" Directory"), "0");
      AddMenuItemNoHighlight(            ADR(" ------------------------- "));
      AddMenuItemWithKey(                ADR(" Duplicate Window"), "K");
      AddMenuItemWithKey(                ADR(" Set Window Defaults"), "W");
      AddMenuItemWithKey(                ADR(" Update All Windows"), "U");
      AddMenuItem(                       ADR(" Clip Window"));
    AddMenu(                           ADR("Options "));
      AddMenuItemWithCheckList(          ADR(" Modula-2 Language Mode  [F10] "), F, M{1});
      AddMenuItemWithCheckList(          ADR(" Assembly Language Mode [^F10]"), F, M{0});
      AddMenuItemNoHighlight(            ADR(" -----------------------------"));
      AddMenuItem(                       ADR(" Set Process Priority"));
      AddMenuItem(                       ADR(" Set Process Stack Size"));
      AddMenuItem(                       ADR(" Set Process Current Dir"));
      AddMenuItemNoHighlight(            ADR(" -----------------------------"));
      AddMenuItemWithCheckList(          ADR(" Stop on First Instruction "), F, M{8,9});
      AddMenuItemWithCheckList(          ADR(" Stop on Library Module"), F, M{7,9});
      AddMenuItemWithCheckList(          ADR(" Stop on Main Module"), F, M{7,8});
      AddMenuItemNoHighlight(            ADR(" -----------------------------"));
      AddMenuItemWithCheckList(          ADR(" WorkBench Screen"), F, M{12});
      AddMenuItemWithCheckList(          ADR(" Custom Screen"), F, M{11});
      AddMenuItemWithCheck(              ADR(" Interlaced Custom Screen"), F);
      AddMenuItem(                       ADR(" Set Custom Screen Colors"));
      AddMenuItemNoHighlight(            ADR(" -----------------------------"));
      AddMenuItemWithCheck(              ADR(" Case Sensitive Search"), F);
      AddMenuItemWithCheck(              ADR(" Verify .MOD File Dates"), F);
      AddMenuItem(                       ADR(" Set Maximum # .MOD Files"));
      AddMenuItem(                       ADR(" Set Symbol Directory List"));
      AddMenuItem(                       ADR(" Set Source Directory List"));
  GlobalMenuStrip := EndMenuStrip();

  IF NOT OSVersion2 THEN
    EnableMenuItem(GlobalMenuStrip^, OpenWindowMenu, ClipWindowItem, FALSE);
  END;

  (* Find left edge where local menus will be defined *)
  ms := GlobalMenuStrip;
  WHILE (ms # NIL) DO
    WITH ms^ DO
      LocalMenuLeftEdge := LeftEdge + Width;
    END;
    ms := ms^.NextMenu;
  END;

  MemValueSize := 1;

  RETURN (GlobalMenuStrip # NIL);
END InitDBMenusKbd;

(* CleanupDBMenusKbd - cleanup DBMenusKbd module *)
PROCEDURE CleanupDBMenusKbd();
BEGIN
  IF (GlobalMenuStrip # NIL) THEN
    FreeMenuStrip(GlobalMenuStrip^);
  END;
END CleanupDBMenusKbd;

END DBMenusKbd.
