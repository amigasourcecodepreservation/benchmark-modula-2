(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                      (c) 1986, 1987 by Jim Olinger                       *
 ****************************************************************************
 * Name: XTextUtil.MOD                     Version: Amiga.00.00             *
 * Created: 10/14/87   Updated: 03/21/94   Author: Leon Frenkel             *
 * Description: Modula-2 interface to high speed text output routines,      *
 * XText by R.J. Mical. Support procedures for XText().                     *
 ****************************************************************************)

IMPLEMENTATION MODULE XTextUtil;

(***************************************************************************
 * Changes for new release of M2Debug 3/21/94
 *
 * Tom Breeden
 * Aglet Software
 * Box 3314
 * Charlottesville, VA 22903
 * tmb@virginia.edu
 *
 *               ----- Beta.2 -----
 * 
 * BUGFIX: XText Produces Distorted Chars if CPU Cache Enabled on M68040
 * 
 *    1. Added a WaitBlit() after call to Text() in MakeXTextFontData. The
 *       68040 was outrunning the blitter.
 * 
 ***************************************************************************
*)

FROM SYSTEM IMPORT
	ADDRESS, BYTE,
	ADR, SHIFT, TSIZE;
FROM Blit IMPORT WaitBlit;
FROM DiskFont IMPORT
	DiskfontName, DiskfontBase,
	OpenDiskFont;
FROM Drawing IMPORT
	SetAPen, SetBPen, SetDrMd, Move;
FROM Graphics IMPORT
	BitMap,
	InitBitMap;
FROM Intuition IMPORT
	RememberPtr,
	AllocRemember, FreeRemember;
FROM Libraries IMPORT
	LibraryPtr,
	OpenLibrary, CloseLibrary;
FROM Memory IMPORT
	MemReqSet, MemClear, MemChip,
	AllocMem, FreeMem;
FROM Rasters IMPORT
	Jam2,
	TmpRas,
	RastPort,
	InitRastPort, InitTmpRas;
FROM Text IMPORT
	NormalFontStyle,
	FontFlagsSet,
	FontStyleSet, FontStyle,
	TextFont, TextFontPtr,
	TextAttr, TextAttrPtr,
	SetFont, AskSoftStyle, SetSoftStyle, Text, CloseFont, OpenFont;
FROM XText IMPORT
	NormalFont,
	ItalicLeftEdge, XTextCharWidth,
	XTextFlagsSet, XTextFlags,
	XTextSupport, XTextSupportPtr;

(*$L+*)

(* ====== PRIVATE ====== *)

(* This routine allocates an XText text plane, with one byte for each
 * line of each character in the widest allowable line.
 *)
PROCEDURE AllocXTextPlane(VAR xtext: XTextSupport): ADDRESS;
BEGIN
  RETURN (AllocRemember(xtext.XTextKey, 
                        CARDINAL(xtext.MaxTextWidth)*CARDINAL(xtext.CharHeight),
                        MemReqSet{MemClear, MemChip}));
END AllocXTextPlane;

(* Attach the contents of a Remember key to the allocations of another
 * Remember key.
 *)
PROCEDURE AttachRemember(VAR tokey: RememberPtr; VAR fromkey: RememberPtr);
VAR
  workkey: RememberPtr;
BEGIN
  workkey := tokey;
  IF (workkey = NIL) THEN
    tokey := fromkey;
  ELSE
    WHILE (workkey^.NextRemember # NIL) DO
      workkey := workkey^.NextRemember;
    END;
    workkey^.NextRemember := fromkey;
  END;
  fromkey := NIL;
END AttachRemember;

(* This awful little routine fills the buffer with the XText-style
 * font imagery.  The font is set to reflect the desired style, if any,
 * and then one character at a time is drawn (using Text()) into
 * a temporary rastport and then copies the character imagery into
 * the XText buffer.
 *)
PROCEDURE MakeXTextFontData(VAR font: TextFont; style: FontStyleSet;
                            bufptr: ADDRESS; VAR xtext: XTextSupport);
VAR
  i, i2: INTEGER;
  baseline, height, xoffset, bytewidth: INTEGER;
  enable, res: FontStyleSet;
  extracolumn, movecolumn: INTEGER;
  rport: RastPort;
  bmap: BitMap;
  tmpras: TmpRas;
  ptr: ADDRESS;
  text: BYTE;
BEGIN
  bytewidth := INTEGER(xtext.MaxTextWidth);
  height := xtext.CharHeight;

  InitBitMap(bmap, 1, bytewidth * XTextCharWidth, height);
  bmap.Planes[0] := xtext.NormalTextPlane;
  InitRastPort(rport);
  rport.BitMap := ADR(bmap);
  rport.TmpRas := ADR(tmpras);
  InitTmpRas(tmpras, xtext.InverseTextPlane, bytewidth*height);

  SetAPen(rport, 1);
  SetBPen(rport, 0);
  SetDrMd(rport, Jam2);
  
  SetFont(rport, font);
  enable := AskSoftStyle(rport);
  res := SetSoftStyle(rport, style, enable);

  baseline := rport.TxBaseline;

   (* Now, italics are a real pain in the ascii.
    * An 8-bit-wide font is normally wider than 8 bits when rendered 
    * in italics, so some of the character data must be lost.  
    * You can set the xoffset variable to some alternate value 
    * if you want an alternate slice of the font data.
    * 
    * Furthermore, I just spent several hours discovering that 
    * the Text() routine creates the italics by shifting the lines above
    * the baseline to the right *and* by shifting the lines below the
    * baseline to the left.  So what, you ask?  Well, if a character 
    * is printed starting at column 0, in a RastPort 
    * unprotected by a Layer, then those left-shifted bits 
    * tickle the nose of the guru, doncha know.  Achoo!  Blink blink blink.
    *)

  xoffset := 0;
  (* See the comment above regarding xoffset *)
  IF (Italic IN style) THEN
    xoffset := ItalicLeftEdge;
  END;

  (* Text is drawn at least at column 8, and perhaps even further 
   * to the right if the left-shift of italics makes it necessary.
   *)
  extracolumn := SHIFT(((height - baseline) - 1), -3);
  movecolumn  := (8 + SHIFT(extracolumn, 3)) - xoffset;

  FOR i := 0 TO 255 DO
    text := BYTE(i);

    (* Finally, move the pens to our spot and draw the next character *)
    Move(rport, movecolumn, baseline);
    Text(rport, ADR(text), 1);

    WaitBlit();

    (* Now copy that character in XText font format into the buffer. *)
    ptr := bmap.Planes[0] + 1D + LONGCARD(extracolumn);
    i2 := 0;
    WHILE (i2 < height) DO
      bufptr^ := ptr^; INC(bufptr);
      IF (NOT (SlimXText IN xtext.Flags)) THEN
        bufptr^ := BYTE(0);  INC(bufptr);
      END;
      INC(ptr, bytewidth);  (* Skip to the next row *)

      INC(i2);
    END;
  END; (* FOR *)

END MakeXTextFontData;

(* ====== PUBLIC ====== *)

PROCEDURE MakeXTextFont(textattr: TextAttrPtr; VAR xtext: XTextSupport;
                        index: INTEGER): ADDRESS;
VAR
  localfont: TextFontPtr;
  openedlib, success: BOOLEAN;
  data: ADDRESS;
  localkey: RememberPtr;
  size: INTEGER;
  myDiskfontBase: LibraryPtr;
BEGIN
  openedlib := FALSE;
  success := FALSE;
  localkey := NIL;
  localfont := NIL;

  (* If the user has specified no font, use 80-column "topaz.font" *)
  IF (textattr = NIL) THEN
    textattr := ADR(DefaultXTextFont);
  END;

  (* Allocate a special-character buffer where there are 256 characters
   * and each character is CharHeight tall.  If not SLIM_XTEXT, make the 
   * text two bytes wide for each character.
   *)
  size := 256 * xtext.CharHeight;
  IF (NOT (SlimXText IN xtext.Flags)) THEN
    size := size * 2;
  END;

  LOOP
    data := AllocRemember(localkey, size, MemReqSet{MemChip});
    IF (data = NIL) THEN
      EXIT; (* error *)
    END;

    (* Attempt to open the specified font *)
    localfont := OpenFont(textattr^);
    IF (localfont = NIL) THEN
      IF (DiskfontBase = NIL) THEN
        myDiskfontBase := OpenLibrary(ADR(DiskfontName), 0D);
        IF (myDiskfontBase = NIL) THEN
          EXIT; (* error *)
        END;
        DiskfontBase := myDiskfontBase;
        openedlib := TRUE;
      END;

      localfont := OpenDiskFont(textattr^);
      IF (localfont = NIL) THEN
        EXIT; (* error *)
      END;
    END;

    (* localfont is opened.  Transform the data into XText format *)
    MakeXTextFontData(localfont^, textattr^.taStyle, data, xtext);

    CloseFont(localfont^);
    success := TRUE;

    EXIT;
  END; (* LOOP *)

  IF (openedlib) THEN 
    CloseLibrary(DiskfontBase^); 
    DiskfontBase := NIL; (* Restore to original state *)
  END;

  IF (success) THEN
    AttachRemember(xtext.XTextKey, localkey);
    xtext.FontData[index] := data;
  ELSE
    FreeRemember(localkey, TRUE);
    data := NIL;
  END;

  RETURN (data);
END MakeXTextFont;


PROCEDURE MakeXTextSupport(VAR rport: RastPort; textattr: TextAttrPtr;
                           maxwidth: INTEGER; 
                           flags: XTextFlagsSet): XTextSupportPtr;
VAR
  xtext: XTextSupportPtr;
  ptr: ADDRESS;
  i: INTEGER;
BEGIN
  maxwidth := INTEGER( BITSET(maxwidth + 1) * {1..15} );

  xtext := AllocMem(TSIZE(XTextSupport), MemReqSet{MemClear});
  IF (xtext = NIL) THEN
    RETURN (NIL);
  END;

  (* If the user has specified no font, use 80-column "topaz.font" *)
  IF (textattr = NIL) THEN
    textattr := ADR(DefaultXTextFont);
  END;

  WITH xtext^ DO
    FrontPen         := BYTE(1);
    DrawMode         := Jam2;
    MaxTextWidth     := BYTE(maxwidth);
    Flags            := flags;
    CharHeight       := textattr^.taYSize;
    NormalTextPlane  := AllocXTextPlane(xtext^);
    InverseTextPlane := AllocXTextPlane(xtext^);
    AllClearPlane    := AllocXTextPlane(xtext^);
    AllSetPlane      := AllocXTextPlane(xtext^);
  END;

  IF (xtext^.NormalTextPlane = NIL) OR
     (xtext^.InverseTextPlane = NIL) OR
     (xtext^.AllClearPlane = NIL) OR
     (xtext^.AllSetPlane = NIL) THEN
    UnmakeXTextSupport(xtext^);
    RETURN (NIL);
  END;

  i := 0;
  ptr := xtext^.AllSetPlane;
  WHILE (i < (xtext^.CharHeight * maxwidth)) DO
    ptr^ := BYTE(-1);

    INC(ptr);
    INC(i);
  END;

  xtext^.OutputRPort := ADR(rport);

  InitBitMap(xtext^.TextBitMap, INTEGER(rport.BitMap^.Depth),
             maxwidth * XTextCharWidth, xtext^.CharHeight);

  IF (MakeXTextFont(textattr, xtext^, NormalFont) = NIL) THEN
    UnmakeXTextSupport(xtext^);
    RETURN (NIL);
  END;

  FOR i := 1 TO 7 DO
    xtext^.FontData[i] := xtext^.FontData[0];
  END;

  RETURN (xtext);
END MakeXTextSupport;

PROCEDURE UnmakeXTextSupport(VAR xtext: XTextSupport);
BEGIN
  FreeRemember(xtext.XTextKey, TRUE);
  FreeMem(ADR(xtext), TSIZE(XTextSupport));
END UnmakeXTextSupport;

BEGIN
  WITH DefaultXTextFont DO
    taName  := ADR("topaz.font");
    taYSize := 8;			(* Preferences.TopazEighty *)
    taStyle := NormalFontStyle;
    taFlags := FontFlagsSet{};
  END;
END XTextUtil.
