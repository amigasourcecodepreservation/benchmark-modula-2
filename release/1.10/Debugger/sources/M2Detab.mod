(****************************************************************************
 *              Benchmark Modula-2 Software Construction Set                *
 *                   Copyright (c) 1989 by Jim Olinger                      *
 ****************************************************************************
 * Name: M2Detab.MOD                       Version: Amiga.01.00             *
 * Created: 03/24/89   Updated: 03/24/89   Author: Leon Frenkel             *
 * Description: Tab to space converter for files created using M2Ed.        *
 ****************************************************************************)

MODULE M2Detab;

FROM SYSTEM IMPORT
  ADR, ADDRESS;
FROM CFileIO IMPORT
  EOF;
FROM CStdIO IMPORT
  FILE,
  openstdio, closestdio, fopen, fclose, puts, getc, putc, ferror, feof;
FROM System IMPORT
  argc, argv;
FROM Terminal IMPORT
  WriteString;

CONST
  Title = "M2SCS: Detab  Amiga.01.00 Copyright \xA9 1989 by Leon Frenkel\n";

  TabSize = 8;

  MsgDone          = "Done.\n";
  ErrMsgUsage      = "usage: M2Detab <InputFile> <OutputFile>";
  ErrMsgOpenIn     = "Opening input file.";
  ErrMsgOpenOut    = "Opening output file.";
  ErrMsgReadingIn  = "Reading input file.";
  ErrMsgWritingOut = "Reading output file.";

  TAB = "\t";
  LF  = "\n";


(* print - print string to output window *)
PROCEDURE print(str: ADDRESS);
VAR
  s: POINTER TO ARRAY [0..255] OF CHAR;
BEGIN
  s := str;
  WriteString(s^);
END print;

(* error - print error message to output *)
PROCEDURE error(str: ADDRESS);
BEGIN
  print(ADR("ERROR: "));
  print(str);
  print(ADR("\n"));
END error;

(* DetabFile - convert file from TAB -> SPACE *)
PROCEDURE DetabFile(inFileName, outFileName: ADDRESS);
VAR
  inFile: FILE;
  outFile: FILE;
  b: BOOLEAN;
  chPos: CARDINAL;
  ch: CHAR;
  i: INTEGER;
  expand: CARDINAL;
BEGIN
  inFile  := NIL;
  outFile := NIL;

  LOOP
    (* Open input file *)
    inFile := fopen(inFileName, ADR("r"));
    IF (inFile = NIL) THEN
      error(ADR(ErrMsgOpenIn));
      EXIT; (* Abort! *)
    END;

    (* Open output file *)
    outFile := fopen(outFileName, ADR("w"));
    IF (outFile = NIL) THEN
      error(ADR(ErrMsgOpenOut));
      EXIT; (* Abort! *)
    END;

    chPos := 0; (* Reset to beginning of line *)
    LOOP
      i  := getc(inFile);
      ch := CHAR(i);
      IF (i = EOF) THEN (* Error reading input or end of file *)
        IF (ferror(inFile)) THEN (* read error *)
          error(ADR(ErrMsgReadingIn));
        END;
        EXIT; (* Done! *)
      END;

      IF (ch = TAB) THEN (* Expand TAB *)
        expand := TabSize - (chPos MOD TabSize);
        WHILE (expand # 0) DO (* Output spaces *)
          IF (putc(" ", outFile) = EOF) THEN
            EXIT; (* Error writing to file *)
          END;
          INC(chPos);
          DEC(expand);
        END
      ELSE
        IF (ch = LF) THEN (* Reset for new line *)
          chPos := 0;
        ELSE (* Any other char *)
          INC(chPos);
        END;

        IF (putc(ch, outFile) = EOF) THEN
          EXIT; (* Error writing to file *)
        END;
      END;
    END; (* LOOP *)

    EXIT; (* Done! *)
  END; (* LOOP *)

  (* Close input file if opened *)
  IF (inFile # NIL) THEN
    b := fclose(inFile);
  END;

  (* Close output file if opened *)
  IF (outFile # NIL) THEN
    IF (fclose(outFile)) THEN (* no errors *)
      print(ADR(MsgDone));
    ELSE (* error writing file *)
      error(ADR(ErrMsgWritingOut));
    END;
  END;
END DetabFile;

BEGIN
  openstdio();

  print(ADR(Title));

  IF (argc # 3) THEN
    error(ADR(ErrMsgUsage));
  ELSE
    DetabFile(argv^[1], argv^[2]);
  END;

  closestdio();
END M2Detab.
