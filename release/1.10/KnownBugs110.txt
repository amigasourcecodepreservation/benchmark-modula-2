
KnownBugs110.txt
Benchmark Compiler Update v1.10
3/27/94

Tom Breeden
Aglet Software
Box 3314
Charlottesville, VA 22903
tmb@virginia.edu

   1. The TRUNC() standard function for IEEE REALs/LONGREALs is too slow.

   2. M2Ed has been observed rarely to go into an infinite loop. Conditions
      under which this occurs are not yet determined, but seem to have
      some relationship to incoming keystrokes arriving at the wrong time.

   3. AmigaDOS2.ReadArgs() does not work for a program spawed from M2Ed via
      F4.

   4. ASL file requester in M2Ed will leak memory if in multi-select mode
      and OK is hit but no file is selected and the filename box is empty.
      This seems to be a problem in the ASL library itself.

   5. M2Ed has a rarely appearing bug in with the F6 ("Main Module")
      command appends what you input to the current name, rather than
      replacing it. A workaround is to set the Main Module to nothing, which
      appears to reset things.

   6. M2Debug can sometimes crash if single-stepping into an address trap.

   7. ^C cannot reach a program started from M2Ed via F4.

   8. M2Debug will not open a Super72 or other newer display mode screen.

   9. The "Conflicting REAL implementations" error from M2Lk does not give
      much information.
