
/****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1993 by Jim Olinger                          *
 ****************************************************************************
 * Name: mgM2Link.c                        Version: Amiga.01.10             *
 * Created: 12/18/86   Updated:  2/20/94   Author: Leon Frenkel             *
 * Description: MicroGnuEmacs functions for linking Modula-2 programs.      *
 ****************************************************************************/

/* Change History:
 * 00.00 Original version
 * 00.01 Fixed bug with improper linking order
 * 00.02 Search "M2L:" as default object directory.
 * 00.03 Fixed bug with handling of module identifier in .RFM file
 * 00.04 Added new commands for executing Modula-2 programs
 * 00.05 Added new support for source level-debug info
 * 00.06 Changed to support 32-bit source pos
 */
/****************************************************************************
 * Changes in M2Ed v01.02 to v01.03                                         *
 * 12/30/93                                                                 *
 *                                                                          *
 * Tom Breeden                                                              *
 * Aglet Software                                                           *
 * Box 3314                                                                 *
 * Charlottesville, VA 22903                                                *
 * tmb@virginia.edu                                                         *
 *                                                                          *
 * for SAS C v6.50                                                          *
 *                                                                          *
 *    1. Replaced the #asm kludges with __emit() and geta4() kludges        *
 *                                                                          *
 *    2. Changed much of the #asm in Proc_End into C.                       *
 *       NOTE: much of this will not work if C compiler does optimization   *
 *             (ie, deferred stack restore).                                *
 *                                                                          *
 *    3. Removed: #include <functions.h>                                    *
 *                                                                          *
 *    4. char  Path[]; / * Path string to dir * /                           *
 *             changed to                                                   *
 *       char  Path[2]; / * Path string to dir, dummy size, malloc'd later  *
 *             in struct Dir_Node                                           *
 *                                                                          *
 *    5. Added a proc decl for static Proc_End() so it can be used before   *
 *       definition.                                                        *
 *                                                                          *
 *    6. static char    User_Prog_Parms[80] = 0;                            *
 *            changed to                                                    *
 *       static char    User_Prog_Parms[80] = "";                           *
 *            at beginning                                                  *
 *                                                                          *
 *    7. Added #include <dos.h>, #include <clib/dos_protos.h>,              *
 *       #include <clib/exec_protos.h>. OTHERWISE, some func results that   *
 *       should be addresses are taken as ints and sign extended!           *
 *                                                                          *
 *    8. Replaced "putw()" with "fwrite()".                                 *
 *                                                                          *
 *    9. Added "(cm->ModAdrRelocCount > 0) &&" in front of test for         *
 *       ModAdrBufPtr being NULL after malloc() call. Seems that SAS'       *
 *       malloc() will return NULL if you ask to malloc 0 bytes.            *
 *                                                                          *
 *   10. PutChar(), PutWord(), PutLong(), LongWord() all changed to use     *
 *       ANSI style parameter specification.                                *
 *                                                                          *
 * BUGFIX: C-Style Bad Call                                                 *
 *                                                                          *
 *    1. if "ERROR: Obj File '%s' is Invalid!" occurs then                  *
 *       "Close(InFile)" is called. It should be "CloseFile(InFile)"        *
 *                                                                          *
 * BUGFIX: Illegal Memory Access                                            *
 *                                                                          *
 *    1. Proc name parameter in CreateProc() was sent as NULL. This is not  *
 *       kosher and caused an innocuous Enforcer hit (reference to          *
 *       address 0). Changed that proc to "".                               *
 *                                                                          *
 * BUGFIX: Spawned Job Crashing on 68040                                    *
 *                                                                          *
 *    1. Added call to CacheClearU() before patch of first segment code in  *
 *        Util_Run_Module. This is called only if Exec library is ver 37 or *
 *        higher (AmigaDos v2).                                             *
 *                                                                          *
 *    2. Another one added in Proc_Start().                                 *
 *                                                                          *
 * Compiler Motivated Changes                                               *
 *                                                                          *
 *    SEE file Changes.txt in the compiler build directory.                 *
 *                                                                          *
 *    1. for import of SystemCPU if M68020 code.                            *
 *    2. for import of SystemIEEE or SystemFFP for REAL code.               *
 *                                                                          *
 ****************************************************************************/

/****************************************************************************
 * Changes in M2Ed v01.03 to v01.04                                         *
 * 01/10/94                                                                 *
 *                                                                          *
 * Tom Breeden                                                              *
 * Aglet Software                                                           *
 * Box 3314                                                                 *
 * Charlottesville, VA 22903                                                *
 * tmb@virginia.edu                                                         *
 *                                                                          *
 * Compiler Motivated Changes                                               *
 *                                                                          *
 *    SEE file Changes.txt in the compiler build directory.                 *
 *                                                                          *
 *    1. for import of SystemIEEE or SystemFFP for REAL code.               *
 *                                                                          *
 * BugFix: Linking into RAM: Produces file without Execute Priviledge       *
 *                                                                          *
 *    1. This turns out to be the intended result for all default specified *
 *       Level 2 IO files in SAS C. However, due to a bug in SAS, it        *
 *       works only for RAM: (this info is from a SAS EMITS bug report      *
 *       response).                                                         *
 *                                                                          *
 *       Patch: set compiler global extern "__fmask" to include execute bit *
 *       just before opening the Link output file. Set it back to default   *
 *       bits just after closing it.                                        *
 *                                                                          *
 ****************************************************************************/

/****************************************************************************
 * Changes in M2Ed v01.04 to v01.05                                         *
 * 02/20/94                                                                 *
 *                                                                          *
 * Tom Breeden                                                              *
 * Aglet Software                                                           *
 * Box 3314                                                                 *
 * Charlottesville, VA 22903                                                *
 * tmb@virginia.edu                                                         *
 *                                                                          *
 * Compiler Motivated Changes                                               *
 *                                                                          *
 *    SEE file Changes.txt in the compiler build directory.                 *
 *                                                                          *
 *    1. for import of SystemIEEEdp                                         *
 *                                                                          *
 ****************************************************************************/

#ifdef	M2LINK

/* Include Files */
#include <stdlib.h>
#include <fcntl.h>
#include <dos.h>
#include <exec/types.h>
#include <exec/memory.h>
#include <exec/execbase.h>
#include <dos/dos.h>
#include <dos/dosextens.h>
#include <clib/exec_protos.h>
#include <clib/dos_protos.h>
#undef TRUE
#undef FALSE
#include "def.h"

extern struct ExecBase *SysBase;

extern unsigned long __fmask;

/*extern char *malloc();*/
/*extern char *calloc();*/
extern long ftell();
extern long atol();

/* Default search direcory */
static char *Def_Search_Dir = "M2L:";
#define DEF_SEARCH_DIR_STRLEN 4

/* Version of OBM file */
#define OBMFileVersion		1

/* Modula-2 Compiler Object File Section Tag Definitions */
#define OBJECT_FILE_TAG_HDR	1
#define OBJECT_FILE_TAG_IMP	2
#define OBJECT_FILE_TAG_DAT	3
#define OBJECT_FILE_TAG_COD	4

/* AmigaDos Hunk Type Definitions */
#define HUNKHeader		0x03F3L
#define HUNKCode		0x03E9L
#define HUNKReloc32		0x03ECL
#define HUNKSymbol		0x03F0L
#define HUNKDebug		0x03F1L
#define HUNKEnd			0x03F2L

/* Linker Abort Codes */
#define ABORT_LINK_NO_MSG		0	/* Don't Dspl Error Msg */
#define	ABORT_LINK_NO_MEMORY_MSG	1	/* No Memory Error Msg	*/

/* 68000 instructions */
#define InstJMPAbs		0x4EF9		/* JMP.L xxx		*/
#define InstJSRAbs		0x4EB9		/* JSR.L xxx		*/

/* Buffered File Struc For Simple I/O System */
#define FILE_BUF_SIZE	1024

typedef struct
{
  long		handle;
  short		pos;
  short		end;
  char		buf[FILE_BUF_SIZE];
} File;

/* Module Types */
typedef char		NAME[256];
typedef short		KEYS[3];
typedef struct Module	*MTable[2];
typedef long		PTable[2];
struct Module
{ 
  struct Module	*Next;				/* Next Module In List	     */
  NAME		*Name;				/* Module Name		     */
  KEYS		Key;				/* Module Key		     */
  MTable	*ModuleTable;			/* Table Of Imported Modules */
  PTable	*ProcTable;			/* Table Of Proc Offsets     */
  short		Hunk;				/* Hunk # Assigned To Module */
  long		HunkLWSize;			/* Size of hunk in longwords */
  short		HunkLWAlign;			/* TRUE = needs to be aligned*/
  long		ModuleBaseOffset;		/* Base of Data in Hunk	     */
  long		CodeBaseOffset;			/* Base of Code in Hunk	     */
  short		Procedures;			/* Procedures in Module	     */
  short		Modules;			/* Modules imported	     */
  long		CodeSize;			/* Size of code (word align) */
  long		DataSize;			/* Size of data (word align) */
  long		ConstSize;			/* Size of const(word align) */
  short		FirstModProcReloc;		/* Adr of first Proc Reloc   */
  short		ModProcRelocCount;		/* Proc Reloc Count          */
  short		ModAdrRelocCount;		/* Adr  Reloc Count          */
  short		GenSymbols;			/* TRUE = generate symbols   */
  short		AppendModName;			/* TRUE = prefix module name */
  File		*OBMFile;			/* Ptr to File structure     */
  char          Visited;			/* Node Has Been Visited     */
};

/* Reloc Stuctures */
struct RelocRec
{
  short	Next;		/* Next RelocRec (array index)	*/
  long	Offset;		/* 32-bit Hunk Offset 		*/
};

struct RelocHdr
{
  short	First;		/* Index of first RelocRec 	*/
  short Count;		/* Number of RelocRec in list	*/
};


/* Directory List Node */
struct Dir_Node
{
  struct Dir_Node *Next;	/* Next Dir in list or NULL */
  char            Path[2];	/* Path string to dir, dummy size, malloc'd later */
};

/* .OBM Files Directory Search List specified "set-m2-obj-dir" or NULL */
static 	struct Dir_Node	*Dir_List_Head = NULL;	/* Head of Dir List	*/
static	struct Dir_Node	*Dir_List_Tail = NULL;	/* Tail of Dir List	*/

/* Main Module Name as set by "set-m2-main-module" or NULL if not spec */
static	char	*Default_Main_Module_Name = NULL;

/* Send linker output to a dir, or NULL if current dir */
static	char	*Link_Output_Dir = NULL;

/* Linker Option Variables */
static	short	LinkOptDebugSymbols = FALSE;	/* T = add syms         */
static	short	LinkOptAppendModName = FALSE;	/* T = append mod name  */
static	short	LinkOptSLDInfo = FALSE;		/* T = add SLD info     */

/* Linker variables */
static	File	*InFile;			/* Get Input From This File */
static	FILE	*OutFile;			/* Put Output to This File  */
static	struct Module 	*ModuleListHead;	/* Head of Module List	*/
static	struct Module 	*ModuleListTail;	/* Tail of Module List	*/
static	struct Module 	*CurrentModule;		/* Current Module       */
static	short		NextHunk;		/* Next Hunk ID		*/
static	long		TotalCode;		/* Total Code Size	*/
static	long		TotalData;		/* Total Data Size	*/
static	long		TotalConst;		/* Total Const Size	*/
static	long		TotalInit;		/* Total Size Of Executable*/
static	short	 	Error_Status;		/* TRUE=Error,FALSE=Ok  */
static	short		Error_Code;		/* Error Code 		*/
static	char		VisitedVal;		/* Node has been visited val*/
static	int		(*ModuleNodeFunc)();	/* Proc to call for each node*/
static	struct Module	*mainmod;		/* Pointer to main module */
static	long		Offset;			/* Offset into hunk */

/* Run Program Variables */
static	short		User_Prog_Count = 0;
static	char		User_Prog_Window[80] = "RAW:0/0/640/100/";
static	char		User_Prog_Parms[80] = "";
static	long		User_Prog_SegList = NULL;
static	long		User_Prog_Parms_Length = 0;
static	long		User_Prog_StackSize = 20000L;
static	long		User_Prog_IO = NULL;
static	struct Process	*User_Prog_Parent;
static	short		User_Prog_w;
static	long		User_Prog_l;
static	long		Emacs_Signal;

static void Proc_End(void);

/***************************************************************************
 * setm2objdir - Specify a list of dirs to search for .OBM files. The dirs
 *  are seperated by a space. Dirs can be specified with quotes.
 ***************************************************************************/
setm2objdir(f, n, k)
{ register int s;
  register int l;
  register int subl;
  register int subi;
  char         buf[80];

  if ((s=ereply("Obj Dir List: ", buf, sizeof(buf))) == ABORT)
  {
    return(s);
  }

  Util_Free_Dir_List();
  
  /* Make "M2L:" the default search path */
  Util_Dir_List_Add_Node(Def_Search_Dir, DEF_SEARCH_DIR_STRLEN);

  /* If <RETURN> entered then simply erases old dir list */
  if (s == FALSE)
  {
    return(TRUE);
  }

  /* Parse List of Directories */
  s = 0;

  while (TRUE)
  {
    s = findtoken(buf, s); /* Advance to next token */
    if (s == -1)
    { /* End of string */
      break;
    }
    l = findtokenlength(buf, s); /* Determine length of token */
    subl = l;		/* length of sub string */
    subi = s;		/* index of sub string */
    s = s + l; 		/* Advance to next char after token */
    if ((subl > 2) && (buf[subi] == '"'))
    {
      subl = subl - 2;	/* length of sub string without quotes */
      subi++;		/* skip over first quote */
    }
    Util_Dir_List_Add_Node(&(buf[subi]), subl );
  }

  return(TRUE);
}

/* findtoken - advance to next token (non space char), if null-term hit then
 *  return -1, otherwise return new index into string.
 */
static findtoken(str, i)
register char str[];
register int i;
{
  while (str[i] == ' ')
  {
    i++;
  }
  if (str[i] == 0) 
  {
    return(-1);
  }
  else
  {
    return(i);
  }
}

/* Find length of token */
static int findtokenlength(str, i)
register char str[];
register int  i;
{ register int len;

  len = 0;
  while ((str[i] != ' ') && (str[i] != 0))
  {
    len++;
    i++;
  }
  return(len);
}

/***************************************************************************
 * setm2mainmodule - Set the main module for future link/debug operations.
 ***************************************************************************/
setm2mainmodule(f, n, k)
{ register int s;
  char         buf[80];

  if ((s=ereply("Main Module: ", buf, sizeof(buf))) == ABORT)
  {
    return(s);
  }

  /* Free space used by old module name string */
  if (Default_Main_Module_Name != NULL)
  {
    free(Default_Main_Module_Name);
    Default_Main_Module_Name = NULL;
  }

  /* if <RETURN> then simply clear main module name */
  if (s == FALSE)
  {
    return(TRUE);
  }

  /* Allocate space for new string */
  Default_Main_Module_Name = malloc(strlen(buf)+1);
  if (Default_Main_Module_Name != NULL)
  {
    strcpy(Default_Main_Module_Name, buf);
  }
  return(TRUE);
}

/***************************************************************************
 * setm2linkoutdir - specify a dir to which to send output of linker.
 ***************************************************************************/
setm2linkoutdir(f, n, k)
{ register int s;
  char         buf[80];

  if ((s=ereply("Linker Output Dir: ", buf, sizeof(buf))) == ABORT)
  {
    return(s);
  }

  if (Link_Output_Dir != NULL)
  {
    free(Link_Output_Dir);
    Link_Output_Dir = NULL;
  }

  if (s == FALSE)
  {
    return(TRUE);
  }

  /* Allocate space for new string */
  Link_Output_Dir = malloc(strlen(buf)+1);
  if (Link_Output_Dir != NULL)
  {
    strcpy(Link_Output_Dir, buf);
  }
  return(TRUE);
}

/***************************************************************************
 * setm2linkgensym - enable/disable generation of symbols by linker.
 ***************************************************************************/
setm2linkgensym(f, n, k)
{ register int s;
  UBYTE optDebugSymbols;
  UBYTE optAppendModName;
  UBYTE optSLDInfo;

  optDebugSymbols  = LinkOptDebugSymbols;
  optAppendModName = LinkOptAppendModName;
  optSLDInfo       = LinkOptSLDInfo;

  s = Util_Prompt_Opt_Bool(&optSLDInfo,"Generate Source Level Debug Info")
      &&
      Util_Prompt_Opt_Bool(&optDebugSymbols,"Generate Debug Symbols")
      &&
      Util_Prompt_Opt_Bool(&optAppendModName,"Prefix Symbols With Module Name");

  /* Reload linker options */
  LinkOptDebugSymbols  = optDebugSymbols;
  LinkOptAppendModName = optAppendModName;
  LinkOptSLDInfo       = optSLDInfo;

  if (s) 
  { /* linker options changed succesfully! */
    return (TRUE);
  }
  else
  { /* user aborted input with ^G */
    return (ABORT);
  }
}

/***************************************************************************
 * m2linkmain - link the currently specified main module
 ***************************************************************************/
m2linkmain(f, n, k)
{
  if (Util_Test_Main_Module() == FALSE)
  {
    return(FALSE);
  }

  return( Util_Link_Module(Default_Main_Module_Name) );
}

/***************************************************************************
 * m2linkmodule - link a specified module.
 ***************************************************************************/
m2linkmodule(f, n, k)
{ register int s;
  char         buf[80];

  if ((s=ereply("Link Module: ", buf, sizeof(buf))) != TRUE)
  {
    return(s);
  }
  return( Util_Link_Module(buf) );
}

/***************************************************************************
 * m2runmain - run the currently specified main module
 ***************************************************************************/
m2runmain(f, n, k)
{
  if (Util_Test_Main_Module() == FALSE)
  {
    return(FALSE);
  }
  return (Util_Run_Module(Default_Main_Module_Name));
}

/***************************************************************************
 * m2runmodule - run a specified module (program)
 ***************************************************************************/
m2runmodule(f, n, k)
{ register int s;
  char         buf[80];

  if ((s=ereply("Run Module: ", buf, sizeof(buf))) != TRUE)
  {
    return(s);
  }
  return( Util_Run_Module(buf) );
}

/***************************************************************************
 * setm2runstacksize - Set the amount of stack space to be allocated for the
 *  use of a program to be executed.
 ***************************************************************************/
setm2runstacksize(f, n, k)
{ register int  s;
  char          buf[16];

  if ((s=ereply("Run Stack Size: (default %ld) ",buf, sizeof(buf),
      User_Prog_StackSize))==ABORT)
  { /* User Aborted Prompt */
    return(s);
  }
  if (s == FALSE)
  { /* User hit <RETURN> at prompt so keep the current stack size value */
    return(TRUE);
  }
  User_Prog_StackSize = atol(buf);
  return(TRUE);
}

/***************************************************************************
 * setm2runwindow - Specify input/output window for programs run from the
 *  editor.
 ***************************************************************************/
setm2runwindow(f, n, k)
{ register int  s;
  char buf[ sizeof(User_Prog_Window) ];

  if ((s=ereply("Run Window: (default %s) ",buf, sizeof(buf),
      User_Prog_Window))==ABORT)
  { /* User Aborted Prompt */
    return(s);
  }

  if (s == FALSE)
  {
    return (TRUE);
  }

  strcpy(User_Prog_Window, buf);

  return(TRUE);
}

/***************************************************************************
 * setm2runargs - specify a parameter string to be passed to a program
 *  invoked from the editor
 ***************************************************************************/
setm2runargs(f, n, k)
{ register int  s;

  if ((s=ereply("Run Args: ",User_Prog_Parms,sizeof(User_Prog_Parms))) == ABORT)
  { /* User Aborted Prompt */
    return(s);
  }

  /* +1 is needed since parm string includes terminator */
  User_Prog_Parms_Length = strlen(User_Prog_Parms) + 1;

  return(TRUE);
}

/******************************* PRIVATE FUNC *******************************/

/***************************************************************************
 * Util_Link_Module - link a module, generating an Amiga executable file.
 ***************************************************************************/
static Util_Link_Module(Prog_Module_Name)
NAME Prog_Module_Name;
{ NAME Output_FileName;

  /* Initialization */
  NextHunk        = 1;   /* Hunk 0 is used for the startup code */
  ModuleListHead  = NULL;
  ModuleListTail  = NULL;
  TotalCode       = 0;
  TotalData       = 0;
  TotalConst      = 0;
  TotalInit       = 0;
  Error_Status    = FALSE;
  OutFile         = NULL;

  /* Linker Pass 1 */
  GetModule(Prog_Module_Name);
  if (Error_Status == TRUE) { goto Abort_Link; }
  CurrentModule = CurrentModule->Next;
  while (CurrentModule != NULL)
  {
    GetModule(CurrentModule->Name);
    if (Error_Status == TRUE) { goto Abort_Link; }
    CurrentModule = CurrentModule->Next;
  }

  /* Generate Load File as Result of Linking */
  Output_FileName[0] = 0; /* null-term string */

  if (Link_Output_Dir != NULL)
  {  
    strcpy(Output_FileName, Link_Output_Dir);
  }
  strcat(Output_FileName, Prog_Module_Name);

  /* Insure that the Execute bit is on for output file */
  __fmask = S_IREAD | S_IWRITE | S_IEXECUTE | S_IDELETE;
                                   /* __fmask is a compiler system global */

  OutFile = fopen(Output_FileName, "w");
  if (OutFile == NULL)
  {
    ewprintf("ERROR: Unable To Open '%s' As Output File!",Output_FileName);
    goto Abort_Link;
  }
  ewprintf("Linker Output File -> %s",Output_FileName);

  Output_Load_File_Header();
  if (Error_Status == TRUE) { goto Abort_Link; }

  CurrentModule = ModuleListHead;
  while (CurrentModule != NULL)
  {
    LinkModule(CurrentModule);
    if (Error_Status == TRUE) { goto Abort_Link; }
    CurrentModule = CurrentModule->Next;
  }
  fclose(OutFile);
  Cleanup_Link(); /* Deallocate Resources */

  /* Printout Statistics: */
  ewprintf("Code:%ld UData:%ld IData:%ld Init:%ld Total:%ld",
           TotalCode, TotalData, TotalConst, TotalInit,
           (TotalCode+TotalData+TotalConst+TotalInit));
  return(TRUE);   /* Good Link */

/* Link Process Aborted Due to Errors */
Abort_Link:
  if (Error_Code == ABORT_LINK_NO_MEMORY_MSG)
  {
    ewprintf("ERROR: Insufficient Memory To Continue Linking Process!");
  }

  /* If Output file has been opened then close file and delete the file */
  if (OutFile != NULL)
  {
    fclose(OutFile);
    DeleteFile(Output_FileName);
  }
  Cleanup_Link(); /* Deallocate resources */

  return(FALSE); /* Bad Link */
}

/***************************************************************************
 * Cleanup_Link - cleanup all dynamic resources
 ***************************************************************************/
static Cleanup_Link()
{
  /* Return the Execute bit normal state to off for an output file */
  __fmask = S_IREAD | S_IWRITE | S_IDELETE;
                                   /* __fmask is a compiler system global */
  Cleanup_Module_List();
}

/***************************************************************************
 * Cleanup_Module_List - step through module list deallocating the nodes and
 *  the objects linked to the nodes, also closing input file attached to node.
 ***************************************************************************/
static Cleanup_Module_List()
{ register struct Module  *p,*f;
  
  p = ModuleListHead;

  while (p != NULL)
  {
    if (p->Name        != NULL) { free(p->Name); }
    if (p->ModuleTable != NULL) { free(p->ModuleTable); }
    if (p->ProcTable   != NULL) { free(p->ProcTable); }
    if (p->OBMFile     != NULL) { CloseFile(p->OBMFile); }
    f = p;
    p = p->Next;
    free(f);
  }
}

/* GetChar - read a char from InFile */
static unsigned char GetChar()
{ char c;
  
  c = InFile->buf[InFile->pos++];
  if (InFile->pos >= InFile->end)
  {
    InFile->pos = 0;
    InFile->end = (long)Read(InFile->handle,
                             &(InFile->buf[0]),
                             (long)FILE_BUF_SIZE);
  }
  return(c);
}

/* GetWord - Read a Word(2 bytes) from the InFile */
static short GetWord()
{
  return( (GetChar()<<8) | (GetChar()) );
}

/* PutChar - write a char to OutFile */
static PutChar(char c)
{
  putc(c, OutFile);
}

/* PutWord - write a word to OutFile */
static PutWord(int w)
{
  /*putw(w, OutFile);*/
  fwrite(&w, 2, 1, OutFile);
}

static PutLong(long l)
/*register long l;*/
{
  /*putw((short)(l>>16), OutFile);*/
  /*putw((short)(l)    , OutFile);*/
  fwrite(&l, 4, 1, OutFile);
}

/* OpenFile - open a file for reading */
static File *OpenFile(fileName)
NAME fileName;
{ register File *fp;

  fp = (File*)malloc(sizeof(File));
  if (fp != NULL)
  {
    fp->pos    = 0;
    fp->handle = (long)Open(fileName, (long)MODE_OLDFILE);
    if (fp->handle == NULL)
    {
      free(fp);
      return(NULL);
    }
    fp->end = (long)Read(fp->handle, &(fp->buf[0]), (long)FILE_BUF_SIZE);
  }
  return(fp);
}

/* CloseFile - close a file */
static CloseFile(fp)
register File *fp;
{
  Close(fp->handle);
  free(fp);
}

static long LongWord(long l)
{
  return((l + 3) >> 2);
}

/****** Debuger Symbol Generation Functions ******/
static	char		*Symbol;		/* Symbol string ptr	   */
static	short		symbolLen;		/* length of symbol	   */
static	short		ModNameLen;		/* Length of module name   */
static	struct Module	*cm;			/* Current Module ptr 	   */

/* GetName - read a name string */
static GetName()
{ register short	L,i;

  L = GetChar() - 1;
  symbolLen = L;
  i = 0;
  while (L > 0)
  {
    Symbol[i++] = GetChar();
    L--;
  }
}

/* PutSymbol - generate a symbol record */
static PutSymbol(value,ModNameFlag)
long  	value;
short 	ModNameFlag;
{ register short	len;
  register short 	i,j;

  len = symbolLen;
  if (ModNameFlag && cm->AppendModName)
  {
    len = len + ModNameLen + 1;
  }

  /* symbol data unit [0][NameLenInLongWords] */
  PutLong(LongWord((long)len));

  /* prefix module name if necessary */
  if (ModNameFlag && cm->AppendModName) /* Output module name and _ */
  {
    i = 0;
    j = ModNameLen;
    while (j > 0)
    {
      PutChar((*cm->Name)[i++]);
      j--;
    }
    PutChar('_');
  }

  /* output symbol */
  i = 0;
  j = symbolLen;
  while (j > 0)
  {
    PutChar(Symbol[i++]);
    j--;
  }

  /* pad symbol to longword boundary */
  j = (LongWord((long)len)*4) - len;
  while (j != 0)
  {
    PutChar(0);
    j--;
  }

  /* output symbol value */
  PutLong(value);
}

/* GenDebugSymbols - output a symbol block to output file */
static GenDebugSymbols(CurMod)
struct Module 		*CurMod;
{ NAME 			InFileName;
  short			FileType;		/* TRUE = RFM, FALSE = SBM */
  register short	block;			/* ref file block	   */
  register short	i,n,l,a;
  short			k0,k1,k2;
  char			c;
  char			symbolStr[512];		/* symbol string	   */

  /* Ref File block types */
#define REFFILE		0x00DD	/* 335B */
#define CTL		((short)0xF600)
#define anchor		0
#define ModTag		1
#define ProcTag		2
#define RefTag		3
#define linkage		4
#define STR		((short)0xF400)
#define Enum		0
#define range		1
#define pointer		2
#define set		3
#define procTyp		4
#define funcTyp		5
#define array		6
#define dynarr		7
#define record		8
#define opaque		9
#define CMP		((short)0xF200)
#define parref		0
#define par		1
#define field		2
#define OBJ		((short)0xF000)
#define varref		0
#define var		1
#define Const		2
#define string		3
#define type		4
#define proc		5
#define func		6
#define module		7
#define svc		8
#define svcfunc		9

  /* initialize some static variables */
  cm = CurMod;
  Symbol = &symbolStr[0];

  ModNameLen = strlen(cm->Name);

  strcpy(InFileName, cm->Name);
  strcat(InFileName, ".RFM");
  Util_Scan_Dir_List(InFileName);
  InFile = OpenFile(InFileName);
  if (InFile == NULL)
  {
    strcpy(InFileName, cm->Name);
    strcat(InFileName, ".SBM");
    Util_Scan_Dir_List(InFileName);
    InFile = OpenFile(InFileName);
    if (InFile == NULL)
    {
      return; /* No Symbol File Found From Which To Read Symbols! */
    }
    FileType = FALSE; /* SBM File */
  }
  else
  {
    FileType = TRUE; /* RFM File */
  }

  /* Display filename for symbol file */
  ewprintf("Sym: %s", InFileName);

  if (GetWord() != REFFILE) /* Invalid File */
  {
    goto error;
  }

  /* Check key of symbol file against key of object module */
  block = GetWord(); /* anchor */
  k0    = GetWord(); /* key1 */
  k1    = GetWord(); /* key2 */
  k2    = GetWord(); /* key3 */
  GetName();	     /* name */
  if ((k0 != cm->Key[0]) ||
      (k1 != cm->Key[1]) ||
      (k2 != cm->Key[2]))
  {
    CloseFile(InFile);
    ewprintf("ERROR: Sym File '%s' Has Bad Key!",InFileName);
    Error_Status = TRUE;
    Error_Code = ABORT_LINK_NO_MSG;
    return;
  }

  /* Start of Symbol Block */
  PutLong(HUNKSymbol);

  /* Generate Module Base Symbol (for view from Hunk Display) */
  strcpy(Symbol, cm->Name);
  symbolLen = strlen(Symbol);
  PutSymbol(0L, FALSE);

  /* Generate Module Body Symbol */
  strcpy(Symbol, cm->Name);
  strcat(Symbol, "_ModuleBody");
  symbolLen = strlen(Symbol);
  PutSymbol(cm->CodeBaseOffset + 4, FALSE);
  
  while (TRUE)
  {
    block = GetWord();
    if (block < CMP)
    {
      block = block - OBJ;
      switch (block)
      {
        case varref:	GetWord(); /* StrRef */
			GetWord(); /* level  */
			GetWord(); /* adr    */
			GetName(); /* name   */
			break;
        case var:	GetWord();     /* StrRef */
			l = GetWord(); /* level  */
			a = GetWord(); /* adr    */
			GetName();     /* name   */
			if (l == 0) /* Global Variable */
			{
			  PutSymbol(cm->ModuleBaseOffset+a,TRUE);
			}
			break;
        case Const:	GetWord();     /* StrRef */
			GetWord();     /* ModRef */
			c = GetChar(); /* ModRef */
			switch(c) /* value */
			{
			  case 2:	GetChar();
					break;
			  case 3:	GetWord();
					break;
			  case 5:	GetWord();
					GetWord();
					break;
			  case 9:	GetWord();
					GetWord();
					GetWord();
					GetWord();
					break;
			}
			GetName(); /* name */
			break;
        case string:	GetWord(); /* StrRef */
			GetName(); /* string */
			GetName(); /* name   */
			break;
        case type:	GetWord(); /* StrRef */
			GetWord(); /* ModRef */
			GetName(); /* name   */
			break;
        case proc:
        case func:	if (block == func) /* function */
			{
			  GetWord(); /* ResRef */
			}
			n = GetWord(); /* ProcNo */
			l = GetWord(); /* level  */
			a = GetWord(); /* adr    */
			GetWord();     /* size   */
			GetName();     /* name   */
			if (FileType) /* RFM File */
			{
			  PutSymbol(cm->CodeBaseOffset + a,TRUE);
			}
			else /* SBM File */
			{
			  PutSymbol(cm->CodeBaseOffset + (*cm->ProcTable)[n],TRUE);
			}
			break;
        case module:	GetWord(); /* ModNo */
			GetName(); /* name */
			break;
        case svc:
        case svcfunc:	if (block == svcfunc)
			{
			  GetWord(); /* ResRef */
			}
			GetWord(); /* cnum */
			GetName(); /* name */
			break;
        default:	goto error; /* Unknown block type */
      }
    }
    else if (block < STR)
    {
      block = block - CMP;
      switch (block)
      {
        case parref:
        case par:	GetWord(); /* StrRef */
			break;
	case field:	GetWord(); /* StrRef */
			GetWord(); /* offset */
			GetName(); /* name   */
			break;
	default:	goto error; /* Unknown block type */
      }
    }
    else if (block < CTL)
    {
      block = block - STR;
      GetWord(); /* size */
      switch (block)
      {
        case Enum:	GetWord(); /* noConst */
			break;
        case range:	GetWord(); /* BaseRef */
			GetWord(); /* min     */
			GetWord(); /* max     */
			break;
        case pointer:   break;
        case set:	GetWord(); /* BaseRef */
			break;
        case procTyp:
        case funcTyp:	if (block == funcTyp)
			{
			  GetWord(); /* ResRef */
			}
			break;
        case array:	GetWord(); /* ElemRef */
			GetWord(); /* IndxRef */
			break;
        case dynarr:	GetWord(); /* ElemRef */
			break;
        case record:	break;
        case opaque:	break;
        default:	goto error; /* Unknown block type */
      }
    }
    else if (block < 0)
    {
      block = block - CTL;
      switch (block)
      {
        case anchor:	GetWord(); /* key1 */
			GetWord(); /* key2 */
			GetWord(); /* key3 */
			GetName(); /* name */
			break;
        case ModTag:	GetWord(); /* ModNo */
			break;
        case ProcTag:	GetWord(); /* ProcNo */
			break;
        case RefTag:	GetWord(); /* adr */
			GetWord(); /* pno */
			goto done; /* end of file */
        case linkage:	GetWord(); /* StrRef */
			GetWord(); /* BaseRef */
			break;
        default:	goto error; /* Unknown block type */
      }
    }
    else /* PC block */
    {
      GetWord(); /* hi src pos */
      GetWord(); /* lo src pos */
    } /* IF */
  } /* LOOP */

error:
  ewprintf("ERROR: Sym File '%s' is Invalid!",InFileName);
  Error_Status = TRUE;
  Error_Code = ABORT_LINK_NO_MSG;

done:
  PutLong(0L); /* Symbol Block Terminator */

  CloseFile(InFile);
}
/****** Debuger Symbol Generation Functions End ******/


/***************************************************************************
 * NewModule - add new module to module list or check against current entry
 ***************************************************************************/
#define NEWMODULE_OK		0	/* Everything Ok    */
#define	NEWMODULE_BAD_KEY	1	/* Incompatible Key */
#define NEWMODULE_NO_MEMORY	2	/* No More Memory   */

static NewModule(mName, mKey, modStruc)
register NAME mName;
register KEYS mKey;
struct Module **modStruc; /* return parm */
{ register struct Module *mod;
  register int i;

  /* Search module list until module found or end of list reached */
  mod = ModuleListHead;
  while ((mod != NULL) && (strcmp(mod->Name,mName) != 0))
  {
    mod = mod->Next;
  }

  if (mod != NULL)
  { /* Already exists in module list */
    if ( mKey[0] != mod->Key[0] ||
         mKey[1] != mod->Key[1] || 
         mKey[2] != mod->Key[2] )
    {
      return(NEWMODULE_BAD_KEY);	/* Incompatible Key */
    }
  }
  else
  { /* Create new module descriptor (clear all fields to zero) */
    mod = (struct Module*)calloc(1, sizeof(struct Module));
    if (mod == NULL)
    {
      return(NEWMODULE_NO_MEMORY);
    }

    /* Allocate space for name string */
    mod->Name = (NAME*)malloc(strlen(mName)+1);
    if (mod->Name == NULL)
    {
      free(mod);
      return(NEWMODULE_NO_MEMORY);
    }

    /* Copy name string to memory */
    strcpy(mod->Name, mName);

    /* Copy module key */
    for (i = 0; i < 3; i++)
    {
      mod->Key[i] = mKey[i];
    }
    mod->Hunk = NextHunk; 
    NextHunk++;

    /* Link Module Into List */
    if (ModuleListHead == NULL)
    {
      ModuleListHead = mod;
      ModuleListTail = mod;
    }
    else
    {
      ModuleListTail->Next = mod;
      ModuleListTail = mod;
    }
  }

  *modStruc = mod; 	/* Set return parm */
  return(NEWMODULE_OK);	/* Sucessfull func */
}

/* ReadProcTable - allocate memory and read from InFile the procedure table */
static PTable *ReadProcTable(procs)
{ register PTable *p;
  register short  i;

  p = (PTable*)malloc(sizeof(long) * procs);
  if (p != NULL)
  {
    for (i = 0; i <= procs-1; i++)
    {
      (*p)[i] = GetWord();
    }
  }
  return(p);
}

/***************************************************************************
 * GetModuleName - Read in the Module Name and the Module Key
 * If Dummy Entry Encountered Return FALSE otherwise return TRUE.
 ***************************************************************************/
static short GetModuleName(mName,mKey)
register NAME mName;
register KEYS mKey;
{ register short i;
  register char  c;

  /* Read Module name which is a null-terminated string */
  i = 0;
  do
  {
    c = GetChar();
    mName[i] = c;
    i++;
  }
  while (c != 0);
  if (i == 1)
  { /* Dummy Entry */
    return(FALSE);
  }

  for (i = 0; i < 3; i++)
  {
    mKey[i] = GetWord();
  }
  return(TRUE);
}

/* GetModule - Perform Pass 1 Processing on a Module */
static GetModule(ModName)
char ModName[];
{ NAME                   InFileName;
  NAME                   mName;
  KEYS                   mKey;
  register struct Module *cm;
  register short         i, j;

  /* Create FileName from Module Name */
  strcpy(InFileName, ModName);
  strcat(InFileName, ".OBM");

  /* Scan dir list, stores result back into InFileName */
  Util_Scan_Dir_List(InFileName);

  /* Open '.OBM' file for input */
  InFile = (File*)OpenFile(InFileName);
  if (InFile == NULL)
  { /* File not opened? */
    ewprintf("ERROR: Can't Open Obj File '%s'!", InFileName);
    Error_Status = TRUE;
    Error_Code = ABORT_LINK_NO_MSG;
    return;
  }
  if (GetWord() != OBJECT_FILE_TAG_HDR) /* Read HDR Tag */
  {
    ewprintf("ERROR: Obj File '%s' is Invalid!", InFileName);
    CloseFile(InFile);
    Error_Status = TRUE; 
    Error_Code = ABORT_LINK_NO_MSG;
    return;
  }
  GetWord(); /* BlockSize */

  /* Read Version of object module, must match version supported by linker */
  if (GetWord() != OBMFileVersion)
  {
    CloseFile(InFile);
    ewprintf("ERROR: Obj File '%s' Wrong Version!", InFileName);
    Error_Status = TRUE;
    Error_Code = ABORT_LINK_NO_MSG;
    return;
  }

  /* Read Module Name and Key */
  GetModuleName(&mName, &mKey);

  /* Create a module desc if not already allocated */
  i = NewModule(mName, mKey, &CurrentModule);
  if (i == NEWMODULE_BAD_KEY)
  {
    ewprintf("ERROR: Module '%s' Has Bad Key!", ModName);
    CloseFile(InFile);
    Error_Status = TRUE;
    Error_Code = ABORT_LINK_NO_MSG;
    return;
  }
  if (i == NEWMODULE_NO_MEMORY)
  {
    CloseFile(InFile);
    Error_Status = TRUE; 
    Error_Code = ABORT_LINK_NO_MEMORY_MSG;
    return;
  }
  cm = CurrentModule; /* Set local synonym for Current Module */

  /* Initialize Module Descriptor */
  cm->OBMFile           = InFile;   /* Attach File Handle */
  cm->CodeSize          = GetWord();
  cm->DataSize          = GetWord();
  cm->ConstSize         = GetWord();
  cm->Procedures        = GetWord();
  cm->Modules           = GetWord();
  cm->ModProcRelocCount = GetWord();
  cm->FirstModProcReloc = GetWord();
  cm->ModAdrRelocCount  = GetWord();
  cm->ModuleBaseOffset  = cm->DataSize;
  cm->CodeBaseOffset    = cm->ModuleBaseOffset + cm->ConstSize;
  cm->HunkLWSize        = LongWord(cm->CodeBaseOffset + cm->CodeSize);
  cm->HunkLWAlign       = ((cm->CodeBaseOffset + cm->CodeSize) % 4) != 0;
  cm->GenSymbols        = LinkOptDebugSymbols;
  cm->AppendModName     = LinkOptAppendModName;
  cm->ModuleTable       = (MTable*)malloc(sizeof(struct Module*) * cm->Modules);
  if (cm->ModuleTable == NULL)
  {
    Error_Status = TRUE; 
    Error_Code = ABORT_LINK_NO_MEMORY_MSG;
    return;
  }
  /* First Index Index In Module Table Set to Current Module */
  (*(cm->ModuleTable))[0] = cm;

  /* Process IMPORT Block */
  GetWord(); /* Read Tag */
  GetWord(); /* BlockSize */

  for (j = 1; j < cm->Modules; j++)
  {
    if (GetModuleName(&mName,&mKey) == FALSE)
    { /* Dummy Import */
      (*(cm->ModuleTable))[j] = NULL;
      continue;
    }
    /* Real Import */
    i = NewModule(mName,mKey, &((*(cm->ModuleTable))[j]) );
    if (i == NEWMODULE_BAD_KEY)
    {
      ewprintf("ERROR: Module '%s' Imported By '%s' Has Bad Key!", mName, ModName);
      Error_Status = TRUE;
      Error_Code = ABORT_LINK_NO_MSG;
      return;
    }
    if (i == NEWMODULE_NO_MEMORY)
    {
      Error_Status = TRUE; 
      Error_Code = ABORT_LINK_NO_MEMORY_MSG;
      return;
    }
  }

  /* DAT Block */
  GetWord();	/* Tag */
  GetWord();	/* BlockSize */
  cm->ProcTable = ReadProcTable(cm->Procedures);
  if (cm->ProcTable == NULL)
  {
      Error_Status = TRUE; 
      Error_Code = ABORT_LINK_NO_MEMORY_MSG;
      return;
  }

  /* Tell user what's going on */
  ewprintf("<- %s", InFileName);
}

/* LinkModule - Link a specified module */
static	short		RelocIdx;
static	struct RelocRec	*RelocBufPtr;
static	struct RelocHdr	*RelocHdrPtr;

static Reloc(hunk,ofs)
register short	hunk;
long	ofs;
{
  RelocBufPtr[RelocIdx].Next   = RelocHdrPtr[hunk].First;
  RelocBufPtr[RelocIdx].Offset = ofs;
  RelocHdrPtr[hunk].First      = RelocIdx;
  RelocHdrPtr[hunk].Count++;
  RelocIdx++;
}

static LinkModule(cm)
register struct Module *cm;
{ register short	*ModAdrBufPtr;
  register short	*CodeBufPtr;
  register short	i,j,k,p,m;
  short			a;
  long			r;
  long			li;

  RelocIdx = 0;
  InFile = cm->OBMFile;	/* Connect input stream to current object module */

  /* Update totals */
  TotalCode  += cm->CodeSize;
  TotalData  += cm->DataSize;
  TotalConst += cm->ConstSize;

  /* Perform all memory allocations for this function! */
  ModAdrBufPtr  = (short*)malloc(cm->ModAdrRelocCount * 2);
  CodeBufPtr    = (short*)malloc((short)cm->CodeSize);
  RelocBufPtr   = (struct RelocRec*)malloc((cm->ModAdrRelocCount +
                      cm->ModProcRelocCount + 1) * sizeof(struct RelocRec));
  RelocHdrPtr   = (struct RelocHdr*)calloc(NextHunk, sizeof(struct RelocHdr));
  if (((cm->ModAdrRelocCount > 0) && (ModAdrBufPtr == NULL))
         ||(CodeBufPtr == NULL)
         ||(RelocBufPtr == NULL)
         ||(RelocHdrPtr == NULL))
  {
    if (ModAdrBufPtr != NULL) { free(ModAdrBufPtr); }
    if (CodeBufPtr   != NULL) { free(CodeBufPtr);   }
    if (RelocBufPtr  != NULL) { free(RelocBufPtr);  }
    if (RelocHdrPtr  != NULL) { free(RelocHdrPtr);  }
    Error_Status = TRUE; 
    Error_Code = ABORT_LINK_NO_MEMORY_MSG;
    return;
  }

  /* Read Adr Reloc Offset list */
  j = 0;
  i = cm->ModAdrRelocCount;
  while (i != 0)
  {
    ModAdrBufPtr[j++] = GetWord();
    i--;
  }

  /* Output Code Hunk Representing Module */
  PutLong(HUNKCode);
  PutLong(cm->HunkLWSize);

  /* Output Data Section */
  i = cm->DataSize;
  while (i != 0)
  {
    PutWord(0);
    i -= 2;
  }

  /* Output Const Section */
  i = cm->ConstSize;
  while (i != 0)
  {
    PutWord(GetWord());
    i -= 2;
  }

  i = GetWord(); /* Tag COD */
  i = GetWord(); /* BlockSize */

  /* Load Code Block */
  i = cm->CodeSize;
  j = 0;
  while (i != 0)
  {
    CodeBufPtr[j++] = GetWord();
    i -= 2;
  }

  /* Store ptr to ModuleBase into adr 0 of code section */
  CodeBufPtr[0] = (short)(cm->ModuleBaseOffset >> 16);
  CodeBufPtr[1] = (short)(cm->ModuleBaseOffset);

  /* Reloc entry for ptr to modulebase at pc address 0 */
  Reloc(cm->Hunk, cm->CodeBaseOffset);

  /* Fixup Proc Reloc */
  i = cm->ModProcRelocCount;
  j = cm->FirstModProcReloc >> 1;
  while (i != 0)
  {
    k = j;
    j = CodeBufPtr[k+1] >> 1;
    m = CodeBufPtr[k] & 0xff;
    p = (CodeBufPtr[k] >> 8) & 0xff;
    li = (*((*cm->ModuleTable)[m])->ProcTable)[p] +
         ((*cm->ModuleTable)[m])->CodeBaseOffset;
    CodeBufPtr[k]   = (short)(li >> 16);
    CodeBufPtr[k+1] = (short)(li);

    Reloc((*cm->ModuleTable)[m]->Hunk, cm->CodeBaseOffset + (k*2));
    i--;
  }

  /* Fixup Adr Reloc */
  i = cm->ModAdrRelocCount;
  j = 0;
  while (i != 0)
  {
    k = ModAdrBufPtr[j] >> 1;
    j++;
    m = CodeBufPtr[k];
    a = CodeBufPtr[k+1];
    li = (*cm->ModuleTable)[m]->ModuleBaseOffset + a;
    CodeBufPtr[k]   = (short)(li >> 16);
    CodeBufPtr[k+1] = (short)(li);

    Reloc((*cm->ModuleTable)[m]->Hunk, cm->CodeBaseOffset + (k*2));
    i--;
  }

  i = cm->CodeSize;
  j = 0;
  while (i != 0)
  {
    PutWord(CodeBufPtr[j++]);
    i -= 2;
  }

  /* Align on Long Word Boundary If Not Already */
  if (cm->HunkLWAlign)
  {
    PutWord(0);
  }

  /* Output Reloc 32 For The Current Hunk */
  PutLong(HUNKReloc32);
  for (i = 1; i <= NextHunk-1; i++)
  {
    if (RelocHdrPtr[i].Count != 0)
    {
      j = RelocHdrPtr[i].First;
      PutLong((long)RelocHdrPtr[i].Count); /* # of offsets */
      PutLong((long)i);	/* Hunk # */
      for (k = 1; k <= RelocHdrPtr[i].Count; k++)
      {
        PutLong(RelocBufPtr[j].Offset);
        j = RelocBufPtr[j].Next;
      }
    }
  }
  PutLong(0L); /* Reloc 32 Terminator */

  if (cm->GenSymbols) /* Generate symbols */
  {
    /* WARNING: InFile is invalid after calling this function since it
     *          changes the value of InFile
     */
    GenDebugSymbols(cm);
  }
  /* End of Hunk */
  PutLong(HUNKEnd);

  /* deallocate all buffers */
  free(ModAdrBufPtr);
  free(CodeBufPtr);
  free(RelocBufPtr);
  free(RelocHdrPtr);
}

/***************************************************************************
 * FindModule - Lookup a module in the Module_List if not found return NULL
 ***************************************************************************/
static struct Module *FindModule(ModName)
register NAME ModName;
{ register struct Module *mod;

  mod = ModuleListHead;
  while ((mod != NULL) && (strcmp(mod->Name,ModName) != 0))
  {
    mod = mod->Next;
  }
  return(mod);
}

/***************************************************************************
 * TraverseModule - Used to recursively traverse the module tree
 ***************************************************************************/
static TraverseModule(mod)
struct Module *mod;
{ register short n;

  if (mod->Visited != VisitedVal) /* Visit node */
  {
    mod->Visited = VisitedVal; /* Node already visited */
    for (n = 1; n <= mod->Modules-1; n++) /* traverse each node of subtree */
    {
      if ( (*mod->ModuleTable)[n] != NULL ) /* not a dummy entry */
      {
        TraverseModule( (*mod->ModuleTable)[n] );
      }
    }
    ModuleNodeFunc(mod); /* Perform operation for current node */
  }
}

/***************************************************************************
 * OutputInitJump - Output JMP or JSR for module node
 ***************************************************************************/
static OutputInitJump(mod)
struct Module *mod;
{
  if (mod == mainmod)
  {
    PutWord(InstJMPAbs);
  }
  else
  {
    PutWord(InstJSRAbs);
  }
  PutLong(mod->CodeBaseOffset + 4);
}

/***************************************************************************
 * OutputInitReloc - Output a relocation entry for a module node
 ***************************************************************************/
static OutputInitReloc(mod)
struct Module *mod;
{
  PutLong(1L);
  PutLong((long)mod->Hunk);
  PutLong((long)Offset);
  Offset += 6L;
}

/* LinkHeader - generate the initial hunk for a Modula-2 program. The purpose
 *              of this hunk is to initialize a program. This proc generates
 *              the following code:
 *              JSR xxx.L  ( System.Module_Body )
 *              JSR xxx.L  ( SystemCPU.Module_Body )   if M68020 code generated
 *              JSR xxx.L  ( Library1.Module_Body )
 *              JSR xxx.L  ( Library2.Module_Body )
 *                 ...
 *              JSR xxx.L  ( LibraryN.Module_Body )
 *              JMP xxx.L  ( Main.Module_Body )
 */
static Output_Load_File_Header()
{ register struct Module 	*mp;
  register struct Module 	*mod;
  register struct Module 	*sysmod,
                                *syscpumod,
                                *sysrealmod,
                                *syslongrealmod;
  register long			InitCodeSize;
  register long			InitCodeLWSize;
  register short		InitCodeAlign;
  register short		i;
  register long			tmpPos;
  register long			oldPos;

  mainmod = ModuleListHead;
  sysmod = FindModule("System");
  if (sysmod == NULL) {
      ewprintf("ERROR: Module 'System' Not Imported!");
      Error_Status = TRUE;
      Error_Code = ABORT_LINK_NO_MSG;
      return;
  }
  syscpumod = FindModule("SystemCPU");

  syslongrealmod = FindModule("SystemIEEEdp");
  sysrealmod = FindModule("SystemIEEE");
  if (sysrealmod == NULL) {
     sysrealmod = FindModule("SystemFFP");
     if ((sysrealmod != NULL) && (syslongrealmod != NULL)) {
       ewprintf("ERROR: LONGREAL requires IEEE REAL!");
       Error_Status = TRUE;
       Error_Code = ABORT_LINK_NO_MSG;
       return;
     }
  } else {
     if (FindModule("SystemFFP") != NULL) {
       ewprintf("ERROR: Conflicting REAL implementations!");
       Error_Status = TRUE;
       Error_Code = ABORT_LINK_NO_MSG;
       return;
     }
  }
  /* IF NEITHER REAL MODULE, ASSUME FFP (BUT CURRENTLY NO MATTER) */

  InitCodeLWSize = LongWord((long)((NextHunk-1) * 6));
  InitCodeSize   = InitCodeLWSize * 4;
  InitCodeAlign  = (((NextHunk-1) * 6) % 4) != 0;

  /* Update total */
  TotalInit = InitCodeSize;

  PutLong(HUNKHeader);			/* hunk tag */
  PutLong(0L);				/* end of resident lib list */
  PutLong((long)NextHunk);		/* hunk table size */
  PutLong(0L);				/* frist hunk in table */
  PutLong((long)(NextHunk-1));		/* last hunk in table */
  PutLong(InitCodeLWSize);		/* size of first hunk */
  mod = ModuleListHead;
  while (mod != NULL)			/* generate rest of hunk table */
  {
    PutLong(mod->HunkLWSize);
    mod = mod->Next;
  }
  PutLong(HUNKCode);			/* generate code hunk */
  PutLong(InitCodeLWSize);

  PutWord(InstJSRAbs);			/* JSR System.Module_Body */
  PutLong(sysmod->CodeBaseOffset + 4);

  /* Generate a JSR to each library module and a JMP to the main module.
   * The order of the jumps in the table is determined by traversing the
   * module tree. */
  sysmod->Visited = TRUE; /* Skip the System module */

  if (sysrealmod != 0) {
     PutWord(InstJSRAbs);               /* JSR SystemIEEE/FFP.Module_Body */
     PutLong(sysrealmod->CodeBaseOffset + 4);
     sysrealmod->Visited = TRUE; /* Skip the SystemIEEE/FFP module */
  }

  if (syslongrealmod != 0) {
     PutWord(InstJSRAbs);               /* JSR SystemIEEEdp.Module_Body */
     PutLong(syslongrealmod->CodeBaseOffset + 4);
     syslongrealmod->Visited = TRUE; /* Skip the SystemIEEEdp module */
  }

  if (syscpumod != 0) {
     PutWord(InstJSRAbs);               /* JSR SystemCPU.Module_Body */
     PutLong(syscpumod->CodeBaseOffset + 4);
     syscpumod->Visited = TRUE; /* Skip the SystemCPU module */
  }

  VisitedVal = TRUE;
  ModuleNodeFunc = OutputInitJump; /* Function to call for each node */
  TraverseModule(ModuleListHead);

  /* align code hunk on long word boundary */
  if (InitCodeAlign)
  {
    PutWord(0);
  }

  /* Generate SourceLevelDebugger Info in a HUNKDebug */
  if (LinkOptSLDInfo)
  {
    PutLong(HUNKDebug);

    /* remember current position for backpatching */
    tmpPos = ftell(OutFile);
    PutLong(0L);

    /* # of modules */
    PutWord(NextHunk - 1);

    /* Output Module Records */
    mod = ModuleListHead;
    while (mod != NULL)
    {
      PutLong(mod->ModuleBaseOffset);
      PutLong(mod->CodeBaseOffset);
      PutWord(mod->Key[0]);
      PutWord(mod->Key[1]);
      PutWord(mod->Key[2]);
      i = -1;
      do
      {
        i++;
        PutChar((*mod->Name)[i]);
      }
      while ((*mod->Name)[i] != 0);
      if (!(i & 1)) { PutChar(0); }

      mod = mod->Next;
    }

    /* Pad block to longword boundary */
    i = ftell(OutFile) % 4L;
    if (i != 0)
    {
      i = 4 - i;
      while (i != 0)
      {
        PutChar(0);
        i--;
      }
    }

    /* Backpatch size of block */
    oldPos = ftell(OutFile);

    /* Backpatch size of block */
    fseek(OutFile, tmpPos, 0);
    PutLong( (oldPos - tmpPos - 4L) / 4L );

    /* Restore to old position */
    fseek(OutFile, oldPos, 0);

  } /* LinkOptSLDInfo */

  PutLong(HUNKReloc32);			/* hunk reloc tag */
  Offset = 2;				/* relocation offset = 2 */

  /* generate reloc for JSR to System */
  PutLong(1L);
  PutLong((long)sysmod->Hunk);
  PutLong(Offset);
  Offset += 6;

  if (sysrealmod != 0) {
     /* Generate reloc for JSR to SystemIEEE/FFP */
     PutLong(1L);
     PutLong((long)sysrealmod->Hunk);
     PutLong(Offset);
     Offset += 6;
     sysrealmod->Visited = FALSE; /* Skip the SystemIEEE/FFP module */
  }

  if (syslongrealmod != 0) {
     /* Generate reloc for JSR to SystemIEEEdp */
     PutLong(1L);
     PutLong((long)syslongrealmod->Hunk);
     PutLong(Offset);
     Offset += 6;
     syslongrealmod->Visited = FALSE; /* Skip the SystemIEEEdp module */
  }

  if (syscpumod != 0) {
     /* Generate reloc for JSR to SystemCPU */
     PutLong(1L);
     PutLong((long)syscpumod->Hunk);
     PutLong(Offset);
     Offset += 6;
     syscpumod->Visited = FALSE; /* Skip the SystemCPU module */
  }

  /* Generate a relocation entry for each JSR or JMP in the module initalizaiton
   * table. This operation is performed by traversing the module tree. */
  sysmod->Visited = FALSE; /* Skip the System module */
  VisitedVal = FALSE;
  ModuleNodeFunc = OutputInitReloc; /* Function to output a reloc entry */
  TraverseModule(ModuleListHead);

  /* Terminate Reloc32 block */
  PutLong(0L);

  /* Terminate Hunk */
  PutLong(HUNKEnd);
}

/***************************************************************************
 * Util_Free_Dir_List - free all nodes of Dir_List
 ***************************************************************************/
static Util_Free_Dir_List()
{ register struct Dir_Node *p,*f;

  p = Dir_List_Head;
  while (p != NULL)
  {
    f = p;
    p = p->Next;
    free(f);
  }
  Dir_List_Head = NULL;
}

/***************************************************************************
 * Util_Dir_List_Add_Node - add node to dir list
 ***************************************************************************/
static Util_Dir_List_Add_Node(str, len)
register char  str[];
register int   len;
{ register struct Dir_Node *p;
  register int    	   i;

  p = (struct Dir_Node*)malloc(sizeof(struct Dir_Node) + (len + 1));
  if (p != NULL)
  {
    p->Next = NULL;
    i = 0;
    while (len)
    {
      p->Path[i] = str[i];
      i++; 
      len--;
    }
    p->Path[i] = 0; /* Null-term string */
    if (Dir_List_Head == NULL)
    {
      Dir_List_Head = p;
      Dir_List_Tail = p;
    }
    else
    {
      Dir_List_Tail->Next = p;
      Dir_List_Tail = p;
    }
  }
}

/***************************************************************************
 * Util_Scan_Dir_List - search throught directories for a file, when found
 *  append directory prefix to original filename.
 ***************************************************************************/
static Util_Scan_Dir_List(fname)
register char fname[];
{ register long            lock;
  register struct Dir_Node *p;
  register char            path[128];

  if (Dir_List_Head == NULL)
  {
    return;
  }
  /* Look into current directory */
  lock = (long)Lock(fname, SHARED_LOCK);
  if (lock != NULL)
  {
    UnLock(lock);
    return;
  }

  /* Begin searching thorugh directories for a match */
  p = Dir_List_Head;
  while (p != NULL)
  {
    strcpy(path, &(p->Path));
    strcat(path, fname);
    lock = (long)Lock(path, SHARED_LOCK);
    if (lock != NULL)
    {
      UnLock(lock);
      strcpy(fname, path);
      return;
    }
    
    p = p->Next; /* Advance to next name in list */
  }
}

/***************************************************************************
 * Util_Init_Emacs_Link - This function is used to initialize emacs to work
 *  with the Modula-2 Linker.
 ***************************************************************************/
Util_Init_Emacs_Link()
{
  /* Make "M2L:" the default search path */
  Util_Dir_List_Add_Node(Def_Search_Dir, DEF_SEARCH_DIR_STRLEN);

  Emacs_Signal = AllocSignal(-1L);
  User_Prog_Parent = (struct Process*)FindTask(NULL);
}

/***************************************************************************
 * Util_Exit_Emacs_Link_Cleanup - This function is called when the user
 *  issues an exit command, this code frees memory occupied by structures.
 ***************************************************************************/
Util_Exit_Emacs_Link_Cleanup()
{
  Util_Free_Dir_List();
  if (Default_Main_Module_Name != NULL)
  {
    free(Default_Main_Module_Name);
  }
  if (Link_Output_Dir != NULL)
  {
    free(Link_Output_Dir);
  }

  FreeSignal(Emacs_Signal);
}

/***********************************************************************
 * Util_Test_Main_Module - returns TRUE or FALSE if the main module is set
 **********************************************************************/
static Util_Test_Main_Module()
{
  if (Default_Main_Module_Name == NULL)
  {
    ewprintf("ERROR: The Main Module Has Not Been Specified!");
    return(FALSE);
  }
  return (TRUE);
}

/* utility variables */
static	struct Process	*User_Prog_Proc;
static	short	*sptr;
static	long	*lptr;

/**********************************************************************
 * Proc_Start - this code is the first thing executed by the process
 **********************************************************************/
static Proc_Start()
{
/*>>>>>>>>>>>>>>>
#asm
	public	DataSegPtr
	unlk	a5
	move.L	DataSegPtr(PC),A4	; load ptr to C data segment
#endasm
<<<<<<<<<<<<<<<<<<*/
/*__emit(0x4E5D);*/   /* unlk a5 */
__emit(0x508F);   /* ADDQ.L #8, A7 */   /* readjust stack since no return here */

geta4();

/* Increment Program Running Count */
User_Prog_Count++;

/* Initialize Process Structure For Executing Program */
User_Prog_Proc = (struct Process*)FindTask(NULL);
User_Prog_Proc->pr_CLI        = User_Prog_Parent->pr_CLI;
User_Prog_Proc->pr_CIS        = User_Prog_IO;
User_Prog_Proc->pr_COS        = User_Prog_IO;
User_Prog_Proc->pr_CurrentDir = User_Prog_Parent->pr_CurrentDir;
User_Prog_Proc->pr_WindowPtr  = User_Prog_Parent->pr_WindowPtr;

/* Restore first 6 bytes of code segment */
*sptr = User_Prog_w;
*lptr = User_Prog_l;

if (SysBase->LibNode.lib_Version >= 37) {
   CacheClearU();
}

/*>>>>>>>>>>>>>>>>
#asm
	move.L	_User_Prog_SegList,-(A7)	; push segment list
	move.L	_User_Prog_IO,-(A7)		; push IO handle
	pea	_Proc_End			; push ending code ptr
#endasm
<<<<<<<<<<<<<<<<<<<<<*/
putreg(0, User_Prog_SegList);
__emit(0x2F00);   /* move.l D0, -(A7) */
putreg(0, User_Prog_IO);
__emit(0x2F00);   /* move.l D0, -(A7) */
putreg(0, Proc_End);
__emit(0x2F00);   /* move.l D0, -(A7) */

/* Signal Emacs That User Program Started */
Signal(User_Prog_Parent, 1L << Emacs_Signal);

/* Jump to start of code */
/*>>>>>>>>>>>>>>>>
#asm

	move.L	_User_Prog_Parms_Length,D0
	lea	_User_Prog_Parms,A0
	move.L	_User_Prog_SegList,A1
	add.L	A1,A1
	add.L	A1,A1
	addq.L	#4,A1
	jmp	(A1)
#endasm
<<<<<<<<<<<<<<<<<<<<<*/
putreg(0, User_Prog_Parms_Length);
putreg(8, &User_Prog_Parms);
putreg(9, User_Prog_SegList);
__emit(0xD3C9);     /* add.L A1,A1  */
__emit(0xD3C9);     /* add.L A1,A1  */
__emit(0x5889);     /* addq.L #4,A1 */
__emit(0x4ED1);     /* jmp (A1) */

}

/* return_msg - msg displayed to user upon program termination */
static	char	return_msg[] = "\n== Press <RETURN> To Exit ==";

/* return_ch - buffer to use when waiting for <cr> from user */
static	char	return_ch;

static long *lp;
static long fh;
static void *sg;

/**********************************************************************
 * Proc_End - this code performs some cleanup
 **********************************************************************/
static void Proc_End(void)
{

/*>>>>>>>>>>>>>>>>>
#asm 
	unlk	a5
	move.L	DataSegPtr(PC),A4	; load ptr to C data segment
#endasm
<<<<<<<<<<<<*/
/*__emit(0x4E5D);*/   /* unlk a5 */
__emit(0x508F);   /* ADDQ.L #8, A7 */   /* readjust stack since no return here */
geta4();

/* Write a message to the i/o window, then wait for key to be hit
 * before continuing.
 */
/*>>>>>>>>>>>>>>>
#asm
	public	_Write
	move.L	(SP),D0
	move.L	#29,-(SP)		; length of buffer
	pea	_return_msg		; pointer to buffer
	move.L	D0,-(SP)		; file
	jsr	_Write
	add.L	#12,A7

	move.L	(SP),D0
	move.L	#1,-(SP)		; length of buffer
	pea	_return_ch		; pointer to buffer
	move.L	D0,-(SP)		; file
	jsr	_Read
	add.L	#12,A7

	jsr	_Close			; TOS - FileHandle
	addq.L	#4,A7
	jsr	_UnLoadSeg		; TOS - SegList
	addq.L	#4,A7
#endasm
<<<<<<<<<<<<<*/
lp = (long *)getreg(15);
fh = *lp;
sg = (void *)(*(lp+1));
Write(fh, return_msg, 29);
Read(fh, &return_ch, 1);
Close(fh);
UnLoadSeg(sg);

Forbid();
User_Prog_Count--;

/*>>>>>>>>>>>>
#asm
	rts				; returns to KillProcess
#endasm
<<<<<<<<<<<<<*/
putreg(15, lp+2);
__emit(0x4E75);   /* rts */

}

/***************************************************************************
 * Util_Run_Module - run the specified program
 ***************************************************************************/
static Util_Run_Module(name)
char	*name;
{ char progName[128];	/* name used for loading */
  char windowName[128]; /* window specification */

  /* create a path for loading program (from linker output directory) */
  progName[0] = 0;
  if (Link_Output_Dir != NULL)
  {
    strcat(progName, Link_Output_Dir);
  }
  strcat(progName, name);

  /* Attempt to load program */
  User_Prog_SegList = (long)LoadSeg(progName);
  if (User_Prog_SegList == NULL)
  {
     ewprintf("ERROR: Program '%s' Not Found!", progName);
     return(FALSE);
  }

  /* Construct a window specification from user and name of program */
  strcpy(windowName, User_Prog_Window);
  strcat(windowName, name);

  /* Open the i/o window */
  User_Prog_IO = (long)Open(windowName, MODE_NEWFILE);
  if (User_Prog_IO == NULL)
  {
    ewprintf("ERROR: Can't Open Input/Output Window!");
    UnLoadSeg(User_Prog_SegList);
    return (FALSE);
  }

  /* If i/o is not interactive then not valid */
  if (IsInteractive(User_Prog_IO) != -1L)
  {
    ewprintf("ERROR: Invalid Input/Output Window Specification!");
    Close(User_Prog_IO);
    UnLoadSeg(User_Prog_SegList);
    return (FALSE);
  }

  /* Calculate pointers to start of code */
  sptr = (short *)((User_Prog_SegList * 4) + 4);
  lptr = (long  *)((User_Prog_SegList * 4) + 4 + 2);

  /* Save the first 6 bytes of code */
  User_Prog_w = *sptr;
  User_Prog_l = *lptr;

  /* Patch in JMP to special startup code for process */
  *sptr = 0x4EF9;
  *lptr = (long)Proc_Start;

  if (SysBase->LibNode.lib_Version >= 37) {
     CacheClearU();
  }

  /* Startup user program process */
  if (CreateProc("??",0L,User_Prog_SegList,User_Prog_StackSize) == NULL)
  {
    ewprintf("ERROR: Insufficient Memory To Start User Process!");
    UnLoadSeg(User_Prog_SegList);
    Close(User_Prog_IO);
    return (FALSE);
  }

  /* Wait for signal from user process to resume */
  Wait((1L << Emacs_Signal) | /*??*/(1L << SIGBREAKB_CTRL_C));

  return (TRUE);
}

/**********************************************************************
 * Util_Check_Run_Status - check if any programs still running
 **********************************************************************/
Util_Check_Run_Status()
{
  if (User_Prog_Count != 0)
  {
    ewprintf("ERROR: Can't Exit Editor While User Program(s) Still Running!");
    return(FALSE);
  }
  return (TRUE);
}

#endif
