/* File: file.c  for M2Ed v1.10 */

/*
 * 		File commands.
 */
#include "def.h"
#include <dos/dos.h>
#include <clib/dos_protos.h>

/*---------------------------------------------------------------------------
Changes in M2Ed v01.02 to v01.03
12/18/93

for new Asl file requester

   1. Added value of savemode parameter for all calls to Do_File_Requester().

BUGFIX: Maintain "script" and "delete" bit status of edited file

   1. Added params ScriptFile and NoDeleteFile to call to ffropen().

   2. Added param "newprotect" to insertfile(). If TRUE, then the files
      AmigaDOS Script and Delete protection bit states are saved in the
      buffer's b_flag field.

   3. In writeout(), after close of file, calling dos.SetProtection()
      for script and/or delete bit if either one of these bits was
      set in the buffer's b_flag field.

-----------------------------------------------------------------------------*/

/****************************************************************************
 * Changes in M2Ed v01.04 to v01.05                                         *
 *  2/27/94                                                                 *
 *                                                                          *
 * Multi-select from Input ASL FileReq                                      *
 *                                                                          *
 * 1. Updated for multimode Do_File_Requester() parameter: insert file,     *
 *    visit file, popto file, write file.                                   *
 *                                                                          *
 * 2. In filevisit(), looping on "multi", calling Next_Multiselect() within *
 *    loop to load in selected files. Doing ShowBuffer() only for the last  *
 *    one.                                                                  *
 *                                                                          *
 * 3. Similar changes in poptofile()                                        *
 *                                                                          *
 ****************************************************************************/

BUFFER	*findbuffer();
VOID	makename();
VOID	upmodes();

#ifdef	FILEREQ
extern	int	Do_File_Requester();	/* Display file requester... */
extern  int Next_Multiselect();	        /* if multiple files selected */
extern	char	File_Req_Flag;		/* TRUE = requester, FALSE = kbd */
#endif


/*
 * insert a file into the current buffer. Real easy - just call the
 * insertfile routine with the file name.
 */
/*ARGSUSED*/
fileinsert(f, n, k) {
	register int	s;
	char		fname[NFILEN];

#ifdef	FILEREQ
	if (File_Req_Flag)
	{
	  if(!Do_File_Requester("Insert File",fname,sizeof(fname),
                                FALSE/*savemode*/, FALSE/*multimode*/))
            return(FALSE);
	}
	else
	{
#endif
	if ((s=ereply("Insert file: ", fname, NFILEN)) != TRUE)
		return (s);
#ifdef	FILEREQ
	}
#endif

	adjustcase(fname);

        /* Convert relative->absolute file path */
        if (!GetAbsFilePath(fname)) { return(FALSE); }

	return (insertfile(fname, (char *) NULL, FALSE));
                     /* don't set buffer name or protection state */
}

/*
 * Select a file for editing.
 * Look around to see if you can find the
 * fine in another buffer; if you can find it
 * just switch to the buffer. If you cannot find
 * the file, create a new buffer, read in the
 * text, and switch to the new buffer.
 *
 * Now, multi-select for the filereq is supported as well.
 */
/*ARGSUSED*/
filevisit(f, n, k) {
	register BUFFER	*bp;
	int		s;
	char		fname[NFILEN];
        int             multi;

#ifdef	FILEREQ
	if (File_Req_Flag)
	{
	  if((multi = Do_File_Requester("Find File",fname,sizeof(fname),
                                 FALSE/*savemode*/, TRUE/*multimode*/)) <= 0)
            return(FALSE);
	}
	else
	{
#endif
        multi = 1;
	if ((s=ereply("Find file: ", fname, NFILEN)) != TRUE)
		return (s);
#ifdef	FILEREQ
	}
#endif

        do {

           /* Convert relative->absolute file path */
           if (!GetAbsFilePath(fname)) { return(FALSE); }

           if ((bp = findbuffer(fname, &s)) == NULL) return s;

           curbp = bp;
           if (showbuffer(bp, curwp, WFHARD) != TRUE) return FALSE;

           if (bp->b_fname[0] == 0) {
              if (!readin(fname)) {		/* Read it in. */
                 return FALSE;
              }
           }

#ifdef	FILEREQ
           if (multi > 1) {
              if (!Next_Multiselect(fname,sizeof(fname))) {
                 return FALSE;
              }
           }
#endif

         } while (--multi > 0);

         return TRUE;
}

/*
 * Pop to a file in the other window. Same as last function, just
 * popbuf instead of showbuffer.
 *
 * Now, multi-select for the filereq is supported as well.
 */
/*ARGSUSED*/
poptofile(f, n, k) {
	register BUFFER	*bp;
	register WINDOW	*wp;
	int		s;
	char		fname[NFILEN];
        int             multi;

#ifdef	FILEREQ
	if (File_Req_Flag)
	{
	  if((multi = Do_File_Requester("Find File In Other Window",fname,
                                sizeof(fname), FALSE/*savemode*/,
                                TRUE/*multimode*/)) <= 0)
            return(FALSE);
	}
	else
	{
#endif
        multi = 1;
	if ((s=ereply("Find file in other window: ", fname, NFILEN)) != TRUE)
		return (s);
#ifdef	FILEREQ
	}
#endif

        do {

           /* Convert relative->absolute file path */
           if (!GetAbsFilePath(fname)) { return(FALSE); }

           if ((bp = findbuffer(fname, &s)) == NULL) return s;

           if ((wp = popbuf(bp)) == NULL) return FALSE;
           curbp = bp;
           curwp = wp;

           if (bp->b_fname[0] == 0) {
              if (!readin(fname)) {		/* Read it in. */
                 return FALSE;
              }
           }

#ifdef	FILEREQ
           if (multi > 1) {
              if (!Next_Multiselect(fname,sizeof(fname))) {
                 return FALSE;
              }
           }
#endif

        } while (--multi > 0);

        return TRUE;
}

/*
 * given a file name, either find the buffer it uses, or create a new
 * empty buffer to put it in.
 */
BUFFER *
findbuffer(fname, s) char *fname; int *s; {
	register BUFFER	*bp;
	char		bname[NBUFN];

	adjustcase(fname);
	for (bp=bheadp; bp!=NULL; bp=bp->b_bufp) {
		if (strcmp(bp->b_fname, fname) == 0) {
			return bp;
		}
	}
	makename(bname, fname);			/* New buffer name.	*/
	while ((bp=bfind(bname, FALSE)) != NULL) {
		*s = ereply("Buffer name: ", bname, NBUFN);
		if (*s == ABORT)		/* ^G to just quit	*/
			return NULL;
		if (*s == FALSE) {		/* CR to clobber it	*/
			bp->b_fname[0] = '\0';
			break;
		}
	}
	if (bp == NULL) bp = bfind(bname, TRUE);
	*s = FALSE;
	return bp;
}

/*
 * Read the file "fname" into the current buffer.
 * Make all of the text in the buffer go away, after checking
 * for unsaved changes. This is called by the "read" command, the
 * "visit" command, and the mainline (for "uemacs file").
 */
readin(fname) char *fname; {
	register int		status;
	register WINDOW		*wp;

	if (bclear(curbp) != TRUE)		/* Might be old.	*/
		return TRUE;
	status = insertfile(fname, fname, TRUE) ;
	curbp->b_flag &= ~BFCHG;		/* No change.		*/
	for (wp=wheadp; wp!=NULL; wp=wp->w_wndp) {
		if (wp->w_bufp == curbp) {
			wp->w_linep = lforw(curbp->b_linep);
			wp->w_dotp  = lforw(curbp->b_linep);
			wp->w_doto  = 0;
			wp->w_markp = NULL;
			wp->w_marko = 0;
		}
	}
	return status;
}
/*
 * insert a file in the current buffer, after dot. Set mark
 * at the end of the text inserted, point at the beginning.
 * Return a standard status. Print a summary (lines read,
 * error message) out as well. If the
 * BACKUP conditional is set, then this routine also does the read
 * end of backup processing. The BFBAK flag, if set in a buffer,
 * says that a backup should be taken. It is set when a file is
 * read in, but not on a new file (you don't need to make a backup
 * copy of nothing).
 *
 * Warning: Adds a trainling nl to files that don't end in one!
 * Need to fix, but later (I suspect that it will require a change
 * in the fileio files for all systems involved).
 */
insertfile(fname, newname, newprotect) char fname[], newname[]; int newprotect; {
	register LINE	*lp1;
	register LINE	*lp2;
	LINE		*olp;			/* Line we started at */
	int		opos;			/* and offset into it */
	register WINDOW	*wp;
	register int	i;
	register int	nbytes;
	int		s, nline;
	BUFFER		*bp;
	char		line[NLINE];
        int             ScriptFile;
        int             NoDeleteFile;

	bp = curbp;				/* Cheap.		*/
	if (newname != (char *) NULL)
		(VOID) strcpy(bp->b_fname, newname);
	if ((s=ffropen(fname, &ScriptFile, &NoDeleteFile)) == FIOERR) 	/* Hard file open.	*/
		goto out;
	if (s == FIOFNF) {			/* File not found.	*/
		if (kbdmop == NULL)
			if (newname != NULL)
				ewprintf("(New file)");
			else	ewprintf("(File not found)");
		goto out;
	}
        if (newprotect) {
           if (ScriptFile) {
              bp->b_flag |= BFSCRIPT;
           }
           if (NoDeleteFile) {
              bp->b_flag |= BFDELPRO;
           }
        }

	opos = curwp->w_doto;
	/* Open a new line, at point, and start inserting after it */
	(VOID) lnewline();
	olp = lback(curwp->w_dotp);
	nline = 0;			/* Don't count fake line at end */
	while ((s=ffgetline(line, NLINE)) == FIOSUC) {
		nbytes = strlen(line);
		if ((lp1=lalloc((RSIZE) nbytes)) == NULL) {
			s = FIOERR;		/* Keep message on the	*/
			break;			/* display.		*/
		}
		lp2 = lback(curwp->w_dotp);
		lp2->l_fp = lp1;
		lp1->l_fp = curwp->w_dotp;
		lp1->l_bp = lp2;
		curwp->w_dotp->l_bp = lp1;
		for (i=0; i<nbytes; ++i)
			lputc(lp1, i, line[i]);
		++nline;
	}
	(VOID) ffclose();			/* Ignore errors.	*/
	if (s==FIOEOF && kbdmop==NULL) {	/* Don't zap an error.	*/
		if (nline == 1)
			ewprintf("(Read 1 line)");
		else
			ewprintf("(Read %d lines)", nline);
	}
	/* Set mark at the end of the text */
	curwp->w_markp = curwp->w_dotp;
	curwp->w_marko = curwp->w_doto;
	/* Now, delete the results of the lnewline we started with */
	curwp->w_dotp = olp;
	curwp->w_doto = opos;
	(VOID) ldelnewline();
	curwp->w_doto = opos;			/* and dot is right	*/
#ifdef	BACKUP
	if (newname != NULL)
		bp->b_flag |= BFCHG | BFBAK;	/* Need a backup.	*/
	else	bp->b_flag |= BFCHG;
#else
	bp->b_flag |= BFCHG;
#endif
	/* if the insert was at the end of buffer, set lp1 to the end of
	 * buffer line, and lp2 to the beginning of the newly inserted
	 * text.  (Otherwise lp2 is set to NULL.)  This is 
	 * used below to set pointers in other windows correctly if they
	 * are also at the end of buffer.
	 */
	lp1 = bp->b_linep;
	if (curwp->w_markp == lp1)
		lp2 = curwp->w_dotp;
	else {
out:		lp2 = NULL;
	}
	for (wp=wheadp; wp!=NULL; wp=wp->w_wndp) {
		if (wp->w_bufp == curbp) {
			wp->w_flag |= WFMODE|WFEDIT;
			if (wp != curwp && lp2 != NULL) {
				if (wp->w_dotp == lp1)
					wp->w_dotp = lp2;
				if (wp->w_markp == lp1)
					wp->w_markp = lp2;
				if (wp->w_linep == lp1)
					wp->w_linep = lp2;
			}
		}
	}
	if (s == FIOERR)			/* False if error.	*/
		return (FALSE);
	return (TRUE);
}

/*
 * Take a file name, and from it
 * fabricate a buffer name. This routine knows
 * about the syntax of file names on the target system.
 * BDC1		left scan delimiter.
 * BDC2		optional second left scan delimiter.
 * BDC3		optional right scan delimiter.
 */
VOID
makename(bname, fname) char bname[]; char fname[]; {
	register char	*cp1;
	register char	*cp2;

	cp1 = &fname[0];
	while (*cp1 != 0)
		++cp1;
#ifdef	BDC2
	while (cp1!=&fname[0] && cp1[-1]!=BDC1 && cp1[-1]!=BDC2)
		--cp1;
#else
	while (cp1!=&fname[0] && cp1[-1]!=BDC1)
		--cp1;
#endif
	cp2 = &bname[0];
#ifdef	BDC3
	while (cp2!=&bname[NBUFN-1] && *cp1!=0 && *cp1!=BDC3)
		*cp2++ = *cp1++;
#else
	while (cp2!=&bname[NBUFN-1] && *cp1!=0)
		*cp2++ = *cp1++;
#endif
	*cp2 = 0;
}

/*
 * Ask for a file name, and write the
 * contents of the current buffer to that file.
 * Update the remembered file name and clear the
 * buffer changed flag. This handling of file names
 * is different from the earlier versions, and
 * is more compatable with Gosling EMACS than
 * with ITS EMACS.
 */
/*ARGSUSED*/
filewrite(f, n, k) {
	register int	s;
	char		fname[NFILEN];

#ifdef	FILEREQ
	if (File_Req_Flag)
	{
	  if(!Do_File_Requester("Write File",fname,sizeof(fname),
                                TRUE/*savemode*/, FALSE/*multimode*/))
            return(FALSE);
	}
	else
	{
#endif
	if ((s=ereply("Write file: ", fname, NFILEN)) != TRUE)
		return (s);
#ifdef	FILEREQ
	}
#endif

	adjustcase(fname);

        /* Convert relative->absolute file path */
        if (!GetAbsFilePath(fname)) { return(FALSE); }

	if ((s=writeout(curbp, fname)) == TRUE) {
		(VOID) strcpy(curbp->b_fname, fname);
#ifdef	BACKUP
		curbp->b_flag &= ~(BFBAK | BFCHG);
#else
		curbp->b_flag &= ~BFCHG;
#endif
		upmodes(curbp);
	}
	return (s);
}

/*
 * Save the contents of the current buffer back into
 * its associated file. Do nothing if there have been no changes
 * (is this a bug, or a feature). Error if there is no remembered
 * file name. If this is the first write since the read or visit,
 * then a backup copy of the file is made.
 * Allow user to select whether or not to make backup files
 * by looking at the value of makebackup.
 */
#ifdef	BACKUP
static	int	makebackup = 0;
#endif

/*ARGSUSED*/
filesave(f, n, k) {
	register int	s;

	if ((curbp->b_flag&BFCHG) == 0)	{	/* Return, no changes.	*/
		if (kbdmop == NULL) ewprintf("(No changes need to be saved)");
		return (TRUE);
	}
	if (curbp->b_fname[0] == 0) {		/* Must have a name.	*/
		ewprintf("No file name");
		return (FALSE);
	}
#ifdef	BACKUP
	if (makebackup && ((curbp->b_flag&BFBAK) != 0)) {
		s = fbackupfile(curbp->b_fname);
		if (s == ABORT)			/* Hard error.		*/
			return FALSE;
		if (s == FALSE			/* Softer error.	*/
		&& (s=eyesno("Backup error, save anyway")) != TRUE)
			return (s);
	}
#endif
	if ((s=writeout(curbp, curbp->b_fname)) == TRUE) {
#ifdef	BACKUP
		curbp->b_flag &= ~(BFCHG | BFBAK);
#else
		curbp->b_flag &= ~BFCHG;
#endif
		upmodes(curbp);
	}
	return (s);
}

#ifdef	BACKUP
/* Since we don't have variables (we probably should)
 * this is a command processor for changing the value of
 * the make backup flag.  If no argument is given,
 * sets makebackup to true, so backups are made.  If
 * an argument is given, no backup files are made when
 * saving a new version of a file. Only used when BACKUP
 * is #defined.
 */
/*ARGSUSED*/
makebkfile(f, n, k)
{
	makebackup = !f;	/* make backup if no argument given */
	ewprintf(makebackup ?	"Backup files enabled" :
				"Disabling backup files");
	return (TRUE);
}
#endif

/*
 * This function performs the details of file
 * writing; writing the file in buffer bp to
 * file fn. Uses the file management routines
 * in the "fileio.c" package. Most of the grief
 * is checking of some sort.
 */
writeout(bp, fn) register BUFFER *bp; char *fn; {
	register int	s;
	register LINE	*lp;
                 long    Protection;

	if ((s=ffwopen(fn)) != FIOSUC)		/* Open writes message.	*/
		return (FALSE);
	lp = lforw(bp->b_linep);		/* First line.		*/
	while (lp != bp->b_linep) {
		if ((s=ffputline(&(ltext(lp))[0], llength(lp))) != FIOSUC)
			break;
		lp = lforw(lp);
	}
	if (s == FIOSUC) {			/* No write error.	*/

		s = ffclose();

		if (s==FIOSUC && kbdmop==NULL)
			ewprintf("Wrote %s", fn);

                if (bp->b_flag & (BFSCRIPT | BFDELPRO) != 0) {
                   Protection = 0;
                   if ((bp->b_flag & BFSCRIPT) != 0) {
                      Protection |= FIBF_SCRIPT;
                   }
                   if ((bp->b_flag & BFDELPRO) != 0) {
                      Protection |= FIBF_DELETE;
                   }
                   SetProtection(fn, Protection);
                }

	} else					/* Ignore close error	*/
		(VOID) ffclose();		/* if a write error.	*/
	if (s != FIOSUC)			/* Some sort of error.	*/
		return (FALSE);
	return (TRUE);
}

/*
 * Tag all windows for bp (all windows if bp NULL) as needing their
 * mode line updated.
 */
VOID
upmodes(bp) register BUFFER *bp; {
	register WINDOW	*wp;

	for (wp = wheadp; wp != NULL; wp = wp->w_wndp)
		if (bp == NULL || wp->w_bufp == bp) wp->w_flag |= WFMODE;
}
