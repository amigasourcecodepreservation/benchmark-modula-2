/****************************************************************************
 *                    MODULA-2 Software Construction Set                    *
 *                         (c) 1993 by Jim Olinger                          *
 ****************************************************************************
 * Name: filereqasl.c                      Version: Amiga.01.10             *
 * Created: 12/05/93   Updated:  2/27/94   Author: Tom Breeden              *
 * Description: File Requester Functions for ASL File Requester.            *
 ****************************************************************************/
/*                                                                          *
 * Tom Breeden                                                              *
 * Aglet Software                                                           *
 * Box 3314                                                                 *
 * Charlottesville, VA 22903                                                *
 * tmb@virginia.edu                                                         *
 *                                                                          *
 ****************************************************************************/

/****************************************************************************
 * Changes in M2Ed v01.04 to v01.05                                         *
 *  2/27/94                                                                 *
 *                                                                          *
 * Multi-select from Input ASL FileReq                                      *
 *                                                                          *
 * 1. Do_Asl_Requester() has "multimode" param added as well.               *
 *                                                                          *
 * 2. Setting FRF_DOMULTISELECT into flags1 tag if multimode.               *
 *                                                                          *
 * 3. Changed return value for Do_Asl_Requester() if successful to          *
 *    indicate the number of selected inputs.                               *
 *                                                                          *
 * 4. Added a new func, "Next_Multiselect()". Using a static file global    *
 *   to keep track of the next multi-select element, this returns it or     *
 *   FALSE.                                                                 *
 *                                                                          *
 ****************************************************************************/

/****************************************************************************
 * Changes in M2Ed v01.05 to v01.06                                         *
 *  3/14/94                                                                 *
 *                                                                          *
 * Multi-select from Input ASL FileReq                                      *
 *                                                                          *
 * 1. "multimode" is now being used only for v2.1 (V38) Asl or later, since *
 *     the v2.04 (V37) library does multi file requesters so poorly.        *
 *                                                                          *
 ****************************************************************************/

#ifdef FILEREQ

/*----------------------- Asl File Requester ---------------*/

#include <exec/types.h>
#include <libraries/asl.h>
#include <exec/libraries.h>
#include <utility/tagitem.h>
#include <clib/exec_protos.h>
#include <clib/asl_protos.h>

struct Library              *AslBase = 0;
static int                   AslLibTried = FALSE;
static struct FileRequester *AslRq = 0;
static int                   NxtPtr = -1;

/*--------------------*/
 void CleanUpAsl(void)
/*--------------------*/
{

if (AslRq != 0) {
   FreeAslRequest(AslRq);
   AslRq = 0;
}

if (AslBase != 0) {
   CloseLibrary(AslBase);
   AslBase = 0;
}

} /*END CleanUpAsl*/

/*-----------------------------------------------*/
 int Next_Multiselect(char *filepath, int length)
/*-----------------------------------------------*/
          /* filepath: pointer to buffer to store filepath into */
          /* length:   length of callers filepath buffer        */
          /* returns   TRUE or FALSE                            */
{
int            len;

if (NxtPtr == -1) {
   return FALSE;
}

if (NxtPtr < AslRq->fr_NumArgs) {

   if ((length-1) > (strlen(AslRq->fr_ArgList[NxtPtr].wa_Name)
                            + strlen(AslRq->fr_Drawer))) {
      strcpy(filepath, AslRq->fr_Drawer);
      if (((len = strlen(filepath)) > 0)
                  && (filepath[len-1] != ':')
                  && (filepath[len-1] != '/')) {
         strcat(filepath, "/");
      }
      strcat(filepath, AslRq->fr_ArgList[NxtPtr].wa_Name);

   }
   NxtPtr++;
   return 1;

} else {

   NxtPtr = -1;
   return FALSE;

}

} /*END Next_Multiselect*/

/***************************************************************************
 * Do_Asl_Requester - utility function called on to put a file requester...
 * returns   >0 = got one or more inputs,
 *            0 = canceled or no input,
 *           -1 = No ASL library
 ***************************************************************************/
int Do_Asl_Requester(char *title, char *filepath, int length, int savemode,
                     int multimode)

          /* title: title to be displayed in the requester */
          /* filepath: pointer to buffer to store filepath into */
          /* length: length of callers filepath buffer */
          /* savemode: TRUE if called to save a file */
          /* multimode: TRUE if multi-select is to be offered */
{
register  char res;
int            len;
long           flags1;

res = FALSE;     /* result of func */

if (!AslLibTried) {

   AslLibTried = TRUE;

   AslBase = OpenLibrary(AslName, 0);
   if (AslBase != 0) {
      atexit(CleanUpAsl);
      AslRq = AllocAslRequestTags(ASL_FileRequest,
                                  ASLFR_InitialFile, "NoName.mod",
                                  ASLFR_InitialPattern, "#?(.def|.mod)",
                                  ASLFR_Flags2, FRF_REJECTICONS,
                                  TAG_END);
                    /* NOTE: v3.0 (at least) ASL lib in multi-select mode */
                    /*       leaks memory if ok hit but no file selected  */
      if (AslRq == 0) {
         CloseLibrary(AslBase);
      } else {
         atexit(CleanUpAsl);
      }
   }

}

if (AslBase != 0) {

   NxtPtr = -1;
   multimode = multimode && (AslBase->lib_Version >= 38);  /* 2.1 only */

   flags1 = (savemode ? (FRF_DOPATTERNS | FRF_DOSAVEMODE)
                      : (FRF_DOPATTERNS));
   if (multimode) {
      flags1 |= FRF_DOMULTISELECT;
   }

   res = AslRequestTags(AslRq, 
                        ASLFR_TitleText, title,
                        ASLFR_Flags1, flags1,
                        TAG_END);
   if (res) {                           /* may return non-zero even if no files */

      len = strlen(AslRq->fr_Drawer);
      if (!multimode) {
         if (strlen(AslRq->fr_File) > 0) {
            len += strlen(AslRq->fr_File);
            res = 1;
         } else {
            res = 0;
         }
      } else {
         res = AslRq->fr_NumArgs;
         if (AslRq->fr_NumArgs > 0) {
            len += strlen(AslRq->fr_ArgList[0].wa_Name);
         }
      }

      if (res && ((length-1) > len)) {
        strcpy(filepath, AslRq->fr_Drawer);
        if (((len = strlen(filepath)) > 0)
                     && (filepath[len-1] != ':')
                     && (filepath[len-1] != '/')) {
           strcat(filepath, "/");
        }
        if (!multimode) {
           strcat(filepath, AslRq->fr_File);
        } else {
           strcat(filepath, AslRq->fr_ArgList[0].wa_Name);
           if (res > 1) {
              NxtPtr = 1;
           }
        }
      } else {
        res = 0;
      }
   }

} else {

   res = -1;

}

return (res);

}

#endif /* FILEREQ */
